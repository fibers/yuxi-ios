//
//  HttpTool.h
//  AFNetworking iOS Example
//
//  Created by define on 14-9-22.
//  Copyright (c) 2014年 Gowalla. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "DDReachability.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
typedef void (^HttpSuccessBlock)(id JSON);
typedef void (^HttpFailureBlock)(NSError *error);

@interface HttpTool : NSObject

+ (void)getWithPath:(NSString *)path params:(NSDictionary *)params success:(HttpSuccessBlock)success failure:(HttpFailureBlock)failure;
+ (void)postWithPath:(NSString *)path params:(NSDictionary *)params success:(HttpSuccessBlock)success failure:(HttpFailureBlock)failure;

@end
