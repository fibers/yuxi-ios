//
//  HttpTool.m
//  AFNetworking iOS Example
//
//  Created by define on 14-9-22.
//  Copyright (c) 2014年 Gowalla. All rights reserved.
//

#import "HttpTool.h"
#import "RNEncryptor.h"
@implementation HttpTool

#pragma mark - 检测网络连接
static  long long lastShowTime;
+ (BOOL)isConnectionAvailable
{
    
    BOOL isExistenceNetwork = YES;
    DDReachability *reach = [DDReachability reachabilityWithHostname:@"www.apple.com"];
    switch ([reach currentReachabilityStatus]) {
        case NotReachable:
            isExistenceNetwork = NO;
            //NSLog(@"notReachable");
            break;
        case ReachableViaWiFi:
            isExistenceNetwork = YES;
            //NSLog(@"WIFI");
            break;
        case ReachableViaWWAN:
            isExistenceNetwork = YES;
            //NSLog(@"3G");
            break;
    }
    
    if (!isExistenceNetwork) {
        NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
        long long dTime = [[NSNumber numberWithDouble:time] longLongValue];
        if (lastShowTime + 60 < dTime) {
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"当前收不到二次元信号…" message:@"请检查一下自己的网络" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            lastShowTime = dTime;
        }
        
             return NO;
        
    }else{

         return isExistenceNetwork;
        
    }
    
}




+(void)getWithPath:(NSString *)path params:(NSDictionary *)params success:(HttpSuccessBlock)success failure:(HttpFailureBlock)failure
{

    if ([self isConnectionAvailable]) {

        [self requestWithPath:path params:params success:success failure:failure method:@"GET"];

    }

}

+(void)postWithPath:(NSString *)path params:(NSDictionary *)params success:(HttpSuccessBlock)success failure:(HttpFailureBlock)failure
{
    if ([self isConnectionAvailable]) {

        [self requestWithPath:path params:params success:success failure:failure method:@"POST"];
    }

}


+ (void)requestWithPath:(NSString *)path params:(NSDictionary *)params success:(HttpSuccessBlock)success failure:(HttpFailureBlock)failure method:(NSString *)method
{
    NSMutableDictionary * dicM = [NSMutableDictionary dictionaryWithDictionary:params];
    NSString * userID = USERID == nil ?@"":USERID;
    NSData * encryptedData = [userID dataUsingEncoding:NSUTF8StringEncoding];
    NSError * error;
    NSData * enData = [RNEncryptor encryptData:encryptedData withSettings:kRNCryptorAES256Settings password:@"Higgs_Yuxi.Login@PassWord!" error:&error];
    if (error) {
        LOG(@"%@",error.localizedDescription);
    }
    [dicM setObject:enData forKey:@"tokenX"];
    params = dicM ;

    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@/%@",ROOT_URL,path];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"text/plain",@"text/html", nil];
    if ([method isEqualToString:@"GET"]) {
        [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [SVProgressHUD dismiss];
            success(responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
            [SVProgressHUD dismiss];
            [SVProgressHUD showErrorWithStatus:@"数据请求失败" duration:2.0];
            
        }];
    }else{
        [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [SVProgressHUD dismiss];
            success(responseObject);
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
            [SVProgressHUD dismiss];
            [SVProgressHUD showErrorWithStatus:@"数据请求失败" duration:2.0];
        }];
    }
    
}

@end
