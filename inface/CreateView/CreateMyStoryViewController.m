//
//  CreateMyStoryViewController.m
//  inface
//
//  Created by Mac on 15/5/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "CreateMyStoryViewController.h"
#import <UITextView+Placeholder.h>
#import "MBProgressHUD.h"

@interface CreateMyStoryViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomToSuperView;
@property(nonatomic,strong)UILabel * placeLable;
@property(nonatomic,copy)MBProgressHUD * hud;

@end

@implementation CreateMyStoryViewController



//左上角按钮
-(void)returnBackl{
    if (self.isModifyStory && self.chatInputTextView.text.length > 0) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"是否保存更新的内容" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"保存",nil];
        alertview.tag = 10000;
        [alertview show];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}


-(MBProgressHUD *)hud{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc]initWithView:self.view];
    }
    return _hud;
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10000) {
        if (buttonIndex== 1) {
            NSDictionary * params = @{@"uid":USERID,@"storyid":[self.storyDic objectForKey:@"id"],@"portraitimg":self.imgSerUrl,@"title":self.chatInputTextField.text,@"content":self.chatInputTextView.text,@"token":@"110"};
            [HttpTool postWithPath:@"ModifySelfStoryContent" params:params success:^(id JSON) {
                if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                    //更新成功
                    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"更新成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alertview show];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"ModifySelfStory" object:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                }
                
            } failure:^(NSError *error) {
                
                
            }];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
//右上角按钮
-(void)returnBackr{
    RightButtonCreate.userInteractionEnabled=NO;
    if ([self.chatInputTextField.text isEmpty]) {
        RightButtonCreate.userInteractionEnabled=YES;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入名称" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([self.chatInputTextView.text isEmpty]) {
        RightButtonCreate.userInteractionEnabled=YES;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入简介" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    
    if (self.chatInputTextField.text.length > 15)
    {
        self.chatInputTextField.text = [self.chatInputTextField.text substringToIndex:15];
    }
    
    //创建我的自戏接口(Post)，创建成功时
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    __block NSDictionary *params;
    //先创建一个临时群
    NSString * user_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];
    if(self.isModifyStory)
    {
        //编辑自戏
        NSString * imgRandomPath = self.imgSerUrl;
        NSData * strData = [self.chatInputTextView.text dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableString * myString = [[NSMutableString alloc]initWithData:strData encoding:NSUTF8StringEncoding];
        NSString * postString = [NSString stringWithFormat:@"%@",myString];
        
        NSDictionary * params = @{@"uid":USERID,@"storyid":[self.storyDic objectForKey:@"id"],@"storyimg":imgRandomPath,@"title":self.chatInputTextField.text,@"content":postString,@"token":@"110"};
        [self.hud setLabelText:@"数据请求中，请稍后."];
        [self.hud setMode:MBProgressHUDModeIndeterminate];
        [self.view addSubview:self.hud];
        [self.hud show:YES];
        [self.view bringSubviewToFront:self.hud];

        [HttpTool postWithPath:@"ModifySelfStoryContent" params:params success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                //更新成功
                [self.hud setHidden:YES];

                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"更新成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"ModifySelfStory" object:nil];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } failure:^(NSError *error) {
            RightButtonCreate.userInteractionEnabled=YES;
            [self.hud setLabelText:@"编辑失败"];
            [self.hud setMode:MBProgressHUDModeText];
            [self.hud hide:YES afterDelay:2];

            
        }];
        
    }else
    {

                NSString * groupid = @"100";
                if ([self.imgSerUrl isEmpty]) {
                    
                    params = @{@"uid":userid,@"title":self.chatInputTextField.text,@"content":self.chatInputTextView.text,@"token":@"110",@"groupid":groupid};
                    
                }else{
                    params = @{@"uid":userid,@"title":self.chatInputTextField.text,@"content":self.chatInputTextView.text,@"portraitimg":self.imgSerUrl,@"token":@"110",@"groupid":groupid};
                }
        [self.hud setLabelText:@"数据请求中，请稍后."];
        [self.hud setMode:MBProgressHUDModeIndeterminate];
        [self.view addSubview:self.hud];
        [self.hud show:YES];
        [self.view bringSubviewToFront:self.hud];
        
                [HttpTool postWithPath:@"CreateSelfStory" params:params success:^(id JSON) {
                    [self.hud setHidden:YES];
                    if ([[JSON objectForKey:@"result"]integerValue]==1) {
                        RightButtonCreate.userInteractionEnabled=YES;
                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"创建成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertview show];
                        
                        [self.navigationController popViewControllerAnimated:YES];
                    
                    }else{
                        RightButtonCreate.userInteractionEnabled=YES;
                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertview show];
                        
                    }
                    
                } failure:^(NSError *error) {
                    RightButtonCreate.userInteractionEnabled=YES;
                    [self.hud setLabelText:@"创建失败"];
                    [self.hud setMode:MBProgressHUDModeText];
                    [self.hud hide:YES afterDelay:2];
                }];
            }

    }
    



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor=[UIColor whiteColor];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];

    [[IQKeyboardManager sharedManager]setEnable:NO];
    //左上角按钮
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 45, 44)];
    [LeftButton setTitle:@"取消" forState:UIControlStateNormal];
    LeftButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [LeftButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    RightButtonCreate=RightButton;
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    if (self.isModifyStory == YES) {
        self.navigationItem.title=@"编辑自戏";
        
    }else{
        self.navigationItem.title=@"创建自戏";
    }
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInputTextView.textColor=[UIColor blackColor];

    self.chatInputTextView.placeholder = @"请输入自戏内容";

    self.chatInputTextView.layer.borderWidth = 0.8;
    self.chatInputTextView.layer.borderColor = [UIColor colorWithHexString:@"#b0adad"].CGColor;
    self.chatInputTextView.layer.cornerRadius = 8.0;
    self.chatInputTextField.delegate = self;
    self.chatInputTextField.textColor=GRAY_COLOR;
    //    self.textViewHeight.constant = ScreenHeight - 260;
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [[self view] addGestureRecognizer:recognizer];
    self.XianLabel.backgroundColor=XIAN_COLOR;
    
    //添加照片视图85*85
    //图片
    UIImageView *aimageView=[[UIImageView alloc]initWithFrame:CGRectMake(12, ScreenHeight -180, 135, 100)];
    aimageViewPhoto=aimageView;
    aimageView.contentMode = UIViewContentModeScaleAspectFit;
    aimageView.backgroundColor=[UIColor clearColor];
    aimageView.userInteractionEnabled=YES;
    [self.view addSubview:aimageView];
    imagepicker = [[UIImagePickerController alloc]init];
    UITapGestureRecognizer *singleTapa = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(paizhao:)];
    
    [aimageView addGestureRecognizer:singleTapa];
    
    self.imgSerUrl=@"";
    self.circleView = [[EVCircularProgressView alloc]initWithFrame:CGRectMake(36, 31, 28, 28)];
    [aimageViewPhoto addSubview:self.circleView];
    self.circleView.hidden = YES;
    
    if (!self.isModifyStory) {
        aimageView.image=[UIImage imageNamed:@"add-photo640"];
        
    }else{
        [self setStoryValue];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

-(void) keyboardWillShow:(NSNotification *) aNotification {
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    float height = keyboardRect.size.height;
    
    if (height == 0) {
        return  ;
        
    }
    
    //键盘没有弹出时
    
    if (ScreenHeight > 700) {
        height = height + 40;
    }else if(ScreenHeight > 650){
        height = height + 40;
    }else if(ScreenHeight > 480){
        height = height + 20;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _bottomToSuperView.constant = height;
        
    }];
}


-(void) keyboardWillHide:(NSNotification *) note
{
    
    [UIView animateWithDuration:.3f animations:^{
        //改变位置
        _bottomToSuperView.constant = 165;
    }];
    
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    //[self.chatInputTextView resignFirstResponder];
    [self.chatInputTextField resignFirstResponder];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{

}

#pragma mark 增减下滑手势
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gesture
{
    if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
        [self.chatInputTextView resignFirstResponder];
        [self.chatInputTextField resignFirstResponder];
    }
}

#pragma  mark 设置
-(void)setStoryValue
{
    self.chatInputTextField.text = [self.storyDic objectForKey:@"title"];
    
    self.chatInputTextView.text = [self.storyDic objectForKey:@"content"];
    //    NSString * text = self.chatInputTextView.text;
    //    self.chatInputTextView.frame = CGRectMake(12, 60, ScreenWidth, 130);
    //    CGRect orgRect=self.chatInputTextView.frame;//获取原始UITextView的frame
    //    CGSize  size = [text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(ScreenWidth, 2000) lineBreakMode:UILineBreakModeWordWrap];
    ////    CGSize  size = [self.chatInputTextView sizeThatFits:CGSizeMake(ScreenWidth,CGFLOAT_MAX) ];
    //    orgRect.size.height=size.height+10;//获取自适应文本内容高度
    //
    //    self.chatInputTextView.frame=orgRect;//重设UITextView的frame
    //    aimageViewPhoto.frame = CGRectMake(12, 80+ self.chatInputTextView.frame.size.height, 135, 100);
    
    NSURL * url =  [NSURL URLWithString: [self.storyDic objectForKey:@"storyimg" ]] ;
    UIImage * image = [UIImage imageNamed:@"add-photo640"];
    [aimageViewPhoto sd_setImageWithURL:url placeholderImage:image];
    
}



#pragma mark动态改变高度
-(void)textViewDidChange:(UITextView *)textView
{
//该判断用于联想输入
    if (textView.text.length > 0) {
        _placeLable.hidden = YES;
    }else{
        _placeLable.hidden = NO;
        
    }
    if (IOSSystemVersion < 8.0 || ScreenHeight < 568) {
        if (textView.text.length > 800) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"文本内容不能超过800字" message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles: nil];
            [alert show];
            textView.text = [textView.text substringToIndex:800];

        }
    }

    if (textView.text.length > 3000)
    {
        textView.text = [textView.text substringToIndex:3000];
    }
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.chatInputTextField resignFirstResponder];
    [self.chatInputTextView resignFirstResponder];
}


- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==self.chatInputTextField) {
        
        if (textField.text.length > 15)
        {
            textField.text = [textField.text substringToIndex:15];
        }
    }
    
}


//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    
//    if (range.location > 15) {
//        [[[UIAlertView alloc]initWithTitle:@"提示" message:@"自戏名最长不超过15个字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
//        [self.view endEditing:YES];
//        NSString * text =    self.chatInputTextField.text;
//        self.chatInputTextField.text = [self.chatInputTextField.text substringToIndex:15];
//        return NO;
//    }else{
//        return YES;
//    }
//    
//}

-(void) paizhao:(UITapGestureRecognizer*) recognizer
{
    
    BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    //如果支持相机，那么多个选项
    if (isCameraSupport) {
        UIActionSheet* mySheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"挑照片" otherButtonTitles:@"现在拍", nil];
        [mySheet showInView:self.view];
        
    }
    else {
        [self startPhotoChooser];
    }
    
}


- (void)uzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    
    
    if([[assets[0] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
    {
        
        [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            ALAsset *representation = obj;
            
            UIImage * original = [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                                     scale:representation.defaultRepresentation.scale
                                               orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
            //            original = [HZSInstances scaleImage:original toScale:0.4];
            aimageViewPhoto.image=original;
            
            
            CGSize  imgSize = CGSizeMake(2210, 1280);
            
            original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
            
            [self getServerKey :original];
            
        }];
        
    }
    else //Video
    {
        
        
    }
    
}



#pragma mark 从又拍获取图片字符串
-(void)getServerKey:(UIImage *)image
{
    //    [SVProgressHUD showWithStatus:@"上传中" maskType:SVProgressHUDMaskTypeBlack];
    //此处需要换成又拍的云存储
    _upYun = [[UpYun alloc]init];
    
    //上传图片成功
    
    [_upYun uploadImage:image];
    [self.circleView setHidden:NO]  ;
    
    self.circleView.center = CGPointMake(aimageViewPhoto.center.x - 10, aimageViewPhoto.frame.size.height*0.5);
    
    [self.circleView setProgress:0.0 animated:YES];
    self.circleView.alpha = 1;
    [self.circleView commonInit];
    [self.view bringSubviewToFront:self.circleView];
    [aimageViewPhoto addSubview:self.circleView];
    [aimageViewPhoto bringSubviewToFront:self.circleView];
    
    WS(weakSelf);
    _upYun.successBlocker = ^(id data){
        if (!weakSelf) {
            return ;
        }
        NSString        *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
        imgUrl  =   [NSString stringWithFormat:@"%@%@", IMAGE_SERVER_BASE_URL, imgUrl];
        [weakSelf.circleView setHidden:YES];
        
        weakSelf.imgSerUrl = imgUrl;
        [SVProgressHUD dismiss];
    };
    _upYun.failBlocker = ^(NSError * error)
    {
        //上传又拍失败的操作
        [weakSelf.circleView setHidden:YES];
        
        [SVProgressHUD showErrorWithStatus:@"上传失败" duration:2.0];
    };
    _upYun.progressBlocker = ^(CGFloat percent,long long requestDidSendBytes)
    {
        [ weakSelf.circleView setProgress:percent animated:YES];
    };
}




#pragma mark actionSheet Delegate

- (void) startPhotoChooser
{
    UzysAssetsPickerController *picker = [[UzysAssetsPickerController alloc] init];
    picker.delegate = self;
    
    picker.maximumNumberOfSelectionPhoto = 1;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //choose photo
    if (buttonIndex == 0) {
        
        [self startPhotoChooser];
        
    } else if (buttonIndex == 1) {
        //take photo.
        //指定使用照相机模式,可以指定使用相册／照片库
        imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        //设置当拍照完或在相册选完照片后，是否跳到编辑模式进行图片剪裁。只有当showsCameraControls属性为true时才有效果
        imagepicker.allowsEditing = NO;
        //设置拍照时的下方的工具栏是否显示，如果需要自定义拍摄界面，则可把该工具栏隐藏
        imagepicker.showsCameraControls  = YES;
        //设置使用后置摄像头，可以使用前置摄像头
        imagepicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        //设置闪光灯模式
        /*
         typedef NS_ENUM(NSInteger, UIImagePickerControllerCameraFlashMode) {
         UIImagePickerControllerCameraFlashModeOff  = -1,
         UIImagePickerControllerCameraFlashModeAuto = 0,
         UIImagePickerControllerCameraFlashModeOn   = 1
         };
         */
        imagepicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        //设置相机支持的类型，拍照和录像
        imagepicker.mediaTypes = @[(NSString*)kUTTypeImage];
        //设置拍摄时屏幕的view的transform属性，可以实现旋转，缩放功能
        // imagepicker.cameraViewTransform = CGAffineTransformMakeRotation(M_PI*45/180);
        // imagepicker.cameraViewTransform = CGAffineTransformMakeScale(1.5, 1.5);
        //所有含有cameraXXX的属性都必须要sourceType是UIImagePickerControllerSourceTypeCamera时设置才有效果，否则会有异常
        //设置UIImagePickerController的代理
        imagepicker.delegate = self;
        [self presentViewController:imagepicker animated:YES completion:^{
            
        }];
        
    }
    
    
}

#pragma take photos image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
    NSString* type = [info objectForKey:UIImagePickerControllerMediaType];
    //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    if ([type isEqualToString:(NSString*)kUTTypeImage]&&picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
        //获取照片的原图
        UIImage* original = [info objectForKey:UIImagePickerControllerOriginalImage];
        aimageViewPhoto.image=original;
        CGSize  imgSize = CGSizeMake(2210, 1280);
        
        original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
        [self getServerKey :original];
        
        //如果是拍照的照片，则需要手动保存到本地，系统不会自动保存拍照成功后的照片
        UIImageWriteToSavedPhotosAlbum(original, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
    }else{
        
    }
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//取消照相机的回调
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//保存照片成功后的回调
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo{
    
}
//add for photo functions end


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
