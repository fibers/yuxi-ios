//
//  LoadImagesView.m
//  inface
//
//  Created by appleone on 15/10/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "LoadImagesView.h"
#import "LoadImageCollectionViewCell.h"
@interface LoadImagesView ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>

@property (strong,nonatomic)UICollectionView * loadImagesCollection;

@property (strong,nonatomic)UILabel * imagesCount;

@property (strong,nonatomic)UILabel * longPressRead;
@end

@implementation LoadImagesView
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (!self.loadImagesCollection) {
        UICollectionViewFlowLayout * layOut = [[UICollectionViewFlowLayout alloc]init];
        layOut.minimumInteritemSpacing = 0.0;
        layOut.minimumLineSpacing = 0.0;
        [layOut setMinimumInteritemSpacing:0];
        [layOut setMinimumLineSpacing:0];
        layOut.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _loadImagesCollection = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth , 136)collectionViewLayout:layOut];
        [self addSubview:_loadImagesCollection];
        [_loadImagesCollection registerNib:[UINib nibWithNibName:@"LoadImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LoadImageCollectionViewCell"];
    }
    _loadImagesCollection.scrollEnabled = YES;
    _loadImagesCollection.alwaysBounceHorizontal = YES;
    
    if (!_imagesCount) {
        _imagesCount = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth /2.0 - 15, 170, 30, 12)];
        
        _imagesCount.font = [UIFont systemFontOfSize:10.0];
        
        _imagesCount.textColor = [UIColor colorWithHexString:@"#949494"];
        
        _imagesCount.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_imagesCount];
    }
    
    if (_longPressRead) {
        _longPressRead = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth /2.0 - 50, 185, 100, 10)];
        
        _longPressRead.font = [UIFont systemFontOfSize:8.0];
        
        _longPressRead.textColor = [UIColor colorWithHexString:@"#949494"];
        
        _longPressRead.textAlignment = NSTextAlignmentCenter;
        
        _longPressRead.text = @"长按图片可以拖动排序";
        
        [self addSubview:_longPressRead];
    }

    return self;
    
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 9;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LoadImageCollectionViewCell * collectionCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LoadImageCollectionViewCell" forIndexPath:indexPath];
    return collectionCell;
    
    
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(90, 130);
}

@end
