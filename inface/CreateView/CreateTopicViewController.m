//
//  CreateTopicViewController.m
//  inface
//
//  Created by huangzengsong on 15/6/30.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "CreateTopicViewController.h"
#import "SelectImageView.h"
#import "LoadImagesView.h"
#import "LoadImageCollectionViewCell.h"
#import "EVCircularProgressView.h"
#import "LXReorderableCollectionViewFlowLayout.h"
#import "MJExtension.h"
#import "MBProgressHUD.h"

@interface CreateTopicViewController ()<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,AddTopicImagesDelegate,UIGestureRecognizerDelegate,LXReorderableCollectionViewDataSource,LXReorderableCollectionViewDelegateFlowLayout>
{
    LoadImagesView * loadImages;//上传图片页面
    
    SelectImageView * selectView;//点击选择图片页面
    
    UILabel  * titleLabel;
    
    __weak IBOutlet NSLayoutConstraint *bottomLabelHeight;
    
    UILabel  * bottomLabel;
    
    BOOL      hasImages;//是否有图片了
    
    BOOL      isKeyBoardShow;//键盘是否弹出
    
    __weak IBOutlet NSLayoutConstraint *imageBottomView;
    
    UICollectionView *  loadImagesCollection;
    
    UILabel         * imagesCount;
    
    UILabel         * longPressedLabel;
    
    UILabel * lineLabel;
    
    NSMutableDictionary * imagesDic;
    
    __weak IBOutlet UIButton *addImageButton;
    
    UIImageView  * countImg;//图片红点数
    
    UILabel  * countLabel;//图片张数
    
    NSMutableArray * _imageAssets;
    
    UILongPressGestureRecognizer * _longPressGestureRecognizer;
    
    UIPanGestureRecognizer *  _panGestureRecognizer;
}

@property(strong,nonatomic)  NSMutableArray * imagesUrlArr;//存储又拍图片链接

@property(strong,nonatomic)  NSMutableArray * imagesArr;//临时存放图片数组
@property (strong, nonatomic) NSIndexPath *selectedItemIndexPath;
@property (strong, nonatomic) UIView *currentView;
@property (assign, nonatomic) CGPoint currentViewCenter;
@property (assign, nonatomic) CGPoint panTranslationInCollectionView;
@property (strong, nonatomic) CADisplayLink *displayLink;
@property(nonatomic,strong)UILabel * placeLable;
@property(nonatomic,copy)MBProgressHUD * hud;

@end

@implementation CreateTopicViewController


- (IBAction)loadImages:(id)sender
{
    if (_imagesArr.count == 0) {
        [self.chatInputTextView resignFirstResponder];
        [self.chatInputTextField resignFirstResponder];
        [self paizhao];

    }else{
        [self.chatInputTextView resignFirstResponder];
        [self.chatInputTextField resignFirstResponder];
        [UIView animateWithDuration:0.3 animations:^{
                //如果此时有图片，则拍照按钮和线不下降到底
                imageBottomView.constant = 210;
                
                bottomLabelHeight.constant = 251;
   
             lineLabel.frame = CGRectMake(0, ScreenHeight-260, ScreenWidth, 1);
            
            loadImagesCollection.frame = CGRectMake(20, ScreenHeight-250, ScreenWidth - 40, 145);
            imagesCount.frame = CGRectMake(ScreenWidth /2.0 - 15, ScreenHeight-100, 30, 12);
            longPressedLabel.frame = CGRectMake(ScreenWidth /2.0 - 50, ScreenHeight - 80, 100, 10);

        }];
    }

}

-(MBProgressHUD *)hud{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc]initWithView:self.view];
    }
    return _hud;
}

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    RightButtonCreate.userInteractionEnabled=NO;
    if ([self.chatInputTextField.text isEmpty]) {
        RightButtonCreate.userInteractionEnabled=YES;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入名称" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([self.chatInputTextView.text isEmpty]) {
        RightButtonCreate.userInteractionEnabled=YES;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入简介" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }

//    
//    if (self.chatInputTextField.text.length > 30)
//    {
//        self.chatInputTextField.text = [self.chatInputTextField.text substringToIndex:30];
//    }
    
//    NSString * user_userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];
//    __block NSString *groupID;
    //循环上传选中的照片到又拍云
    if (_imagesArr.count > 0 && _imagesArr.count > _imagesUrlArr.count) {
        [SVProgressHUD showWithStatus:@"上传中..." maskType:SVProgressHUDMaskTypeBlack];

        [_imagesArr enumerateObjectsUsingBlock:^(UIImage * image, NSUInteger idx, BOOL *stop) {
            [self getServerKey:image];
            
        }];
    }else{
        [self publishTopic];
    }

       RightButtonCreate.userInteractionEnabled = NO;
    //创建群

}

#pragma mark 此时上传话题的详情
-(void)publishTopic
{
    NSMutableString * jsonStr = [NSMutableString string];
    [self.chatInputTextField resignFirstResponder];
    
    [self.chatInputTextView resignFirstResponder];
    NSDictionary * params;
    [jsonStr appendString:@"{\"images\":["];
    if (_imagesUrlArr.count > 0) {
        [self.imagesUrlArr enumerateObjectsUsingBlock:^(NSString * imgUrl, NSUInteger idx, BOOL *stop) {
            [jsonStr appendString:[NSString stringWithFormat:@"\"%@\",",imgUrl]];
            
        }];
        if (jsonStr.length > 0) {
            
            [jsonStr replaceCharactersInRange:NSMakeRange(jsonStr.length -1, 1) withString:@"]"];
            [jsonStr appendString:@"}"];
        }
        params = @{@"uid":USERID,@"title":self.chatInputTextField.text,@"images":jsonStr,@"content":self.chatInputTextView.text,@"token":@"110"};

        
    }else{
        NSString * jsonStr = @"{\"images\":[]}";
        params = @{@"uid":USERID,@"title":self.chatInputTextField.text,@"images":jsonStr,@"content":self.chatInputTextView.text,@"token":@"110"};

    }
    [self.hud setLabelText:@"数据请求中..."];
    [self.hud setMode:MBProgressHUDModeIndeterminate];
    [self.view addSubview:self.hud];
    [self.hud show:YES];
    [self.view bringSubviewToFront:self.hud];

    [HttpTool postWithPath:@"CreateNewTopic" params:params success:^(id JSON) {
        [self.hud setHidden:YES];
        if ([[JSON objectForKey:@"result"]integerValue]>=1) {
            [self.hud setLabelText:@"创建成功"];
            [self.hud setMode:MBProgressHUDModeText];
            [self.hud hide:YES afterDelay:1];
            RightButtonCreate.userInteractionEnabled=YES;

            [[NSNotificationCenter defaultCenter]postNotificationName:@"kYXSquareViewControllerRefreshDataNotification" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
            

        }else{
            [self.hud setLabelText:[JSON objectForKey:@"describe"]];
            [self.hud setMode:MBProgressHUDModeText];
            [self.hud hide:YES afterDelay:2];
            RightButtonCreate.userInteractionEnabled=YES;

            RightButtonCreate.userInteractionEnabled=YES;
          
        }

      
     } failure:^(NSError *error) {
         [self.hud setLabelText:@"创建失败"];
         [self.hud setMode:MBProgressHUDModeText];
         [self.hud hide:YES afterDelay:2];
         RightButtonCreate.userInteractionEnabled=YES;
 
        
    }];
    
}





- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor=[UIColor whiteColor];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];

    [[IQKeyboardManager sharedManager]setEnable:NO];
    //左上角按钮
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 45, 44)];
    [LeftButton setTitle:@"取消" forState:UIControlStateNormal];
    LeftButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [LeftButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    RightButtonCreate=RightButton;
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"发起话题";
    
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInputTextView.textColor=[UIColor blackColor];
    if (!_placeLable) {
        _placeLable = [[UILabel alloc]initWithFrame:CGRectMake(3, 8, 100, 20)];
        
    }
    _placeLable.font = [UIFont systemFontOfSize:14];
    
    _placeLable.textColor=COLOR(204, 204, 204, 1);
    
    _placeLable.text = @"请输入内容";

    [self.chatInputTextView addSubview:_placeLable];
    self.chatInputTextView.scrollEnabled = YES;
    self.chatInputTextField.delegate = self;
    self.chatInputTextField.textColor=GRAY_COLOR;
    self.XianLabel.backgroundColor=XIAN_COLOR;
    
    imagepicker = [[UIImagePickerController alloc]init];
 
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [self.view addGestureRecognizer:recognizer];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardAppearance:) name:UIKeyboardWillShowNotification  object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardDismissMode:) name:UIKeyboardWillHideNotification object:nil];
    
    _imagesArr = [NSMutableArray arrayWithCapacity:10];
    
    _imagesUrlArr = [NSMutableArray arrayWithCapacity:9];
    
    [[UzysAssetsPickerController shareInstance]deleteAssets];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.chatInputTextField resignFirstResponder];
    [self.chatInputTextView resignFirstResponder];
}



#pragma mark 增减下滑手势
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gesture
{
    if (isKeyBoardShow) {
        [self.chatInputTextView resignFirstResponder];
        [self.chatInputTextField resignFirstResponder];
    }else{
        //此时若有图片，则图片下降，
        
        lineLabel.frame = CGRectMake(0, ScreenHeight, ScreenWidth, 1);
        
        loadImagesCollection.frame = CGRectMake(20, ScreenHeight, ScreenWidth-40, 145);
        
        imagesCount.frame = CGRectMake(ScreenWidth /2.0 - 15, ScreenHeight+30, 30, 12);
        
        longPressedLabel.frame = CGRectMake(ScreenWidth /2.0 - 50, ScreenHeight +40, 100, 10);
        [UIView animateWithDuration:0.3 animations:^{
            if (loadImagesCollection) {
                //如果此时有图片，则拍照按钮和线不下降到底

                imageBottomView.constant = 9;
                
                bottomLabelHeight.constant = 51;

                
            }else{
                
            }
            
        }];
    }

}


/*由于联想输入的时候，函数textView:shouldChangeTextInRange:replacementText:无法判断字数，
 因此使用textViewDidChange对TextView里面的字数进行判断
 */
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length > 0) {
        _placeLable.hidden = YES;
    }else{
        _placeLable.hidden = NO;
        
    }
    //该判断用于联想输入
    if (textView.text.length > 3000)
    {
        textView.text = [textView.text substringToIndex:3000];
        
    }
}

// 
//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//
//  if (range.location > 15) {
//    [[[UIAlertView alloc]initWithTitle:@"提示" message:@"话题名最长不超过15个字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
//    [self.view endEditing:YES];
//    self.chatInputTextField.text = [self.chatInputTextField.text substringToIndex:15];
//    return NO;
//   }else{
//    return YES;
//   }
//}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField==self.chatInputTextField) {
        
        if (textField.text.length > 15)
        {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"标题内容不能超过15个字" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [alert show];
            textField.text = [textField.text substringToIndex:15];
        }
    }
    
}

-(void) paizhao
{
    
    BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    //如果支持相机，那么多个选项
    if (isCameraSupport) {
        UIActionSheet* mySheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"挑照片" otherButtonTitles:@"现在拍", nil];
        [mySheet showInView:self.view];
        
    }
    else {
        
            [self startPhotoChooser];

        }
    
}

-(void)keyboardAppearance:(NSNotification *)notification
{
    NSDictionary * userInfo = [notification userInfo];
    
    NSValue * value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyBoardRect = [value CGRectValue];
    
    float height = keyBoardRect.size.height;
    
    if (height == 0) {
        return;
    }
    isKeyBoardShow = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        bottomLabelHeight.constant = height+50;
        imageBottomView.constant = height;
        
    }];
}


-(void)keyboardDismissMode:(NSNotification *)notification
{
    isKeyBoardShow = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        //如果图片是弹出状态，则拍照按钮不下降到底部
        if (_imagesArr.count > 0 && loadImagesCollection.frame.origin.y < ScreenHeight) {
            //如果此时有图片，则拍照按钮和线不下降到底
            imageBottomView.constant = 210;
            
            bottomLabelHeight.constant = 251;
        }else{
            
            bottomLabelHeight.constant = 51;
            imageBottomView.constant = 9;
        }

        
    }];
}

-(void)recordAssets:(NSMutableArray *)imageAssets
{
    _imageAssets = [NSMutableArray array];
    
    _imageAssets = [imageAssets mutableCopy];
    
}

- (void)uzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    
    if([[assets[0] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
    {
        
        [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            ALAsset *representation = obj;
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                
                
          
            UIImage * original = [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                                     scale:representation.defaultRepresentation.scale
                                               orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
            
            CGSize  imgSize = CGSizeMake(2210, 1280);
            
            original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
//            aimageViewPhoto.image=original;
            [_imagesArr addObject:original];
            //创建上传照片的视图
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self createCollectionView];

                });
//            [self getServerKey :original index:idx];
            //创建上传图片的视图
        });
        }];
        
    }
    else //Video
    {
        
        
    }
    
}

#pragma mark AddImageDelegate
-(void)addMoreImages
{
    [self paizhao];
}

#pragma mark DeleteImageDelegate
-(void)deleteImage:(NSInteger)index
{
    [_imagesArr removeObjectAtIndex:index];
    
    imagesCount.text = [NSString stringWithFormat:@"%ld / 9",_imagesArr.count];
    
    [loadImagesCollection reloadData];
    if (_imagesArr.count == 0) {
        countLabel.hidden= YES;
        countImg.hidden = YES;
        return;
    }
    [countLabel setText:[NSString stringWithFormat:@"%ld",_imagesArr.count]];
}


#pragma mark 初始化collectionView展示照片
-(void)createCollectionView
{
    imageBottomView.constant = 210;
    
    bottomLabelHeight.constant = 251;

    //在拍照按钮右上角加角标
    if (!countImg) {
        countImg = [[UIImageView alloc]initWithFrame:CGRectMake(20, -2, 15, 15)];
       
        countImg.image =  [UIImage imageNamed:@"imageCount"];
        
        [addImageButton addSubview:countImg];
    }
    countImg.hidden = NO;
    
    countLabel.hidden = NO;
    
    if (!countLabel) {
        countLabel = [[UILabel alloc]initWithFrame:CGRectMake(24, 0, 10, 10)];
        
        countLabel.textColor = [UIColor whiteColor];
        
        [countLabel setFont:[UIFont systemFontOfSize:10]];
        
        
        [addImageButton addSubview:countLabel];
    }
    
    [countLabel setText:[NSString stringWithFormat:@"%ld",_imagesArr.count]];

    [addImageButton bringSubviewToFront:countLabel];

    if (!lineLabel) {
        lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, ScreenHeight - 260, ScreenWidth, 1)];
        
        lineLabel.backgroundColor = [UIColor colorWithHexString:@"#c5c5c6"];
        
        [self.view addSubview:lineLabel];
    }
    lineLabel.frame = CGRectMake(0, ScreenHeight-260, ScreenWidth, 1);
    
    if (!loadImagesCollection) {
        
        LXReorderableCollectionViewFlowLayout * layOut = [[LXReorderableCollectionViewFlowLayout alloc]init];
        
        [layOut setMinimumInteritemSpacing:13.0];
        [layOut setMinimumLineSpacing:13.0];
        layOut.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        loadImagesCollection = [[UICollectionView alloc]initWithFrame:CGRectMake(20, ScreenHeight - 250,ScreenWidth -40, 145)collectionViewLayout:layOut];
        [self.view addSubview:loadImagesCollection];
        
        loadImagesCollection.backgroundColor = [UIColor clearColor];
        loadImagesCollection.delegate = self;
        loadImagesCollection.dataSource = self;
        
        [loadImagesCollection registerNib:[UINib nibWithNibName:@"LoadImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LoadImageCollectionViewCell"];
    }
    
//    [loadImagesCollection addGestureRecognizer:_longPressGestureRecognizer];
    
    loadImagesCollection.frame = CGRectMake(20, ScreenHeight-250, ScreenWidth - 40, 145);
    
    [loadImagesCollection reloadData];
    
    loadImagesCollection.scrollEnabled = YES;
    loadImagesCollection.alwaysBounceHorizontal = YES;
    
    CGFloat  collectionViewWidth = ScreenWidth * (_imagesArr.count / 3 + _imagesArr.count %3) + 20;
    loadImagesCollection.contentSize = CGSizeMake(collectionViewWidth, 145);
    if (_imagesArr.count < 9 ) {
           [ loadImagesCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_imagesArr.count inSection:0] atScrollPosition: UICollectionViewScrollPositionRight animated:YES];
    }else{
            [ loadImagesCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_imagesArr.count -1  inSection:0] atScrollPosition: UICollectionViewScrollPositionRight animated:YES];
    }

    if (!imagesCount) {
        imagesCount = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth /2.0 - 15, ScreenHeight-100, 30, 12)];
        
        imagesCount.font = [UIFont systemFontOfSize:10.0];
        
        imagesCount.textColor = [UIColor colorWithHexString:@"#949494"];
        
        imagesCount.textAlignment = NSTextAlignmentCenter;
        
        [self.view addSubview:imagesCount];
    }
    imagesCount.frame = CGRectMake(ScreenWidth /2.0 - 15, ScreenHeight-100, 30, 12);
    imagesCount.text = [NSString stringWithFormat:@"%ld / 9",_imagesArr.count];
    
    if (!longPressedLabel) {
        longPressedLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth /2.0 - 50, ScreenHeight - 80, 100, 10)];
        
        longPressedLabel.font = [UIFont systemFontOfSize:8.0];
        
        longPressedLabel.textColor = [UIColor colorWithHexString:@"#949494"];
        
        longPressedLabel.textAlignment = NSTextAlignmentCenter;
        
        longPressedLabel.text = @"长按图片可以拖动排序";
        
        [self.view addSubview:longPressedLabel];
    }
    longPressedLabel.frame = CGRectMake(ScreenWidth /2.0 - 50, ScreenHeight - 80, 100, 10);
}



#pragma makr longPressDelegate
- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath willMoveToIndexPath:(NSIndexPath *)toIndexPath
{
    UIImage * image = _imagesArr[fromIndexPath.item];
    
    [_imagesArr removeObjectAtIndex:fromIndexPath.item];
    
    [_imagesArr insertObject:image atIndex:toIndexPath.item];
    
}


- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_imagesArr.count < 9 && indexPath.item == _imagesArr.count) {
        return NO;
    }else{
        return YES;
    }
}
- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath
{
    if (_imagesArr.count < 9 && toIndexPath.item == _imagesArr.count) {
        return NO;
    }else{
        return YES;
    }
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (_imagesArr.count < 9) {
        return _imagesArr.count + 1;
    }else{
        return 9;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LoadImageCollectionViewCell * collectionCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LoadImageCollectionViewCell" forIndexPath:indexPath];
    collectionCell.delegate = self;
    if (_imagesArr.count < 9) {
        if (indexPath.row < _imagesArr.count) {
            [collectionCell setImage:_imagesArr[indexPath.row] index:indexPath.row];
        }else{
            [collectionCell setAddImage];
        }
    }else{
        [collectionCell setImage:_imagesArr[indexPath.row] index:indexPath.row];
    }
    return collectionCell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(95, 145);
}



#pragma mark 从又拍获取图片字符串
-(void)getServerKey:(UIImage *)image
{
    //    [SVProgressHUD showWithStatus:@"上传中" maskType:SVProgressHUDMaskTypeBlack];
    //此处需要换成又拍的云存储
    _upYun = [[UpYun alloc]init];
    
    //上传图片成功
    
    [_upYun uploadImage:image];

    WEAKSELF
    _upYun.successBlocker = ^(id data){
        if (!weakSelf) {
            return ;
        }
        NSString        *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
        imgUrl  =   [NSString stringWithFormat:@"%@%@", IMAGE_SERVER_BASE_URL, imgUrl];
        
        [weakSelf.imagesUrlArr addObject:imgUrl];
        
        if (weakSelf.imagesUrlArr.count == weakSelf.imagesArr.count) {
            //此时ds
            [weakSelf publishTopic];
        }
    };
    
    _upYun.failBlocker = ^(NSError * error)
    {
        //上传又拍失败的操作
        
        [SVProgressHUD showErrorWithStatus:@"上传失败" duration:2.0];
    };
    
    _upYun.progressBlocker = ^(CGFloat percent,long long requestDidSendBytes)
    {
    };
}




#pragma mark actionSheet Delegate

- (void) startPhotoChooser
{
    isKeyBoardShow = NO;
    UzysAssetsPickerController *picker = [[UzysAssetsPickerController alloc]init];
    
    picker.delegate = self;
    
    picker.maximumNumberOfSelectionPhoto = 9 - _imagesArr.count;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
    //存储之前的图片


}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //choose photo
    if (buttonIndex == 0) {
        
        [self startPhotoChooser];
        
    } else if (buttonIndex == 1) {
        //take photo.
        //指定使用照相机模式,可以指定使用相册／照片库
        imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        //设置当拍照完或在相册选完照片后，是否跳到编辑模式进行图片剪裁。只有当showsCameraControls属性为true时才有效果
        imagepicker.allowsEditing = NO;
        //设置拍照时的下方的工具栏是否显示，如果需要自定义拍摄界面，则可把该工具栏隐藏
        imagepicker.showsCameraControls  = YES;
        //设置使用后置摄像头，可以使用前置摄像头
        imagepicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        //设置闪光灯模式
        /*
         typedef NS_ENUM(NSInteger, UIImagePickerControllerCameraFlashMode) {
         UIImagePickerControllerCameraFlashModeOff  = -1,
         UIImagePickerControllerCameraFlashModeAuto = 0,
         UIImagePickerControllerCameraFlashModeOn   = 1
         };
         */
        imagepicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        //设置相机支持的类型，拍照和录像
        imagepicker.mediaTypes = @[(NSString*)kUTTypeImage];
        //设置拍摄时屏幕的view的transform属性，可以实现旋转，缩放功能
        // imagepicker.cameraViewTransform = CGAffineTransformMakeRotation(M_PI*45/180);
        // imagepicker.cameraViewTransform = CGAffineTransformMakeScale(1.5, 1.5);
        //所有含有cameraXXX的属性都必须要sourceType是UIImagePickerControllerSourceTypeCamera时设置才有效果，否则会有异常
        //设置UIImagePickerController的代理
        imagepicker.delegate = self;
        [self presentViewController:imagepicker animated:YES completion:^{
            
        }];
        
    }
    
    
}

#pragma take photos image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
    NSString* type = [info objectForKey:UIImagePickerControllerMediaType];
    //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    if ([type isEqualToString:(NSString*)kUTTypeImage]&&picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
        //获取照片的原图
        UIImage* original = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        CGSize  imgSize = CGSizeMake(2210, 1280);
        
        original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
        //            aimageViewPhoto.image=original;
        [_imagesArr addObject:original];
   
        [self createCollectionView];
        //如果是拍照的照片，则需要手动保存到本地，系统不会自动保存拍照成功后的照片
        UIImageWriteToSavedPhotosAlbum(original, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
    }else{
        
    }
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//取消照相机的回调
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//保存照片成功后的回调
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo{
    
}
//add for photo functions end

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
