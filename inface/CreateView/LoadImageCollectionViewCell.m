//
//  LoadImageCollectionViewCell.m
//  inface
//
//  Created by appleone on 15/10/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "LoadImageCollectionViewCell.h"
@interface LoadImageCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UIButton * deleteImgBtn;

@end

@implementation LoadImageCollectionViewCell

- (IBAction)deleteImage:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(deleteImage:)]) {
        [self.delegate deleteImage:sender.tag];
    }
}

-(void)setImage:(UIImage *)image index:(NSInteger)tag;
{
    
    _imageView.image = image;
    
    _deleteImgBtn.tag = tag;
    
    _deleteImgBtn.hidden = NO;
    
    _imageView.userInteractionEnabled = NO;
}

-(void)setAddImage
{
    _imageView.image = [UIImage imageNamed:@"topAddImg"];
    
    _deleteImgBtn.hidden = YES;
    
    UITapGestureRecognizer * singleAdd = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addImages:)];
    
    [_imageView addGestureRecognizer:singleAdd];
    
    _imageView.userInteractionEnabled = YES;
    
}

-(void)addImages:(UITapGestureRecognizer *)gesture
{
    if ([self.delegate respondsToSelector:@selector(addMoreImages)]) {
        [self.delegate addMoreImages];
    }
    
}

- (UIView *)LX_snapshotView
{
    if ([self respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)]) {
        return [self snapshotViewAfterScreenUpdates:YES];
    } else {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0f);
        [self.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return [[UIImageView alloc] initWithImage:image];
    }
}
- (void)awakeFromNib {
    // Initialization code
}

@end
