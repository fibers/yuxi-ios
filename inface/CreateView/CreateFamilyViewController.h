//
//  CreateFamilyViewController.h
//  inface
//
//  Created by Mac on 15/5/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "UzysAssetsPickerController.h"
#import "SessionEntity.h"
#import "EVCircularProgressView.h"
@interface CreateFamilyViewController : BaseViewController<UITextViewDelegate,UITextFieldDelegate,UzysAssetsPickerControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIImagePickerController    *imagepicker;
    UIImageView *aimageViewPhoto;
    UIButton *RightButtonCreate;
}

@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property (weak, nonatomic) IBOutlet UITextField *CodeTextField;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (strong,nonatomic)  NSString * imgSerUrl;
@property (strong,nonatomic)  EVCircularProgressView * circleView;
@property (weak, nonatomic) IBOutlet UILabel *XianLabel;
@property (weak, nonatomic) IBOutlet UILabel *XianLabel1;
@property (weak, nonatomic) IBOutlet UILabel *PromptLabel;
@property (assign,nonatomic) BOOL             isAnimating;
@end
