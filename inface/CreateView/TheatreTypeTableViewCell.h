//
//  TheatreTypeTableViewCell.h
//  inface
//
//  Created by Mac on 15/5/19.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TheatreTypeTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UIButton *SelectButton;//选择按钮
@end
