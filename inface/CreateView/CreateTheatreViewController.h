//
//  CreateTheatreViewController.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "UzysAssetsPickerController.h"
#import "UpYun.h"
#import "DDCreateGroupAPI.h"
#import "GroupEntity.h"
#import "EVCircularProgressView.h"
@interface CreateTheatreViewController : BaseViewController<UITextViewDelegate,UITextFieldDelegate,UzysAssetsPickerControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIImagePickerController    *imagepicker;
    UIImageView *aimageViewPhoto;
    UpYun * _upYun;
    UIButton *RightButtonCreate;
}

@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (strong,nonatomic)  NSString * imgSerUrl;//图片又拍地址
@property (strong,nonatomic)  NSString * TheatreTypeId;
@property (strong,nonatomic)  EVCircularProgressView * circleView;
@property (weak, nonatomic) IBOutlet UILabel *XianLabel;
@property (weak, nonatomic) IBOutlet UILabel *PromptLabel;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
