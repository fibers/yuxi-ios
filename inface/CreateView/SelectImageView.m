//
//  SelectImageView.m
//  inface
//
//  Created by appleone on 15/10/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SelectImageView.h"
@interface SelectImageView()
{
    UILabel  * titleLabel;
    
    UILabel  * bottomLabel;
    
    UIButton * takePhotoBtn;
}
@end
@implementation SelectImageView

-(void)awakeFromNib
{
    titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    titleLabel.backgroundColor = [UIColor colorWithHexString:@"#c5c5c6"];
    [self addSubview:titleLabel];
    
    takePhotoBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
    
    takePhotoBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 20, 40, 34)];
    
    [takePhotoBtn setImage:[UIImage imageNamed:@"topicLoadImage"] forState:UIControlStateNormal];
    
    [takePhotoBtn addTarget:self action:@selector(selectImages) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:takePhotoBtn];
    
    bottomLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 50.5, ScreenWidth, 1)];
    bottomLabel.backgroundColor = [UIColor colorWithHexString:@"#c5c5c6"];
    [self addSubview:bottomLabel];
    

    

}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    titleLabel.backgroundColor = [UIColor colorWithHexString:@"#c5c5c6"];
    [self addSubview:titleLabel];

    takePhotoBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
    
    takePhotoBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 20, 40, 34)];
    
    [takePhotoBtn setImage:[UIImage imageNamed:@"topicLoadImage"] forState:UIControlStateNormal];
    
    [takePhotoBtn addTarget:self action:@selector(selectImages) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:takePhotoBtn];
    
    bottomLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 50.5, ScreenWidth, 1)];
    bottomLabel.backgroundColor = [UIColor colorWithHexString:@"#c5c5c6"];
    [self addSubview:bottomLabel];
    return self;
    
}

-(void)selectImages
{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
