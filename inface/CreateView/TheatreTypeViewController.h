//
//  TheatreTypeViewController.h
//  inface
//
//  Created by Mac on 15/5/19.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

typedef enum{
    RightButtonChoose = 0,
    RightButtonFinish = 1,
}RightButtontype;

#import "BaseViewController.h"
#import "TheatreTypeTableViewCell.h"
#import "CreateTheatreViewController.h"
@interface TheatreTypeViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableDataFriends;
@property (nonatomic,assign) RightButtontype buttonType;
@property (nonatomic,strong) NSDictionary *dataDic;
@property (nonatomic,assign) BOOL          isAnimating;
@end
