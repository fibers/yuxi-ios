//
//  PlayOfCreate.h
//  inface
//
//  Created by lizhen on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlayOfCreate : NSObject

///又拍云图片URL
@property (copy, nonatomic    ) NSString * upYunOfImageUrl;

///剧的名字
@property (copy, nonatomic    ) NSString * nameOfPlay;

///选择类别的数组 <StoryCategory>
@property (strong, nonatomic  ) NSArray  * typeOfSelectArray;

///剧简介
@property (copy, nonatomic    ) NSString * summaryOfPlay;

///主线剧情
@property (copy, nonatomic    ) NSString * mainStoryLine;

///角色类型数组 <RolesTypeItem>
@property (strong, nonatomic  ) NSArray  * rolesTypeSettingArray;

///规则设定
@property (copy, nonatomic    ) NSString * setRules;
///角色设定-审核规则
@property (copy,nonatomic     ) NSString * verifiyRules;
///角色设定-人物设定 <StoryCategory>
@property (strong,nonatomic   ) NSArray  * roleSettingArr;

///分类id组成的数组
@property (strong, nonatomic)NSArray *IDArray;

///一个map组成的数组
//@property (strong, nonatomic)NSArray *mapArray;

///一个属性id组成的数组
//@property (strong, nonatomic)NSArray *setplayIDArray;



@end
