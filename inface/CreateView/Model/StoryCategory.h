//
//  StoryCategory.h
//  inface
//
//  Created by 邢程 on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CustomMenuModel;
@interface StoryCategory : NSObject

@property (nonatomic, assign) NSInteger result;

@property (nonatomic, strong) NSMutableArray<CustomMenuModel *> *list;

@end

