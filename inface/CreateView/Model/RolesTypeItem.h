//
//  RolesTypeItem.h
//  inface
//
//  Created by 邢程 on 15/11/2.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RolesTypeItem : NSObject
///角色名称
@property (copy,nonatomic  ) NSString  * roleName;
///角色数量
@property (assign,nonatomic) NSInteger roleNum;
///身份背景
@property (copy,nonatomic  ) NSString  * roleDesc;

@property (assign,nonatomic)BOOL isMoreThanWordCount;
@end
