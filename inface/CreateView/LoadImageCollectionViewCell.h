//
//  LoadImageCollectionViewCell.h
//  inface
//
//  Created by appleone on 15/10/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
@protocol AddTopicImagesDelegate <NSObject>

-(void)addMoreImages;

-(void)deleteImage:(NSInteger)index;

@end

#import <UIKit/UIKit.h>

@interface LoadImageCollectionViewCell : UICollectionViewCell
@property(unsafe_unretained,nonatomic)id<AddTopicImagesDelegate>delegate;
-(void)setImage:(UIImage *)image index:(NSInteger )tag;

-(void)setAddImage;

- (UIView *)LX_snapshotView ;

@end
