//
//  YXPlayIntroductionViewController.h
//  inface
//
//  Created by lizhen on 15/10/30.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "PlayOfCreate.h"

@interface YXPlayIntroductionViewController : BaseViewController<UITextViewDelegate,UIScrollViewDelegate>

-(void)YXPlayintroductionPlayOfCreateModel:(PlayOfCreate *)playOfModel;
@end
