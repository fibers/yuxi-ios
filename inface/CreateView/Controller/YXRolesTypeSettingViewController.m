//
//  YXRolesTypeSettingViewController.m
//  inface
//
//  Created by 邢程 on 15/10/29.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXRolesTypeSettingViewController.h"
#import "YXCreateViewCellFooterViews.h"
#import "YXCreateViewRoleTypeTableViewCell.h"
#import "RolesTypeItem.h"
#import "YXRolesSettingViewController.h"
#import "YXSetRulesViewController.h"
#import "YXProgressView.h"
#import "YXCreateViewChoseTypeViewController.h"
@interface YXRolesTypeSettingViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate, YXCreateViewRoleTypeTableViewCellDelegate>
@property(nonatomic,strong)UITableView * tableView;
@property(nonatomic,strong)NSMutableArray *roleTypeItemArrM;
@property (strong, nonatomic)PlayOfCreate *model;
@property (strong,nonatomic)UIButton *rightButton;
@end

@implementation YXRolesTypeSettingViewController

-(void)YXPlayintroductionPlayOfCreateModel:(PlayOfCreate *)playOfModel{
    
    _model = [[PlayOfCreate alloc]init];
    _model = playOfModel;
}
-(void)setNavBar{
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    LeftButton.tag = 10;
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _rightButton.frame = CGRectMake(0, 0, 48, 24);
    //rightButton.backgroundColor = [UIColor colorWithHexString:@"#7fb0e9"];
    [_rightButton setTitle:@"下一步" forState:normal];
    _rightButton.titleLabel.font = [UIFont systemFontOfSize:12.0];
    _rightButton.tag = 11;
    [_rightButton setTitleColor:[UIColor colorWithHexString:@"#9b9b9b"] forState:normal];
    _rightButton.layer.cornerRadius = 4.0;
    _rightButton.layer.borderWidth = 1.0;
    [_rightButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
    _rightButton.layer.borderColor = [[UIColor colorWithHexString:@"#9b9b9b"]CGColor];
    [_rightButton addTarget:self action:@selector(nextButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_rightButton];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
}
-(void)returnBackl:(UIButton*)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (instancetype)init
{
    if (self = [super init]) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        [[IQKeyboardManager sharedManager]setEnable:YES];
        [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];
        [[IQKeyboardManager sharedManager]setKeyboardDistanceFromTextField:0];
        [[IQKeyboardManager sharedManager]setCanAdjustTextView:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(styleOfPlayerChange:) name:@"styleOfPlayerChange" object:nil];
        [self.navigationItem setTitle:@"角色类型"];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        [self.tableView registerNib:[UINib nibWithNibName:@"YXCreateViewRoleTypeTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"YXCreateViewRoleTypeTableViewCell"];
        self.hidesBottomBarWhenPushed = YES;
        [self setNavBar];
    }
    return self;
}
-(void)nextButtonClicked{
    [self.view endEditing:YES];
    if([self.roleTypeItemArrM count] == 0){
        [[[UIAlertView alloc]initWithTitle:@"提示" message:@"至少添加一个" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
        return;
    }
    NSMutableArray *arrTMP = [NSMutableArray array];
    __block BOOL isCanPushViewController = YES;
    [self.roleTypeItemArrM enumerateObjectsUsingBlock:^(RolesTypeItem * item, NSUInteger idx, BOOL * _Nonnull stop) {
        if((item.roleName == nil || [item.roleName isEqualToString:@""])){
            *stop = YES;
            isCanPushViewController = NO;
            [self showAlertVeiw:@"角色不能为空"];
            return ;
        }
        if([arrTMP containsObject:item.roleName]){
            *stop = YES;
            isCanPushViewController = NO;
            [self showAlertVeiw:@"角色名不能重复"];
            return ;
        }
        if (item.isMoreThanWordCount) {
            *stop = YES;
            isCanPushViewController = NO;
            [self showAlertVeiw:@"身份背景最多500字"];
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        [arrTMP addObject:item.roleName];
        
    }];
    if (isCanPushViewController) {
        
        YXRolesSettingViewController * vc = [[YXRolesSettingViewController alloc]init];
        _model.rolesTypeSettingArray = _roleTypeItemArrM;
        [vc YXPlayintroductionPlayOfCreateModel:_model];
        [self.navigationController pushViewController:vc animated:YES];
    }
   
}
-(void)showAlertVeiw:(NSString*)str{
    [[[UIAlertView alloc]initWithTitle:@"提示" message:str delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
}
-(void)yxCreateViewRoleTypeTableViewCellCountZero:(RolesTypeItem*)item{

    [self.roleTypeItemArrM removeObject:item];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
}
-(NSMutableArray *)roleTypeItemArrM{
    if (!_roleTypeItemArrM) {
        _roleTypeItemArrM = [NSMutableArray array];
        for(int i = 0;i<1;i++){
            RolesTypeItem * typeItem = [[RolesTypeItem alloc]init];
            typeItem.roleNum = 1;
            [_roleTypeItemArrM addObject:typeItem];
        }
    }
    return _roleTypeItemArrM;
}
#pragma mark -
#pragma mark tableView dataSource&delegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
            return [self viewForSecitonHeader:@"角色类型"];
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
            return 56;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

            return [self viewForSectionFooter:@"设定剧中角色及数量，例：咖啡师x3；面包师x2"];

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return [self.roleTypeItemArrM count]+1;

    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
        return 195;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger arrCount = [self.roleTypeItemArrM count]-1;
    NSInteger row = indexPath.row;
            if(arrCount<row){
                return [self addBtnCell];
            }else{
                YXCreateViewRoleTypeTableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"YXCreateViewRoleTypeTableViewCell"];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                [cell setContentWithRoleTypeItemModel:[self.roleTypeItemArrM objectAtIndex:indexPath.row]];
                cell.delegate = self;
                return cell;
            }
 
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}
///添加按钮被点击
-(void)addBtnClicked:(UIButton*)sender{
    RolesTypeItem * typeItem = [[RolesTypeItem alloc]init];
    typeItem.roleNum = 1;
    [self.roleTypeItemArrM addObject:typeItem];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
}
-(UITableViewCell *)addBtnCell{
    UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"addBtnCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setFrame:CGRectMake(0, 0, ScreenWidth, 44)];
    UIButton * addBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, 9, 60, 25)];
    [addBtn addTarget:self action:@selector(addBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [addBtn setTitleColor:[UIColor colorWithHexString:@"949494"] forState:UIControlStateNormal];
    [addBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    addBtn.layer.cornerRadius = 4;
    [addBtn setClipsToBounds:YES];
    addBtn.layer.borderWidth = 0.5;
    addBtn.layer.borderColor = [UIColor colorWithHexString:@"4a4a4a"].CGColor;
    [cell addSubview:addBtn];
    [cell setBackgroundColor:[UIColor whiteColor]];
    [addBtn setTitle:@"添加" forState:UIControlStateNormal];
    return cell;
}
/**
 *  生成一个FooterView
 *
 *  @param infoStr 描述文字
 *
 *  @return FooterView
 */
-(UIView *)viewForSectionFooter:(NSString*)infoStr{
    UIView * bgV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 55)];
    [bgV setBackgroundColor:[UIColor whiteColor]];
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(17,0,ScreenWidth,13)];
    [label setText:infoStr];
    [label setFont:[UIFont systemFontOfSize:12]];
    [bgV addSubview:label];
    YXProgressView * progressV = [[YXProgressView alloc]init];
    [progressV getCurrentPage:4 andCurrentViewOfY:40];
    [bgV addSubview:progressV];
    
    return bgV;
}
/**
 *  生成一个HeaderView
 *
 *  @param title 标题
 *
 *  @return HeaderView
 */
-(UIView *)viewForSecitonHeader:(NSString*)title{
    UIView* headerV  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 35)];
    UILabel * titleL = [[UILabel alloc]initWithFrame:CGRectMake(15, 35-18, ScreenWidth-15, 18)];
    [titleL setFont:[UIFont systemFontOfSize:15]];
    [titleL setTextColor:[UIColor colorWithHexString:@"656464"]];
    [titleL setText:title];
    [headerV addSubview:titleL];
    [headerV setBackgroundColor:[UIColor whiteColor]];
    return headerV;
    
}

-(void)styleOfPlayerChange:(NSNotification *)noti{

    _rightButton.backgroundColor = [UIColor colorWithHexString:@"#7fb0e9"];
    [_rightButton setTitleColor:[UIColor whiteColor] forState:normal];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
