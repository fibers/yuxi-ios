//
//  YXRolesSettingViewController.m
//  inface
//
//  Created by 邢程 on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXRolesSettingViewController.h"
#import "YXCreateViewCellFooterViews.h"
#import "YXCreateViewRoleTypeTableViewCell.h"
#import "YXCustomMenuView.h"
#import "StoryCategory.h"
#import "CustomMenuModel.h"
#import "MBProgressHUD+YXStyle.h"
#import "YXSetRulesViewController.h"
#import "YXProgressView.h"
#import "PlayOfCreate.h"
#import <UITextView+Placeholder.h>
@interface YXRolesSettingViewController ()<UITableViewDelegate,UITableViewDataSource,YXCustomMenuViewDelegate,UIAlertViewDelegate,UITextViewDelegate>
@property (strong,nonatomic ) UITableView      * tableView;
@property (strong,nonatomic ) YXCustomMenuView * firstGroupMenuView;
@property (strong,nonatomic ) StoryCategory    * category;
@property (strong,nonatomic ) UITextView       * verifyRulesTV;
@property (strong, nonatomic) PlayOfCreate     * model;
@end

@implementation YXRolesSettingViewController
-(void)YXPlayintroductionPlayOfCreateModel:(PlayOfCreate *)playOfModel{
    _model = [[PlayOfCreate alloc]init];
    _model = playOfModel;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        [self loadData];
        [self setNavBar];
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

-(void)setNavBar{
    [self.navigationItem setTitle:@"角色设定"];
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    LeftButton.tag = 10;
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 48, 24);
    rightButton.backgroundColor = [UIColor colorWithHexString:@"#7fb0e9"];
    [rightButton setTitle:@"下一步" forState:normal];
    rightButton.tag = 11;
    [rightButton setTitleColor:[UIColor whiteColor] forState:normal];
    rightButton.layer.cornerRadius = 4.0;
    rightButton.layer.borderWidth = 1.0;
    [rightButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
    rightButton.layer.borderColor = [[UIColor colorWithHexString:@"#9b9b9b"]CGColor];
    [rightButton addTarget:self action:@selector(nextButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
}
-(void)returnBackl:(UIButton*)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)nextButtonClicked{
    YXSetRulesViewController *vc = [[YXSetRulesViewController alloc]init];
    _model.roleSettingArr = self.category.list;
    _model.verifiyRules = self.verifyRulesTV.text;
    [vc YXPlayintroductionPlayOfModel:_model];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark tableView dataSource&delegate
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 35;
    }
    return 55;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
        switch (section) {
            case 0:
                return [self viewForSectionFooter:@"选择参与者需要填写的角色设定项，或选择'+'添加" hiddenXuXian:NO];
                break;
            case 1:
                return [self viewForSectionFooter:@"" hiddenXuXian:YES];
                break;
            default:
                return [self viewForSectionFooter:@"" hiddenXuXian:NO];
                break;
        }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return [self viewForSecitonHeader:@"人物类型"];
            break;
        case 1:
            return [self viewForSecitonHeader:@"审核规则"];
        default:
            break;
    }
    return [self viewForSecitonHeader:@""];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
           return  self.firstGroupMenuView.viewH;
        }
            break;
        case 1:{
            return 100;
        }
            break;
        default:
            break;
    }
    return 0;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        UITableViewCell * cell = [[UITableViewCell alloc]init];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        _firstGroupMenuView = nil;
        [cell addSubview:self.firstGroupMenuView];
        return cell;
    }else{
        UITableViewCell * cell = [[UITableViewCell alloc]init];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell addSubview:self.verifyRulesTV];
        return cell;
    }
    return nil;
}
/**
 *  生成一个FooterView
 *
 *  @param infoStr 描述文字
 *
 *  @return FooterView
 */
-(UIView *)viewForSectionFooter:(NSString*)infoStr hiddenXuXian:(BOOL)isHidden{
    if (isHidden) {
        UIView * bgV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 55)];
        [bgV setBackgroundColor:[UIColor whiteColor]];
        YXCreateViewCellFooterViews * footerV = [[YXCreateViewCellFooterViews alloc]init];
        [footerV hideXuXian:isHidden];
        [footerV setContent:infoStr];
        [bgV addSubview:footerV];
        YXProgressView * progressV = [[YXProgressView alloc]init];
        [progressV getCurrentPage:5 andCurrentViewOfY:40];
        [bgV addSubview:progressV];
        return bgV;
        
    }else{
        YXCreateViewCellFooterViews * footerV = [[YXCreateViewCellFooterViews alloc]init];
        [footerV hideXuXian:isHidden];
        [footerV setContent:infoStr];
            return footerV;
    }
}
/**
 *  生成一个HeaderView
 *
 *  @param title 标题
 *
 *  @return HeaderView
 */
-(UIView *)viewForSecitonHeader:(NSString*)title{
    UIView* headerV  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 35)];
    UILabel * titleL = [[UILabel alloc]initWithFrame:CGRectMake(15, 35-18, ScreenWidth-15, 18)];
    [titleL setFont:[UIFont systemFontOfSize:15]];
    [titleL setTextColor:[UIColor colorWithHexString:@"656464"]];
    [titleL setText:title];
    [headerV addSubview:titleL];
    [headerV setBackgroundColor:[UIColor whiteColor]];
    return headerV;
    
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}
#pragma mark -
#pragma mark 网络请求
-(void)loadData{
    MBProgressHUD * hud = [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    [HttpTool postWithPath:@"GetStoryRoleNatures" params:@{@"uid":USERID} success:^(id JSON) {
        [hud hide:YES];
        _category = [StoryCategory objectWithKeyValues:JSON];
        [self.view addSubview:self.tableView];
        
    } failure:^(NSError *error) {
        [hud hide:YES];
    }];
}
#pragma mark -
#pragma mark 分类view
-(YXCustomMenuView *)firstGroupMenuView{
    if(!_firstGroupMenuView){
        YXCustomMenuView * mv = [[YXCustomMenuView alloc]init];
        mv.delegate = self;
        [mv setShowAddBtn:YES];
        CustomMenuModel  * model = [self.category.list objectAtIndex:0];
        model.isIrreversible = YES;
        model.isSelected = YES;
        [mv setMenusWithCustomMenuModelArr:self.category.list];
        [mv setMaxSelectedBtnsCount:MAXFLOAT andShouldShowAlertView:YES];
        mv.frame = CGRectMake(0, 0, ScreenWidth, mv.viewH);
        _firstGroupMenuView = mv;
    }
    return _firstGroupMenuView;
}
#pragma mark 审核规则textVeiw
-(UITextView *)verifyRulesTV{
    if (!_verifyRulesTV) {
        _verifyRulesTV = [[UITextView alloc]initWithFrame:CGRectMake(15, 0, ScreenWidth -30 , 72)];
        _verifyRulesTV.layer.borderColor = [UIColor colorWithHexString:@"656464"].CGColor;
        _verifyRulesTV.layer.borderWidth = 0.5;
        _verifyRulesTV.layer.cornerRadius = 3.0;
        [_verifyRulesTV.layer setMasksToBounds:YES];
        _verifyRulesTV.delegate = self;
        [_verifyRulesTV setPlaceholder:@"选填。人设和戏文审核的要求写在这里"];
    }
    return _verifyRulesTV;
}
-(void)addBtnClicked{
    UIAlertView * alertV = [[UIAlertView alloc]initWithTitle:@"添加标签" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertV setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alertV setTitle:@"添加标签"];
    [alertV show];
}
-(void)textViewDidChange:(UITextView *)textView{
    if([textView.text charLength]>300){
        [textView setTextColor:[UIColor redColor]];
    }else{
        [textView setTextColor:[UIColor blackColor]];
    
    }

}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text charLength]>300) {
        UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:@"提示" message:@"审核规则不能超过300字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertV show];
        
        [textView becomeFirstResponder];
    }

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
       UITextField * tf =  [alertView textFieldAtIndex:0];
        tf.text = [tf.text trim];
        if([tf.text charLength] <1){
            return;
        }
        if([tf.text charLength]>4){
            MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = @"最多4个字,请重新添加。";
            [hud show:YES];
            [hud hide:YES afterDelay:1.5];
            [self.view endEditing:YES];
            return;
        }
        
        CustomMenuModel * model  = [[CustomMenuModel alloc]init];
        model.isUserAdd = YES;
        model.isSelected = YES;
        model.name = tf.text;
        [self.category.list addObject:model];

        [self.tableView reloadData];
    }
}
@end
