//
//  YXSetRulesViewController.m
//  inface
//
//  Created by lizhen on 15/10/30.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXSetRulesViewController.h"
#import "YXProgressView.h"
#import "MBProgressHUD.h"
#import "StoryCategory.h"
#import "CustomMenuModel.h"
#import "RolesTypeItem.h"
#import "ApplyTheaterViewController.h"
@interface YXSetRulesViewController ()

@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UILabel *lab;
@property (nonatomic,strong)UITextView *textView;
@property(nonatomic,copy)MBProgressHUD * hud;
@property(nonatomic,copy)NSString * storyId;

@property (strong, nonatomic)PlayOfCreate *model;
@end

@implementation YXSetRulesViewController

-(void)YXPlayintroductionPlayOfModel:(PlayOfCreate *)playOfModel{
    
    _model = [[PlayOfCreate alloc]init];
    _model = playOfModel;
}

-(MBProgressHUD *)hud{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc]initWithView:self.view];
    }
    return _hud;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self createLeftButton];
    [self createScrollView];
    [self createTextView];
}

-(void)createLeftButton{
    
    self.navigationItem.title = @"规则设定";
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    LeftButton.tag = 10;
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _rightButton.frame = CGRectMake(0, 0, 48, 24);
    [_rightButton setTitle:@"发布" forState:normal];
    _rightButton.titleLabel.font = [UIFont systemFontOfSize:12.0];
    _rightButton.tag = 11;
    _rightButton.layer.borderWidth = 0;
    [_rightButton setTitleColor:[UIColor whiteColor] forState:normal];
    _rightButton.backgroundColor = [UIColor colorWithHexString:@"#7fb0e9"];
    _rightButton.layer.cornerRadius = 4.0;
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_rightButton];
    [_rightButton addTarget:self action:@selector(returnBackl:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    
}

//左上角按钮&&&发布按钮
-(void)returnBackl:(UIButton *)btn{
    if (btn.tag == 10) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [_textView endEditing:YES];
        //先创建一个审核群
        __block NSString *groupID = @"";
        //创建群
        DDCreateGroupAPI * createGroup = [[DDCreateGroupAPI alloc]init];
        [self.hud setLabelText:@"数据请求中..."];
        [self.hud setMode:MBProgressHUDModeIndeterminate];
        [self.view addSubview:self.hud];
        [self.hud show:YES];
        [self.view bringSubviewToFront:self.hud];
        NSString * user_userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];

        NSArray * object = @[@"审核群",@"",@[user_userid]];
        [createGroup requestWithObject:object Completion:^(GroupEntity * response, NSError *error) {
            if (!error) {
                groupID=response.objID;
                NSString * group_id= @"";
                NSArray * ids = [groupID componentsSeparatedByString:@"_"];
                if (ids[1]) {
                    group_id = ids[1];
                }else{
                    group_id = ids[0];
                }
                NSDictionary * params = [self getDictFrom:_model];
                
                NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:params];
                
                [dic setObject:group_id forKey:@"groupid"];
                //创建剧
                [HttpTool postWithPath:@"CreateStoryStepByStep" params:dic success:^(id JSON) {
                    if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                        //绑定审核群
                        NSString *stroyidAndgroupid=[JSON objectForKey:@"describe"];
                        __block  NSArray *aArray=[stroyidAndgroupid componentsSeparatedByString:@","];
                        self.storyId = [aArray objectAtIndex:0];
                    NSDictionary * params = @{@"uid":USERID,@"storyid":[aArray objectAtIndex:0],@"title":@"审核群",@"intro":@"",@"groupid":[aArray objectAtIndex:1],@"isplay":@"0"};
                        //绑定审核群和剧
                        [HttpTool postWithPath:@"AddGroupToStory" params:params success:^(id JSON) {
                            if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                                //绑定成功，创建水聊群
                                __block NSString * groupid2  =@"";
                                NSArray * object = @[@"水聊群",@"",@[user_userid]];
                         [createGroup requestWithObject:object Completion:^(GroupEntity * response, NSError *error) {
                             if (!error) {
                                 groupid2=response.objID;
                                 NSArray * gruops = [groupid2 componentsSeparatedByString:@"_"];

                                 NSDictionary * params = @{@"uid":USERID,@"storyid":[aArray objectAtIndex:0],@"title":@"水聊群",@"intro":@"",@"groupid":[gruops objectAtIndex:1],@"isplay":@"2"};
                               //绑定水聊群
                                 [HttpTool postWithPath:@"AddGroupToStory" params:params success:^(id JSON) {
                                     if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                                         //绑定成功,创建对戏群
                                         NSArray * object = @[@"对戏群",@"",@[user_userid]];
          [createGroup requestWithObject:object Completion:^(GroupEntity * response, NSError *error) {
           if (!error) {
        NSString * groupid3 = response.objID;//新建群的id
        NSArray * group_id = [groupid3 componentsSeparatedByString:@"_"];
        NSDictionary * params = @{@"uid":USERID,@"storyid":[aArray objectAtIndex:0],@"title":@"对戏群",@"intro":@"",@"groupid":[group_id objectAtIndex:1],@"isplay":@"1"};
        [HttpTool postWithPath:@"AddGroupToStory" params:params success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                [[MyLiveViewController shareInstance]getMyStorys];
                //跳到认领角色
                ApplyTheaterViewController * detailer = [[ApplyTheaterViewController alloc]init];
                
                detailer.hidesBottomBarWhenPushed = YES;
                
                detailer.thearterId = self.storyId;
                [detailer ifStoryOwnerApply];
                
                [self.navigationController pushViewController:detailer animated:YES];
    
            }else{
                [self.hud setLabelText:[JSON objectForKey:@"describe"]];
                
                [self.hud hide:YES afterDelay:2.0f];
            }
            
        } failure:^(NSError *error) {
            
            
            [self.hud setLabelText:@"创建失败"];
            
            [self.hud hide:YES afterDelay:2.0f];
        }];

    }else{
        [self.hud setLabelText:@"创建失败"];
        
        [self.hud hide:YES afterDelay:2.0f];
    }
    
}];
                                         
                                     }else{
                                         [self.hud setLabelText:[JSON objectForKey:@"describe"]];
                                         
                                         [self.hud hide:YES afterDelay:2.0f];
                                     }
                                     
                                 } failure:^(NSError *error) {
                                     
                                     [self.hud setLabelText:@"创建失败"];
                                     
                                     [self.hud hide:YES afterDelay:2.0f];
                                 }];
                                 
                             }else{
                                 [self.hud setLabelText:@"创建失败"];
                                 
                                 [self.hud hide:YES afterDelay:2.0f];
                             }
    
                         }];
                                
                            }else
                            {
                                [self.hud setLabelText:[JSON objectForKey:@"describe"]];
                                
                                [self.hud hide:YES afterDelay:2.0f];
                            }
                            
                        } failure:^(NSError *error) {
                            
                            [self.hud setLabelText:@"创建失败"];
                            
                            [self.hud hide:YES afterDelay:2.0f];
                        }];
                    }else{
                        //创建出问题
                        [self.hud setLabelText:[JSON objectForKey:@"describe"]];
                        
                        [self.hud hide:YES afterDelay:2.0f];
                    }
                    
                } failure:^(NSError *error) {
                    [self.hud setLabelText:@"创建失败"];
                    
                    [self.hud hide:YES afterDelay:2.0f];
                    
                }];
                
            }else{
                [[DDTcpClientManager instance]reconnect];
                
                [self.hud setLabelText:@"创建失败"];
                [self.hud setMode:MBProgressHUDModeText];
                [self.hud hide:YES afterDelay:2];
            }
            
        }];
        
        
        _model.setRules = _textView.text;
        LOG(@"%@",_model);
        [self getDictFrom:_model];
        
        //[self ooo];
    }
}

///返回一个字典
-(NSDictionary *)getDictFrom:(PlayOfCreate *)playModel{
    
    PlayOfCreate * play_model = [[PlayOfCreate alloc]init];
    play_model = playModel;
    
    RolesTypeItem *rolesType = [[RolesTypeItem alloc]init];
    
    
    NSArray *array = play_model.rolesTypeSettingArray;
    
    //NSDictionary *dict;
    //NSString *num = [NSString stringWithFormat:@"%ld",(long)rolesType.roleNum];
    ///一个map组成的数组
    NSMutableArray *mulArray = [[NSMutableArray alloc]init];
    
    for (rolesType in array) {
        //NSString *num = [NSString stringWithFormat:@"%ld",(long)rolesType.roleNum];
        NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:
                               rolesType.roleName, @"type",
                               [NSString stringWithFormat:@"%ld",(long)rolesType.roleNum] , @"count",
                               rolesType.roleDesc, @"background",
                               nil];
        [mulArray addObject:dict];
    }
    ///一个属性id组成的数组
    NSMutableArray *ziji_id_array = [[NSMutableArray alloc]init];
    
    ///用户添加的属性数组
    NSMutableArray *custom_name_array = [[NSMutableArray alloc]init];
    ///剧的分类的模型
    CustomMenuModel *customeModel = [[CustomMenuModel alloc]init];
    NSArray *listMul = [[NSArray alloc]init];
    listMul = play_model.roleSettingArr;
    
    for (customeModel in listMul) {
        if (customeModel.isUserAdd == NO) {
            if (customeModel.isSelected == YES) {
                NSString *zijide_id = customeModel.ID;
                NSNumber *n5 = [NSNumber numberWithInt:[zijide_id intValue]];
                [ziji_id_array addObject:n5];
            }
        }else{
            if (customeModel.isSelected == YES) {
                NSString *custom_name = customeModel.name;
                [custom_name_array addObject:custom_name];
            }
            
        }
        
    }
    
     NSString *s1 = [play_model.IDArray JSONString];

    NSString *s2 = [custom_name_array JSONString];

    NSString *s3 = [ziji_id_array JSONString];

    
    NSString *s4 = [mulArray JSONString];

    ///校验
    if (play_model.nameOfPlay == nil) {
        play_model.nameOfPlay = @"";
    }
    if (play_model.summaryOfPlay == nil) {
        play_model.summaryOfPlay = @"";
    }
    if (play_model.mainStoryLine == nil) {
        play_model.mainStoryLine = @"";
    }
    if (play_model.setRules == nil) {
        play_model.setRules = @"";
    }
    if (play_model.verifiyRules == nil) {
        play_model.verifiyRules = @"";
    }

    NSDictionary *fanhuiDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                USERID,@"uid",
                                @"00000",@"groupid",
                                play_model.nameOfPlay,@"title",
                                play_model.upYunOfImageUrl,@"storyimg",
                                s1,@"categories", //类别
                                play_model.summaryOfPlay,@"intro",
                                play_model.mainStoryLine,@"mainplot",
                                s4,@"roletypes",
                                s3,@"rolenatures", //自己的
                                s2,@"newrolenatures", //添加的
                                play_model.verifiyRules,@"rolerule",
                                play_model.setRules,@"storyrule",
                                nil];
    
    
    return fanhuiDict;
}

- (NSData *)toJSONData:(id)theData{
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if ([jsonData length] > 0 && error == nil){
        return jsonData;
    }else{
        return nil;
    }
}

-(void)createScrollView{
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [self.view addSubview:_scrollView];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(ScreenWidth, ScreenWidth==320?ScreenHeight+100:ScreenHeight);
    [_scrollView setShowsVerticalScrollIndicator:NO];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [_scrollView addGestureRecognizer:tap];

}

-(void)createTextView{
    ///TextView的高度
    CGFloat textView_H;
    if (ScreenHeight > 700) {
        textView_H = 280.0;
    }else if(ScreenHeight > 650){
        textView_H = 250.0;
    }else{
        textView_H = 200.0;
    }
    if (ScreenHeight == 480) {
        textView_H = 170;
    }
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(15, 6, ScreenWidth-15, textView_H)];
    [_scrollView addSubview:_textView];
    _textView.placeholder = @"选填。立个群规也是很重要哒。例如戏群严禁灌水，剧情探讨移驾水聊群：禁水禁短句，码字30字起。";
    _textView.font = [UIFont systemFontOfSize:14.0];
    _textView.delegate = self;

    UILabel *lineLab = [[UILabel alloc]initWithFrame:CGRectMake(0, textView_H+20, ScreenWidth, 1)];
    lineLab.backgroundColor = [UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1];
    [_scrollView addSubview:lineLab];

    YXProgressView *progressView = [[YXProgressView alloc]init];
    UIView *progress =[progressView getCurrentPage:6 andCurrentViewOfY:_textView.frame.origin.y+textView_H+220];
    [_scrollView addSubview:progress];
}

-(void)tapClick{
    
    [_textView endEditing:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//判断内容大小，多出去的部分进行自动截取
- (void)textViewDidChange:(UITextView *)textView {
    NSInteger number = [textView.text length];
    if (number >= 300) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"规则设定最长不超过300个字" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        textView.text = [textView.text substringToIndex:300];
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    CGFloat panging;
    if (ScreenHeight == 480 && ScreenWidth == 320 ) {
        panging = 10;
    }
    if (ScreenWidth == 320 && ScreenHeight !=480) {
        panging = 10;
    }
    if(ScreenWidth != 320 && ScreenHeight !=480){
        panging = 0;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - panging, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    CGFloat panging;
    if (ScreenHeight == 480 && ScreenWidth == 320 ) {
        panging = 10;
    }
    if (ScreenWidth == 320 && ScreenHeight !=480) {
        panging = 10;
    }
    if(ScreenWidth != 320 && ScreenHeight !=480){
        panging = 0;
    }
    
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + panging, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}



@end
