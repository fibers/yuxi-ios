//
//  YXFrontCoverViewController.m
//  inface
//
//  Created by lizhen on 15/11/2.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXFrontCoverViewController.h"
#import "YXProgressView.h"
#import "YXSummayViewController.h"
#import "MBProgressHUD.h"
#import "CustomMenuModel.h"
#import "YXPlayIntroductionViewController.h"
#import "PlayOfCreate.h"
#import "EVCircularProgressView.h"
#import "YXRoleReviewViewController.h"
#import "YXApplicationRoleViewController.h"

@interface YXFrontCoverViewController ()

@property (nonatomic, strong) UIScrollView            *scrollView;
///灰色大背景
@property (nonatomic, strong) UIImageView             *imageView;
///加号按钮
@property (nonatomic, strong) UIButton                *plusBtn;
@property (nonatomic, strong) UILabel                 *lab;
///剧名输入框
@property (nonatomic, strong) UITextField             *textField;
@property (nonatomic, strong) UIView                  *lineView;
@property (nonatomic, strong) UILabel                 *jumingLab;
///选择类别按钮
@property (nonatomic, strong) UIButton                *selectBtn;
@property (nonatomic, strong) UIImageView             *dropDownImage;
@property (nonatomic, strong) UIButton                *rightButton;
///图片又拍地址
@property (strong,nonatomic ) NSString                * imgSerUrl;

@property (strong, nonatomic) UpYun                   * upYun;

@property (nonatomic, strong) UIImagePickerController *imagepicker;

@property (strong,nonatomic ) EVCircularProgressView  * circleView;

///待用的model
@property (strong, nonatomic) CustomMenuModel         *model;

@property (strong, nonatomic) CustomMenuModel         *model1;
@property (strong, nonatomic) CustomMenuModel         *model2;
@property (strong, nonatomic) CustomMenuModel         *model3;

@property (strong,nonatomic ) NSArray                 *modelArray;
@property (strong,nonatomic ) NSMutableArray          *IDarray;
@end

@implementation YXFrontCoverViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        [[IQKeyboardManager sharedManager]setEnable:YES];
        [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        self.hidesBottomBarWhenPushed = YES;
        self.view.userInteractionEnabled = YES;
        [self createLeftButton];
        [self createScrollView];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldEditChanged:) name:UITextFieldTextDidChangeNotification object:_textField];
}

-(void)createLeftButton{
    
    self.navigationItem.title = @"创建剧";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#656464"],NSFontAttributeName:[UIFont systemFontOfSize:17.0]}];
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    LeftButton.tag = 12;
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(click_plusBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _rightButton.frame = CGRectMake(0, 0, 48, 24);
    [_rightButton setTitle:@"下一步" forState:normal];
    _rightButton.titleLabel.font = [UIFont systemFontOfSize:12.0];
    _rightButton.tag = 13;
    [_rightButton setTitleColor:[UIColor colorWithHexString:@"#9b9b9b"] forState:normal];
    _rightButton.layer.cornerRadius = 4.0;
    _rightButton.layer.borderWidth = 1.0;
    _rightButton.layer.borderColor = [[UIColor colorWithHexString:@"#9b9b9b"]CGColor];
    [_rightButton addTarget:self action:@selector(click_plusBtn:) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_rightButton];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    
}


-(void)createScrollView{
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    
    _scrollView.delegate = self;
    _scrollView.scrollEnabled = YES;
    [_scrollView setContentSize: CGSizeMake(ScreenWidth, ScreenHeight+70)];
    [self.view addSubview:_scrollView];
    
    CGFloat imageView_W = ScreenWidth == 320?230:270;
    CGFloat imageView_H = ScreenWidth == 320?196:230;
    CGFloat plusBtn_W = ScreenWidth == 320?120:140;
    CGFloat plusBtn_H = ScreenWidth == 320?103:120;
    
    
    _lab = [[UILabel alloc]init];
    _lab.frame = CGRectMake((ScreenWidth-imageView_W)/2, 12, ScreenWidth-52, 16);
    _lab.text = @"封面";
    _lab.font = [UIFont systemFontOfSize:15.0];
    _lab.textColor = [UIColor blackColor];
    [_scrollView addSubview:_lab];
    
    //后方背景
    _imageView = [[UIImageView alloc]init];
    _imageView.frame = CGRectMake((ScreenWidth-imageView_W)/2, 21+20, imageView_W, imageView_H);
    _imageView.backgroundColor = [UIColor colorWithHexString:@"#e9e9e9"];
    _imageView.userInteractionEnabled = YES;
    [_imageView setContentMode:UIViewContentModeScaleAspectFill];
    _imageView.clipsToBounds = YES;
    [_scrollView addSubview:_imageView];

 
    _imagepicker = [[UIImagePickerController alloc]init];
    
    //TableView单击手势
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyBoard)];
    [_scrollView addGestureRecognizer:tapGesture];
    
    _plusBtn = [[UIButton alloc]init];
    _plusBtn.tag = 10;
    _plusBtn.frame = CGRectMake((_imageView.frame.size.width-plusBtn_W)/2, (_imageView.frame.size.height-plusBtn_H)/2, plusBtn_W, plusBtn_H);
    [_plusBtn setBackgroundImage:[UIImage imageNamed:@"CreateView_add_background"] forState:normal];
    [_plusBtn addTarget:self action:@selector(click_plusBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_imageView addSubview:_plusBtn];
    ///进度圈
    CGFloat circleView_W_H = 40;
    _circleView = [[EVCircularProgressView alloc]initWithFrame:CGRectMake((_imageView.frame.size.width-circleView_W_H)/2, (_imageView.frame.size.height-circleView_W_H)/2, 40, 40)];
    
    //剧名输入框
    _textField = [[UITextField alloc]init];
    _textField.frame = CGRectMake(25, _imageView.frame.origin.y+imageView_H+5+42, ScreenWidth-25, 16);
    _textField.placeholder = @"请输入剧名（30字内）";
    UIColor *placeholderColor = [UIColor colorWithHexString:@"#c5c5c6"];
    [placeholderColor setFill];
    _textField.font = [UIFont systemFontOfSize:15.0];
    _textField.delegate = self;
    [_scrollView addSubview:_textField];
    
    _lineView = [[UIView alloc]init];
    _lineView.frame = CGRectMake(0, _textField.frame.origin.y+16+16, ScreenWidth, 1);
    _lineView.backgroundColor = [UIColor colorWithHexString:@"#c5c5c6"];
    [_scrollView addSubview:_lineView];
    
    //类别选择按钮
    _selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _selectBtn.tag = 11;
    _selectBtn.frame = CGRectMake(15, _lineView.frame.origin.y+13+35, ScreenWidth-30, 50);
    [_selectBtn setTitle:@"选择类别" forState:UIControlStateNormal];
    _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _selectBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    _selectBtn.titleLabel.font  = [UIFont systemFontOfSize:14.0];
    [_selectBtn setTitleColor:[UIColor colorWithHexString:@"#656464"] forState:UIControlStateNormal];
    _selectBtn.layer.cornerRadius = 4.0;
    _selectBtn.layer.borderWidth = 1.0;
    _selectBtn.layer.borderColor = [[UIColor colorWithHexString:@"#979797"]CGColor];
    
    _dropDownImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"CreateView_dropDown"]];
    _dropDownImage.frame = CGRectMake(_selectBtn.frame.size.width-30, 15, 20, 20);
    [_selectBtn addSubview:_dropDownImage];
    [_selectBtn addTarget:self action:@selector(click_plusBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:_selectBtn];
    
    //进度条
    YXProgressView *progressView = [[YXProgressView alloc]init];
    UIView *progress =[progressView getCurrentPage:1 andCurrentViewOfY:_selectBtn.frame.origin.y+50+47];
    [_scrollView addSubview:progress];
    
}

-(void)dismissKeyBoard{

    [_textField endEditing:YES];
}

///按钮点击事件
-(void)click_plusBtn:(UIButton *)btn{

    if (btn.tag == 10) {
        [self selectPhotos];
    }else if(btn.tag == 11){
        LOG(@"选择类别");
        YXCreateViewChoseTypeViewController *vc = [[YXCreateViewChoseTypeViewController alloc]init];
        vc.delegate = self;
        [self presentViewController:vc animated:YES completion:^{
            
        }];
    }else if(btn.tag == 12){
        [self.navigationController popViewControllerAnimated:YES];
    }else if(btn.tag == 13){
        
        [self pandaun];
    }
}
#pragma mark --YXCreateViewChoseTypeViewControllerDelegate  delegate
-(void)yxCreateViewChoseTypeViewSelectedContent:(NSArray *)arr{
    
    _model = [[CustomMenuModel alloc]init];
    _model1 = [[CustomMenuModel alloc]init];
    _model2 = [[CustomMenuModel alloc]init];
    _model3 = [[CustomMenuModel alloc]init];
    NSString *s1 = [[NSString alloc]init];
    NSString *s2 = [[NSString alloc]init];
    NSString *s3 = [[NSString alloc]init];
    
    NSString *s4 = [[NSString alloc]init];
    NSString *s5 = [[NSString alloc]init];
    NSString *s6 = [[NSString alloc]init];
    
    
    _modelArray = [NSArray array];
    _modelArray = arr;
    
    _IDarray = [[NSMutableArray alloc]init];
    
    ///脑子懵了，想不起好的方法，用的笨方法,以后在改进
    if (arr.count == 1) {
        _model = arr[0];
        [_selectBtn setTitle:[NSString stringWithFormat:@"%@",_model.name] forState:normal];
        s4 = _model.ID;
        NSNumber *n4 = [NSNumber numberWithInt:[s4 intValue]];
        [_IDarray addObject:n4];
    }else if (arr.count == 2) {
        for (NSInteger i = 0; i<arr.count; i++) {
            _model1 = arr[0];
            s1 = [NSString stringWithFormat:@"%@",_model1.name];
            _model2 = arr[1];
            s2 = [NSString stringWithFormat:@"%@",_model2.name];
            
            s4 = _model1.ID;
            s5 = _model2.ID;
            
        }
        NSNumber *n4 = [NSNumber numberWithInt:[s4 intValue]];
        NSNumber *n5 = [NSNumber numberWithInt:[s5 intValue]];
        [_selectBtn setTitle:[NSString stringWithFormat:@"%@、%@",s1,s2] forState:normal];
        [_IDarray addObject:n4];
        [_IDarray addObject:n5];
    }else{
        for (NSInteger i = 0; i<arr.count; i++) {
            _model1 = arr[0];
            s1 = [NSString stringWithFormat:@"%@",_model1.name];
            _model2 = arr[1];
            s2 = [NSString stringWithFormat:@"%@",_model2.name];
            _model3 = arr[2];
            s3 = [NSString stringWithFormat:@"%@",_model3.name];
            
            s4 = _model1.ID;
            s5 = _model2.ID;
            s6 = _model3.ID;
        }
        NSNumber *n4 = [NSNumber numberWithInt:[s4 intValue]];
        NSNumber *n5 = [NSNumber numberWithInt:[s5 intValue]];
        NSNumber *n6 = [NSNumber numberWithInt:[s6 intValue]];
        [_selectBtn setTitle:[NSString stringWithFormat:@"%@、%@、%@",s1,s2,s3] forState:normal];
        [_IDarray addObject:n4];
        [_IDarray addObject:n5];
        [_IDarray addObject:n6];
    }
    
}

///选取照片
-(void) selectPhotos
{
    
    BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    //如果支持相机，那么多个选项
    if (isCameraSupport) {
        UIActionSheet* mySheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"挑照片" otherButtonTitles:@"现在拍", nil];
        [mySheet showInView:self.view];
        
    }
    else {
        
        [self startPhotoChooser];
        
    }
     
}

#pragma mark actionSheet Delegate

- (void) startPhotoChooser
{
    UzysAssetsPickerController *picker = [[UzysAssetsPickerController alloc] init];
    picker.delegate = self;
    
    picker.maximumNumberOfSelectionPhoto = 1;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //choose photo
    if (buttonIndex == 0) {
        
        [self startPhotoChooser];
        
    } else if (buttonIndex == 1) {
        //take photo.
        //指定使用照相机模式,可以指定使用相册／照片库
        _imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        //设置当拍照完或在相册选完照片后，是否跳到编辑模式进行图片剪裁。只有当showsCameraControls属性为true时才有效果
        _imagepicker.allowsEditing = NO;
        //设置拍照时的下方的工具栏是否显示，如果需要自定义拍摄界面，则可把该工具栏隐藏
        _imagepicker.showsCameraControls  = YES;
        //设置使用后置摄像头，可以使用前置摄像头
        _imagepicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        //设置闪光灯模式
        /*
         typedef NS_ENUM(NSInteger, UIImagePickerControllerCameraFlashMode) {
         UIImagePickerControllerCameraFlashModeOff  = -1,
         UIImagePickerControllerCameraFlashModeAuto = 0,
         UIImagePickerControllerCameraFlashModeOn   = 1
         };
         */
        _imagepicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        //设置相机支持的类型，拍照和录像
        _imagepicker.mediaTypes = @[(NSString*)kUTTypeImage];
        //设置拍摄时屏幕的view的transform属性，可以实现旋转，缩放功能
        // imagepicker.cameraViewTransform = CGAffineTransformMakeRotation(M_PI*45/180);
        // imagepicker.cameraViewTransform = CGAffineTransformMakeScale(1.5, 1.5);
        //所有含有cameraXXX的属性都必须要sourceType是UIImagePickerControllerSourceTypeCamera时设置才有效果，否则会有异常
        //设置UIImagePickerController的代理
        _imagepicker.delegate = self;
        [self presentViewController:_imagepicker animated:YES completion:^{
            
        }];
        
    }
    
    
}

- (void)uzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    
    
    if([[assets[0] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
    {
        
        [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            ALAsset *representation = obj;
            
            UIImage * original = [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                                     scale:representation.defaultRepresentation.scale
                                               orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
            //            original = [HZSInstances scaleImage:original toScale:0.4];
            _imageView.image=original;
            _plusBtn.hidden = YES;
            
            CGSize  imgSize = CGSizeMake(2210, 1280);
            
            original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
            
            [self getServerKey :original];
            
        }];
        
    }
    else //Video
    {
        
        
    }
    
}

#pragma take photos image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
    NSString* type = [info objectForKey:UIImagePickerControllerMediaType];
    //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    if ([type isEqualToString:(NSString*)kUTTypeImage]&&picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
        //获取照片的原图
        UIImage* original = [info objectForKey:UIImagePickerControllerOriginalImage];
        _imageView.image=original;
        _plusBtn.hidden = YES;
        CGSize  imgSize = CGSizeMake(2210, 1280);
        
        original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
        [self getServerKey :original];
        
        //如果是拍照的照片，则需要手动保存到本地，系统不会自动保存拍照成功后的照片
        UIImageWriteToSavedPhotosAlbum(original, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
    }else{
        
    }
    //模态方式退出uiimagepickercontroller
    [_imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}



#pragma mark 从又拍获取图片字符串
-(void)getServerKey:(UIImage *)image
{
        //[SVProgressHUD showWithStatus:@"上传中" maskType:SVProgressHUDMaskTypeBlack];
    //此处需要换成又拍的云存储
    _upYun = [[UpYun alloc]init];
    
    //上传图片成功
    
    [_upYun uploadImage:image];
    [self.circleView setHidden:NO];
    
    [self.circleView setProgress:0.0 animated:YES];
    self.circleView.alpha = 1;
    [self.circleView commonInit];
    [_imageView addSubview:self.circleView];
    [_imageView bringSubviewToFront:self.circleView];
    UITapGestureRecognizer *tapOn = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Click)];
    [_imageView addGestureRecognizer:tapOn];

    WS(weakSelf);
    _upYun.successBlocker = ^(id data){
        if (!weakSelf) {
            return ;
        }
        
        
        NSString        *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
        imgUrl  =   [NSString stringWithFormat:@"%@%@", IMAGE_SERVER_BASE_URL, imgUrl];
        [weakSelf.circleView setHidden:YES];
        
        weakSelf.imgSerUrl = imgUrl;
        [SVProgressHUD dismiss];
        if (_circleView.progress == 1.0) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
            [_imageView addGestureRecognizer:tap];
        }
        
    };
    _upYun.failBlocker = ^(NSError * error)
    {
        //上传又拍失败的操作
        [weakSelf.circleView setHidden:YES];
        
        [SVProgressHUD showErrorWithStatus:@"上传图片失败,请检查网络" duration:2.0];
        UITapGestureRecognizer *t = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
        [_imageView addGestureRecognizer:t];
    };
    _upYun.progressBlocker = ^(CGFloat percent,long long requestDidSendBytes)
    {
        [ weakSelf.circleView setProgress:percent animated:YES];
    };
   
}


//取消照相机的回调
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //模态方式退出uiimagepickercontroller
    [_imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//保存照片成功后的回调
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo{
    
}

-(void)DidChange:(NSNotification*)noti{
    
    //判断三项必填内容
    if (( _imageView.image != nil) && ![_selectBtn.titleLabel.text isEqualToString:@"选择类别"]) {
        
        ///可用
        _rightButton.layer.borderWidth = 0;
        [_rightButton setTitleColor:[UIColor whiteColor] forState:normal];
        _rightButton.backgroundColor = [UIColor colorWithHexString:@"#7fb0e9"];
        [_rightButton setEnabled:YES];
    }
    else if (( _imageView.image != nil) | ![_selectBtn.titleLabel.text isEqualToString:@"选择类别"]){
        ///不可用
        _rightButton.layer.borderWidth = 1.0;
        [_rightButton setTitleColor:[UIColor colorWithHexString:@"#9b9b9b"] forState:normal];
        _rightButton.backgroundColor = [UIColor clearColor];
        //[_rightButton setEnabled:NO];
    }
    [_rightButton addTarget:self action:@selector(click_plusBtn:) forControlEvents:UIControlEventTouchUpInside];
}

//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//
//    if (textField.text.length > 30) {
//        [[[UIAlertView alloc]initWithTitle:@"提示" message:@"剧名最长不超过30个字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
//        [self.view endEditing:YES];
//        _textField.text = [textField.text substringToIndex:30];
//        NSLog(@"%@***",_textField.text);
//        return NO;
//    }else{
//        return YES;
//    }
//
//}

///界面出现时，判断必填内容是否为空
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    if (( _imageView.image != nil) && ![_selectBtn.titleLabel.text isEqualToString:@"选择类别"] && _textField.text.length > 0) {
        _rightButton.layer.borderWidth = 0;
        [_rightButton setTitleColor:[UIColor whiteColor] forState:normal];
        _rightButton.backgroundColor = [UIColor colorWithHexString:@"#7fb0e9"];
        [_rightButton setEnabled:YES];
        [_rightButton addTarget:self action:@selector(click_plusBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
}


-(void)pandaun{

    if (_imageView.image == nil) {
        [[[UIAlertView alloc]initWithTitle:@"提示" message:@"请提供一张封面图片" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
        return;
    }
    if (_textField.text.length == 0) {
        [[[UIAlertView alloc]initWithTitle:@"提示" message:@"请输入剧名, 剧名最长不超过30个字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
        [_textField becomeFirstResponder];
        return;
    }
    if ([_selectBtn.titleLabel.text isEqualToString:@"选择类别"]) {
        [[[UIAlertView alloc]initWithTitle:@"提示" message:@"请选择一个类别" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
        return;
    }
    if (_imageView.image != nil && ![_selectBtn.titleLabel.text isEqualToString:@"选择类别"] && _textField.text.length > 0) {
        if ([_imgSerUrl isEqualToString:@""] | (_imgSerUrl == nil) ) {
            [[[UIAlertView alloc]initWithTitle:@"提示" message:@"请等待图片上传完成" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
            return;
        }
        
        YXPlayIntroductionViewController *vc = [[YXPlayIntroductionViewController alloc]init];
        
        PlayOfCreate *playModel = [[PlayOfCreate alloc]init];
        playModel.upYunOfImageUrl = _imgSerUrl;
        LOG(@"%@",_imgSerUrl);
        if ([_textField.text length ]> 30) {
            _textField.text = [_textField.text substringToIndex:30];
        }
        LOG(@"%@",_textField.text);
        playModel.nameOfPlay = _textField.text;
        playModel.typeOfSelectArray = _modelArray;
        playModel.IDArray = _IDarray;
        [vc YXPlayintroductionPlayOfCreateModel:playModel];
        [_textField endEditing:YES];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [self.view endEditing:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
//    [_textField endEditing:YES];
}

///textfield改变 不同版本上移的大小
-(void)textFieldDidBeginEditing:(UITextField *)textField{

    CGFloat panging;
    if (ScreenHeight == 480 && ScreenWidth == 320 ) {
        panging = 150;
    }
    if (ScreenWidth == 320 && ScreenHeight !=480) {
        panging = 80;
    }
    if(ScreenWidth != 320 && ScreenHeight !=480){
        panging = 15;
    }
    
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - panging, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    CGFloat panging;
    if (ScreenHeight == 480 && ScreenWidth == 320 ) {
        panging = 150;
    }
    if (ScreenWidth == 320 && ScreenHeight !=480) {
        panging = 80;
    }
    if(ScreenWidth != 320 && ScreenHeight !=480){
        panging = 15;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + panging, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}


///实时监控textfield的字数限制;
-(void)textFieldEditChanged:(NSNotification *)noti{
    
    UITextField *field = (UITextField *)noti.object;
    NSString *toBeString = field.text;
    
    //获取高亮状态
    UITextRange *selectRange = [field markedTextRange];
    UITextPosition *position = [field positionFromPosition:selectRange.start offset:0];
    
    //没有高亮选择的字，则对已经输入的文字进行字数统计和限制
    if (!position) {
        if (toBeString.length > 30) {
            [[[UIAlertView alloc]initWithTitle:@"提示" message:@"最长不超过30个字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
            [self.view endEditing:YES];
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:30];
            if (rangeIndex.length == 1) {
                field.text = [toBeString substringToIndex:30];
            }
            else{
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, 30)];
                field.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}


-(void)tapClick{

    
    [self selectPhotos];
    
}


-(void)Click{

    [[[UIAlertView alloc]initWithTitle:@"提示" message:@"图片上传中，稍等修改" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
