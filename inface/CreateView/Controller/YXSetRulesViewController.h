//
//  YXSetRulesViewController.h
//  inface
//
//  Created by lizhen on 15/10/30.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayOfCreate.h"

@interface YXSetRulesViewController : BaseViewController<UITextViewDelegate,UIScrollViewDelegate>

-(void)YXPlayintroductionPlayOfModel:(PlayOfCreate *)playOfModel;
@end
