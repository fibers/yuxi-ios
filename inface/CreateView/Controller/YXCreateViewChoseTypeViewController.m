//
//  YXCreateViewChoseTypeViewController.m
//  inface
//
//  Created by 邢程 on 15/11/2.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXCreateViewChoseTypeViewController.h"
#import "YXCustomMenuView.h"
#import "CustomMenuModel.h"
#import "MBProgressHUD+YXStyle.h"
#import "StoryCategory.h"
#import "YXButton.h"
@interface YXCreateViewChoseTypeViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (strong,nonatomic) UITableView      * tableView;
@property (strong,nonatomic) StoryCategory    * category;
@property (strong,nonatomic) YXCustomMenuView * firstGroupMenuView;
@property (strong,nonatomic) YXCustomMenuView * secendGroupMenuView;
@property (strong,nonatomic) UIButton * bottomButton;
@end

@implementation YXCreateViewChoseTypeViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self loadData];
        [self.view setBackgroundColor:[UIColor whiteColor]];
    }
    return self;
}
-(UIButton*)bottomButton{
    if (!_bottomButton) {
        _bottomButton = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight-40, ScreenWidth, 40)];
        [_bottomButton addTarget:self action:@selector(okBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomButton setBackgroundColor:[UIColor colorWithHexString:@"f8b1d1"]];
        [_bottomButton setTitle:@"确定" forState:UIControlStateNormal];
        [_bottomButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    return _bottomButton;
}
-(void)okBtnClicked:(UIButton*)btn{
    NSArray * arr = self.firstGroupMenuView.selectedButtons;
    NSArray * arr1 = self.secendGroupMenuView.selectedButtons;
    NSMutableArray * contentArrM = [NSMutableArray array];
    if(!arr){
        [[[UIAlertView alloc]initWithTitle:@"提示" message:@"至少选择一项类别" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
        return;
    }
    [arr enumerateObjectsUsingBlock:^(YXButton*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [contentArrM addObject:obj.model];
    }];
    if(arr1){
        [arr1 enumerateObjectsUsingBlock:^(YXButton*   _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [contentArrM addObject:obj.model];
        }];
    }
    [contentArrM enumerateObjectsUsingBlock:^(CustomMenuModel*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        LOG(@"%@",obj.name);
    }];
    if([self.delegate respondsToSelector:@selector(yxCreateViewChoseTypeViewSelectedContent:)]){
        [self.delegate yxCreateViewChoseTypeViewSelectedContent:contentArrM];
    }
    [self dismissViewControllerAnimated:YES completion:^{
       
    }];
}
-(void)loadData{
    MBProgressHUD * hud = [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    [HttpTool postWithPath:@"GetStoryCategory" params:@{@"uid":USERID,@"token":@"110"} success:^(id JSON) {
        [hud hide:YES];
        StoryCategory *category = [StoryCategory objectWithKeyValues:JSON];
        _category = category;
        [self.view addSubview:self.tableView];
        [self.view addSubview:self.bottomButton];
        
    } failure:^(NSError *error) {
        [hud hide:YES];
        UIAlertView * alertV = [[UIAlertView alloc]initWithTitle:@"网络错误" message:@"您的网络环境不好，是否重试？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"重试", nil];
        alertV.tag = 666;
        [alertV show];
        
    }];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 666) {
        if (buttonIndex == 0) {
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        }
        if (buttonIndex == 1) {
            [self loadData];
        }
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 33;
            break;
        case 1:
            return 50;
            break;
        case 2:
            return 50;
            break;
            
        default:
            return 0;
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 20;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 1;
            break;
            
        default:
            return 0;
            break;
    }
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            UILabel * titleL = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 30)];
            [titleL setText:@"选择类别"];
            [titleL setTextAlignment:NSTextAlignmentCenter];
            [titleL setFont:[UIFont systemFontOfSize:17]];
            [titleL setTextColor:[UIColor colorWithHexString:@"656464"]];
            return titleL;
        }
            break;
        case 1:
            return [self headerViewWithTitleStr:@"类别"];
            break;
        case 2:
            return [self headerViewWithTitleStr:@"类目"];
            break;
            
        default:
            return [[UIView alloc]init];;
            break;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *bgV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 20)];
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(14, 0, 300, 18 )];
    [label setTextColor:[UIColor colorWithHexString:@"949494"]];
    [bgV addSubview:label];
    switch (section) {
        case 0:
            return [[UIView alloc]init];
            break;
        case 1:
            [label setText:@"只能选一项哦"];
            return bgV;
            break;
        case 2:
            [label setText:@"可以选两项哦"];
            return bgV;
            break;
            
        default:
            return [[UIView alloc]init];;
            break;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 1:
            return self.firstGroupMenuView.viewH;
            break;
        case 2:
            return self.secendGroupMenuView.viewH;
            break;
        default:
            break;
    }
    return 0;
}
-(YXCustomMenuView *)firstGroupMenuView{
    if(!_firstGroupMenuView){
        YXCustomMenuView * mv = [[YXCustomMenuView alloc]init];
        NSMutableArray *arrM = [[NSMutableArray alloc]init];
        for(int i = 0 ;i<(([self.category.list count]>2)?2:[self.category.list count]);i++){
            CustomMenuModel * model  = [self.category.list objectAtIndex:i];
            model.isSelected = NO;
            [arrM addObject:model];
        }
        [mv setMenusWithCustomMenuModelArr:arrM];
        [mv setMaxSelectedBtnsCount:1 andShouldShowAlertView:YES];
        mv.frame = CGRectMake(0, 0, ScreenWidth, mv.viewH);
        _firstGroupMenuView = mv;
    }
    return _firstGroupMenuView;
}

-(YXCustomMenuView *)secendGroupMenuView{
    if(!_secendGroupMenuView){
        YXCustomMenuView * mv = [[YXCustomMenuView alloc]init];
        NSMutableArray *arrM = [[NSMutableArray alloc]init];
        for(int i = 2 ;i<[self.category.list count];i++){
            
            CustomMenuModel * model  = [self.category.list objectAtIndex:i];
            model.isSelected = NO;
            [arrM addObject:model];
        }
        [mv setMenusWithCustomMenuModelArr:arrM];
        [mv setMaxSelectedBtnsCount:2 andShouldShowAlertView:YES];
        mv.frame = CGRectMake(0, 0, ScreenWidth, mv.viewH);
        _secendGroupMenuView = mv;
    }
    return _secendGroupMenuView;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
             return [[UITableViewCell alloc]init];
            break;
        case 1:{
            UITableViewCell * cell = [[UITableViewCell alloc]init];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell addSubview:self.firstGroupMenuView];
            return cell;
        }
            break;
        case 2:{
            UITableViewCell * cell = [[UITableViewCell alloc]init];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell addSubview:self.secendGroupMenuView];
            return cell;
        }
            break;
        default:
            break;
    }
    return [[UITableViewCell alloc]init];
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 20, ScreenWidth, ScreenHeight-20)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}
-(UIView *)headerViewWithTitleStr:(NSString *)title{
    UIView * bgV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 50)];
    [bgV setBackgroundColor:[UIColor whiteColor]];
    UILabel * titleL = [[UILabel alloc]init];
    titleL.frame = CGRectMake(16, 17, 300, 15);
    [titleL setText:title];
    [bgV addSubview:titleL];
    return bgV;
}
@end
