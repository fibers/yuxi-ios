//
//  YXCreateViewChoseTypeViewController.h
//  inface
//
//  Created by 邢程 on 15/11/2.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol YXCreateViewChoseTypeViewControllerDelegate <NSObject>
/**
 *  被选择的选项。
 *
 *  @param arr <CustomMenuModel> CustomMenuModel的数组
 */
-(void)yxCreateViewChoseTypeViewSelectedContent:(NSArray *)arr;
@end
@interface YXCreateViewChoseTypeViewController : UIViewController
@property(weak,nonatomic)id<YXCreateViewChoseTypeViewControllerDelegate>delegate;
@end