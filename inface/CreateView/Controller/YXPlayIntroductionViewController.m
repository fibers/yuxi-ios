//
//  YXPlayIntroductionViewController.m
//  inface
//
//  Created by lizhen on 15/10/30.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXPlayIntroductionViewController.h"
#import "YXProgressView.h"
#import "YXSummayViewController.h"
#import "YXFrontCoverViewController.h"
#import "YXSummayViewController.h"

@interface YXPlayIntroductionViewController ()

@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UILabel *lab;
@property (nonatomic,strong)UITextView *textView;

@property (nonatomic,strong)PlayOfCreate *model;
@end

@implementation YXPlayIntroductionViewController

-(void)YXPlayintroductionPlayOfCreateModel:(PlayOfCreate *)playOfModel{

    _model = [[PlayOfCreate alloc]init];
    _model = playOfModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self createLeftButton];
    [self createScrollView];
    [self createTextView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DidChange:) name:UITextViewTextDidChangeNotification object:_textView];
}

-(void)createLeftButton{
    
    self.navigationItem.title = @"剧简介";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#656464"],NSFontAttributeName:[UIFont systemFontOfSize:17.0]}];
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    LeftButton.tag = 10;
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _rightButton.frame = CGRectMake(0, 0, 48, 24);
    [_rightButton setTitle:@"下一步" forState:normal];
    _rightButton.titleLabel.font = [UIFont systemFontOfSize:12.0];
    _rightButton.tag = 11;
    [_rightButton setTitleColor:[UIColor colorWithHexString:@"#9b9b9b"] forState:normal];
    _rightButton.layer.cornerRadius = 4.0;
    _rightButton.layer.borderWidth = 1.0;
    _rightButton.layer.borderColor = [[UIColor colorWithHexString:@"#9b9b9b"]CGColor];
    
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_rightButton];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    
}

//左上角按钮
-(void)returnBackl:(UIButton *)btn{
    if (btn.tag == 10) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        //YXFrontCoverViewController *vc = [[YXFrontCoverViewController alloc]init];
        YXSummayViewController *vc = [[YXSummayViewController alloc]init];
        _model.summaryOfPlay = _textView.text;
        [vc YXPlayintroductionPlayOfModel:_model];
        [_textView endEditing:YES];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)DidChange:(NSNotification*)noti{
    
    if (self.textView.text.length > 0) {
        _rightButton.layer.borderWidth = 0;
        [_rightButton setTitleColor:[UIColor whiteColor] forState:normal];
        _rightButton.backgroundColor = [UIColor colorWithHexString:@"#7fb0e9"];
        [_rightButton setEnabled:YES];
    }
    else{
        _rightButton.layer.borderWidth = 1.0;
        [_rightButton setTitleColor:[UIColor colorWithHexString:@"#9b9b9b"] forState:normal];
        _rightButton.backgroundColor = [UIColor clearColor];
        [_rightButton setEnabled:NO];
    }
    [_rightButton addTarget:self action:@selector(returnBackl:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)createScrollView{
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [self.view addSubview:_scrollView];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(ScreenWidth, ScreenWidth==320?ScreenHeight+100:ScreenHeight);
    [_scrollView setShowsVerticalScrollIndicator:NO];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [_scrollView addGestureRecognizer:tap];
    
}

-(void)createTextView{
    ///TextView的高度
    CGFloat textView_H;
    if (ScreenHeight > 700) {
        textView_H = 280.0;
    }else if(ScreenHeight > 650){
        textView_H = 250.0;
    }else{
        textView_H = 200.0;
    }
    if (ScreenHeight == 480) {
        textView_H = 170;
    }
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(15, 6, ScreenWidth-15, textView_H)];
    [_scrollView addSubview:_textView];
    _textView.placeholder = @"必填。请输入剧简介，一句话概括剧情内容";
    _textView.font = [UIFont systemFontOfSize:14.0];
    _textView.delegate = self;
    
    UILabel *lineLab = [[UILabel alloc]initWithFrame:CGRectMake(0, textView_H+20, ScreenWidth, 1)];
    lineLab.backgroundColor = [UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1];
    [_scrollView addSubview:lineLab];

    YXProgressView *progressView = [[YXProgressView alloc]init];
    UIView *progress =[progressView getCurrentPage:2 andCurrentViewOfY:_textView.frame.origin.y+textView_H+220];
    [_scrollView addSubview:progress];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//判断内容大小，多出去的部分进行自动截取
- (void)textViewDidChange:(UITextView *)textView {
    NSInteger number = [textView.text length];

    if (number >= 500) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"简介最长不超过500个字" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        textView.text = [textView.text substringToIndex:500];
    }
}

-(void)tapClick{

    [_textView endEditing:YES];

}


-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    CGFloat panging;
    if (ScreenHeight == 480 && ScreenWidth == 320 ) {
        panging = 10;
    }
    if (ScreenWidth == 320 && ScreenHeight !=480) {
        panging = 10;
    }
    if(ScreenWidth != 320 && ScreenHeight !=480){
        panging = 0;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - panging, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    CGFloat panging;
    if (ScreenHeight == 480 && ScreenWidth == 320 ) {
        panging = 10;
    }
    if (ScreenWidth == 320 && ScreenHeight !=480) {
        panging = 10;
    }
    if(ScreenWidth != 320 && ScreenHeight !=480){
        panging = 0;
    }
    
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + panging, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}





@end
