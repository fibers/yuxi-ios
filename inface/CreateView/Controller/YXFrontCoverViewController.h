//
//  YXFrontCoverViewController.h
//  inface
//
//  Created by lizhen on 15/11/2.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "YXCreateViewChoseTypeViewController.h"

@interface YXFrontCoverViewController : BaseViewController<UIScrollViewDelegate,UzysAssetsPickerControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,YXCreateViewChoseTypeViewControllerDelegate,UIGestureRecognizerDelegate>


@end
