//
//  CreateFamilyViewController.m
//  inface
//
//  Created by Mac on 15/5/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "CreateFamilyViewController.h"
#import "UpYun.h"
#import "CreatKindsOfModel.h"
#import "GroupEntity.h"
#import "DistinguishFamilyAndStory.h"
#import "MBProgressHUD.h"

#define MaxNumberOfDescriptionChars  800//textview最多输入的字数
@interface CreateFamilyViewController ()
{
    UpYun * _upYun;
    UILabel * placeLable;
}
@property(nonatomic,copy)MBProgressHUD * hud;

@end

@implementation CreateFamilyViewController

-(MBProgressHUD *)hud{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc]initWithView:self.view];
    }
    return _hud;
}


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    RightButtonCreate.userInteractionEnabled=NO;
    if ([self.chatInputTextField.text isEmpty]) {
        RightButtonCreate.userInteractionEnabled=YES;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入名称" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([self.chatInputTextView.text isEmpty]) {
        RightButtonCreate.userInteractionEnabled=YES;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入简介" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([self.imgSerUrl isEmpty]) {
        RightButtonCreate.userInteractionEnabled=YES;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请上传图片" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if (self.chatInputTextField.text.length > 15)
    {
        self.chatInputTextField.text = [self.chatInputTextField.text substringToIndex:15];
    }
    

    NSString * user_userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];
    __block NSString *groupID;
    //创建群
    DDCreateGroupAPI * createGroup = [[DDCreateGroupAPI alloc]init];
    [self.hud setLabelText:@"数据请求中..."];
    [self.hud setMode:MBProgressHUDModeIndeterminate];
    [self.view addSubview:self.hud];
    [self.hud show:YES];
    [self.view bringSubviewToFront:self.hud];

    NSArray * object = @[self.chatInputTextField.text,@"",@[user_userid]];
    [createGroup requestWithObject:object Completion:^(GroupEntity *response, NSError *error) {
        if (!error) {
            groupID=response.objID;
            
            if ([groupID isEmpty]) {
                return ;
            }
  
            NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            NSString * group_id;
            NSArray * ids = [groupID componentsSeparatedByString:@"_"];
            if (ids[1]) {
                group_id = ids[1];
            }else{
                group_id = ids[0];
            }
            //创建家族
            NSDictionary * params;
            //创建成功，关联家族Id，增加字段
            if (self.CodeTextField.text &&![self.CodeTextField.text isEqualToString:@""]) {
                params = @{@"uid":userid,@"familyname":self.chatInputTextField.text,@"portrait":self.imgSerUrl,@"vipcode":self.CodeTextField.text, @"familyintro":self.chatInputTextView.text,@"groupid":group_id,@"token":@"110"};
            }else{
                  params = @{@"uid":userid,@"familyname":self.chatInputTextField.text,@"portrait":self.imgSerUrl, @"familyintro":self.chatInputTextView.text,@"groupid":group_id,@"token":@"110"};
            }
            [HttpTool postWithPath:@"NewFamily" params:params success:^(id JSON) {
                RightButtonCreate.userInteractionEnabled=YES;
                [self.hud setHidden:YES];
                if ([[JSON objectForKey:@"result"]integerValue]==1) {
                    
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"创建成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alertview show];
               
                    [[MyFamilyViewController shareInstance]getMyFamilies];
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }else{
                    NSString * describe = [JSON objectForKey:@"result"];
                    if ([describe isEqualToString:@"2"]) {
                        //特权码失败
                        self.CodeTextField.text  = nil;
                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:@"请重新输入暗号并创建家族" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertview show];

                    }
                    if ([describe isEqualToString:@"3"]) {
                        //重名
                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"家族名已经存在" message:@"请重新输入家族名称并创建家族" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertview show];

                        self.chatInputTextField.text = nil;
                    }
                    if ([describe isEqualToString:@"0"]) {
                        //重名
                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:@"请重新创建家族" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertview show];
                        
                        self.chatInputTextField.text = nil;
                    }

                    
                }
                
            } failure:^(NSError *error) {
                RightButtonCreate.userInteractionEnabled=YES;

                [self.hud setLabelText:@"创建失败"];
                [self.hud setMode:MBProgressHUDModeText];
                [self.hud hide:YES afterDelay:2];
            }];
            
        }else{
            RightButtonCreate.userInteractionEnabled=YES;
            [[DDTcpClientManager instance]reconnect];

            [self.hud setLabelText:@"创建失败"];
            [self.hud setMode:MBProgressHUDModeText];
            [self.hud hide:YES afterDelay:2];
  
        }
    }];
}





- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor=[UIColor whiteColor];
    
    //左上角按钮
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 45, 44)];
    [LeftButton setTitle:@"取消" forState:UIControlStateNormal];
    LeftButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [LeftButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"完成" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    RightButtonCreate=RightButton;
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"创建家族";
    
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInputTextView.textColor=[UIColor blackColor];
    
    if (!placeLable) {
        placeLable = [[UILabel alloc]initWithFrame:CGRectMake(3, 8, 150, 20)];
        
    }
    placeLable.font = [UIFont systemFontOfSize:14];
    
    placeLable.textColor=COLOR(204, 204, 204, 1);
    
    placeLable.text = @"请输入家族简介";
    
    [self.chatInputTextView addSubview:placeLable];
    
    self.chatInputTextField.delegate = self;
    self.chatInputTextField.textColor=GRAY_COLOR;
    
    self.countLabel.textColor=GRAY_COLOR;
    
    self.XianLabel.backgroundColor=XIAN_COLOR;
    self.XianLabel1.backgroundColor=XIAN_COLOR;
    
    self.PromptLabel.textColor=BLUECOLOR;
    
    //添加照片视图85*85
    //图片
    UIImageView *aimageView=[[UIImageView alloc]initWithFrame:CGRectMake(12, 250, 135, 100)];
    aimageViewPhoto=aimageView;
    aimageView.contentMode = UIViewContentModeScaleAspectFit;
    aimageView.backgroundColor=[UIColor clearColor];
    aimageView.image=[UIImage imageNamed:@"add-photo640"];
    aimageView.userInteractionEnabled=YES;
    [self.view addSubview:aimageView];
    imagepicker = [[UIImagePickerController alloc]init];
    UITapGestureRecognizer *singleTapa = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(paizhao:)];
    
    [aimageView addGestureRecognizer:singleTapa];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [[self view] addGestureRecognizer:recognizer];
    self.imgSerUrl = @"";
    self.circleView = [[EVCircularProgressView alloc]initWithFrame:CGRectMake(36, 31, 28, 28)];
    [aimageViewPhoto addSubview:self.circleView];
    self.circleView.hidden = YES;
}


#pragma mark 增减下滑手势
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gesture
{
    if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
        [self.chatInputTextView resignFirstResponder];
        [self.chatInputTextField resignFirstResponder];
    }
}

/*由于联想输入的时候，函数textView:shouldChangeTextInRange:replacementText:无法判断字数，
 因此使用textViewDidChange对TextView里面的字数进行判断
 */
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length > 0) {
        placeLable.hidden = YES;
    }else{
        placeLable.hidden = NO;
        
    }
    //该判断用于联想输入
    if (textView.text.length > MaxNumberOfDescriptionChars)
    {
        textView.text = [textView.text substringToIndex:MaxNumberOfDescriptionChars];
        
    }
    self.countLabel.text=[NSString stringWithFormat:@"%lu",MaxNumberOfDescriptionChars-textView.text.length];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.chatInputTextField resignFirstResponder];
    [self.chatInputTextView resignFirstResponder];
}



- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==self.chatInputTextField) {
        
        if (textField.text.length > 15)
        {
            textField.text = [textField.text substringToIndex:15];
        }
    }
    
}

//
//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if (textField == self.chatInputTextField) {
//        if (range.location > 15) {
//            [[[UIAlertView alloc]initWithTitle:@"提示" message:@"家族名最长不超过15个字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
//            [self.view endEditing:YES];
//            self.chatInputTextField.text = [self.chatInputTextField.text substringToIndex:15];
//            return NO;
//        }else{
//            return YES;
//        }
//        
//    }
//    return YES;
//}


-(void) paizhao:(UITapGestureRecognizer*) recognizer
{

    BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    //如果支持相机，那么多个选项
    if (isCameraSupport) {
        UIActionSheet* mySheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"挑照片" otherButtonTitles:@"现在拍", nil];
        [mySheet showInView:self.view];
        
    }
    else {
        [self startPhotoChooser];
    }

}


- (void)uzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    
    
    if([[assets[0] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
    {

        [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            ALAsset *representation = obj;
            
            UIImage * original = [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                                     scale:representation.defaultRepresentation.scale
                                               orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
            
            aimageViewPhoto.image=original;
            
            
            CGSize  imgSize = CGSizeMake(2210, 1280);
            
            original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
            [self getServerKey :original];
            
        }];

    }
    else //Video
    {
        
        
    }
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

#pragma mark 从又拍获取图片字符串
-(void)getServerKey:(UIImage *)image
{
//    [SVProgressHUD showWithStatus:@"上传中" maskType:SVProgressHUDMaskTypeBlack];
    //此处需要换成又拍的云存储
    _upYun = [[UpYun alloc]init];
    [_upYun uploadImage:image];
    [self.circleView setHidden:NO];
    
    self.circleView.center = CGPointMake(aimageViewPhoto.center.x -10, aimageViewPhoto.center.y - 250);
    
    [self.circleView setProgress:0.0 animated:YES];
    self.circleView.alpha = 1;
    [self.circleView commonInit];
    [aimageViewPhoto addSubview:self.circleView];
    [aimageViewPhoto bringSubviewToFront:self.circleView];

    //上传图片成功
    WEAKSELF
    _upYun.successBlocker = ^(id data){
        if (!weakSelf) {
            return ;
        }
        NSString        *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
        imgUrl  =   [NSString stringWithFormat:@"%@%@", IMAGE_SERVER_BASE_URL, imgUrl];
        [weakSelf.circleView setHidden:YES];
   
        weakSelf.imgSerUrl = imgUrl;
        [SVProgressHUD dismiss];

    };
    _upYun.failBlocker = ^(NSError * error)
    {
        //上传又拍失败的操作
        [weakSelf.circleView setHidden:YES];

        [SVProgressHUD showErrorWithStatus:@"上传失败" duration:2.0];
    };
    _upYun.progressBlocker = ^(CGFloat percent,long long requestDidSendBytes)
    {
        [ weakSelf.circleView setProgress:percent animated:YES];
    };
}




#pragma mark actionSheet Delegate

- (void) startPhotoChooser
{
    UzysAssetsPickerController *picker = [[UzysAssetsPickerController alloc] init];
    picker.delegate = self;
    
    picker.maximumNumberOfSelectionPhoto = 1;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{

    //choose photo
    if (buttonIndex == 0) {
        
        [self startPhotoChooser];
        
    } else if (buttonIndex == 1) {
        //take photo.
        //指定使用照相机模式,可以指定使用相册／照片库
        imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        //设置当拍照完或在相册选完照片后，是否跳到编辑模式进行图片剪裁。只有当showsCameraControls属性为true时才有效果
        imagepicker.allowsEditing = NO;
        //设置拍照时的下方的工具栏是否显示，如果需要自定义拍摄界面，则可把该工具栏隐藏
        imagepicker.showsCameraControls  = YES;
        //设置使用后置摄像头，可以使用前置摄像头
        imagepicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        //设置闪光灯模式
        /*
         typedef NS_ENUM(NSInteger, UIImagePickerControllerCameraFlashMode) {
         UIImagePickerControllerCameraFlashModeOff  = -1,
         UIImagePickerControllerCameraFlashModeAuto = 0,
         UIImagePickerControllerCameraFlashModeOn   = 1
         };
         */
        imagepicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        //设置相机支持的类型，拍照和录像
        imagepicker.mediaTypes = @[(NSString*)kUTTypeImage];
        //设置拍摄时屏幕的view的transform属性，可以实现旋转，缩放功能
        // imagepicker.cameraViewTransform = CGAffineTransformMakeRotation(M_PI*45/180);
        // imagepicker.cameraViewTransform = CGAffineTransformMakeScale(1.5, 1.5);
        //所有含有cameraXXX的属性都必须要sourceType是UIImagePickerControllerSourceTypeCamera时设置才有效果，否则会有异常
        //设置UIImagePickerController的代理
        imagepicker.delegate = self;
        [self presentViewController:imagepicker animated:YES completion:^{
            
        }];
        
    }

    
}

#pragma take photos image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
    NSString* type = [info objectForKey:UIImagePickerControllerMediaType];
    //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    if ([type isEqualToString:(NSString*)kUTTypeImage]&&picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
        //获取照片的原图
        UIImage* original = [info objectForKey:UIImagePickerControllerOriginalImage];

        aimageViewPhoto.image=original;
        
        CGSize  imgSize = CGSizeMake(2210, 1280);
        
        original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
        [self getServerKey :original];
        
        //如果是拍照的照片，则需要手动保存到本地，系统不会自动保存拍照成功后的照片
        UIImageWriteToSavedPhotosAlbum(original, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
    }else{
        
    }
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//取消照相机的回调
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//保存照片成功后的回调
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo{
    
}
//add for photo functions end




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
