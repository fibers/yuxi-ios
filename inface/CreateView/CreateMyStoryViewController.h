//
//  CreateMyStoryViewController.h
//  inface
//
//  Created by Mac on 15/5/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "UzysAssetsPickerController.h"
#import "EVCircularProgressView.h"
#import "UpYun.h"
#import <UITextView+Placeholder.h>
@interface CreateMyStoryViewController : BaseViewController<UITextViewDelegate,UITextFieldDelegate,UzysAssetsPickerControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIAlertViewDelegate>
{
    UIImagePickerController    *imagepicker;
    UIImageView *aimageViewPhoto;
    UpYun * _upYun;
    UIButton *RightButtonCreate;
}

@property (weak, nonatomic) IBOutlet UITextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property (strong,nonatomic)  NSString * imgSerUrl;//图片又拍地址
@property (strong,nonatomic)  EVCircularProgressView * circleView;
@property (weak, nonatomic) IBOutlet UILabel *XianLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatInPutTextHeight;

@property (nonatomic,assign)BOOL   isModifyStory;//是否是编辑自戏
@property (strong,nonatomic) NSDictionary * storyDic;//自戏的详情
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeight;
@property (assign,nonatomic)BOOL   isAnimating;

@end
