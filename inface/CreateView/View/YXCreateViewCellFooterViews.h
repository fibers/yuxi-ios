//
//  YXCreateViewCellFooterViews.h
//  inface
//
//  Created by 邢程 on 15/10/29.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXCreateViewCellFooterViews : UIView
///不想注释
-(void)setContent:(NSString *)str;
///不用写注释
-(CGFloat)viewH;
-(void)hideXuXian:(BOOL)isHidden;
@end
