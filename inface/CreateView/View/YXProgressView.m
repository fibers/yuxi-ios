//
//  YXProgressView.m
//  inface
//
//  Created by lizhen on 15/10/30.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXProgressView.h"

@implementation YXProgressView

-(UIView *)getCurrentPage:(NSInteger)currentPage andCurrentViewOfY:(CGFloat)Y{

    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(15, Y, ScreenWidth-30, 6)];
    [self addSubview:view];
    view.layer.masksToBounds = YES;
    view.layer.cornerRadius = 4.0;
    view.layer.borderWidth = 1.0;
    view.layer.borderColor = [[UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1]CGColor];
    
    UIView *view_other = [[UIView alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width/6*currentPage, 6)];
    view_other.backgroundColor = [UIColor colorWithRed:0.92 green:0.44 blue:0.66 alpha:1];
    [view addSubview:view_other];
    
    for (NSInteger i = 1; i < 6; i++) {
        UIView *blockView = [[UIView alloc]init];
        blockView.frame = CGRectMake((view.frame.size.width/6)*i,0, 2, 6);
        blockView.backgroundColor = [UIColor colorWithRed:0.76 green:0.79 blue:0.78 alpha:1];
        [view addSubview:blockView];
    }
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view_other.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft cornerRadii:CGSizeMake(4.0, 4.0)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view_other.bounds;
    maskLayer.path = maskPath.CGPath;
    view_other.layer.mask = maskLayer;
    
    return view;
}

@end
