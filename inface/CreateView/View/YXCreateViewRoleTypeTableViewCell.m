//
//  YXCreateViewRoleTypeTableViewCell.m
//  inface
//
//  Created by 邢程 on 15/10/29.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXCreateViewRoleTypeTableViewCell.h"
#import "RolesTypeItem.h"
#import <UITextView+Placeholder.h>
@interface YXCreateViewRoleTypeTableViewCell()<UITextFieldDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *roleName;
@property (weak, nonatomic) IBOutlet UITextField *roleCount;
@property (weak, nonatomic) IBOutlet UITextView *roleDesc;
@property (strong,nonatomic)RolesTypeItem * rolesTypeItem;
@end
@implementation YXCreateViewRoleTypeTableViewCell
- (IBAction)addRoleCount:(id)sender {
    NSInteger roleCountNum = [self.roleCount.text integerValue]>0?[self.roleCount.text integerValue]+1:1;
    self.roleCount.text = [NSString stringWithFormat:@"%ld",roleCountNum];
    self.rolesTypeItem.roleNum = roleCountNum;
    
}
- (IBAction)subRoleCout:(id)sender {
    NSInteger roleCountNum = [self.roleCount.text integerValue]>0?[self.roleCount.text integerValue]-1:0;
    self.roleCount.text = [NSString stringWithFormat:@"%ld",roleCountNum];
    self.rolesTypeItem.roleNum = roleCountNum;
    if (roleCountNum == 0) {
        if ([self.delegate respondsToSelector:@selector(yxCreateViewRoleTypeTableViewCellCountZero:)]) {
            [self.delegate yxCreateViewRoleTypeTableViewCellCountZero:self.rolesTypeItem];
        }
    }
}
#pragma mark -
#pragma mark textField 代理

- (void)awakeFromNib {
    self.roleDesc.delegate  = self;
    self.roleName.delegate  = self;
    self.roleCount.delegate = self;
    [self.roleDesc setPlaceholder:@"选填。设定角色身世背景"];
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == 666) {
        self.rolesTypeItem.roleName = textField.text;
    }
    if(textField.tag == 999){
        textField.text = [NSString stringWithFormat:@"%ld",[textField.text integerValue]];
    }
    
}
-(void)textViewDidChange:(UITextView *)textView{
    if([textView.text charLength] >500){
        self.rolesTypeItem.roleDesc = textView.text;
        [textView setTextColor:[UIColor redColor]];
        self.rolesTypeItem.isMoreThanWordCount = YES;
    }else{
        self.rolesTypeItem.roleDesc = textView.text;
        [textView setTextColor:[UIColor blackColor]];
        self.rolesTypeItem.isMoreThanWordCount = NO;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    if ([textField.text length] == 0) {
        NSNotification *notification =[NSNotification notificationWithName:@"styleOfPlayerChange" object:nil userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotification:notification];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
-(void)setContentWithRoleTypeItemModel:(RolesTypeItem*)typeItem{
    if ([typeItem.roleDesc charLength]>500) {
        [self.roleDesc setTextColor:[UIColor redColor]];
    }else{
        [self.roleDesc setTextColor:[UIColor blackColor]];
    }
    _rolesTypeItem = typeItem;
    [self.roleName setText:((typeItem.roleName == nil)?@"":typeItem.roleName)];
    [self.roleCount setText:[NSString stringWithFormat:@"%ld",typeItem.roleNum]];
    [self.roleDesc setText:typeItem.roleDesc==nil?@"":typeItem.roleDesc];

}
@end
