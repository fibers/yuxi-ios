//
//  YXProgressView.h
//  inface
//
//  Created by lizhen on 15/10/30.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXProgressView : UIView

/**
 *  进度条
 *
 *  @param currentPage 当前页
 *  @param Y           Y值
 *
 *  @return 进度条
 */
-(UIView *)getCurrentPage:(NSInteger)currentPage andCurrentViewOfY:(CGFloat)Y;

@end
