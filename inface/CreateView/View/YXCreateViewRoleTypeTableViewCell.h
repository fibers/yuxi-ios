//
//  YXCreateViewRoleTypeTableViewCell.h
//  inface
//
//  Created by 邢程 on 15/10/29.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RolesTypeItem;
@protocol YXCreateViewRoleTypeTableViewCellDelegate <NSObject>
@optional
/**
 *  角色数量为0的时候调用该方法
 *
 *  @param item model
 */
-(void)yxCreateViewRoleTypeTableViewCellCountZero:(RolesTypeItem*)item;
@end
@interface YXCreateViewRoleTypeTableViewCell : UITableViewCell
-(void)setContentWithRoleTypeItemModel:(RolesTypeItem*)typeItem;
@property(nonatomic,weak)id<YXCreateViewRoleTypeTableViewCellDelegate>delegate;
@end
