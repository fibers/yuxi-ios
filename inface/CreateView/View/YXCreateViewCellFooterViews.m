//
//  YXCreateViewCellFooterViews.m
//  inface
//
//  Created by 邢程 on 15/10/29.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXCreateViewCellFooterViews.h"

@interface YXCreateViewCellFooterViews()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelCH;
@property (weak, nonatomic) IBOutlet UIImageView *xuxianImgV;
@property (assign,nonatomic)CGFloat viewH;
@end
@implementation YXCreateViewCellFooterViews
-(void)hideXuXian:(BOOL)isHidden{
    [self.xuxianImgV setHidden:isHidden];
}
-(instancetype)init{
    if ((self = [[[NSBundle mainBundle]loadNibNamed:@"YXCreateViewCellFooterViews" owner:nil options:nil]lastObject])) {
    }
    return self;
}
-(void)setContent:(NSString *)str{
    [self.label setText:str];
    CGFloat textW = [str sizeWithAttributes:@{NSFontAttributeName:self.label.font}].width;
    int row = textW>(ScreenWidth - 30)?ceil(textW/(ScreenWidth - 30)):1;
    self.labelCH.constant = 16*row;
    _viewH = 33 + 16*(row-1);
}
-(CGFloat)viewH{
    return _viewH;
}
@end
