//
//  CreateTopicViewController.h
//  inface
//
//  Created by huangzengsong on 15/6/30.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "UzysAssetsPickerController.h"
#import "UpYun.h"
@interface CreateTopicViewController : BaseViewController<UITextViewDelegate,UITextFieldDelegate,UzysAssetsPickerControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIImagePickerController    *imagepicker;
    UIImageView *aimageViewPhoto;
    UpYun * _upYun;
    UIButton *RightButtonCreate;
}

@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property (strong,nonatomic)  NSString * imgSerUrl;//图片又拍地址
@property (strong,nonatomic)  EVCircularProgressView * circleView;
@property (weak, nonatomic) IBOutlet UILabel *XianLabel;
@property (assign,nonatomic) BOOL        isAnimating;
@end
