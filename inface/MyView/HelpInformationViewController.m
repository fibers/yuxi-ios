//
//  HelpInformationViewController.m
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "HelpInformationViewController.h"

@interface HelpInformationViewController ()

@end

@implementation HelpInformationViewController

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=self.navTitle;
    
    self.aWebView.delegate=self;
    self.aWebView.scrollView.bounces=NO;//不弹跳
    
    activityIndicatorView = [ [ UIActivityIndicatorView  alloc ]
                             initWithFrame:CGRectMake(ScreenWidth/2.0-15,ScreenHeight/2.0,30.0,30.0)];
    activityIndicatorView.activityIndicatorViewStyle= UIActivityIndicatorViewStyleGray;
    [self.view addSubview:activityIndicatorView ];
    
    
    for (UIView *aView in [self.aWebView subviews])
    {
        if ([aView isKindOfClass:[UIScrollView class]])
        {
            //            [(UIScrollView *)aView setShowsVerticalScrollIndicator:NO]; //右侧的滚动条 （水平的类似）
            [(UIScrollView *)aView setShowsHorizontalScrollIndicator:NO]; //下侧的滚动条 （水平的类似）
            for (UIView *shadowView in aView.subviews)
            {
                if ([shadowView isKindOfClass:[UIImageView class]])
                {
                    shadowView.hidden = YES;  //上下滚动出边界时的黑色的图片 也就是拖拽后的上下阴影
                }
            }
        }
    }
    NSRange  range = [self.contentUrl rangeOfString:@"http://"];
    if (range.length == 0) {
        self.contentUrl = [NSString stringWithFormat:@"http://%@",self.contentUrl];
    }
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self.aWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.contentUrl]]];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [activityIndicatorView startAnimating ];//启动
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [activityIndicatorView stopAnimating ];//停止
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    [activityIndicatorView stopAnimating ];//停止
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    _isAnimating = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
