//
//  OtherPersonRolrView.h
//  inface
//
//  Created by appleone on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface OtherPersonRolrView : BaseViewController
@property(strong,nonatomic)   NSString * friendId;
@property(strong,nonatomic)   NSString * thearterId;
@property(strong,nonatomic)   NSString * portrait;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property(strong,nonatomic)   NSString * groupId;
@property(assign,nonatomic)   BOOL     isChatInCheckGroup;
@property(assign,nonatomic)   BOOL     isAnimating;
@end
