//
//  MyCenterFooterTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/7/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MyCenterFooterTableViewCell.h"

@implementation MyCenterFooterTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.TmpupLabel.textColor=GRAY_COLOR;
    self.TmpdownLabel.textColor=GRAY_COLOR;
    
    
    self.WriteBtn.layer.borderWidth = 1;
    self.WriteBtn.layer.borderColor = BLUECOLOR.CGColor;
    self.WriteBtn.layer.cornerRadius = 4;
    self.WriteBtn.clipsToBounds = YES;
    [self.WriteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.WriteBtn setBackgroundColor:BLUECOLOR];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
