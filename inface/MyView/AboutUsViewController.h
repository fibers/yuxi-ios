//
//  AboutUsViewController.h
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsViewController : BaseViewController
@property (assign, nonatomic) IBOutlet UILabel *VersionLabel;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
