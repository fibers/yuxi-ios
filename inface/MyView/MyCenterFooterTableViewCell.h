//
//  MyCenterFooterTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCenterFooterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *TmpupLabel;
@property (weak, nonatomic) IBOutlet UILabel *TmpdownLabel;
@property (weak, nonatomic) IBOutlet UIButton *WriteBtn;

@end
