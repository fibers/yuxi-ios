//
//  MyCenterDetailTableViewCell.m
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MyCenterDetailTableViewCell.h"

@implementation MyCenterDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.BackgroundView.backgroundColor=COLOR(0, 0, 0, 0);
    self.UserImage.layer.cornerRadius = CGRectGetHeight(self.UserImage.bounds)/2;
    self.UserImage.clipsToBounds = YES;
    self.UserImage.layer.borderWidth = 2;
    self.UserImage.layer.borderColor = [UIColor whiteColor].CGColor;

    self.UserDetailLabel.textColor=GRAY_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
