//
//  HelpInforViewController.m
//  inface
//
//  Created by huangzengsong on 15/6/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "HelpInforViewController.h"

@interface HelpInforViewController ()

@end

@implementation HelpInforViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=@"帮助信息";
    

    self.scrollView.userInteractionEnabled=YES;//用户可交互
    
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.delegate=self;
    UIImage *aimage=[UIImage imageNamed:@"helpinfomation.png"];
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*aimage.size.height/aimage.size.width)];
//    imageView.contentMode=UIViewContentModeScaleAspectFill;
//    imageView.clipsToBounds=YES;
    imageView.image=aimage;
    imageView.userInteractionEnabled=YES;
    [self.scrollView addSubview:imageView];
    self.scrollView.contentSize=CGSizeMake(ScreenWidth, ScreenWidth*aimage.size.height/aimage.size.width);//滚动的内容尺寸
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
