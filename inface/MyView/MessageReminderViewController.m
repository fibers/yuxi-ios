//
//  MessageReminderViewController.m
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MessageReminderViewController.h"

@interface MessageReminderViewController ()

@end

@implementation MessageReminderViewController

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    
    self.navigationItem.title=@"新消息提醒";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }

    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    return 1;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *identifier=@"MessageReminderTableViewCell";
    MessageReminderTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"MessageReminderTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }

        cell.UserNameLabel.text=@"新消息声音";
        cell.Switch.tag=1001;
        NSString * state = [[NSUserDefaults standardUserDefaults]objectForKey:@"VOICE"];
            if ([state isEqualToString:@"NO"]) {
                [cell.Switch setOn:NO];
            }else{
                [cell.Switch setOn:YES];
            }
    
    cell.Switch.onTintColor=BLUECOLOR;
    [cell.Switch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    return cell;
    
}

-(void)switchAction:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    NSInteger index=switchButton.tag;
    BOOL isButtonOn = [switchButton isOn];
    if (!isButtonOn) {
        //去关闭
        if (index==1000) {
            //操作接受新消息通知
            
            NSString * userId = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            
            NSDictionary * dic = @{@"uid":userId,@"setting":@"0",@"token":@"110"};
            
            [HttpTool postWithPath:@"NewMessageSetting" params:dic success:^(id JSON) {
                if ([[JSON objectForKey:@"result"]integerValue]==1) {
//                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"设置成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//                    [alertview show];
                    
                }else{
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alertview show];
                }
                
            } failure:^(NSError *error) {
                //网络未连接
            }];
            
        } else if (index==1001) {
            //关闭声音
            [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"VOICE"];
        }

    }else {
        //去打开
        if (index==1000) {
            //操作接受新消息通知
            
            NSString * userId = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            
            
            NSDictionary * dic = @{@"uid":userId,@"setting":@"1",@"token":@"110"};
            
            [HttpTool postWithPath:@"NewMessageSetting" params:dic success:^(id JSON) {
                if ([[JSON objectForKey:@"result"]integerValue]==1) {
//                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"设置成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//                    [alertview show];
                    
                    
                }else{
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alertview show];
                    
                }
                
            } failure:^(NSError *error) {
                //网络未连接
            }];
            

            

        } else if (index==1001) {
    
            //打开声音
            [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"VOICE"];

        }

       
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *aview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 20)];
    aview.backgroundColor=[UIColor clearColor];
    
    return aview;
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 44;
    
    
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
