//
//  AboutUsViewController.m
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
#import "FriendsViewController.h"

#import "AboutUsViewController.h"
#import <objc/runtime.h>
#if DEBUG
#import "FLEXManager.h"
#endif
@interface AboutUsViewController ()
@property (assign,nonatomic) IBOutlet UIImageView *logoImgV;

@end

@implementation AboutUsViewController
- (void)showDebugView
{
#if DEBUG
//    if (tapRecognizer.state == UIGestureRecognizerStateRecognized) {
        // This could also live in a handler for a keyboard shortcut, debug menu item, etc.
        [[FLEXManager sharedManager] showExplorer];
//    }
#endif
}

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    _isAnimating = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UILongPressGestureRecognizer * press = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(showDebugView)];
    [press setMinimumPressDuration:4];
    [self.logoImgV addGestureRecognizer:press];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    
    self.navigationItem.title=@"关于我们";
    
    NSString *nowVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    self.VersionLabel.text=[NSString stringWithFormat:@"语戏%@版本",nowVersion];
//    [self getInstanceVar];
}


-(void)getInstanceVar
{
    
    NSInteger unreadCount;
    
    FriendsViewController * friendView = [[FriendsViewController alloc]init];
    object_getInstanceVariable(friendView,"_unreadCount",(void*)&unreadCount);
    
    NSLog(@"消息未读数:%ld******",unreadCount);
    

}

-(void)propertyNameList
{
    u_int  count;
    
    objc_property_t * propertyLists = class_copyPropertyList([UIViewController class], &count);
    
    for (int i = 0 ; i < count; ++i) {
        const char * propertyName = property_getName(propertyLists[i]);
        
        NSString * strName = [NSString stringWithCString:propertyName encoding:NSUTF8StringEncoding];
        NSLog(@"%@*****",strName);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
