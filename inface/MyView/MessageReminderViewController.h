//
//  MessageReminderViewController.h
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageReminderTableViewCell.h"
@interface MessageReminderViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
