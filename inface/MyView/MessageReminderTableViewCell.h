//
//  MessageReminderTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageReminderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISwitch *Switch;
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@end
