//
//  PersonalSetViewController.h
//  inface
//
//  Created by Mac on 15/4/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FamilyNameTableViewCell.h"
#import "FamilyIDTableViewCell.h"
#import "UzysAssetsPickerController.h"
#import "EditNicknameViewController.h"
#import "EditIntroduceViewController.h"
#import "EVCircularProgressView.h"
#import "CaptureViewController.h"//截图的视图控制器
#import "PassImageDelegate.h"
#import "MyCenter.h"


@interface PersonalSetViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UzysAssetsPickerControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,PassImageDelegate>
{
    UIImagePickerController    *imagepicker;
    UILabel *sexLabel;//性别label
    UIImageView *aimageViewPhoto;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;
@property (strong,nonatomic) NSString * imgSerUrl;
@property (strong,nonatomic) UITableViewCell * tableViewCell;
@property (strong,nonatomic) EVCircularProgressView * circleView;
@property (strong,nonatomic) FamilyNameTableViewCell * familyName;
@property (assign,nonatomic) BOOL  shuaxinImage;
@property(assign,nonatomic)    BOOL   isAnimating;

-(void)setContentWithModel:(MyCenter *)mycenter;
@end
