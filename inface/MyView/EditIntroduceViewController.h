//
//  EditIntroduceViewController.h
//  inface
//
//  Created by huangzengsong on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
@class  MyCenter;
@interface EditIntroduceViewController : BaseViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property(assign,nonatomic)    BOOL   isAnimating;

-(void)setContent:(MyCenter *)myCenter;
@end
