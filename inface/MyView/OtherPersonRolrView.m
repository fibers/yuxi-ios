//
//  OtherPersonRolrView.m
//  inface
//
//  Created by appleone on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "OtherPersonRolrView.h"
#import "OtherPersonRoleCell.h"
#import "YXMyCenterViewController.h"
#import "MsgAddGroupConfirmAPI.h"
@interface OtherPersonRolrView ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSDictionary * roleDic;
@property (assign, nonatomic) float  contentLabelHeight;

@end

@implementation OtherPersonRolrView

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc]init];
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    // Do any additional setup after loading the view from its nib.
    [self getPersonRole];
    self.navigationItem.title = @"TA的名片";
    // Do any additional setup after loading the view from its nib.
    [self showAllKindsOfButton];
}

-(void)returnBackl
{

   [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(void)showAllKindsOfButton
{
    //先判断是否是好友
    NSString * sessionid = [NSString stringWithFormat:@"user_%@",self.friendId];
//     __block  BOOL  isFriend = NO;
//    [[DDUserModule shareInstance]getUserForUserID:sessionid Block:^(DDUserEntity *user) {
//        
//        if (user) {
//            //不是好友
//            isFriend = YES;
//        }else{
//            //非好友
//            isFriend = NO;
//        }
//    }];
    if ([self.friendId isEqualToString:USERID]) {
        return;
    }
    NSDictionary * params = @{@"uid":USERID,@"personid":self.friendId};
    [HttpTool postWithPath:@"GetPersonInfo" params:params success:^(id JSON) {
        NSString * isFriend = [[JSON objectForKey:@"personinfo"]objectForKey:@"isfriend"];
        __block NSDictionary * storyInfo;
        [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:self.groupId success:^(id JSON) {
            storyInfo = JSON;
            
        }failure:^(id Json) {
            
            
        }];
        NSString * creatorid = [storyInfo objectForKey:@"creatorid"];
        if ([creatorid isEqualToString:USERID]) {
            //自己是群主,需要获取剧的相关信息
            NSDictionary * storyInfo =  [[GetAllFamilysAndStorys shareInstance]getStoryDetail:self.thearterId];
            NSArray * groups = [storyInfo objectForKey:@"groups"];
            NSMutableArray * groupids = [NSMutableArray array];
            for (NSDictionary * groupDic in groups) {
                //剧里面的群
                NSString * isplay = [groupDic objectForKey:@"isplay"];
                NSString * groupId = [groupDic objectForKey:@"groupid"];
                if ([isplay isEqualToString:@"1"]||[isplay isEqualToString:@"2"]) {
                    [groupids addObject:groupId];
                }
            }
            
            NSMutableArray * membersArr = [NSMutableArray arrayWithCapacity:10];
            
            if (groupids.count>0) {
                if (groupids.count == 1) {
                    NSDictionary * params = @{@"uid":USERID,@"storyid":self.thearterId,@"groupid":groupids[0],@"token":@"110"};
                    //将数组的人都取回
                    [HttpTool postWithPath:@"GetStoryGroupInfo" params:params success:^(id JSON) {
                        [membersArr addObjectsFromArray:[[JSON objectForKey:@"groupinfo"]objectForKey:@"members"]];
                        
                    } failure:^(NSError *error) {
                        
                        
                    }];
                }else{
                    //有两个群
                    NSDictionary * params = @{@"uid":USERID,@"storyid":self.thearterId,@"groupid":groupids[0],@"token":@"110"};
                    [HttpTool postWithPath:@"GetStoryGroupInfo" params:params success:^(id JSON) {
                        [membersArr addObjectsFromArray:[[JSON objectForKey:@"groupinfo"]objectForKey:@"members"]];
                        NSDictionary * params = @{@"uid":USERID,@"storyid":self.thearterId,@"groupid":groupids[1],@"token":@"110"};
                        [HttpTool postWithPath:@"GetStoryGroupInfo" params:params success:^(id JSON) {
                            [membersArr addObjectsFromArray:[[JSON objectForKey:@"groupinfo"]objectForKey:@"members"]];
                            
                        } failure:^(NSError *error) {
                            
                        }];
                        
                    } failure:^(NSError *error) {
                        
                        
                    }];
                }
            }
            
            //判断成员是否已经存在于其他群里
            BOOL  isInStoryGroup = NO;
            for(NSDictionary * personInfo in membersArr)
            {
                NSString * personId = [personInfo objectForKey:@"id"];
                if ([personId isEqualToString:self.friendId]) {
                    isInStoryGroup = YES;
                }
            }
            
            if (self.isChatInCheckGroup == YES) {
                //在审核群里，需要判断是否在戏群或者水聊群里
                if ([isFriend isEqualToString:@"1"]) {
                    //已经是好友
                    if(isInStoryGroup ==YES)
                    {
                        //已经存在于其他群了,只有移出该群
                        UIButton * removeOutBtn = [[UIButton alloc]initWithFrame:CGRectMake(112, 7, ScreenWidth-224, 35)];
                        [removeOutBtn setImage:[UIImage imageNamed:@"sinoutuns"] forState:UIControlStateNormal];
                        [removeOutBtn setImage:[UIImage imageNamed:@"sinoutsel"] forState:UIControlStateHighlighted];
                        [removeOutBtn addTarget:self action:@selector(deleteMembers:) forControlEvents:UIControlEventTouchUpInside];
                        [self.backView addSubview:removeOutBtn];
                        
                    }else{
                        //不在其他群,移出该群和移到别的群  //少两张移出群的图
                        float btnWidth =  (ScreenWidth-140)/2.0;
                        UIButton * removeOutBtn = [[UIButton alloc]initWithFrame:CGRectMake(35, 7,btnWidth, 35)];
                        [removeOutBtn setImage:[UIImage imageNamed:@"tworeuns"] forState:UIControlStateNormal];
                        [removeOutBtn setImage:[UIImage imageNamed:@"twoRemove"] forState:UIControlStateHighlighted];
                        [removeOutBtn addTarget:self action:@selector(deleteMembers:) forControlEvents:UIControlEventTouchUpInside];
                        [self.backView addSubview:removeOutBtn];
                        
                        UIButton * gotoGroups = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - btnWidth-35, 7, btnWidth, 35)];
                        [gotoGroups setImage:[UIImage imageNamed:@"treUns"] forState:UIControlStateNormal];
                        [gotoGroups setImage:[UIImage imageNamed:@"twothesel"] forState:UIControlStateHighlighted];
                        [gotoGroups addTarget:self action:@selector(gotoOtherGroups:) forControlEvents:UIControlEventTouchUpInside];
                        [self.backView addSubview:gotoGroups];
                        
                    }
                }else{
                    //非好友，判断存在于群里
                    if (isInStoryGroup ) {
                        //存在于该群，只有移除和申请好友
                        float btnWidth =  (ScreenWidth-140)/2.0;
                        UIButton * removeOutBtn = [[UIButton alloc]initWithFrame:CGRectMake(35, 7,btnWidth, 35)];
                        [removeOutBtn setImage:[UIImage imageNamed:@"tworeuns"] forState:UIControlStateNormal];
                        [removeOutBtn setImage:[UIImage imageNamed:@"twoRemove"] forState:UIControlStateHighlighted];
                        [removeOutBtn addTarget:self action:@selector(deleteMembers:) forControlEvents:UIControlEventTouchUpInside];
                        [self.backView addSubview:removeOutBtn];
                        
                        UIButton * gotoGroups = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - btnWidth-35, 7, btnWidth, 35)];
                        [gotoGroups setImage:[UIImage imageNamed:@"tappfrisel"] forState:UIControlStateNormal];
                        [gotoGroups setImage:[UIImage imageNamed:@"tappfriun"] forState:UIControlStateHighlighted];
                        [gotoGroups addTarget:self action:@selector(applyAddFriend:) forControlEvents:UIControlEventTouchUpInside];
                        [self.backView addSubview:gotoGroups];
                        
                        
                    }else{
                        //非好友，不在其他群里，有三个按钮
                        float  btnWidth = (ScreenWidth - 90)/3.0;
                        UIButton * removeOutBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, 7,btnWidth, 35)];
                        [removeOutBtn setImage:[UIImage imageNamed:@"threeoutuns"] forState:UIControlStateNormal];
                        [removeOutBtn setImage:[UIImage imageNamed:@"threeoutsel"] forState:UIControlStateHighlighted];
                        [removeOutBtn addTarget:self action:@selector(deleteMembers:) forControlEvents:UIControlEventTouchUpInside];
                        [self.backView addSubview:removeOutBtn];
                        
                        UIButton * gotoGroups = [[UIButton alloc]initWithFrame:CGRectMake(45+btnWidth, 7,btnWidth, 35)];
                        [gotoGroups setImage:[UIImage imageNamed:@"threeinuns"] forState:UIControlStateNormal];
                        [gotoGroups setImage:[UIImage imageNamed:@"threeinsel"] forState:UIControlStateHighlighted];
                        [gotoGroups addTarget:self action:@selector(gotoOtherGroups:) forControlEvents:UIControlEventTouchUpInside];
                        [self.backView addSubview:gotoGroups];
                        
                        UIButton * deleteButton = [[UIButton alloc]initWithFrame:CGRectMake(75+btnWidth*2, 7,btnWidth, 35)];
                        [deleteButton setImage:[UIImage imageNamed:@"thappsel"] forState:UIControlStateNormal];
                        [deleteButton setImage:[UIImage imageNamed:@"thrappsel"] forState:UIControlStateHighlighted];
                        [deleteButton addTarget:self action:@selector(applyAddFriend:) forControlEvents:UIControlEventTouchUpInside];
                        [self.backView addSubview:deleteButton];
                    }
                }
            }else{
                //非审核群
                if([isFriend isEqualToString:@"1"])
                {
                    //如果是好友只有移出此群
                    UIButton * removeOutBtn = [[UIButton alloc]initWithFrame:CGRectMake(112, 7, ScreenWidth-224, 35)];
                    [removeOutBtn setImage:[UIImage imageNamed:@"sinoutuns"] forState:UIControlStateNormal];
                    [removeOutBtn setImage:[UIImage imageNamed:@"sinoutsel"] forState:UIControlStateHighlighted];
                    [removeOutBtn addTarget:self action:@selector(deleteMembers:) forControlEvents:UIControlEventTouchUpInside];
                    [self.backView addSubview:removeOutBtn];
                }else{
                    
                    float btnWidth =  (ScreenWidth-140)/2.0;
                    UIButton * removeOutBtn = [[UIButton alloc]initWithFrame:CGRectMake(35, 7,btnWidth, 35)];
                    [removeOutBtn setImage:[UIImage imageNamed:@"tworeuns"] forState:UIControlStateNormal];
                    [removeOutBtn setImage:[UIImage imageNamed:@"twoRemove"] forState:UIControlStateHighlighted];
                    [removeOutBtn addTarget:self action:@selector(deleteMembers:) forControlEvents:UIControlEventTouchUpInside];
                    [self.backView addSubview:removeOutBtn];
                    
                    UIButton * gotoGroups = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - btnWidth-35, 7, btnWidth, 35)];
                    [gotoGroups setImage:[UIImage imageNamed:@"tappfrisel"] forState:UIControlStateNormal];
                    [gotoGroups setImage:[UIImage imageNamed:@"tappfriun"] forState:UIControlStateHighlighted];
                    [gotoGroups addTarget:self action:@selector(applyAddFriend:) forControlEvents:UIControlEventTouchUpInside];
                    [self.backView addSubview:gotoGroups];
                }
            }
        }else{
            //只需要判断是否是好友
            if ([isFriend isEqualToString:@"1"]) {
                self.backView.backgroundColor = [UIColor clearColor];
                self.backView.hidden = YES;
            }else{
                //只有申请好友
                UIButton * removeOutBtn = [[UIButton alloc]initWithFrame:CGRectMake(112, 7, ScreenWidth-224, 35)];
                [removeOutBtn setImage:[UIImage imageNamed:@"sinappuns2x"] forState:UIControlStateNormal];
                [removeOutBtn setImage:[UIImage imageNamed:@"sinappsel"] forState:UIControlStateHighlighted];
                [removeOutBtn addTarget:self action:@selector(applyAddFriend:) forControlEvents:UIControlEventTouchUpInside];
                [self.backView addSubview:removeOutBtn];
            }
            
        }

        
    } failure:^(NSError *error) {
        
        
    }];
    
   }


#pragma mark 移除该群
-(void)deleteMembers:(UIButton *)sender
{
    NSString * group_id = [NSString stringWithFormat:@"group_%@",self.groupId];
    NSString * user_id = [NSString stringWithFormat:@"user_%@",self.friendId];
    [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:@[user_id]];
    
    UIAlertView * alertView =  [[UIAlertView alloc]initWithTitle:@"删除成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
    [self.navigationController popViewControllerAnimated:YES];


}


#pragma mark 移到水聊&对戏
-(void)gotoOtherGroups:(UIButton *)sender
{
    NSDictionary * storyInfo =  [[GetAllFamilysAndStorys shareInstance]getStoryDetail:self.thearterId];
    NSArray * groups = [storyInfo objectForKey:@"groups"];
    NSMutableArray * groupId = [NSMutableArray array];
    for (int i = 0; i < [groups count]; i++) {
        NSDictionary * dic = [groups objectAtIndex:i];
        NSString * isplay = [dic objectForKey:@"isplay"];
        if([isplay isEqualToString:@"1"]||[isplay isEqualToString:@"2"]){
            //是水群或者戏群
            NSString  * groupid = [dic objectForKey:@"groupid"];
            NSString * group_id =[NSString stringWithFormat:@"group_%@",groupid];
            [groupId addObject:group_id];
        }
    }
    
    NSString * user_id = [NSString stringWithFormat:@"user_%@",self.friendId];

    if ([groupId count]>0) {
        for (int i = 0; i < [groupId count]; i++) {
            NSString * grouuID = [groupId objectAtIndex:i];
            NSString * originalGid = [grouuID componentsSeparatedByString:@"_"][1];

            [[ChangeGroupMemberModel shareInsatance]addMemberNotNeedNotifygroupid:grouuID users:@[user_id]];
            
            MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
            NSArray * arrary = @[@(0),self.friendId,originalGid,@(0),@(SessionTypeSessionTypeSingle),@(1)];
            [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
                
            }];

        }
    }
    UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:@"穿越成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark 添加好友
-(void)applyAddFriend:(UIButton *)sender
{
    [self.view endEditing:YES];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"验证消息" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"发送", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = @"请输入20字以内的验证信息";
    alert.tag = 20000;
    
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==20000) {
        
        if (buttonIndex == 1  && [alertView textFieldAtIndex:0].text.length <=20) {
            MsgAddFriendReqAPI * addFriendReq = [[MsgAddFriendReqAPI alloc]init];
            NSString * user_id = [NSString stringWithFormat:@"user_%@",self.friendId];
            NSString * addmessage = @"";
            if ([alertView textFieldAtIndex:0].text != NULL && ![[alertView textFieldAtIndex:0].text isEqual:nil]) {
                addmessage= [alertView textFieldAtIndex:0].text;
            }
            [addFriendReq requestWithObject:@[user_id,@(0),@(SessionTypeSessionTypeSingle),addmessage] Completion:^(id response, NSError *error) {
                
            }];
            
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"添加请求已发送" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            [self.navigationController popViewControllerAnimated:YES];

        }
    }
    
}

-(void)getPersonRole
{
    NSDictionary * params = @{@"uid":self.friendId,@"storyid":self.thearterId,@"token":@"110"};
    [HttpTool postWithPath:@"GetUserRoleInfoInStory" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            self.roleDic = [JSON objectForKey:@"roleinfo"];
            NSString * string = [self.roleDic objectForKey:@"userroleinfo"];
            [self.tableView reloadData];

        }
        NSString * userRole = [JSON objectForKey:@"describe"];
        LOG(@"%@*********",userRole);
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"OtherPersonRoleCell";
    OtherPersonRoleCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"OtherPersonRoleCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    NSString * rolename = [self.roleDic objectForKey:@"userrolename"];
    NSString * roleTitle = [self.roleDic objectForKey:@"roletitle"];

    if ((!roleTitle || [roleTitle isEqualToString:@""]) && ([rolename isEqualToString:@""] || !rolename)) {
        cell.roleName.text = @"对方暂时还没有角色名";
    }
    
    if ((!roleTitle || [roleTitle isEqualToString:@""]) && (![rolename isEqualToString:@""] && rolename)) {
        cell.roleName.text = rolename;

    }
    
    if ((roleTitle && ![roleTitle isEqualToString:@""]) && ([rolename isEqualToString:@""] || !rolename)) {
        cell.roleName.text = [NSString stringWithFormat:@"[%@]",roleTitle];
    }
    if ((roleTitle && ![roleTitle isEqualToString:@""]) && (![rolename isEqualToString:@""] && rolename)) {
        cell.roleName.text = [NSString stringWithFormat:@"[%@] %@",roleTitle,rolename];
    }

    [cell.personRoleLabel setNumberOfLines:0];
    cell.personRoleLabel.text = [self.roleDic objectForKey:@"userroleinfo"];
    cell.personRoleLabel.layer.borderColor =[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1].CGColor;
    cell.personRoleLabel.layer.borderWidth = 1;
    cell.personRoleLabel.layer.cornerRadius = 4;
    if (cell.personRoleLabel.text.length > 0) {
          NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:cell.personRoleLabel.text];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];

        [paragraphStyle setLineSpacing:5.0];//调整行间距

        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [cell.personRoleLabel.text length])];

        [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:NSMakeRange(0, cell.personRoleLabel.text.length)];
        
        cell.personRoleLabel.attributedText = attributedString;

        self.contentLabelHeight = [attributedString boundingRectWithSize:CGSizeMake(ScreenWidth - 50, 99999) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin context:NULL].size.height;
        
        cell.contLableHeight.constant = self.contentLabelHeight;
        if (self.contentLabelHeight< ScreenHeight-150) {
            cell.personRoleLabel.frame = CGRectMake(0, 120, ScreenWidth -40, ScreenHeight-150);

        }else{
            cell.personRoleLabel.frame = CGRectMake(0, 120, ScreenWidth -40, self.contentLabelHeight);
        }
    }else{
        cell.personRoleLabel.text  = @"人设";
        cell.personRoleLabel.frame = CGRectMake(0, 120, ScreenWidth -40, ScreenHeight-150);
    }
                      
//    [cell.portraitBtn sd_setImageWithURL:[NSURL URLWithString:self.portrait] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
//    cell.portraitBtn.clipsToBounds = YES;
//    cell.portraitBtn.layer.cornerRadius = cell.portraitBtn.bounds.size.height / 2;
//    [cell.portraitBtn setBackgroundColor:[UIColor clearColor]];
//    cell.portraitBtn.imageView.layer.borderWidth = 0;
//    cell.portraitBtn.imageView.layer.borderColor = BLUECOLOR.CGColor;
//    cell.portraitBtn.imageView.layer.cornerRadius = 25;
//    cell.portraitBtn.imageView.clipsToBounds = YES;
    cell.portriatImageView.layer.cornerRadius = 28;
    cell.portriatImageView.clipsToBounds = YES;
    [cell.portriatImageView sd_setImageWithURL:[NSURL URLWithString:self.portrait] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    cell.mainViewBtn.layer.cornerRadius = 4;
    cell.mainViewBtn.clipsToBounds = YES;
    [cell.mainViewBtn addTarget:self action:@selector(gotoPersonMainView) forControlEvents:UIControlEventTouchUpInside];
    [cell.portraitBtn addTarget:self action:@selector(gotoPersonMainView) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}


-(void)gotoPersonMainView
{
//    FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
//    detailViewController.FriendID = self.friendId;
//    [self.navigationController pushViewController:detailViewController animated:YES];
    YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
    [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:self.friendId];
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 140 +self.contentLabelHeight;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
