//
//  MyCenterSetTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCenterSetTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (weak, nonatomic) IBOutlet UILabel *unreadCountLabel;
@property (strong, nonatomic) IBOutlet UIImageView *ArrowImage;//箭头
@end
