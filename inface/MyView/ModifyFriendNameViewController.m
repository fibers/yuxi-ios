//
//  ModifyFriendNameViewController.m
//  inface
//
//  Created by appleone on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ModifyFriendNameViewController.h"
#import "SavePersonPortrait.h"
@interface ModifyFriendNameViewController ()

@end

@implementation ModifyFriendNameViewController


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"修改备注名";
    self.chatINtextField.text = @"";
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
}


-(void)returnBackl
{
    [self.chatINtextField resignFirstResponder];

    [self.navigationController popViewControllerAnimated:YES];
}

-(void)returnBackr
{
    //保存人设
    if (self.chatINtextField.text.length == 0) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"备注名不能为空" message:@"请输入8个以内的字符" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        return;
    }else{
        
        int strLength = 0;
        NSString * textStr = self.chatINtextField.text;
        char * p = (char *)[textStr cStringUsingEncoding:NSUnicodeStringEncoding];
        
        for (int i = 0; i < [textStr lengthOfBytesUsingEncoding:NSUnicodeStringEncoding]; i++) {
            if (*p) {
                p++;
                strLength++;
            }else{
                p++;
            }
        }
        
        if (strLength > 12 ) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"备注名长度有点问题" message:@"昵称不能超过12个英文或者6个中文" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alert show];
            return;
        }
        
        if (strLength <= 12 ) {
            NSArray * ids = [self.friendId componentsSeparatedByString:@"_"];
            NSString * friendId;
            if (ids.count > 1) {
                friendId = ids[1];
            }else{
                friendId = ids[0];
            }
            NSDictionary * params = @{@"uid":USERID,@"personnick":self.chatINtextField.text,@"personid":friendId};
            [HttpTool postWithPath:@"ModifyUserName" params:params success:^(id JSON) {
                if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                    //
                    NSDictionary * personDic = @{@"id":friendId,@"nickname":self.chatINtextField.text};
                    
                    [[DDUserModule shareInstance]getUserForUserID:self.friendId Block:^(DDUserEntity *user) {
                        user.name = self.chatINtextField.text;
                        
                        
                        [[DDUserModule shareInstance]changeOtherPersonInfo:self.friendId userEntiry:user];
                        
                    }];
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"修改备注名成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alert show];
                    if (self.returnTextBlock != nil) {
                        self.returnTextBlock(self.chatINtextField.text);
                    }

                    [self.navigationController popViewControllerAnimated:YES];
                }
                
            } failure:^(NSError *error) {
                
            }];
        }
    }
}

- (void)returnText:(ReturnTextBlock)block {
    self.returnTextBlock = block;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)claerText:(id)sender
{
    self.chatINtextField.text = @"";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
