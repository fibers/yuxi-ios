//
//  HelpInformationViewController.h
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpInformationViewController : BaseViewController<UIWebViewDelegate>
{
    UIActivityIndicatorView* activityIndicatorView;
}
@property (strong, nonatomic) IBOutlet UIWebView *aWebView;
@property (nonatomic ,retain) NSString *navTitle;

@property (nonatomic ,retain) NSString *contentUrl;

@property (nonatomic,assign)  BOOL     isAnimating;
@end
