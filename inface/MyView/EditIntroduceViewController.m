//
//  EditIntroduceViewController.m
//  inface
//
//  Created by huangzengsong on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "EditIntroduceViewController.h"
#import"MyCenter.h"
#import "Personinfo.h"
#define MaxNumberOfDescriptionChars  120//textview最多输入的字数
@interface EditIntroduceViewController ()
@property(nonatomic,copy)MyCenter *myCenter;
@end

@implementation EditIntroduceViewController



//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)setContent:(MyCenter *)myCenter{
    _myCenter = myCenter;
}
//右上角按钮
-(void)returnBackr{
    
    if ([self.chatInputTextView.text isEmpty]){
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入文字" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary * dic = @{@"uid":userid,@"intro":self.chatInputTextView.text,@"token":@"110"};
    
    [HttpTool postWithPath:@"SubmitPersonInfo" params:dic success:^(id JSON) {
        //成功的处理
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            [[NSUserDefaults standardUserDefaults]setValue:self.chatInputTextView.text forKey:@"intro"];//简介
            self.myCenter.personinfo.intro = self.chatInputTextView.text;
            
            //            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:@"修改成功"delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            //            [alertview show];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PersonInformationChanged" object:self userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
    } failure:^(NSError *error) {
        
    }];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"修改";
    
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInputTextView.textColor=[UIColor blackColor];
    self.chatInputTextView.placeholder=@"输入文字";
    self.chatInputTextView.placeholderFont=[UIFont systemFontOfSize:14];
    self.chatInputTextView.placeholderColor=COLOR(204, 204, 204, 1);
    self.chatInputTextView.returnKeyType=UIReturnKeyDone;
    NSString * intro=self.myCenter.personinfo.intro;
    self.chatInputTextView.text=intro;
    self.countLabel.textColor=COLOR(204, 204, 204, 1);
    self.countLabel.text=[NSString stringWithFormat:@"%lu",MaxNumberOfDescriptionChars-self.chatInputTextView.text.length];
    
}


/*由于联想输入的时候，函数textView:shouldChangeTextInRange:replacementText:无法判断字数，
 因此使用textViewDidChange对TextView里面的字数进行判断
 */
- (void)textViewDidChange:(UITextView *)textView
{
    
    //该判断用于联想输入
    if (textView.text.length > MaxNumberOfDescriptionChars)
    {
        textView.text = [textView.text substringToIndex:MaxNumberOfDescriptionChars];
        
    }
    self.countLabel.text=[NSString stringWithFormat:@"%lu",MaxNumberOfDescriptionChars-textView.text.length];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
