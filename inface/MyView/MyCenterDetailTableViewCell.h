//
//  MyCenterDetailTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCenterDetailTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *UserImage;//头像

@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UILabel *UserIDLabel;//ID
@property (strong, nonatomic) IBOutlet UILabel *UserDetailLabel;//详情
@property (strong, nonatomic) IBOutlet UILabel *CreateCountLabel;//创作字数

@property (weak, nonatomic) IBOutlet UIButton *ToSetButton;//设置按钮


@property (weak, nonatomic) IBOutlet UIImageView *IconUpImg;
@property (weak, nonatomic) IBOutlet UIImageView *IconDownImg;

@property (weak, nonatomic) IBOutlet UIView *BackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *SexBtn;
@property (weak, nonatomic) IBOutlet UIView *BtnView;
@property (weak, nonatomic) IBOutlet UIButton *ReportBtn;
@property (weak, nonatomic) IBOutlet UIImageView *BackgroundImageview;
@end
