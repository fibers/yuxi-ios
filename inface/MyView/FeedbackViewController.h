//
//  FeedbackViewController.h
//  inface
//
//  Created by Mac on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackViewController : BaseViewController<UITextViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property(assign,nonatomic)    BOOL   isAnimating;


@end
