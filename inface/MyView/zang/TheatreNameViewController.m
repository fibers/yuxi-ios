//
//  TheatreNameViewController.m
//  inface
//
//  Created by dev01 on 15/7/23.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "TheatreNameViewController.h"

@interface TheatreNameViewController ()<UITextViewDelegate>
@property (nonatomic,strong) UITextView *textView;
@property (nonatomic,strong) UILabel *countLabel;
@end

@implementation TheatreNameViewController

- (UITextView *)textView
{
    if (_textView == nil) {
        
        _textView = [[UITextView alloc]init];
        _textView.delegate = self;
        _textView.placeholder = @"  请输入剧名（30字内）";
    }
    return _textView;
}

- (UILabel *)countLabel
{
    if (_countLabel == nil) {
        
        _countLabel = [[UILabel alloc]init];
        _countLabel.textColor = COLOR(200, 200, 200, 1);
        _countLabel.font = [UIFont systemFontOfSize:12];
    }
    return _countLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.view.backgroundColor = BACKGROUND_COLOR;
    
    [self setNavBar];
    [self setViewData];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
- (void)setNavBar
{
    self.navigationItem.title=@"更改剧名";
    [self setLetfButton];
    [self setRightButton];
    
}
- (void)setLetfButton
{
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
}
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setRightButton
{
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
}

- (void)save
{
    NSMutableDictionary *myDic = [NSMutableDictionary dictionary];
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    myDic[@"uid"] = userid;
    myDic[@"storyid"] = self.dataDic[@"id"];
    myDic[@"type"] = @"4";
    myDic[@"content"] = self.textView.text;
    myDic[@"token"] = @"110";
    
    [HttpTool postWithPath:@"ModifyStory" params:myDic success:^(id JSON) {
        [[MyLiveViewController shareInstance]getMyStorys];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"NAMECHANGE" object:nil];
        [self back];
    } failure:^(NSError *error) {
        
    }];

}

- (void)setViewData
{
    self.textView.frame = CGRectMake(0, 20, ScreenWidth, 100);
    self.textView.backgroundColor = [UIColor whiteColor];
    NSString *title = self.dataDic[@"title"];
    if (title.length > 0) {
        
        self.textView.text = title;
    }
    [self.view addSubview:self.textView];
    
    CGSize size = self.textView.frame.size;
    self.countLabel.frame = CGRectMake(size.width - 30, size.height - 30, 20, 20);
    self.countLabel.text = @"30";
    [self.textView addSubview:self.countLabel];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    textView.text = @"";
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    int length = (int)textView.text.length;
    if (length < 31) {
        
        self.countLabel.text = [NSString stringWithFormat:@"%d",30 - length];
    } else {
        
        [SVProgressHUD showErrorWithStatus:@"最多30字"];
        NSString *tmp = [textView.text substringToIndex:30];
        textView.text = tmp;
    }
}

@end
