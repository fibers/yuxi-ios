//
//  SystemSetViewController.m
//  inface
//
//  Created by Mac on 15/5/23.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SystemSetViewController.h"
#import "YXBindMobilePhoneViewController.h"
#import "NSString+DDPath.h"
#define TOPSESSIONS   @"topSessions"

@interface SystemSetViewController ()
@property(copy,nonatomic)NSString * bindingPhoneNumber;
@property(nonatomic,assign)CGFloat casheSize;//缓存大小
@end

@implementation SystemSetViewController
//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.bindingPhoneNumber = [[NSUserDefaults standardUserDefaults]objectForKey:kBindPhoneNumber];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;

    
    self.navigationItem.title=@"设置";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 15)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 15)];
    }

    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
//    self.tableView.tableFooterView=[[UIView alloc]init];
    //计算缓存大小
    

}


-(void)folderSizeAtPath
{
    self.casheSize +=[[SDImageCache sharedImageCache]getSize] /1024.0/1024.0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *identifier=@"MyCenterSetTableViewCell";
    MyCenterSetTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"MyCenterSetTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    //        cell.backgroundColor=[UIColor clearColor];
    
    if (indexPath.row==0) {
        cell.UserNameLabel.text=@"新消息提醒";
    } else if (indexPath.row==1){
        if(self.bindingPhoneNumber){
            cell.UserNameLabel.text = [NSString stringWithFormat:@"已绑定:%@",self.bindingPhoneNumber];
        }else{
            cell.UserNameLabel.text=@"绑定手机号";
        }
       
    } else if (indexPath.row==2){
         cell.UserNameLabel.text=@"反馈我们";
       
    } else if(indexPath.row ==3){
         cell.UserNameLabel.text=@"帮助信息";
        
    }else if(indexPath.row ==4){
        cell.UserNameLabel.text=@"关于我们";
    }
    
    else if(indexPath.row ==  5){
        cell.UserNameLabel.text = @"清除缓存";
    }
    
    return cell;
    
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    return 44;
    
}



- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *aview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 90)];
    aview.backgroundColor=[UIColor clearColor];
    aview.userInteractionEnabled=YES;
    
    UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
    aButton.frame=CGRectMake(50,20, ScreenWidth-100, 50);
    aButton.layer.cornerRadius = 4;
    aButton.clipsToBounds = YES;
    [aButton addTarget:self action:@selector(handleBtn:) forControlEvents:UIControlEventTouchUpInside];
    [aButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    aButton.titleLabel.font=[UIFont systemFontOfSize:20];
    
    [aButton setBackgroundColor:BLUECOLOR];
    [aButton setTitle:@"退出登录" forState:UIControlStateNormal];
    [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
    
    [aview addSubview:aButton];
    
    return aview;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 50.0;
}

//退出登录
- (void)handleBtn:(UIButton *)btn
{
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"确定要退出登录?" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.tag = 55555;
    [alert show];
    
}

#pragma mark 登出操作
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 55555 && buttonIndex == 1) {
        LoginOut * loginOut = [[LoginOut alloc]init];
        [loginOut requestWithObject:nil Completion:^(id response, NSError *error) {
            
            [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationLogout object:nil];
            [RuntimeStatus instance].user = nil;
            [RuntimeStatus instance].userID = nil;
            [DDClientState shareInstance].userState = DDUserOffLineInitiative;
            [[DDTcpClientManager instance]disconnect];
            
        }];
        NSDictionary * param = @{@"user_id":USERID};
        [[AFHTTPSessionManager manager]POST:[NSString stringWithFormat:@"%@/%@",SERVER_PUSH,@"unregister_push"]  parameters:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            LOG(@"取消推送 %@",responseObject);
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            LOG(@"取消推送失败！");
        }];
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:kBindPhoneNumber];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"username"];
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"loginstatus"];//已经登录成功
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"userid"];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"user_userid"];
        
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"gender"];//性别
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"nickname"];//昵称
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"portrait"];//头像
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"intro"];//简介
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"wordcount"];//创作字数
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"newfriendaddmessages"];//新好友添加
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"storycount"];//我的剧的个数
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"likeStorys"];//喜欢得剧的个数
        [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"VOICE"];
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"port"];
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"priorIP"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:PUSH_CHATVIEW_NUMBER];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:TOPSESSIONS];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        LoginViewController * loginView = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        if (loginView.isAnimating) {
            return;
        }

        loginView.isAnimating = YES;
        [self.navigationController pushViewController:loginView animated:YES];
    }
    if (alertView.tag == 3333 && buttonIndex == 1) {
        //清除缓存

        [[SDImageCache sharedImageCache]clearDisk];

    }
}

//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    if (indexPath.row==0) {
        MessageReminderViewController * detailViewController = [[MessageReminderViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;

        [self.navigationController pushViewController:detailViewController animated:YES];
    } else if (indexPath.row==1) {
//        if (self.bindingPhoneNumber) {
//            [[[UIAlertView alloc]initWithTitle:@"提示" message:@"已经绑定过手机号" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil] show];
//            return;
//        }else{
            YXBindMobilePhoneViewController * bindVC = [[YXBindMobilePhoneViewController alloc]init];
            [self.navigationController pushViewController:bindVC animated:YES];
//        }
    } else if (indexPath.row==2) {
        FeedbackViewController * detailViewController = [[FeedbackViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];

        
    } else if(indexPath.row ==3){
        
        HelpInforViewController * detailViewController = [[HelpInforViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];

    } else if(indexPath.row == 4){
        AboutUsViewController * detailViewController = [[AboutUsViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }else if (indexPath.row == 5){
        self.casheSize =[[SDImageCache sharedImageCache]getSize] /1024.0/1024.0;
        if (self.casheSize <= 0) {
            UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"当前没有缓存文件" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alertView show];
            return;
        }
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"缓存大小为:%.1fM,确定要清除缓存？",self.casheSize] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alertView.tag = 3333;
        
        [alertView show];
    }

    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
