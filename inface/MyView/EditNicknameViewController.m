//
//  EditNicknameViewController.m
//  inface
//
//  Created by huangzengsong on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "EditNicknameViewController.h"
#import "PersonalSetViewController.h"

@interface EditNicknameViewController ()

@end

@implementation EditNicknameViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
    
}

//右上角按钮
-(void)returnBackr{
    
    if (self.chatInputTextField.text ==nil){
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"昵称不能为空哦" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    int strLength = 0;
    NSString * textStr = self.chatInputTextField.text;
    char * p = (char *)[textStr cStringUsingEncoding:NSUnicodeStringEncoding];
    
    for (int i = 0; i < [textStr lengthOfBytesUsingEncoding:NSUnicodeStringEncoding]; i++) {
        if (*p) {
            p++;
            strLength++;
        }else{
            p++;
        }
    }
    

    if (strLength > 12 ) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"昵称长度有点问题" message:@"昵称不能超过12个英文或者6个中文" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (strLength < 2) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"昵称长度有点问题" message:@"昵称不能少于两个字符哦" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }

    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary * dic = @{@"uid":userid,@"nickname":self.chatInputTextField.text,@"token":@"110"};
    
    [HttpTool postWithPath:@"SubmitPersonInfo" params:dic success:^(id JSON) {
        //修改成功
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            [[NSUserDefaults standardUserDefaults]setValue:self.chatInputTextField.text forKey:@"nickname"];//昵称
//            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:@"修改成功"delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//            [alertview show];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PersonInformationChanged" object:self userInfo:nil];
            [[DDUserModule shareInstance]changeUseerName:self.chatInputTextField.text ];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        //添加 字典，将label的值通过key值设置传递
        NSDictionary *dict =[[NSDictionary alloc] initWithObjectsAndKeys:self.chatInputTextField.text,@"textOne", nil];
        //创建通知
        NSNotification *notification =[NSNotification notificationWithName:@"xiugaimingzi" object:nil userInfo:dict];
        //通过通知中心发送通知
        [[NSNotificationCenter defaultCenter] postNotification:notification];

    } failure:^(NSError *error) {
        
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    //[RightButton addTarget:self action:@selector(saveNickName) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    NSString * nickname=[[NSUserDefaults standardUserDefaults]objectForKey:@"nickname"];
    self.chatInputTextField.text=nickname;
    self.navigationItem.title=@"修改";
}

-(void)saveNickName{

    [self.navigationController popViewControllerAnimated:YES];
}







- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==self.chatInputTextField) {
        
        if (textField.text.length > 12)
        {
            textField.text = [textField.text substringToIndex:12];
        }
    }else{
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
