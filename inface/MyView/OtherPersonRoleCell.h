//
//  OtherPersonRoleCell.h
//  inface
//
//  Created by appleone on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherPersonRoleCell : UITableViewCell
- (IBAction)portraitBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *roleName;
- (IBAction)personMainView:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *personRoleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contLableHeight;
@property (weak, nonatomic) IBOutlet UIButton *portraitBtn;
@property (weak, nonatomic) IBOutlet UIButton *mainViewBtn;
@property (weak, nonatomic) IBOutlet UIImageView *portriatImageView;

@end
