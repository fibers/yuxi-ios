//
//  HelpInforViewController.h
//  inface
//
//  Created by huangzengsong on 15/6/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface HelpInforViewController : BaseViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(assign,nonatomic)    BOOL   isAnimating;


@end
