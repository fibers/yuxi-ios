//
//  SystemSetViewController.h
//  inface
//
//  Created by Mac on 15/5/23.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "MyCenterSetTableViewCell.h"
#import "MessageReminderViewController.h"
#import "FeedbackViewController.h"
//#import "HelpInformationViewController.h"
#import "AboutUsViewController.h"
#import "HelpInforViewController.h"
@interface SystemSetViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
