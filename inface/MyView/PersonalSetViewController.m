//
//  PersonalSetViewController.m
//  inface
//
//  Created by Mac on 15/4/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PersonalSetViewController.h"
#import "UpYun.h"
#import "LoginOut.h"
#import "LoginViewController.h"
#import "Personinfo.h"
#import "EditNicknameViewController.h"
@interface PersonalSetViewController ()<UIAlertViewDelegate>
{
    UpYun * _upYun;
    NSString *loadUrl;
    NSString *sex;
    NSString *newname;
    
}
@property(nonatomic,strong)MyCenter * myCenter;
@end

@implementation PersonalSetViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(xiugaimingzi:) name:@"xiugaimingzi" object:nil];
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=@"个人信息";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 20)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 20)];
    }
    
    self.tableView.separatorColor=XIAN_COLOR;
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    imagepicker = [[UIImagePickerController alloc]init];
    
    //观察编辑动作的改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadArray:) name:@"PersonInformationChanged" object:nil];
    self.circleView = [[EVCircularProgressView alloc]initWithFrame:CGRectMake(36, 31, 28, 28)];
}

-(void)reloadArray:(NSNotification *)notification
{
    [self.tableView reloadData];
    
}

-(void)xiugaimingzi:(NSNotification *)noti{

    newname = noti.userInfo[@"textOne"];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==0) {
        static NSString *identifier=@"FamilyNameTableViewCell";
        FamilyNameTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyNameTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.UserImage.userInteractionEnabled=YES;
        aimageViewPhoto=cell.UserImage;
        
        NSString * portrait=[self.myCenter.personinfo.portrait stringOfW250ImgUrl];
        NSURL* url = [NSURL URLWithString:portrait];
        //上传图了
        if (self.shuaxinImage == YES) {
            //cell.UserImage.image  = aimageViewPhoto.image;
            NSURL *url = [NSURL URLWithString:loadUrl];
            [cell.UserImage sd_setImageWithURL:url placeholderImage:cell.UserImage.image];
        }else
        {
            [cell.UserImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
        }
        cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;
        cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
        cell.UserImage.clipsToBounds=YES;
        self.familyName = cell;
        return cell;
    } else if (indexPath.row==1){
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.UserNameLabel.text=@"用户名";
        NSString * nickname=self.myCenter.personinfo.nickname;
        
        
        cell.UserDetailLabel.text=nickname;
        
        if (newname == nil) {
        }else{
            cell.UserDetailLabel.text = newname;
        }
        return cell;
    } else if (indexPath.row==2){
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.ArrowImage.hidden=YES;
        cell.UserNameLabel.text=@"语戏号";
        NSString * userid=self.myCenter.personinfo.id;
        cell.UserDetailLabel.text=userid;
        
        return cell;
    } else  if (indexPath.row==3){
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        sexLabel=cell.UserDetailLabel;
        cell.UserNameLabel.text=@"性别";
        NSString * gender=self.myCenter.personinfo.gender;
        if ([gender isEqualToString:@"1"]) {
            cell.UserDetailLabel.text=@"男";
        } else {
            cell.UserDetailLabel.text=@"女";
        }
        if (sex == nil) {
            
        }else{
        cell.UserDetailLabel.text = sex;
        }
        return cell;
    }else{
        
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.UserNameLabel.text=@"简介";
        cell.UserDetailLabel.textAlignment=NSTextAlignmentLeft;
        NSString * intro= self.myCenter.personinfo.intro;
        
        cell.UserDetailLabel.numberOfLines=0;
        cell.UserDetailLabel.text=intro;
        
        return cell;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 50;
}


-(void) setValue:(NSString *)param{
    NSLog(@"pass value is:%@",param);
}




//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    UIView *aview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 50)];
//    aview.backgroundColor=[UIColor clearColor];
//
//
//    UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
//    aButton.frame=CGRectMake(50,10, ScreenWidth-100, 30);
//    aButton.layer.cornerRadius = 15;
//    aButton.clipsToBounds = YES;
//    [aButton addTarget:self action:@selector(handleBtn:) forControlEvents:UIControlEventTouchUpInside];
//    [aButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    aButton.titleLabel.font=[UIFont systemFontOfSize:14];
//
//    [aButton setBackgroundColor:[UIColor blackColor]];
//    [aButton setTitle:@"退出登录" forState:UIControlStateNormal];
//    [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
//
//    [aview addSubview:aButton];
//
//
//    return aview;
//
//
//}

//退出登录
//- (void)handleBtn:(UIButton *)btn
//{
//
//    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"确定要退出登录?" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//    alert.tag = 55555;
//    [alert show];
//
//}
//
//#pragma mark 登出操作
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (alertView.tag == 55555 && buttonIndex == 1) {
//        LoginOut * loginOut = [[LoginOut alloc]init];
//        [loginOut requestWithObject:nil Completion:^(id response, NSError *error) {
//
//            [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationLogout object:nil];
//            [RuntimeStatus instance].user = nil;
//            [RuntimeStatus instance].userID = nil;
//            [DDClientState shareInstance].userState = DDUserOffLineInitiative;
//            [[DDTcpClientManager instance]disconnect];
//
//        }];
//        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"password"];
//        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"username"];
//        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"loginstatus"];//已经登录成功
//
//        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"userid"];
//
//        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"user_userid"];
//
//        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"gender"];//性别
//        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"nickname"];//昵称
//        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"portrait"];//头像
//        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"intro"];//简介
//        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"wordcount"];//创作字数
//        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"newfriendaddmessages"];//新好友添加
//        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"storycount"];//我的剧的个数
//        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"likeStorys"];//喜欢得剧的个数
//        [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"VOICE"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//
//
//        LoginViewController * loginView = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
//
//        [self.navigationController pushViewController:loginView animated:YES];
//    }
//}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        return 90;
    }else if (indexPath.row==4) {
       
        NSString * intro= self.myCenter.personinfo.intro;
        float height=[HZSInstances labelAutoCalculateRectWith:intro FontSize:14.0 MaxSize:CGSizeMake(ScreenWidth-140, 2000)].height;
        if (height>30) {
            return height+14;
        } else {
            return 44;
        }
        
    } else {
        return 44;
    }
    
    
    
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    if (indexPath.row==0) {
        //        BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        //
        //        //如果支持相机，那么多个选项
        //        if (isCameraSupport) {
        UIActionSheet* mySheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"挑照片" otherButtonTitles:@"现在拍", nil];
        [mySheet showInView:self.view];
        
        //        }
        //        else {
        //            [self startPhotoChooser];
        //        }
        
    } else if (indexPath.row==1){
        
        
        EditNicknameViewController * detailViewController = [[EditNicknameViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
        
    } else if (indexPath.row==3){
        
        UIActionSheet* mySheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"男",@"女",nil];
        mySheet.tag=2000;
        [mySheet showInView:self.view];
        
        
    }else if (indexPath.row==4){
        
        EditIntroduceViewController * detailViewController = [[EditIntroduceViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        [detailViewController setContent:self.myCenter];
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    }
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}



- (void)uzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    
    
    if([[assets[0] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
    {
        
        [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            ALAsset *representation = obj;
            
            UIImage * original = [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                                     scale:representation.defaultRepresentation.scale
                                               orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
            
            self.familyName.UserImage.image = original ;

            
            CGSize  imgSize = CGSizeMake(2210, 1280);
            
            original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
            self.shuaxinImage = YES;
            [self.tableView reloadData];
            [self getServerKey:original];
            
        }];
    }
    else //Video
    {
        
        
    }
    
}


#pragma mark 从又拍获取图片字符串
-(void)getServerKey:(UIImage *)image
{
    //    [SVProgressHUD showWithStatus:@"上传中" maskType:SVProgressHUDMaskTypeBlack];
    //此处需要换成又拍的云存储
    _upYun = [[UpYun alloc]init];
    [_upYun uploadImage:image];
    [self.circleView setHidden:NO];
    aimageViewPhoto.image = image;
    self.circleView.center = CGPointMake(aimageViewPhoto.center.x , aimageViewPhoto.center.y );
    [self.circleView setProgress:0.0 animated:YES];
    self.circleView.alpha = 1;
    [self.circleView commonInit];
    [self.familyName addSubview:self.circleView];
    [self.familyName bringSubviewToFront:self.circleView];
    
    //上传图片成功
    WEAKSELF
    _upYun.successBlocker = ^(id data){
        if (!weakSelf) {
            return ;
        }
        NSString        *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
        imgUrl  =   [NSString stringWithFormat:@"%@%@", IMAGE_SERVER_BASE_URL, imgUrl];
        loadUrl = imgUrl;
        weakSelf.shuaxinImage = YES;
        weakSelf.imgSerUrl = imgUrl;
        [weakSelf.circleView setHidden:YES];
        
        [[NSUserDefaults standardUserDefaults]setValue:imgUrl forKey:@"portrait"];//头像
        NSString * userid= weakSelf.myCenter.personinfo.id;
        NSDictionary * dic = @{@"uid":userid,@"portraitImg":weakSelf.imgSerUrl};
        [weakSelf postPersonMessage:dic];
        [SVProgressHUD dismiss];
    };
    _upYun.failBlocker = ^(NSError * error)
    {
        //上传又拍失败的操作
        [weakSelf.circleView setHidden:YES];
        
        [SVProgressHUD showErrorWithStatus:@"上传失败" duration:2.0];
    };
    
    _upYun.progressBlocker = ^(CGFloat percent,long long requestDidSendBytes)
    {
        [ weakSelf.circleView setProgress:percent animated:YES];
    };
    
}


#pragma mark 更新个人信息
-(void)postPersonMessage :(NSDictionary *)params
{
    [HttpTool postWithPath:@"SubmitPersonInfo" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            //            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:@"修改成功"delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            //            [alertview show];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PersonInformationChanged" object:self userInfo:nil];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }

    } failure:^(NSError *error) {
    }];
    
}

#pragma mark actionSheet Delegate

- (void) startPhotoChooser
{
    UzysAssetsPickerController *picker = [[UzysAssetsPickerController alloc] init];
    picker.delegate = self;
    
    picker.maximumNumberOfSelectionPhoto = 1;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
    
}
-(void)setContentWithModel:(MyCenter *)mycenter{
    self.myCenter = mycenter;
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==2000) {
        if (buttonIndex==0) {
            //男按钮
            sexLabel.text=@"男";
            sex = sexLabel.text;
            [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"gender"];//性别
            NSString * userid=self.myCenter.personinfo.id;
            NSDictionary * dic = @{@"uid":userid,@"gender":@"1"};
            
            [self postPersonMessage:dic];
        }else if (buttonIndex==1) {
            //女按钮
            sexLabel.text=@"女";
            sex = sexLabel.text;
            [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"gender"];//性别
            NSString * userid=self.myCenter.personinfo.id;
            NSDictionary * dic = @{@"uid":userid,@"gender":@"0"};
            
            [self postPersonMessage:dic];
            
        }
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    } else {
        //        //choose photo
        //        if (buttonIndex == 0) {
        //
        //            [self startPhotoChooser];
        //
        //        } else if (buttonIndex == 1) {
        //            //take photo.
        //            //指定使用照相机模式,可以指定使用相册／照片库
        //            imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        //            //设置当拍照完或在相册选完照片后，是否跳到编辑模式进行图片剪裁。只有当showsCameraControls属性为true时才有效果
        //            imagepicker.allowsEditing = NO;
        //            //设置拍照时的下方的工具栏是否显示，如果需要自定义拍摄界面，则可把该工具栏隐藏
        //            imagepicker.showsCameraControls  = YES;
        //            //设置使用后置摄像头，可以使用前置摄像头
        //            imagepicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        //            //设置闪光灯模式
        //            /*
        //             typedef NS_ENUM(NSInteger, UIImagePickerControllerCameraFlashMode) {
        //             UIImagePickerControllerCameraFlashModeOff  = -1,
        //             UIImagePickerControllerCameraFlashModeAuto = 0,
        //             UIImagePickerControllerCameraFlashModeOn   = 1
        //             };
        //             */
        //            imagepicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        //            //设置相机支持的类型，拍照和录像
        //            imagepicker.mediaTypes = @[(NSString*)kUTTypeImage];
        //            //设置拍摄时屏幕的view的transform属性，可以实现旋转，缩放功能
        //            // imagepicker.cameraViewTransform = CGAffineTransformMakeRotation(M_PI*45/180);
        //            // imagepicker.cameraViewTransform = CGAffineTransformMakeScale(1.5, 1.5);
        //            //所有含有cameraXXX的属性都必须要sourceType是UIImagePickerControllerSourceTypeCamera时设置才有效果，否则会有异常
        //            //设置UIImagePickerController的代理
        //            imagepicker.delegate = self;
        //            [self presentViewController:imagepicker animated:YES completion:^{
        //
        //            }];
        //
        //        }
        
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        
        switch (buttonIndex) {
                
            case 0://From album
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                
                [self presentViewController:picker animated:YES completion:nil];
                break;
            case 1://Take picture
                
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    
                }else{
                }
                
                [self presentViewController:picker animated:YES completion:nil];
                
                break;
                
            default:
                
                break;
        }
        
        
    }
    
}

#pragma take photos image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
    NSString* type = [info objectForKey:UIImagePickerControllerMediaType];
    //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    if ([type isEqualToString:(NSString*)kUTTypeImage]) {
        //获取照片的原图
        UIImage* original = [info objectForKey:UIImagePickerControllerOriginalImage];
        ALAssetsLibrary * alibary = [[ALAssetsLibrary alloc]init
                                     ];
        __block float fileMB = 0.0;
        [alibary assetForURL:[info objectForKey:UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset) {
            ALAssetRepresentation * representation = [asset defaultRepresentation];
            float perMBBytes = 1024 *1024.0;
            fileMB = (float)([representation size]/perMBBytes);
            
        } failureBlock:^(NSError *error) {
            
            
        }];
        original = [HZSInstances scaleImage:original toScale:0.8];
        aimageViewPhoto.image=original;
        
        //将图片传递给截取界面进行截取并设置回调方法（协议）
        CaptureViewController *captureView = [[CaptureViewController alloc] init];
        captureView.delegate = self;
        captureView.image = original;
        //隐藏UIImagePickerController本身的导航栏
        picker.navigationBar.hidden = YES;
        if (captureView.isAnimating) {
            return;
        }
        captureView.isAnimating = YES;
        [picker pushViewController:captureView animated:YES];
        
        
        
        //        //如果是拍照的照片，则需要手动保存到本地，系统不会自动保存拍照成功后的照片
        //        UIImageWriteToSavedPhotosAlbum(original, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
    }
    
    //    //模态方式退出uiimagepickercontroller
    //    [imagepicker dismissViewControllerAnimated:YES completion:^{
    //
    //    }];
}
////取消照相机的回调
//- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
//    //模态方式退出uiimagepickercontroller
//    [imagepicker dismissViewControllerAnimated:YES completion:^{
//
//    }];
//}
////保存照片成功后的回调
//- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
//  contextInfo:(void *)contextInfo{
//
//}
////add for photo functions end

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


#pragma mark - 图片回传协议方法
-(void)passImage:(UIImage *)image
{
    
    //上传图片
    [self getServerKey :image];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
