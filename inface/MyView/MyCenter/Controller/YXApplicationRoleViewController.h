//
//  YXApplicationRoleViewController.h
//  inface
//
//  Created by lizhen on 15/11/6.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface YXApplicationRoleViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>

@property(assign,nonatomic)    BOOL   isAnimating;


@end
