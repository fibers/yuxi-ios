//
//  YXRoleReviewViewController.m
//  inface
//
//  Created by 邢程 on 15/11/5.
//  Copyright © 2015年 huangzs. All rights reserved.
//
//
#import "YXRoleReviewViewController.h"
#import "RoleReviewEntity.h"
#import "MBProgressHUD.h"
#import "YXRoleReviewHeaderCell.h"
#import "YXRoleReviewGroupView.h"
#import "YXRoleReviewHeaderView.h"
#import "YXRoleReviewXiWenCell.h"
#import <Masonry.h>
#import "MsgAddGroupConfirmAPI.h"
@interface YXRoleReviewViewController ()<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate>
@property(strong,nonatomic)UITableView * tableView;
@property(strong,nonatomic)RoleReviewEntity * entity;
@property(copy,nonatomic)NSString *applyid;
@property(strong,nonatomic)NSString *storyid;

@end

@implementation YXRoleReviewViewController
-(void)setApplyID:(NSString*)applyid{
    _applyid = applyid;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        
       
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setTitle:@"角色设定"];
    [self loadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)loadData{
    [HttpTool postWithPath:@"GetStoryRoleApplication" params:@{@"uid":USERID,@"applyid":self.applyid == nil ?@"":self.applyid} success:^(id JSON) {
        _entity = [RoleReviewEntity objectWithKeyValues:JSON];
        if ([self.entity.result isEqual:@"1"]) {
            [self.view addSubview:self.tableView];
            [self.view addSubview:[self buttomView]];
            self.storyid = [JSON objectForKey:@"applystoryid"];
        }else{
            [self showHud:@"服务器错误！"];
        }
    } failure:^(NSError *error) {
        
    }];
}
-(void)showHud:(NSString*)str{
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setMode:MBProgressHUDModeText];
    [hud setLabelText:str];
    [hud show:YES];
    [hud hide:YES afterDelay:1.5];
}
#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            return 0;
            break;
        case 2:
            return 0;
            
            break;
        case 3:
            return [self.entity.applyrolenatures count];
            break;
        case 4:
            return 1;
            
            break;
        default:
            return 1;
            break;
    }
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 50;
            break;
        case 1:
            return 44;
            break;
        case 2:{
            YXRoleReviewGroupView * view = [[[NSBundle mainBundle]loadNibNamed:@"YXRoleReviewGroupView" owner:nil options:nil]lastObject];
            [view setContentWithType:YXRoleReviewGroupViewWithSubContentType title:@"身世背景" andSubContent:self.entity.applyrolebackground];
            return view.viewH;
        }
        case 3:
            return 44;
            
            break;
        case 4:
            return 44;
            break;
            
        default:
            return 0;
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 3:
            return 44;
            
            break;
        case 4:{
            YXRoleReviewXiWenCell * cell = [[YXRoleReviewXiWenCell alloc]init];
            return [cell heightWithStr:self.entity.applytext];
        }
            break;
            
        default:
            return 0;
            break;
    }
    
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            YXRoleReviewHeaderView * view = [[[NSBundle mainBundle]loadNibNamed:@"YXRoleReviewHeaderView" owner:nil options:nil]lastObject];
            view.frame = CGRectMake(0, 0, ScreenWidth, 50);
            [view setContentWithEntity:self.entity];
            return view;
        }
            break;
        case 1:{
            YXRoleReviewGroupView * view = [[[NSBundle mainBundle]loadNibNamed:@"YXRoleReviewGroupView" owner:nil options:nil]lastObject];
            [view setContentWithType:YXRoleReviewGroupViewWithSubTitleType title:@"想扮演的角色" andSubContent:self.entity.applyroletype];
            view.frame = CGRectMake(0, 0, ScreenWidth, 44);
            return view;
        }
            break;
        case 2:{//
            YXRoleReviewGroupView * view = [[[NSBundle mainBundle]loadNibNamed:@"YXRoleReviewGroupView" owner:nil options:nil]lastObject];
            [view setContentWithType:YXRoleReviewGroupViewWithSubContentType title:@"身世背景" andSubContent:self.entity.applyrolebackground];
            view.frame = CGRectMake(0, 0, view.viewH, 44);
            return view;
        
        }
        case 3:{
            YXRoleReviewGroupView * view = [[[NSBundle mainBundle]loadNibNamed:@"YXRoleReviewGroupView" owner:nil options:nil]lastObject];
            [view setContentWithType:YXRoleReviewGroupViewHeaderDefault title:@"角色设定：" andSubContent:@""];
            view.frame = CGRectMake(0, 0, ScreenWidth, 44);
            return view;
        }
            break;
        case 4:{
            YXRoleReviewGroupView * view = [[[NSBundle mainBundle]loadNibNamed:@"YXRoleReviewGroupView" owner:nil options:nil]lastObject];
            [view setContentWithType:YXRoleReviewGroupViewHeaderDefault title:@"审核戏文：" andSubContent:@""];
            view.frame = CGRectMake(0, 0, ScreenWidth, 44);
            return view;
        }
            break;
        default:{
            return [[UIView alloc]init];
        }
            break;
    }
}
-(void)optBtnClicked:(UIButton*)btn{
    UIActionSheet * sheetV = [[UIActionSheet alloc]initWithTitle:@"请确认操作" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
    if(btn.tag == 666){
        sheetV.tag = 666;
        [sheetV showInView:self.view];
       
    }
    if (btn.tag == 999) {
        sheetV.tag = 999;
        [sheetV showInView:self.view];
        
    }
}

-(void)actionSheetCancel:(UIActionSheet *)actionSheet{

}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        NSString * approved ;
        MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDictionary * storyInfo  = [[GetAllFamilysAndStorys shareInstance]getStoryDetail:_storyid];
        NSArray * groups = [storyInfo objectForKey:@"groups"];
        
        NSMutableArray * group_ids = [NSMutableArray array];
        [groups enumerateObjectsUsingBlock:^(NSDictionary * group, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([[group objectForKey:@"isplay"]isEqualToString:@"1"] ||[[group objectForKey:@"isplay"]isEqualToString:@"2"]) {
                [ group_ids addObject:[NSString stringWithFormat:@"group_%@",[group objectForKey:@"groupid"]] ];
            }
            
        }];
        
        if (actionSheet.tag == 666) {
            approved = @"1";
            //将成员加到戏群和水聊群
            NSString * user_id = [NSString stringWithFormat:@"user_%@",self.entity.applicantuid];
            for (int i = 0; i < group_ids.count; i++) {
                [[ChangeGroupMemberModel shareInsatance]addMemberNotNeedNotifygroupid:group_ids[i] users:@[user_id]];
                
                MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
                
                NSString  *originalId = [group_ids[i] componentsSeparatedByString:@"_"][1];
                NSArray * arrary = @[@(0),self.entity.applicantuid,originalId,@(0),@(SessionTypeSessionTypeSingle),@(1)];
                
                [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
                    
                    
                }];

         }
        }
        if (actionSheet.tag == 999) {
            approved = @"0";
            //将成员加到戏群和水聊群
            NSString * user_id = [NSString stringWithFormat:@"user_%@",self.entity.applicantuid];
            for (int i = 0; i < group_ids.count; i++) {

                MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
                
                NSString  *originalId = [group_ids[i] componentsSeparatedByString:@"_"][1];
                NSArray * arrary = @[@(0),self.entity.applicantuid,originalId,@(0),@(SessionTypeSessionTypeSingle),@(0)];
                [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
                    
                }];
            }
        }
            [HttpTool postWithPath:@"ReviewRoleApplication"
                            params:@{@"uid":USERID,
                                     @"applyid":self.applyid,
                                     @"approved":approved
                                     } success:^(id JSON) {
                                         if([[JSON objectForKey:@"result"] isEqualToString:@"1"]){
                                             [hud setHidden:YES];
                                             [self.navigationController popViewControllerAnimated:YES];
                                             UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight-64-40, ScreenWidth, 40)];
                                             [btn setBackgroundColor:[UIColor colorWithHexString:@"d1d1d1"]];
                                             [btn setEnabled:NO];
                                             if ([approved isEqualToString:@"1"]) {
                                                 [btn setTitle:@"已同意" forState:UIControlStateNormal];
                                             }
                                             if([approved isEqualToString:@"0"]){
                                                 [btn setTitle:@"已拒绝" forState:UIControlStateNormal];
                                             }
                                         }else{
                                             hud.mode = MBProgressHUDModeText;
                                             [hud setLabelText:[JSON objectForKey:@"describe"]];
                                             [hud hide:YES afterDelay:1.5];
                                         }
                                     }
                           failure:^(NSError *error) {
                           }];
        

        //根据剧id获取剧的详情

    }

}

-(UIView*)buttomView
    {
    UIView * bgView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-64-40, ScreenWidth, 40)];
    [bgView setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
    UIButton * agreeBtn = [[UIButton alloc]init];
    [agreeBtn  addTarget:self action:@selector(optBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [agreeBtn setBackgroundColor:[UIColor colorWithHexString:@"eb71a8"]];
    [agreeBtn setTitle:@"同意" forState:UIControlStateNormal];
    agreeBtn.tag = 666;
    agreeBtn.layer.cornerRadius = 4;
    [agreeBtn setClipsToBounds:YES];
    [bgView addSubview:agreeBtn];
    [agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView.mas_left).offset = 50;
        make.top.equalTo(bgView.mas_top).offset = 6;
        make.width.equalTo(@85);
        make.bottom.equalTo(bgView.mas_bottom).offset = -6;
    }];
    
    UIButton * rejectBtn = [[UIButton alloc]init];
    [rejectBtn addTarget:self action:@selector(optBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [rejectBtn setBackgroundColor:[UIColor colorWithHexString:@"d1d1d1"]];
    [rejectBtn setTitle:@"拒绝" forState:UIControlStateNormal];
    rejectBtn.tag = 999;
    rejectBtn.layer.cornerRadius = 4;
    [rejectBtn setClipsToBounds:YES];
    [bgView addSubview:rejectBtn];
    [rejectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView.mas_right).offset = -50;
        make.top.equalTo(bgView.mas_top).offset = 6;
        make.width.equalTo(@85);
        make.bottom.equalTo(bgView.mas_bottom).offset = -6;
    }];
    return bgView;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 3){
        YXRoleReviewHeaderCell * cell = [tableView dequeueReusableCellWithIdentifier:@"YXRoleReviewHeaderCell"];
        if(!cell){
            cell = [[[NSBundle mainBundle]loadNibNamed:@"YXRoleReviewHeaderCell" owner:nil options:nil]lastObject];
        }
        Applyrolenatures * app= [self.entity.applyrolenatures objectAtIndex:indexPath.row];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setContentWithTitle:[NSString stringWithFormat:@"%@ : %@",app.name,app.value]];
            return cell;
    }
    if(indexPath.section == 4){
        YXRoleReviewXiWenCell * cell  = [[[NSBundle mainBundle]loadNibNamed:@"YXRoleReviewXiWenCell" owner:nil options:nil]lastObject];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setContentWithStr:self.entity.applytext];
        return cell;
    }
    return [[UITableViewCell alloc]init];
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}
@end
