//
//  YXMyCenterViewController.h
//  inface
//
//  Created by 邢程 on 15/8/25.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  FriendModel;
@class  GroupModel;
@class  FriendGroupModel;
typedef NS_ENUM(NSInteger, YXMyCenterViewControllerType) {
    YXMyCenterViewControllerTypeFromTabBar,
    YXMyCenterViewControllerTypeFromOtherPage,
};

@interface YXMyCenterViewController : BaseViewController

@property(assign,nonatomic)     BOOL  isAnimating;

-(void)setFromType:(YXMyCenterViewControllerType)fromType andPersonID:(NSString*)personID;
-(void)setFriendModel:(FriendModel *)fModel groupModel:(GroupModel *)groupModel fgModel:(FriendGroupModel*)friendgModel;
-(void)initData;
-(void)setChatSession:(SessionEntity *)chatSession;
@end
