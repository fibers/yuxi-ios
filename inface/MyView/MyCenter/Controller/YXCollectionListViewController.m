//
//  YXCollectionListViewController.m
//  inface
//
//  Created by 邢程 on 15/8/28.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXCollectionListViewController.h"
#import "Mycollects.h"
#import "YXCollectionListTableViewCell.h"
#import "DramaDetailViewController.h"
@interface YXCollectionListViewController () <UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView * tableView;
@property(nonatomic,strong)NSArray * collectionArr;
@end

@implementation YXCollectionListViewController
-(instancetype)init{
    if (self = [super init]) {
       [self.view addSubview:self.tableView];
        self.navigationItem.title = @"收藏";
        self.hidesBottomBarWhenPushed = YES;
        _collectionArr = [NSArray array];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)setContenWith:(NSArray *)collectionArr{
//     NSAssert([[collectionArr lastObject]isKindOfClass:[Mycollects class]], @"类型错误");
    _collectionArr = collectionArr;
}
#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.collectionArr count];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 107;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    YXCollectionListTableViewCell * cell ;
    cell = [tableView dequeueReusableCellWithIdentifier:@"YXCollectionListTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"YXCollectionListTableViewCell" owner:self options:nil]lastObject];
    }
    [cell setContentWithMycollects:[self.collectionArr objectAtIndex:indexPath.row]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Mycollects * collection = [self.collectionArr objectAtIndex:indexPath.row];
    if ([collection.type isEqualToString:@"0"]) {
        WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc] init];
        detailViewController.storyID = collection.id;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }else{
        DramaDetailViewController * ddVC = [[DramaDetailViewController alloc]init];
        ddVC.storyID = collection.id;
        ddVC.hidesBottomBarWhenPushed = YES;
        if (ddVC.isAnimating) {
            return;
        }
        ddVC.isAnimating = YES;
        [self.navigationController pushViewController:ddVC animated:YES];
    }
    
    
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}

@end
