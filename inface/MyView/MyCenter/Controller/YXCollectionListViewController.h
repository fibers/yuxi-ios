//
//  YXCollectionListViewController.h
//  inface
//
//  Created by 邢程 on 15/8/28.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXCollectionListViewController : UIViewController
@property(assign,nonatomic)    BOOL   isAnimating;

-(void)setContenWith:(NSArray *)collectionArr;
@end
