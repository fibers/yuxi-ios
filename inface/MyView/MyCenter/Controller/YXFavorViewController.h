//
//  YXFavorViewController.h
//  inface
//
//  Created by 邢程 on 15/8/28.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Myfavor;
@interface YXFavorViewController : UIViewController
@property(assign,nonatomic)    BOOL   isAnimating;

-(void)setContentWithMyFavor:(NSArray *)myFavor;
@end
