//
//  YXApplicationRoleViewController.m
//  inface
//
//  Created by lizhen on 15/11/6.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXApplicationRoleViewController.h"
#import "YXApplicationRoleCell.h"
#import "ApplicationRole.h"
#import "ApplicationRoleFristModel.h"
#import "MyCenter.h"
#import "MBProgressHUD+YXStyle.h"
#import "YXRoleReviewViewController.h"
#import "MBProgressHUD.h"

@interface YXApplicationRoleViewController ()

@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *dataArray;

@property (strong,nonatomic) MyCenter *myCenter;

@property (nonatomic,strong)YXApplicationRoleCell * cell;

@property(strong,nonatomic)NSArray * familyArr;

@end

@implementation YXApplicationRoleViewController

-(instancetype)init{

    self = [super init];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [[NSMutableArray alloc]init];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];

    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:_tableView];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    
    [self createLeftButton];
    [self refreshData];
    // Do any additional setup after loading the view.
}

-(void)createLeftButton{
    
    self.navigationItem.title = @"角色申请";
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    LeftButton.tag = 12;
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(click_plusBtn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
}

-(void)click_plusBtn{

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataArray count];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

///刷新
-(void)refreshData{

    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    [HttpTool postWithPath:@"GetMyStoryRoleApplications" params:@{ @"uid":USERID,@"token":@"110"} success:^(id JSON) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        ApplicationRoleFristModel *fristModel = [ApplicationRoleFristModel objectWithKeyValues:JSON];
        _dataArray = [[NSMutableArray alloc]init];
        [_dataArray addObjectsFromArray:fristModel.list];
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [MBProgressHUD showYXHudShowTextAddToView:self.view Title:@"加载失败，请重试。" andHideTime:1.5];
    }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 120.0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     _cell = [tableView dequeueReusableCellWithIdentifier:@"applicationRoleCell"];
    if (!_cell) {
        _cell = [[[NSBundle mainBundle]loadNibNamed:@"YXApplicationRoleCell" owner:nil options:nil]lastObject];
    }
    _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [_cell setContentWithTopicModel:[self.dataArray objectAtIndex:indexPath.row]];
    
    return _cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
      ApplicationRole *applicationRoleModel = [[ApplicationRole alloc]init];
    applicationRoleModel = [self.dataArray objectAtIndex:indexPath.row];
    YXRoleReviewViewController *vc = [[YXRoleReviewViewController alloc]init];
    [vc setApplyID:applicationRoleModel.applyid];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{

    [self refreshData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
