//
//  YXFavorViewController.m
//  inface
//
//  Created by 邢程 on 15/8/28.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXFavorViewController.h"
#import "YXCollectionListTableViewCell.h"
#import "Myfavor.h"
#import "WorldDetailViewController.h"
#import "TopicDetailViewController.h"
#import "YXTopicDetailViewController.h"
@interface YXFavorViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSArray * myFavorArr;
@end

@implementation YXFavorViewController
-(instancetype)init{
    if (self = [super init]) {
        [self.view addSubview:self.tableView];
        [self.navigationItem setTitle:@"喜欢"];
        [self setHidesBottomBarWhenPushed:YES];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)setContentWithMyFavor:(NSArray *)myFavor{
    _myFavorArr = myFavor;

}
#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.myFavorArr count];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 107;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Myfavor* myFavor = [self.myFavorArr objectAtIndex:indexPath.row];
    YXCollectionListTableViewCell * cell ;
    cell = [tableView dequeueReusableCellWithIdentifier:@"YXCollectionListTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"YXCollectionListTableViewCell" owner:self options:nil]lastObject];
    }
    [cell setContentWithMyFavor:myFavor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Myfavor * favor = [self.myFavorArr objectAtIndex:indexPath.row];
    if ([favor.type isEqualToString:@"0"]) {//剧
        WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc] init];
        detailViewController.storyID = favor.id;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }else if([favor.type isEqualToString:@"1"]){//自戏
        DramaDetailViewController * detailViewController = [[DramaDetailViewController alloc] init];
        detailViewController.storyID= favor.id;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    
    }else if([favor.type isEqualToString:@"2"]){//话题
//        TopicDetailViewController * detailViewController = [[TopicDetailViewController alloc] init];
//        detailViewController.storyID= favor.id;
//        detailViewController.hidesBottomBarWhenPushed=YES;
//        if (detailViewController.isAnimating) {
//            return;
//        }
//        detailViewController.isAnimating = YES;
//        [self.navigationController pushViewController:detailViewController animated:YES];
        YXTopicDetailViewController *topicDetaile = [[YXTopicDetailViewController alloc]init];
        [topicDetaile setType:YXTopicDetailTypeDetail];
        [topicDetaile setTopicID:favor.id];
        [self.navigationController pushViewController:topicDetaile animated:YES];
    }

}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}
@end
