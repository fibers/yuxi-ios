//
//  YXMyCenterViewController.m
//  inface
//
//  Created by 邢程 on 15/8/25.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXMyCenterViewController.h"
#import "MyCenter.h"
#import "YXMyCenterHeader.h"
#import "MBProgressHUD+YXStyle.h"
#import "Masonry.h"
#import "YXMyCenterGroupView.h"
#import "YXMyCenterCell.h"
#import "PersonalSetViewController.h"
#import "SystemSetViewController.h"
#import "PageTableViewCell.h"
#import "MsgDeleteFriendReqAPI.h"
#import "YXMyCenterButtomView.h"
#import "ModifyFriendNameViewController.h"
#import "ReportFriendViewController.h"
#import "YXFamilyListViewController.h"
#import "YXCollectionListViewController.h"
#import "YXFavorViewController.h"
#import "ModifyFriendController.h"
#import "FriendGroupModel.h"
#import "YXTopicDetailViewController.h"
#import "YXApplicationRoleViewController.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#define TableViewReusepPageTableViewCellID @"TableViewReusepPageTableViewCellID"
@interface YXMyCenterViewController () <UITableViewDelegate,UITableViewDataSource,YXMyCenterGroupViewDelegate,YXMyCenterButtomViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,YXMyCenterHeaderDelegate>
@property (strong,nonatomic ) UITableView                  * tableView;
@property (strong,nonatomic ) MyCenter                     * myCenter;
@property (assign,nonatomic ) YXMyCenterViewControllerType type;
@property (copy,nonatomic   ) NSString                     * persionID;
@property (strong,nonatomic ) YXMyCenterGroupView          * groupView;
@property (strong,nonatomic ) FriendModel                  * fModel;
@property (strong,nonatomic ) GroupModel                   * gModel;
@property (strong,nonatomic ) FriendGroupModel             * fgModel;
@property (strong,nonatomic ) YXMyCenterHeader             * headerV;
@property (assign,nonatomic ) BOOL                         isNeedChatting;
@property (strong,nonatomic ) SessionEntity                * session;
@property (assign, nonatomic) NSInteger                    updataCount;

@property(nonatomic,strong)UIButton * tmpBtn;
///当前选择的cell 类型
@property(assign,nonatomic)PageTableViewType cellType;
@end

@implementation YXMyCenterViewController
-(instancetype)init{
    if (self=[super init]) {
        _cellType = PageTableViewMyStoryType;
        _type = YXMyCenterViewControllerTypeFromTabBar;
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]    ;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self refreshData];
    
    
}

-(void)setChatSession:(SessionEntity *)chatSession;
{
    _isNeedChatting = YES;
    _session = chatSession;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if(_type != YXMyCenterViewControllerTypeFromTabBar){
        [self initData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
/**
 *  渲染界面。（数据请求完成后才可以调用）
 */
-(void)renderView{
    self.tableView.tableHeaderView = [self headerView];
    [self.view addSubview:self.tableView];
    [self setUpTabBar];
    
    int isVip = [self.myCenter.personinfo.portrait isSignedUser];
    if (isVip == 1 || isVip == 3) {
        [self addAtestationView];
    }

}
/**
 *  添加签约标记
 */
-(void)addAtestationView{
    UIImageView * imgV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 120, 72)];
    [imgV setImage:[UIImage imageNamed:@"myCenter_qianyue"]];
    [self.tableView addSubview:imgV];
    imgV.frame = CGRectMake(ScreenWidth - 120 - 6, 66, 120, 72);
}

-(void)setNavBar{
    if (_type == YXMyCenterViewControllerTypeFromTabBar) {
        self.navigationItem.title = @"我的主页";
        UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"编辑资料" style:UIBarButtonItemStylePlain target:self action:@selector(editInfo)];
        [rightBtnItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]} forState:UIControlStateNormal];
        UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MyCenter_setting"] style:UIBarButtonItemStylePlain target:self action:@selector(systemSetting)];
        self.navigationItem.rightBarButtonItem = rightBtnItem;
        self.navigationItem.leftBarButtonItem = leftBtnItem;
    }else{
        self.navigationItem.title = @"TA的主页";
        UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MyCenter_nav_rightNav"] style:UIBarButtonItemStylePlain target:self action:@selector(reportAction:)];
        NSString * isFriend = _myCenter.personinfo.isfriend;
        if ([isFriend isEqualToString:@"1"]) {
            self.navigationItem.rightBarButtonItem = rightBtnItem;
            
        }
        UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
        UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
        [LeftButton setImage:LeftImage forState:UIControlStateNormal];
        [LeftButton addTarget:self action:@selector(popView) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
        self.navigationItem.leftBarButtonItem=barback;
        //        UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MyCenter_nav_lift"] style:UIBarButtonItemStylePlain target:self action:@selector(popView)];
        //        self.navigationItem.ba = leftBtnItem;
    }
    
}

-(void)popView{
    [self.navigationController popViewControllerAnimated:YES];
    if (_isNeedChatting) {
        [[ChatViewController shareInstance]showChattingContentForSession:_session];
    }
}
///设置tabBar  加好友 删除好友等功能
-(void)setUpTabBar{
    if (_type == YXMyCenterViewControllerTypeFromOtherPage && ![self.persionID isEqualToString:USERID]) {
        YXMyCenterButtomView * buttomV = [[[NSBundle mainBundle]loadNibNamed:@"YXMyCenterButtomView" owner:self options:nil]lastObject];
        buttomV.delegate = self;
        [buttomV setIsFriend:[self.myCenter.personinfo.isfriend isEqualToString:@"1"]];
        [buttomV setMaserFlag:[self.myCenter.personinfo.addMasterStatus intValue] andIsMaster:[self.myCenter.personinfo.masterOrApprentice isEqualToString:@"master"]];
        [self.view addSubview:buttomV];
        [buttomV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view.mas_bottom);
            make.width.equalTo(self.view.mas_width);
            make.left.equalTo(self.view.mas_left);
            make.height.equalTo(@50.0f);
        }];
        [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 50, 0)];
    }
    if (_type == YXMyCenterViewControllerTypeFromTabBar) {
        [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 44, 0)];
    }
    
}
#pragma mark -
#pragma mark ButtomView Delegate
-(void)chatButtonClicked:(UIButton *)btn{
    [self chat];
}
-(void)delButtonClicked:(UIButton *)btn{
   
    UIAlertView * delAlertV = [[UIAlertView alloc]initWithTitle:@"注意！" message:@"是否删除好友" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    delAlertV.tag = 666;
    [delAlertV show];
    
}
-(void)addButtonClicked:(UIButton *)btn{
    [self addFriend];
}
-(void)dissMissShiTu:(UIButton *)btn{
    UIActionSheet * actionSheet = [[UIActionSheet alloc]initWithTitle:@"确定删除师徒关系吗？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
    actionSheet.tag = 501;
    [actionSheet showInView:self.view];
}
-(void)baiShi:(UIButton *)btn{
     self.tmpBtn = btn;
    UIActionSheet * actionSheet = [[UIActionSheet alloc]initWithTitle:@"确定拜他为师吗？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
    actionSheet.tag = 601;
    [actionSheet showInView:self.view];
}
-(void)cencleAddMaster:(UIButton *)btn{
    UIActionSheet * actionSheet = [[UIActionSheet alloc]initWithTitle:@"确定取消拜师吗？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
    actionSheet.tag = 701;
    [actionSheet showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) return;
    if (actionSheet.tag == 501) {
        MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud show:YES];
        [HttpTool postWithPath:@"DeleteMasterOrApprentice" params:@{@"uid":USERID,@"peerId":self.persionID,@"token":@"110"} success:^(id JSON) {
            [hud setMode:MBProgressHUDModeText];
            [hud setLabelText:[JSON objectForKey:@"describe"]];
            [hud hide:YES afterDelay:2.0f];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"GETFRIENDLIST" object:nil];
            [self.navigationController popViewControllerAnimated:YES];

        } failure:^(NSError *error) {
            
        }];
        
    }
    if (actionSheet.tag == 601) {//拜师
                MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [hud show:YES];
                NSDictionary * params = @{@"uid":USERID,@"masterId":self.persionID,@"token":@"110"};
                [HttpTool postWithPath:@"RequireAddMaster" params:params success:^(NSDictionary* JSON) {
                    
                    [hud setMode:MBProgressHUDModeText];
                    [hud setLabelText:[JSON objectForKey:@"describe"]];
                    [hud hide:YES afterDelay:2.0f];
                    if([[JSON objectForKey:@"result"]isEqualToString:@"1"])
                    {
                        [self.tmpBtn setEnabled:NO];
                    }
                    
                } failure:^(NSError *error) {
                    
                }];

    }
    if(actionSheet.tag == 701){
        MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud show:YES];
        NSDictionary * params = @{@"uid":USERID,@"masterId":self.persionID,@"token":@"110"};
        [HttpTool postWithPath:@"CancelAddMaster" params:params success:^(NSDictionary* JSON) {
            
            [hud setMode:MBProgressHUDModeText];
            [hud setLabelText:[JSON objectForKey:@"describe"]];
            [hud hide:YES afterDelay:2.0f];
            [self.tmpBtn setEnabled:NO];
            
        } failure:^(NSError *error) {
            
        }];

        
    }

}
#pragma mark -
#pragma mark navBar上的功能
#pragma mark举报
- (void)reportAction:(UIButton *)btn
{
    //    NSArray *menuItems;
    //    if( [self.myCenter.personinfo.isfriend isEqualToString:@"1"])
    //    {
    //        menuItems  =
    //        @[
    //          [KxMenuItem menuItem:@"修改备注名"
    //                         image:[UIImage imageNamed:@"举报"]
    //                        target:self
    //                        action:@selector(modifyFriendName:)],
    //
    //          [KxMenuItem menuItem:@"举报"
    //                         image:[UIImage imageNamed:@"举报"]
    //                        target:self
    //                        action:@selector(reportPerson:)]
    //
    //          ];
    //
    //    }else{
    //        menuItems =
    //        @[
    //
    //
    //          [KxMenuItem menuItem:@"举报"
    //                         image:[UIImage imageNamed:@"举报"]
    //                        target:self
    //                        action:@selector(reportPerson:)]
    //          ];
    //
    //    }
    //
    //
    //    KxMenuItem *first = menuItems[0];
    //    first.alignment = NSTextAlignmentLeft;
    //
    //    [KxMenu showMenuInView:self.navigationController.view
    //                  fromRect:CGRectMake(ScreenWidth-45, 0, 25, 44)
    //                 menuItems:menuItems];
    
    ModifyFriendController * detailController = [[ModifyFriendController alloc]init];
    
    detailController.hidesBottomBarWhenPushed = YES;
    if (_gModel != NULL && ![_gModel isEqual:nil]) {
        [detailController setFriendModel:_fModel groupModel:_gModel fgModel:_fgModel];
        if (detailController.isAnimating) {
            return;
        }
        detailController.isAnimating = YES;

        [self.navigationController pushViewController:detailController animated:YES];
        
    }else{
        
        //需要获取一下
        NSDictionary * params = @{@"uid":USERID,@"token":@"110"};
        [HttpTool postWithPath:@"GetFriendListWithGroup" params:params success:^(id JSON) {
            _fgModel = [FriendGroupModel objectWithKeyValues:JSON];
            NSArray * gmArr = _fgModel.friendgroups;//GroupModel数组
            for (int i = 0; i < gmArr.count; i++) {
                GroupModel * gModel = gmArr[i];
                for (int j =0; j < gModel.groupmembers.count; j++) {
                    FriendModel * fModel = gModel.groupmembers[j];
                    if ([fModel.userid isEqualToString:self.persionID]) {
                        _fModel = fModel;
                        _gModel = gModel;
                        [detailController setFriendModel:_fModel groupModel:_gModel fgModel:_fgModel];
                        if (detailController.isAnimating) {
                            return;
                        }
                        detailController.isAnimating = YES;

                        [self.navigationController pushViewController:detailController animated:YES];
                    }
                }
                
            }
            
        } failure:^(NSError *error) {
            
            
        }];
    }
    
    
    [detailController returnGroupModel:^(GroupModel *groupModel,FriendGroupModel*fg_model) {
        _gModel = groupModel;
        _fgModel = fg_model;
        
    }];
    
    
}


#pragma mark 设置FriendModel
-(void)setFriendModel:(FriendModel *)fModel groupModel:(GroupModel *)groupModel fgModel:(FriendGroupModel *)friendgModel
{
    _fModel =fModel;
    
    _gModel = groupModel;
    
    _fgModel = friendgModel;
}

#pragma mark 修改备注姓名
-(void)modifyFriendName:(UIButton *)sender
{
    
    ModifyFriendNameViewController * detailerView = [[ModifyFriendNameViewController alloc]init];
    detailerView.hidesBottomBarWhenPushed = YES;
    detailerView.friendId = self.persionID;
    [detailerView returnText:^(NSString *showText) {
        //        self.headerView.UserNameLabel.text = showText;
        
    }];
    if (detailerView.isAnimating) {
        return;
    }
    detailerView.isAnimating = YES;

    [self.navigationController pushViewController:detailerView animated:YES];
    
}

-(void)reportPerson:(UIButton*)sender
{
    //举报
    ReportFriendViewController * detailViewController = [[ReportFriendViewController alloc] init];
    detailViewController.hidesBottomBarWhenPushed=YES;
    detailViewController.reportid = self.persionID;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


#pragma mark 取消按钮
-(void)deselectMenu
{
    
}


#pragma mark 聊天
///聊天
-(void)chat{
    NSString * frId = self.myCenter.personinfo.id;
    
    NSString * friendName = self.myCenter.personinfo.nickname;
    NSString *user_id = [NSString stringWithFormat:@"user_%@",frId];
    SessionEntity * sessionEntity = [[SessionModule sharedInstance]getSessionById:user_id];
    if (!sessionEntity) {
        sessionEntity = [[SessionEntity alloc]initWithSessionID:user_id SessionName:friendName type:SessionTypeSessionTypeSingle];
    }
    
    NSArray * viewControllers = self.navigationController.viewControllers;
    
    NSString *havechat=@"0";
    for (UIViewController * aViewController in viewControllers) {
        
        if ([aViewController isKindOfClass:[ChatViewController class]]) {
            havechat=@"1";
        }
    }
    ChatViewController * detailViewController = [ChatViewController shareInstance];
    if ([havechat isEqualToString:@"0"]) {
        detailViewController.hidesBottomBarWhenPushed=YES;
        [detailViewController showChattingContentForSession:sessionEntity];
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;

        [self.navigationController pushViewController:detailViewController animated:YES];
        
    } else {
        self.navigationController.navigationBar.hidden=NO;
        detailViewController.hidesBottomBarWhenPushed=YES;
        [detailViewController showChattingContentForSession:sessionEntity];
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;

        [self.navigationController popToViewController:(ChatViewController*)detailViewController animated:YES];
    }
}

#pragma mark 添加好友
///添加好友
-(void)addFriend{
    
    [self.view endEditing:YES];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"验证消息" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"发送", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = @"请输入20字以内的验证信息";
    alert.tag = 20000;
    
    [alert show];
    
    //    MsgAddFriendReqAPI * addFriendReq = [[MsgAddFriendReqAPI alloc]init];
    //
    //    [addFriendReq requestWithObject:@[preStr,@(0),@(SessionTypeSessionTypeSingle)] Completion:^(id response, NSError *error) {
    //
    //
    //    }];
    //    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请求已发送" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    //    [alertview show];
}
///删除好友
-(void)delFriend{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
#pragma mark 删除好友
    MsgDeleteFriendReqAPI * deleteFriendReq = [[MsgDeleteFriendReqAPI alloc]init];
    NSMutableString * tofriendId = [NSMutableString stringWithString:@"user_"];
    NSString * fid = _persionID;
    [tofriendId appendString:fid];
    [deleteFriendReq requestWithObject:@[tofriendId,@(0),@(SessionTypeSessionTypeSingle)] Completion:^(id response, NSError *error) {
        
    }];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:DELETE_FRIEND object:fid];
    
    //****将好友列表的好友干掉，将会话体删掉，将数据库的添加信息删除
    
    [[DDDatabaseUtil instance]clearUpAddMessages:fid toid:userid groupid:@"-2" type:@"-2"];
    self.navigationController.navigationBar.hidden= NO;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    NSString * deleteTitle = [NSString stringWithFormat:@"您已删除好友"];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:deleteTitle message:nil delegate:nil cancelButtonTitle:@"好的" otherButtonTitles: nil];
    [alert show];
}
///编辑个人信息
-(void)editInfo{
    
    PersonalSetViewController * detailViewController = [[PersonalSetViewController alloc] init];
//    detailViewController.shuaxinImage = YES;
    detailViewController.imgSerUrl = [self.myCenter.personinfo.portrait stringOfW250ImgUrl];
    [detailViewController setContentWithModel:self.myCenter];
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;

    [self.navigationController pushViewController:detailViewController animated:YES];
}
///系统设置
-(void)systemSetting{
    SystemSetViewController * detailViewController = [[SystemSetViewController alloc] init];
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}
-(UIView *)headerView{
    self.headerV = [[[NSBundle mainBundle]loadNibNamed:@"YXMyCenterHeader" owner:self options:nil]lastObject];
    self.headerV.delegate = self;
    [self.headerV setContentWithPersonifno:self.myCenter.personinfo];
    return self.headerV;
}

-(void)centerGroupViewClickedWithItem:(UILabel *)label{
    if (label.tag == 1) {
        _cellType = PageTableViewMyStoryType;
    }else if(label.tag ==2){
        _cellType = PageTableViewMyselfStoryType;
    }else if(label.tag ==3){
        _cellType = PageTableViewTypeMyTopicType;
    }
    [self.tableView reloadData];
}
#pragma mark -
#pragma mark 数据请求
-(void)initData{
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary *params = @{@"uid":userid,
                             @"personid":_persionID ==nil?userid:_persionID,
                             @"token":@"110"};
    [HttpTool postWithPath:@"GetPersonInfo" params:params success:^(id JSON) {
        _myCenter = [MyCenter objectWithKeyValues:JSON];
        if ([_myCenter.personinfo.isfriend isEqualToString:@"1"]) {
            //更新存储的好友信息
            NSString * user_id = [NSString stringWithFormat:@"user_%@",_persionID ==nil?userid:_persionID];
            [[DDUserModule shareInstance]getUserForUserID:user_id Block:^(DDUserEntity *user) {
                user.name = _myCenter.personinfo.nickname;
                
                user.avatar = _myCenter.personinfo.portrait;
                
                [[DDUserModule shareInstance]changeOtherPersonInfo:user_id userEntiry:user];
                
            }];
        }
        [self renderView];
        [self setNavBar];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showYXHudShowTextAddToView:self.view Title:@"加载失败，请重试。" andHideTime:1.5];
        
    }];

}

int tmp =0;

-(void)refreshData{
    if (tmp == 0) {
        tmp++;
        return;
    }
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if (!userid) {
        return;
    }
    NSDictionary *params = @{@"uid":userid,
                             @"personid":_persionID ==nil?userid:_persionID,
                             @"token":@"110"};
    [HttpTool postWithPath:@"GetPersonInfo" params:params success:^(id JSON) {
        _myCenter = [MyCenter objectWithKeyValues:JSON];
        [self.tableView reloadData];
        [self.headerV setContentWithPersonifno:self.myCenter.personinfo];
    } failure:^(NSError *error) {
        [MBProgressHUD showYXHudShowTextAddToView:self.view Title:@"加载失败，请重试。" andHideTime:1.5];
        
    }];
}
#pragma mark -
#pragma mark delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section ==0) {
        if (_persionID == nil || [_persionID isEqualToString:USERID]) {
            return 5;
        }else{
            return 4;
        }
        
    }else if(section == 1){
        if(_cellType == PageTableViewMyStoryType){//如果是我的剧
            return [self.myCenter.personinfo.mystorys count];
        }else if(_cellType == PageTableViewMyselfStoryType){//如果是我的自戏
            return [self.myCenter.personinfo.myselfstorys count];
        }else if(_cellType == PageTableViewTypeMyTopicType){//如果是我的话题
            return [self.myCenter.personinfo.mytopics count];
        }
    }
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        YXMyCenterCell * cell = [[[NSBundle mainBundle]loadNibNamed:@"YXMyCenterCell" owner:self options:nil]lastObject];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        switch (indexPath.row) {
            case 0:
                [cell setContentWithType:YXMyCenterTypeModeWord titleString:@"创作字数" andDescString:[NSString stringWithFormat:@"%@ 字",self.myCenter.personinfo.wordcount]];
                break;
            case 1:
                
                [cell setContentWithType:YXMyCenterTypeModeFamily titleString:@"家族" andDescString:[NSString stringWithFormat:@"%ld 个",(unsigned long)[self.myCenter.personinfo.myfamily count]]];
                break;
            case 2:
                
                [cell setContentWithType:YXMyCenterTypeModeCollection titleString:@"收藏" andDescString:[NSString stringWithFormat:@"%ld 个",(unsigned long)[self.myCenter.personinfo.mycollects count]]];
                break;
            case 3:
                [cell setContentWithType:YXMyCenterTypeModeFavor titleString:@"喜欢" andDescString:[NSString stringWithFormat:@"%ld 个",(unsigned long)[self.myCenter.personinfo.myfavor count]]];
                break;
            case 4:
                [cell setContentWithType:YXMyCenterTypeModeApplication titleString:@"角色申请" andDescString:[NSString stringWithFormat:@"%@ 个",self.myCenter.personinfo.applycount ]];
                break;
                
            default:
                break;
        }
        return cell;
        
    }else if(indexPath.section ==1){
        PageTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:TableViewReusepPageTableViewCellID];
        if(!cell){
            cell = [[[NSBundle mainBundle] loadNibNamed:@"PageTableViewCell" owner:self options:nil]lastObject];
        }
        if(_cellType == PageTableViewMyStoryType){//如果是我的剧
            [cell setContentWithType:self.cellType andModel:[self.myCenter.personinfo.mystorys objectAtIndex:indexPath.row]];
        }else if(_cellType == PageTableViewMyselfStoryType){//如果是我的自戏
            [cell setContentWithType:self.cellType andModel:[self.myCenter.personinfo.myselfstorys objectAtIndex:indexPath.row]];
        }else if(_cellType == PageTableViewTypeMyTopicType){//如果是我的话题
            [cell setContentWithType:self.cellType andModel:[self.myCenter.personinfo.mytopics objectAtIndex:indexPath.row]];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    return [[UITableViewCell alloc]init];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section ==1) {
        return 60;
    }
    return 37;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return [[UIView alloc]init];
    }else{
        self.groupView.delegate = self;
        if (_type == YXMyCenterViewControllerTypeFromTabBar){
            [self.groupView setContentWithTitleOne:[NSString stringWithFormat:@"%ld\n%@",(unsigned long)[self.myCenter.personinfo.mystorys count],@"我的剧"] titleTwo:[NSString stringWithFormat:@"%ld\n%@",(unsigned long)[self.myCenter.personinfo.myselfstorys count],@"我的自戏"] andTitleThree:[NSString stringWithFormat:@"%ld\n%@",(unsigned long)[self.myCenter.personinfo.mytopics count],@"我的话题"]];
        }else{
            [self.groupView setContentWithTitleOne:[NSString stringWithFormat:@"%ld\n%@",(unsigned long)[self.myCenter.personinfo.mystorys count],@"TA的剧"] titleTwo:[NSString stringWithFormat:@"%ld\n%@",(unsigned long)[self.myCenter.personinfo.myselfstorys count],@"TA的自戏"] andTitleThree:[NSString stringWithFormat:@"%ld\n%@",(unsigned long)[self.myCenter.personinfo.mytopics count],@"TA的话题"]];
        }
        return self.groupView;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 51;
}

-(YXMyCenterGroupView *)groupView{
    if (!_groupView) {
        _groupView = [[[NSBundle mainBundle]loadNibNamed:@"YXMyCenterGroupView" owner:self options:nil]lastObject];
    }
    return _groupView;
}
//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        if (indexPath.row == 1) {
            YXFamilyListViewController * familyVC = [[YXFamilyListViewController alloc]init];
            [familyVC setContenWith:self.myCenter.personinfo.myfamily];
            if (familyVC.isAnimating) {
                return;
            }
            familyVC.isAnimating = YES;

            [self.navigationController pushViewController:familyVC animated:YES];
        }else if(indexPath.row == 2){
            YXCollectionListViewController * clVC = [[YXCollectionListViewController alloc]init];
            [clVC setContenWith:self.myCenter.personinfo.mycollects];
            if (clVC.isAnimating) {
                return;
            }
            clVC.isAnimating = YES;

            [self.navigationController pushViewController:clVC animated:YES];
            
        }else if(indexPath.row == 3){
            YXFavorViewController *favorVC = [[YXFavorViewController alloc]init];
            [favorVC setContentWithMyFavor:self.myCenter.personinfo.myfavor];
            if (favorVC.isAnimating) {
                return;
            }
            favorVC.isAnimating = YES;

            [self.navigationController pushViewController:favorVC animated:YES];
        }else if (indexPath.row == 4){
            YXApplicationRoleViewController *applyVC = [[YXApplicationRoleViewController alloc]init];
            //[favorVC setContentWithMyFavor:self.myCenter.personinfo.myfavor];
            if (applyVC.isAnimating) {
                return;
            }
            applyVC.isAnimating = YES;
            
            [self.navigationController pushViewController:applyVC animated:YES];
        }
        
        
    }else{
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        if ([selectedCell isKindOfClass:[PageTableViewCell class]]) {
            PageTableViewCell * cell = (PageTableViewCell*)selectedCell;
            if(cell.contentType == PageTableViewMyStoryType){
                WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc] init];
                detailViewController.storyID = cell.storyID;
                detailViewController.hidesBottomBarWhenPushed=YES;
                if (detailViewController.isAnimating) {
                    return;
                }
                detailViewController.isAnimating = YES;
                [self.navigationController pushViewController:detailViewController animated:YES];
                
            }else if(cell.contentType == PageTableViewMyselfStoryType){
                DramaDetailViewController * detailViewController = [[DramaDetailViewController alloc] init];
                detailViewController.storyID= cell.storyID;
                detailViewController.hidesBottomBarWhenPushed=YES;
                if (detailViewController.isAnimating) {
                    return;
                }
                detailViewController.isAnimating = YES;
                [self.navigationController pushViewController:detailViewController animated:YES];
                
            }else if(cell.contentType == PageTableViewTypeMyTopicType){
                YXTopicDetailViewController * detailViewController = [[YXTopicDetailViewController alloc] init];
                [detailViewController setTopicID: cell.storyID];
                [detailViewController setType:YXTopicDetailTypeDetail];
                 [detailViewController setIsTopicView];
                detailViewController.hidesBottomBarWhenPushed=YES;
                if (detailViewController.isAnimating) {
                    return;
                }
                detailViewController.isAnimating = YES;
                [self.navigationController pushViewController:detailViewController animated:YES];
            }
        }
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _isAnimating = NO;
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    _isAnimating = NO;

}

-(void)setFromType:(YXMyCenterViewControllerType)fromType andPersonID:(NSString*)personID{
    _type = fromType;
    _persionID = personID;
    //    [self setNavBar];
}

#pragma mark  头像被点击

-(void)YXMyCenterHeaderClicked:(UIImageView *)imageV{
    IDMPhoto * photo = [IDMPhoto photoWithImage:imageV.image];
    IDMPhotoBrowser * browser = [[IDMPhotoBrowser alloc]initWithPhotos:@[photo] animatedFromView:imageV];
    [self presentViewController:browser animated:YES completion:nil];
}
#pragma mark -
#pragma mark init
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView            = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
        _tableView.delegate   = self;
        _tableView.dataSource = self;
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}

#pragma mark UIAlertView  Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 666) {
        if(buttonIndex == 1){
            [self delFriend];
        }
        
    }
    if (alertView.tag ==20000) {
        
        if (buttonIndex == 1  && [alertView textFieldAtIndex:0].text.length <=20) {
            MsgAddFriendReqAPI * addFriendReq = [[MsgAddFriendReqAPI alloc]init];
            NSMutableString * preStr = [NSMutableString stringWithString:@"user_"];
            NSString * tofriendId = self.persionID;
            if (!tofriendId) {
                UIAlertView * errorAlertV = [[UIAlertView alloc]initWithTitle:@"Error" message:@"请求出错" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [errorAlertV show];
                return;
            }
            [preStr appendString:tofriendId];
            NSString * addmessage = @"";
            if ([alertView textFieldAtIndex:0].text != NULL && ![[alertView textFieldAtIndex:0].text isEqual:nil]) {
                addmessage= [alertView textFieldAtIndex:0].text;
            }
            [addFriendReq requestWithObject:@[preStr,@(0),@(SessionTypeSessionTypeSingle),addmessage] Completion:^(id response, NSError *error) {
                
            }];
            
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"添加请求已发送" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
}

@end
