//
//  YXFamilyListViewController.m
//  inface
//
//  Created by 邢程 on 15/8/28.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXFamilyListViewController.h"
#import "Myfamily.h"
#import "YXFamilyTableViewCell.h"
#import "FamilyInformationViewController.h"
@interface YXFamilyListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView * tableView;
@property(strong,nonatomic)NSArray * familyArr;
@end

@implementation YXFamilyListViewController
#pragma mark -
#pragma mark implementation
-(void)setContenWith:(NSArray *)familyArr{
//    NSAssert([[familyArr lastObject]isKindOfClass:[Myfamily class]], @"类型错误");
    _familyArr = familyArr;
}
#pragma mark -
#pragma mark init
-(instancetype)init{
    if (self = [super init]) {
        [self.view addSubview:self.tableView];
        [self setHidesBottomBarWhenPushed:YES];
        [self.navigationItem setTitle:@"家族"];
        _familyArr = [NSArray array];
    }
    return self;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64)];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}
#pragma mark -
#pragma mark view life cycl
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _isAnimating = NO;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    _isAnimating = NO;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.familyArr count];

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;

}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    YXFamilyTableViewCell * cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:@"YXFamilyTableViewCell"];
    if (!cell) {
        cell =  [[[NSBundle mainBundle]loadNibNamed:@"YXFamilyTableViewCell" owner:self options:nil] lastObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    Myfamily * family = [self.familyArr objectAtIndex:indexPath.row];
    [cell setContentWithModel:family];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Myfamily * family =  [self.familyArr objectAtIndex:indexPath.row];
    FamilyInformationViewController * fiVC = [[FamilyInformationViewController alloc]init];
    fiVC.familyid = family.id;
    if (fiVC.isAnimating) {
        return;
    }
    fiVC.isAnimating = YES;
    [self.navigationController pushViewController:fiVC animated:YES];
}
@end
