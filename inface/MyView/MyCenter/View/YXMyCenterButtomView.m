//
//  YXMyCenterButtomView.m
//  inface
//
//  Created by 邢程 on 15/8/27.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXMyCenterButtomView.h"

typedef NS_ENUM(NSInteger,YXMyCenterButtonStyle) {
    ///拜他为师
    YXMyCenterButtonStyleBaiShiLevel0 = 1,
    ///拜师中
    YXMyCenterButtonStyleBaiShiLevel1 = 2,
    ///拜师完成
    YXMyCenterButtonStyleBaishiLevel2 = 3,
    ///徒弟
    YXMyCenterButtonStyleStudent      = 4,
    ///师傅
    YXMyCenterButtonStyleMaster       = 5,
    ///加好友
    YXMyCenterButtonStyleAddFriend    = 6,
    ///发消息
    YXMyCenterButtonStyleSendMessage  = 7,
    ///删好友
    YXMyCenterButtonStyleDelFriend    = 8,
};
@interface YXMyCenterButtomView()
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *centerBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;

@property (assign,nonatomic) BOOL isFriend;
@end
@implementation YXMyCenterButtomView
-(void)awakeFromNib{
}
-(void)setIsFriend:(BOOL)isFriend{
    _isFriend = isFriend;
}
-(void)setMaserFlag:(NSInteger)masterFlag andIsMaster:(BOOL)isMaster{
    
    
    if (self.isFriend) { //是好友
        [self setButton:self.rightBtn StyleWithButtonType:YXMyCenterButtonStyleSendMessage];
        [self setButton:self.centerBtn StyleWithButtonType:YXMyCenterButtonStyleDelFriend];
        switch (masterFlag) {
            case 0://前
            {
                [self setButton:self.leftBtn StyleWithButtonType:YXMyCenterButtonStyleBaiShiLevel0];
            
            }
                break;
            case 1://ing
            {
                [self setButton:self.leftBtn StyleWithButtonType:YXMyCenterButtonStyleBaiShiLevel1];
            }
                break;
            case 2://后
            {
                if (isMaster) {
                    [self setButton:self.leftBtn StyleWithButtonType:YXMyCenterButtonStyleMaster];
                }else{
                    [self setButton:self.leftBtn StyleWithButtonType:YXMyCenterButtonStyleStudent];
                }
            }
                break;
            default:
                break;
        }
    }else{ //非好友
        [self.centerBtn setHidden:YES];
        [self setButton:self.rightBtn StyleWithButtonType:YXMyCenterButtonStyleAddFriend];
        switch (masterFlag) {
            case 0:
            {
                [self setButton:self.leftBtn StyleWithButtonType:YXMyCenterButtonStyleBaiShiLevel0];
                
            }
                break;
            case 1:
            {
                [self setButton:self.leftBtn StyleWithButtonType:YXMyCenterButtonStyleBaiShiLevel1];
            }
                break;
            default:
                break;
        }
    }
    
}
-(void)add{
    if ([self.delegate respondsToSelector:@selector(addButtonClicked:)]) {
        [self.delegate addButtonClicked:self.centerBtn];
    }
    
}

-(void)del{
    if ([self.delegate respondsToSelector:@selector(delButtonClicked:)]) {
        [self.delegate delButtonClicked:self.leftBtn];
    }
    
}
-(void)chat{
    if ([self.delegate respondsToSelector:@selector(chatButtonClicked:)]) {
        [self.delegate chatButtonClicked:self.rightBtn];
    }
    
    
}
///解散师徒关系
-(void)dissMissShiTu{
    if([self.delegate respondsToSelector:@selector(dissMissShiTu:)]){
        [self.delegate dissMissShiTu:self.leftBtn];
    }
}

-(void)cencleAddMaster{
    if ([self.delegate respondsToSelector:@selector(cencleAddMaster:)]) {
        [self.delegate cencleAddMaster:self.centerBtn];
    }
}
///拜师
-(void)baiShiBtnClicked:(UIButton*)btn{
    LOG(@"12");
    if([self.delegate respondsToSelector:@selector(baiShi:)]){
        [self.delegate baiShi:self.leftBtn];
    }
}
-(void)setButton:(UIButton *)btn StyleWithButtonType:(YXMyCenterButtonStyle)style{
    btn.layer.borderColor = [UIColor clearColor].CGColor;
     btn.layer.borderWidth = 1.0f;
    switch (style) {
        case YXMyCenterButtonStyleBaiShiLevel0:
        {
            [btn setTitle:@"拜TA为师" forState:UIControlStateNormal];
            [btn setTitle:@"拜师中" forState:UIControlStateDisabled];
            [btn setTitleColor:[UIColor colorWithHexString:@"ffa00c"] forState:UIControlStateNormal];
            btn.layer.cornerRadius = 5;
            [btn setClipsToBounds:YES];
            [btn.layer setBorderColor:[UIColor colorWithHexString:@"ffa00c"].CGColor];
            [btn addTarget:self action:@selector(baiShiBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
           
           
        }
            break;
        case YXMyCenterButtonStyleBaiShiLevel1:
        {
            [btn setTitle:@"拜师中" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor colorWithHexString:@"ffa00c"] forState:UIControlStateNormal];
            btn.layer.cornerRadius = 5;
            [btn setClipsToBounds:YES];
            [btn addTarget:self action:@selector(cencleAddMaster) forControlEvents:UIControlEventTouchUpInside];
            [btn.layer setBorderColor:[UIColor colorWithHexString:@"ffa00c"].CGColor];
        }
            break;
        case YXMyCenterButtonStyleBaishiLevel2:
        {
//            [btn setTitle:@"拜师中" forState:UIControlStateNormal];
//            [btn setEnabled:NO];
        }
            break;
        case YXMyCenterButtonStyleStudent:
        {
            btn.layer.cornerRadius = 5;
            [btn setClipsToBounds:YES];
            [btn setTitle:@"徒弟" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor colorWithHexString:@"46c28f"]];
            [btn addTarget:self action:@selector(dissMissShiTu) forControlEvents:UIControlEventTouchUpInside];
            [self.centerBtn setHidden:YES];
        }
            break;
        case YXMyCenterButtonStyleMaster:
        {
            btn.layer.cornerRadius = 5;
            [btn setClipsToBounds:YES];
            [btn setTitle:@"师傅" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor colorWithHexString:@"ffa00c"]];
            [btn addTarget:self action:@selector(dissMissShiTu) forControlEvents:UIControlEventTouchUpInside];
            [self.centerBtn setHidden:YES];
            
        }
            break;
        case YXMyCenterButtonStyleAddFriend:
        {
            [btn setTitle:@"加好友" forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor whiteColor]];
            btn.layer.cornerRadius = 5;
            [btn setClipsToBounds:YES];
            [btn.layer setBorderColor:[UIColor colorWithHexString:@"e2e2e2"].CGColor];
            [btn  addTarget:self action:@selector(add) forControlEvents:UIControlEventTouchUpInside];
            [btn setTitleColor:[UIColor colorWithHexString:@"eb71a8"] forState:UIControlStateNormal];
        }
            break;
        case YXMyCenterButtonStyleSendMessage:
        {
            [btn setTitle:@"发消息" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor colorWithHexString:@"eb71a8"]];
            btn.layer.cornerRadius = 5;
            [btn setClipsToBounds:YES];
            [btn  addTarget:self action:@selector(chat) forControlEvents:UIControlEventTouchUpInside];
        }
            break;
        case YXMyCenterButtonStyleDelFriend:
        {
            [btn setTitle:@"删除好友" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor colorWithHexString:@"eb71a8"] forState:UIControlStateNormal];
            btn.layer.cornerRadius = 5;
            [btn setClipsToBounds:YES];
            [btn.layer setBorderColor:[UIColor colorWithHexString:@"eb71a8"].CGColor];
            [btn  addTarget:self action:@selector(del) forControlEvents:UIControlEventTouchUpInside];
            
        }
            break;
            
        default:
            break;
    }
}
@end
