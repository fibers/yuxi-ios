//
//  PageTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, PageTableViewType) {
    ///我的剧
    PageTableViewMyStoryType,
    PageTableViewMyselfStoryType,
    PageTableViewTypeMyTopicType
};
@interface PageTableViewCell : UITableViewCell
-(void)setContentWithType:(PageTableViewType)type andModel:(id)model;
@property(assign,nonatomic)PageTableViewType contentType;
@property(copy,nonatomic)NSString * storyID;
@end
