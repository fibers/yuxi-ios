//
//  YXRoleReviewXiWenCell.m
//  inface
//
//  Created by 邢程 on 15/11/5.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXRoleReviewXiWenCell.h"
#import "VerticalAlignmentLabel.h"
@interface YXRoleReviewXiWenCell()
@property (weak, nonatomic) IBOutlet UIView *bgV;
@property (weak, nonatomic) IBOutlet VerticalAlignmentLabel *contentL;

@end
@implementation YXRoleReviewXiWenCell
-(void)setContentWithStr:(NSString*)str{
    [self.contentL setText:str];
    self.bgV.layer.borderWidth = 0.5;
    self.bgV.layer.cornerRadius = 4;
    [self.bgV setClipsToBounds:YES];
    self.bgV.layer.borderColor = [UIColor colorWithHexString:@"a4a4a4"].CGColor;
}
- (void)awakeFromNib {
    // Initialization code
}
-(CGFloat)heightWithStr:(NSString *)str{
return     [str boundingRectWithSize:CGSizeMake(ScreenWidth - 54, 99999) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.height+20;
//    CGFloat h =  [str boundingRectWithSize: options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.contentL.font} context:nil].size.height;
//   return h;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
