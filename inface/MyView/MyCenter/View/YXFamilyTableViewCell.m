//
//  YXFamilyTableViewCell.m
//  inface
//
//  Created by 邢程 on 15/8/28.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXFamilyTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "Myfamily.h"
@interface YXFamilyTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *headerImgV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@end
@implementation YXFamilyTableViewCell
-(void)setContentWithModel:(Myfamily*)family{
    [self.headerImgV sd_setImageWithURL:[NSURL URLWithString:family.portrait] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    [self.titleL setText:family.title];
}
- (void)awakeFromNib {
    self.headerImgV.layer.cornerRadius = self.headerImgV.frame.size.height *0.5;
    [self.headerImgV setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
