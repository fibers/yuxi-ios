//
//  YXRoleReviewHeaderView.m
//  inface
//
//  Created by 邢程 on 15/11/5.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXRoleReviewHeaderView.h"
#import "RoleReviewEntity.h"
@interface YXRoleReviewHeaderView()
@property (weak, nonatomic) IBOutlet UIImageView *headerImgV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *timeL;


@end
@implementation YXRoleReviewHeaderView
-(void)setContentWithEntity:(RoleReviewEntity*)entity{
    [self.headerImgV sd_setImageWithURL:[NSURL URLWithString:entity.applicantportrait] placeholderImage:[UIImage imageNamed:@""]];
    self.titleL.text = entity.applicantname;
    self.timeL.text = [entity.applytime dateTimeString];
}
@end
