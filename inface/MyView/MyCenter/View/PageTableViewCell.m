//
//  PageTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/7/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PageTableViewCell.h"
#import "Mystorys.h"
#import "Myselfstorys.h"
#import "Mytopics.h"
@interface PageTableViewCell()
///所有者
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel;
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UILabel *TimeLabel;//时间
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ownerLabelConstraint;

@end
@implementation PageTableViewCell
- (void)awakeFromNib {
    _UserNameLabel.textColor = BLACK_COLOR;
    _TimeLabel.textColor     = GRAY_COLOR;
    _ownerLabel.textColor    = GRAY_COLOR;
}
-(void)layoutSubviews{
    CGSize ownerLabelSize              = [self.ownerLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:10.0f],NSFontAttributeName, nil]];
    CGSize timeLabelSize               = [self.TimeLabel.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:10.0f],NSFontAttributeName, nil]];
    CGFloat ownerLMaxW                 = (ScreenWidth - timeLabelSize.width);
    self.ownerLabelConstraint.constant = ownerLabelSize.width >ownerLMaxW?ownerLMaxW:ownerLabelSize.width;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
-(void)setContentWithType:(PageTableViewType)type andModel:(id)model{
    [self.UserNameLabel setText:@""];
    [self.ownerLabel setText:@""];
    _storyID = @"";
    _contentType = type;
    switch (type) {
        case PageTableViewMyStoryType:{
            if ([model isKindOfClass:[Mystorys class]]) {
                Mystorys * topic        = (Mystorys *)model;
                self.UserNameLabel.text = topic.title;
                NSString *creattime     = topic.createtime;
                NSDate* date            = [NSDate dateWithTimeIntervalSince1970:[creattime doubleValue]];
                NSString* ownerStr      = topic.creatorname;
                [self.ownerLabel setText:ownerStr];
                self.TimeLabel.text     = [date promptDateString];
                _storyID                = topic.storyid;
            }
        
        }
            break;
        case PageTableViewMyselfStoryType:{
            if ([model isKindOfClass:[Myselfstorys class]]) {
                Myselfstorys * myselfStory = (Myselfstorys *)model;
                self.UserNameLabel.text    = myselfStory.title;
                NSString *creattime        = myselfStory.createtime;
                NSDate* date               = [NSDate dateWithTimeIntervalSince1970:[creattime doubleValue]];
                self.TimeLabel.text        = [date promptDateString];
                _storyID                   = myselfStory.storyid;
            }
        
        }
            break;
        case PageTableViewTypeMyTopicType:{
            if ([model isKindOfClass:[Mytopics class]]) {
                Mytopics * mytopics     = (Mytopics *)model;
                self.UserNameLabel.text = mytopics.title;
                NSString *creattime     = mytopics.createtime;
                NSDate* date            = [NSDate dateWithTimeIntervalSince1970:[creattime doubleValue]];
                self.TimeLabel.text     = [date promptDateString];
                _storyID                = mytopics.topicid;
                
            }
        
        }
            break;
            
        default:
            break;
    }
}
@end
