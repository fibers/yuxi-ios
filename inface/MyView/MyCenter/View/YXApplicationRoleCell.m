//
//  YXApplicationRoleCell.m
//  inface
//
//  Created by lizhen on 15/11/6.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXApplicationRoleCell.h"
#import "AFNetworking.h"



@interface YXApplicationRoleCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@property (weak, nonatomic) IBOutlet UIButton *iconButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *applicationRoleLabel;
@property (weak, nonatomic) IBOutlet UILabel *playNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playNameLabelHight;

@property (nonatomic,strong)MyCenter *myCenter;
@property(nonatomic,copy)NSString *ID;

@end

@implementation YXApplicationRoleCell

- (void)awakeFromNib {
    
    _iconImageView.layer.cornerRadius = 15.0;
    _iconImageView.layer.masksToBounds = YES;
}


-(void)setContentWithTopicModel:(ApplicationRole *)model{

    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.applicantportrait stringOfW60ImgUrl]]  placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    _nameLabel.text = model.applicantname;
    _applicationRoleLabel.text = [NSString stringWithFormat:@"申请角色: %@",model.applyroletype];
    _playNameLabel.text = [NSString stringWithFormat:@"剧名: %@", model.applystoryname];
    _playNameLabelHight.constant = [_playNameLabel.text boundingRectWithSize:CGSizeMake(ScreenWidth-56, 99999) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName :[UIFont systemFontOfSize:15.0]} context:nil].size.height;
    NSString *time = [self bySecondgetDate:model.applytime];
    _timeLabel.text = time;
}

///时间戳
- (NSString *)bySecondgetDate:(NSString *)second
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dataLoca = [NSString stringWithFormat:@"%@",second];
    NSTimeInterval time = [dataLoca doubleValue];
    NSDate *detailDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [formatter stringFromDate:detailDate];
    return timeStr;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
