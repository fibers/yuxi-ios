//
//  YXRoleReviewGroupView.h
//  inface
//
//  Created by 邢程 on 15/11/5.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,YXRoleReviewGroupViewType) {
    YXRoleReviewGroupViewHeaderDefault = 1,
    YXRoleReviewGroupViewWithSubTitleType = 2,
    YXRoleReviewGroupViewWithSubContentType = 3,
};
@interface YXRoleReviewGroupView : UIView
-(void)setContentWithType:(YXRoleReviewGroupViewType)type title:(NSString *)title andSubContent:(NSString*)content;
@property(assign,nonatomic,readonly)CGFloat viewH;
@end
