//
//  YXCollectionListTableViewCell.m
//  inface
//
//  Created by 邢程 on 15/8/28.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXCollectionListTableViewCell.h"
#import "Mycollects.h"
#import "Myfavor.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
@interface YXCollectionListTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *headerImgV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UIImageView *storyImgV;
@property (weak, nonatomic) IBOutlet UILabel *storyTitleL;
@property (weak, nonatomic) IBOutlet UILabel *storyIntroL;
@property (weak, nonatomic) IBOutlet UILabel *timeL;

@end
@implementation YXCollectionListTableViewCell
-(void)setContentWithMycollects:(Mycollects*)collects{
    [self.titleL setText:collects.creatorname];
    [self.headerImgV sd_setImageWithURL:[NSURL URLWithString:collects.creatorportrait] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    [self.storyImgV sd_setImageWithURL:[NSURL URLWithString:collects.portrait] placeholderImage:[UIImage imageNamed:@"remen"]];
    [self.storyTitleL setText:collects.title];
    [self.storyIntroL setText:collects.content];
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:[collects.createtime doubleValue]];
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [self.timeL setText:[formatter stringFromDate:date]];

}
-(void)setContentWithMyFavor:(Myfavor *)collects{
    [self.titleL setText:collects.creatorname];
    [self.headerImgV sd_setImageWithURL:[NSURL URLWithString:collects.creatorportrait] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    [self.storyImgV sd_setImageWithURL:[NSURL URLWithString:collects.portrait] placeholderImage:[UIImage imageNamed:@"remen"]];
    [self.storyTitleL setText:collects.title];
    [self.storyIntroL setText:collects.content];
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:[collects.createtime doubleValue]];
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [self.timeL setText:[formatter stringFromDate:date]];
    
    
    if (collects.portrait ==nil ||[collects.portrait isEqualToString:@""]) {
        [self.storyImgV mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@0);
        }];
    }
    if (collects.content ==nil ||[collects.content isEqualToString:@""]) {
        [self.storyIntroL mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
    }

}
- (void)awakeFromNib {
    self.headerImgV.layer.cornerRadius = self.headerImgV.frame.size.width*0.5;
    [self.headerImgV setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
