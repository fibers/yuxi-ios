//
//  YXMyCenterCell.m
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXMyCenterCell.h"
#import "Masonry.h"
#import "NSString+Extention.h"
@interface YXMyCenterCell()
@property (weak, nonatomic) IBOutlet UIImageView *headerIcon;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *descL;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;
@end
@implementation YXMyCenterCell
-(void)setContentWithType:(YXMyCenterCellType)type titleString:(NSString*)title andDescString:(NSString*)desc{
    [self.titleL setText:title];
    [self.descL setText:desc];
    switch (type) {
        case YXMyCenterTypeModeWord:
            [self.rightIcon setHidden:YES];
            [self.headerIcon setImage:[UIImage imageNamed:@"MyCenter_zishu"]];
            break;
        case YXMyCenterTypeModeFamily:
            [self.headerIcon setImage:[UIImage imageNamed:@"MyCenter_jiazu"]];
            break;
        case YXMyCenterTypeModeCollection:
            [self.headerIcon setImage:[UIImage imageNamed:@"MyCenter_shoucang"]];
            break;
        case YXMyCenterTypeModeFavor:
            [self.headerIcon setImage:[UIImage imageNamed:@"MyCenter_xihuan"]];
            break;
        case YXMyCenterTypeModeApplication:
            [self.headerIcon setImage:[UIImage imageNamed:@"MyCenter_apped"]];
            break;
            
        default:
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
