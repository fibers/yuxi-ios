//
//  YXCollectionListTableViewCell.h
//  inface
//
//  Created by 邢程 on 15/8/28.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Mycollects;
@class Myfavor;
@interface YXCollectionListTableViewCell : UITableViewCell
-(void)setContentWithMycollects:(Mycollects*)collects;
-(void)setContentWithMyFavor:(Myfavor *)myFavor;
@end
