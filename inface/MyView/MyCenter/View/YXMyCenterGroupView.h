//
//  YXMyCenterGroupView.h
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol YXMyCenterGroupViewDelegate<NSObject>
@optional
-(void)centerGroupViewClickedWithItem:(UILabel *)label;
@end
@interface YXMyCenterGroupView : UIView
@property(weak,nonatomic)id<YXMyCenterGroupViewDelegate> delegate;
-(void)setContentWithTitleOne:(NSString*)t1 titleTwo:(NSString*)t2 andTitleThree:(NSString *)t3;
@end
