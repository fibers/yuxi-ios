//
//  YXMyCenterButtomView.h
//  inface
//
//  Created by 邢程 on 15/8/27.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol YXMyCenterButtomViewDelegate <NSObject>
///添加好友
-(void)addButtonClicked:(UIButton * )btn;
///发送消息
-(void)chatButtonClicked:(UIButton *)btn;
//删除好友
-(void)delButtonClicked:(UIButton *)btn;
///解散师徒关系
-(void)dissMissShiTu:(UIButton *)btn;
///取消拜师
-(void)cencleAddMaster:(UIButton*)btn;
///拜师
-(void)baiShi:(UIButton*)btn;
@end
@interface YXMyCenterButtomView : UIView
@property(weak,nonatomic) id<YXMyCenterButtomViewDelegate> delegate;
/**
 *  设置是否为好友关系
 *
 *  @param isFriend BOOL 是否为好友
 */
-(void)setIsFriend:(BOOL)isFriend;
/**
 *  设置拜师状态
 *
 *  @param masterFlag 拜师状态 0 拜师前 1拜师中 2拜师后
 *  @param isMaster   YES为师傅 NO为徒弟
 */
-(void)setMaserFlag:(NSInteger)masterFlag andIsMaster:(BOOL)isMaster;
@end
