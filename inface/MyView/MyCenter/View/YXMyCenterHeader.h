//
//  YXMyCenterHeader.h
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Personinfo;
@protocol YXMyCenterHeaderDelegate <NSObject>
-(void)YXMyCenterHeaderClicked:(UIImageView *)imageV;

@end
@interface YXMyCenterHeader : UIView

@property(nonatomic,weak)id<YXMyCenterHeaderDelegate> delegate;
/**
 *  设置view中的内容
 *
 *  @param personInfo Model
 */
-(void)setContentWithPersonifno:(Personinfo*)personInfo;
@end
