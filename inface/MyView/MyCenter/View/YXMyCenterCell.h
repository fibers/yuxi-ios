//
//  YXMyCenterCell.h
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, YXMyCenterCellType) {
    YXMyCenterTypeModeWord,
    YXMyCenterTypeModeFamily,
    YXMyCenterTypeModeCollection,
    YXMyCenterTypeModeFavor,
    YXMyCenterTypeModeApplication,
};
@interface YXMyCenterCell : UITableViewCell
-(void)setContentWithType:(YXMyCenterCellType)type titleString:(NSString*)title andDescString:(NSString*)desc;
@end
