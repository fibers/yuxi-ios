//
//  YXMyCenterHeader.m
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXMyCenterHeader.h"
#import "Personinfo.h"
#import "NSString+Extention.h"
#import "UIImageView+WebCache.h"
@interface YXMyCenterHeader()
@property (weak, nonatomic) IBOutlet UIImageView *bgImgV;
@property (weak, nonatomic) IBOutlet UIImageView *headerImgV;
@property (weak, nonatomic) IBOutlet UILabel *nickNameL;
@property (weak, nonatomic) IBOutlet UILabel *idL;
@property (weak, nonatomic) IBOutlet UITextView *introTextV;
@property (weak, nonatomic) IBOutlet UIImageView *sexImgV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nickNameCW;
@property (weak, nonatomic) IBOutlet UIImageView *vipImageView;

@end
@implementation YXMyCenterHeader
-(void)awakeFromNib{
    self.headerImgV.layer.cornerRadius = self.headerImgV.frame.size.height *0.5;
    self.headerImgV.layer.borderWidth = 2;
    self.headerImgV.layer.borderColor = [UIColor whiteColor].CGColor;
    [self.headerImgV setClipsToBounds:YES];
    [self.headerImgV setUserInteractionEnabled:YES];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerVClicked)];
    [self.headerImgV addGestureRecognizer:tap];
    
}

-(void)headerVClicked{
    if ([self.delegate respondsToSelector:@selector(YXMyCenterHeaderClicked:)]) {
        [self.delegate YXMyCenterHeaderClicked:self.headerImgV];
    }
}
-(void)setContentWithPersonifno:(Personinfo*)personInfo{
    [self.bgImgV setHighlighted:[personInfo.gender isEqualToString:@"0"]];
    [self.sexImgV setHighlighted:[personInfo.gender isEqualToString:@"0"]];
    CGSize  size = [personInfo.nickname sizeWithfont:[UIFont systemFontOfSize:14.0] MaxX:100];
    _nickNameCW.constant = size.width + 5;
    [self.nickNameL setText:personInfo.nickname];
    [self.idL setText:[NSString stringWithFormat:@"语戏号: %@",personInfo.id]];
    [self.introTextV setText:personInfo.intro];
    [self.headerImgV sd_setImageWithURL:[NSURL URLWithString:personInfo.portrait] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    int isVip = [personInfo.portrait isSignedUser];
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
    switch (isVip) {
        case 0:
            self.vipImageView.hidden = YES;
            break;
        case 1:
            self.vipImageView.hidden = NO;
            
            self.vipImageView.image = image;
            
            break;
        case 2:
            self.vipImageView.hidden = NO;
            
            self.vipImageView.image = officalImage;
            
            break;
        case 3:
            self.vipImageView.hidden = NO;
            
            self.vipImageView.image = officalImage;
            
            break;
        default:
            break;
    }
}
@end
