//
//  YXMyCenterGroupView.m
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXMyCenterGroupView.h"
@interface YXMyCenterGroupView()
@property (weak, nonatomic) IBOutlet UILabel *labelOne;
@property (weak, nonatomic) IBOutlet UILabel *labelTwo;
@property (weak, nonatomic) IBOutlet UILabel *labelThree;
@property (strong, nonatomic) UILabel * tmpL;

@end
@implementation YXMyCenterGroupView

-(void)awakeFromNib{
    UITapGestureRecognizer * tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelClicked:)];
    UITapGestureRecognizer * tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelClicked:)];
    UITapGestureRecognizer * tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelClicked:)];
    [self.labelOne addGestureRecognizer:tap1];
    [self.labelTwo addGestureRecognizer:tap2];
    [self.labelThree addGestureRecognizer:tap3];
    [self labelClicked:tap1];
}
-(void)labelClicked:(UITapGestureRecognizer *)sender{
    if (_tmpL) {
        _tmpL.highlighted = !_tmpL.highlighted;
    }
    UILabel * clickedView = (UILabel*)sender.view;
    clickedView.highlighted = !clickedView.highlighted;
    
    
    if ([self.delegate respondsToSelector:@selector(centerGroupViewClickedWithItem:)]) {
        if (_tmpL !=clickedView) {
            [self.delegate centerGroupViewClickedWithItem:clickedView];
        }
    }
    _tmpL = clickedView;
}
-(void)setContentWithTitleOne:(NSString*)t1 titleTwo:(NSString*)t2 andTitleThree:(NSString *)t3{
    [self.labelOne setText:t1];
    [self.labelTwo setText:t2];
    [self.labelThree setText:t3];
}
@end
