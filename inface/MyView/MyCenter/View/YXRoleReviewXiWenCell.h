//
//  YXRoleReviewXiWenCell.h
//  inface
//
//  Created by 邢程 on 15/11/5.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXRoleReviewXiWenCell : UITableViewCell
-(void)setContentWithStr:(NSString*)str;
-(CGFloat)heightWithStr:(NSString *)str;
@end
