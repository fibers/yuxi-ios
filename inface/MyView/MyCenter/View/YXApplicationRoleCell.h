//
//  YXApplicationRoleCell.h
//  inface
//
//  Created by lizhen on 15/11/6.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationRole.h"
#import "MyCenter.h"

@interface YXApplicationRoleCell : UITableViewCell

-(void)setContentWithTopicModel:(ApplicationRole *)model;

@end
