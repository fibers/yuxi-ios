//
//  YXRoleReviewHeaderCell.m
//  inface
//
//  Created by 邢程 on 15/11/5.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXRoleReviewHeaderCell.h"
#import "RoleReviewEntity.h"
@interface YXRoleReviewHeaderCell()
@property (weak, nonatomic) IBOutlet UILabel *titleL;

@end
@implementation YXRoleReviewHeaderCell

-(void)setContentWithTitle:(NSString*)title{
    [self.titleL setText:title];

}

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
