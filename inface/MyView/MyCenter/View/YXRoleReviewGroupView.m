//
//  YXRoleReviewGroupView.m
//  inface
//
//  Created by 邢程 on 15/11/5.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXRoleReviewGroupView.h"
#import <TTTAttributedLabel.h>
@interface YXRoleReviewGroupView()
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLCH;

@end
@implementation YXRoleReviewGroupView
-(void)setContentWithType:(YXRoleReviewGroupViewType)type title:(NSString *)title andSubContent:(NSString*)content{
    if (type == YXRoleReviewGroupViewWithSubTitleType) {
        NSMutableAttributedString * strM = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@:%@",title,content]];
        [strM addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(title.length+1,strM.length - title.length-1)];
        [self.titleL setAttributedText:strM];
    }
    if(type == YXRoleReviewGroupViewWithSubContentType){
        NSString * simpleStr = [NSString stringWithFormat:@"%@:%@",title,content];
        NSMutableAttributedString * strM = [[NSMutableAttributedString alloc]initWithString:simpleStr];
        [strM addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(title.length+1,strM.length - title.length-1)];
        [self.titleL setAttributedText:strM];
        CGFloat labelH = [simpleStr boundingRectWithSize:CGSizeMake(264, 9999) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:self.titleL.font} context:nil].size.height;
        self.titleLCH.constant = labelH;
//        [self.titleL setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
        self.titleL.numberOfLines = 0;
        _viewH = 32+labelH;
    }
    if(type == YXRoleReviewGroupViewHeaderDefault){
        [self.titleL setText:title];
    }
   
}

@end
