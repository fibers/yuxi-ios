//
//  Mytopics.h
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mytopics : NSObject

@property (nonatomic, copy) NSString *topicid;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *createtime;

@end