//
//  ApplicationRole.h
//  inface
//
//  Created by lizhen on 15/11/6.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplicationRole : NSObject

///该申请的id
@property (nonatomic,copy)NSString *applyid;
///该申请绑定的剧的标题
@property (nonatomic,copy)NSString *applystoryname;
///申请人的id
@property (nonatomic,copy)NSString *applicantuid;
///申请人的头像地址
@property (nonatomic,copy)NSString *applicantportrait;
///申请人的name
@property (nonatomic,copy)NSString *applicantname;
///申请时间
@property (nonatomic,copy)NSString *applytime;
///申请的角色
@property (nonatomic,copy)NSString *applyroletype;


@end
