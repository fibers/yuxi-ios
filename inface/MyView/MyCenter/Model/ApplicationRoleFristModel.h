//
//  ApplicationRoleFristModel.h
//  inface
//
//  Created by lizhen on 15/11/6.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@class Status,ApplicationRole;
@interface ApplicationRoleFristModel : NSObject

@property(copy,nonatomic)NSString * result;
@property(strong,nonatomic)NSArray *list;

@end
