//
//  Myfamily.h
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Myfamily : NSObject

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *creatorname;

@property (nonatomic, copy) NSString *creatorid;

@property (nonatomic, copy) NSString *createtime;

@property (nonatomic, copy) NSString *portrait;

@end