//
//  MyCenter.h
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
#import "Personinfo.h"
@interface MyCenter : NSObject
@property (nonatomic, copy) NSString *result;
@property (nonatomic, strong) Personinfo *personinfo;
@end
