//
//  ApplicationRoleFristModel.m
//  inface
//
//  Created by lizhen on 15/11/6.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "ApplicationRoleFristModel.h"
#import "ApplicationRole.h"
@implementation ApplicationRoleFristModel

+ (NSDictionary *)objectClassInArray{
    return @{@"list" : [ApplicationRole class]};
}

@end
