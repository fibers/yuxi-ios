//
//  Personinfo.m
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "Personinfo.h"

@implementation Personinfo

+ (NSDictionary *)objectClassInArray{
    return @{@"mystorys" : [Mystorys class], @"myselfstorys" : [Myselfstorys class], @"mycollects" : [Mycollects class], @"myfamily" : [Myfamily class],@"mytopics":[Mytopics class],@"myfavor":[Myfavor class]};
}

@end