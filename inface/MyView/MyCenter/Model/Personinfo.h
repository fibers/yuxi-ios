//
//  Personinfo.h
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mystorys.h"
#import "Myselfstorys.h"
#import "Mycollects.h"
#import "Myfamily.h"
#import "Mytopics.h"
#import "Myfavor.h"
#import "ApplicationRole.h"
@interface Personinfo : NSObject

@property (nonatomic, copy  ) NSString *id;

@property (nonatomic, copy  ) NSString *intro;

@property (nonatomic, strong) NSArray  *myfamily;

@property (nonatomic, strong) NSArray  *mycollects;

@property (nonatomic, copy  ) NSString *age;

@property (nonatomic, strong) NSArray  *myfavor;

@property (nonatomic, copy  ) NSString *likeStorys;

@property (nonatomic, copy  ) NSString *storycount;

@property (nonatomic, strong) NSArray  *mystorys;

@property (nonatomic, copy  ) NSString *type;

@property (nonatomic, copy  ) NSString *isfriend;

@property (nonatomic, copy  ) NSString *portrait;

@property (nonatomic, strong) NSArray  *mytopics;

@property (nonatomic, copy  ) NSString *nickname;

@property (nonatomic, copy  ) NSString *wordcount;

@property (nonatomic, copy  ) NSString *gender;

@property (nonatomic, strong) NSArray  *myselfstorys;

@property (nonatomic, copy ) NSString *applycount;
/// 师徒状态 0 非师徒 1 申请师傅中  2 已经是师徒关系
@property(copy,nonatomic)NSString * addMasterStatus;
/// 师徒状态 master 师傅  apprentice 徒弟  "" 什么都不是
@property(copy,nonatomic)NSString * masterOrApprentice;

@end