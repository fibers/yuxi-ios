//
//  Mycollects.h
//  inface
//
//  Created by 邢程 on 15/8/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mycollects : NSObject

@property (nonatomic, copy) NSString *creatorid;

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *creatorname;

@property (nonatomic, copy) NSString *createtime;

@property (nonatomic, copy) NSString *portrait;

@property (nonatomic, copy) NSString *creatorportrait;

@property (nonatomic, copy) NSString *content;

@end
