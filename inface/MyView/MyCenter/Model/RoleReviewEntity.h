//
//  RoleReviewEntity.h
//  inface
//
//  Created by 邢程 on 15/11/5.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Applyrolenatures;
@interface RoleReviewEntity : NSObject

@property (nonatomic, copy) NSString *result;

@property (nonatomic, copy) NSString *applicantuid;

@property (nonatomic, strong) NSArray<Applyrolenatures *> *applyrolenatures;

@property (nonatomic, copy) NSString *applytext;

@property (nonatomic, copy) NSString *applicantportrait;

@property (nonatomic, copy) NSString *applytime;

@property (nonatomic, copy) NSString *applystory;

@property (nonatomic, copy) NSString *applicantname;

@property (nonatomic, copy) NSString *applyroletype;

@property (nonatomic, copy) NSString *applyrolebackground;

@end
@interface Applyrolenatures : NSObject

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *value;

@end

