//
//  ModifyFriendNameViewController.h
//  inface
//
//  Created by appleone on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
typedef void (^ReturnTextBlock)(NSString *showText);

@interface ModifyFriendNameViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *chatINtextField;
@property(strong,nonatomic)NSString * friendId;
@property (nonatomic, copy) ReturnTextBlock returnTextBlock;
@property(assign,nonatomic)    BOOL   isAnimating;

- (void)returnText:(ReturnTextBlock)block;

@end
