//
//  EditNicknameViewController.h
//  inface
//
//  Created by huangzengsong on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface EditNicknameViewController : BaseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property(assign,nonatomic)    BOOL   isAnimating;

@end

