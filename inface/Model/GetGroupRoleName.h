//
//  GetGroupRoleName.h
//  inface
//
//  Created by appleone on 15/6/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetGroupRoleName : NSObject
{
    NSMutableDictionary * storyRoles;
    NSMutableDictionary * groupRoles;
}
+(instancetype)shareInstance;

-(void)getGroupRoleName:(NSString *)storyid groupid:(NSString*)groupid success:(void(^)(id  JSON))success;

-(void)ReturnRoleName:(NSString*)groupid stroryid:(NSString*)storyid userid:(NSString*)userid success:(void(^)(NSDictionary * personDic))success ;


-(BOOL)isNeedGetGroupMembers:(NSString*)groupid;

-(void)getFamilyInfo:(NSString *)familyid userid:(NSString *)userid success:(void(^)(id  JSON))success;
-(void)ReturnFamilyInfo:(NSString*)familyid  userid:(NSString*)userid success:(void (^)(NSDictionary *))success;
-(void)saveFamily:(NSString*)familyId membersArr:(NSArray * )membersArrary;//根据familyId存储家族成员信息
-(NSArray * )familyMembers:(NSString*)familyid;
@end
