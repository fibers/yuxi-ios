//
//  GetTopicsModel.h
//  inface
//
//  Created by appleone on 15/7/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetTopicsModel : NSObject
{
    int minFromId;
    int maxFromId;
    NSMutableArray * storys_Arr;
    NSInteger  timestamp;
    int  minRecFromid;//招募的最小id
    int  maxRecFromid;//招募的最大id
    NSMutableArray * recommends_Arr;

}
+(instancetype)shareInstance;
-(void)firstGetStorysSuccess:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//第一次取话题，不需要上传所有参数

-(void)updateGetStorysDirection:(NSString *)direction success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//根据时间顺序获取话题

-(void)firstGetRecommendSuccess:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//第一次获取招募贴
-(void)getRecommendStorysDirection:(NSString*)direction success:(void(^)(id JSON))success  failure:(void(^)(id JSON))failure;//获取广场的招募贴

-(NSMutableArray *)returnTopicArr;//返回话题数组
-(NSMutableArray*)returnRecommendArr;//返回招募贴数目

@end
