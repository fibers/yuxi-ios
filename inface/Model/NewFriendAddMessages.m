//
//  NewFriendAddMessages.m
//  inface
//
//  Created by appleone on 15/6/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "NewFriendAddMessages.h"

@implementation NewFriendAddMessages
+(instancetype)shareInstance
{
    static NewFriendAddMessages * getGroupRoleName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getGroupRoleName = [[NewFriendAddMessages alloc] init];
        
    });
    return getGroupRoleName;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        newfriendsDic = [[NSUserDefaults standardUserDefaults]objectForKey:@"newfriendaddmessages"];
    }
    return self;
}

-(void)saveNewFriendId:(NSString*)friendId
{
    if (newfriendsDic) {
        NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:newfriendsDic];
        [dic setObject:@"-2" forKey:friendId];
        [[NSUserDefaults standardUserDefaults]setObject:dic forKey:@"newfriendaddmessages"];
    }else{
       newfriendsDic = [NSMutableDictionary dictionaryWithObject:@"-2" forKey:friendId];
        [[NSUserDefaults standardUserDefaults]setObject:newfriendsDic forKey:@"newfriendaddmessages"];

    }
    
}
@end
