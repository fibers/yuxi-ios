//
//  GetGroupMemberNotify.m
//  inface
//
//  Created by xigesi on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GetGroupMemberNotify.h"
#import "MsgAddGroupNotifyAPI.h"
#import "MsgAddGroupConfirmNotify.h"
#import "MsgAddGroupConfirmAPI.h"
#import "AddGroupMemberModule.h"
#import "DDAddMemberToGroupAPI.h"
#import "MsgReceiveChangeMerber.h"
#import "GetGroupRoleName.h"
@implementation GetGroupMemberNotify

+(instancetype)shareInsatance
{
    static GetGroupMemberNotify* g_getGroupMemberNotify;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_getGroupMemberNotify = [[GetGroupMemberNotify alloc] init];
        
    });
    return g_getGroupMemberNotify;
}
-(instancetype)init
{
    self = [super init];
    if (self) {
        MsgAddGroupNotifyAPI * addGroupNotify = [[MsgAddGroupNotifyAPI alloc]init];
        
        [addGroupNotify registerAPIInAPIScheduleReceiveData:^(id object, NSError *error) {
            
            //收到被添加到群的通知
           // 通知服务器，已经收到消息
            NSMutableString * groupId = [object objectForKey:@"group_id"];
            long objectValue = [[object objectForKey:@"type"]integerValue];
            NSMutableString * from_id = [NSMutableString stringWithFormat:@"%@",[object objectForKey:@"from_id"]];
            NSString * uid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];

            NSString * posttype;//更新服务器状态
            NSString * sqltypeVal;//保存到数据库的值
            if (objectValue == 1) {
                posttype = @"3";
               //群被申请时候
              sqltypeVal = @"5";//群被申请时type为5
                
                NSRange  range = [from_id rangeOfString:@"user_"];
                if (range.length > 0) {
                    [from_id deleteCharactersInRange:range];
                }
                
                NSRange rangeGroup = [groupId rangeOfString:@"group_"];
                if (rangeGroup.length > 0) {
                    [groupId deleteCharactersInRange:rangeGroup];
                }
                BOOL ifHasApplyed =  [[DDDatabaseUtil instance]determineMemerHasApplyGroup:from_id groupid:groupId applyType:sqltypeVal];
                if (ifHasApplyed) {
                    return ;
                }
                //获取对方的信息
                //发送http请求，获取对方的头像和姓名
 
                //需要
                [HttpTool postWithPath:@"GetPersonInfo" params:@{@"uid":uid,@"personid":from_id,@"token":@"110"} success:^(id JSON) {
                    NSDictionary * persondic = [JSON objectForKey:@"personinfo"];
                    NSString * name = [persondic objectForKey:@"nickname"];
                    NSString * portrait = [persondic objectForKey:@"portrait"];
                    if ([portrait isEmpty]) {
                        portrait = @"100";
                    }
                    NSString * result = @"-2";
                    NSString * groupid = groupId;
                     __block NSString * groupType = @"2";
                   [[GetAllFamilysAndStorys shareInstance]getGroupTypeStringgroupid:groupid success:^(id JSON) {
                       //从本地保存的群信息里拿到类型
                       groupType = (NSString *)JSON;
                   }];
                    //需要根据群id拿到对应剧或者家族的信息
                   __block NSString * text = @"";
                    [HttpTool postWithPath:@"GetGroupDetail" params:@{@"uid":uid,@"groupid":groupId,@"token":@"110"} success:^(id JSON) {
                        NSDictionary * groupdetail = [JSON objectForKey:@"groupdetails"];
                        NSString * typeVal = [groupdetail objectForKey:@"type"];
                        NSString * gname = [groupdetail objectForKey:@"refname"];
                        NSString * groupname = [groupdetail objectForKey:@"groupname"];
                        if ([typeVal isEqualToString:@"1"]) {
                            //对方申请进家族
                            text = [NSString stringWithFormat:@"%@申请加入家族%@",name,gname];
                            
                        }else{
                            NSDictionary * storyinfo = [[GetAllFamilysAndStorys shareInstance]getStoryDetail:groupid];
                            NSString * storyname = [storyinfo objectForKey:@"title"];
                            text = [NSString stringWithFormat:@"%@申请加入剧[%@][%@]",name,storyname, groupname];
                        }
                        [SVProgressHUD showSuccessWithStatus:text duration:2.0];
                        NSString * haschange = @"0";
                        NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                        NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                        NSMutableDictionary * dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:from_id, @"fromid",uid,@"toid",groupid,@"groupid",result,@"result",sqltypeVal,@"type",name,@"name",portrait,@"portrait",haschange,@"haschange",groupType,@"grouptype", text,@"detailtext" ,time,@"time", nil];
                        
                        //将监听的消息假如数据库
                        [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dictionary];
                        //给自己发一条消息
                        NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                            DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                        messageEntity.state = DDmessageSendSuccess;
                        
                        [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                        NSString * toId = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
                        _sessionId = from_id;
                        
                        _groupId = groupId;
                        
                        NSDictionary * paras= @{@"uid":toId,@"fromid":from_id,@"toid":toId,@"type":posttype,@"token":@"110"};
                        
                        //发送给杨老师
                        [HttpTool postWithPath:@"UpdateNotifyState" params:paras success:^(id JSON) {
                        } failure:^(NSError *error) {
                            
                        }];
                        
                    } failure:^(NSError *error) {
                        
                        
                    }];

                    
                } failure:^(NSError *error) {
                    
                    
                }];
                
                
            }else{
                posttype = @"5";//邀请进群
                sqltypeVal = @"3";
                NSRange  range = [from_id rangeOfString:@"group_"];
                if (range.length > 0) {
                    [from_id deleteCharactersInRange:range];
                }
                BOOL ifHasApplyed =  [[DDDatabaseUtil instance]determineMemerHasApplyGroup:from_id groupid:groupId applyType:sqltypeVal];

                //根据grouid获取对方群的消息
                [HttpTool postWithPath:@"GetGroupDetail" params:@{@"uid":uid,@"groupid":groupId,@"token":@"110"} success:^(id JSON) {
                    NSDictionary * persondic = [JSON objectForKey:@"groupdetails"];
                    NSString * name = [persondic objectForKey:@"refname"];
                    NSString * storyid = [persondic objectForKey:@"refid"];
                    NSString * groupname = [persondic objectForKey:@"groupname"];
                    NSString * portrait = [persondic objectForKey:@"portrait"];
                    NSString  * creator = [persondic objectForKey:@"creator"];
                    if ([portrait isEmpty]) {
                        portrait = @"100";
                    }
                    NSString * grouptype = [persondic objectForKey:@"type"];
                    if([grouptype isEmpty]){
                        grouptype = @"2";
                    }
                    NSString * result = @"-2";
                    NSString * haschange = @"0";
                    __block  NSString * text= @"";
                    if ([grouptype isEqualToString:@"1"]) {
                        text = [NSString stringWithFormat:@"[%@]邀请您加入家族:[%@]",creator,name];
                        [SVProgressHUD showSuccessWithStatus:text duration:2.0];
                        NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                        NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                        NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:from_id,@"fromid",uid,@"toid",groupId,@"groupid",result,@"result",sqltypeVal,@"type",name,@"name" ,portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype",text,@"detailtext" ,time,@"time" ,nil];
                        //将监听的消息假如数据库
                        [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dic];
                    }else{
                        //根据剧的id获取剧名
                        NSDictionary * params = @{@"uid":USERID,@"storyid":storyid,@"token":@"110"};
                        [HttpTool postWithPath:@"GetStoryInfo" params:params success:^(id JSON) {
                            if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                                NSString * storyname = [JSON objectForKey:@"title"];
                                text = [NSString stringWithFormat:@"[%@]邀请您加入剧[%@][%@]",creator,storyname, groupname];
                                [SVProgressHUD showSuccessWithStatus:text duration:2.0];
                                NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                                NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                                NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:from_id,@"fromid",uid,@"toid",groupId,@"groupid",result,@"result",sqltypeVal,@"type",name,@"name" ,portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype",text,@"detailtext" ,time,@"time", nil];
                                //将监听的消息假如数据库
                                [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dic];
                            }
                            
                        } failure:^(NSError *error) {
                            
                            
                        }];
                    }

                    //给自己发一条消息
                    NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                    DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                    messageEntity.state = DDmessageSendSuccess;
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                    NSString * toId = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
                    _sessionId = from_id;
                    
                    _groupId = groupId;
                    
                    NSDictionary * paras= @{@"uid":toId,@"fromid":from_id,@"toid":toId,@"type":posttype,@"token":@"110"};
                    
                    //发送给杨老师
                    [HttpTool postWithPath:@"UpdateNotifyState" params:paras success:^(id JSON) {
                    } failure:^(NSError *error) {
                        
                    }];
                    
                } failure:^(NSError *error) {
                    
                }];
            }
            
//            NSString * toId = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
//            _sessionId = from_id;
//
//            _groupId = groupId;
//
//            NSDictionary * paras= @{@"uid":toId,@"fromid":from_id,@"toid":toId,@"type":posttype,@"token":@"110"};
//            
//            //发送给杨老师
//            [HttpTool postWithPath:@"UpdateNotifyState" params:paras success:^(id JSON) {
//                NSLog(@"返回数值为:%@",[JSON objectForKey:@"result"]);
//            } failure:^(NSError *error) {
//                
//            }];
            
        }];
        MsgAddGroupConfirmNotify * addGroupConfirmNotify = [[MsgAddGroupConfirmNotify alloc]init];
        [addGroupConfirmNotify registerAPIInAPIScheduleReceiveData:^(id object, NSError *error) {
            //需要区分申请加群或者是邀请
            //更新http服务器的状态
            NSString * postType;//更新服务器的状态
            NSString * sqlType;//传入数据的type类型

            //result为1时为同意，0时为拒绝
            NSMutableString * toId = [NSMutableString stringWithFormat:@"%@",[object objectForKey:@"from_id"]];
            NSMutableString * group_ID = [NSMutableString stringWithString:[object objectForKey:@"group_id"]];
            NSRange range = [group_ID rangeOfString:@"_"];
            NSString * groupid=@"";
            if (range.length > 0) {
                NSArray * strArr = [group_ID componentsSeparatedByString:@"_"];
                groupid = [strArr objectAtIndex:1];
            }
            long objectType = [[object objectForKey:@"type"]integerValue];
            long result = [[object objectForKey:@"result"]integerValue];
            if (objectType == 1) {
                
                postType = @"4";
                sqlType = @"6";

                //申请进群，被管理员同意
                if (result == 1) {
                    

                NSRange range = [toId rangeOfString:@"user_"];
                if (range.length > 0) {
                    [toId deleteCharactersInRange:range];
                }
                NSDictionary * unknownDic = @{@"portrait":@"zs_touxianglanfamily",@"title":@"群"};
                
                [[GetAllFamilysAndStorys shareInstance].groupsInfoDic setObject:unknownDic forKey:groupid];
   //获取群的详情，存储群的各类信息
                [HttpTool postWithPath:@"GetGroupDetail" params:@{@"uid":USERID,@"groupid":groupid,@"token":@"110"} success:^(id JSON) {
                    
                    NSDictionary * persondic = [JSON objectForKey:@"groupdetails"];
                    NSString * name = [persondic objectForKey:@"refname"];
                    NSString * portrait = [persondic objectForKey:@"portrait"];
                    if ([portrait isEmpty]) {
                        portrait = @"100";
                    }
                    NSString * grouptype = [persondic objectForKey:@"type"];
                    if ([grouptype isEmpty]) {
                        grouptype = @"2";
                    }
                    NSString * result = @"-2";
                    NSString * haschange = @"3";
                    NSString * text =@"";
                    NSString * creator = [persondic objectForKey:@"creator"];
                    NSString * groupname = [persondic objectForKey:@"groupname"];
                    
                    if ([grouptype isEqualToString:@"1"]) {
                        
                        text = [NSString stringWithFormat:@"管理员[%@]已将你加入家族[%@]",creator,name];

                        [[MyFamilyViewController shareInstance]agreeByFamilyLeader:groupid];
                        
                    }else{
                        text = [NSString stringWithFormat:@"管理员[%@]已将你加入剧群[%@]",creator,groupname];

                        [[MyLiveViewController shareInstance]agreeByStory:groupid];
                    }
                    
                    [SVProgressHUD showSuccessWithStatus:text duration:2.0];
                    NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                    NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:toId,@"fromid",USERID,@"toid",groupid,@"groupid",result,@"result",@"-2",@"type",name,@"name" ,portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype",text,@"detailtext" ,time,@"time", nil];
                    //将监听的消息假如数据库
                    [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dic];
                    //                    [[NSNotificationCenter defaultCenter]postNotificationName:@"newaddmessages" object:nil];
                    //给自己发一条未读消息
                    //给自己发一条消息
                    NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                    DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                    messageEntity.state = DDmessageSendSuccess;
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                
                    
                } failure:^(NSError *error) {
                    
                    
                    
                }];
                }
            }else{
                //添加群成员，对方拒绝
                postType = @"6";
                sqlType =@"4";
                NSRange range = [toId rangeOfString:@"group_"];
                if (range.length > 0) {
                    [toId deleteCharactersInRange:range];
                }
            }
    
            NSString * result_ = @"";

            //根据group_id获取该组的成员列表
            if ([[object objectForKey:@"result"]integerValue] == 1) {
                result_ = @"1";
                [[NSNotificationCenter defaultCenter] postNotificationName:@"FamilyInformationChanged" object:self userInfo:nil];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"StoryInformationChanged" object:self userInfo:nil];
                
            }else{
                result_ = @"0";
                //拒绝了请求,邀请群成员时被拒绝了
                
            }
            NSString * uid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            NSDictionary * params = @{@"uid":uid,@"fromid":uid,@"toid":toId,@"type":postType,@"token":@"110"};
            [HttpTool postWithPath:@"UpdateNotifyState" params:params success:^(id JSON) {
                
            } failure:^(NSError *error) {
            
            }];
        }];
    }
#pragma mark 群组里有人员变动时
    MsgReceiveChangeMerber * receiveGroupAddMember = [[MsgReceiveChangeMerber alloc]init];
    [receiveGroupAddMember registerAPIInAPIScheduleReceiveData:^(id object, NSError *error) {
        NSString * ownerId = [object objectForKey:@"ownerid"];
        NSString * groupid = [object objectForKey:@"groupid"];
        NSString * changeType = [object objectForKey:@"changeType"];
        [[ GetAllFamilysAndStorys shareInstance]preSaveSessionId:groupid];
//        NSMutableArray * curUsers = [object objectForKey:@"usersArr"];
        NSMutableArray * chagArr = [object objectForKey:@"changArr"];
        NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if ([changeType isEqualToString:@"1"]) {
            //添加成员的情况,此处不做处理，在confirm API中已经做了处理
            
//            BOOL isChanged = NO;//改变的成员里是否包含自己
//        for (int i = 0;i < [chagArr count];i++) {
//            NSString * user = [chagArr objectAtIndex:i];
//            if ([user isEqualToString:userid]) {
//                isChanged = YES;
//            }
//        }
//            if (!isChanged) {
//                //需要更新家族和剧的成员信息
//                [[GetGroupRoleName shareInstance]getFamilyInfo:groupid userid:nil success:^(id JSON) {
//                    
//                    
//                }];
//                
//                //如果是剧群信息，现根据剧群的ID获取剧ID；然后再获取相应的剧群成员信息
//                NSString * storyId = [[GetAllFamilysAndStorys shareInstance]getStoryidByGroupid:groupid];
//                
//                [[GetGroupRoleName shareInstance]getGroupRoleName:storyId groupid:groupid success:^(id JSON) {
//                    
//                    
//                }];
//                
//            }
            
//
//                isChanged = YES;
//                [HttpTool postWithPath:@"GetGroupDetail" params:@{@"uid":userid,@"groupid":groupid,@"token":@"110"} success:^(id JSON) {
//                    NSDictionary * persondic = [JSON objectForKey:@"groupdetails"];
//                    NSString * name = [persondic objectForKey:@"refname"];
//                    NSString * portrait = [persondic objectForKey:@"portrait"];
//                    if ([portrait isEmpty]) {
//                        portrait = @"100";
//                    }
//                    NSString * grouptype = [persondic objectForKey:@"type"];
//                    if ([grouptype isEmpty]) {
//                        grouptype = @"2";
//                    }
//                    NSString * result = @"-2";
//                    NSString * haschange = @"3";
//                    NSString * text =@"";
//                    NSString * creator = [persondic objectForKey:@"creator"];
//                    NSString * groupname = [persondic objectForKey:@"groupname"];
//                    //家族
//                
//                        if ([grouptype isEqualToString:@"1"]) {
//                            
//                            [[MyFamilyViewController shareInstance]agreeByFamilyLeader:groupid];
//
//                        }else{
//                            [[MyLiveViewController shareInstance]agreeByStory:groupid];
//                        }
//                    [SVProgressHUD showSuccessWithStatus:text duration:2.0];
//                    NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
//                    NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
//                    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:ownerId,@"fromid",userid,@"toid",groupid,@"groupid",result,@"result",@"-2",@"type",name,@"name" ,portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype",text,@"detailtext" ,time,@"time", nil];
//                    //将监听的消息假如数据库
//                    [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dic];
////                    [[NSNotificationCenter defaultCenter]postNotificationName:@"newaddmessages" object:nil];
//                    //给自己发一条未读消息
//                    //给自己发一条消息
//                    NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
//                    DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
//                    messageEntity.state = DDmessageSendSuccess;
//                    
//                    [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
//                    
//                }failure:^(NSError *error) {
//                    
//                }];
//            }
//        }
//            if (isChanged == YES) {
//                //如果自己被某个群加入了，需要将相应的群存到GetAllfamilysAndStorys中
//                
//                NSDictionary * unknownDic = @{@"portrait":@"zs_touxianglanfamily",@"title":@"群"};
//                
//                [[GetAllFamilysAndStorys shareInstance].groupsInfoDic setObject:unknownDic forKey:groupid];
//            }
        }else{
            //删除的类型
            [HttpTool postWithPath:@"GetGroupDetail" params:@{@"uid":userid,@"groupid":groupid,@"token":@"110"} success:^(id JSON) {
                NSDictionary * persondic = [JSON objectForKey:@"groupdetails"];
                NSString * name = [persondic objectForKey:@"refname"];
                NSString * groupname = [persondic objectForKey:@"groupname"];
                NSString * portrait = [persondic objectForKey:@"portrait"];
                if ([portrait isEmpty]) {
                    portrait = @"100";
                }
                NSString * grouptype = [persondic objectForKey:@"type"];
                if ([grouptype isEmpty]) {
                    grouptype = @"2";
                }
                NSString * result = @"-2";
                NSString * haschange = @"3";
                 __block  NSString * text=@"";
                NSString * creatorid = [persondic objectForKey:@"creator"];
                //家族
                NSString * changUser = [chagArr objectAtIndex:0];
                if ([changUser isEqualToString:ownerId]&&[creatorid isEqualToString:userid]) {
                    //先获取对方的人名
                    //退出群的情况,且你是群主的时候，发有成员退出群的通知
                    [HttpTool postWithPath:@"GetPersonInfo" params:@{@"uid":userid,@"personid":ownerId} success:^(id JSON) {
                        NSDictionary * personDic = [JSON objectForKey:@"personinfo"];
                        NSString * nickname = [personDic objectForKey:@"nickname"];
                        if ([grouptype isEqualToString:@"1"]) {
                            text = [NSString stringWithFormat:@"成员%@已退出了家族[%@]",nickname,name];
                        }else{
                            text = [NSString stringWithFormat:@"成员%@退出了剧群[%@]",nickname,groupname];
                        }
                        [SVProgressHUD showSuccessWithStatus:text duration:2.0];
                        NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                        NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                        NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:ownerId,@"fromid",userid,@"toid",groupid,@"groupid",result,@"result",@"-2",@"type",name,@"name" ,portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype",text,@"detailtext" ,time,@"time" ,nil];
                        //将监听的消息假如数据库
                        [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dic];
                        //给自己发一条消息
                        NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                        DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                        messageEntity.state = DDmessageSendSuccess;
                        
                        [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
//                        [[DDDatabaseUtil instance]clearUpAddMessages:ownerId toid:userid groupid:groupid type:grouptype];
                        
                    } failure:^(NSError *error) {
                        
                        
                    }];

                }else{
                    for (int i = 0; i < [chagArr count]; i++) {
                        NSString * user = [chagArr objectAtIndex:i];
                        if ([userid isEqualToString:user]) {
                            //被家族删除
                            NSString * sessionid = [NSString stringWithFormat:@"group_%@",groupid];
//                            [[FriendsViewController shareInstance]deleteSession:sessionid];
                            [[NSNotificationCenter defaultCenter]postNotificationName:DELETE_SESSION object:sessionid];
                            if ([grouptype isEqualToString:@"1"]) {
                                text = [NSString stringWithFormat:@"管理员[%@]已将您移出家族[%@][%@]",creatorid, name,groupid];
                                [[MyFamilyViewController shareInstance]getMyFamilies];
            
                            }else{
                                text = [NSString stringWithFormat:@"管理员[%@]已将您移除剧群[%@][%@]",creatorid,groupname,groupid];
                                [[MyLiveViewController shareInstance]getMyStorys];
                                //删除聊天页面的会话体
                            }
                            [SVProgressHUD showSuccessWithStatus:text duration:2.0];
                            NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                            NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                            NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:ownerId,@"fromid",userid,@"toid",groupid,@"groupid",result,@"result",@"-2",@"type",name,@"name" ,portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype",text,@"detailtext" ,time,@"time" ,nil];
                            //将监听的消息假如数据库
                            [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dic];
            
                            //给自己发一条消息
                            NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                            DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                            messageEntity.state = DDmessageSendSuccess;
                            
                            [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                        }
                    }
                }
                
                
            } failure:^(NSError *error) {
                
            }];
        }
    }];
    return  self; 

}



@end
