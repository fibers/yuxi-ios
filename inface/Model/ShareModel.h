//
//  ShareModel.h
//  inface
//
//  Created by xigesi on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareModel : NSObject
+(instancetype)sharedInstance;
-(void)shareToThird :(UIButton *)sender dic:(NSDictionary * )dic;
-(void)applyQQFriends:(UIButton *)sender applyDic:(NSDictionary*)applyDic;//邀请qq好友
@end
