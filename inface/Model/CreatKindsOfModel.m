//
//  CreatKindsOfModel.m
//  inface
//
//  Created by xigesi on 15/5/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "CreatKindsOfModel.h"
#import "DDCreateGroupAPI.h"
#import "GroupEntity.h"
@implementation CreatKindsOfModel
+(instancetype)shareInstance
{
    static CreatKindsOfModel* g_messageModule;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_messageModule = [[CreatKindsOfModel alloc] init];
    });
    return g_messageModule;
    
}


#pragma mark 设置个人信息
-(void)setRoleInStory :(NSString *)userid :(NSString*)groupid :(NSString*)storyid :(NSString*)roleid :(NSString *)nickname :(NSString*)gender  :(NSString*)property :(NSString*)characyer :(NSString*)appearance :(NSString*)other success:(void(^)(id object))success failure:(void(^)(NSError *error))failure
{
    NSDictionary * params = @{@"uid":userid,@"groupid":groupid,@"stroryid":storyid,@"roleid":roleid,@"nickname":nickname,@"gender":gender,@"property":property,@"character":characyer,@"appearance":appearance,@"other":other,@"token":@"110"};
    
    [HttpTool postWithPath:@"SettingRoleInStory" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}



#pragma mark 创建主线剧情
-(void)createStoryScreen:(NSString *)userid :(NSString *)groupid :(NSString *)title :(NSString *)content success:(void(^)(id object))success failure:(void(^)(NSError *error))failure
{
    NSDictionary *params = @{@"uid":userid,@"groupid":groupid,@"title":title,@"content":content,@"token":@"110"};
    
    [HttpTool postWithPath:@"CreateStoryScreen" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

#pragma mark 创建主线角色
-(void)createStoryRole :(NSString *)userid :(NSString *)groupid :(NSString*)title :(NSString*)number :(NSString *)intro success:(void(^)(id object))success failure:(void(^)(NSError *error))failure
{
    NSDictionary * params = @{@"uid":userid,@"groupid":groupid,@"title":title,@"num":number,@"intro":intro};
    
    [HttpTool postWithPath:@"CreateStoryRole" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

#pragma mark 获取主线角色
-(void)getStoryRoles:(NSString*)ownerId :(NSString*)strotyid success:(void(^)(id object))success failure:(void(^)(NSError *error))failure;//获取主线角色
{
    NSDictionary * params = @{@"uid":ownerId,@"storyid":strotyid,@"token":@"110"};
     [HttpTool postWithPath:@"GetStoryRoles" params:params success:^(id JSON) {
         
         
     } failure:^(NSError *error) {
         
         
     }];
}


#pragma mark 添加表情
-(void)addFavoritEmotionUserid:(NSString*)userid title:(NSString*)title content:(NSString*)content//添加表情
{
    NSDictionary * dic = @{@"uid":userid,@"title":title,@"content":content};
    
    [HttpTool postWithPath:@"AddFavoritEmotion" params:dic success:^(id JSON) {
        
        
    } failure:^(NSError *error) {
        
        
    }];
}

@end
