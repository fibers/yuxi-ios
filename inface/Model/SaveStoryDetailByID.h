//
//  SaveStoryDetailByID.h
//  inface
//
//  Created by appleone on 15/6/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SaveStoryDetailByID : NSObject
{
    NSMutableDictionary * storysDetail;
}
+(instancetype)shareInstance;
-(void)addStory:(NSString*)storyId nsarr:(NSDictionary*)detail;
-(NSDictionary *)getstorryDetail:(NSString*)storyid;
@end
