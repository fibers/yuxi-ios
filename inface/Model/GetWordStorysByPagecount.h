//
//  GetWordStorysByPagecount.h
//  inface
//
//  Created by appleone on 15/7/8.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GetWordStorysByPagecount : NSObject
{
    int minFromId;
    int maxFromId;
    NSMutableArray * storys_Arr;
    NSInteger  timestamp;
    NSMutableArray * favorStorys;
    NSMutableArray * serverFavorStorys;//喜欢的剧
    NSMutableArray * serverNewestStorys;//最新的剧
    
    
}
+(instancetype)shareInstance;

//direction 为方向，0是刷新，1是加载更多
-(void)firstGetStorysSuccess:(void(^)(id JSON))success  failure:(void(^)(id JSON))failure;//第一次取数据，不需要上传所有参数

-(void)updateGetStorysDirection:(NSString *)direction success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//根据时间顺序获取所有的剧

-(void)getFavorStorysByDirection:(NSString *)direction success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//获取最热的数据

//获取最新的剧,包括热门和最新
-(void)getCurrentNewestStoryByType:(NSString *)type success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;

//jiaza
-(void)getHistoryStorysByType:(NSString*)type success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;

-(void)getFavorAndNewestStorySucces:(void (^)(id JSON))success failure:(void (^)(id JSON))failure;

-(NSMutableArray *)returnStorysArr;//返回热门数据
-(NSMutableArray *)returnFavorARRL;//返回最新数据

@end
