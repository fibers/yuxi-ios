//
//  UpLoadImageModel.h
//  inface
//
//  Created by appleone on 15/8/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpLoadImageModel : NSObject
@property(strong,nonatomic)UpYun * upYun;
+(void)getServerKeyImage:(UIImage*)image success:(void(^)(id  json))success failure:(void(^)(NSError * error))failure;


@end
