//
//  PlaySoundModel.h
//  inface
//
//  Created by appleone on 15/6/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
static SystemSoundID shake_sound_male_id = 0;


@interface PlaySoundModel : NSObject
{
     SystemSoundID soundID;
}

-(id)initForPlayingVibrate;


-(id)initForPlayingSystemSoundEffectWith:(NSString *)resourceName ofType:(NSString *)type;
/**
 *  @brief  为播放系统音效初始化(无需提供音频文件)
 *
 *  @param resourceName 系统音效名称
 *  @param type 系统音效类型
 *
 *  @return self
 */

-(id)initForPlayingSoundEffectWith:(NSString *)filename;
/**
 *  @brief  为播放特定的音频文件初始化（需提供音频文件）
 *
 *  @param filename 音频文件名（加在工程中）
 *
 *  @return self
 */

-(void)play;


@end
