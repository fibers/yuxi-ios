//
//  DeleteMyStoryModel.h
//  inface
//
//  Created by appleone on 15/7/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeleteMyStoryModel : NSObject
+(instancetype)shareInstance;

-(void)deleteMyStory:(NSString *)storyid deleteType:(NSString *)deleteType success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//删除自戏或者剧
//推荐剧到广场
-(void)recommendStoryUid:(NSString*)uid content:(NSString*)content storyid:(NSString *)storyid success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;
@end
