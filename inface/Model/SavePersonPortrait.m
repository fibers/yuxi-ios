//
//  SavePersonPortrait.m
//  inface
//
//  Created by appleone on 15/6/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SavePersonPortrait.h"

@implementation SavePersonPortrait
+(instancetype)shareInstance
{
    static SavePersonPortrait * addGroupMemberModule;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,^{
        addGroupMemberModule = [[SavePersonPortrait alloc]init];
    });
    return addGroupMemberModule;
}


-(instancetype)init
{
    self = [super init];
    if (self) {
        if (!personDic) {
            personDic = [NSMutableDictionary new];
        }
        if (! self.personNickName) {
            self.personNickName = [NSMutableDictionary new];

        }
    }
    return self;
}


#pragma mark 保存好友头像
-(void)setSessionid:(NSString*)sessionId portraitImage:(NSString*)portraitIma;
{
    [personDic setObject:portraitIma forKey:sessionId];
}

#pragma mark 获取头像字符串
-(void)getPortraitImage:(NSString*)sessionid  success:(void(^)(id JSON))success;
{
    NSString * portraitUrl = [personDic objectForKey:sessionid];
    if (portraitUrl) {
        success(portraitUrl);
    }
}

#pragma mark 获取昵称
-(void)getPersonNickName:(NSString *)personId success:(void(^)(id JSON))success failure:(void (^)(id))failure
 {
    NSDictionary * person = [self.personNickName objectForKey:personId];
    NSString * nickname = [person objectForKey:@"nickname"];
    if (nickname) {
        success(nickname);

    }else{
        failure(@"0");
    }
}


@end
