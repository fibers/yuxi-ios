//
//  GetAllFamilysAndStorys.h
//  inface
//
//  Created by xigesi on 15/5/22.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface GetAllFamilysAndStorys : NSObject
{
    NSMutableDictionary * groupsDic;//存储我的家族和剧
    NSMutableArray * familysArr;    //存储家族数组
    NSMutableArray * strorysArr;    // 存储剧的数组
    NSMutableDictionary * strorysGroup;//一个剧里存放的群数
}
@property(strong,nonatomic)NSMutableDictionary * currentGroup;

@property(strong,nonatomic)NSMutableDictionary * groupsInfoDic;//存储我的家族和剧的所有信息

@property(strong,nonatomic)NSMutableDictionary * theaterDic;//存储群的信息

+(instancetype)shareInstance;
-(void)getAllFamilysAndStorysSuccess:(void (^)(id json))success  failure:(void(^)(id failure))failure;
-(void)getGroupTypeStringgroupid:(NSString*)groupid success:(void(^)(id JSON))success;
-(void)getGroupInfoStringgroupid:(NSString *)groupid success:(void (^)(id JSON))success  failure:(void(^)(id Json))failure;
-(NSMutableArray *)type:(NSString *)type ;
-(void)updateFamilysOrSto:(NSMutableArray *)groupsArr typestr:(NSString*)grouptype;//根据类型更新存储的数组
-(void)deleteGroups:(NSString * )groupid grouptype:(NSString*)grouptype;
-(void)GetFavoriteEmotionList;//获得表情列表
-(NSString * )getStoryidByGroupid:(NSString*)groupid;//根据群名获取剧ID
-(NSDictionary * )getStoryDetail:(NSString*)storyid;//根据剧id获取剧详情
-(NSDictionary *)getStoryGroup:(NSString*)groupid;//根据剧群id获取剧群详情

-(void)preSaveSessionId:(NSString *)groupID;
-(NSDictionary *)getCurrentGroupDetail:(NSString *)groupid;//根据群id字符串获取
@end
