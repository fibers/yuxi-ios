//
//  GetUnreadNotify.m
//  inface
//
//  Created by xigesi on 15/5/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GetUnreadNotify.h"
#import "MsgAddFriendNotifyAPI.h"
#import "MsgAddFriendConfirmReqAPI.h"
#import "MsgAddFriendConfirmNotifyAPI.h"
#import "DDFixedGroupAPI.h"
#import "MsgDeleteFriendNotifyAPI.h"
#import "NewFriendAddMessages.h"
#import "HttpReceiveMsgAPI.h"
@implementation GetUnreadNotify
{
    NSString * from_id;
}
+(instancetype)shareInsatance
{
    static GetUnreadNotify* g_getUnreadNotify;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_getUnreadNotify = [[GetUnreadNotify alloc] init];
        
    });
    return g_getUnreadNotify;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
       
        self.notifyDic = [NSMutableDictionary new];
        //监听添加好友API
        MsgAddFriendNotifyAPI * addFriendAPI = [[MsgAddFriendNotifyAPI alloc]init];
        [addFriendAPI registerAPIInAPIScheduleReceiveData:^(id object, NSError *error) {
            NSMutableString * fromId = [NSMutableString stringWithFormat:@"%@",[object objectForKey:@"from_id"]];
            NSRange range = [fromId rangeOfString:@"user_"];
            from_id =[fromId mutableCopy];
            NSMutableString * toId =  [NSMutableString stringWithFormat:@"%@",[RuntimeStatus instance].user.objID];
            [toId stringByReplacingCharactersInRange:range withString:@""];
            [toId deleteCharactersInRange:range];
            [fromId stringByReplacingCharactersInRange:range withString:@""];
            [fromId deleteCharactersInRange:range];
            NSString * addMessage = [object objectForKey:@"addMessage"];//好友添加的验证信息
            //发送http请求，获取对方的头像和姓名
            [HttpTool postWithPath:@"GetPersonInfo" params:@{@"uid":toId,@"personid":fromId,@"token":@"110"} success:^(id JSON) {
                NSDictionary * persondic = [JSON objectForKey:@"personinfo"];
                NSString * name = [persondic objectForKey:@"nickname"];
                NSString * portrait = [persondic objectForKey:@"portrait"];
                NSString * result = @"-2";
                NSString * groupid =@"-2";
                NSString * typeVal = @"1";
                NSString * haschange = @"0";
                NSString * grouptype = @"-2";
                NSString * text = [NSString stringWithFormat:@"%@想添加你为好友",name];
 
                if ([portrait isEmpty]) {
                    portrait = @"100";
                }
                NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                NSMutableDictionary * dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:fromId, @"fromid",toId,@"toid",groupid,@"groupid",result,@"result",typeVal,@"type",name,@"name",portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype" ,text,@"detailtext" ,addMessage,@"addmessage" ,time,@"time" ,nil];
                
                //将监听的消息假如数据库
                [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dictionary];
//                [[NSNotificationCenter defaultCenter]postNotificationName:@"newaddmessages" object:nil];
                //给自己发一条消息
                NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                messageEntity.state = DDmessageSendSuccess;
                
                [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                //告诉杨老师的服务器添加好友的通知收到了
                NSDictionary * params = @{@"uid":toId,@"fromid":fromId,@"toid":toId,@"type":@"1",@"token":@"110"};
#pragma 黄增松有人添加我为好友
                //do any options..........
                [HttpTool postWithPath:@"UpdateNotifyState" params:params success:^(id JSON) {
                    
                    
                } failure:^(NSError *error) {
                    
                }];
                
            } failure:^(NSError *error) {
                
                
            }];


            
    }];
        
        //收消息
        
    HttpReceiveMsgAPI *httpReceiveMsg = [[HttpReceiveMsgAPI alloc] init];
        [httpReceiveMsg registerAPIInAPIScheduleReceiveData:^(NSDictionary *object, NSError *error) {
            
            //        [dic setObject:@(from_user_id) forKey:@"from_user_id"];
            //        [dic setObject:@(to_session_id) forKey:@"to_session_id"];
            //        [dic setObject:@(msg_id) forKey:@"msg_id"];
            //        [dic setObject:@(group_id) forKey:@"group_id"];
            //        [dic setObject:@(reserver) forKey:@"reserver"];
            //        [dic setObject:@(create_time) forKey:@"create_time"];
            //        [dic setObject:@(msgType) forKey:@"msgType"];
            
            NSString *fromId= [object objectForKey:@"fromfrom_user_id_id"];
//            NSInteger msgID = [[object objectForKey:@"msg_id"] integerValue];
//            SessionType type = [[object objectForKey:@"msgType"] intValue];
            NSString * msgcontant = object[@"msgContant"];
//            if(msgID == 0)
//            {
//                msgID = 1;
//            }
            if (msgcontant) {
                NSString * sender_id = [NSString stringWithFormat:@"user_%@",fromId];
                DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sender_id senderID:sender_id msgContent:msgcontant toUserID:sender_id];
                messageEntity.state = DDmessageSendSuccess;
                [[DDDatabaseUtil instance] insertMessages:@[messageEntity] success:^{
                    
                } failure:^(NSString *errorDescripe) {
                    
                }];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
            }


        }];
        //监听确认添加好友API
        MsgAddFriendConfirmNotifyAPI * addFriendConNotify = [[MsgAddFriendConfirmNotifyAPI alloc]init];
        
        [addFriendConNotify registerAPIInAPIScheduleReceiveData:^(id object, NSError *error) {
            
            NSMutableString *fromId= [NSMutableString stringWithFormat:@"%@",[object objectForKey:@"from_id"]] ;
            
            NSInteger  result = [[object objectForKey:@"result"]integerValue];
            NSString * message ;
            NSRange  range = [fromId rangeOfString:@"user_"];
            [fromId deleteCharactersInRange:range];
            NSMutableString * userId =  [NSMutableString stringWithFormat:@"%@",[RuntimeStatus instance].user.objID];
            [userId deleteCharactersInRange:range];
            //告诉杨老师的服务器添加好友的通知收到了
            NSDictionary * paramas = @{@"uid":userId,@"fromid":userId,@"toid":fromId,@"type":@"2",@"token":@"110"};
            [HttpTool postWithPath:@"UpdateNotifyState" params:paramas success:^(id JSON) {
               
                
            } failure:^(NSError *error) {
                

            }];
            
            //添加确认的请求到数据库*****************
            NSString * resultVal;
            //客户端现在自定义的向后台发1同意0拒绝
            if (result > 0) {
                message = [NSString stringWithFormat:@"%@同意了你的好友请求",fromId];
                [SVProgressHUD showSuccessWithStatus:message duration:2.0];

                resultVal = @"1";
                [HttpTool postWithPath:@"GetPersonInfo" params:@{@"uid":userId,@"personid":fromId} success:^(id JSON) {
                    if ([[JSON objectForKey:@"result"]integerValue]==1) {
                        NSDictionary * persondic = [JSON objectForKey:@"personinfo"];
                        [[NSNotificationCenter defaultCenter]postNotificationName:BE_AGREE_FRIEND object:persondic];//接受对方同意添加自己为好友
                    }
                } failure:^(NSError *error) {
                    
                    
                }];
                
            }else{
                message = [NSString stringWithFormat:@"%@拒绝了你的好友请求",fromId];
                [SVProgressHUD showSuccessWithStatus:message duration:2.0];

                resultVal = @"0";
            }
            
        }];
        MsgDeleteFriendNotifyAPI * deleteFrindNotify = [[MsgDeleteFriendNotifyAPI alloc]init];
        [deleteFrindNotify registerAPIInAPIScheduleReceiveData:^(id object, NSError *error) {
            
            NSMutableString *fromId= [NSMutableString stringWithFormat:@"%@",[object objectForKey:@"from_id"]] ;
            NSRange  range = [fromId rangeOfString:@"user_"];
            [fromId deleteCharactersInRange:range];
            NSString * toid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            
       //做删除好友的操作，本地的消息会话体删除，好友关系删除
            [[ NSNotificationCenter defaultCenter]postNotificationName:DELETE_FRIEND object:fromId];
            [[DDDatabaseUtil instance]clearUpAddMessages:fromId toid:toid groupid:@"-2" type:@"-2"];
        
        }];
        
        //监听被添加对方发送的确认或者拒绝请求
    }
    return self;
}


-(void)getUnreadNotify
{
    //获取未读的添加类消息
    NSString * userid = [RuntimeStatus instance].user.objID;
    long  uid = [[RuntimeStatus instance]changeIDToOriginal:userid];
    NSDictionary * params = @{@"uid":[NSString stringWithFormat:@"%lu",uid],@"token":@"110"};
  
    [HttpTool postWithPath:@"GetUnreadNotify" params:params success:^(id JSON) {
        NSArray * unReadNotify = [NSArray arrayWithArray:JSON];
        
        for (int i = 0; i<[unReadNotify count]; i++) {
            
            NSDictionary * notifyDic = [unReadNotify objectAtIndex:i];
            NSString * type = [notifyDic objectForKey:@"type"];
            NSArray * notifyArr = [notifyDic objectForKey:@"notifylist"];
            [self.notifyDic setObject:notifyArr forKey:type];
            [self distinguishAddMessages:notifyArr type:type];
        }
    }failure:^(NSError *error) {
        
        
    }];

    if ([RuntimeStatus instance].pushToken) {
        SendPushTokenAPI *pushToken = [[SendPushTokenAPI alloc] init];
        [pushToken requestWithObject:[RuntimeStatus instance].pushToken Completion:^(id response, NSError *error) {
            
        }];
    }


}


#pragma mark 将被添加的消息区分后假如到数据库
-(void)distinguishAddMessages :(NSArray *)arrary type:(NSString*)type
{
    if ([type isEqualToString:@"1" ]) {
    
    for (int i = 0; i < [arrary count]; i++) {
        NSMutableDictionary *  dic = [[arrary objectAtIndex:i]mutableCopy];
        NSString * fromid = [dic objectForKey:@"fromid"];
        NSString* toid = [dic objectForKey:@"toid"];
        //获取添加的信息
        NSString * addMessage = [dic objectForKey:@"magdata"];
        if (addMessage==NULL ||[addMessage isEqual:nil]) {
            addMessage = @"";
        }
        [HttpTool postWithPath:@"GetPersonInfo" params:@{@"uid":toid,@"personid":fromid,@"token":@"110"} success:^(id JSON) {
            NSDictionary * persondic = [JSON objectForKey:@"personinfo"];
            NSString * name = [persondic objectForKey:@"nickname"];
            NSString * portrait = [persondic objectForKey:@"portrait"];
            NSString * result = @"-2";
            NSString * groupid =@"-2";
            NSString * typeVal = @"1";
            NSString * haschange = @"0";
            NSString * grouptype = @"-2";
            if (portrait==nil) {
                portrait =@"100";
            }
            NSString * text = [NSString stringWithFormat:@"%@申请添加你为好友",name];
            NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
            NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
            NSMutableDictionary * dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:fromid, @"fromid",toid,@"toid",groupid,@"groupid",result,@"result",typeVal,@"type",name,@"name",portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype" ,text,@"detailtext", addMessage,@"addmessage",time,@"time",  nil];
            
            //将监听的消息假如数据库
            [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dictionary];
            //给自己发一条消息
            NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
            DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
            messageEntity.state = DDmessageSendSuccess;
            
            [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
            //************************************
            
            
        } failure:^(NSError *error) {
            
            
        }];
        
    }
}if ([type isEqualToString:@"5" ]) {
    //群邀请个人进入
    for (int i = 0; i < [arrary count]; i++) {
        NSMutableDictionary *  dic = [[arrary objectAtIndex:i]mutableCopy];
        NSString * fromid = [dic objectForKey:@"fromid"];
        NSString* toid = [dic objectForKey:@"toid"];
        NSString * groupid = [dic objectForKey:@"groupid"];
        
        [HttpTool postWithPath:@"GetGroupDetail" params:@{@"uid":toid,@"groupid":groupid,@"token":@"110"} success:^(id JSON) {
            NSDictionary * persondic = [JSON objectForKey:@"groupdetails"];
            NSString * name = [persondic objectForKey:@"refname"];
            NSString * portrait = [persondic objectForKey:@"portrait"];
            NSString * grouptype = [persondic objectForKey:@"type"];
            NSString * result = @"-2";
            NSString * haschange = @"0";
            if (portrait==nil) {
                portrait = @"100";
            }
            NSString * text =@"";
            if ([grouptype isEqualToString:@"1"]) {
                text = [NSString stringWithFormat:@"%@家族邀请你加入",name];
            }else{
                text = [NSString stringWithFormat:@"%@剧邀请你加入",name];

            }
            NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
            NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
            NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:fromid,@"fromid",toid,@"toid",groupid,@"groupid",result,@"result",@"3",@"type",name,@"name" ,portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype",text,@"detailtext" , time,@"time" ,nil];
            //将监听的消息假如数据库
            [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dic];
            //************************************
            //给自己发一条消息
            NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
            DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
            messageEntity.state = DDmessageSendSuccess;
            
            [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
            
        } failure:^(NSError *error) {
            
            
        }];
        
    }
    
}
    if ([type isEqualToString:@"3" ]) {
        for (int i = 0; i < [arrary count]; i++) {
            NSMutableDictionary *  dic = [[arrary objectAtIndex:i]mutableCopy];
            NSString * fromid = [dic objectForKey:@"fromid"];
            NSString* toid = [dic objectForKey:@"toid"];
            NSString * groupid = [dic objectForKey:@"groupid"];
            [HttpTool postWithPath:@"GetPersonInfo" params:@{@"uid":toid,@"personid":fromid,@"token":@"110"} success:^(id JSON) {
                NSDictionary * persondic = [JSON objectForKey:@"personinfo"];
                NSString * name = [persondic objectForKey:@"nickname"];
                NSString * portrait = [persondic objectForKey:@"portrait"];
                NSString * result = @"-2";
                __block NSString * groupType;
                if ([portrait isEmpty]) {
                    portrait = @"100";
                }
        
                __block NSString * text=@"";
                [HttpTool postWithPath:@"GetGroupDetail" params:@{@"uid":toid,@"groupid":groupid,@"token":@"110"} success:^(id JSON) {
                    NSDictionary * groupdetail = [JSON objectForKey:@"groupdetails"];
                    NSString * typeVal = [groupdetail objectForKey:@"type"];
                    NSString * gname = [groupdetail objectForKey:@"refname"];
                    groupType = typeVal;
                    if ([typeVal isEqualToString:@"1"]) {
                        //对方申请进家族
                        text = [NSString stringWithFormat:@"%@申请加入家族%@",name,gname];
                        
                    }else{
                        text = [NSString stringWithFormat:@"%@申请加入剧%@",name,gname];
                    }
                    
          
                    NSString * haschange = @"0";
                    NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                    NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                    NSMutableDictionary * dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:fromid, @"fromid",toid,@"toid",groupid,@"groupid",result,@"result",@"5",@"type",name,@"name",portrait,@"portrait",haschange,@"haschange",groupType,@"grouptype",text,@"detailtext" ,time,@"time", nil];
                    
                    //将监听的消息假如数据库
                    [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dictionary];
                    //************************************
                    //给自己发一条消息
                    NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                    DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                    messageEntity.state = DDmessageSendSuccess;
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                    
                } failure:^(NSError *error) {
                    
                    
                }];
                
            }failure:^(NSError *error) {
                
                
            }];
            
        }
    }
    if ([type isEqualToString:@"4"]) {
        //管理员邀请他人加入群的反馈结果
        for (int i = 0; i < [arrary count]; i++) {
            NSMutableDictionary *  dic = [[arrary objectAtIndex:i]mutableCopy];
            NSString * fromid = [dic objectForKey:@"fromid"];
            NSString* toid = [dic objectForKey:@"toid"];
            NSString * groupid = [dic objectForKey:@"groupid"];
            [HttpTool postWithPath:@"GetPersonInfo" params:@{@"uid":toid,@"personid":fromid,@"token":@"110"} success:^(id JSON) {
                NSDictionary * persondic = [JSON objectForKey:@"personinfo"];
                NSString * name = [persondic objectForKey:@"nickname"];
                NSString * portrait = [persondic objectForKey:@"portrait"];
                NSString * result = @"-2";
                __block NSString * groupType;
                if ([portrait isEmpty]) {
                    portrait = @"100";
                }
                
                __block NSString * text=@"";
                [HttpTool postWithPath:@"GetGroupDetail" params:@{@"uid":toid,@"groupid":groupid,@"token":@"110"} success:^(id JSON) {
                    NSDictionary * groupdetail = [JSON objectForKey:@"groupdetails"];
                    NSString * typeVal = [groupdetail objectForKey:@"type"];
                    NSString * gname = [groupdetail objectForKey:@"refname"];
                    groupType = typeVal;
                    if ([typeVal isEqualToString:@"1"]) {
                        //对方申请进家族
                        text = [NSString stringWithFormat:@"%@同意入驻家族%@",name,gname];
                        
                    }else{
                        text = [NSString stringWithFormat:@"%@同意入驻剧%@",name,gname];
                    }
                    
                    
                    NSString * haschange = @"0";
                    NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                    NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                    NSMutableDictionary * dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:fromid, @"fromid",toid,@"toid",groupid,@"groupid",result,@"result",@"-2",@"type",name,@"name",portrait,@"portrait",haschange,@"haschange",groupType,@"grouptype",text,@"detailtext" ,time,@"time", nil];
                    
                    //将监听的消息假如数据库
                    [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dictionary];
                    //给自己发一条消息
                    NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                    DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                    messageEntity.state = DDmessageSendSuccess;
                    
                } failure:^(NSError *error) {
                    
                    
                }];
                
            }failure:^(NSError *error) {
                
            }];
            
        }
}
    if ([type isEqualToString:@"6"]) {
        //用户加入群，管理员反馈的结果
        for (int i = 0; i < [arrary count]; i++) {
            NSMutableDictionary *  dic = [[arrary objectAtIndex:i]mutableCopy];
            NSString * fromid = [dic objectForKey:@"fromid"];
            NSString* toid = [dic objectForKey:@"toid"];
            NSString * groupid = [dic objectForKey:@"groupid"];
            
            [HttpTool postWithPath:@"GetGroupDetail" params:@{@"uid":toid,@"groupid":groupid,@"token":@"110"} success:^(id JSON) {
                NSDictionary * persondic = [JSON objectForKey:@"groupdetails"];
                NSString * name = [persondic objectForKey:@"refname"];
                NSString * portrait = [persondic objectForKey:@"portrait"];
                NSString * grouptype = [persondic objectForKey:@"type"];
                NSString * result = @"-2";
                NSString * haschange = @"0";
                if (portrait==nil) {
                    portrait = @"100";
                }
                NSString * text=@"";
                if ([grouptype isEqualToString:@"1"]) {
                    text = [NSString stringWithFormat:@"%@家族同意你加入",name];
                }else{
                    text = [NSString stringWithFormat:@"%@剧同意你加入",name];
                    
                }
                NSInteger timeInterval = [[NSDate date]timeIntervalSince1970];
                NSString * time = [NSString stringWithFormat:@"%ld",timeInterval];
                NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:fromid,@"fromid",toid,@"toid",groupid,@"groupid",result,@"result",@"-2",@"type",name,@"name" ,portrait,@"portrait",haschange,@"haschange",grouptype,@"grouptype",text,@"detailtext" ,time,@"time", nil];
                //将监听的消息假如数据库
                [[DDDatabaseUtil instance]updateNewAddMessagesFromdic:dic];
                //************************************
                //给自己发一条消息
                NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                messageEntity.state = DDmessageSendSuccess;
                
                [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                
            } failure:^(NSError *error) {
                
            }];
            
        }
    }
}


@end
