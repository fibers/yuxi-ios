//
//  AddGroupMemberModule.m
//  inface
//
//  Created by xigesi on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "AddGroupMemberModule.h"
#import "MsgAddGroupMemberReqAPI.h"
@implementation AddGroupMemberModule

+(instancetype)sharedInstance
{
    static AddGroupMemberModule * addGroupMemberModule;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,^{
        addGroupMemberModule = [[AddGroupMemberModule alloc]init];
    });
    return addGroupMemberModule;
    
}

#pragma mark 添加成员，需要成员反馈
-(void)addGroupMembersessionType:(SessionType)sessionType groupId:(NSString *)groupId toUserIds:(NSArray *)toUserIds msgId:(NSString *)msgId ownerId:(NSString*)ownerId
{
    MsgAddGroupMemberReqAPI * addGroupMemberApi = [[MsgAddGroupMemberReqAPI alloc]init];
    
    NSArray * object = @[groupId,toUserIds,@(SessionTypeSessionTypeGroup),@(0),ownerId];
     [addGroupMemberApi requestWithObject:object Completion:^(id response, NSError *error) {
         
         
     }];

}

#pragma mark 用户主动申请加群,需要群主确认
-(void)applyToJoinGroupsessionType:(SessionType )sessionType groupId:(NSString*)groupId toUserIds:(NSArray*)toUserIds msgId:(NSString *)msgId ownerId:(NSString*)ownerId

{
    MsgAddGroupMemberReqAPI * addGroupMemberAPI = [[MsgAddGroupMemberReqAPI alloc]init];
    
    NSArray * objec = @[groupId,toUserIds,@(sessionType),@(0),ownerId];
    [addGroupMemberAPI requestWithObject:objec Completion:^(id response, NSError *error) {
        
        
    }];

}

/*\
 
 NSArray * uids = @[[RuntimeStatus instance].user.objID] ;
 NSArray * obj = @[groupName,@"",uids];
 [creatGroup requestWithObject:obj Completion:^(GroupEntity * response, NSError *error) {
 if (response) {
 SessionEntity * session = [[SessionEntity alloc]initWithSessionID:response.objID  type:SessionTypeSessionTypeGroup];
 session.name = response.name;
 
 [(FamilyViewController *)self.delegate AddSession :session ];
 }
 }];
 
 
 
 
 
 
 */


@end
