//
//  GetUnreadNotify.h
//  inface
//
//  Created by xigesi on 15/5/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseTabBarViewController.h"
@interface GetUnreadNotify : NSObject
@property(strong,nonatomic)NSMutableDictionary * notifyDic;

+(instancetype)shareInsatance;
-(void)getUnreadNotify ;
@end
