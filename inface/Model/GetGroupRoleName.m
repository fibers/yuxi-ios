//
//  GetGroupRoleName.m
//  inface
//
//  Created by appleone on 15/6/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GetGroupRoleName.h"

@implementation GetGroupRoleName
+(instancetype)shareInstance
{
    static GetGroupRoleName * getGroupRoleName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getGroupRoleName = [[GetGroupRoleName alloc] init];
        
    });
    return getGroupRoleName;
}


-(instancetype)init
{
    self = [super init];
    if (self) {
        groupRoles = [NSMutableDictionary dictionary];
        storyRoles = [NSMutableDictionary dictionary];
    }
    return  self;
}


#pragma mark 存储家族成员的信息
-(void)saveFamily:(NSString*)familyId membersArr:(NSArray * )membersArrary//根据familyId存储家族成员信息
{
    [groupRoles setObject:membersArrary forKey:familyId];
    
}

#pragma mark根据familyID获取家族群成员
-(NSArray * )familyMembers:(NSString*)familyid
{
    return [groupRoles objectForKey:familyid];
}






#pragma mark 获取家族详情
-(void)getFamilyInfo:(NSString *)familyid userid:(NSString *)userid success:(void(^)(id  JSON))success
{
    NSString * user_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * parames = @{@"uid":user_id,@"familyid":familyid,@"token":@"110"};
    [HttpTool postWithPath:@"GetFamilyInfo" params:parames success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            if ([JSON isKindOfClass:[NSDictionary class]]) {
                NSDictionary * familydic = [JSON objectForKey:@"familyinfo"];
                NSArray * memberArr = [familydic objectForKey:@"members"];
                [groupRoles setObject:memberArr forKey:familyid];
                success(memberArr);
            }

        }

    } failure:^(NSError *error) {
        
        
    }];
    
}

-(void)ReturnFamilyInfo:(NSString*)familyid  userid:(NSString*)userid success:(void (^)(NSDictionary *))success
{

    NSArray * familyinfo = [groupRoles objectForKey:familyid];
    BOOL isExistMember = NO;
    for (NSDictionary * dic in familyinfo) {
        if ([[dic allValues]containsObject:userid]) {
            isExistMember = YES;
            success(dic);
        }
        
    }
    if (!isExistMember) {
        NSDictionary * personinfo = @{@"id":userid,@"nickname":userid,@"portrait":@"zs_touxianglan.png"};
        success(personinfo);
        NSMutableArray  * memberArr = [NSMutableArray arrayWithArray:[groupRoles objectForKey:familyid]];
        [memberArr addObject:personinfo];
        [groupRoles setObject:memberArr forKey:personinfo];
    }
    //如果成员不存在，需要重新获取该群的members
    if (!isExistMember) {
        [self getFamilyInfo:familyid userid:userid success:^(id JSON) {
            
                NSArray * membersArr = (NSArray*)JSON;
                BOOL isExist = NO;
                for (NSDictionary * dic in membersArr) {
                    if ([[dic allValues]containsObject:userid]) {
                        isExist = YES;
                        success(dic);
                        //此时需要刷新聊天页面,需要判断是否还是之前的家族聊天页面

                    }
                }
            if (!isExist) {
        
                    NSDictionary * personinfo = @{@"id":userid,@"nickname":userid,@"portrait":@"zs_touxianglan.png"};
                    success(personinfo);
                    NSMutableArray  * memberArr = [NSMutableArray arrayWithArray:[groupRoles objectForKey:familyid]];
                    [memberArr addObject:personinfo];
                    [groupRoles setObject:memberArr forKey:personinfo];
                
            }
            
        }];
    }
}


#pragma mark
-(void)getGroupRoleName:(NSString *)storyid groupid:(NSString *)groupid success:(void(^)(id  JSON))success
{
    NSString * uid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    //获取剧群详情
    NSDictionary * params = @{@"uid":uid,@"storyid":storyid,@"groupid":groupid,@"token":@"110"};
    [HttpTool postWithPath:@"GetStoryGroupInfo" params:params success:^(id JSON) {
        NSDictionary * membersDic = [JSON objectForKey:@"groupinfo"];
        NSArray * memberArr = [membersDic objectForKey:@"members"];
        [groupRoles setObject:memberArr forKey:groupid];
        success(memberArr);
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark 根据userid和群id获取角色名
-(void)ReturnRoleName:(NSString*)groupid  stroryid:(NSString*)storyid userid:(NSString *)userid success:(void (^)(NSDictionary *))success
{
    NSArray * membersArr = [groupRoles objectForKey:groupid];
    __block BOOL isExistMember = NO;
    [membersArr enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj.allValues containsObject:userid]) {
            isExistMember = YES;
            success(obj);
        }
    }];
    
    if (!isExistMember) {
        NSDictionary * personinfo = @{@"id":userid,@"nickname":userid,@"portrait":@"zs_touxianglan.png"};
        success(personinfo);
        NSMutableArray  * memberArr = [NSMutableArray arrayWithArray:[groupRoles objectForKey:groupid]];
        [memberArr addObject:personinfo];
        [groupRoles setObject:memberArr forKey:personinfo];

    }

    //如果成员不存在，需要重新获取该群的members
    if (!isExistMember) {
        [self getGroupRoleName:storyid groupid:groupid success:^(id JSON) {
           
            NSArray * membersArr = (NSArray *)JSON;
            BOOL isExist = NO;
            for (NSDictionary * dic in membersArr) {
                if ([[dic allValues]containsObject:userid]) {
                    isExist = YES;
                    success(dic);
                }
            }
            if (!isExist) {
                    //此时判断该人不在群里了，获取该用户信息
                    NSDictionary * personinfo = @{@"id":userid,@"nickname":userid,@"portrait":@"zs_touxianglan.png"};
                    NSMutableArray  * memberArr = [NSMutableArray arrayWithArray:[groupRoles objectForKey:groupid]];
                    [memberArr addObject:personinfo];
                    [groupRoles setObject:memberArr forKey:personinfo];
                    success(personinfo);
            }
        }];
    }
    
}



#pragma mark groupRoles键值中是否有groupid字典
-(BOOL)isNeedGetGroupMembers:(NSString*)groupid
{
    NSArray * groupArr = [groupRoles objectForKey:groupid];
    if (!groupArr) {
        return NO;
    }else{
        return YES;
    }
    
}

@end
