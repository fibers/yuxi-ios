//
//  GetLocalRecentSessions.m
//  inface
//
//  Created by xigesi on 15/5/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GetLocalRecentSessions.h"
#import "StorySessionsModel.h"
@implementation GetLocalRecentSessions
+(instancetype)shareInstance
{
    static GetLocalRecentSessions * addGroupMemberModule;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,^{
        addGroupMemberModule = [[GetLocalRecentSessions alloc]init];
    });
    return addGroupMemberModule;
    
}


-(instancetype)init
{
    self = [super init];
    if (self) {
        sessionsDic = [NSMutableDictionary new];
    }
    return self;
}


#pragma mark 获取本地消息
-(void)getLocalSessionSuccess:(void (^)(id))success
{
    [[SessionModule sharedInstance] loadLocalSession:^(bool isok) {
        if (isok) {

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[SessionModule sharedInstance] getRecentSession:^(NSUInteger count) {
                
                    BaseTabBarViewController * baseViewController = [[BaseTabBarViewController alloc]initWithNibName:@"BaseTabBarViewController" bundle:nil];
                    success(baseViewController);

                }];
            });
            
        }
    }];
}


-(void)totalUnreadMessagesCountSuccess:(void(^)(id json))success
{
    NSArray * sessions =  [[SessionModule sharedInstance]getAllSessions];
    NSMutableArray * unreadArrary = [NSMutableArray array];
        //此时是临时群的消息
        for (int i = 0; i < sessions.count; i++) {
            SessionEntity * sessionEntity = [sessions objectAtIndex:i];
            [unreadArrary addObject:sessionEntity];
        }
    NSInteger count = [[unreadArrary valueForKeyPath:@"@sum.unReadMsgCount"]integerValue];
    if (count > 0) {
        NSString * unreadcount = [NSString stringWithFormat:@"%ld",count];
        success(unreadcount);
    }
 
}


-(void)totalGroupsUnreadMessageCountSuccess:(void(^)(id  json))success;
{   NSMutableArray * sessionsArr = [NSMutableArray array];
    NSArray *ChatArr=[[SessionModule sharedInstance]getAllSessions];
    for (int i=0; i<ChatArr.count; i++) {
        SessionEntity *sessionEntity=[ChatArr objectAtIndex:i];
        //如果是群消息，需要判断是非临时群的消息
        if (sessionEntity.sessionType == SessionTypeSessionTypeGroup) {
            NSString * session_id = sessionEntity.sessionID;
            NSArray * sessionArr = [session_id componentsSeparatedByString:@"_"];
            NSString * groupid = [sessionArr objectAtIndex:1];
            [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:groupid success:^(id JSON) {
                
                //如果此群消息为非临时群消息时加载到消息列表
                if (JSON) {
                    [sessionsArr addObject:sessionEntity];
                }
            }failure:^(id Json) {
                
            }];
        }else{
            [sessionsArr addObject:sessionEntity];
        }
        
        
        //sessionType 1为单个用户会话2为群会话
    }
    NSInteger count = [[sessionsArr valueForKeyPath:@"@sum.unReadMsgCount"]integerValue];
    if (count > 0) {
        NSString * unreadcount = [NSString stringWithFormat:@"%ld",count];
        success(unreadcount);
        
    }
    
}



-(void)sortItems
{
     NSArray *ChatArr=[[SessionModule sharedInstance] getAllSessions];
//    NSMutableArray * storyArr = [NSMutableArray array];//数组里
     groupsDic = [NSMutableDictionary new];//根据键值是剧id存放的会话体数组
    for (int  i = 0; i < [ChatArr count]; i++) {
        SessionEntity * entity = [ChatArr objectAtIndex:i];
        if (entity.sessionType == 1) {
            [singleSessions addObject:entity];
        }else{
            
            //需要区分是家族的会话还是剧的会话
            NSArray * groupArr = [entity.sessionID componentsSeparatedByString:@"_"];
            NSString * groupid = [groupArr objectAtIndex:1];
            //根据groupid区分会话体的类型
            [[GetAllFamilysAndStorys shareInstance]getGroupTypeStringgroupid:groupid success:^(id JSON) {
                //如果根据groupid返回的类型是1时，说明是家族的消息
                if ([JSON isEqualToString:@"1"]) {
                    [familySessions addObject:entity];
                }else{
                    //此时是剧群的消息
                    NSString * storyId = [[GetAllFamilysAndStorys shareInstance]getStoryidByGroupid:groupid];
                    NSMutableArray * storyArr = [NSMutableArray array];//数组里添加的是一个群id下的一个会话体
                    storyArr = [[groupsDic objectForKey:storyId]mutableCopy];

                        //该群id对应的剧已经在字典里了，只需要在字典里加上该群id对应的字典
                    NSDictionary * dic = [NSDictionary dictionaryWithObject:entity forKey:groupid];
                    [storyArr addObject:dic];
                    [groupsDic setValue:storyArr forKey:storyId];
                    
                }
            }];
        }
    }

}


#pragma mark 根据会话iD存储session,存储session的时候已经去掉_
-(void)saveSessionEntity:(SessionEntity *)sessionEntity//根据会话的id存储会话

{
    NSString * session_ID = sessionEntity.sessionID;
    NSArray * idStr = [session_ID componentsSeparatedByString:@"_"];
    NSString * sessionId ;

    if (idStr.count ==1) {
        sessionId = idStr[0];
    }else{
        sessionId = idStr[1];
    }
    [sessionsDic setObject:sessionEntity forKey:sessionId];
    
}


#pragma mark 根据会话id获取session
-(void)returnSession:(NSString * )sessionId success:(void(^)(id  JSON))success;
{
    SessionEntity * session = [sessionsDic objectForKey:sessionId];
  
    success(session);

}


#pragma mark 获取本地消息
-(void)returnSessions:(int)sessionType success:(void(^)(id JSON))success;
{
    switch (sessionType) {
        case 0:
            //获取好友的消息
            success(singleSessions);
            break;
        case 1:
            success(familySessions);
            break;
        case 2:
            success(groupsDic);
            
        default:
            break;
    }
}

@end
