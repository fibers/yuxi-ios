//
//  DiscussModel.h
//  inface
//
//  Created by appleone on 15/7/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiscussModel : NSObject
{
}
@property (strong,nonatomic)    NSMutableDictionary * memberArrDic;

+(instancetype)shareInstance;
-(void)readyDiscussGroup:(NSString*)groupId success:(void(^)(id json))success;
-(void)getCurrentGroupMembers:(NSString *)groupid success:(void(^)(id json))success ;//根据群id获取临时群成员列表
-(void)returnGroupMembers:(NSString * )groupid userid:(NSString *)userid  success:(void(^)(NSDictionary * personDic))success;//返回成员所在群的名字

@end
