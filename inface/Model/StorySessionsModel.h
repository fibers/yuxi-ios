//
//  StorySessionsModel.h
//  inface
//
//  Created by appleone on 15/6/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StorySessionsModel : NSObject
@property(strong,nonatomic)NSMutableDictionary * groupsDic;
@property(assign,nonatomic)int  unreadCount;
+(instancetype)shareInstance;
-(void)getLocalSessions;
-(void)returnLocalSessionsSuccess:(void(^)(id JSON))success;
-(void)updateLocalSessions:(SessionEntity*)session success:(void(^)(id  JSON))success;
-(void)getSessionBySelfGroupId:(NSString*)groupid storyid:(NSString*)storyid isMulusUnreadCount:(NSString*)yesOrNo  success:(void(^)(id JSON))success failure:(void(^)(id error))failure;//进入剧群的时候从已有的字典中判断是否已经聊过天
@end
