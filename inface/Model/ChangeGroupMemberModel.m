//
//  ChangeGroupMemberModel.m
//  inface
//
//  Created by xigesi on 15/5/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ChangeGroupMemberModel.h"
#import "DDAddMemberToGroupAPI.h"
#import "DDDeleteMemberFromGroupAPI.h"
#import "DDAddMemberToGroupAPI.h"
#import "DDDeleteMemberFromGroupAPI.h"
@implementation ChangeGroupMemberModel

+(instancetype)shareInsatance
{
    static ChangeGroupMemberModel* g_changeGroupMemberModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_changeGroupMemberModel = [[ChangeGroupMemberModel alloc] init];
        
    });
    return g_changeGroupMemberModel;
}

#pragma 直接添加成员,不需要确认
-(void)addMemberNotNeedNotifygroupid:(NSString *)groupid users:(NSArray *)users
{
    DDAddMemberToGroupAPI * addGroup = [[DDAddMemberToGroupAPI alloc]init];
    
    [addGroup requestWithObject:@[groupid,users] Completion:^(id response, NSError *error) {
        
        
    }];


    
}


#pragma mark 删除成员
-(void)deleteMembersNoNeedNotifygroupid:(NSString *)groupid users:(NSArray *)users
{
    DDDeleteMemberFromGroupAPI * deleteMemberFromGroup = [[DDDeleteMemberFromGroupAPI alloc]init];
    [deleteMemberFromGroup requestWithObject:@[groupid,users] Completion:^(id response, NSError *error) {
        
        
    }];
    

}


#pragma mark 删除退出家族
-(void)exitFamilyUid:(NSString *)uid familyid:(NSString *)familyid token:(NSString *)token success:(void (^)(id))success failure:(void (^)(id))failure
{
    NSDictionary * dic = @{@"uid":uid,@"familyid":familyid,@"token":@"110"};
    
    [HttpTool postWithPath:@"ExitFamily" params:dic success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            success([JSON objectForKey:@"result"]);
        }
        
    } failure:^(NSError *error) {
        failure(error);
        
    }];
}
@end
