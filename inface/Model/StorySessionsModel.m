//
//  StorySessionsModel.m
//  inface
//
//  Created by appleone on 15/6/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "StorySessionsModel.h"
@implementation StorySessionsModel
+(instancetype)shareInstance;
{
    static StorySessionsModel * strorySessionModel;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,^{
        strorySessionModel = [[StorySessionsModel alloc]init];
    });
    return strorySessionModel;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        self.groupsDic = [NSMutableDictionary new];
        self.unreadCount = 0;
    }
    return self;
}



#pragma mark 获取本地的剧群聊天信息
-(void)getLocalSessions
{
    NSArray *ChatArr=[[SessionModule sharedInstance]getAllSessions];
    for (int i=0; i<ChatArr.count; i++) {
        SessionEntity *sessionEntity=[ChatArr objectAtIndex:i];
        if (sessionEntity.sessionType==2 ){
            NSArray * grouparr = [sessionEntity.sessionID componentsSeparatedByString:@"_"];
            NSString * groupid = [grouparr objectAtIndex:1];
            [[GetAllFamilysAndStorys shareInstance]getGroupTypeStringgroupid:groupid success:^(id JSON) {
                if ([JSON isEqualToString:@"2"]) {
                    //需要区分是家族的会话还是剧的会话
                    NSArray * groupArr = [sessionEntity.sessionID componentsSeparatedByString:@"_"];
                    NSString * groupid = [groupArr objectAtIndex:1];
                    //根据groupid区分会话体的类
                        //如果根据groupid返回的类型是1时，说明是家族的消息
                        //此时是剧群的消息
                        NSString * storyId = [[GetAllFamilysAndStorys shareInstance]getStoryidByGroupid:groupid];
                        NSMutableArray * storyArr ;//数组里添加的是一个群id下的一个会话体
                        storyArr = [[self.groupsDic objectForKey:storyId]mutableCopy];
                    if (storyId) {
                    
                        if ([storyArr count]> 0) {
                            
                            //该群id对应的剧已经在字典里了，只需要在字典里加上该群id对应的字典
                            NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObject:sessionEntity forKey:groupid];
                            [storyArr addObject:dic];
                            [self.groupsDic setValue:storyArr forKey:storyId];
                        }else{
                            storyArr = [NSMutableArray array];
                            //该群id对应的剧已经在字典里了，只需要在字典里加上该群id对应的字典
                            NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObject:sessionEntity forKey:groupid];
                            [storyArr addObject:dic];
                            [self.groupsDic setValue:storyArr forKey:storyId];
                        }
                    }

                    self.unreadCount += (int)sessionEntity.unReadMsgCount;
                }
                
            }];
            //sessionType 1为单个用户会话2为群会
            
        }
    }

}


#pragma mark 返回会话体的数据
-(void)returnLocalSessionsSuccess:(void(^)(id JSON))success
{
    NSArray * arr = [NSArray arrayWithObjects:self.groupsDic,@(self.unreadCount), nil];
    success(arr);
    
}

#pragma mark 更新本地存储的剧群消息
-(void)updateLocalSessions:(SessionEntity*)session success:(void(^)(id  JSON))success
{
    self.unreadCount = 0;
    NSArray * groupArr = [session.sessionID componentsSeparatedByString:@"_"];
    NSString * groupid = [groupArr objectAtIndex:1];
    //根据groupid区分会话体的类型
    //如果根据groupid返回的类型是1时，说明是家族的消息
    
    //此时是剧群的消息
    NSString * storyId = [[GetAllFamilysAndStorys shareInstance]getStoryidByGroupid:groupid];
    NSMutableArray * storyArr = [NSMutableArray array];//数组里添加的是一个群id下的一个会话体
    storyArr = [[self.groupsDic objectForKey:storyId]mutableCopy];
    BOOL isExit = NO;
    //如果新的会话id已经存在数组里了，只需要更新相应的字典就可以
    for (int i = 0;i < [storyArr count];i++) {
        NSMutableDictionary * dic = [storyArr objectAtIndex:i];
        if ([[dic allKeys]containsObject:groupid]) {
            [dic setObject:session  forKey:groupid];
            [storyArr replaceObjectAtIndex:i withObject:dic];
            isExit = YES;
        }
        
    }
    if (isExit == NO) {
        
        NSDictionary * dic = [NSDictionary dictionaryWithObject:session forKey:groupid];
        [storyArr addObject:dic];
    }
    [self.groupsDic setValue:storyArr forKey:storyId];
   
    for (int i = 0; i < [self.groupsDic.allValues count]; i++) {
        NSArray * groupsArr = [[self.groupsDic allValues]objectAtIndex:i];
        for (int j = 0; j < [groupsArr count]; j++) {
            NSDictionary * groupsEntiry = [groupsArr objectAtIndex:j];
            NSString * groupsStr = [[groupsEntiry allKeys]objectAtIndex:0];
            SessionEntity * session = [groupsEntiry objectForKey:groupsStr];
            self.unreadCount += (int)session.unReadMsgCount;
        }
    }
    NSArray * arr = [NSArray arrayWithObjects:self.groupsDic,@(self.unreadCount), nil];
    success(arr);
    
}



#pragma mark 判断当前进入的戏群有没有聊天记录，要是没有聊天记录，需要重新构造会话体，要是已经聊过，直接从已有的session体中开启会话，这样做是为了消除session中得未读消息
-(void)getSessionBySelfGroupId:(NSString*)groupid storyid:(NSString*)storyid isMulusUnreadCount:(NSString*)yesOrNo success:(void(^)(id JSON))success failure:(void(^)(id error))failure;
{
    
    NSMutableArray * storyOfGroupsSessions = [self.groupsDic objectForKey:storyid];
    NSRange  range = [groupid rangeOfString:@"_"];
    NSString * groupID = groupid;
    if (range.length >0) {
        NSArray * strASrr = [groupid componentsSeparatedByString:@"_"];
        groupID = [strASrr objectAtIndex:1];
    }
    BOOL  isInSessions = NO;
    for (int i  =  0; i < [storyOfGroupsSessions count]; i++) {
        NSDictionary * groupSessionsDic = [storyOfGroupsSessions objectAtIndex:i];
        if ([[groupSessionsDic allKeys]containsObject:groupID]  ) {
            SessionEntity * session = [groupSessionsDic objectForKey:groupID];
            isInSessions = YES;
            int count = (int)session.unReadMsgCount;
            if ([yesOrNo isEqualToString:@"YES"]) {
                [self mulusUnreadCount:count];

            }
            success(session);
        }
    }
    if (isInSessions == NO) {
    NSString * inSession = @"Null";
        failure(inSession);
    }
    
    
}

#pragma mark 消除小红点
-(void)mulusUnreadCount:(int)count
{
    self.unreadCount -= count;
    
}



















@end
