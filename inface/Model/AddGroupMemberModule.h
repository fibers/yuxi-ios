//
//  AddGroupMemberModule.h
//  inface
//
//  Created by xigesi on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddGroupMemberModule : NSObject
+(instancetype)sharedInstance;
-(void)addGroupMembersessionType:(SessionType)sessionType groupId:(NSString *)groupId toUserIds:(NSArray *)toUserIds msgId:(NSString *)msgId ownerId:(NSString*)ownerId;
-(void)applyToJoinGroupsessionType:(SessionType )sessionType groupId:(NSString*)groupId toUserIds:(NSArray*)toUserIds msgId:(NSString *)msgId ownerId:(NSString*)ownerId;
@end
