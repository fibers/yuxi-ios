//
//  ChangeGroupMemberModel.h
//  inface
//
//  Created by xigesi on 15/5/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChangeGroupMemberModel : NSObject
#pragma 直接添加成员,不需要确认
-(void)addMemberNotNeedNotifygroupid:(NSString *)groupid users:(NSArray *)users;
#pragma mark 删除成员
-(void)deleteMembersNoNeedNotifygroupid:(NSString *)groupid users:(NSArray *)users;
-(void)exitFamilyUid:(NSString*)uid familyid:(NSString *)familyid token :(NSString*)token success :(void(^)(id JSON))success failure :(void(^)(id JSON))failure;//

+(instancetype)shareInsatance;
@end
