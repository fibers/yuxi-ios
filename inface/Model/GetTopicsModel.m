
//
//  GetTopicsModel.m
//  inface
//
//  Created by appleone on 15/7/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GetTopicsModel.h"
#define STORYS_COUNT    @"15"

@implementation GetTopicsModel {
    NSMutableDictionary *topTopicIDDic; //存放置顶的topicid。
}
+(instancetype)shareInstance
{
    static GetTopicsModel * getGroupRoleName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getGroupRoleName = [[GetTopicsModel alloc] init];
        
    });
    return getGroupRoleName;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        storys_Arr     = [NSMutableArray array];
        recommends_Arr = [NSMutableArray array];
        
        topTopicIDDic   =   [[NSMutableDictionary alloc] init];
    }
    return self;
}


#pragma mark 第一次获取话题和招募贴
-(void)firstGetStorysSuccess:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//第一次取数据，不需要上传所有参数
{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSMutableDictionary * story_recommend =[NSMutableDictionary dictionary];
    minFromId = 0;
    maxFromId = 0;
//    [storys_Arr removeAllObjects];
//    [recommends_Arr removeAllObjects];
    NSDictionary * params = @{@"uid":userid,@"count":STORYS_COUNT,@"token":@"110",@"boundaryid":@"0",@"direction":@"0",@"fromid":@"0"};
    [HttpTool postWithPath:@"GetTopics" params:params success:^(id JSON) {
        
        [topTopicIDDic  removeAllObjects];
        int     oldTopicId  =   0;
        //最后一条数据的id最大，最上一条数据id最小
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
            NSArray * storysArr = [JSON objectForKey:@"topiclist"];
            if (storysArr.count > 0) {
                minFromId = [[[storysArr objectAtIndex:([storysArr count] - 1)]objectForKey:@"topicid"]intValue];
                maxFromId = [[[storysArr objectAtIndex:0]objectForKey:@"topicid"]intValue];
                storys_Arr = [NSMutableArray arrayWithArray:storysArr];
                NSInteger maxTimeStamp = 0;
                for (int i = (int)[storys_Arr count] - 1; i >= 0; i--) {
                    NSDictionary * storyDic = [storys_Arr objectAtIndex:i];
                    int serverID = [[storyDic objectForKey:@"topicid"]intValue];
                    
                    if (i == (int)[storys_Arr count] - 1) {
                        oldTopicId  =   serverID;
                    }
                    else {
                        //如果没有置顶，倒叙排列都是id递增，当id减少的时候，说明是置顶的
                        if (serverID > oldTopicId) {
                            oldTopicId =    serverID;
                        }
                        else {
                            //如果是置顶的，纪录在这个表里。
                            [topTopicIDDic setObject:@"1" forKey:[NSString stringWithFormat:@"%d", serverID]];
                        }
                    }

                }
                
                for (int i = 0; i < [storysArr count]; i++) {
                    NSDictionary * storyDic = [storysArr objectAtIndex:i];
                    //
                    int serverID = [[storyDic objectForKey:@"topicid"]intValue];
                    
                    
                    if (serverID >= maxFromId) {
                        maxFromId = serverID;
                    }
                    if (serverID < minFromId) {
                        if ([topTopicIDDic objectForKey:[NSString stringWithFormat:@"%d", serverID]] == NULL) {
                            minFromId = serverID;
                        }
                        
                    }
                    NSInteger timeStamp = [[storyDic objectForKey:@"timestamp"]integerValue];
                   
                    if (timeStamp >=maxTimeStamp) {
                        maxTimeStamp = timeStamp;
                    }
                }
                timestamp = maxTimeStamp;//当前最大的时间戳
                [story_recommend setObject:storys_Arr forKey:@"topic"];

            }
            NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            NSDictionary * params = @{@"uid":userid,@"fromid":@"0",@"direction":@"0",@"count":STORYS_COUNT,@"token":@"110"};
            [HttpTool postWithPath:@"GetRecommendStorys" params:params success:^(id JSON) {
                if ([[JSON objectForKey:@"result"]integerValue] == 1) {
                    //成功获取到招募贴
                    NSArray * array = [JSON objectForKey:@"posterlist"];
                    if (array.count >0 ) {
                        //第一次返回时存储最小id和最大id
                        minRecFromid = [[[array objectAtIndex:0]objectForKey:@"posterid"]intValue];
                        maxRecFromid = [[[array objectAtIndex:0]objectForKey:@"posterid"]intValue];
                        for (int i = 0; i < array.count; i++) {
                            NSDictionary * recommentDic = [array objectAtIndex:i];
                            int serverID = [[recommentDic objectForKey:@"posterid"]intValue];
                            if (serverID >= maxRecFromid) {
                                maxRecFromid = serverID;
                            }
                            if (serverID < minFromId) {
                                minRecFromid = serverID;
                            }
                        }
                        recommends_Arr = [NSMutableArray arrayWithArray:array];
//                        [recommends_Arr addObjectsFromArray:array];
                        [story_recommend setObject:recommends_Arr forKey:@"recommend"];
                        success(story_recommend);
                    }
                    
                }
//                success(story_recommend);

                
            } failure:^(NSError *error) {
                failure((id)error);
                
            }];            
        }
        
    } failure:^(NSError *error) {
        failure((id)error);
    }];
}


#pragma mark新获取数据，direction为0是刷新数据，direction为1是加载数据，获取话题接口

-(void)updateGetStorysDirection:(NSString *)direction success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure//根据时间顺序获取所有的剧
{
    NSString * userid= [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params;
    NSString * time = [NSString stringWithFormat:@"%ld",(long)timestamp];
    
    int         flag    =   0;  //标示是否是获取最新数据。
    if ([time isEqualToString:@"0"]) {
        time = @"1";
    }
    NSString * maxid = [NSString stringWithFormat:@"%d",maxFromId];
    NSString * minid = [NSString stringWithFormat:@"%d",minFromId];

    if ([direction isEqualToString:@"0"])
    {
        //此时是下拉刷新数据，boundaryid为缓存的最小id，fromid为最大storyid
        params = @{@"uid":userid,@"boundaryid":@"0",@"fromid":@"0",@"direction":@"0",@"timestamp":time,@"token":@"110",@"count":STORYS_COUNT};
        flag    =   0;
    }else{
        params = @{@"uid":userid,@"boundaryid":maxid,@"fromid":minid,@"direction":@"1",@"timestamp":time,@"token":@"110",@"count":STORYS_COUNT};
        flag    =   1;
    }
    [HttpTool postWithPath:@"GetTopics" params:params success:^(id JSON) {
        NSMutableArray * storysArr = [[JSON objectForKey:@"topiclist"] mutableCopy];
//        NSArray * updateArr = [JSON objectForKey:@"updatetopiclist"];
        NSArray * deleteArr = [JSON objectForKey:@"deletetopiclist"];
        
        //把置顶的数据删除掉。
        if (flag == 1) {
            //如果是加载更多，需要删除重复数据。
            for (int i = (int)storysArr.count - 1; i >= 0 ; i--) {
                NSDictionary * storyDic = (NSDictionary *)[storysArr objectAtIndex:i];
                NSString     * topicid  = (NSString *) [storyDic objectForKey:@"topicid"];
                if ([topTopicIDDic objectForKey:topicid] != NULL) {
                    [storysArr removeObject:storyDic];
                }
            }
        }
        else {
            //如果不是加载更多，需要重构topicid。
            int     oldTopicId  =   0;
            [topTopicIDDic removeAllObjects];
            [storys_Arr removeAllObjects];
            for (int i = (int)[storysArr count] - 1; i >= 0; i--) {
                NSDictionary * storyDic = [storysArr objectAtIndex:i];
                int serverID = [[storyDic objectForKey:@"topicid"]intValue];
                
                if (i == (int)[storysArr count] - 1) {
                    oldTopicId  =   serverID;
                }
                else {
                    //如果没有置顶，倒叙排列都是id递增，当id减少的时候，说明是置顶的
                    if (serverID > oldTopicId) {
                        oldTopicId =    serverID;
                    }
                    else {
                        //如果是置顶的，纪录在这个表里。
                        [topTopicIDDic setObject:@"1" forKey:[NSString stringWithFormat:@"%d", serverID]];
                    }
                }
                
            }
        }
        
        //从三个数组中获取最新的时间戳
        for (int i = 0; i < storysArr.count; i++) {
            NSDictionary * storyDic = [storysArr objectAtIndex:i];
            NSInteger times = [[storyDic objectForKey:@"timestamp"]integerValue];
            if (times >= timestamp) {
                timestamp = times;
            }
            
            NSString * serverId = (NSString *)[storyDic objectForKey:@"topicid"];
            
            if ([serverId integerValue]>=maxFromId) {
                maxFromId = [serverId intValue];
            }
            if ([serverId integerValue]<= minFromId ) {
                if ([topTopicIDDic objectForKey:serverId] == NULL) {
                    minFromId = [serverId intValue];
                }

            }
        }
        
        //需要遍历本地存储的剧，如果已有的剧被改变了，需要替换掉老数据,如果有被删除的剧，则从本地列表中拿掉
        if (deleteArr.count > 0) {
            //需要构造一个临时数组，不能直接删除原数组的数据
            for (long i = storys_Arr.count - 1; i >= 0; i--) {
                NSDictionary * localStory = [storys_Arr objectAtIndex:i];
                NSString * localStoryid = [localStory objectForKey:@"topicid"];
                //比较两个数组中的剧id
                for (int j = 0; j < deleteArr.count; j++) {
                    NSDictionary * deleteDic = [deleteArr objectAtIndex:j];
                    NSString * deleteid = [deleteDic objectForKey:@"topicid"];
                    NSInteger times = [[deleteDic objectForKey:@"timestamp"]integerValue];
                    if (times>=timestamp) {
                        timestamp = times;
                    }
                    if ([deleteid isEqualToString:localStoryid]) {
                        [storys_Arr removeObjectAtIndex:i];
                    }
                }
            }
        }
        
        //如果有数据被更新
//        if (updateArr.count > 0) {
//            for (int i = 0; i < storys_Arr.count; i++) {
//                NSDictionary * localStory = [storys_Arr objectAtIndex:i];
//                NSString * localStoryid = [localStory objectForKey:@"topicid"];
//                for (int j = 0; j < updateArr.count; j++) {
//                    NSDictionary * deleteDic = [updateArr objectAtIndex:j];
//                    NSString * deleteid = [deleteDic objectForKey:@"topicid"];
//                    NSInteger times = [[deleteDic objectForKey:@"timestamp"]integerValue];
//                    if (times>=timestamp) {
//                        timestamp = times;
//                    }
//                    if ([deleteid isEqualToString:localStoryid]) {
//                        //替换存储的剧
//                        [storys_Arr replaceObjectAtIndex:i withObject:deleteDic];
//                    }
//                }
//                
//            }
//        }
        
        //需要将返回的最新数据插入到前面，已有的数据和更新的数据放在原地
        NSMutableArray * tampArr = [NSMutableArray array];
        if (storysArr.count > 0) {
            if ([direction isEqualToString:@"0"]) {
                //此时是刷新，返回的数据放在上面，否则返回的storyarr放在下面
//                [tampArr addObjectsFromArray:storysArr];
//                if (tampArr.count < 5) {
//                    int count = storys_Arr.count > 5 ?5:(int)storys_Arr.count;
//                    for (int i = 0; i<count; i++) {
//                    [tampArr addObject:[storys_Arr objectAtIndex:i]];
//                    }
//                }
                //            [tampArr addObjectsFromArray:storys_Arr];//之前的数据先不加进来
            }else{
                [tampArr addObjectsFromArray:storys_Arr];
                [tampArr addObjectsFromArray:storysArr];
                
            }
            
            storys_Arr = [tampArr mutableCopy];
            //获取本地的maxFromid和minFromid
            //maxFromId = [[[storys_Arr objectAtIndex:0]objectForKey:@"topicid"]intValue];
            //minFromId = [[[storys_Arr objectAtIndex:0]objectForKey:@"topicid"]intValue];
//            for (int i = 0; i < storys_Arr.count; i++) {
//                NSDictionary * dic = [storys_Arr objectAtIndex:i];
//                NSString * serverId = [dic objectForKey:@"topicid"];
//                
//                if ([serverId integerValue]>=maxFromId) {
//                    maxFromId = [serverId intValue];
//                }
//                if ([serverId integerValue]<= minFromId ) {
//                    minFromId = [serverId intValue];
//                }
//            }
             success(storys_Arr);
                
        }
            
        } failure:^(NSError *error) {
            
            failure((id)error);
        }];
        

}


//#pragma mark第一次获取招募贴
//-(void)firstGetRecommendSuccess:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//第一次获取招募贴
//{
//    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
//    NSDictionary * params = @{@"uid":userid,@"fromid":@"0",@"direction":@"0",@"count":STORYS_COUNT,@"token":@"110"};
//    [HttpTool postWithPath:@"GetRecommendStorys" params:params success:^(id JSON) {
//        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
//            //成功获取到招募贴
//            NSArray * array = [JSON objectForKey:@"posterlist"];
//            if (array.count >0 ) {
//                //第一次返回时存储最小id和最大id
//                minRecFromid = [[array objectAtIndex:0]objectForKey:@"posterid"];
//                maxRecFromid = [[array objectAtIndex:array.count -1]objectForKey:@"posterid"];
//                [recommends_Arr addObjectsFromArray:array];
//                success(array);
//
//            }
//            
//        }
//        
//    } failure:^(NSError *error) {
//
//        
//    }];
//    
//}



#pragma mark  获取广场的招募贴
-(void)getRecommendStorysDirection:(NSString*)direction success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//获取广场的招募贴
{
    
    if ([direction isEqualToString:@"0"]) {
        //此时为下拉刷新
        int from_recommend;
        if (maxRecFromid) {
            from_recommend = maxRecFromid;
        }else{
            from_recommend = 0;
        }
        NSDictionary * params= @{@"uid":USERID,@"fromid":@"0",@"direction":@"0",@"count":STORYS_COUNT,  @"token":@"110"};
        [HttpTool postWithPath:@"GetRecommendStorys" params:params success:^(id JSON) {
            //此时是刷新数据，将返回的数据插入到表格最上方
            if ([[JSON objectForKey:@"result"]integerValue]==1) {

                NSArray * recommends = [JSON objectForKey:@"posterlist"];
                if (recommends.count > 0) {
                    maxRecFromid = [[[recommends objectAtIndex:0]objectForKey:@"posterid"]intValue];
                    minRecFromid = [[[recommends objectAtIndex:0]objectForKey:@"posterid"]intValue];

                    for (int i = 0; i < recommends.count; i++) {
                        NSDictionary * recommentDic = [recommends objectAtIndex:i];
                        int serverID = [[recommentDic objectForKey:@"posterid"]intValue];
                        if (serverID >maxRecFromid) {
                            maxRecFromid = serverID;
                        }
                        if (serverID < minRecFromid) {
                            minRecFromid = serverID;
                        }
                    }
    
                    recommends_Arr = [recommends mutableCopy];
                    success(recommends_Arr);
                }


            }
            
        } failure:^(NSError *error) {
            failure((id)error);

        }];
        
    }else{
        //此时是上拉加载
  
        NSString * min_fromId = [NSString stringWithFormat:@"%d",minRecFromid];
        NSDictionary * params = @{@"uid":USERID,@"fromid":min_fromId,@"direction":@"1",@"count":STORYS_COUNT,@"token":@"110"};
        [HttpTool postWithPath:@"GetRecommendStorys" params:params success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]integerValue]==1) {
                NSArray * arrary = [JSON objectForKey:@"posterlist"];
                //将数据插到最末尾，最后一条招募贴的id为最小id
                if (arrary.count > 0) {
                    for (int i = 0; i < arrary.count; i++) {
                        int serverID = [[[arrary objectAtIndex:i]objectForKey:@"posterid"]intValue];
                        if (serverID > maxRecFromid) {
                            maxRecFromid = serverID;
                        }
                        if (serverID < minRecFromid) {
                            minRecFromid  = serverID;
                        }
                    }
                    [recommends_Arr addObjectsFromArray:arrary];
                    success(recommends_Arr);
                }
            }
        } failure:^(NSError *error) {
            failure((id)error);

        }];
    }
}


#pragma mark 返回topic数组
-(NSMutableArray *)returnTopicArr
{
    return storys_Arr;
}


#pragma mark 返回recommends数组
-(NSMutableArray *)returnRecommendArr
{
    return recommends_Arr;
}
@end
