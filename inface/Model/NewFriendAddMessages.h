//
//  NewFriendAddMessages.h
//  inface
//
//  Created by appleone on 15/6/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewFriendAddMessages : NSObject
{
    NSMutableDictionary * newfriendsDic;
}
+(instancetype)shareInstance;
-(void)saveNewFriendId:(NSString*)friendId;

@end
