//
//  PlaySoundModel.m
//  inface
//
//  Created by appleone on 15/6/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PlaySoundModel.h"

@implementation PlaySoundModel
-(id)initForPlayingVibrate
{
    self = [super init];
    if (self) {
        soundID = kSystemSoundID_Vibrate;
    }
    return self;
}

-(id)initForPlayingSystemSoundEffectWith:(NSString *)resourceName ofType:(NSString *)type
{
    self = [super init];
    if (self) {
        NSString *path = [[NSBundle bundleWithIdentifier:@"com.apple.UIKit"] pathForResource:resourceName ofType:type];
        if (path) {
            SystemSoundID theSoundID;
            OSStatus error =  AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path], &theSoundID);
            if (error == kAudioServicesNoError) {
                soundID = theSoundID;
            }else {
            }
        }
        
    }
    return self;
}

-(id)initForPlayingSoundEffectWith:(NSString *)filename
{
    self = [super init];
    if (self) {
        NSURL *fileURL = [[NSBundle mainBundle] URLForResource:filename withExtension:nil];
        if (fileURL != nil)
        {
            SystemSoundID theSoundID;
            OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)fileURL, &theSoundID);
            if (error == kAudioServicesNoError){
                soundID = theSoundID;
            }else {
            }
        }
    }
    return self;
}

-(void)play
{
//    AudioServicesPlaySystemSound(soundID);
//    SystemSoundID sameViewSoundID;
//
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"clap crowd" ofType:@"caf"];
//    CFURLRef thesoundURL = (__bridge CFURLRef)[NSURL fileURLWithPath:path];
//    AudioServicesCreateSystemSoundID(thesoundURL, &sameViewSoundID);
//    AudioServicesPlaySystemSound(sameViewSoundID); //播放SoundID声音
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MessageVoice" ofType:@"wav"];

    if (path) {
        //注册声音到系统
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path],&shake_sound_male_id);
        AudioServicesPlaySystemSound(shake_sound_male_id);
        //        AudioServicesPlaySystemSound(shake_sound_male_id);//如果无法再下面播放，可以尝试在此播放
    }
    
    AudioServicesPlaySystemSound(shake_sound_male_id);   //播放注册的声音，（此句代码，可以在本类中的任意位置调用，不限于本方法中）
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);   //让手机震动
}

-(void)dealloc
{
    AudioServicesDisposeSystemSoundID(soundID);
}
@end
