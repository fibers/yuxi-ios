//
//  DeleteMyStoryModel.m
//  inface
//
//  Created by appleone on 15/7/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "DeleteMyStoryModel.h"
#import "MyLiveViewController.h"
@implementation DeleteMyStoryModel
+(instancetype)shareInstance
{
    static DeleteMyStoryModel * getGroupRoleName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getGroupRoleName = [[DeleteMyStoryModel alloc] init];
        
    });
    return getGroupRoleName;
}

#pragma mark 删除自戏或者我的剧
-(void)deleteMyStory:(NSString *)storyid deleteType:(NSString *)deleteType success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure//删除自戏或者剧
{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"storyid":storyid,@"token":@"110"};
    //需要判断删除的类型，如果类型是1，删除自戏，0时删除剧，
    if ([deleteType isEqualToString:@"0"]) {
        //删除剧的情况，需要判断剧中的群没有了，或者所有群中的人数只有自己
        [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:storyid success:^(id JSON) {
            
            NSDictionary * storyDic = JSON;
            NSArray * groups = [storyDic objectForKey:@"groups"];
            int totalMembers = 0;
            for (int i = 0; i < groups.count; i++) {
                NSString * members = [[groups objectAtIndex:i]objectForKey:@"members"];
                totalMembers += [members intValue];
            }
            if(groups.count == 0 || totalMembers == groups.count){
               [HttpTool postWithPath:@"DeleteStory" params:params success:^(id JSON) {
                   NSString * describe = [JSON objectForKey:@"describe"];
                   success(describe);
                   //删除剧后 重新获取，刷新UI
                   [[ MyLiveViewController shareInstance]getMyStorys];
                   
               } failure:^(NSError *error) {
                   
                   failure(error);
               }];
            }
        }failure:^(id Json) {
            
            
        }];
    }else{
        //删除的是自戏，直接删除
        [HttpTool postWithPath:@"DeleteStory" params:params success:^(id JSON) {
            NSString * describe = [JSON objectForKey:@"describe"];
            success(describe);

        } failure:^(NSError *error) {
            failure(error);

        }];
    }
    
}


#pragma mark 推荐剧到广场
-(void)recommendStoryUid:(NSString*)uid content:(NSString*)content storyid:(NSString *)storyid success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure
{
    NSDictionary * params = @{@"uid":uid,@"content":content,@"storyid":storyid,@"token":@"110"};
    [HttpTool postWithPath:@"RecommendStory" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            NSString * describe = [JSON objectForKey:@"describe"];
            success(describe);
        }
        
    } failure:^(NSError *error) {
        


    }];
}


@end
