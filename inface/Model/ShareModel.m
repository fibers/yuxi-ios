//
//  ShareModel.m
//  inface
//
//  Created by xigesi on 15/4/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ShareModel.h"
#import <ShareSDK/ShareSDK.h>
@implementation ShareModel
+(instancetype)sharedInstance
{
    static ShareModel * shareModel;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,^{
        shareModel = [[ShareModel alloc]init];
    });
    return shareModel;
    
}

-(void)applyQQFriends:(UIButton *)sender applyDic:(NSDictionary*)applyDic;//邀请qq好友
{
    //applyDic 键值是1，分享类型  2，分享地址  3，标题  4内容
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"未安装手机QQ无法邀请好友" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    NSString * applyTitle = [applyDic objectForKey:@"title"];
    NSString * applyContent = [applyDic objectForKey:@"content"];
    NSString * portrait =  [applyDic objectForKey:@"portrait"];
    
    NSString * applyUrl = [applyDic objectForKey:@"url"];
    
    id<ISSContent> publishContent = [ShareSDK content:applyContent defaultContent:applyTitle image:[ShareSDK imageWithPath:portrait] title:applyTitle url:applyUrl description:applyTitle mediaType:SSPublishContentMediaTypeNews];
    
    NSArray *oneKeyShareList = [ShareSDK getShareListWithType:ShareTypeQQ ,nil];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:nil
                         shareList:oneKeyShareList
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                
                            }];
    
}


-(void)shareToThird:(UIButton*)sender dic:(NSDictionary *)dic
{
    NSString * title = [dic objectForKey:@"title"];
    //分享戏文
    NSString * contentUrl = [dic objectForKey:@"section"];
    NSString * portraitImage = [dic objectForKey:@"storyimg"];
    UIImage * image ;
    if ([portraitImage isEqualToString:@""] || !portraitImage || [portraitImage isEqual:NULL]) {
        
        image = [UIImage imageNamed:@"YXIcon"];
    }else{
        NSURL * url = [NSURL URLWithString:portraitImage];
        NSData * data = [NSData dataWithContentsOfURL:url];
        image = [UIImage imageWithData:data];
    }
    CGSize  imgSize = CGSizeMake(600, 400);
    
    image = [HZSInstances compressImage:image toSize:imgSize withCompressionQuality:0.0];
    NSString * intro = [dic objectForKey:@"intro"];
    NSString * introStr;
    if (intro.length > 50) {
        introStr = [intro substringToIndex:49];
    }else{
        introStr = intro;
    }
    //    NSURL * url = [NSURL URLWithString:contentUrl];
    //我的戏内容
    NSArray *oneKeyShareList = [[NSArray alloc]init];
    
    id<ISSContent> publishContent = [ShareSDK content:introStr defaultContent:title image:[ShareSDK pngImageWithImage:image] title:title url:contentUrl description:introStr mediaType:SSPublishContentMediaTypeNews];
    
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]] &&  ![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]) {
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"未安装手机QQ和微信，无法分享" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }else{
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]] &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]) {
            oneKeyShareList = [ShareSDK getShareListWithType: ShareTypeQQ ,ShareTypeQQSpace,ShareTypeWeixiSession,ShareTypeWeixiTimeline, nil];
        }
        //客户端没有安装QQ
        if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]] &&  [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]) {
            oneKeyShareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession,ShareTypeWeixiTimeline,nil];
            
        }
        //客户端没有安装微信
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]] &&  ![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]) {
            oneKeyShareList = [ShareSDK getShareListWithType:ShareTypeQQ,ShareTypeQQSpace,nil];
            
        }
    }
    
    //创建弹出菜单容器
    
    [ShareSDK showShareActionSheet:nil
                         shareList:oneKeyShareList
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                //可以根据回调提示用户。
                            }];
    
    
}

@end
