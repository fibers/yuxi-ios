//
//  CreatKindsOfModel.h
//  inface
//
//  Created by xigesi on 15/5/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreatKindsOfModel : NSObject

+(instancetype)shareInstance;

-(void)setRoleInStory :(NSString *)userid :(NSString*)groupid :(NSString*)storyid :(NSString*)roleid :(NSString *)nickname :(NSString*)gender  :(NSString*)property :(NSString*)characyer :(NSString*)appearance :(NSString*)other success:(void(^)(id JSON))success failure:(void(^)(NSError *error))failure;//人物设置

-(void)createStoryScreen:(NSString *)userid :(NSString *)groupid :(NSString *)title :(NSString *)content success:(void(^)(id JSON))success failure:(void(^)(NSError *error))failure;//创建主线剧情

-(void)createStoryRole :(NSString *)userid :(NSString *)groupid :(NSString*)title :(NSString*)number :(NSString *)intro success:(void(^)(id JSON))success failure:(void(^)(NSError *error))failure;//创建主线角色

-(void)getStoryRoles:(NSString*)ownerId :(NSString*)strotyid success:(void(^)(id JSON))success failure:(void(^)(NSError *error))failure;//获取主线角色


-(void)addFavoritEmotionUserid:(NSString*)userid title:(NSString*)title content:(NSString*)content ;//添加表情
@end
