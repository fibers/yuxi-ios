//
//  DiscussModel.m
//  inface
//
//  Created by appleone on 15/7/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "DiscussModel.h"
#import "DDAddMemberToGroupAPI.h"
@implementation DiscussModel
@synthesize memberArrDic;
+(instancetype)shareInstance
{
    static DiscussModel * getGroupRoleName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getGroupRoleName = [[DiscussModel alloc] init];
        
    });
    return getGroupRoleName;
}

-(instancetype)init
{
    self = [super init];
    if( self) {
        memberArrDic = [NSMutableDictionary dictionary];
    }
    return self;
}


#pragma mark进入评论列表，需要判断是否已经是群成员了，否则需要先进入群
-(void)readyDiscussGroup:(NSString*)groupId success:(void(^)(id json))success
{
    //发送请求判断
    NSString * userId = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSString * group_id = [NSString stringWithFormat:@"group_%@",groupId];//构造会话id
 
    NSDictionary * params = @{@"uid":userId,@"groupid":groupId,@"token":@"110"};
     __block  SessionEntity * sessionEntity;
    [HttpTool postWithPath:@"GetCommentGroupDetail" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
            
            NSArray * members = [JSON  objectForKey:@"userlist"];
            [memberArrDic setObject:members forKey:groupId];//将群成员当成员字典存储起来

            BOOL isMember = NO;
            for (NSDictionary * dic in members) {
                NSString * member_id = [dic objectForKey:@"userid"];
                if ([userId isEqualToString:member_id]) {
                    //已经在群里
                    isMember = YES;
                }
            }
  
            
            if (isMember == YES) {
                //已经在群里，判断是否需要重新构造会话体，如果是已经发表过评论
               
                [[GetLocalRecentSessions shareInstance]returnSession:groupId success:^(id JSON) {
                    if (JSON) {
                        //已经评论过了，直接根据session拿评论信息
                        sessionEntity = (SessionEntity*)JSON;
                        
                    }else{
                        //如果没有评论过，需要根据群id构造会话体
                        sessionEntity = [[SessionEntity alloc]initWithSessionID:group_id type:SessionTypeSessionTypeGroup];
                    }
                    
                }];
            }else{
                //需要将自己加到群里
                NSString * user_id = [NSString stringWithFormat:@"user_%@",userId];
                DDAddMemberToGroupAPI * addGroup = [[DDAddMemberToGroupAPI alloc]init];
                
                [addGroup requestWithObject:@[group_id,@[user_id]] Completion:^(id response, NSError *error) {
                    //将自己成功添加到评论群,构造会话体，获取消息
                   sessionEntity = [[SessionEntity alloc]initWithSessionID:group_id type:SessionTypeSessionTypeGroup];
                }];
            }
            
            success(sessionEntity);
            
        }
        
        
    } failure:^(NSError *error) {
        
        
    }];
    
}




-(void)getCurrentGroupMembers:(NSString *)groupid success:(void(^)(id json))success//根据群id获取临时群成员列表
{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"groupid":groupid,@"token":@"110"};
    [HttpTool postWithPath:@"GetCommentGroupDetail" params:params success:^(id JSON) {
        int  result = [[JSON objectForKey:@"result"]intValue];
        if (result == 1) {
            //将群成员存储起来
            NSArray * memberArr = [JSON objectForKey:@"userlist"];
            [memberArrDic setObject:memberArr forKey:groupid];
            success(memberArr);
        }
        
    } failure:^(NSError *error) {
        
        
    }];

}

#pragma mark 获取群单条消息对应人员的名字
-(void)returnGroupMembers:(NSString * )groupid userid:(NSString *)userid  success:(void(^)(NSDictionary * personDic))success;//返回成员所在群的名字
{
    NSArray * familyinfo = [memberArrDic objectForKey:groupid];
    BOOL isExistMember = NO;
    for (NSDictionary * dic in familyinfo) {
        if ([[dic allValues]containsObject:userid]) {
            isExistMember = YES;
            success(dic);
        }
    }
   
}
@end
