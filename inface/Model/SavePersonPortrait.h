//
//  SavePersonPortrait.h
//  inface
//
//  Created by appleone on 15/6/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SavePersonPortrait : NSObject
{
    NSMutableDictionary * personDic;//存储好友头像字典
}
+(instancetype)shareInstance;
@property(strong,nonatomic)     NSMutableDictionary * personNickName;

-(void)setSessionid:(NSString*)sessionId portraitImage:(NSString*)portraitIma;
-(void)getPortraitImage:(NSString*)sessionid  success:(void(^)(id JSON))success;

-(void)getPersonNickName:(NSString *)personId success:(void(^)(id JSON))success failure:(void(^)(id  json))failure;
@end
