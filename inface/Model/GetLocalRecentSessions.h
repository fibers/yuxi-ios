//
//  GetLocalRecentSessions.h
//  inface
//
//  Created by xigesi on 15/5/26.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetLocalRecentSessions : NSObject
{
    NSMutableArray * singleSessions;//好友聊天消息
    NSMutableArray * familySessions;//家族聊天信息
    NSMutableDictionary * storysSessions;//剧的聊天消息，键值是剧id，值是该剧中包含群的会话体
    NSMutableArray * groupsInStory;     //一个剧包含的群数，
    NSMutableDictionary * groupsDic;
    NSMutableDictionary *  sessionsDic;//存储所有会话的字典
}
+(instancetype)shareInstance;

-(void)getLocalSessionSuccess:(void(^)(id JSON))success;
-(void)returnSessions:(int)sessionType success:(void(^)(id JSON))success;
-(void)saveSessionEntity:(SessionEntity *)sessionEntity;//根据会话的id存储会话
-(void)returnSession:(NSString * )sessionId success:(void(^)(id  JSON))success;
//计算评论群或者正常消息的未读消息
-(void)totalUnreadMessagesCountSuccess:(void(^)(id json))success;

-(void)totalGroupsUnreadMessageCountSuccess:(void(^)(id  json))success;

@end
