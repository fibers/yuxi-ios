//
//  CurrentGroupModel.h
//  inface
//
//  Created by appleone on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrentGroupModel : NSObject
@property(strong,nonatomic) NSMutableArray * currentArr;
@property(assign,nonatomic) NSInteger  unReadMsgCount;
@property (assign)          NSUInteger timeInterval;
@property(nonatomic,strong) NSString * lastMsgContent;//最后一条消息
+(instancetype)sharedInstance;
@end
