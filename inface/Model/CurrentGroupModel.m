//
//  CurrentGroupModel.m
//  inface
//
//  Created by appleone on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "CurrentGroupModel.h"

@implementation CurrentGroupModel
-(instancetype)init
{
    self = [super init];
    if (self) {
        if (!self.currentArr) {
            self.currentArr  =[NSMutableArray arrayWithCapacity:10];

        }
    }
    return self;
}

+(instancetype)sharedInstance
{
    static CurrentGroupModel * strorySessionModel;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,^{
        strorySessionModel = [[CurrentGroupModel alloc]init];
    });
    return strorySessionModel;
}
@end
