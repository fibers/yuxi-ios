//
//  CreateVariousControls.h
//  inface
//
//  Created by appleone on 15/8/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreateVariousControls : NSObject
+ (UIButton *)createButtonWithFrame:(CGRect)frame BtnColor:(UIColor*)color ButtonText:(NSString*)text Btnrow:(CGFloat)cornerRadius BtnClick:(void(^)(void))block titleFont:(float)font  BtnClick:(SEL)clickname addTaget:(id)taget;
+ (UISwitch *)createSwitchwWithFrame:(CGRect)frame Taget:(id)taget ClickName:(SEL)clickname;
+(UIView*)creatViewWithFrame:(CGRect)frame Viewcolor:(UIColor*)color;
+(UIImageView*)createImageWithImageNamed:(NSString *)name Frame:(CGRect)frame;
+(UILabel*)creatSecondLabelwith:(NSString*)text Frame:(CGRect)frame Color:(UIColor*)color Coruidis:(CGFloat)cornerRadius;
+(UILabel*)creatLabelWithFrame:(CGRect)frame LabelText:(NSString*)text;

@end
