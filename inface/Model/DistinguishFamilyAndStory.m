//
//  DistinguishFamilyAndStory.m
//  inface
//
//  Created by xigesi on 15/5/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "DistinguishFamilyAndStory.h"

@implementation DistinguishFamilyAndStory
+(instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    static DistinguishFamilyAndStory *g_createTheatreViewController = nil;
    dispatch_once(&onceToken, ^{
        g_createTheatreViewController = [DistinguishFamilyAndStory new];
    });
    return g_createTheatreViewController;

}


#pragma mark 区分家族和剧的群
-(void)ditinguish:(NSString *)groupId :(NSString *)subsidiaryId :(RandofGroup)groupType;//将不同的群区分
{
    NSArray * typeId =@[@(groupType),subsidiaryId];
    [groupsDic setObject:typeId forKey:groupId];
}

#pragma mark 根据群id获取类型和家族或者剧的id
-(void)getGroupType:(NSString*)groupID success:(void(^)(NSArray * arrary))success;//根据群名返回对应的类型和家族或者剧的id，
{
    NSArray * arrary = [groupsDic objectForKey:groupID];
    if (arrary) {
        success(arrary);
    }
}
@end
