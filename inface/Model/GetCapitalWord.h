//
//  GetCapitalWord.h
//  inface
//
//  Created by appleone on 15/6/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetCapitalWord : NSObject
+(instancetype) shareInstance;
-(void)getFirstLetter:(NSString*)name success:(void(^)(id json))success;
@end
