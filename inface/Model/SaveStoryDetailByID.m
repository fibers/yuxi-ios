//
//  SaveStoryDetailByID.m
//  inface
//
//  Created by appleone on 15/6/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SaveStoryDetailByID.h"

@implementation SaveStoryDetailByID
+(instancetype)shareInstance
{
    static SaveStoryDetailByID * getGroupRoleName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getGroupRoleName = [[SaveStoryDetailByID alloc] init];
        
    });
    return getGroupRoleName;
}


-(instancetype)init
{
    self =[super init];
    if (self) {
        storysDetail = [NSMutableDictionary dictionary];
    }
    return self;
}



-(void)addStory:(NSString*)storyId nsarr:(NSDictionary*)detail
{
    [storysDetail setObject:detail forKey:storyId];
    
}


-(NSDictionary *)getstorryDetail:(NSString *)storyid
{
    return [storysDetail objectForKey:storyid];
    
}
@end
