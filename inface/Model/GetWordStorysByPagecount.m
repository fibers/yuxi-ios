//
//  GetWordStorysByPagecount.m
//  inface
//
//  Created by appleone on 15/7/8.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GetWordStorysByPagecount.h"
//默认返回15条数据
#define STORYS_COUNT    @"15"
@implementation GetWordStorysByPagecount
+(instancetype)shareInstance
{
    static GetWordStorysByPagecount * getGroupRoleName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getGroupRoleName = [[GetWordStorysByPagecount alloc] init];
        
    });
    return getGroupRoleName;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        storys_Arr = [NSMutableArray array];
        favorStorys = [NSMutableArray array];
        serverFavorStorys  = [NSMutableArray array];
        serverNewestStorys = [NSMutableArray array];
    }
    return self;
}

#pragma 第一次获取所有的剧

-(void)firstGetStorysSuccess:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//第一次取数据，不需要上传所有参数
{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSMutableDictionary * currentArr = [[NSMutableDictionary alloc]init];
    maxFromId = 0;
    minFromId = 0;
    NSDictionary * params = @{@"uid":userid,@"count":STORYS_COUNT ,@"token":@"110",@"boundaryid":@"0",@"direction":@"0",@"fromid":@"0"};
    [HttpTool postWithPath:@"GetStorys" params:params success:^(id JSON) {
        //最后一条数据的id最大，最上一条数据id最小
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
            NSArray * storysArr = [JSON objectForKey:@"storys"];
            if (storysArr.count > 0) {
                NSInteger maxTimeStamp = 0;
                maxFromId = [[[storysArr objectAtIndex:0]objectForKey:@"storyid"]intValue];
                minFromId = [[[storysArr objectAtIndex:0]objectForKey:@"storyid"]intValue];

                for (int i = 0; i < storysArr.count; i++) {
                    NSDictionary * storyDic = [storysArr objectAtIndex:i];
                    //
                    NSInteger timeStamp = [[storyDic objectForKey:@"createtime"]integerValue];
                    if (timeStamp >=maxTimeStamp) {
                        maxTimeStamp = timeStamp;
                    }
                    int currentId = [[storyDic objectForKey:@"storyid"]intValue];
                    if (maxFromId <= currentId) {
                        maxFromId = currentId;
                    }
                    if (minFromId >currentId) {
                        minFromId = currentId;
                    }
                }
                [storys_Arr addObjectsFromArray:storysArr];


                [currentArr setObject:storys_Arr forKey:@"time"];

                timestamp = maxTimeStamp;//当前最大的时间戳
            }
            //同时获取热门的剧
            NSDictionary * params = @{@"uid":USERID,@"index":@"0",@"count":STORYS_COUNT,@"token":@"110"};
            [HttpTool postWithPath:@"GetStoryByFavor" params:params success:^(id JSON) {
                if ([[JSON objectForKey:@"result"]integerValue]==1) {
                    //第一次获取最热的剧
                    NSArray * arrary = [JSON objectForKey:@"storys"];
                    if ( arrary.count > 0) {
                        [favorStorys addObjectsFromArray:[JSON objectForKey:@"storys"]];
                        [currentArr setObject:favorStorys forKey:@"favor"];

                    }
                }
                success(currentArr);

                
            } failure:^(NSError *error) {
                failure(error);
                
            }];
        }
        
    } failure:^(NSError *error) {
        
        failure(error);
    }];
}


#pragma mark新获取数据，direction为0是刷新数据，direction为1是加载数据,获取最新的数据

-(void)updateGetStorysDirection:(NSString *)direction success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure//根据时间顺序获取所有的剧
{
    NSString * userid= [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params;
    NSString * time = [NSString stringWithFormat:@"%ld",(long)timestamp];
    int  maxid;
    int  minid;
    if (maxFromId >0) {
        maxid = maxFromId;
    }else{
        maxid =0;
    }
    if (minFromId >0) {
        minid = minFromId;
    }else{
        minid =0;
    }
    NSString * maxFromID = [NSString stringWithFormat:@"%d",maxid];
    NSString * minFromID = [NSString stringWithFormat:@"%d",minid];

    if ([direction isEqualToString:@"0"])
    {
        //此时是下拉刷新数据，boundaryid为缓存的最小id，fromid为最大storyid
        params = @{@"uid":userid,@"boundaryid":@"0",@"fromid":@"0",@"direction":@"0",@"token":@"110",@"count":STORYS_COUNT};
    }else{
        params = @{@"uid":userid,@"boundaryid":maxFromID,@"fromid":minFromID,@"direction":@"1",@"timestamp":time,@"token":@"110",@"count":STORYS_COUNT};
    }
        [HttpTool postWithPath:@"GetStorys" params:params success:^(id JSON) {
            NSArray * storysArr = [JSON objectForKey:@"storys"];
//            NSArray * updateArr = [JSON objectForKey:@"updatestorys"];
            NSArray * deleteArr = [JSON objectForKey:@"deletestorys"];
            //从三个数组中获取最新的时间戳
            for (int i = 0; i < storysArr.count; i++) {
                NSDictionary * storyDic = [storysArr objectAtIndex:i];
                NSInteger times = [[storyDic objectForKey:@"createtime"]integerValue];
                if (times >= timestamp) {
                    timestamp = times;
                }
            }
            
            //需要遍历本地存储的剧，如果已有的剧被改变了，需要替换掉老数据,如果有被删除的剧，则从本地列表中拿掉
            if (deleteArr.count > 0) {
                //需要构造一个临时数组，不能直接删除原数组的数据
                for (long i = storys_Arr.count - 1; i >= 0; i--) {
                    NSDictionary * localStory = [storys_Arr objectAtIndex:i];
                    NSString * localStoryid = [localStory objectForKey:@"storyid"];
                    //比较两个数组中的剧id
                    for (int j = 0; j < deleteArr.count; j++) {
                        NSDictionary * deleteDic = [deleteArr objectAtIndex:j];
                        NSString * deleteid = [deleteDic objectForKey:@"storyid"];
                        NSInteger times = [[deleteDic objectForKey:@"updatetime"]integerValue];
                        if (times>=timestamp) {
                            timestamp = times;
                        }
                        if ([deleteid isEqualToString:localStoryid]) {
                            [storys_Arr removeObjectAtIndex:i];
                        }
                    }
                }
            }
            
//            //如果有数据被更新
//            if (updateArr.count > 0) {
//                for (int i = 0; i < storys_Arr.count; i++) {
//                    NSDictionary * localStory = [storys_Arr objectAtIndex:i];
//                    NSString * localStoryid = [localStory objectForKey:@"storyid"];
//                    for (int j = 0; j < updateArr.count; j++) {
//                        NSDictionary * deleteDic = [deleteArr objectAtIndex:j];
//                        NSString * deleteid = [deleteDic objectForKey:@"storyid"];
//                        NSInteger times = [[deleteDic objectForKey:@"updatetime"]integerValue];
//                        if (times>=timestamp) {
//                            timestamp = times;
//                        }
//                        if ([deleteid isEqualToString:localStoryid]) {
//                            //替换存储的剧
//                            [storys_Arr replaceObjectAtIndex:i withObject:deleteDic];
//                        }
//                    }
//
//                }
//            }

            //需要将返回的最新数据插入到前面，已有的数据和更新的数据放在原地
            if (storysArr.count > 0)
        {
            
            NSMutableArray * tampArr = [NSMutableArray array];
            if ([direction isEqualToString:@"0"]) {
                //此时是刷新，返回的数据放在上面，否则返回的storyarr放在下面
                [tampArr addObjectsFromArray:storysArr];
            }else{
                [tampArr addObjectsFromArray:storys_Arr];
                [tampArr addObjectsFromArray:storysArr];

            }

            storys_Arr = [tampArr mutableCopy];
            //获取本地的maxFromid和minFromid
            if (maxFromId == 0) {
                maxFromId = [[[storys_Arr objectAtIndex:0]objectForKey:@"storyid"]intValue];
            }
            if (minFromId== 0) {
                minFromId = [[[storys_Arr objectAtIndex:0]objectForKey:@"storyid"]intValue];

            }
           
            for (int i = 0; i < storys_Arr.count; i++) {
                NSDictionary * dic = [storys_Arr objectAtIndex:i];
                NSString * serverId = [dic objectForKey:@"storyid"];

                if ([serverId integerValue]>=maxFromId) {
                    maxFromId = [serverId intValue];
                }
                if ([serverId integerValue]<= minFromId ) {
                    minFromId = [serverId intValue];
                }
            }
          
                success(storys_Arr);
        }
            
        } failure:^(NSError *error) {
            failure(error);
            
        }];
    
}


#pragma mark 下拉刷新或者上啦加载获取剧，根据热度获取剧
-(void)getFavorStorysByDirection:(NSString *)direction success:(void(^)(id JSON))success failure:(void(^)(id JSON))failure;//获取最热的数据
{
    NSString * userid= [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params;
    if ([direction isEqualToString:@"0"]) {
        //此时是刷新获取最新的，index传0
        params = @{@"uid":USERID,@"index":@"0",@"count":STORYS_COUNT,@"token":@"110"};
 
    }else{
        NSString * count = [NSString stringWithFormat:@"%ld",(unsigned long)favorStorys.count ] ;
        params = @{@"uid":userid,@"index":count,@"count":STORYS_COUNT,@"token":@"110"};
    }
    [HttpTool postWithPath:@"GetStoryByFavor" params:params success:^(id JSON) {
        if ([direction isEqualToString:@"0"]) {
            NSArray * serverArr = [JSON objectForKey:@"storys"];
            if (serverArr.count > 0) {
                //此时是刷新获取最新的，index传0
                [favorStorys removeAllObjects];
                [favorStorys addObjectsFromArray:[JSON objectForKey:@"storys"]];
            }

        }else{
            //此时是加载更多，有updateStorys和deleteStorys
            NSMutableArray * deleteArr = [JSON objectForKey:@"deletestorys"];
            NSMutableArray * serverArr = [JSON objectForKey:@"storys"];

            if (deleteArr.count > 0) {
                //有删除的数据，将本地相应的剧删掉
                for (int i = (int)favorStorys.count - 1  ; i >= 0 ; i-- ) {
                    NSDictionary * localStory = [favorStorys objectAtIndex:i];
                    NSString * localStoryid = [localStory objectForKey:@"storyid"];
                    
                    for (int j =0 ; j < deleteArr.count; j++) {
                        NSDictionary * deleteDic = [deleteArr objectAtIndex:j];
                        NSString * deleteStoryid = [deleteDic objectForKey:@"storyid"];
                        if ([deleteStoryid isEqualToString:localStoryid]) {
                            //本地的剧被删除了
                            [favorStorys removeObjectAtIndex:i];
                        }
                    }
                }
            }
            //将新获取的数据添加到已有的数据中
            [favorStorys addObjectsFromArray:serverArr];
        }
        success(favorStorys);
        
    } failure:^(NSError *error) {
        
        failure(error);
    }];
    
}


#pragma mark 获取最新的热门或者最新的剧
-(void)getCurrentNewestStoryByType:(NSString *)type success:(void (^)(id))success failure:(void (^)(id))failure
{
    NSDictionary * params ;
    if ([type isEqualToString:@"0"]) {
        params = @{@"uid":USERID,@"index":@"0",@"count":@"0",@"type":type,@"token":@"110"};
    }else{

        params = @{@"uid":USERID,@"index":@"0",@"count":@"0",@"type":type,@"token":@"110"};
    }
    
    [HttpTool postWithPath:@"GetStorysByType" params:params success:^(id JSON) {
        NSArray * serverStorys = [JSON objectForKey:@"storys"];
        if (serverStorys.count > 0) {
            if ([type isEqualToString:@"0"]) {
                //获取的是最新的剧
                serverNewestStorys = [serverStorys mutableCopy];
                success(serverNewestStorys);
            }else{
                serverFavorStorys = [serverStorys mutableCopy];
                success(serverFavorStorys);
            }
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}


#pragma mark 加载热门或者最新的剧
-(void)getHistoryStorysByType:(NSString *)type success:(void (^)(id))success failure:(void (^)(id))failure
{
    NSDictionary * params ;
    if ([type isEqualToString:@"0"]) {
        NSString * newestStorysCount =  [NSString stringWithFormat:@"%ld",serverNewestStorys.count];
        params = @{@"uid":USERID,@"index":newestStorysCount,@"count":STORYS_COUNT,@"type":type,@"token":@"110"};
    }else{
        NSString * favorStorysCount =  [NSString stringWithFormat:@"%ld",serverFavorStorys.count];

        params = @{@"uid":USERID,@"index":favorStorysCount,@"count":STORYS_COUNT,@"type":type,@"token":@"110"};
    }
    
    [HttpTool postWithPath:@"GetStorysByType" params:params success:^(id JSON) {
        NSArray * serverStorys = [JSON objectForKey:@"storys"];
        if (serverStorys.count > 0) {
            if ([type isEqualToString:@"0"]) {
                //获取的是最新的剧,需要做去重处理
                NSMutableDictionary * idsDic = [NSMutableDictionary new];
                for (int i = 0; i < serverNewestStorys.count; i++) {
                    [idsDic setObject:serverNewestStorys[i] forKey:[serverNewestStorys[i]objectForKey:@"storyid"]];
                }
                for (int i = 0; i < serverStorys.count; i++) {
                    NSString * storyid = [serverStorys[i]objectForKey:@"storyid"];
                    if (![[idsDic allKeys]containsObject:storyid]) {
                        [serverNewestStorys addObject:serverStorys[i]];

                    }
                }
                success(serverNewestStorys);
            }else{
                NSMutableDictionary * idsDic = [NSMutableDictionary new];
                for (int i = 0; i < serverFavorStorys.count; i++) {
                    [idsDic setObject:serverFavorStorys[i] forKey:[serverFavorStorys[i]objectForKey:@"storyid"]];
                }
                for (int i = 0; i < serverStorys.count; i++) {
                    NSString * storyid = [serverStorys[i]objectForKey:@"storyid"];
                    if (![[idsDic allKeys]containsObject:storyid]) {
                        [serverFavorStorys addObject:serverStorys[i]];
                        
                    }
            }
                success(serverFavorStorys);

        }
        
    }
        
    }failure:^(NSError *error) {
        
        
    }];
}


#pragma maek 第一次获取最新和热门的剧
-(void)getFavorAndNewestStorySucces:(void (^)(id JSON))success failure:(void (^)(id JSON))failure
{
    NSDictionary * params = @{@"uid":USERID,@"index":@"0",@"count":@"0",@"type":@"1",@"token":@"110"};
    [HttpTool postWithPath:@"GetStorysByType" params:params success:^(id JSON) {
        NSArray * favor_storys = [JSON objectForKey:@"storys"];
        if (favor_storys.count >0) {
            serverFavorStorys = [favor_storys mutableCopy];
        }
        
        NSDictionary * params = @{@"uid":USERID,@"index":@"0",@"count":@"0",@"type":@"0",@"token":@"110"};
       
         [HttpTool postWithPath:@"GetStorysByType" params:params success:^(id JSON) {
             NSArray * newset_storys = [JSON objectForKey:@"storys"];
             if (newset_storys.count >0) {
                 serverNewestStorys = [newset_storys mutableCopy];
             }
             NSMutableArray * favorAndNewest = [NSMutableArray array];
             [favorAndNewest addObject:serverFavorStorys];
             [favorAndNewest addObject:serverNewestStorys];
             success(favorAndNewest);
            
        } failure:^(NSError *error) {
            
            
        }];
        
    } failure:^(NSError *error) {
        
        
    }];
}


#pragma mark 返回热门数据

-(NSMutableArray *)returnFavorARRL
{
    return serverFavorStorys;
}

#pragma mark 返回最新数据
-(NSMutableArray *)returnStorysArr
{
    return serverNewestStorys;
}


#pragma mark 分页获取话题
-(void)pageGetTopicsDirection:(NSString *)direction success:(void(^)(id JSON))success 
{
    
}


@end
