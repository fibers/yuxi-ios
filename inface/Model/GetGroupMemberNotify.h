//
//  GetGroupMemberNotify.h
//  inface
//
//  Created by xigesi on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetGroupMemberNotify : NSObject<UIAlertViewDelegate>
{
    NSString * _groupId;
    NSMutableString * _sessionId;
    NSString * confirmValue;
}
+(instancetype)shareInsatance;

@end
