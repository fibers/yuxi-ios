//
//  GetAllFamilysAndStorys.m
//  inface
//
//  Created by xigesi on 15/5/22.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GetAllFamilysAndStorys.h"
#import "BaseTabBarViewController.h"
#import "GetRecentSession.h"
#import "SaveStoryDetailByID.h"
@implementation GetAllFamilysAndStorys
+(instancetype)shareInstance
{
    static GetAllFamilysAndStorys* g_getUnreadNotify;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_getUnreadNotify = [[GetAllFamilysAndStorys alloc] init];
        
        
    });
    return g_getUnreadNotify;
}


-(instancetype)init
{
    familysArr = [NSMutableArray array];
    strorysArr = [NSMutableArray array];
    groupsDic = [NSMutableDictionary new];

    strorysGroup = [NSMutableDictionary new];

    _groupsInfoDic = [NSMutableDictionary new];
    _theaterDic = [NSMutableDictionary new];
    _currentGroup = [NSMutableDictionary new];

    return self;
}

-(void)getAllFamilysAndStorysSuccess:(void (^)(id json))success  failure:(void(^)(id failure))failure
{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    familysArr = [NSMutableArray array];
    strorysArr = [NSMutableArray array];
    //获取我的家族
    [HttpTool postWithPath:@"GetMyFamilyList" params:@{@"uid":userid,@"token":@"110"} success:^(id JSON) {
        familysArr = [[JSON objectForKey:@"familylist"]mutableCopy];
        NSArray * arrary = [JSON objectForKey:@"familylist"];
        if ([familysArr count] > 0) {
            for (int i = 0; i < [arrary  count]; i++) {
                //循环遍历数组，将family家族存入值为1
                NSString * familyid = [[arrary objectAtIndex:i]objectForKey:@"id"];
                [groupsDic setObject:@"1" forKey:familyid];
                
                [_groupsInfoDic setObject:[arrary objectAtIndex:i] forKey:familyid];
            }
        }
        //获取我的剧
        [HttpTool postWithPath:@"GetMyStory" params:@{@"uid":userid,@"token":@"110"} success:^(id JSON) {
            
          NSArray * currentArr = [JSON objectForKey:@"storys"];
            for (int i = 0; i < currentArr.count; i++) {
                NSArray * groups = [[currentArr objectAtIndex:i]objectForKey:@"groups"];
                NSString * storyimg = [[currentArr objectAtIndex:i]objectForKey:@"portrait"];
                if (groups.count == 1) {
                    NSDictionary * groupDic = [groups objectAtIndex:0];
                    NSString * groupid = [groupDic objectForKey:@"groupid"];
                    if (![[groupDic objectForKey:@"isplay"] isEqualToString:@"3"])
                    {
                        [strorysArr addObject:[currentArr objectAtIndex:i]];
                    }else{
                        //将评论过剧的信息保存到评论群里面
                        NSMutableDictionary * storyDic =  [NSMutableDictionary dictionaryWithDictionary:[currentArr objectAtIndex:i]   ];
                        [storyDic setObject:@"2" forKey:@"type"];
                        [storyDic setObject:storyimg forKey:@"storyimg"];
                        [_currentGroup setObject:storyDic forKey:groupid];
                    }
                }
                if (groups.count > 1) {
                    [strorysArr addObject:[currentArr objectAtIndex:i]];

                }
            }
            
            
            if ([strorysArr count]> 0) {
                for (int i = 0; i < [strorysArr count]; i++) {
                    
                    NSString * storyid = [[strorysArr objectAtIndex:i]objectForKey:@"id"];
                    
#pragma mark 此处是storysArr还是currentArr
                    NSString * storyimg = [[strorysArr objectAtIndex:i]objectForKey:@"portrait"];

                    [groupsDic setObject:@"2" forKey:storyid];
                    NSArray * groups = [[strorysArr objectAtIndex:i]objectForKey:@"groups"];

                    for (int j = 0; j < [groups count]; j++) {
                        NSDictionary * group = [groups objectAtIndex:j];
                        NSString * isplay = [group objectForKey:@"isplay"];

                        NSString * group_id = [ group objectForKey:@"groupid"];
                        //如果是
                        if (![isplay isEqualToString:@"3"]) {
                        [groupsDic setObject:@"2" forKey:group_id];//剧群类型都是2
                        [strorysGroup setObject:storyid forKey:group_id];//根据群id存储剧id
                        [_groupsInfoDic setObject:[strorysArr objectAtIndex:i] forKey:group_id];//根据群名存储剧的详情
                        [_theaterDic setObject:[groups objectAtIndex:j] forKey:group_id];//根据群名存储群信息
                        }else{
                            //将剧的详情保存在groupid中
                            NSMutableDictionary * storyDic =  [NSMutableDictionary dictionaryWithDictionary:[strorysArr objectAtIndex:i]];
                            [storyDic setObject:@"2" forKey:@"type"];
                            [storyDic setObject:storyimg forKey:@"storyimg"];
                            [_currentGroup setObject:storyDic forKey:group_id];
                        }
                    }
                    [_groupsInfoDic setObject:[strorysArr objectAtIndex:i] forKey:storyid];//根据剧id存储剧的信息
                    
                    [[SaveStoryDetailByID shareInstance]addStory:storyid nsarr:[strorysArr objectAtIndex:i]];
                }
            
                
            }
            
            //不在这个地方获取Session，直接返回BaseTabBarController
          [[GetLocalRecentSessions shareInstance]getLocalSessionSuccess:^(id JSON) {
                BaseTabBarViewController * baseViewController = [[BaseTabBarViewController alloc]init];
                success(baseViewController);
          }];

            
        } failure:^(NSError *error) {
            BaseTabBarViewController * baseViewController = [[BaseTabBarViewController alloc]init];

            failure(baseViewController);
            
        }];

        
    } failure:^(NSError *error) {
        BaseTabBarViewController * baseViewController = [[BaseTabBarViewController alloc]init];

        failure(baseViewController);
    }];
    
}

#pragma mark 保存被管理员同意的群id
-(void)preSaveSessionId:(NSString *)groupID
{
    
}

#pragma mark 获取剧详情字典
-(NSDictionary * )getStoryDetail:(NSString*)storyid;//根据剧id获取剧详情
{
    NSDictionary * storyDic = [_groupsInfoDic objectForKey:storyid];
    
    return storyDic;
}

-(void)GetFavoriteEmotionList{
    
    //获取表情列表接口(Post)
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary *params = @{@"uid":userid,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetFavoriteEmotionList" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            NSMutableArray *emotionArr=[NSMutableArray new];
            NSDictionary *adic1=[NSDictionary dictionaryWithObjectsAndKeys:@"[",@"title",@"[",@"content", nil];
            [emotionArr addObject:adic1];
            
            NSDictionary *adic2=[NSDictionary dictionaryWithObjectsAndKeys:@"]",@"title",@"]",@"content", nil];
            [emotionArr addObject:adic2];

            NSDictionary *adic3=[NSDictionary dictionaryWithObjectsAndKeys:@"【",@"title",@"【",@"content", nil];
            [emotionArr addObject:adic3];
            
            NSDictionary *adic4=[NSDictionary dictionaryWithObjectsAndKeys:@"】",@"title",@"】",@"content", nil];
            [emotionArr addObject:adic4];
            
            NSDictionary *adic5=[NSDictionary dictionaryWithObjectsAndKeys:@"“",@"title",@"“",@"content", nil];
            [emotionArr addObject:adic5];
            
            NSDictionary *adic6=[NSDictionary dictionaryWithObjectsAndKeys:@"”",@"title",@"”",@"content", nil];
            [emotionArr addObject:adic6];
            
            NSMutableArray *aArr =[JSON objectForKey:@"emotions"];
            [emotionArr addObjectsFromArray:aArr];
            
            NSDictionary *adic=[NSDictionary dictionaryWithObjectsAndKeys:@"+",@"title",@"+",@"content", nil];
            [emotionArr addObject:adic];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            
            NSString *fullpath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"GetFavoriteEmotionList.plist"];
            [emotionArr writeToFile:fullpath atomically:YES];
            
            
        }else{
//            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//            [alertview show];
//            
        }
        
    } failure:^(NSError *error) {
    }];
    
}





#pragma mark 重新更新存储的
-(void)updateFamilysOrSto:(NSMutableArray *)groupsArr typestr:(NSString*)grouptype;//根据类型更新存储的数组
{
    if ([grouptype isEqualToString:@"1"]) {
        familysArr = [groupsArr mutableCopy];
        if ([familysArr count] > 0) {
            for (int i = 0; i < [familysArr  count]; i++) {
                //循环遍历数组，将family家族存入值为1
                NSString * familyid = [[familysArr objectAtIndex:i]objectForKey:@"id"];
                [groupsDic setObject:@"1" forKey:familyid];
                //根据家族的id存储家族的信息
                [_groupsInfoDic setObject:[familysArr objectAtIndex:i] forKey:familyid];
                
            }
        }
    }else{
        if (groupsArr.count >0) {
            [strorysArr removeAllObjects];//先移除之前的数据
            for (int i = 0; i < groupsArr.count; i++) {
                NSArray * groups = [[groupsArr objectAtIndex:i]objectForKey:@"groups"];
                NSString * storyimg = [[groupsArr objectAtIndex:i]objectForKey:@"portrait"];
                if (groups.count == 1) {
                    NSDictionary * groupDic = [groups objectAtIndex:0];
                    NSString * groupid = [groupDic objectForKey:@"groupid"];
                    if (![[groupDic objectForKey:@"isplay"] isEqualToString:@"3"]) {
                        [strorysArr addObject:[groupsArr objectAtIndex:i]];
                    }else{
                        NSMutableDictionary * storyDic =  [NSMutableDictionary dictionaryWithDictionary:[groupsArr objectAtIndex:i] ];
                        [storyDic setObject:@"2" forKey:@"type"];
                        [storyDic setObject:storyimg forKey:@"storyimg"];
                        [_currentGroup setObject:storyDic forKey:groupid];
                    }
                }
                if (groups.count > 1) {
                    [strorysArr addObject:[groupsArr objectAtIndex:i]];
                    
                }

            }
        }
        if ([strorysArr count]> 0) {
            for (int i = 0; i < [strorysArr count]; i++) {
                
                NSString * storyid = [[strorysArr objectAtIndex:i]objectForKey:@"id"];
                [groupsDic setObject:@"2" forKey:storyid];              // 将剧id存为2
                NSArray * groups = [[strorysArr objectAtIndex:i]objectForKey:@"groups"];
                NSString * storyimg = [[groupsArr objectAtIndex:i]objectForKey:@"portrait"];

                for (int j = 0; j < [groups count]; j++) {
                    NSDictionary * group = [groups objectAtIndex:j];
                    NSString * isplay = [group objectForKey:@"isplay"];
                    
                    NSString * group_id = [ group objectForKey:@"groupid"];
                    //如果是
                    if (![isplay isEqualToString:@"3"]) {
                        [groupsDic setObject:@"2" forKey:group_id];//剧群类型都是2
                        [strorysGroup setObject:storyid forKey:group_id];//根据群id存储剧id
                        [_groupsInfoDic setObject:[strorysArr objectAtIndex:i] forKey:group_id];//根据群名存储剧的详情
                        [_theaterDic setObject:[groups objectAtIndex:j] forKey:group_id];//根据群名存储群信息
                    }else{
                        NSMutableDictionary * storyDic =  [NSMutableDictionary dictionaryWithDictionary:[strorysArr objectAtIndex:i]   ];
                        [storyDic setObject:@"2" forKey:@"type"];
                        [storyDic setObject:storyimg forKey:@"storyimg"];
                        [_currentGroup setObject:storyDic forKey:group_id];
                    }
                }
                [_groupsInfoDic setObject:[strorysArr objectAtIndex:i] forKey:storyid];
                [[SaveStoryDetailByID shareInstance]addStory:storyid nsarr:[strorysArr objectAtIndex:i]];//存储剧的详情
 
            } 
            
        }
    }
}


#pragma mark 根据群id获取剧的id
-(NSString * )getStoryidByGroupid:(NSString*)groupid;//根据群名获取剧ID
{
    return [strorysGroup objectForKey:groupid];
}

#pragma mark 根据剧群id获取剧群的详情，主要是获取群名
-(NSDictionary *)getStoryGroup:(NSString*)groupid//根据剧群id获取剧群详情
{
    return [_theaterDic objectForKey:groupid];
}



#pragma mark 根据群id判断是家族还是剧
-(void)getGroupTypeStringgroupid:(NSString *)groupid success:(void (^)(id JSON))success
{
    NSString * type = [groupsDic objectForKey:groupid];
    if (type) {
        success(type);
    }
}


#pragma mark 根据群id判断是家族还是剧获得所有信息
-(void)getGroupInfoStringgroupid:(NSString *)groupid success:(void (^)(id JSON))success failure:(void (^)(id))failure
{
    NSDictionary * groupsInfo = [_groupsInfoDic objectForKey:groupid];
    if (groupsInfo) {
        
        success(groupsInfo);
    }else{
        NSString * nill = @"failure";
        failure(nill);
    }
}

#pragma mark 删除相应的群
-(void)deleteGroups:(NSString * )groupid grouptype:(NSString*)grouptype;
{
    for (int i = 0; i <[familysArr count]; i++) {
        NSString * str = [[familysArr objectAtIndex:i]objectForKey:@"id"];
        if ([groupid isEqualToString:str]) {
            [familysArr removeObjectAtIndex:i];
            [groupsDic removeObjectForKey:groupid];
        }
    }
    
}


#pragma mark  根据临时群的id获取剧的信息


-(NSMutableArray *)type:(NSString *)type
{
    if ([type isEqualToString:@"1"]) {
        return familysArr;
        
    }else{
        return strorysArr;
    }
}


-(NSDictionary *)getCurrentGroupDetail:(NSString *)groupid
{
    return [_currentGroup objectForKey:groupid];
}

@end
