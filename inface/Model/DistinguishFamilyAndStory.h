//
//  DistinguishFamilyAndStory.h
//  inface
//
//  Created by xigesi on 15/5/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    RandofGroupFamily,
    RandofGroupStory
}RandofGroup;
@interface DistinguishFamilyAndStory : NSObject
{
    NSMutableDictionary * groupsDic;//键值是群id，存放类型
}
+(instancetype)shareInstance;

-(void)ditinguish:(NSString *)groupId :(NSString *)subsidiaryId :(RandofGroup)groupType;//将不同的群区分

-(void)getGroupType:(NSString*)groupID success:(void(^)(NSArray * arrary))success;//根据群名返回对应的类型和家族或者剧的id，
@end
