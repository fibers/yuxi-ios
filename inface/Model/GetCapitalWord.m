//
//  GetCapitalWord.m
//  inface
//
//  Created by appleone on 15/6/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GetCapitalWord.h"
#import "NSString+FirstLetter.h"
@implementation GetCapitalWord
+(instancetype)shareInstance
{
    static GetCapitalWord * addGroupMemberModule;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,^{
        addGroupMemberModule = [[GetCapitalWord alloc]init];
    });
    return addGroupMemberModule;
}


-(void)getFirstLetter:(NSString*)name success:(void(^)(id json))success;
{
   NSString * firstLetter = [name firstLetter];
    success(firstLetter);
}
@end
