//
//  CommentGroupsModel.m
//  inface
//
//  Created by appleone on 15/7/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "CommentGroupsModel.h"

@implementation CommentGroupsModel
+(instancetype)sharedInstance
{
    static CommentGroupsModel * addGroupMemberModule;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,^{
        addGroupMemberModule = [[CommentGroupsModel alloc]init];
    });
    return addGroupMemberModule;
    
}


@end
