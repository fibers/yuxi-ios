//
//  TheatreListDetailViewController.h
//  inface
//
//  Created by Mac on 15/5/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧情阅读版权3个视图总得控制器（世界-剧）
#import "BaseViewController.h"
#import "TheatreDetailViewController.h"

#import "CopyrightInformationViewController.h"

@interface TheatreListDetailViewController : BaseViewController<UIScrollViewDelegate>
{

    UIView *aNavView;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,nonatomic) NSString * storyID;
@property(nonatomic ,strong)NSDictionary * dic;
@property(nonatomic,strong)NSString * creatorid;
@property(nonatomic,strong)NSString * playingStory;//是否从正在开戏的群进来的
@end
