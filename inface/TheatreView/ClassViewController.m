//
//  ClassViewController.m
//  inface
//
//  Created by huangzengsong on 15/7/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ClassViewController.h"

@interface ClassViewController ()

@end

@implementation ClassViewController

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=@"分类";
    
    self.scrollView.userInteractionEnabled=YES;//用户可交互
    self.scrollView.backgroundColor=[UIColor clearColor];
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.delegate=self;
    atitleArray = [NSMutableArray array];

    [self getStoryType];
    
    
}

-(void)getStoryType
{
    NSDictionary *params = @{@"uid":USERID,@"token":@"110"};
    [HttpTool postWithPath:@"GetStoryCategory" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            atitleArray= [[JSON objectForKey:@"list"]mutableCopy];
            float width=70;
            float height=35;
            
            float widthBlank=(ScreenWidth-width*4)/5.0;
            float heightBlank=20;
            //添加12个按钮
            for (int i=0; i<atitleArray.count; i++) {
                UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
                aButton.tag=i+10001;
                NSString * storyCategory = [[atitleArray objectAtIndex:i]objectForKey:@"name"];
                
                int j=i/4;//第几行
                int k=i%4;//第几列
                
                aButton.frame=CGRectMake(widthBlank+(width+widthBlank)*k,20+(height+heightBlank)*j, width, height);
                
                [aButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
                aButton.titleLabel.font=[UIFont systemFontOfSize:14];
                aButton.titleLabel.textAlignment=NSTextAlignmentCenter;
                [aButton setBackgroundColor:[UIColor whiteColor]];
                [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
                [aButton setTitle:storyCategory forState:UIControlStateNormal];
                [aButton addTarget:self action:@selector(searchaction:) forControlEvents:UIControlEventTouchUpInside];
                aButton.layer.borderWidth = 1;
                aButton.layer.borderColor = XIAN_COLOR.CGColor;
                aButton.layer.cornerRadius = 4;
                aButton.clipsToBounds = YES;
                
                [self.scrollView addSubview:aButton];
                
            }
            
            int m=atitleArray.count/4;//第几行
            int n=atitleArray.count%4;//第几列
            if (n==0) {
                m=m;
            } else {
                m=m+1;
            }
            self.scrollView.contentSize=CGSizeMake(ScreenWidth,  (height+heightBlank)*m+50);//滚动的内容尺寸
        }else{
            
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
        }
        
    } failure:^(NSError *error) {
    }];


}

//12个按钮的搜索
-(void)searchaction:(UIButton *)sender{
    
    NSInteger buttonIndex=sender.tag-10000;
    NSString * type = [NSString stringWithFormat:@"%ld",buttonIndex];
    NSString * storyname = sender.titleLabel.text;

    [self.navigationController popViewControllerAnimated:YES];

    [self.delegate getStorysByMoreType:type storyname:storyname];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
