//
//  AddCheckGroupViewController.h
//  inface
//
//  Created by appleone on 15/6/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

@protocol GetGroupMembersDelegate;


#import "BaseViewController.h"

//@咒语
@interface AddCheckGroupViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

{
    NSMutableArray *selectArr;

}
@property(assign,nonatomic) id<GetGroupMembersDelegate>delegate;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *tableDataFriends;
@property(nonatomic ,strong)NSString *theatreid;
@property(nonatomic ,strong)NSString *theatreGroupid;//审核群id
@property(nonatomic ,strong)NSString *CurtheatreGroupid;//当前群id
@property(nonatomic,assign)BOOL   isInCurrentGroup;//是否从临时群进来的
@property(assign,nonatomic)BOOL   isAnimating;
@end
@protocol GetGroupMembersDelegate <NSObject>

-(void)getChatTextView :(NSString *)chatText selectPersons:(NSMutableArray *)selectPersons;//给输入框赋值

@end