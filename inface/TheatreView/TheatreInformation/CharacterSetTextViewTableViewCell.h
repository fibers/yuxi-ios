//
//  CharacterSetTextViewTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//人物设置TextView
#import <UIKit/UIKit.h>

@interface CharacterSetTextViewTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet PlaceholderTextView *UserDetailTextView;//详情
@end
