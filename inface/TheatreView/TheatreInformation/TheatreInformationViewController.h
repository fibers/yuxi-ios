//
//  TheatreInformationViewController.h
//  inface
//
//  Created by Mac on 15/4/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧群的详情页
#import <UIKit/UIKit.h>
#import "FamilyNameTableViewCell.h"
#import "FamilyIDTableViewCell.h"
#import "HZSFamilyButton.h"
#import "TheCharacterSetViewController.h"
#import "EditTextfieldTheatreViewController.h"
#import "EditTextViewTheatreViewController.h"
#import "TheatreIDTableViewCell.h"
#import "ReadingAuthoritySettingsViewController.h"
#import "ReportTheatreViewController.h"
#import "AddTheatreFriendViewController.h"

#import "CharacterSetViewController.h"
@interface TheatreInformationViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    NSMutableArray *aimgArray;//家族成员数组
    NSMutableArray * personsArr;
    
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;
@property(nonatomic ,strong)NSDictionary * dic;
@property(nonatomic ,strong)NSString *theatreid;
@property(nonatomic ,strong)NSString *theatreGroupid;
@property(nonatomic,strong) NSString * applyType;
@property(nonatomic,assign)BOOL  isApplyToJoin;//是否被邀请加入
@property(nonatomic,strong)NSString * fromid;
@property (assign,nonatomic)   BOOL isNeedShowchatting;//返回的时候是哦否需要根据senssion来加载会话
@property(strong,nonatomic )   SessionEntity * chatSession;
@property(assign,nonatomic)     BOOL canChat;//是否能进聊天页面
@property(strong,nonatomic)    NSString * theaterName;
@property(assign,nonatomic)    BOOL hasDelete;
@property(strong,nonatomic)    UITableViewCell * deleteGroups;
@property(assign,nonatomic)    BOOL   isAnimating;
@end
