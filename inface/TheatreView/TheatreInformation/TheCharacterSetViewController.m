//
//  TheCharacterSetViewController.m
//  inface
//
//  Created by Mac on 15/4/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "TheCharacterSetViewController.h"

@interface TheCharacterSetViewController ()

@end

@implementation TheCharacterSetViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    
    if ([massTextField.text isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入皮表" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    if ([nameTextField.text isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入姓名" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    if ([sexTextField.text isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入性别" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    if ([attributeTextField.text isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入属性" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    if ([characterTextField.text isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入性格" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    if ([appearanceTextView.text isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入外貌" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }


    //黄增松roleid 皮表id 需要对接接口 
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSString * roleid;
    if (massTextField.selectedRow==-1) {
        roleid=[self.dic objectForKey:@"roleid"];

    } else {
        roleid=[[self.cityArray objectAtIndex:massTextField.selectedRow]objectForKey:@"id"];

    }
    
    NSString *gender;
    if ([sexTextField.text isEqualToString:@
        "男"]) {
        gender=@"1";
    } else {
        gender=@"0";
    }
    NSDictionary * dic ;
    if ([otherTextView.text isEmpty]) {

        dic = @{@"uid":userid,@"storyid":self.theatreid,@"groupid":self.theatreGroupid,@"roleid":roleid,@"nickname":nameTextField.text,@"gender":gender,@"property":attributeTextField.text,@"character":characterTextField.text,@"appearance":appearanceTextView.text,@"token":@"110"};
        
    }else{
    
        dic = @{@"uid":userid,@"storyid":self.theatreid,@"groupid":self.theatreGroupid,@"roleid":roleid,@"nickname":nameTextField.text,@"gender":gender,@"property":attributeTextField.text,@"character":characterTextField.text,@"appearance":appearanceTextView.text,@"other":otherTextView.text,@"token":@"110"};
    }

    
    [HttpTool postWithPath:@"SettingRoleInStory" params:dic success:^(id JSON) {
        //修改成功
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:@"修改成功"delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StoryInformationChanged" object:self userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"人物设置";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
//    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    [self getPickerData];
    
    //获取制定用户在群里的角色设定信息
    [self GetStoryPersonInfo];
    
}

-(void)GetStoryPersonInfo{


    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"pid":userid,@"token":@"110",@"storyid":self.theatreid};
    [HttpTool postWithPath:@"GetStoryPersonInfo" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
            
            self.dic=[JSON objectForKey:@"personinfo"];
            [self.tableView reloadData];
        }else{
            
        }
    } failure:^(NSError *error) {
        
        
    }];
    

}

- (void)getPickerData {
    

    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary *params = @{@"uid":userid,@"storyid":self.theatreid,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetStoryRoles" params:params success:^(id JSON) {
        
        self.cityArray=[[NSMutableArray alloc]init];
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            self.cityArray=[JSON objectForKey:@"roles"];

            
            [self.tableView reloadData];

        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];

    
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        static NSString *identifier=@"IQDropDownTextFieldTableViewCell";
        IQDropDownTextFieldTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"IQDropDownTextFieldTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor clearColor];
        }
        
        cell.UserDetailTextField.borderStyle=UITextBorderStyleRoundedRect;
        
        cell.UserNameLabel.text=@"皮表>>";
        cell.UserDetailTextField.placeholder=@"请选择皮表";
        cell.UserDetailTextField.text=[self.dic objectForKey:@"roletitle"];
        cell.UserDetailTextField.delegate=self;
        massTextField=cell.UserDetailTextField;
        
        NSMutableArray *aArray=[[NSMutableArray alloc]init];
        for (int i=0; i<self.cityArray.count; i++) {
            NSString *title=[[self.cityArray objectAtIndex:i]objectForKey:@"title"];
            [aArray addObject:title];
        }
        if (aArray.count!=0) {
            massTextField.itemList=aArray;
        }
        
        
        return cell;
    }else if (indexPath.row==1) {
        static NSString *identifier=@"CharacterSetTextFieldTableViewCell";
        CharacterSetTextFieldTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"CharacterSetTextFieldTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor clearColor];
        }
        
        cell.UserDetailTextField.borderStyle=UITextBorderStyleRoundedRect;
        
        cell.UserNameLabel.text=@"姓名>>";
        cell.UserDetailTextField.placeholder=@"请输入姓名";
        cell.UserDetailTextField.text=[self.dic objectForKey:@"nickname"];
        cell.UserDetailTextField.delegate=self;
        nameTextField=cell.UserDetailTextField;
        
        return cell;
    } else if (indexPath.row==2){
        static NSString *identifier=@"IQDropDownTextFieldTableViewCell";
        IQDropDownTextFieldTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"IQDropDownTextFieldTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor clearColor];
        }
        
        cell.UserDetailTextField.borderStyle=UITextBorderStyleRoundedRect;
        
        cell.UserNameLabel.text=@"性别>>";
        cell.UserDetailTextField.placeholder=@"请选择性别";
        if ([[self.dic objectForKey:@"gender"]isEqualToString:@"1"]) {
            cell.UserDetailTextField.text=@"男";
        } else {
            cell.UserDetailTextField.text=@"女";
        }
        
        cell.UserDetailTextField.delegate=self;
        sexTextField=cell.UserDetailTextField;
        sexTextField.itemList=[[NSMutableArray alloc]initWithObjects:@"男",@"女", nil];
        
        return cell;
    } else if (indexPath.row==3){
        static NSString *identifier=@"CharacterSetTextFieldTableViewCell";
        CharacterSetTextFieldTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"CharacterSetTextFieldTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor clearColor];
        }
        
        cell.UserDetailTextField.borderStyle=UITextBorderStyleRoundedRect;
        
        cell.UserNameLabel.text=@"属性>>";
        cell.UserDetailTextField.placeholder=@"请输入属性";
        cell.UserDetailTextField.text=[self.dic objectForKey:@"property"];
        cell.UserDetailTextField.delegate=self;
        attributeTextField=cell.UserDetailTextField;
        
        return cell;
    } else  if (indexPath.row==4){
        static NSString *identifier=@"CharacterSetTextFieldTableViewCell";
        CharacterSetTextFieldTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"CharacterSetTextFieldTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor clearColor];
        }
        
        cell.UserDetailTextField.borderStyle=UITextBorderStyleRoundedRect;
        
        cell.UserNameLabel.text=@"性格>>";
        cell.UserDetailTextField.placeholder=@"请输入性格";
        cell.UserDetailTextField.text=[self.dic objectForKey:@"character"];
        cell.UserDetailTextField.delegate=self;
        characterTextField=cell.UserDetailTextField;
        
        return cell;
    }else if (indexPath.row==5) {
        
        static NSString *identifier=@"CharacterSetTextViewTableViewCell";
        CharacterSetTextViewTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"CharacterSetTextViewTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor clearColor];
        }
        
        //可以编辑状态
        cell.UserDetailTextView.userInteractionEnabled=YES;
        //设置发文字框为圆角矩形
        cell.UserDetailTextView.layer.borderWidth = 1;
        cell.UserDetailTextView.layer.borderColor = COLOR(239, 239, 239, 1).CGColor;
        cell.UserDetailTextView.layer.cornerRadius = 6;
        cell.UserDetailTextView.clipsToBounds = YES;
        
        cell.UserNameLabel.text=@"外貌>>";
        cell.UserDetailTextView.delegate=self;
        cell.UserDetailTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        cell.UserDetailTextView.font = [UIFont systemFontOfSize:14.0f];
        cell.UserDetailTextView.textColor=[UIColor blackColor];
        cell.UserDetailTextView.placeholder=@"请输入外貌";
        cell.UserDetailTextView.text=[self.dic objectForKey:@"appearance"];
        cell.UserDetailTextView.placeholderFont=[UIFont systemFontOfSize:14];
        cell.UserDetailTextView.placeholderColor=COLOR(204, 204, 204, 1);
        appearanceTextView=cell.UserDetailTextView;
        
        return cell;
        
    }else {
        
        static NSString *identifier=@"CharacterSetTextViewTableViewCell";
        CharacterSetTextViewTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"CharacterSetTextViewTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor clearColor];
        }
        
        //可以编辑状态
        cell.UserDetailTextView.userInteractionEnabled=YES;
        //设置发文字框为圆角矩形
        cell.UserDetailTextView.layer.borderWidth = 1;
        cell.UserDetailTextView.layer.borderColor = COLOR(239, 239, 239, 1).CGColor;
        cell.UserDetailTextView.layer.cornerRadius = 6;
        cell.UserDetailTextView.clipsToBounds = YES;
        
        
        cell.UserNameLabel.text=@"其他>>";
        cell.UserDetailTextView.delegate=self;
        cell.UserDetailTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        cell.UserDetailTextView.font = [UIFont systemFontOfSize:14.0f];
        cell.UserDetailTextView.textColor=[UIColor blackColor];
        cell.UserDetailTextView.placeholder=@"请输入其他";
        cell.UserDetailTextView.text=[self.dic objectForKey:@"other"];
        cell.UserDetailTextView.placeholderFont=[UIFont systemFontOfSize:14];
        cell.UserDetailTextView.placeholderColor=COLOR(204, 204, 204, 1);
        otherTextView=cell.UserDetailTextView;
        
        return cell;
    }
    
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0||indexPath.row==1||indexPath.row==2||indexPath.row==3||indexPath.row==4){
        
        
        return 44;
        
    }else{
        return 140;
    }
    
    
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
