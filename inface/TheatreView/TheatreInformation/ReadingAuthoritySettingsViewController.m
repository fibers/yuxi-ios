//
//  ReadingAuthoritySettingsViewController.m
//  inface
//
//  Created by Mac on 15/5/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ReadingAuthoritySettingsViewController.h"

@interface ReadingAuthoritySettingsViewController ()
{
    ChangeReadAutority      readBlock;
    
    NSString            *readAutority;
}
@end

@implementation ReadingAuthoritySettingsViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{

    //阅读权限设置
//    int ReadingAuthoritySettings=[[[NSUserDefaults standardUserDefaults]objectForKey:@"ReadingAuthoritySettings"]intValue];

    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSString *permission;
    if ([readAutority isEqualToString:@"1"]) {
        permission=@"0";//群成员
    } else {
        permission=@"1";//所有人
    }
    NSDictionary *params = @{@"uid":userid,@"storyid":self.theatreid,@"groupid":@"",@"permission":permission,@"token":@"110"};
    [HttpTool postWithPath:@"SetGroupPerssion" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            [[NSUserDefaults standardUserDefaults]setObject:readAutority forKey:self.theatreid];//将剧的阅读权限设置保存起来

            if (readBlock) {
                readBlock(permission);
            }
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"修改成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"StoryInformationChanged" object:self userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
        
    } failure:^(NSError *error) {
        
        
    }];
    

    
}

-(void)changeReadPermisson:(ChangeReadAutority)block
{
    readBlock = block;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    readAutority = [[NSUserDefaults standardUserDefaults]objectForKey:self.theatreid];
    
    if (!readAutority || [readAutority isEqualToString:@""] || [readAutority isEqual:nil] || [readAutority isEqual:NULL]) {
        readAutority  = @"0";
    }
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"发送" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"阅读权限设置";
    
    self.tableDataFriends=[[NSMutableArray alloc]initWithObjects:@"所有人", @"群成员", nil];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }

    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.tableDataFriends.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"ReadingAuthoritySettingsTableViewCell";
    ReadingAuthoritySettingsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"ReadingAuthoritySettingsTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    cell.UserNameLabel.text=[self.tableDataFriends objectAtIndex:indexPath.row];
    
    if ([readAutority integerValue] == indexPath.row) {
        [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang"] forState:UIControlStateNormal];

    }else{
        [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang-1"] forState:UIControlStateNormal];

    }
    cell.SelectButton.tag=10000+indexPath.row;
    [cell.SelectButton addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
    

    return cell;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 44;
    
}


//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}




//选择按钮
-(void)SelectButton:(UIButton *)sender{
    UIButton *btn = (UIButton *)sender;
    
    readAutority = [NSString stringWithFormat:@"%ld",btn.tag - 10000];
//    [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithInteger:btn.tag-10000] forKey:@"ReadingAuthoritySettings"];
    
    
    [self.tableView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
