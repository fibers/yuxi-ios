//
//  AddTheatreFriendViewController.m
//  inface
//
//  Created by huangzengsong on 15/6/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "AddTheatreFriendViewController.h"
#import "YXMyCenterViewController.h"
#import "MsgAddGroupConfirmAPI.h"
@interface AddTheatreFriendViewController ()

@end

@implementation AddTheatreFriendViewController

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    if (selectArr.count==0) {
        UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:@"请选择邀请的成员" message:nil delegate:nil
 cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    NSMutableArray *deleteArr=[[NSMutableArray alloc]init];
    for (int i=0; i<selectArr.count; i++) {
        NSDictionary * dic = [selectArr objectAtIndex:i];
        long indexValue  = [[[dic allValues]objectAtIndex:0]integerValue];
        NSString * selectid;
        if ([switching isEqualToString:@"0"]) {
            //审核群好友
            
            selectid=[[self.tableData objectAtIndex:indexValue]objectForKey:@"id"];
            
        }else{
            
            //好友
            selectid=[[self.tableDataFriends objectAtIndex:indexValue]objectForKey:@"id"];
        }
        
        NSString *user_id=[NSString stringWithFormat:@"user_%@",selectid];
        [deleteArr addObject:user_id];
    }
    NSString *group_id=[NSString stringWithFormat:@"group_%@",self.CurtheatreGroupid];
    
    [[ChangeGroupMemberModel shareInsatance]addMemberNotNeedNotifygroupid:group_id users:deleteArr];
    [deleteArr enumerateObjectsUsingBlock:^(NSString * obj, NSUInteger idx, BOOL *stop) {
        NSString * sessionid = [obj componentsSeparatedByString:@"_"][1];
        //添加群成员时，需要增加cofirm接口
        MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
        NSArray * arrary = @[@(0),sessionid,self.CurtheatreGroupid,@(0),@(SessionTypeSessionTypeSingle),@(1)];
        [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
            
        }];
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StoryInformationChanged" object:self userInfo:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
    UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:@"添加成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}
-(void)uisegumentSelectionChange:(NSInteger)selection{
    if (selection==0) {
        switching=@"0";
        selectArr=[[NSMutableArray alloc]init];
        [self.tableView reloadData];
    } else {
        switching=@"1";
        selectArr=[[NSMutableArray alloc]init];
        [self.tableView reloadData];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    switching=@"0";
    
    LjjUISegmentedControl* ljjuisement=[[LjjUISegmentedControl alloc]initWithFrame:CGRectMake(77, 0, ScreenWidth-154, 44)];
    NSArray* ljjarray=[NSArray arrayWithObjects:@"审核群成员",@"我的好友",nil];
    [ljjuisement AddSegumentArray:ljjarray];
    ljjuisement.delegate=self;
    self.navigationItem.titleView=ljjuisement;
    

    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"添加" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    selectArr=[[NSMutableArray alloc]init];
    
    [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:self.theatreid success:^(id JSON) {
        //取得审核群的ID
        NSArray * groupsArr = [JSON objectForKey:@"groups"];
        for (NSDictionary * dic in groupsArr) {
            if ([[dic objectForKey:@"isplay"]isEqualToString:@"0"]) {
                self.theatreGroupid = [dic objectForKey:@"groupid"];
                [self GetStoryGroupInfo];

            }
        }
    }failure:^(id Json) {
        
        
    }];
    //获取好友列表
    [self getFriendList];
}




//获取剧群信息
-(void)GetStoryGroupInfo
{
    self.tableData = [NSMutableArray array];
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"groupid":self.theatreGroupid,@"token":@"110",@"storyid":self.theatreid};
    [HttpTool postWithPath:@"GetStoryGroupInfo" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
            
            NSArray * arr =[[JSON objectForKey:@"groupinfo"]objectForKey:@"members"];
            for (int i = 0;i < [arr count];i++) {
                NSDictionary * dic = [arr objectAtIndex:i];
                if (![[dic objectForKey:@"id"]isEqualToString:userid]) {
                    [self.tableData addObject:dic];
                }
            }
            [self.tableView reloadData];
        }else{
            
        }
    } failure:^(NSError *error) {
        
        
    }];
    
}




-(void)getFriendList{
    
    //获取好友列表接口(Post)
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary *params = @{@"uid":userid,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetFriendList" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            NSMutableArray *zongArray=[[NSMutableArray alloc]init];
            NSMutableArray *aArray=[[JSON objectForKey:@"groups"]mutableCopy];
            for (int i=0; i<aArray.count; i++) {
                NSMutableDictionary *adic=[aArray objectAtIndex:i];
                NSMutableArray *aArray=[adic objectForKey:@"friends"];
                for (int j=0; j<aArray.count; j++) {
                    [zongArray addObject:[aArray objectAtIndex:j]];
                }
                
            }
            
            self.tableDataFriends=zongArray;
            [self.tableView reloadData];
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if ([switching isEqualToString:@"0"]) {
        //审核群好友
        
        return self.tableData.count;
        
    }else{
        
        //好友
        return self.tableDataFriends.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"FriendsSelectTableViewCell";
    FriendsSelectTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"FriendsSelectTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if ([switching isEqualToString:@"0"]) {
        //审核群好友
        cell.UserNameLabel.text=[[self.tableData objectAtIndex:indexPath.row]objectForKey:@"nickname"];
        
        NSString *urlString=[[self.tableData objectAtIndex:indexPath.row]objectForKey:@"portrait"];
        int isVip = [urlString isSignedUser];
        UIImage * image = [UIImage imageNamed:@"vipWriter"];
        UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
        switch (isVip) {
            case 0:
                cell.vipWriterImage.hidden = YES;
                break;
            case 1:
                cell.vipWriterImage.hidden = NO;
                
                cell.vipWriterImage.image = image;
                
                break;
            case 2:
                cell.vipWriterImage.hidden = NO;
                
                cell.vipWriterImage.image = officalImage;
                
                break;
            case 3:
                cell.vipWriterImage.hidden = NO;
                
                cell.vipWriterImage.image = officalImage;
                
                break;
            default:
                break;
        }

        [cell.UserImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
        cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
        cell.UserImage.clipsToBounds=YES;
        cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;

        
    }else{
        
        //好友
        cell.UserNameLabel.text=[[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"name"];
        
        NSString *urlString=[[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"portrait"];
        
        int isVip = [urlString isSignedUser];
        UIImage * image = [UIImage imageNamed:@"vipWriter"];
        UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
        switch (isVip) {
            case 0:
                cell.vipWriterImage.hidden = YES;
                break;
            case 1:
                cell.vipWriterImage.hidden = NO;
                
                cell.vipWriterImage.image = image;
                
                break;
            case 2:
                cell.vipWriterImage.hidden = NO;
                
                cell.vipWriterImage.image = officalImage;
                
                break;
            case 3:
                cell.vipWriterImage.hidden = NO;
                
                cell.vipWriterImage.image = officalImage;
                
                break;
            default:
                break;
        }

        [cell.UserImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
        cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
        cell.UserImage.clipsToBounds=YES;
        cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;

    }
    
    BOOL isexit = NO;
    for (NSDictionary * dic in selectArr) {
        NSString * index_path =[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        if ([[dic allKeys]containsObject:index_path]) {
            isexit = YES;
        }
    }
    if (isexit == YES)
    {
        [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang-1"] forState:UIControlStateNormal];
    }
    
    cell.SelectButton.tag=10000+indexPath.row;
    [cell.SelectButton addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 70;
    
}


//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    

    if ([switching isEqualToString:@"0"]) {
        //审核群好友
        
        NSDictionary *friendArr=[self.tableData objectAtIndex:indexPath.row];
        NSNumber * fid = [friendArr objectForKey:@"id"];
        NSString * FriendID = [NSString stringWithFormat:@"%@",fid];
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if ([userid isEqualToString:FriendID]) {
            //TODO(Xc.)这里注释了。
//            MyCenterViewController * detailViewController = [[MyCenterViewController alloc]init];
//            detailViewController.fromClass=@"ViewPersonalData";
//            detailViewController.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:detailViewController animated:YES];
            
        } else {
            
//            FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
//            detailViewController.FriendID = FriendID;
//            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
        [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
        detailViewController.hidesBottomBarWhenPushed = YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;

        [self.navigationController pushViewController:detailViewController animated:YES];
        
    }else{
        
        //好友
        NSDictionary *friendArr=[self.tableDataFriends objectAtIndex:indexPath.row];
        NSNumber * fid = [friendArr objectForKey:@"id"];
        NSString * FriendID = [NSString stringWithFormat:@"%@",fid];
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if ([userid isEqualToString:FriendID]) {
            //TODO(Xc.)这里注释了。
//            MyCenterViewController * detailViewController = [[MyCenterViewController alloc]init];
//            detailViewController.fromClass=@"ViewPersonalData";
//            detailViewController.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:detailViewController animated:YES];
            
        } else {
//            
//            FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
//            detailViewController.FriendID = FriendID;
//            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
        [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
        detailViewController.hidesBottomBarWhenPushed = YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:NO];
    }
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

//选择按钮
-(void)SelectButton:(UIButton *)sender{
    UIButton *btn = (UIButton *)sender;
    NSInteger index=btn.tag-10000;
    NSString * index_key = [NSString stringWithFormat:@"%ld",(long)index];
    BOOL  isExit = NO;
    for (int i = 0; i < [selectArr count]; i++) {
        NSDictionary * dic = [selectArr objectAtIndex:i];
        int indexValue = [[[dic allValues]objectAtIndex:0]integerValue];
        if (index == indexValue) {
            //已经被选中
            isExit = YES;
            [selectArr removeObjectAtIndex:i];
        }
    }
    if (isExit == NO) {
        //没被选过
        NSDictionary * dic = @{index_key:index_key};
        [selectArr addObject:dic];
    }
    
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
