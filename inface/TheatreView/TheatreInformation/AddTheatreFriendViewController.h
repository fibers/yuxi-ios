//
//  AddTheatreFriendViewController.h
//  inface
//
//  Created by huangzengsong on 15/6/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "FriendsSelectTableViewCell.h"
#import "AddGroupMemberModule.h"
#import "LjjUISegmentedControl.h"
@interface AddTheatreFriendViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,LjjUISegmentedControlDelegate>
{
    NSString * switching;
    NSMutableArray *selectArr;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableDataFriends;
@property (strong, nonatomic) NSMutableArray *tableData;
@property(assign,nonatomic)    BOOL   isAnimating;

@property(nonatomic ,strong)NSString *theatreid;
@property(nonatomic ,strong)NSString *theatreGroupid;//审核群id
@property(nonatomic ,strong)NSString *CurtheatreGroupid;//当前群id
@end
