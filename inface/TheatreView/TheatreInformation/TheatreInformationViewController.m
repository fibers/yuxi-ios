  //
//  TheatreInformationViewController.m
//  inface
//
//  Created by Mac on 15/4/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "TheatreInformationViewController.h"
#import "DDAddMemberToGroupAPI.h"
#import "MsgAddGroupConfirmAPI.h"
#import "ChatViewController.h"
#import "GetGroupRoleName.h"
#import "PersonSetViewController.h"
#import "YXGroupCardViewController.h"
#import "OtherPersonRolrView.h"
#import "ChattingModule.h"
#import "GroupMessageSetViewController.h"
#import "AddGroupNotyViewController.h"
#import "ReadNotyViewController.h"
#import "AddGroupNotifyViewController.h"
#import "ReadNotifyViewController.h"
#import "NSString+YXExtention.h"
@interface TheatreInformationViewController ()

@end

@implementation TheatreInformationViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];

//    if (self.isNeedShowchatting == YES) {
//        //发通知chattingviewcontroller根据session重新加载会话
//        [[ChatViewController shareInstance]showChattingContentForSession:self.chatSession];
//    }
}

//右上角按钮
-(void)returnBackr{
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
        //右上角按钮
        //是管理员的话
        
        UIActionSheet* mySheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"删除剧群成员",@"删除并退出剧群",@"举报",nil];
        mySheet.tag=3000;
        [mySheet showInView:self.view];
    }else  if ([[self.dic objectForKey:@"ismember"]isEqualToString:@"1"]){
        
        //是成员
        UIActionSheet* mySheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"退出群",@"举报",nil];
        mySheet.tag=6000;
        [mySheet showInView:self.view];
    }else{
        
        //不是成员
        UIActionSheet* mySheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"举报",nil];
        mySheet.tag=5000;
        [mySheet showInView:self.view];
    }
    
    
}


- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag==3000) {
        //是管理员的话
        if (buttonIndex==0) {
            
            //删除剧群成员
            DeleteFamilyFriendViewController * detailViewController = [[DeleteFamilyFriendViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.familyid = self.theatreGroupid;
            detailViewController.tableDataFriends= [NSMutableArray arrayWithArray: [self.dic objectForKey:@"members"]] ;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
            
        }else if (buttonIndex==1){
            //删除并退出群
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"确定解散剧群吗" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = 9999;
            [alert show];
        }else  if (buttonIndex==2){
            //举报
            ReportTheatreViewController * detailViewController = [[ReportTheatreViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.reportid = self.theatreGroupid;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
            
        }
    }else if (actionSheet.tag==6000) {
        //是成员
        if (buttonIndex==0) {
            
            //退出群
            NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            NSString * user_id = [NSString stringWithFormat:@"user_%@",userid];
            NSString *group_id=[NSString stringWithFormat:@"group_%@",self.theatreGroupid];
            [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:@[user_id]];
            //在聊天页面删除对应的会话体
//            [[FriendsViewController shareInstance]deleteSession:group_id];
            [[NSNotificationCenter defaultCenter]postNotificationName:DELETE_SESSION object:group_id];
            [[MyLiveViewController shareInstance]getMyStorys];
            [self.navigationController popToRootViewControllerAnimated:YES];
            NSString * groupname = [self.dic objectForKey:@"name"];
            NSString * groupName = [NSString stringWithFormat:@"您已退出[%@]",groupname];
            UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:groupName message:@"快去剧场找有兴趣的剧群一起开戏吧" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alertView show];
        }else if (buttonIndex==1){
            
            //举报
            ReportTheatreViewController * detailViewController = [[ReportTheatreViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.reportid = self.theatreGroupid;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
    }else{
        
        //不是成员
        
        if (buttonIndex==0) {
            //举报
            ReportTheatreViewController * detailViewController = [[ReportTheatreViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.reportid = self.theatreGroupid;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
            
        }
    }
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIImage *RightImage=[UIImage imageNamed:@"morebtn"];
    
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 44)];
    [RightButton setImage:RightImage forState:UIControlStateNormal];
    [RightButton setImage:[UIImage imageNamed:@"morebtnselect"] forState:UIControlStateHighlighted];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"群资料";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 20, 0, 20)];
    }

    self.tableView.separatorColor=XIAN_COLOR;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    personsArr = [NSMutableArray array];
    
    //获取剧群信息
    [self GetStoryGroupInfo];
    
    
    
    //观察编辑动作的改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadArray:) name:@"StoryInformationChanged" object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

-(void)reloadArray:(NSNotification *)notification
{
    [self GetStoryGroupInfo];
    
}



#pragma mark alertView代理方法
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==9999) {
        //退出家族操作
        if (buttonIndex== 1) {
            
            NSMutableArray *selectArr=[self.dic objectForKey:@"members"];
            
            NSMutableArray *deleteArr=[[NSMutableArray alloc]init];
            for (int i=0; i<selectArr.count; i++) {
                NSString * selectid=[[selectArr objectAtIndex:i]objectForKey:@"id"];
                NSString * select_id = [NSString stringWithFormat:@"user_%@",selectid];
                [deleteArr addObject:select_id];
            }
            NSString *group_id=[NSString stringWithFormat:@"group_%@",self.theatreGroupid];
            [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:deleteArr];
     
            [self.navigationController popToRootViewControllerAnimated:YES];
//            [[FriendsViewController shareInstance]deleteSession:group_id];
            [[NSNotificationCenter defaultCenter]postNotificationName:DELETE_SESSION object:group_id];
            [[MyLiveViewController shareInstance]getMyStorys];
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"解散剧群成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            
        }
    }
}



//获取剧群信息
-(void)GetStoryGroupInfo
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"groupid":self.theatreGroupid,@"token":@"110",@"storyid":self.theatreid};
    [HttpTool postWithPath:@"GetStoryGroupInfo" params:params success:^(id JSON) {
        [ HttpTool postWithPath:@"GetStoryInfo" params:@{@"uid":userid,@"storyid":self.theatreid} success:^(id JSON) {
            self.theaterName = [JSON objectForKey:@"title"];
            
        } failure:^(NSError *error) {
            
            
        }];
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
            self.dic = [JSON objectForKey:@"groupinfo"];
            //家族成员数组
            aimgArray = [NSMutableArray arrayWithArray:[self.dic objectForKey:@"members"]];
            personsArr = [NSMutableArray arrayWithArray:[self.dic objectForKey:@"members"]];
            //保存剧群成员信息
            [[GetGroupRoleName shareInstance]saveFamily:self.theatreGroupid membersArr:[self.dic objectForKey:@"members"]];
            if ([[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
                [aimgArray addObject:@"+"];
                [aimgArray addObject:@"+"];
            }

            [self.tableView reloadData];

                UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
                aButton.frame=CGRectMake(0,ScreenHeight-64-50, ScreenWidth, 50);
                //    aButton.layer.cornerRadius = 15;
                //    aButton.clipsToBounds = YES;
                
                [aButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                aButton.titleLabel.font=[UIFont systemFontOfSize:20];
                
                [aButton setBackgroundColor:BLUECOLOR];


            if ([[self.dic objectForKey:@"ismember"]isEqualToString:@"1"]) {
                //是家族成员了
//                if (self.canChat == YES) {
//                    aButton.hidden = YES;
//                }else{
                   [aButton addTarget:self action:@selector(handleBtnChat:) forControlEvents:UIControlEventTouchUpInside];
                   [aButton setTitle:@"聊天" forState:UIControlStateNormal];
//                }
            } else {
                //不是家族成员
                if (self.isApplyToJoin) {
                    [aButton addTarget:self action:@selector(agreeJoinInStory) forControlEvents:UIControlEventTouchUpInside];
                    [aButton setTitle:@"同意" forState:UIControlStateNormal];

                }else{
                    //同意群邀请
                    [aButton addTarget:self action:@selector(handleBtnAdd:) forControlEvents:UIControlEventTouchUpInside];
                    [aButton setTitle:@"申请" forState:UIControlStateNormal];
                }

            }
            
            [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
            
            [self.view addSubview:aButton];
            
            
        }else{
            
        }
    } failure:^(NSError *error) {
        
        
    }];
    
}


//邀请好友事件,点击我在本家族的昵称
- (void)AddFriendPeople:(UIButton *)btn
{
            NSString *examinegroupid= [self.dic objectForKey:@"judgegroupid"];
#pragma mark 判断审核群的方法改变
        if ([examinegroupid isEqualToString:self.theatreGroupid]) {
            //审核群
            //添加群成员
            AddFamilyFriendViewController * detailViewController = [[AddFamilyFriendViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.familyid = self.theatreGroupid;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        } else {
            //非审核群
            //添加群成员
            AddTheatreFriendViewController * detailViewController = [[AddTheatreFriendViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.theatreid = self.theatreid;
            detailViewController.CurtheatreGroupid=self.theatreGroupid;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
 
    
}

//查看好友详情事件
- (void)FriendPeopleDetail:(UIButton *)btn
{
    NSInteger index=btn.tag-10000;
    //好友

//    NSString *FriendID=[[aimgArray objectAtIndex:index] objectForKey:@"id"];
//    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
//    if ([userid isEqualToString:FriendID]) {
//        MyCenterViewController * detailViewController = [[MyCenterViewController alloc]init];
//        detailViewController.fromClass=@"ViewPersonalData";
//        detailViewController.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:detailViewController animated:YES];
//        
//    } else {
//        
//        FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
//        detailViewController.FriendID = FriendID;
//        [self.navigationController pushViewController:detailViewController animated:YES];
//    }
        NSString *FriendID=[[aimgArray objectAtIndex:index] objectForKey:@"id"];
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([userid isEqualToString:FriendID]) {
        //自己的人设画面
//        YXGroupCardViewController * detailViewController = [[YXGroupCardViewController alloc] init];
//        detailViewController.theartorId = self.theatreid;
//        detailViewController.groupId  =self.theatreGroupid;
//        detailViewController.hidesBottomBarWhenPushed = YES;
//        if (detailViewController.isAnimating) {
//            return;
//        }
//        detailViewController.isAnimating = YES;
//        [self.navigationController pushViewController:detailViewController animated:YES];
        
        OtherPersonRolrView * detailViewController = [[OtherPersonRolrView alloc] init];
        detailViewController.friendId = [[aimgArray objectAtIndex:index] objectForKey:@"id"];
        NSString *portrait=[[aimgArray objectAtIndex:index] objectForKey:@"portrait"];
        detailViewController.portrait = portrait;
        detailViewController.thearterId = self.theatreid;
        detailViewController.hidesBottomBarWhenPushed = YES;
        detailViewController.groupId = self.theatreGroupid;
        NSString *examinegroupid= [self.dic objectForKey:@"judgegroupid"];
        if ([self.theatreGroupid isEqualToString:examinegroupid]) {
            //是审核群
            detailViewController.isChatInCheckGroup = YES;
        }
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    }
    
    else{
        OtherPersonRolrView * detailViewController = [[OtherPersonRolrView alloc] init];
        detailViewController.friendId = [[aimgArray objectAtIndex:index] objectForKey:@"id"];
        NSString *portrait=[[aimgArray objectAtIndex:index] objectForKey:@"portrait"];
        detailViewController.portrait = portrait;
        detailViewController.thearterId = self.theatreid;
        detailViewController.hidesBottomBarWhenPushed = YES;
        detailViewController.groupId = self.theatreGroupid;
        NSString *examinegroupid= [self.dic objectForKey:@"judgegroupid"];
        if ([self.theatreGroupid isEqualToString:examinegroupid]) {
            //是审核群
            detailViewController.isChatInCheckGroup = YES;
        }
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    }
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
//如果是戏群则多一个阅读权限的单元格
}


#pragma mark 邀请QQ好友
-(void)applyQQFriends:(UIButton * )sender
{
    //    NSString * ownerName = [self.dic objectForKey:@"ownername"];
//    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
//    NSString * title = [NSString stringWithFormat:@"剧的邀请函"];
//    NSString * familyName = [self.dic objectForKey:@"name"];
//    NSString * content = [NSString stringWithFormat:@"[%@]剧邀请你入驻语戏一起开心地对戏",self.theaterName];
//    NSString * portrait = [self.dic objectForKey:@"portrait"];
//    [HttpTool postWithPath:@"GetAppStoreUrl" params:@{@"uid":userid,@"token":@"100"} success:^(id JSON) {
//        NSString * url = [[JSON objectForKey:@"appurl"]objectForKey:@"appurl"];
//        NSDictionary * params = @{@"title":title,@"content":content,@"portrait":@"100",@"url":url};
//        [[ShareModel sharedInstance]applyQQFriends:sender applyDic:params];
//
//
//    } failure:^(NSError *error) {
//        
//        
//    }];
    
    if (self.hasDelete == NO) {
        self.hasDelete = YES;
        for (int i = 0; i < personsArr.count; i++) {
            //
            UIButton * deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            float width = 25;
            float height = 25;
            int line = i /4;//行数
            int brank = i%4;//列数
            float widthBlank = (ScreenWidth - 50*4)/5.0;
            float heightBlank = 10;
            deleteBtn.frame = CGRectMake(widthBlank+35 +(50+widthBlank)*brank, 5 + 80*line, width, height);
            [deleteBtn setImage:[UIImage imageNamed:@"deleteGroupsMember"] forState:UIControlStateNormal];
            [deleteBtn addTarget:self action:@selector(deleteGroupMember:) forControlEvents:UIControlEventTouchUpInside];
            deleteBtn.tag = 10000+i;
            NSDictionary * dic = [personsArr objectAtIndex:i];
            if (![[dic objectForKey:@"id"]isEqualToString:USERID]) {
                [self.deleteGroups.contentView addSubview:deleteBtn];
                
            }
            
        }
    }else{
        [self GetStoryGroupInfo];
        self.hasDelete = NO;
    }

}



-(void)deleteGroupMember:(UIButton*)sender
{
    NSDictionary * personDic = personsArr[sender.tag-10000];
    NSString * user_id = [NSString stringWithFormat:@"user_%@",[personDic objectForKey:@"id"]];
    NSString * group_id = [NSString stringWithFormat:@"group_%@",self.theatreGroupid];
    [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:@[user_id]];
    self.hasDelete = NO;
    [self GetStoryGroupInfo];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        
    if (indexPath.row==0) {
        static NSString *identifier=@"TheatreIDTableViewCell";
        TheatreIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.UserNameLabel.text=@"剧成员";
        NSInteger count=[[self.dic objectForKey:@"members"]count];
        cell.UserDetailLabel.text=[NSString stringWithFormat:@"%ld人",(long)count];
        cell.ArrowImage.hidden=YES;
        
        return cell;
        
    }else  if (indexPath.row==1){
        static NSString *identifier=@"UITableViewCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        for (UIView *acellview in cell.contentView.subviews) {
            [acellview removeFromSuperview];
        }
        self.deleteGroups = cell;
        //添加12个按钮
        for (int i=0; i<aimgArray.count; i++) {
            HZSFamilyButton *aButton=[HZSFamilyButton buttonWithType:UIButtonTypeCustom];
            aButton.tag=i+10000;
            float width=50;
            float height=70;
            
            int j=i/4;//第几行
            int k=i%4;//第几列
            float widthBlank=(ScreenWidth-50*4)/5.0;
            float heightBlank=10;
            aButton.frame=CGRectMake(widthBlank+(width+widthBlank)*k,heightBlank+(height+heightBlank)*j, width, height);
            aButton.imageView.layer.borderWidth = 0;
            aButton.imageView.layer.borderColor = BLUECOLOR.CGColor;
            aButton.imageView.layer.cornerRadius = 25;
            aButton.imageView.clipsToBounds = YES;
            [aButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            aButton.titleLabel.font=[UIFont systemFontOfSize:12];
            aButton.titleLabel.textAlignment=NSTextAlignmentCenter;
            [aButton setBackgroundColor:[UIColor clearColor]];
            [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
            
            [cell.contentView addSubview:aButton];
            NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
           //非管理员时
            if(![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]){
                NSString *portrait=[[[aimgArray objectAtIndex:i] objectForKey:@"portrait"]stringOfW100ImgUrl];
                int isVip = [portrait isSignedUser];
                if (isVip ==1) {
                    //如果是vip写手
                    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(35, 35, 15, 15)];
                    
                    imageView.image = [UIImage imageNamed:@"vipWriter"];
                    
                    [aButton addSubview:imageView];
                }else{
                    if (isVip == 2 || isVip == 3) {
                        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(35, 35, 15, 15)];
                        
                        imageView.image = [UIImage imageNamed:@"maxOffical"];
                        
                        [aButton addSubview:imageView];
                    }
                }
                [aButton sd_setImageWithURL:[NSURL URLWithString:portrait] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
                [aButton setTitle:[[aimgArray objectAtIndex:i] objectForKey:@"nickname"] forState:UIControlStateNormal];
                [aButton addTarget:self action:@selector(FriendPeopleDetail:) forControlEvents:UIControlEventTouchUpInside];
            }else{
                
                if (i==aimgArray.count - 1) {
                    [aButton setImage:[UIImage imageNamed:@"addFriends"] forState:UIControlStateNormal];
                    [aButton setTitle:@"" forState:UIControlStateNormal];
                    [aButton addTarget:self action:@selector(AddFriendPeople:) forControlEvents:UIControlEventTouchUpInside];
                }else{
                    if (i==aimgArray.count-2) {
                        [aButton setImage:[UIImage imageNamed:@"deleteMembers"] forState:UIControlStateNormal];
                        //                    [aButton setTitle:@"QQ好友" forState:UIControlStateNormal];
                        [aButton addTarget:self action:@selector(applyQQFriends:) forControlEvents:UIControlEventTouchUpInside];
                    }else{
                        NSString *portrait=[[aimgArray objectAtIndex:i] objectForKey:@"portrait"];
                        int isVip = [portrait isSignedUser];
                        if (isVip ==1) {
                            //如果是vip写手
                            UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(35, 35, 15, 15)];
                            
                            imageView.image = [UIImage imageNamed:@"vipWriter"];
                            
                            [aButton addSubview:imageView];
                        }else{
                            if (isVip == 2 || isVip == 3) {
                                UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(35, 35, 15, 15)];
                                
                                imageView.image = [UIImage imageNamed:@"maxOffical"];
                                
                                [aButton addSubview:imageView];
                            }
                        }
                        [aButton sd_setImageWithURL:[NSURL URLWithString:portrait] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
                        [aButton setTitle:[[aimgArray objectAtIndex:i] objectForKey:@"nickname"] forState:UIControlStateNormal];
                        [aButton addTarget:self action:@selector(FriendPeopleDetail:) forControlEvents:UIControlEventTouchUpInside];
                    }
                }
            }
            
//            if (i==aimgArray.count-1) {
//                NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
//                if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
//                    //不是管理员的话
//                    aButton.hidden=YES;
//                    aButton.userInteractionEnabled = NO;
//
//                }
//                [aButton setImage:[UIImage imageNamed:@"addFriends"] forState:UIControlStateNormal];
//                [aButton setTitle:@"" forState:UIControlStateNormal];
//                [aButton addTarget:self action:@selector(AddFriendPeople:) forControlEvents:UIControlEventTouchUpInside];
//            }else{
//                if (i == [aimgArray count]-2) {
//                    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
//                    if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
//                        //不是管理员的话
//                        aButton.hidden=YES;
//                        aButton.userInteractionEnabled = NO;
//                    }
//                    [aButton setImage:[UIImage imageNamed:@"deleteMembers"] forState:UIControlStateNormal];
////                    [aButton setTitle:@"QQ好友" forState:UIControlStateNormal];
//                    [aButton addTarget:self action:@selector(applyQQFriends:) forControlEvents:UIControlEventTouchUpInside];
//                }else{
//                   NSString *portrait=[[aimgArray objectAtIndex:i] objectForKey:@"portrait"];
//                   [aButton sd_setImageWithURL:[NSURL URLWithString:portrait] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
//                   [aButton setTitle:[[aimgArray objectAtIndex:i] objectForKey:@"nickname"] forState:UIControlStateNormal];
//                   [aButton addTarget:self action:@selector(FriendPeopleDetail:) forControlEvents:UIControlEventTouchUpInside];
//            }
//            
//            }
            
            
        }
        
        
        return cell;
        
    }
//    }else if (indexPath.row==2) {
//        static NSString *identifier=@"TheatreIDTableViewCell";
//        TheatreIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//        if (!cell)
//        {
//            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreIDTableViewCell" owner:nil options:nil] firstObject];
//            cell.selectionStyle=UITableViewCellSelectionStyleNone;
//        }
//        cell.UserNameLabel.text=@"人物设置";
//        cell.UserDetailLabel.text=[self.dic objectForKey:@"nickname"];
//    
//        return cell;
//    } else if (indexPath.row==3)
    else if (indexPath.row == 2){
        static NSString *identifier=@"TheatreIDTableViewCell";
        TheatreIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.UserNameLabel.text=@"剧群名称";
        cell.UserDetailLabel.text=[self.dic objectForKey:@"name"];
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            cell.ArrowImage.hidden=YES;
        }
        
        return cell;
        
    } else if (indexPath.row==3){
        static NSString *identifier=@"TheatreIDTableViewCell";
        TheatreIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.ArrowImage.hidden=YES;
        cell.UserNameLabel.text=@"ID";
        cell.UserDetailLabel.text=[self.dic objectForKey:@"id"];
        
        return cell;
    }  else if (indexPath.row==4){
        static NSString *identifier=@"TheatreIDTableViewCell";
        TheatreIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.ArrowImage.hidden=YES;
        cell.UserNameLabel.text=@"剧创建者";
        cell.UserDetailLabel.text=[self.dic objectForKey:@"ownernickname"];
        
        return cell;
    } else if (indexPath.row==5){
        static NSString *identifier=@"TheatreIDTableViewCell";
        TheatreIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.UserNameLabel.text=@"剧群简介";
        cell.UserDetailLabel.textAlignment=NSTextAlignmentLeft;
        NSString *FamilyDetail=[self.dic objectForKey:@"intro"];
        
        cell.UserDetailLabel.numberOfLines=0;
        cell.UserDetailLabel.text=FamilyDetail;
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            cell.ArrowImage.hidden=YES;
        }
        
        return cell;
        
    } else if (indexPath.row == 7){
        static NSString *identifier=@"TheatreIDTableViewCell";
        TheatreIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.UserNameLabel.text=@"群公告";
//        cell.UserDetailLabel.text=[self.dic objectForKey:@"nickname"];
        
        return cell;
    }
    else{
        //屏蔽群消息
        static NSString *identifier=@"TheatreIDTableViewCell";
        TheatreIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.UserNameLabel.text=@"群消息设置";
        cell.UserDetailLabel.text=[self.dic objectForKey:@"nickname"];
        
        return cell;
    }
    
    
    
}


#pragma mark聊天,需要判断该群之前有没有聊过天
- (void)handleBtnChat:(UIButton *)btn
{
    
    NSString * familyName = [self.dic objectForKey:@"name"];
    NSString * group_id = [NSString stringWithFormat:@"group_%@",self.theatreGroupid];
    SessionEntity * sessionEntity = [[SessionModule sharedInstance]getSessionById:group_id];
    if (!sessionEntity) {
        sessionEntity = [[SessionEntity alloc]initWithSessionID:group_id SessionName:familyName type:SessionTypeSessionTypeGroup];
        //del by guoq-s
        //[[SessionModule sharedInstance]addToSessionModel:sessionEntity];
        //del by guoq-e
    }

    //剧群聊天页面
    NSString * storyId = [[GetAllFamilysAndStorys shareInstance]getStoryidByGroupid:self.theatreGroupid];//剧Id
    
    NSDictionary * storyDic = [[GetAllFamilysAndStorys shareInstance]getStoryDetail:storyId];
    NSArray * groupsArr = [storyDic objectForKey:@"groups"];
    ChatViewController * detailViewController = [ChatViewController shareInstance];
    detailViewController.isChatInStory = YES;
    detailViewController.checkGroupId = self.theatreGroupid;
    detailViewController.theartorId = self.theatreid;
    [[GetGroupRoleName shareInstance]getGroupRoleName:self.theatreid groupid:self.theatreGroupid success:^(id JSON) {
        for (NSDictionary * dic in groupsArr) {
            if ([[dic objectForKey:@"groupid"]isEqualToString:self.theatreGroupid]) {
                //进入的是剧的某个群
                NSString * isplay = [dic objectForKey:@"isplay"];
                if ([isplay isEqualToString:@"0"]) {
                    //进入的是审核群
                    detailViewController.isInCheckGroup = YES;
                }
                
            }
        }
        detailViewController.hidesBottomBarWhenPushed=YES;
        NSArray * viewControllers = self.navigationController.viewControllers;
        
        NSString *havechat=@"0";
        for (UIViewController * aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[ChatViewController class]]) {
                havechat=@"1";
            }
        }
        if ([havechat isEqualToString:@"0"]) {
            detailViewController.hidesBottomBarWhenPushed=YES;
            [detailViewController showChattingContentForSession:sessionEntity];
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
            
        } else {
            self.navigationController.navigationBar.hidden=NO;
            [detailViewController showChattingContentForSession:sessionEntity];
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController popToViewController:(ChatViewController*)detailViewController animated:YES];
            
        }
        
        
    }];

}

#pragma mark申请加入群
- (void)handleBtnAdd:(UIButton *)btn
{
    NSString *groupid=[NSString stringWithFormat:@"group_%@",self.theatreGroupid];
    [[AddGroupMemberModule sharedInstance]applyToJoinGroupsessionType:SessionTypeSessionTypeSingle groupId:groupid toUserIds:@[] msgId:@"0" ownerId:[self.dic objectForKey:@"ownerid"]];
    

    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"发送请求成功" delegate:@"等待群主大人的批准哦" cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertview show];
    
}


#pragma mark  同意进群
-(void)agreeJoinInStory
{
    NSArray * groupUsers = @[[RuntimeStatus instance].user.objID];
    NSString *groupid=[NSString stringWithFormat:@"group_%@",self.theatreGroupid];
    DDAddMemberToGroupAPI * addGroupMember = [[DDAddMemberToGroupAPI alloc]init];
    NSArray * object = @[groupid,groupUsers];
    [addGroupMember requestWithObject:object Completion:^(id response, NSError *error) {
        if (error) {
            //同意失败
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"数据请求失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alert show];
            [[DDTcpClientManager instance]reconnect];
            return ;
        }
        MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
        NSArray * arrary = @[@(0),self.fromid,groupid,@(0),@(SessionTypeSessionTypeGroup),@(1)];
        [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
            
        }];
        [[MyLiveViewController shareInstance ]getMyStorys];
        [[DDDatabaseUtil instance]deleteAddFriendsMessageFromid:self.fromid groupid:self.theatreGroupid friendsType:@"3" result:@"-2" grouptype:@"2" haschange:@"1"];
        //在群里发一条消息
        DDMessageContentType msgContentType = DDMessageTypeText;
        NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        NSString * msgContent = [NSString stringWithFormat:@"%@已经是剧成员了",userid];
        NSString *sessionid = [NSString stringWithFormat:@"group_%@",groupid];
        SessionEntity * entity = [[SessionModule sharedInstance]getSessionById:sessionid];
        if (!entity) {
            entity = [[SessionEntity alloc]initWithSessionID:sessionid type:SessionTypeSessionTypeGroup];
        }
        ChattingModule * module = [[ChattingModule alloc]init];
        module.sessionEntity = entity;
        DDMessageEntity * message = [DDMessageEntity makeMessage:msgContent Module:module MsgType:msgContentType isSendImage:NO isCurrentGroup:NO];
        [[DDMessageSendManager instance]sendMessage:message isGroup:YES Session:entity completion:^(DDMessageEntity *message, NSError *error) {
            
            
        } Error:^(NSError *error) {
            
            
        }];

    }];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==1){
        
        int j=aimgArray.count/4;//第几行
        int k=aimgArray.count%4;//第几列
        if (k==0) {
            return j*80+10;
        } else {
            return (j+1)*80+10;
        }
        
        
        
    }if (indexPath.row==6) {

        NSString * intro=[self.dic objectForKey:@"intro"];
        float height=[HZSInstances labelAutoCalculateRectWith:intro FontSize:14.0 MaxSize:CGSizeMake(ScreenWidth-140, 2000)].height;
        if (height>30) {
            return height+14;
        } else {
            return 44;
        }
    } else {
        return 44;
    }


    
}


//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==100){
        //人物设置
//        TheCharacterSetViewController * detailViewController = [[TheCharacterSetViewController alloc] init];
//        detailViewController.theatreid=self.theatreid;
//        detailViewController.theatreGroupid=self.theatreGroupid;
//        detailViewController.hidesBottomBarWhenPushed=YES;
//        [self.navigationController pushViewController:detailViewController animated:YES];
        
//        PersonSetViewController * detailViewController = [[PersonSetViewController alloc] init];
//        detailViewController.theatreid=self.theatreid;
//        detailViewController.theatreGroupid=self.theatreGroupid;
//        detailViewController.hidesBottomBarWhenPushed=YES;
          YXGroupCardViewController * detailViewController = [[YXGroupCardViewController alloc] init];
        [detailViewController setOwnerId:[self.dic objectForKey:@"ownerid"]];
        detailViewController.theartorId = self.theatreid;
        detailViewController.groupId  =self.theatreGroupid;
        detailViewController.hidesBottomBarWhenPushed = YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }else if (indexPath.row==2) {
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            return;
        }
        //剧群名称
        EditTextfieldTheatreViewController * detailViewController = [[EditTextfieldTheatreViewController alloc] init];
        detailViewController.theatreGroupid = self.theatreGroupid;
        detailViewController.content=[self.dic objectForKey:@"name"];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }else if (indexPath.row==5) {
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            return;
        }
        //剧简介
        EditTextViewTheatreViewController * detailViewController = [[EditTextViewTheatreViewController alloc] init];
        detailViewController.theatreGroupid = self.theatreGroupid;
        detailViewController.content=[self.dic objectForKey:@"intro"];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    } else if (indexPath.row == 6) {
        //设置群消息屏蔽
        GroupMessageSetViewController * detailerViewController = [[GroupMessageSetViewController alloc]init];
         detailerViewController.group_id = [NSString stringWithFormat:@"group_%@",self.theatreGroupid];
        detailerViewController.hidesBottomBarWhenPushed = YES;
        if (detailerViewController.isAnimating) {
            return;
        }
        detailerViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailerViewController animated:YES];
    }
    if (indexPath.row == 7) {
        //看群公告
        NSString * creatorid = [self.dic objectForKey:@"ownerid"];
        if ([creatorid isEqualToString:USERID]) {
            //管理员自己看群公告
            AddGroupNotifyViewController * addGroupView = [[AddGroupNotifyViewController alloc]init];
            addGroupView.hidesBottomBarWhenPushed = YES;
            [addGroupView setContent:self.theatreGroupid];
            if (addGroupView.isAnimating) {
                return;
            }
            addGroupView.isAnimating = YES;
            [self.navigationController pushViewController:addGroupView animated:YES];
        }else{
            ReadNotifyViewController * readViewController = [[ReadNotifyViewController alloc]init];
            readViewController.hidesBottomBarWhenPushed = YES;
            NSString * creatorName = [self.dic objectForKey:@"ownername"];
            [readViewController setContent:self.theatreGroupid creatorName:creatorName];
            if (readViewController.isAnimating) {
                return;
            }
            readViewController.isAnimating = YES;
            [self.navigationController pushViewController:readViewController animated:YES];
        }
    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
