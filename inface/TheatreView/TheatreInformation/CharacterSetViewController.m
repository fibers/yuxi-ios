//
//  CharacterSetViewController.m
//  inface
//
//  Created by huangzengsong on 15/6/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "CharacterSetViewController.h"

@interface CharacterSetViewController ()

@end

@implementation CharacterSetViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{

    
    if ([[self.dic objectForKey:@"roleid"] isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入皮表" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    if ([[self.dic objectForKey:@"nickname"] isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入姓名" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }

    
    if ([[self.dic objectForKey:@"gender"] isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入性别" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    if ([[self.dic objectForKey:@"property"] isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入属性" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    
    if ([[self.dic objectForKey:@"favourite"] isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入喜好" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    if ([[self.dic objectForKey:@"character"] isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入性格" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    if ([[self.dic objectForKey:@"appearance"] isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入外貌" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
        
    }
    
    
    //黄增松roleid 皮表id 需要对接接口
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * dic ;
    if ([[self.dic objectForKey:@"other"] isEmpty]) {
        
        dic = @{@"uid":userid,@"storyid":self.theatreid,@"groupid":self.theatreGroupid,@"roleid":[self.dic objectForKey:@"roleid"],@"nickname":[self.dic objectForKey:@"nickname"],@"gender":[self.dic objectForKey:@"gender"],@"property":[self.dic objectForKey:@"property"],@"character":[self.dic objectForKey:@"character"],@"appearance":[self.dic objectForKey:@"appearance"],@"token":@"110",@"favourite":[self.dic objectForKey:@"favourite"],@"portrait":[self.dic objectForKey:@"portrait"]};
        
    }else{
        
        dic = @{@"uid":userid,@"storyid":self.theatreid,@"groupid":self.theatreGroupid,@"roleid":[self.dic objectForKey:@"roleid"],@"nickname":[self.dic objectForKey:@"nickname"],@"gender":[self.dic objectForKey:@"gender"],@"property":[self.dic objectForKey:@"property"],@"character":[self.dic objectForKey:@"character"],@"appearance":[self.dic objectForKey:@"appearance"],@"other":[self.dic objectForKey:@"other"],@"token":@"110",@"favourite":[self.dic objectForKey:@"favourite"],@"portrait":[self.dic objectForKey:@"portrait"]};
    }
    
    
    [HttpTool postWithPath:@"SettingRoleInStory" params:dic success:^(id JSON) {
        //修改成功
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:@"修改成功"delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StoryInformationChanged" object:self userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
    } failure:^(NSError *error) {
        
    }];


}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"人物设置";
    
    float imgwidth=ScreenWidth;
    float imgheight=imgwidth*942/640.0;
    self.imgs=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"image0"], [UIImage imageNamed:@"image1"],[UIImage imageNamed:@"image2"],[UIImage imageNamed:@"image3"], [UIImage imageNamed:@"image4"],[UIImage imageNamed:@"image5"],[UIImage imageNamed:@"image6"], [UIImage imageNamed:@"image7"],nil];
    //定义一个滚动视图
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, ScreenHeight-64-imgheight, imgwidth, imgheight)];
    self.scrollView.pagingEnabled=YES;//可以翻页
    self.scrollView.userInteractionEnabled=YES;//用户可交互
    self.scrollView.contentOffset=CGPointMake(0, 0);//记录滚动的点
    self.scrollView.contentSize=CGSizeMake((self.imgs.count)*imgwidth, imgheight);//滚动的内容尺寸
    self.scrollView.delegate=self;
    [self.view addSubview:self.scrollView];
    for (int i=0; i<self.imgs.count; i++) {
//        NSString *path=[self.imgs objectAtIndex:i];
//        NSLog(@"%@",path);
        //GCD加载图片
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            UIImage *image = [RSAinstance downloadImage:path];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(imgwidth*i, 0, imgwidth, imgheight)];
                imageView.tag=i+8765;
                imageView.contentMode=UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds=YES;
                imageView.image=[self.imgs objectAtIndex:i];
                
                
                [self.scrollView addSubview:imageView];
                
            });
            
        });
        
        
    }
    
    UIPageControl *pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0, 0, imgwidth, 30)];
    pageControl.center=CGPointMake(ScreenWidth/2.0, 15);
    self.pageControl=pageControl;
    pageControl.currentPageIndicatorTintColor=BLUECOLOR;
    pageControl.pageIndicatorTintColor=COLOR(168, 196, 211, 1);
    pageControl.userInteractionEnabled=NO;
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages=self.imgs.count;
    [self.view addSubview:self.pageControl];
    
    float width=65;
    float height=57;
    float heightBlank=(ScreenHeight-64-120-4*height)/4;
    //添加左边4个按钮
    for (int i=0; i<4; i++) {
        UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
        aButton.tag=i+10000;
        [aButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
        aButton.frame=CGRectMake(2,heightBlank+(height+heightBlank)*i, width, height);
        [aButton setBackgroundColor:[UIColor clearColor]];
        [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
        [aButton setBackgroundImage:[UIImage imageNamed:@"renshebackground"] forState:UIControlStateNormal];
        [self.view addSubview:aButton];
        [aButton addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
        if (i==0) {
//            [aButton setImage:[UIImage imageNamed:@"biaopi"] forState:UIControlStateNormal];
            [aButton setTitle:@"皮表" forState:UIControlStateNormal];
        }else if (i==1) {

//            [aButton setImage:[UIImage imageNamed:@"xingming"] forState:UIControlStateNormal];
            [aButton setTitle:@"姓名" forState:UIControlStateNormal];
        }else if (i==2) {
            
//            [aButton setImage:[UIImage imageNamed:@"xingbie"] forState:UIControlStateNormal];
            [aButton setTitle:@"性别" forState:UIControlStateNormal];
        }else if (i==3) {
            
//            [aButton setImage:[UIImage imageNamed:@"shuxing"] forState:UIControlStateNormal];
            [aButton setTitle:@"属性" forState:UIControlStateNormal];
        }
        
        
        
        
    }
    
    //添加右边边4个按钮
    for (int i=0; i<4; i++) {
        UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
        aButton.tag=i+10004;
        [aButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
        aButton.frame=CGRectMake(ScreenWidth-70+2,heightBlank+(height+heightBlank)*i, width, height);
        [aButton setBackgroundColor:[UIColor clearColor]];
        [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
        [aButton setBackgroundImage:[UIImage imageNamed:@"renshebackground"] forState:UIControlStateNormal];
        [self.view addSubview:aButton];
        [aButton addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
        if (i==0) {
//            [aButton setImage:[UIImage imageNamed:@"xihao"] forState:UIControlStateNormal];
            [aButton setTitle:@"喜好" forState:UIControlStateNormal];
        }else if (i==1) {
            
//            [aButton setImage:[UIImage imageNamed:@"xingge"] forState:UIControlStateNormal];
            [aButton setTitle:@"性格" forState:UIControlStateNormal];
        }else if (i==2) {
            
//            [aButton setImage:[UIImage imageNamed:@"waimao"] forState:UIControlStateNormal];
            [aButton setTitle:@"外貌" forState:UIControlStateNormal];
        }else if (i==3) {
            
//            [aButton setImage:[UIImage imageNamed:@"qita"] forState:UIControlStateNormal];
            [aButton setTitle:@"其他" forState:UIControlStateNormal];
        }
        
        
        
        
    }
    
    aview=[[UIView alloc]initWithFrame:CGRectMake(25, ScreenHeight-64-100, ScreenWidth-50, 80)];
    aview.layer.borderWidth = 2;
    aview.layer.borderColor = XIAN_COLOR.CGColor;
    aview.layer.cornerRadius = 8;
    aview.clipsToBounds = YES;
    aview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:aview];
    
    //设置发文字框为圆角矩形
    PlaceholderTextView *atextview=[[PlaceholderTextView alloc]initWithFrame:CGRectMake(10, 0, ScreenWidth-70, 80)];
    self.chatInputTextView=atextview;
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInputTextView.placeholder = @"";
    self.chatInputTextView.textColor=BLACK_GRAY_COLOR;
    self.chatInputTextView.placeholderFont=[UIFont systemFontOfSize:14];
    self.chatInputTextView.placeholderColor=GRAY_COLOR;
    self.chatInputTextView.backgroundColor=[UIColor clearColor];
    [self.chatInputTextView addDoneOnKeyboardWithTarget:self action:@selector(doneAction:)];
    [aview addSubview:atextview];
    
    bview=[[UIView alloc]initWithFrame:CGRectMake(25, ScreenHeight-64-60, ScreenWidth-50, 40)];
    bview.layer.borderWidth = 2;
    bview.layer.borderColor = XIAN_COLOR.CGColor;
    bview.layer.cornerRadius = 8;
    bview.clipsToBounds = YES;
    bview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:bview];
    
    
    IQDropDownTextField *atextfield=[[IQDropDownTextField alloc]initWithFrame:CGRectMake(10, 0, ScreenWidth-70, 40)];
    self.chatTextField=atextfield;
    self.chatTextField.delegate=self;
    self.chatTextField.font = [UIFont systemFontOfSize:14.0f];
    self.chatTextField.placeholder = @"";
    self.chatTextField.textColor=BLACK_GRAY_COLOR;
    self.chatTextField.backgroundColor=[UIColor clearColor];
    [self.chatTextField addDoneOnKeyboardWithTarget:self action:@selector(doneAction:)];
    [bview addSubview:atextfield];

    self.dic=[[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"",@"appearance",@"",@"character",@"",@"gender",@"",@"nickname",@"",@"other",@"",@"property",@"",@"roletitle",@"",@"roleid",@"",@"favourite",@"",@"portrait", nil]mutableCopy];
    
    aview.hidden=YES;
    bview.hidden=YES;

    [self getPickerData];
    
    //获取制定用户在群里的角色设定信息
    [self GetStoryPersonInfo];
    
}

-(void)GetStoryPersonInfo{
    
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"pid":userid,@"token":@"110",@"storyid":self.theatreid};
    [HttpTool postWithPath:@"GetStoryPersonInfo" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
            
            self.dic=[[JSON objectForKey:@"personinfo"] mutableCopy];
            
            NSString *portrait=[self.dic objectForKey:@"portrait"];
            if (!portrait || [portrait isEqual:[NSNull null]] ||[portrait isEqualToString:@""]) {
                [self.dic setObject:@"image0" forKey:@"portrait"];
                portrait=[self.dic objectForKey:@"portrait"];
            }
            NSInteger i =[[portrait substringFromIndex:5]integerValue];
            float imgwidth=ScreenWidth;
            float imgheight=imgwidth*942/640.0;
            [self.scrollView scrollRectToVisible:CGRectMake(imgwidth*i, ScreenHeight-64-imgheight, imgwidth, imgheight) animated:NO];

        }else{
            
        }
    } failure:^(NSError *error) {
        
        
    }];
    
    
}

- (void)getPickerData {
    
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary *params = @{@"uid":userid,@"storyid":self.theatreid,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetStoryRoles" params:params success:^(id JSON) {
        
        self.cityArray=[[NSMutableArray alloc]init];
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            self.cityArray=[JSON objectForKey:@"roles"];
            
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];
    
    
}



- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView==self.scrollView) {
        int index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
        
        self.pageControl.currentPage = index;
        
        
        [self.dic setValue:[NSString stringWithFormat:@"image%d",index] forKey:@"portrait"];
        
    }
    
    
    
    
}

- (void)SelectButton:(UIButton *)btn
{
    
    NSInteger i=btn.tag-10000;
    currentIndex=i;
    if (i==0) {
        aview.hidden=YES;
        bview.hidden=NO;
        self.chatTextField.placeholder = @"此群中职位表";
        
        self.chatTextField.text=[self.dic objectForKey:@"roletitle"];
        
        NSMutableArray *aArray=[[NSMutableArray alloc]init];
        for (int i=0; i<self.cityArray.count; i++) {
            NSString *title=[[self.cityArray objectAtIndex:i]objectForKey:@"title"];
            [aArray addObject:title];
        }
        if (aArray.count!=0) {
            self.chatTextField.itemList=aArray;
        }else{
        
            self.chatTextField.itemList=[[NSMutableArray alloc]init];
        }
        
    }else if (i==1) {
        aview.hidden=NO;
        bview.hidden=YES;
        self.chatInputTextView.placeholder = @"输入你在此群中所使用的名字";
        
        self.chatInputTextView.text=[self.dic objectForKey:@"nickname"];
        
    }else if (i==2) {
        aview.hidden=YES;
        bview.hidden=NO;
        self.chatTextField.placeholder = @"戳我输入你在这里的性别";
        
        if ([[self.dic objectForKey:@"gender"]isEqualToString:@"1"]) {
            self.chatTextField.text=@"男";
        } else {
            self.chatTextField.text=@"女";
        }
        
        self.chatTextField.itemList=[[NSMutableArray alloc]initWithObjects:@"男",@"女", nil];
       
    }else if (i==3) {
        aview.hidden=NO;
        bview.hidden=YES;
        self.chatInputTextView.placeholder = @"例如：攻/受/点五";
        
        self.chatInputTextView.text=[self.dic objectForKey:@"property"];
        
    }else if (i==4) {
        aview.hidden=NO;
        bview.hidden=YES;
        self.chatInputTextView.placeholder = @"请输入您的喜好或者习惯";
        
        self.chatInputTextView.text=[self.dic objectForKey:@"favourite"];
        
    }else if (i==5) {
        aview.hidden=NO;
        bview.hidden=YES;
        self.chatInputTextView.placeholder = @"请详细描述您所想要表现的性格";
        
        self.chatInputTextView.text=[self.dic objectForKey:@"character"];
        
        
    }else if (i==6) {
        aview.hidden=NO;
        bview.hidden=YES;
        self.chatInputTextView.placeholder = @"请详细描述您的身高，身形，相貌和穿着";
        
        self.chatInputTextView.text=[self.dic objectForKey:@"appearance"];
        
    }else if (i==7) {
        aview.hidden=NO;
        bview.hidden=YES;
        self.chatInputTextView.placeholder = @"如果您有特别注意事项请填写在这里";

        self.chatInputTextView.text=[self.dic objectForKey:@"other"];
        
    }

}


-(void)doneAction:(UIBarButtonItem*)barButton
{
    //doneAction

    
    if (currentIndex==0) {
        [self.chatTextField resignFirstResponder];
        bview.hidden=YES;
        
        [self.dic setValue:self.chatTextField.text forKey:@"roletitle"];
        
        NSString * roleid;
        if (self.chatTextField.selectedRow==-1) {
            roleid=[self.dic objectForKey:@"roleid"];
            
        } else {
            roleid=[[self.cityArray objectAtIndex:self.chatTextField.selectedRow]objectForKey:@"id"];
            
        }
        [self.dic setValue:roleid forKey:@"roleid"];

        
    }else if (currentIndex==1) {
        [self.chatInputTextView resignFirstResponder];
        aview.hidden=YES;
        
        [self.dic setValue:self.chatInputTextView.text forKey:@"nickname"];
        
    }else if (currentIndex==2) {
        [self.chatTextField resignFirstResponder];
        bview.hidden=YES;
        
        NSString *gender;
        if ([self.chatTextField.text isEqualToString:@
             "男"]) {
            gender=@"1";
        } else {
            gender=@"0";
        }

        [self.dic setValue:gender forKey:@"gender"];
        
    }else if (currentIndex==3) {
        [self.chatInputTextView resignFirstResponder];
        aview.hidden=YES;
        
        [self.dic setValue:self.chatInputTextView.text forKey:@"property"];
        
    }else if (currentIndex==4) {
        [self.chatInputTextView resignFirstResponder];
        aview.hidden=YES;
        
        [self.dic setValue:self.chatInputTextView.text forKey:@"favourite"];
        
    }else if (currentIndex==5) {
        [self.chatInputTextView resignFirstResponder];
        aview.hidden=YES;
        
        [self.dic setValue:self.chatInputTextView.text forKey:@"character"];
        
    }else if (currentIndex==6) {
        [self.chatInputTextView resignFirstResponder];
        aview.hidden=YES;
        
        
        [self.dic setValue:self.chatInputTextView.text forKey:@"appearance"];
        
    }else if (currentIndex==7) {
        [self.chatInputTextView resignFirstResponder];
        aview.hidden=YES;
        
        [self.dic setValue:self.chatInputTextView.text forKey:@"other"];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
