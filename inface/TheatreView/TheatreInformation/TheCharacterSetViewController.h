//
//  TheCharacterSetViewController.h
//  inface
//
//  Created by Mac on 15/4/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//人物设置
#import "BaseViewController.h"
#import "CharacterSetTextFieldTableViewCell.h"
#import "CharacterSetTextViewTableViewCell.h"
#import "IQDropDownTextField.h"
#import "IQDropDownTextFieldTableViewCell.h"
@interface TheCharacterSetViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIActionSheetDelegate,UIPickerViewDataSource, UIPickerViewDelegate,IQDropDownTextFieldDelegate>
{
    IQDropDownTextField *massTextField;//皮表
    UITextField *nameTextField;//姓名
    IQDropDownTextField *sexTextField;//性别
    UITextField *attributeTextField;//属性
    UITextField *characterTextField;//性格
    PlaceholderTextView *appearanceTextView;//外貌
    PlaceholderTextView *otherTextView;//其他
    
    BOOL _wasKeyboardManagerEnabled;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableDictionary *dic;

@property (strong, nonatomic) NSMutableArray *cityArray;
@property(nonatomic ,strong)NSString *theatreid;
@property (strong,nonatomic) NSString *theatreGroupid;

@end
