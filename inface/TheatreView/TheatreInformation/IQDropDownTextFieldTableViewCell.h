//
//  IQDropDownTextFieldTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/6/3.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IQDropDownTextFieldTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet IQDropDownTextField *UserDetailTextField;//详情
@end
