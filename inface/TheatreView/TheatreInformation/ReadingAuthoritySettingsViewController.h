//
//  ReadingAuthoritySettingsViewController.h
//  inface
//
//  Created by Mac on 15/5/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//阅读权限设置
typedef void(^ChangeReadAutority)(NSString * readAoturity);
#import "BaseViewController.h"
#import "ReadingAuthoritySettingsTableViewCell.h"

@interface ReadingAuthoritySettingsViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableDataFriends;
@property(nonatomic ,strong)NSString *theatreid;
@property (strong,nonatomic) NSString *theatreGroupid;
@property (strong,nonatomic) NSString *permission;
@property(assign,nonatomic)    BOOL   isAnimating;

-(void)changeReadPermisson:(ChangeReadAutority)block;
@end
