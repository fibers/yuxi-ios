//
//  TheatreIDTableViewCell.h
//  inface
//
//  Created by Mac on 15/5/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧群cell ID
#import <UIKit/UIKit.h>

@interface TheatreIDTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UILabel *UserDetailLabel;//详情
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *UserDetailLabelWidth;

@property (strong, nonatomic) IBOutlet UIImageView *ArrowImage;//箭头
@end
