//
//  CharacterSetTextFieldTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//人物设置TextField
#import <UIKit/UIKit.h>

@interface CharacterSetTextFieldTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UITextField *UserDetailTextField;//详情
@end
