//
//  EditTextfieldTheatreViewController.h
//  inface
//
//  Created by Mac on 15/5/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//修改剧群名称
#import "BaseViewController.h"

@interface EditTextfieldTheatreViewController : BaseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property (strong,nonatomic) NSString *theatreGroupid;
@property (strong,nonatomic) NSString *content;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
