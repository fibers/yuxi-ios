//
//  EditTextViewTheatreViewController.h
//  inface
//
//  Created by Mac on 15/5/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//修改剧群简介
#import "BaseViewController.h"

@interface EditTextViewTheatreViewController : BaseViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (strong,nonatomic)NSString *theatreGroupid;
@property (strong,nonatomic) NSString *content;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
