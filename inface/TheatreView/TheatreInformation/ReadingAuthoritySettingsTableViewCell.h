//
//  ReadingAuthoritySettingsTableViewCell.h
//  inface
//
//  Created by Mac on 15/5/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadingAuthoritySettingsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UIButton *SelectButton;//选择按钮
@end
