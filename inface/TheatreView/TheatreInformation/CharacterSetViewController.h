//
//  CharacterSetViewController.h
//  inface
//
//  Created by huangzengsong on 15/6/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "IQDropDownTextField.h"
@interface CharacterSetViewController : BaseViewController<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,IQDropDownTextFieldDelegate>
{

    NSInteger currentIndex;
    UIView *aview;//chatInputTextView背景
    UIView *bview;//chatTextField背景
}
@property (nonatomic ,retain) UIScrollView * scrollView;
@property (nonatomic ,retain) UIPageControl * pageControl;
@property(retain,nonatomic)NSMutableArray *imgs;

@property (weak, nonatomic)  PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic)  IQDropDownTextField *chatTextField;
@property(nonatomic ,strong)NSString *theatreid;
@property (strong,nonatomic) NSString *theatreGroupid;

@property (strong, nonatomic) NSMutableDictionary *dic;

@property (strong, nonatomic) NSMutableArray *cityArray;

@end
