//
//  Article.h
//  inface
//
//  Created by 邢程 on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Content;
@interface Article : NSObject

@property (nonatomic, copy) NSString *result;

@property (nonatomic, strong) NSMutableArray *contentlist;

@property (nonatomic, copy) NSString *totalcontens;

@property (nonatomic, copy) NSString *position;

@property (nonatomic, copy) NSString *direction;

@end

