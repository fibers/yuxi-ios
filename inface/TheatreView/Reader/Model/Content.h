//
//  Content.h
//  inface
//
//  Created by 邢程 on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Content : NSObject

@property (nonatomic, copy) NSString *msgid;

@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) NSString *groupid;

@property (nonatomic, copy) NSString *senderid;

@property (nonatomic, copy) NSString *senderrolename;

@property (nonatomic, copy) NSString *updatetime;

@property (nonatomic, copy) NSString *sendername;

@end
