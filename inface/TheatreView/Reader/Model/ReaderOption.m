//
//  ReaderOption.m
//  inface
//
//  Created by 邢程 on 15/9/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ReaderOption.h"
@interface ReaderOption()
@end
@implementation ReaderOption
+(instancetype)shareInstance{
    static ReaderOption * option;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        option = [[ReaderOption alloc]init];
    });
    return option;

}

-(UIColor *)bgColor{
    return _bgColor == nil?[UIColor whiteColor]:_bgColor;
}
-(UIColor *)titleFontColor{
    return _titleFontColor == nil?[UIColor colorWithHexString:@"949494"]:_titleFontColor;
}
-(UIColor *)contentFontColor{
    return _contentFontColor == nil?[UIColor colorWithHexString:@"4a4a4a"]:_contentFontColor;
}
@end
