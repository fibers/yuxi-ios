//
//  ReaderCellFrame.m
//  inface
//
//  Created by 邢程 on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ReaderCellFrame.h"
#import "DDMessageEntity.h"
#define MarginLeft 20.0f
#define MarginTop 12.0f
#import <TTTAttributedLabel.h>
@interface ReaderCellFrame()

@end
@implementation ReaderCellFrame
-(void)initReaderCellFrameWithContent:(Content*)content{
    CGFloat width = ScreenWidth - MarginLeft *2;
//    CGSize titleSizeTMP = [content.sendername sizeWithAttributes:@{[UIFont systemFontOfSize:12]:NSFontAttributeName}];
    _titleF = CGRectMake(MarginLeft, MarginTop, ScreenWidth - MarginLeft, 12);
    NSString * contentTMP = [DDMessageEntity makeContentText:content.content];
     contentTMP = (contentTMP == nil) ?@" ":contentTMP;
    if ( [self isImageContentType:contentTMP]) {//如果是图片
        _imgVF = CGRectMake(_titleF.origin.x, CGRectGetMaxY(_titleF)+MarginTop, width, width);
        self.cellH = CGRectGetMaxY(self.imgVF)+MarginTop;
    }else{
        if ([contentTMP isKindOfClass:[NSString class]] || [contentTMP isKindOfClass:[NSMutableString class]]) {
            
            
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:contentTMP];
            NSMutableParagraphStyle *paragrapStyle = [[NSMutableParagraphStyle alloc] init];
            [paragrapStyle setLineSpacing:8];
            [attrString addAttribute:NSParagraphStyleAttributeName value:paragrapStyle range:NSMakeRange(0, contentTMP.length)];
            [attrString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, contentTMP.length)];
//            CGFloat height = [contentTMP boundingRectWithSize:CGSizeMake(width, MAXFLOAT)  options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17],NSParagraphStyleAttributeName:paragrapStyle
//} context:nil].size.height;
            CGFloat height = [attrString boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading  context:nil].size.height;
            self.contentF = CGRectMake(_titleF.origin.x, CGRectGetMaxY(_titleF)+MarginTop, width,height);
            self.cellH = CGRectGetMaxY(self.contentF)+MarginTop;
        }
    }
   
    
    
}


-(BOOL)isImageContentType:(NSString*)str{
    NSRange  range1 = [str rangeOfString:@"&$#@~^"];
    NSRange  range2 = [str rangeOfString:@"&$~@#@"];
    if (range1.length > 0 && range2.length >0) {
        return YES;
    }else{
        return NO;
    }

}

@end
