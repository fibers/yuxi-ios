//
//  ReaderCellFrame.h
//  inface
//
//  Created by 邢程 on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Content.h"
@interface ReaderCellFrame : NSObject
@property (nonatomic,assign) CGRect  titleF;
@property (nonatomic,assign) CGRect  contentF;
@property (nonatomic,assign) CGRect  imgVF;
@property (nonatomic,assign) CGFloat cellH;
-(void)initReaderCellFrameWithContent:(Content*)content;
@end
