//
//  ReaderOption.h
//  inface
//
//  Created by 邢程 on 15/9/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReaderOption : NSObject
///阅读器背景颜色
@property(strong,nonatomic)UIColor *bgColor;
///阅读器文章item标题颜色
@property(strong,nonatomic)UIColor *titleFontColor;
///阅读器文章content颜色
@property(strong,nonatomic)UIColor *contentFontColor;
///阅读器底部进度条背景颜色
@property(strong,nonatomic)UIColor *progressBarBGColor;
@property(assign,nonatomic,getter=isNightModel)BOOL nightModel;

+(instancetype)shareInstance;
@end
