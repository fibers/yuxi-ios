//
//  Article.m
//  inface
//
//  Created by 邢程 on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "Article.h"
#import "Content.h"
#import "MJExtension.h"
@implementation Article
MJExtensionCodingImplementation

+ (NSDictionary *)objectClassInArray{
    return @{@"contentlist" : [Content class]};
}
@end


