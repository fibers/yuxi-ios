//
//  YXReaderViewController.m
//  inface
//
//  Created by 邢程 on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXReaderViewController.h"
#import "Article.h"
#import "Content.h"
#import "ReaderCellFrame.h"
#import "MJExtension.h"
#import "MJRefresh.h"
#import "YXReaderCell.h"
#import "YXAtricleProgressView.h"
#import "MBProgressHUD+YXStyle.h"
#import "YXSlider.h"
#import "FMDB.h"
#import "Masonry.h"
#import "ReaderOption.h"
#import <IDMPhoto.h>
#import <IDMPhotoBrowser.h>
///获取stroy uri
#define Story_URI @"GetStoryContent"
///默认每页显示条数
#define Default_Page_Count @"100"

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
@interface YXReaderViewController ()<UITableViewDelegate,UITableViewDataSource,YXAtricleProgressViewDelegate,YXReaderCellDelegate>
@property (nonatomic,strong) UITableView           * contentTableView;
@property (nonatomic,copy  ) NSString              * storyId;
@property (nonatomic,strong) Article               * article;
@property (nonatomic,strong) YXAtricleProgressView * progressV;
@property (nonatomic,copy  ) NSString              * navTitleStr;
@property (nonatomic,assign) NSInteger             progress;
@property (nonatomic,assign) NSInteger             rowTMP;
@property (nonatomic,strong) ReaderOption          * readerOption;
@end

@implementation YXReaderViewController
-(instancetype)init{
    if (self = [super init]) {
        _progress = 0;
        [self setHidesBottomBarWhenPushed:YES];
        [self createDB];
    }
    return self;
}


-(void)setNavTitle:(NSString *)navTitle{
    _navTitleStr = navTitle;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat currentY = scrollView.contentOffset.y;
    NSIndexPath * indexPath = [self.contentTableView indexPathForRowAtPoint:CGPointMake(ScreenWidth*0.5, currentY+ScreenHeight-64-44)];
    if(indexPath){
        if (_progress != indexPath.row) {
            _progress = indexPath.row;
            [self updateProgress:self.progress WithStoryID:self.storyId];
            [self.progressV setCurrentNum:indexPath.row+1];
        }
        
    }
}
#pragma mark -
#pragma mark view life cycle
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self nightModel];
    
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:self.navTitleStr];
//    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 44)];
//    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
//    [RightButton setShowsTouchWhenHighlighted:YES];
//    [RightButton setImage:[UIImage imageNamed:@"morebtn"] forState:UIControlStateNormal];
//    [RightButton setImage:[UIImage imageNamed:@"morebtnselect"] forState:UIControlStateHighlighted];
//    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
//    self.navigationItem.rightBarButtonItem = barback2;
    
    
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        Article * article = [self getArticleFromCacheWithStoryID:self.storyId];
        if ([article.contentlist count]!=0) {
            self.article = article;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self renderView];
                [self.progressV setTotalNum:[self.article.totalcontens integerValue]];
                [self.contentTableView reloadData];
                self.progress = [self getProgressWithStoryID:self.storyId];
                
                [self.contentTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.progress inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                [self.progressV setCurrentNum:self.progress+1];
            });
            
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
            //        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self loadDataWithDirection:nil Count:nil StoryID:self.storyId frommsgID:nil andUID:nil];
            });
        }
    });
 
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBarTintColor:BACKGROUND_COLOR];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)returnBackr
{
    
//    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
        NSArray *menuItems =
        @[
          [KxMenuItem menuItem:@"分享"
                         image:[UIImage imageNamed:@"编辑剧大纲"]
                        target:self
                        action:@selector(shareStory:)]

          ];
        
        

        KxMenuItem *first = menuItems[0];
        //first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
        first.alignment = NSTextAlignmentLeft;
        
        [KxMenu showMenuInView:self.navigationController.view
                      fromRect:CGRectMake(ScreenWidth-45, 10, 25, 44)
                     menuItems:menuItems];

    
}


#pragma mark 分享自戏，暂时给的是百度网址链接
-(void)shareStory:(UIButton *)sender
{
    ShareModel * model = [ShareModel sharedInstance];
    if (self.storyDic) {
        [model shareToThird :nil dic:self.storyDic];
 
    }
}


-(void)renderView{
    [self.view addSubview:self.contentTableView];
    [self.progressV setBackgroundColor:[UIColor redColor]];
   
    [self.view addSubview:self.progressV];
    WS(ws);
    [_progressV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, 50));
        make.bottom.equalTo(ws.view.mas_bottom).with.offset(0);
        make.left.equalTo(ws.view.mas_left).with.offset(0);
        make.right.equalTo(ws.view.mas_right).with.offset(0);
    }];
}
#pragma mark -
#pragma mark tableView Delegate&DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.article.contentlist count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Content * content       = [self.article.contentlist objectAtIndex:indexPath.row];
    ReaderCellFrame * cellF = [[ReaderCellFrame alloc]init];
    [cellF initReaderCellFrameWithContent:content];
    return cellF.cellH;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Content * content       = [self.article.contentlist objectAtIndex:indexPath.row];
    ReaderCellFrame * cellF = [[ReaderCellFrame alloc]init];
    [cellF initReaderCellFrameWithContent:content];
    YXReaderCell * cell = [tableView dequeueReusableCellWithIdentifier:@"YXReaderTableViewCellID"];
    if (!cell) {
        cell = [[YXReaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"YXReaderTableViewCellID"];
    }
    [cell setFrameWithReaderCellFrame:cellF];
    [cell setContentWith:content];
    cell.delegate = self;
    return cell;
}

#pragma mark 夜间模式

-(void)changeNightModel{
    self.readerOption.nightModel = !self.readerOption.isNightModel;
    [self nightModel];
}
-(void)nightModel{
    NSString * nightModelKey = @"isNightModelShowed";
    if (self.readerOption.isNightModel) {
        if ([NSNumber numberWithBool:YES] != [[NSUserDefaults standardUserDefaults]objectForKey:nightModelKey]) {
            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:YES] forKey:nightModelKey];
            UIAlertView *alerV = [[UIAlertView alloc]initWithTitle:@"嘘" message:@"好好看！乱戳什么！o_O???\n三次点击开启/关闭夜间模式" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alerV show];
        }
        
        self.readerOption.bgColor = [UIColor blackColor];
        self.readerOption.titleFontColor = [UIColor whiteColor];
        self.readerOption.contentFontColor = [UIColor whiteColor];
        [self.progressV setBGcolor:[UIColor blackColor]];
        [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
        [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
        
    }else{
        self.readerOption.bgColor = [UIColor whiteColor];
        self.readerOption.titleFontColor = [UIColor colorWithHexString:@"949494"];
        self.readerOption.contentFontColor = [UIColor colorWithHexString:@"4a4a4a"];
        [self.progressV setBGcolor:COLOR(245, 245, 245, 1)];
        [self.navigationController.navigationBar setBarTintColor:BACKGROUND_COLOR];
        [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
    
    }
    [self.contentTableView setBackgroundColor:self.readerOption.bgColor];
    [self.contentTableView reloadData];
   

}
-(void)yxReaderCellImageViewClickedWithImage:(id)image imageV:(UIImageView *)imageV andImageType:(YXReaderCellImageType)type{
    IDMPhoto * photo;
    if (type == YXReaderCellImageTypeObj) {
        photo = [IDMPhoto  photoWithImage:image];
        
    }else{
        photo = [IDMPhoto photoWithURL:[NSURL URLWithString:image]];
    }
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:@[photo] animatedFromView:imageV];
    [self presentViewController:browser animated:YES completion:nil];
}
#pragma mark -
#pragma mark 数据初始化
-(ReaderOption *)readerOption{
    return [ReaderOption shareInstance];
}
-(UITableView *)contentTableView{
    if (!_contentTableView) {
        _contentTableView            = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
        _contentTableView.delegate   = self;
        _contentTableView.dataSource = self;
        [_contentTableView setAllowsSelection:NO];
        [_contentTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_contentTableView setContentInset:UIEdgeInsetsMake(0, 0, 40, 0)];
        [_contentTableView addFooterWithTarget:self action:@selector(tableLoadMoreData)];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeNightModel)];
        [tap setNumberOfTapsRequired:3];
        [_contentTableView addGestureRecognizer:tap];
        
    }
    return _contentTableView;
}
-(void)storyId:(NSString*)storyId{
    _storyId = storyId;
}
-(YXAtricleProgressView *)progressV{
    if (!_progressV) {
        _progressV = [[[NSBundle mainBundle]loadNibNamed:@"YXAtricleProgressView" owner:nil options:nil]lastObject];
        _progressV.delegate = self;
    }
    return _progressV;
}
#pragma mark -
#pragma mark 数据加载
/**
 *  加载文章的数据
 *
 *  @param direction (默认:0) 0: 取更新的数据。也就是大于frommsgid的数据。1；取更旧的数据，也就是小于frommsgid的数据
 *  @param count     (默认:20)每次取数据的个数。
 *  @param storyid   剧的id
 *  @param frommsgid (默认:0)本地存储的剧的内容的id。第一次为0。 以后每次获取更新的内容。当需要取更新的数据，为本地缓存数据中的最大内容id。当需要取更老的数据，取本地缓冲数据中，最小的内容id。
 *  @param uid       用户id。
 */
-(void)loadDataWithDirection:(NSString*)direction Count:(NSString*)count StoryID:(NSString*)storyid frommsgID:(NSString*)frommsgid andUID:(NSString *)uid{
    //初始化默认值
    direction = [self isEmptyString:direction]?@"0":direction;
    count     = [self isEmptyString:count]?Default_Page_Count:count;
    frommsgid = [self isEmptyString:frommsgid]?@"0":frommsgid;
    uid       = [self isEmptyString:uid]?USERID:uid;
    NSDictionary * paramsDic = @{@"count":count,
                                 @"direction":direction,
                                 @"frommsgid":frommsgid,
                                 @"storyid":storyid,
                                 @"uid":uid
                                 };
    [HttpTool postWithPath:Story_URI params:paramsDic success:^(id JSON) {
        if ([direction isEqualToString:@"0"]) {
            if(self.article){
                Article * newArticleTMP = [Article objectWithKeyValues:JSON];
                [self.article.contentlist addObjectsFromArray:newArticleTMP.contentlist];
            }else{ //第一次加载
                self.article = [Article objectWithKeyValues:JSON];
                [self.progressV setTotalNum:[self.article.totalcontens integerValue]];
                [self renderView];
            }
            if (self.contentTableView.footerRefreshing) {
                [self.contentTableView footerEndRefreshing];
            }
        }else{
            Article * newArticleTMP = [Article objectWithKeyValues:JSON];
            [self.article.contentlist addObjectsFromArray:newArticleTMP.contentlist];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.contentTableView reloadData];
        [self cacheDataWithStoryID:self.storyId progress:self.progress andData:[NSKeyedArchiver archivedDataWithRootObject:self.article]];
        
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];
   
}
/**
 *  加载更多数据
 */
-(void)tableLoadMoreData{
    Content * lastContent =  [self.article.contentlist lastObject];
    LOG(@"%@",lastContent.msgid);
    [self loadDataWithDirection:@"0" Count:nil StoryID:self.storyId frommsgID:lastContent.msgid andUID:nil];

}

#pragma mark -
#pragma mark 工具方法
/**
 *  是否为@""或者nil字符串
 *
 *  @param str 需要判断的字符串
 *
 *  @return YES 为空字符串 , NO 为非空字符串
 */
-(BOOL)isEmptyString:(NSString*)str{
    return ([@""isEqualToString:str]||nil == str);
}

#pragma mark -
#pragma mark 响应事件 以及代理方法实现
-(void)progressViewTouchUpInside:(YXSlider *)view{
    LOG(@"totle : %f ,now : %f",view.maximumValue,view.value);
    NSInteger currentCount = [self.article.contentlist count]-1;
    NSInteger newValue = view.value;
    newValue = newValue < 0?0:newValue;
    NSIndexPath *indexPath;
    if (newValue > currentCount) {
        indexPath = [NSIndexPath indexPathForRow:currentCount inSection:0];
        [self.progressV setCurrentNum:currentCount];
    }else{
        indexPath = [NSIndexPath indexPathForRow:newValue inSection:0];
    }
    [self.contentTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    self.progress = view.value;
}
-(void)RecommendAction{
}
#pragma mark -
#pragma mark 缓存数据
-(void)cacheDataWithStoryID:(NSString *)storyID progress:(NSInteger)progress andData:(NSData *)data{
    FMDatabase* db = [FMDatabase databaseWithPath:ReaderCacheFile];
    [db open];
    FMResultSet * resultSet = [db executeQuery:@"select count(*) as total_count from article_simple where StoryId = ?",storyID];
    int count = 0;
    while ([resultSet next]) {
       count =  [resultSet intForColumn:@"total_count"];
    }
    
    if(count == 1){
        [db executeUpdate:@"update  article_simple set Progress = ? , Model = ? where StoryId = ?",[NSNumber numberWithInteger:self.progress],data,storyID];
    }else if(count > 1){
        [db executeUpdate:@"delete from article_simple where StoryId = ?",storyID];
        count = 0;
    
    }
    if(count == 0){
        [db executeUpdate:@"insert into article_simple ( \"Progress\", \"Model\",\"StoryId\") values (?,?,?)",[NSNumber numberWithInteger:progress],data,storyID];
    }
    [db close];
}
-(void)updateProgress:(NSInteger)progress WithStoryID:(NSString*)storyID{
    FMDatabase* db = [FMDatabase databaseWithPath:ReaderCacheFile];
    [db open];
    [db executeUpdate:@"update article_simple set Progress = ? where StoryId = ?",[NSNumber numberWithInteger:progress],storyID];
    [db close];
}
-(NSInteger)getProgressWithStoryID:(NSString*)storyID{
    FMDatabase* db = [FMDatabase databaseWithPath:ReaderCacheFile];
    [db open];
    FMResultSet *resultSet = [db executeQuery:@"select Progress from article_simple where StoryID = ?",storyID];
    NSInteger tmp = 0;
    while ([resultSet next]) {
        tmp = [resultSet intForColumn:@"Progress"];
    }
    [db close];
    return tmp;
}
-(Article*)getArticleFromCacheWithStoryID:(NSString * )storyID{
    FMDatabase* db = [FMDatabase databaseWithPath:ReaderCacheFile];
    [db open];
    FMResultSet * resultSet = [db executeQuery:@"select Model from article_simple where StoryId = ?",storyID];
    NSData * data;
    while ([resultSet next]) {
         data= [resultSet dataForColumn:@"Model"];
    }
    [db close];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}
#pragma mark 创建数据库
-(void)createDB{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        FMDatabase* db = [FMDatabase databaseWithPath:ReaderCacheFile];
        [db open];
        NSString *sql = @"CREATE TABLE \"article_simple\" (\n\t \"id\" integer NOT NULL PRIMARY KEY AUTOINCREMENT,\n\t \"StoryID\" text,\n\t \"Model\" blob,\n     \"Progress\" integer\n);";
        [db executeStatements:sql];
    });
}
@end


