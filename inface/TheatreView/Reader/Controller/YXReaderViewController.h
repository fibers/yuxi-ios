//
//  YXReaderViewController.h
//  inface
//
//  Created by 邢程 on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface YXReaderViewController : BaseViewController
@property(strong,nonatomic)NSDictionary * storyDic;
@property(assign,nonatomic)    BOOL   isAnimating;

-(void)storyId:(NSString*)storyId;
-(void)setNavTitle:(NSString *)navTitle;
@end
