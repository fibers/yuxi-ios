//
//  YXAtricleProgressView.m
//  inface
//
//  Created by 邢程 on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXAtricleProgressView.h"
#import "YXSlider.h"
@interface YXAtricleProgressView()
@property (weak, nonatomic  ) IBOutlet UIView             *bgV;
@property (weak, nonatomic  ) IBOutlet UILabel            *numL;
@property (weak, nonatomic  ) IBOutlet NSLayoutConstraint *numLCW;
@property (weak, nonatomic  ) IBOutlet YXSlider           *progressBarV;
@property (assign, nonatomic) NSInteger                   total;
@property (assign, nonatomic) NSInteger                   current;
@end
@implementation YXAtricleProgressView
-(instancetype)init{
    if(self = [super init]){
       
    }
    return self;
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
}
-(void)setCurrentNum:(NSInteger)current{
    _current = current;
    [self setProgressViewWithCurrentNum:_current andTotalNum:_total];
}
-(void)setTotalNum:(NSInteger)total{
    _total = total;
    [self setProgressViewWithCurrentNum:_current andTotalNum:_total];
}
-(void)setProgressViewWithCurrentNum:(NSInteger)currentNum andTotalNum:(NSInteger)totalNum{
    if (currentNum > totalNum) {
        return;
    }
    NSString * numStr = [NSString stringWithFormat:@"%ld/%ld",currentNum,totalNum];
    NSString * fullNumStr = [NSString stringWithFormat:@"%ld/%ld",totalNum,totalNum];
    NSMutableAttributedString *strAM = [[NSMutableAttributedString alloc]initWithString:numStr];
    [strAM addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"f06fa8"] range:[numStr rangeOfString:[NSString stringWithFormat:@"%ld",currentNum]]];
    [self.numL setAttributedText:strAM];
    CGSize currentLS = [fullNumStr sizeWithAttributes:@{[UIFont systemFontOfSize:12]:NSFontAttributeName}];
    self.numLCW.constant = currentLS.width+15;
    [self.progressBarV setMinimumValue:0];
    [self.progressBarV setMaximumValue:totalNum];
    [self.progressBarV setValue:currentNum animated:YES];
}
-(void)progressBarVDidChange:(YXSlider*)sender{
    if (sender.value == sender.maximumValue) {
        return;
    }
    [self setCurrentNum:sender.value];
    if ([self.delegate respondsToSelector:@selector(progressViewTouchUpInside:)]) {
        [self.delegate progressViewTouchUpInside:sender];
    }
}
-(void)awakeFromNib{
    [self.progressBarV setThumbImage:[UIImage imageNamed:@"slider_icon"] forState:UIControlStateNormal];
     [self.progressBarV addTarget:self action:@selector(progressBarVDidChange:) forControlEvents:UIControlEventValueChanged];
    
}
-(void)setBGcolor:(UIColor*)color{
    [self.bgV setBackgroundColor:color];
}
@end
