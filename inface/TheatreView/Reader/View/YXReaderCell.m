//
//  YXReaderCell.m
//  inface
//
//  Created by 邢程 on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXReaderCell.h"
#import "Content.h"
#import "DDMessageEntity.h"
#import "UILabel+Copyable.h"
#import "ReaderOption.h"
#import "NSString+YXExtention.h"
#import <TTTAttributedLabel.h>
@interface YXReaderCell()
@property (nonatomic,strong) UILabel            * titleL;
@property (nonatomic,strong) UILabel * contentL;
@property (nonatomic,strong) UIImageView        * imgV;
@property (nonatomic,strong) ReaderOption       * readerOption;
@property (nonatomic,copy  ) NSString           * imageUrl;
@property (nonatomic,assign,getter = isImageVeiwDidLoadImage) BOOL imageViewDidLoadImage;
@end
@implementation YXReaderCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier]){
        _imgV = [[UIImageView alloc]init];
        [self.contentView addSubview:self.titleL];
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}
- (void)awakeFromNib {
    
}
-(UIImageView *)imgV{
    if (!_imgV) {
        _imgV = [[UIImageView alloc]init];
        
    }
    return _imgV;
}
-(UILabel *)contentL{
    if (!_contentL) {
        _contentL = [[UILabel alloc]initWithFrame:CGRectZero];
        _contentL.numberOfLines = 0;
//        _contentL.lineSpacing = 8;z
        [_contentL setTextColor:self.readerOption.contentFontColor];
        [_contentL setFont:[UIFont systemFontOfSize:17]];
        [_contentL setCopyingEnabled:YES];
        [_contentL setLineBreakMode:NSLineBreakByWordWrapping];
        [_contentL setShouldUseLongPressGestureRecognizer:YES];
    }
    return _contentL;
}
-(UILabel *)titleL{
    if (!_titleL) {
        _titleL = [[UILabel alloc]init];
        [_titleL setTextColor:self.readerOption.titleFontColor];
        [_titleL setFont:[UIFont systemFontOfSize:12.0f]];
    }
    return _titleL;
}

#pragma mark 需要判断下有无角色
-(void)setContentWith:(Content *)content{
    self.imageViewDidLoadImage = NO;
    NSString *titleStr = ([content.senderrolename isEqualToString:@""]||[content.senderrolename isEqualToString:@"[]"])?content.sendername:content.senderrolename;
    [self.titleL setText:titleStr];
    NSString * contentStr = [DDMessageEntity makeContentText:content.content];
    contentStr = (contentStr == nil) ?@" ":contentStr;
    if ([self isImageContentType:contentStr]) {
        [self.contentView addSubview:self.imgV];
        [self.imgV setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgVClicked)];
        [self.imgV addGestureRecognizer:tap];
        WS(weakSelf);
        self.imageUrl = [self getImgaeString:contentStr];
        [self.imgV sd_setImageWithURL:[NSURL URLWithString:[[self getImgaeString:contentStr] stringOfW900ImgUrl]]placeholderImage:[UIImage imageNamed:@"juxiangqing"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!error) {
                weakSelf.imageViewDidLoadImage = YES;
            }
        }];
        [_imgV setContentMode:UIViewContentModeScaleAspectFit];
    }else{
        [self.contentView addSubview:self.contentL];
        _contentL.numberOfLines = 0;
        [_contentL setTextColor:self.readerOption.contentFontColor];
        [_contentL setFont:[UIFont systemFontOfSize:18]];
        NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:contentStr];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
        [paragraphStyle setLineSpacing:8];
        [attStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, contentStr.length)];
        [self.contentL setAttributedText:attStr];
    }
    [self.contentL setTextColor:self.readerOption.contentFontColor];
    //     [self.contentView setBackgroundColor:self.readerOption.bgColor];
    [self.titleL setTextColor:self.readerOption.titleFontColor];
}
-(void)setFrameWithReaderCellFrame:(ReaderCellFrame*)frame{
    [self.titleL setFrame:frame.titleF];
    [self.contentL setFrame:frame.contentF];
    [self.imgV setFrame:frame.imgVF];
}

-(NSString*)getImgaeString:(NSString*)tmp{
    tmp =  [tmp stringByReplacingCharactersInRange:[tmp rangeOfString:@"&$#@~^@[{:"]  withString:@""];
    tmp =  [tmp stringByReplacingCharactersInRange:[tmp rangeOfString:@":}]&$~@#@"] withString:@""];
    return tmp;
}
-(BOOL)isImageContentType:(NSString*)str{
    //    NSString*urlRegex = @"^(&$#@~^).{10,}(&$~@#@)$";
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegex];
    //    BOOL isValid = [predicate evaluateWithObject:str];
    NSRange  range1 = [str rangeOfString:@"&$#@~^"];
    NSRange  range2 = [str rangeOfString:@"&$~@#@"];
    if (range1.length > 0 && range2.length >0) {
        return YES;
    }else{
        return NO;
    }
    
}
/**
 *  图片被点击
 */
-(void)imgVClicked{
    if([self.delegate respondsToSelector:@selector(yxReaderCellImageViewClickedWithImage:imageV:andImageType:)]){
        [self.delegate yxReaderCellImageViewClickedWithImage:self.isImageVeiwDidLoadImage?self.imgV.image:self.imageUrl imageV:self.imgV andImageType:self.isImageVeiwDidLoadImage?YXReaderCellImageTypeObj:YXReaderCellImageTypeUrl];
    }
}
-(void)setContentColor:(ReaderOption*)option{
    
}
-(ReaderOption *)readerOption{
    return [ReaderOption shareInstance];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:NO];
}

@end
