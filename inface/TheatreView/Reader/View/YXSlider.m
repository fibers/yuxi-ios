//
//  YXSlider.m
//  inface
//
//  Created by 邢程 on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXSlider.h"

@implementation YXSlider
-(CGRect)trackRectForBounds:(CGRect)bounds{
    return CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width, 7);
}
-(UIImage*)thumbImageForState:(UIControlState)state{
    return [UIImage imageNamed:@"slider_icon"];
}
//-(CGRect)thumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(float)value{
//    return CGRectMake(bounds.origin.x, bounds.origin.y, 14, 14);
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
