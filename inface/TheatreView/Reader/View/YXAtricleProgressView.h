//
//  YXAtricleProgressView.h
//  inface
//
//  Created by 邢程 on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YXSlider;
@protocol YXAtricleProgressViewDelegate<NSObject>
@optional
-(void)progressViewTouchUpInside:(YXSlider*)view;
@end

@interface YXAtricleProgressView : UIView

-(void)setProgressViewWithCurrentNum:(NSInteger)currentNum andTotalNum:(NSInteger)totalNum;
-(void)setTotalNum:(NSInteger)total;
-(void)setCurrentNum:(NSInteger)current;
-(void)setBGcolor:(UIColor*)color;
@property(weak,nonatomic)id<YXAtricleProgressViewDelegate>delegate;
@end
