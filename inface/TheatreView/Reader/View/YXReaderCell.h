//
//  YXReaderCell.h
//  inface
//
//  Created by 邢程 on 15/8/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderCellFrame.h"
@class Content,ReaderOption;
/**
 *  阅读器中image的类型
 */
typedef NS_ENUM(NSInteger, YXReaderCellImageType){
    /**
     *  url str
     */
    YXReaderCellImageTypeUrl,
    /**
     *  UIImageView
     */
    YXReaderCellImageTypeObj,
};
@protocol YXReaderCellDelegate <NSObject>
@optional
/**
 *  cell中的图片被点击
 *
 *  @param image 被点击的图片的url(未加载完成的时候)或者被点击图片的UIImage(完成加载的时候)
 *  @param iamgeV 被点击图片的UIImageView
 *  @param type  图片的类型，url string，或者是imageView
 */
-(void)yxReaderCellImageViewClickedWithImage:(id)image imageV:(UIImageView*)imageV andImageType:(YXReaderCellImageType)type;
@end
@interface YXReaderCell : UITableViewCell
@property(nonatomic,weak)id<YXReaderCellDelegate> delegate;
-(void)setFrameWithReaderCellFrame:(ReaderCellFrame*)frame;
-(void)setContentWith:(Content *)content;
@end
