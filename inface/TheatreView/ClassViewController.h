//
//  ClassViewController.h
//  inface
//
//  Created by huangzengsong on 15/7/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
@protocol GetStorysByTypeDelegate;
@interface ClassViewController : BaseViewController<UIScrollViewDelegate>
{

    NSMutableArray *atitleArray;//12个按钮数组
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (unsafe_unretained,nonatomic)id <GetStorysByTypeDelegate>delegate;
@end


@protocol GetStorysByTypeDelegate <NSObject>

-(void)getStorysByMoreType:(NSString*)type storyname:(NSString*)storyname;

@end