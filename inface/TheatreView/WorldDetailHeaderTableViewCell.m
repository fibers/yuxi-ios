//
//  WorldDetailHeaderTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "WorldDetailHeaderTableViewCell.h"

@implementation WorldDetailHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.TimeLabel.textColor=GRAY_COLOR;
    self.FontCountLabel.textColor=GRAY_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
