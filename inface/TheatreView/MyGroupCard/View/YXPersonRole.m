//
//  YXPersonRole.m
//  inface
//
//  Created by appleone on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXPersonRole.h"

@implementation YXPersonRole
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
}

-(instancetype)init{
    if (self= [super init]) {
        self.textField.text = @"";
        self.chatInTextView.text = @"";
    }
    return self;
}


-(void)setPersonRole:(NSString *)role roleInfo:(NSString *)roleInfo
{
    
    
    self.textField = [[UITextField alloc]initWithFrame:CGRectMake(9, 0, ScreenWidth-18, 30)];
    self.textField.textColor = [UIColor colorWithHexString:@"#949494"];
    self.textField.font = [UIFont systemFontOfSize:15.0f];
    self.textField.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
    self.textField.layer.cornerRadius = 4;
    self.textField.textAlignment = NSTextAlignmentLeft ;
    [self addSubview:self.textField];
    
    
    if (role.length >0 && ![role isEqualToString:@""]) {
        self.textField.text =role;
    }else{
        self.textField.text = @"角色名称";
        
    }
    
    
    self.chatInTextView =[[PlaceholderTextView alloc]initWithFrame:CGRectMake(9, 48, ScreenWidth-18,ScreenHeight-69 -120)];
    self.chatInTextView.backgroundColor    = [UIColor colorWithHexString:@"f5f5f5"];
    self.chatInTextView.layer.cornerRadius = 8;
    self.chatInTextView.text               = roleInfo;
    self.chatInTextView.font               = [UIFont systemFontOfSize:14.0f];
    self.chatInTextView.delegate           = self;
    self.chatInTextView.font               = [UIFont systemFontOfSize:14.0f];
    self.chatInTextView.textColor          = [UIColor blackColor];
    self.chatInTextView.placeholder        = @"人物设置及使用说明";
    self.chatInTextView.placeholderFont    = [UIFont systemFontOfSize:14];
    self.chatInTextView.placeholderColor   = COLOR(0, 0, 0, 1);
    self.chatInTextView.layer.borderColor = [UIColor colorWithHexString:@"#b0adad"].CGColor;
    self.chatInTextView.layer.borderWidth = 0.8;
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [self addGestureRecognizer:recognizer];
    
    //    self.scrollView.contentSize            = CGSizeMake(ScreenWidth, self.chatInTextView.frame.size.height + 40 );
    //    CGRect orgRect                         = self.chatInTextView.frame;//获取原始UITextView的frame
    //
    //
    //    CGRect newRect = [roleInfo boundingRectWithSize:CGSizeMake(ScreenWidth-18, 99999) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:NULL];
    //    self.chatInTextView.frame              = CGRectMake(orgRect.origin.x, orgRect.origin.y, ScreenWidth-18, newRect.size.height);//重设UITextView的frame
    //    self.scrollView.contentSize            = CGSizeMake(ScreenWidth, self.chatInTextView.frame.size.height + 60);
    self.chatInTextView.textColor          = COLOR(0, 0, 0, 1);
    [self addSubview:self.chatInTextView];
    
    
}



#pragma mark 增减下滑手势
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gesture
{
    if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
        [self.chatInTextView resignFirstResponder];
        [self.textField resignFirstResponder];
    }
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.chatInTextView resignFirstResponder];

}



#pragma mark UItextViewDelegate
-(void)textViewDidChange:(UITextView *)textView
{
    
    //    NSString * text = self.chatInTextView.text;
    //    CGRect orgRect=self.chatInTextView.frame;//获取原始UITextView的frame
    //    CGSize  size = [text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(ScreenWidth-18, 2000) lineBreakMode:UILineBreakModeWordWrap];
    //    orgRect.size.height=size.height+20;//获取自适应文本内容高度
    //
    //    self.chatInTextView.frame=orgRect;//重设UITextView的frame
    //    self.scrollView.contentSize = CGSizeMake(ScreenWidth, self.chatInTextView.frame.size.height + 60);
    //    self.frame = CGRectMake(0, 0, ScreenWidth, 80 +self.chatIntextView.frame.size.height);
    
}

@end
