//
//  YXPersonRole.h
//  inface
//
//  Created by appleone on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXPersonRole : UIView<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic)  UITextField *textField;
//@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatIntextView;
@property (strong, nonatomic)  PlaceholderTextView *chatInTextView;

-(void)setPersonRole:(NSString*)role roleInfo:(NSString*)roleInfo;//获取人设信息
@end
