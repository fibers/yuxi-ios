//
//  YXGroupGardMenuView.h
//  inface
//
//  Created by 邢程 on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GroupCardName;
@protocol YXGroupGradMenuViewDelegate <NSObject>
@optional
-(void)menuBtnClicked:(GroupCardName*)groupCardName;
-(void)uploadViewHeight:(float)viewHeight;
-(void)downloadView :(float)viewHeight;
@end
@interface YXGroupGardMenuView : UIView
@property(weak,nonatomic)id<YXGroupGradMenuViewDelegate> delegate;
/**
 *  设置群名片菜单标题
 *
 *  @param groupNameArr 菜单标题数组 GroupCardName.h
 */
-(void)setGroupCardNameArr:(NSArray *)groupNameArr;

/**
 *  设置标题，默认为皮表
 *
 *  @param title 标题
 */
-(void)setTitleBarTitleStr:(NSString *)title;
/**
 *  显示菜单按钮界面
 */
-(void)showMenuBtnView;
/**
 *  隐藏菜单按钮页面
 */
-(void)hideMenuBtnView;
@end
