//
//  YXGroupGardMenuView.m
//  inface
//
//  Created by 邢程 on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXGroupGardMenuView.h"
#import "GroupCardName.h"
///菜单按钮水平边距
static const float  Menu_Btn_Margin_H = 20;
///菜单按钮垂直边距
static const float  Menu_Btn_Margin_V = 22;
///菜单按钮中间边距
static const float  Menu_Btn_Margin_C = 25;
///菜单按钮高度
static const float  Menu_Btn_H = 29;
@interface YXGroupGardMenuView()
@property(strong,nonatomic)UILabel * titleBarTitleL;
@property(strong,nonatomic)UIButton * titleBarArrowBtn;
@property(strong,nonatomic)NSArray *groupNameArr;
@property(strong,nonatomic)NSMutableArray * titleBtnArrMTMP;
@property(assign,nonatomic,getter = isShowMenuBtnView)BOOL showMenuBtnView;
@property(strong,nonatomic)UIView *titleBarView;
@property(strong,nonatomic)UIView *titleMenuBtnView;
@property(strong,nonatomic)NSString * labelText;
@end
@implementation YXGroupGardMenuView
#pragma mark -
#pragma mark 初始化
-(instancetype)init{
    if (self= [super init]) {
    }
    return self;
}


-(void)setGroupCardNameArr:(NSArray *)groupNameArr{
    _groupNameArr = groupNameArr;
     [self addSubview:self.titleBarView];
}

-(NSMutableArray *)titleBtnArrMTMP{
    if (!_titleBtnArrMTMP) {
        _titleBtnArrMTMP = [NSMutableArray array];
    }
    return _titleBtnArrMTMP;
}
-(void)showMenuBtnView{
    [self.titleBarArrowBtn setSelected:YES];
    _showMenuBtnView = YES;
    [self addSubview:self.titleMenuBtnView];
    self.frame = CGRectMake(0, 0, self.titleMenuBtnView.frame.size.width, self.titleBarView.frame.size.height+self.titleMenuBtnView.frame.size.height);
    [self.delegate downloadView:self.frame.size.height];

}
-(void)hideMenuBtnView{
    [self.titleBarArrowBtn setSelected:NO];
    _showMenuBtnView = NO;
    [self.titleMenuBtnView removeFromSuperview];
    
    self.frame = CGRectMake(self.titleMenuBtnView.frame.origin.x, 0, self.titleMenuBtnView.frame.size.width, 45);
    [self.delegate uploadViewHeight:self.frame.size.height];
    LOG(@"%@",NSStringFromCGRect(self.frame));

}

#pragma mark -
#pragma mark view的计算
/**
 *  标题BarView
 */
-(UIView *)titleBarView{
    if (!_titleBarView) {
        CGFloat titleBarH            = 46;
        UIView * titleBarBGV         = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, titleBarH)];
        [titleBarBGV setBackgroundColor:[UIColor whiteColor]];
        self.titleBarTitleL          = [[UILabel alloc]initWithFrame:CGRectMake(20, 15, 100, 18)];
        [self.titleBarTitleL setFont:[UIFont systemFontOfSize:15]];
         [self.titleBarTitleL setText:self.labelText];
        [self.titleBarTitleL setTextColor:[UIColor colorWithHexString:@"4a4a4a"]];
        self.titleBarArrowBtn        = [[UIButton alloc]initWithFrame:CGRectMake(0,0,9,5)];
        self.titleBarArrowBtn.center = CGPointMake(ScreenWidth-30, 24);
        [self.titleBarArrowBtn setBackgroundImage:[UIImage imageNamed:@"arrow_pulldown"] forState:UIControlStateNormal];
        [self.titleBarArrowBtn setBackgroundImage:[UIImage imageNamed:@"arrow_upDown"] forState:UIControlStateSelected];
        UIView *lineV                = [[UIView alloc]initWithFrame:CGRectMake(0, titleBarH-1, ScreenWidth, 1)];
        [lineV setBackgroundColor:[UIColor colorWithHexString:@"d7d7d7"]];
        [titleBarBGV addSubview:self.titleBarTitleL];
        [titleBarBGV addSubview:self.titleBarArrowBtn];
        [titleBarBGV addSubview:lineV];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(titleBarViewClicked)];
        [titleBarBGV addGestureRecognizer:tap];
        [titleBarBGV setUserInteractionEnabled:YES];
        _titleBarView                = titleBarBGV;
        [self hideMenuBtnView];
        
    }
    
    return _titleBarView;
}

-(void)setTitleBarTitleStr:(NSString *)title{
    if(!title||[@"" isEqualToString:title]){
         self.labelText = @"皮表";
 
    }else{
        self.labelText = title;
    }
}
-(UIView*)titleMenuBtnView{
    if(!_titleMenuBtnView){
        float btnW = (ScreenWidth - Menu_Btn_Margin_H*2-Menu_Btn_Margin_C)*0.5;
        UIView * bgV = [[UIView alloc]init];
        UIButton * lastBtn;
        for (int i =0 ;i<[self.groupNameArr count];i++) {
            GroupCardName * cardName = [self.groupNameArr objectAtIndex:i];
            UIButton * btn           = [self getNewBtnWithTitle:cardName.name andEnable:cardName.isEnable];
            btn.frame                = CGRectMake(Menu_Btn_Margin_H+((i%2)*(btnW+25)), Menu_Btn_Margin_V+((i/2)*(Menu_Btn_H+14)), 0.5*(ScreenWidth - Menu_Btn_Margin_H*2-Menu_Btn_Margin_C),29);
            [bgV addSubview:btn];
            btn.tag                  = i;
            [btn addTarget:self action:@selector(titleMenuBtnViewClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self.titleBtnArrMTMP addObject:btn];
            lastBtn                  = btn;
        }
        bgV.frame = CGRectMake(0, 46, ScreenWidth,CGRectGetMaxY(lastBtn.frame)+Menu_Btn_Margin_H);
        [bgV setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
        _titleMenuBtnView = bgV;
    }
    return _titleMenuBtnView;
}

-(UIButton *)getNewBtnWithTitle:(NSString*)title andEnable:(BOOL)enable{
    
    float btnW            = ScreenWidth - Menu_Btn_Margin_H*2-Menu_Btn_Margin_C;
    UIButton * btn        = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.layer.cornerRadius = 4.0f;
    [btn.layer setMasksToBounds:YES];
    btn.frame = CGRectMake(0, 0, btnW, Menu_Btn_H);
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setEnabled:enable];
//    btn.layer.cornerRadius = 5.0f;

    [btn setBackgroundImage:[UIImage imageNamed:@"btn_normal"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"btn_selected"] forState:UIControlStateDisabled];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"c4c4c4"] forState:UIControlStateDisabled];
    
    return btn;
    
}

#pragma mark -
#pragma mark 响应事件
-(void)titleBarViewClicked{
    if (self.isShowMenuBtnView) {
        [self hideMenuBtnView];
    }else{
        [self showMenuBtnView];
    }
}
-(void)titleMenuBtnViewClicked:(UIButton*)btn{
    GroupCardName * groupCardName = [self.groupNameArr objectAtIndex:btn.tag];
    [self.titleBarTitleL setText:groupCardName.name];
    if ([self.delegate respondsToSelector:@selector(menuBtnClicked:)]) {
        [self.delegate menuBtnClicked:groupCardName];
        [self.delegate uploadViewHeight:self.frame.size.height];
    }
    
}

@end
