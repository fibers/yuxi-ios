//
//  YXGroupCardViewController.h
//  inface
//
//  Created by 邢程 on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXGroupCardViewController : UIViewController

@property(nonatomic,strong)NSString * theartorId;

@property(nonatomic,strong)NSString * groupId;

@property(assign,nonatomic)BOOL       isAnimating;
-(void)setOwnerId:(NSString *)owner_id;
@end
