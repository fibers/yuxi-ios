//
//  YXGroupCardViewController.m
//  inface
//
//  Created by 邢程 on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXGroupCardViewController.h"
#import "GroupCardName.h"
#import "YXGroupGardMenuView.h"
#import "YXPersonRole.h"
#import "YXPersonRole.h"
@interface YXGroupCardViewController ()<YXGroupGradMenuViewDelegate,UIAlertViewDelegate>
@property(nonatomic,strong)NSMutableArray * rolesArr;
@property(nonatomic,strong)YXGroupGardMenuView * gardMenuView;
@property(nonatomic,strong)YXPersonRole * aview;
@property(nonatomic,strong)NSString * roleId;
@property(nonatomic,strong)NSString * ownerId;
-(void)menuBtnClicked:(GroupCardName *)groupCardName;

-(void)uploadViewHeight:(float)viewHeight;
-(void)downloadView :(float)viewHeight;
@end

@implementation YXGroupCardViewController

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self getPersonRoleStory];
    self.rolesArr = [NSMutableArray array];
    self.roleId = @"";
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    [[IQKeyboardManager sharedManager] disableInViewControllerClass:[YXGroupCardViewController class]];
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
//    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
//    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
//    [self.view addGestureRecognizer:recognizer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.navigationItem.title=@"我的群名片";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//
//#pragma mark 增减下滑手势
//-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gesture
//{
//    if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
//   
//        [self.aview.chatInTextView resignFirstResponder];
//    }
//}


-(void)returnBackl
{
    //    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"是否保存设置" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    //    [alert show];
    //    alert.tag = 10000;
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(void)returnBackr
{
    [self savePersonRole];
}

-(void)setOwnerId:(NSString *)owner_id
{
    _ownerId = owner_id;
}

-(void) keyboardWillShow:(NSNotification *) aNotification {
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    float height = keyboardRect.size.height;
    
    if (height == 0) {
        return  ;
        
    }
    
    //键盘没有弹出时
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.aview.chatInTextView.frame =  CGRectMake(9, 48, ScreenWidth-18, ScreenHeight-189-height);
        
        
    }];
}


-(void) keyboardWillHide:(NSNotification *) note
{
    
    [UIView animateWithDuration:.3f animations:^{
        //改变位置
        self.aview.chatInTextView.frame = CGRectMake(9, 48, ScreenWidth-18, ScreenHeight-189);
    }];
    
}




-(void)savePersonRole
{
    if (self.theartorId && self.roleId && self.aview.textField.text && self.aview.chatInTextView.text) {
        NSDictionary * params = @{@"uid":USERID,@"storyid":self.theartorId,@"roleid":self.roleId,@"userrolename":self.aview.textField.text,@"userroleinfo":self.aview.chatInTextView.text};
        [HttpTool postWithPath:@"SetUserRoleInfoInStory" params:params success:^(id JSON) {
            
            if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"保存数据成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alrt show];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                
                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alrt show];
            }
            
        } failure:^(NSError *error) {
            
            
        }];
        
    }else{
        return;
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==10000 &&buttonIndex==1) {
        [self savePersonRole];
    }else{
        if (buttonIndex==0) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark 获取人设信息
-(void)getPersonRoleStory
{
    NSDictionary * params = @{@"uid":USERID,@"storyid":self.theartorId};
    [HttpTool postWithPath:@"GetStoryRoles" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            NSMutableArray * arr = [[JSON objectForKey:@"roles"]mutableCopy];
            for (int i = 0; i<arr.count; i++) {
                NSDictionary * dic = arr[i];
                GroupCardName * groupModel = [[GroupCardName alloc]init];
                groupModel.ID =[dic objectForKey:@"id"];
                groupModel.name = [dic objectForKey:@"title"];
                if ([[dic objectForKey:@"num"]isEqualToString:@"0"]) {
                    groupModel.enable = NO;
                }else{
                    groupModel.enable = YES;
                }
                [self.rolesArr addObject:groupModel];
            }
            
        }
        [HttpTool postWithPath:@"GetUserRoleInfoInStory" params:params success:^(id JSON) {
            NSString * result = [JSON objectForKey:@"result"];
//            if ( [result isEqualToString:@"1"]) {
                NSDictionary * roleInfo = [JSON objectForKey:@"roleinfo"];
                //                self.view addSubview:]
                self.roleId = [roleInfo objectForKey:@"roleid"];
                self.aview = [[[NSBundle mainBundle]loadNibNamed:@"YXPersonRole" owner:nil options:nil] lastObject];
                [self.aview setPersonRole:[roleInfo objectForKey:@"userrolename"] roleInfo:[roleInfo objectForKey:@"userroleinfo"]];
//            if (ScreenHeight==480) {
//                self.aview.frame = CGRectMake(0, 105, ScreenWidth, self.aview.frame.size.height);
//            }else{
                self.aview.frame = CGRectMake(0, 60, ScreenWidth, self.aview.frame.size.height);

//            }
                self.gardMenuView = [[YXGroupGardMenuView alloc]init];
                self.gardMenuView.delegate =self;
                
                [self.gardMenuView setTitleBarTitleStr:[roleInfo objectForKey:@"roletitle"]];
                [self.gardMenuView setGroupCardNameArr:self.rolesArr];
//            if (ScreenHeight==480) {
//                self.gardMenuView.frame = CGRectMake(0, 60, ScreenWidth, 45);
//            }
                [self.view addSubview:self.gardMenuView];
                [self.view addSubview:self.aview];
  
//            }else{
//                if ([_ownerId isEqualToString:USERID]) {
//                    //自己是管理员
//                    UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"你是管理员大大哦" message:@"你需要在编辑剧大纲里编辑人设" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//                    [alrt show];
//                    return ;
//                }
//                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//                [alrt show];
//            }
            
        } failure:^(NSError *error) {
            
            
        }];
        
    } failure:^(NSError *error) {
        
        
    }];
}


-(void)menuBtnClicked:(GroupCardName *)groupCardName
{
    if (self.gardMenuView) {
        [self.gardMenuView hideMenuBtnView];
        self.roleId = groupCardName.ID;
    }
}


-(void)uploadViewHeight:(float)viewHeight
{
    self.aview.frame = CGRectMake(0, 60, ScreenWidth, self.aview.frame.size.height);
    
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)downloadView :(float)viewHeight
{
    self.aview.frame = CGRectMake(0, 14+ viewHeight, ScreenWidth, self.aview.frame.size.height);
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
