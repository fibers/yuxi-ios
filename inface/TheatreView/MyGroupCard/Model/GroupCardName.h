//
//  GroupCardName.h
//  inface
//
//  Created by 邢程 on 15/8/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupCardName : NSObject
@property(copy,nonatomic)NSString * ID;
@property(copy,nonatomic)NSString * name;
@property(assign,nonatomic,getter=isEnable)BOOL enable;
@end
