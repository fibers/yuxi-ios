//
//  TheatreGroupToChatTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/6/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TheatreGroupToChatTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (weak, nonatomic) IBOutlet UILabel *unreadCountLabel;
@property (strong, nonatomic) IBOutlet UIImageView *ArrowImage;//箭头
@end
