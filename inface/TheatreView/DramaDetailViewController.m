//
//  DramaDetailViewController.m
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "DramaDetailViewController.h"
#import "MBProgressHUD+YXStyle.h"
#import "YXMyCenterViewController.h"
#import <IDMPhoto.h>
#import <IDMPhotoBrowser.h>
#import "CurrentGroupModel.h"
#import "Storys.h"
#import "YXTopicDetailViewController.h"
@interface DramaDetailViewController ()
{
    UILabel * favor_label;
    int  favorcount;
}
@property(assign,nonatomic)BOOL isCollected;
@property(nonatomic,strong)UIImageView *imageV;
@property(nonatomic,strong)Storys * storyTMP;
@end

@implementation DramaDetailViewController

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)menuItemSelected:(id)sender
{
    KxMenuItem* item = (KxMenuItem*)sender;
    if ([item.title isEqualToString:@"删除"]) {

        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"是否确定删除?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag = 22222;
        [alert show];
    } else if ([item.title isEqualToString:@"分享"]) {
        [self shareAction:nil];
    }if ([item.title isEqualToString:@"收藏"]){
        [self collectAction:sender];
    }else if ([item.title isEqualToString:@"取消收藏"]){
        [self collectAction:sender];
    }

}
-(void)collectAction:(id)sender{
    KxMenuItem* item = (KxMenuItem*)sender;
    NSString * behavior = [item.title isEqualToString:@"收藏"]?@"1":@"0";
    [HttpTool postWithPath:@"CollectStory" params:@{@"uid":USERID,
                                                    @"storyid":self.storyID,
                                                    @"behavior":behavior,
                                                    @"type":@"0"
                                                    } success:^(id JSON) {
                                                        _isCollected =!_isCollected;
                                                        [MBProgressHUD showYXHudShowTextAddToView:self.view Title:[JSON objectForKey:@"describe"] andHideTime:1.5];
                                                    } failure:^(NSError *error) {
                                                        [MBProgressHUD showYXHudShowTextAddToView:self.view Title:@"操作失败" andHideTime:1.5];
                                                        
                                                    }];
}
//右上角按钮
-(void)returnBackr{
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([[self.dic objectForKey:@"creatorid"]isEqualToString:userid]) {
        //右上角按钮
        //我的自戏
        NSArray *menuItems =
        @[
          
          [KxMenuItem menuItem:@"编辑"
                         image:[UIImage imageNamed:@"·"]
                        target:self
                        action:@selector(modifySelfStory)],
          [KxMenuItem menuItem:@"分享"
                         image:[UIImage imageNamed:@"·"]
                        target:self
                        action:@selector(menuItemSelected:)],

          [KxMenuItem menuItem:@"删除"
                         image:[UIImage imageNamed:@"·"]
                        target:self
                        action:@selector(menuItemSelected:)],

          
          ];
        

        
        KxMenuItem *first = menuItems[0];
        //first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
        first.alignment = NSTextAlignmentLeft;
        
        [KxMenu showMenuInView:self.navigationController.view
                      fromRect:CGRectMake(ScreenWidth-45, 10, 25, 44)
                     menuItems:menuItems];
//        mySheet = [[UIActionSheet alloc]
//                                  initWithTitle:nil
//                                  delegate:self
//                                  cancelButtonTitle:@"取消"
//                                  destructiveButtonTitle:nil
//                                  otherButtonTitles:@"分享",@"删除",nil];
//        mySheet.tag=3000;
//        [mySheet showInView:self.view];

    }else{
        
        //别人的自戏

        NSArray *menuItems =
        @[

          [KxMenuItem menuItem:@"分享"
                         image:[UIImage imageNamed:@"分享"]
                        target:self
                        action:@selector(menuItemSelected:)],
          _isCollected?
          [KxMenuItem menuItem:@"取消收藏"
                         image:[UIImage imageNamed:@"版权"]
                        target:self action:@selector(menuItemSelected:)]:
          [KxMenuItem menuItem:@"收藏"
                         image:[UIImage imageNamed:@"版权"]
                        target:self action:@selector(menuItemSelected:)],
          
          [KxMenuItem menuItem:@"举报"
                         image:[UIImage imageNamed:@"分享"]
                        target:self
                        action:@selector(SendClick:)]
          ];
        
        
        KxMenuItem *first = menuItems[0];
        //first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
        first.alignment = NSTextAlignmentLeft;
        
        [KxMenu showMenuInView:self.navigationController.view
                      fromRect:CGRectMake(ScreenWidth-45, 10, 25, 44)
                     menuItems:menuItems];
//        mySheet = [[UIActionSheet alloc]
//                                  initWithTitle:nil
//                                  delegate:self
//                                  cancelButtonTitle:@"取消"
//                                  destructiveButtonTitle:nil
//                                  otherButtonTitles:@"分享",nil];
//        mySheet.tag=6000;
//        [mySheet showInView:self.view];
    }
}




//- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (actionSheet.tag==3000) {
//        //我的自戏
//        if (buttonIndex==0) {
//            //分享
//            [self shareAction:shareBtn];
//        }else if (buttonIndex==1){
//            //删除剧
//            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"是否确定删除?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//            alert.tag = 22222;
//            [alert show];
//            
//            
//        }
//    }else if (actionSheet.tag==6000) {
//        //别人的自戏
//        if (buttonIndex==0) {
//            //分享
//            [self shareAction:shareBtn];
//        }
//    }
//}



#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==22222 && buttonIndex==1) {
    
        if (buttonIndex==1) {
            //删除
            NSDictionary * params = @{@"uid":USERID,@"storyid":self.storyID,@"token":@"110"};
            [HttpTool postWithPath:@"DeleteStory" params:params success:^(id JSON) {
                [self.navigationController popToRootViewControllerAnimated:YES];
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
            } failure:^(NSError *error) {
                
            }];
        }

    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    self.scrollView.userInteractionEnabled=YES;//用户可交互
    self.scrollView.backgroundColor=[UIColor whiteColor];
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.delegate=self;
    [self.view addSubview:self.scrollView];
    #pragma mark 获取我的自戏的详情
    [self GetSelfStoryDetail];
    
    aTopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    aTopBtn.frame = CGRectMake(0,ScreenHeight-104, ScreenWidth, 40);
    aTopBtn.backgroundColor = [UIColor colorWithHexString:@"#ffb7c8"];
    [aTopBtn setTitle:@"评论" forState:UIControlStateNormal];
    aTopBtn.titleLabel.font = [UIFont systemFontOfSize:20.0f];
//    [aTopBtn setImage:[UIImage imageNamed:@"xuanfubtn"] forState:UIControlStateNormal];
    [aTopBtn addTarget:self action:@selector(aToping:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:aTopBtn];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateSelfStory) name:@"ModifySelfStory" object:nil];
    
}


-(void)updateSelfStory
{
    [[self.scrollView subviews]enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        //[obj removeFromSuperview];
    }];
    //[self GetSelfStoryDetail];
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"storyid":self.storyID,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetSelfStoryDetail" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            self.dic=[JSON objectForKey:@"detatils"];
            _isCollected = ![[self.dic objectForKey:@"isCollectedByUser"] isEqualToString:@"0"];
            self.commendgroupid = [self.dic objectForKey:@"commentgroupid"];
            NSDictionary * params = @{@"title":[self.dic objectForKey:@"title"],@"wordcount":[self.dic objectForKey:@"wordcount"],@"storyimg":[self.dic objectForKey:@"storyimg"],@"praisenum": [self.dic objectForKey:@"praisenum"] ,@"commentnum":[self.dic objectForKey:@"commentnum"],@"ispraised":[self.dic objectForKey:@"ispraisedbyuser"]};
            favorcount = [[self.dic objectForKey:@"praisenum"]intValue];
            if (self.switching == NO) {
                [self.delegate updateStoryInfo:params storyid:self.storyID];
                
            }
            if (![[self.dic objectForKey:@"storyimg"] isEqualToString:@""]) {
                
                UIImage *aimage=[UIImage imageNamed:@"juxiangqing"];
                UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(20, 60, ScreenWidth-40, (ScreenWidth-40)*380/670)];
                imageView.contentMode=UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds=YES;
                [imageView sd_setImageWithURL:[NSURL URLWithString:[self.dic objectForKey:@"storyimg"]] placeholderImage:aimage completed:nil];
                self.imageV = imageView;
                UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(storyImgClicked)];
                [imageView addGestureRecognizer:tap];
                imageView.userInteractionEnabled=YES;
                [self.scrollView addSubview:imageView];
                
                contenLabel.frame = CGRectMake(20, 60+(ScreenWidth-40)*380/670+20, ScreenWidth-40, 200) ;
            }
            
            NSString *title=[self.dic objectForKey:@"title"];
            title_label.text=title;
            
            NSString *content= [self.dic objectForKey:@"wordcount"];
            wordCountLabel.text = [NSString stringWithFormat:@"创作%@字",content];

            NSString *contentLblDetail= [self.dic objectForKey:@"content"];
           
            contenLabel.text = contentLblDetail;
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentLblDetail];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            
            [paragraphStyle setLineSpacing:5.0];//调整行间距
            
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentLblDetail length])];
            contenLabel.attributedText = attributedString;
            //     self.chatInTextView.attributedText = [[NSAttributedString alloc] initWithString:content ==nil ?@"":content attributes:att];
            [contenLabel sizeToFit];
            
            [self.scrollView addSubview:contenLabel];
            self.scrollView.contentSize=CGSizeMake(ScreenWidth,  60+(ScreenWidth-40)*380/670+20+contenLabel.frame.size.height+50+150);//滚动的内容尺寸
            

        }
        
    } failure:^(NSError *error) {
        
        
    }];
    
}
-(void)storyImgClicked{
    IDMPhoto *photo = [IDMPhoto photoWithImage:self.imageV.image];
    IDMPhotoBrowser * browser = [[IDMPhotoBrowser alloc]initWithPhotos:@[photo] animatedFromView:self.imageV];
    [self presentViewController:browser animated:YES completion:^{
        
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self  GetSelfStoryDetail];
}

//回到顶端
-(void)aToping:(UIButton *)sender{
    //去评论
    //发送请求判断
    YXTopicDetailViewController * Vcontroller = [[YXTopicDetailViewController alloc]init];
    
    [Vcontroller setType:YXTopicDetailTypeStory];
    
    [Vcontroller setTopicID:self.storyID];
    
    [self.navigationController pushViewController:Vcontroller animated:YES];
}


#pragma mark 获取我的自戏详情
-(void)GetSelfStoryDetail
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"storyid":self.storyID,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetSelfStoryDetail" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
           
            self.dic=[JSON objectForKey:@"detatils"];
             self.commendgroupid = [self.dic objectForKey:@"commentgroupid"];
            NSDictionary * params = @{@"title":[self.dic objectForKey:@"title"],@"wordcount":[self.dic objectForKey:@"wordcount"],@"storyimg":[self.dic objectForKey:@"storyimg"],@"praisenum": [self.dic objectForKey:@"praisenum"] ,@"commentnum":[self.dic objectForKey:@"commentnum"],@"ispraised":[self.dic objectForKey:@"ispraisedbyuser"]};
            favorcount = [[self.dic objectForKey:@"praisenum"]intValue];
            if (self.switching == NO) {
                [self.delegate updateStoryInfo:params storyid:self.storyID];
                
            }

            [self setRightBarButtonItem];
            [self addscrollview];
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
        
    } failure:^(NSError *error) {
        
        
    }];
}

#pragma mark 编辑自戏内容
-(void)modifySelfStory
{
    CreateMyStoryViewController * detailViewController = [[CreateMyStoryViewController alloc] init];
    detailViewController.hidesBottomBarWhenPushed=YES;
    detailViewController.storyDic = self.dic;
    detailViewController.isModifyStory = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}

//6按钮
-(void)tapfriendpage:(UITapGestureRecognizer*) recognizer
{
    NSString * FriendID = [self.dic objectForKey:@"creatorid"];

    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([userid isEqualToString:FriendID]) {
        //TODO(Xc.)这里注释了。
//        MyCenterViewController * detailViewController = [[MyCenterViewController alloc]init];
//        detailViewController.fromClass=@"ViewPersonalData";
//        detailViewController.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:detailViewController animated:YES];
        
    } else {
        
//        FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
//        detailViewController.FriendID = FriendID;
//        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
    [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}

-(void)setRightBarButtonItem{


    UIView *aview=[[UIView alloc]init];
    aview.backgroundColor=[UIColor clearColor];
    aview.frame=CGRectMake(0, 0, 140, 44);
    self.navigationItem.titleView=aview;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapfriendpage:)];
    
    [aview addGestureRecognizer:tapGestureRecognizer];
    
    UIImageView *imageView=[[UIImageView alloc]init];
    imageView.userInteractionEnabled=YES;
    imageView.frame=CGRectMake(0, 8, 27, 27);
    imageView.layer.borderWidth = 0;
    imageView.layer.borderColor = [UIColor grayColor].CGColor;
    imageView.clipsToBounds =YES;
    imageView.layer.cornerRadius = CGRectGetHeight(imageView.bounds)/2;
    imageView.backgroundColor=[UIColor whiteColor];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[self.dic objectForKey:@"portrait"]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView * vipImage = [[UIImageView alloc]initWithFrame:CGRectMake(23, 23, 15, 15)];
    [imageView addSubview:vipImage];
    
    int isVip = [[self.dic objectForKey:@"portrait"] isSignedUser];
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
    switch (isVip) {
        case 0:
            vipImage.hidden = YES;
            break;
        case 1:
            vipImage.hidden = NO;
            
            [vipImage setImage:image];
            
            break;
        case 2:
            vipImage.hidden = NO;
            
            vipImage.image = officalImage;
            
            break;
        case 3:
            vipImage.hidden = NO;
            
            vipImage.image = officalImage;
            
            break;
        default:
            break;
    }
    [aview addSubview:imageView];
    
    //定义一个label，显示标题内容
    if (!titleLabel) {
        titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(35, 8, 100, 14)];
        titleLabel.backgroundColor =[UIColor clearColor];
        titleLabel.userInteractionEnabled=YES;
        NSString *creatorname=[self.dic objectForKey:@"creatorname"];
        titleLabel.text=[NSString stringWithFormat:@"%@的自戏",creatorname];
        titleLabel.textColor=BLACK_COLOR;
        titleLabel.font=[UIFont systemFontOfSize:11];
        titleLabel.textAlignment=NSTextAlignmentLeft;
        [aview addSubview:titleLabel];
    }
    NSString *creatorname=[self.dic objectForKey:@"creatorname"];
    titleLabel.text=[NSString stringWithFormat:@"%@的自戏",creatorname];
    

    
    
    //定义一个label，显示标题内容
    if (!timeLabel) {
        timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(35, 25, 100, 14)];
        timeLabel.backgroundColor =[UIColor clearColor];
        timeLabel.userInteractionEnabled=YES;
    
        timeLabel.textColor=GRAY_COLOR;
        timeLabel.font=[UIFont systemFontOfSize:9];
        timeLabel.textAlignment=NSTextAlignmentLeft;
        [aview addSubview:timeLabel];

    }
    NSString *createtime=[self.dic objectForKey:@"createtime"];
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[createtime doubleValue]];
    
    timeLabel.text=[date detailPromptDateString];
    
    UIView *bview=[[UIView alloc]init];
    bview.backgroundColor=[UIColor clearColor];
    bview.frame=CGRectMake(0, 0, 80, 44);
    UIBarButtonItem *barback2 = [[UIBarButtonItem alloc]initWithCustomView:bview];
    self.navigationItem.rightBarButtonItem=barback2;
    
    UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
    aButton.frame=CGRectMake(0,12, 21, 21);
    [aButton setImage:[UIImage imageNamed:@"weishoucang"] forState:UIControlStateNormal];
    [aButton setImage:[UIImage imageNamed:@"shoucang"] forState:UIControlStateSelected];
    if ([[self.dic objectForKey:@"ispraisedbyuser"]isEqualToString:@"1"]) {
        aButton.selected=YES;
    } else {
        aButton.selected=NO;
    }
    shareBtn = aButton;
    [aButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
    [aButton setBackgroundColor:[UIColor clearColor]];
    [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
    
    [bview addSubview:aButton];
    
    UILabel *favorlabel=[[UILabel alloc]initWithFrame:CGRectMake(25, 0, 30, 44)];
    favorlabel.backgroundColor =[UIColor clearColor];
    favorlabel.userInteractionEnabled=YES;
    //收藏个数没给返回
    favorlabel.text=[self.dic objectForKey:@"praisenum"];
    favorlabel.textColor=BLACK_COLOR;
    favorlabel.font=[UIFont systemFontOfSize:11];
    favorlabel.textAlignment=NSTextAlignmentCenter;
    [bview addSubview:favorlabel];
    favor_label = favorlabel;

    UIButton *aButtonmore=[UIButton buttonWithType:UIButtonTypeCustom];
    aButtonmore.frame=CGRectMake(55,0, 25, 44);
    [aButtonmore setImage:[UIImage imageNamed:@"morebtn"] forState:UIControlStateNormal];
    [aButtonmore setImage:[UIImage imageNamed:@"morebtnselect"] forState:UIControlStateHighlighted];
    [aButtonmore addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    [aButtonmore setBackgroundColor:[UIColor clearColor]];
    [aButtonmore setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
    
    [bview addSubview:aButtonmore];
    

}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)SendClick:(UIButton *)btn
{
    //举报
    ReportTheatreViewController * reportTheater = [[ReportTheatreViewController alloc]init];
    reportTheater.hidesBottomBarWhenPushed = YES;
    reportTheater.reportid = self.storyID;
    if (reportTheater.isAnimating) {
        return;
    }
    reportTheater.isAnimating = YES;
    [self.navigationController pushViewController:reportTheater animated:YES];
}

-(void)addscrollview{

    
    //定义一个label，显示标题内容
     title_label=[[UILabel alloc]initWithFrame:CGRectMake(20, 10, ScreenWidth-90, 50)];
    title_label.backgroundColor =[UIColor clearColor];
    title_label.userInteractionEnabled=YES;
    NSString *title=[self.dic objectForKey:@"title"];
    title_label.text=title;
    title_label.textColor=[UIColor blackColor];
    title_label.font=[UIFont systemFontOfSize:18];
    title_label.numberOfLines=0;
    title_label.textAlignment=NSTextAlignmentLeft;
    [self.scrollView addSubview:title_label];
    
    //发送按钮
    wordCountLabel =[[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth-90, 20, 80, 30)];
    [self.scrollView addSubview:wordCountLabel];
    wordCountLabel.font = [UIFont systemFontOfSize:12.0f];
    NSString *wordcontent= [self.dic objectForKey:@"wordcount"];
    wordCountLabel.textColor = [UIColor colorWithHexString:@"#a1a1a1"];
    wordCountLabel.textAlignment = NSTextAlignmentCenter;
    wordCountLabel.text = [NSString stringWithFormat:@"创作%@字",wordcontent];
    if (![[self.dic objectForKey:@"storyimg"] isEqualToString:@""]) {
    
        UIImage *aimage=[UIImage imageNamed:@"juxiangqing"];
        UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(20, 60, ScreenWidth-40, (ScreenWidth-40)*380/670)];
        imageView.contentMode=UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds=YES;
        [imageView sd_setImageWithURL:[NSURL URLWithString:[self.dic objectForKey:@"storyimg"]] placeholderImage:aimage completed:nil];
        imageView.userInteractionEnabled=YES;
        self.imageV = imageView;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(storyImgClicked)];
        [imageView addGestureRecognizer:tap];
        [self.scrollView addSubview:imageView];
    }
    

    
    NSString *contentLblDetail= [self.dic objectForKey:@"content"];
//    float contentLblDetailHeight=[HZSInstances labelAutoCalculateRectWith:contentLblDetail FontSize:15.0 MaxSize:CGSizeMake(ScreenWidth-40, 2000)].height;
//    //定义一个label，显示标题内容
//    UILabel *labeltime=[[UILabel alloc]initWithFrame:CGRectMake(20, 60+(ScreenWidth-40)*380/670+20, ScreenWidth-40, contentLblDetailHeight)];
//    labeltime.backgroundColor =[UIColor clearColor];
//    labeltime.userInteractionEnabled=YES;
//    labeltime.text=contentLblDetail;
//    labeltime.textColor=BLACK_CONTENT_COLOR;
//    labeltime.font=[UIFont systemFontOfSize:15];
//    labeltime.numberOfLines=0;
//    labeltime.textAlignment=NSTextAlignmentLeft;
    
    contenLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 60+(ScreenWidth-40)*380/670+20,  ScreenWidth-40, 200)];
    [contenLabel setBackgroundColor:[UIColor blackColor]];
    contenLabel.textColor = BLACK_CONTENT_COLOR;
    contenLabel.backgroundColor  = [UIColor clearColor];
    [contenLabel setNumberOfLines:0];
//    [contenLabel setFont:[UIFont systemFontOfSize:18.0f]];
    NSString *labelText = contentLblDetail;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:5.0];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    contenLabel.attributedText = attributedString;
//     self.chatInTextView.attributedText = [[NSAttributedString alloc] initWithString:content ==nil ?@"":content attributes:att];
    [contenLabel sizeToFit];
    
    [self.scrollView addSubview:contenLabel];
        self.scrollView.contentSize=CGSizeMake(ScreenWidth,  60+(ScreenWidth-40)*380/670+20+contenLabel.frame.size.height+50+150);//滚动的内容尺寸

    
    if ([[self.dic objectForKey:@"storyimg"] isEqualToString:@""]) {
        
        
        contenLabel.frame = CGRectMake(20, 80, ScreenWidth-40,contenLabel.frame.size.height);
        
    }
//
//    self.scrollView.contentSize=CGSizeMake(ScreenWidth,  60+(ScreenWidth-40)*380/670+20+contentLblDetailHeight+50+150);//滚动的内容尺寸
//
//    
//    UILabel *fontCountlabel=[[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2.0-50, ScreenHeight-64-15-30, 100, 30)];
//    fontCountlabel.backgroundColor =COLOR(0, 0, 0, 0.45);
//    fontCountlabel.userInteractionEnabled=YES;
//    fontCountlabel.text=[NSString stringWithFormat:@"创作%ld字",(unsigned long)contentLblDetail.length];
//    fontCountlabel.textColor=[UIColor whiteColor];
//    fontCountlabel.font=[UIFont systemFontOfSize:12];
//    fontCountlabel.textAlignment=NSTextAlignmentCenter;
//    fontCountlabel.layer.borderWidth = 0;
//    fontCountlabel.layer.borderColor = GRAY_COLOR.CGColor;
//    fontCountlabel.layer.cornerRadius = 4;
//    fontCountlabel.clipsToBounds = YES;
//    [self.view addSubview:fontCountlabel];
    



}
-(void)setStory:(Storys *)story{
    _storyTMP = story;
}
//喜欢
-(void)likeAction:(UIButton *)sender{
    UIButton *aButton=sender;
    NSInteger praiseNum = [_storyTMP.praisenum integerValue];
    if (_storyTMP) {
        _storyTMP.praisenum = (aButton.isSelected?[NSString stringWithFormat:@"%ld",(praiseNum-1)]:[NSString stringWithFormat:@"%ld",(praiseNum+1)]);
        _storyTMP.ispraised = aButton.isSelected?@"0":@"1";
    }
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if (aButton.selected==NO) {
        aButton.selected=YES;
        //由不喜欢变为喜欢
        //storyid剧的id  behavior0: 取消赞  1: 赞
        NSDictionary * parameters = @{@"uid":userid,@"storyid":self.storyID,@"behavior":@"1",@"token":@"110"};
        
        [HttpTool postWithPath:@"PraiseStory" params:parameters success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]integerValue]==1) {
                
                favorcount+=1;
                favor_label.text= [NSString stringWithFormat:@"%d",favorcount];
                [SVProgressHUD showSuccessWithStatus:@"点赞成功" duration:1.0];

             NSDictionary * params = @{@"title":[self.dic objectForKey:@"title"],@"wordcount":[self.dic objectForKey:@"wordcount"],@"storyimg":[self.dic objectForKey:@"storyimg"],@"praisenum": favor_label.text ,@"commentnum":[self.dic objectForKey:@"commentnum"],@"ispraised":@"1"};
                [self.delegate updateStoryInfo:params storyid:self.storyID];
                if (self.switching == NO) {
                    [self.delegate updateStoryInfo:params storyid:self.storyID];
                    
                }
            }else{
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                
            }
            
        } failure:^(NSError *error) {
        }];
        
    } else {
        aButton.selected=NO;
        //由喜欢变为不喜欢
        //storyid剧的id  behavior0: 取消赞  1: 赞
        
        NSDictionary * parameters = @{@"uid":userid,@"storyid":self.storyID,@"behavior":@"0",@"token":@"110"};
        
        [HttpTool postWithPath:@"PraiseStory" params:parameters success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]integerValue]==1) {
                favorcount -=1;
                favor_label.text= [NSString stringWithFormat:@"%d",favorcount];
                [SVProgressHUD showSuccessWithStatus:@"取消赞成功" duration:1.0];

                NSDictionary * params = @{@"title":[self.dic objectForKey:@"title"],@"wordcount":[self.dic objectForKey:@"wordcount"],@"storyimg":[self.dic objectForKey:@"storyimg"],@"praisenum": favor_label.text ,@"commentnum":[self.dic objectForKey:@"commentnum"],@"ispraised":@"0"};
                if (self.switching == NO) {
                    [self.delegate updateStoryInfo:params storyid:self.storyID];
                    
                }
            }else{
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                
            }
            
        } failure:^(NSError *error) {
        }];
        
    }
    
    
}

#pragma mark 分享自戏，暂时给的是百度网址链接
-(void)shareAction:(UIButton *)sender
{

//    [mySheet dismissWithClickedButtonIndex:0 animated:YES];

    
    ShareModel * model = [ShareModel sharedInstance];
    NSString * portrait = [self.dic objectForKey:@"portrait"];
    NSString * storyImage = [self.dic objectForKey:@"storyimg"];
    NSString * title = [self.dic objectForKey:@"title"];
    NSString * intro = [self.dic objectForKey:@"content"];
    NSString *   baiduUrl = [self.dic objectForKey:@"section"] ;

    NSDictionary * params = @{@"portrait":portrait,@"storyimg":storyImage,@"section":baiduUrl, @"title":title,@"intro":intro};
    [model shareToThird :nil dic:params];
}




- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    if (scrollView.contentOffset.y>0) {
        //改变位置
//        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
//            aTopBtn.frame = CGRectMake(ScreenWidth-67,ScreenHeight-64-60, 57, 60);
//            
//        } completion:^(BOOL finished) {
//            
//            
//            aTopBtn.hidden=YES;
//        }];
        
        
    } else {
        
        //        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        //            aTopBtn.frame = CGRectMake(ScreenWidth-67,ScreenHeight-64-150, 57, 60);
        //        } completion:^(BOOL finished) {
        //            aTopBtn.hidden=NO;
        //        }];
        
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
