//
//  SearchTheatreViewController.m
//  inface
//
//  Created by Mac on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SearchTheatreViewController.h"
#import "Storys.h"
#import "MBProgressHUD+YXStyle.h"
@interface SearchTheatreViewController ()<PolymerizationTableViewCellDelegate>

@end

@implementation SearchTheatreViewController

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    if ([_SearchText.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入关键字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    [_SearchText resignFirstResponder];
    [self searchList];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"搜索" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    UIImageView *imageView=[[UIImageView alloc]init];
    imageView.userInteractionEnabled=YES;
    imageView.frame=CGRectMake(0, 0, ScreenWidth-90, 36);
    imageView.layer.borderWidth = 1;
    imageView.layer.borderColor = XIAN_COLOR.CGColor;
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = 4;
    imageView.backgroundColor=[UIColor whiteColor];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView=imageView;
    
    UIImageView *imageViewtmp=[[UIImageView alloc]init];
    imageViewtmp.userInteractionEnabled=YES;
    imageViewtmp.image=[UIImage imageNamed:@"sousuoju"];
    imageViewtmp.frame=CGRectMake(10, 9, 22, 18);
    imageViewtmp.clipsToBounds = YES;
    imageViewtmp.contentMode = UIViewContentModeScaleAspectFit;
    [imageView addSubview:imageViewtmp];
    
    _SearchText = [[UITextField alloc] initWithFrame:CGRectMake(37, 0, ScreenWidth-90-40, 36)];
    _SearchText.borderStyle = UITextBorderStyleNone;
    _SearchText.backgroundColor = [UIColor clearColor];
    _SearchText.placeholder = @"请输入剧的名称或ID号";
    _SearchText.text=@"";
    _SearchText.tintColor=BLUECOLOR;
    _SearchText.font = [UIFont systemFontOfSize:17];
    _SearchText.returnKeyType = UIReturnKeyDefault;
    _SearchText.delegate = self;
    _SearchText.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [imageView addSubview:_SearchText];
    
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    
}

-(void)searchList
{
    [self.SearchText endEditing:YES];
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary *params = @{@"uid":userid,@"cond":_SearchText.text,@"token":@"110",@"type":@"1"};
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    [HttpTool postWithPath:@"SearchStory" params:params success:^(id JSON) {
        
        NSMutableArray * families = [JSON objectForKey:@"storys"];
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            self.tableStorys= [families mutableCopy];
            [self.tableView reloadData];
            
            if (self.tableStorys.count==0) {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"该剧还没有，搜索其他剧试试吧~" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
            }
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"没有找到符合条件的剧" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //所有的剧列表
    return [self.tableStorys count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"PolymerizationTableViewCell";
    PolymerizationTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"PolymerizationTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    NSDictionary * stopryDic = [self.tableStorys objectAtIndex:indexPath.row];
    Storys * story = [Storys objectWithKeyValues:stopryDic];
    [cell setContentWithStory:story];
    
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //    NSDictionary * dic = [self.tableStorys objectAtIndex:indexPath.row];
    //
    //    NSString *intro=[dic objectForKey:@"intro"];
    //    float height=[HZSInstances labelAutoCalculateRectWith:intro FontSize:14.0 MaxSize:CGSizeMake(ScreenWidth-45, 2000)].height;
    //
    //    float imgwidth=ScreenWidth-45;
    //    float imgheight=imgwidth*7.0/10.0;
    //    if (height<40) {
    //        return (height+10)+145+imgheight;
    //
    //    }else{
    //        return 40+145+imgheight;
    //
    //    }
    return 200;
    
    
}


//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    NSString *type=[[self.tableStorys objectAtIndex:indexPath.row] objectForKey:@"type"];
    if ([type isEqualToString:@"0"]) {
        //剧0
        WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc] init];
        detailViewController.storyID = [[self.tableStorys objectAtIndex:indexPath.row]objectForKey:@"storyid"];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    } else {
        //我的自戏1
        DramaDetailViewController * detailViewController = [[DramaDetailViewController alloc] init];
        detailViewController.storyID=[[self.tableStorys objectAtIndex:indexPath.row]objectForKey:@"storyid"];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

-(void)polymerizationTableViewCellGoBtnClicked:(Storys *)story{
    TheatreGroupToChatViewController * detailViewController = [[TheatreGroupToChatViewController alloc]init];
    detailViewController.theatreid = story.storyid;
    detailViewController.checkGroupId = story.checkgroupid;
    detailViewController.checkTitle = story.checkgrouptitle;
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
