//
//  TheatreDetailIntroduceTableViewCell.h
//  inface
//
//  Created by Mac on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧详情单独的cell 简介cell
#import <UIKit/UIKit.h>

@interface TheatreDetailIntroduceTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@end
