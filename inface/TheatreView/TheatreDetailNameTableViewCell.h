//
//  TheatreDetailNameTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TheatreDetailNameTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *TmpLabel;
@property (weak, nonatomic) IBOutlet UILabel *ContentLabel;

@end
