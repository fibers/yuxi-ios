//
//  TheatreDetailNameTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/7/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "TheatreDetailNameTableViewCell.h"

@implementation TheatreDetailNameTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.TmpLabel.textColor=BLACK_COLOR;
    self.ContentLabel.textColor=BLUECOLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
