//
//  TheatreDetailViewController.m
//  inface
//
//  Created by Mac on 15/5/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "TheatreDetailViewController.h"
#import "TheatreTypeViewController.h"
#import "TheatreNameViewController.h"
#import "PersonSetModelViewController.h"
#import "ApplyTheaterViewController.h"
@interface TheatreDetailViewController ()<UzysAssetsPickerControllerDelegate>
{
    UpYun * _upYun;
    BOOL _isChange;
}
@property (nonatomic,strong) UIImageView *aimageViewPhoto;
@property (strong,nonatomic)  EVCircularProgressView * circleView;
@property (strong,nonatomic)  NSString * imgSerUrl;//图片又拍地址
@end

@implementation TheatreDetailViewController

- (UIImageView *)aimageViewPhoto
{
    if (_aimageViewPhoto == nil) {
        
        _aimageViewPhoto = [[UIImageView alloc]init];
    }
    return _aimageViewPhoto;
}

//左上角按钮
-(void)returnBackl{
    if (self.isCreateStory) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)showDoneBtn:(BOOL)isShowBtn{

    if (isShowBtn) {
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(doneBtnClicked)];
        self.navigationItem.rightBarButtonItem = rightItem;
    }
}

-(void)createType
{
    self.isCreateStory = YES;
}
-(void)doneBtnClicked{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=@"编辑剧";
    
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 15)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 15)];
    }

    self.tableView.separatorColor=XIAN_COLOR;
//    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    [self getStoryInfo];
    
    //观察编辑动作的改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadArray:) name:@"MajorIntroduceAddChanged" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(nameChanged) name:@"NAMECHANGE" object:nil];
}
- (void)nameChanged
{
    [self getStoryInfo];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)reloadArray:(NSNotification *)notification
{
    
#pragma mark 获取剧的详情
    [self getStoryInfo];
    
}

#pragma mark 获取剧详情
-(void)getStoryInfo
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"storyid":self.storyID,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetStoryInfo" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            self.dic=[NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)JSON];
            [self.tableView reloadData];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        static NSString *identifier = @"TheatreDetailTableViewCell";
        TheatreDetailTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreDetailTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        NSURL* urlWork = [NSURL URLWithString:[self.dic objectForKey:@"storyimg"]];
        if (_isChange) {
            
            cell.WorkImage.image = self.aimageViewPhoto.image;
            self.aimageViewPhoto.hidden = NO;
            _isChange = NO;
        } else {
            
            [cell.WorkImage sd_setImageWithURL:urlWork placeholderImage:[UIImage imageNamed:@"juxiangqing"] completed:nil];
            cell.WorkImage.contentMode=UIViewContentModeScaleAspectFill;
            cell.WorkImage.clipsToBounds=YES;
            self.aimageViewPhoto.hidden = YES;
        }
        return cell;

    } else if (indexPath.row==1){
        static NSString *identifier=@"TheatreDetailNameTableViewCell";
        TheatreDetailNameTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreDetailNameTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
    
            cell.TmpLabel.text=@"剧名";
            cell.ContentLabel.text=[[self.dic objectForKey:@"title"] description];
            return cell;
        
    }else if(indexPath.row == 7){
        static NSString *identifier=@"TheatreIDTableViewCell";
        TheatreIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.UserNameLabel.text=@"阅读权限";
        cell.UserNameLabel.textColor = [UIColor blackColor];
        NSString * permission = [[NSUserDefaults standardUserDefaults]objectForKey:self.storyID];
        if ([permission isEqualToString:@"1"]) {
            cell.UserDetailLabel.text=@"关闭";
        }else{
            
            cell.UserDetailLabel.text=@"打开";
        }
        //判断是否是管理员。非管理员只能读不能操作

        return cell;
        
    }else{
        static NSString *identifier=@"TheatreDetailIntroduceTableViewCell";
        TheatreDetailIntroduceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreDetailIntroduceTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if (indexPath.row==2) {
            cell.UserNameLabel.text=@"简介";
        } else if (indexPath.row==3){
            
            cell.UserNameLabel.text=@"主线剧情";
        }  else if (indexPath.row==4){
            
            cell.UserNameLabel.text=@"规则";
        }else if(indexPath.row == 5){
            cell.UserNameLabel.text=@"剧群";
        }else if (indexPath.row == 6){
            cell.UserNameLabel.text=@"主线人物";

        }
        return cell;
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        return 280;
    } else {
        return 44;
    }
    
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    
    if (indexPath.row==2) {
        IntroduceViewController * detailViewController = [[IntroduceViewController alloc] init];
        detailViewController.dic=self.dic;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    } else if (indexPath.row==3) {
        MajorStoryViewController * detailViewController = [[MajorStoryViewController alloc] init];
        detailViewController.dic=self.dic;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;

        [self.navigationController pushViewController:detailViewController animated:YES];
    }  else if (indexPath.row==4) {
        RuleViewController * detailViewController = [[RuleViewController alloc] init];
        detailViewController.dic=self.dic;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    } else if (indexPath.row==5) {
        KindOfGroupViewController * detailViewController = [[KindOfGroupViewController alloc] init];
        detailViewController.dic=self.dic;
        detailViewController.theatreid = self.storyID;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    } else if (indexPath.row == 0) {
        
        // 第一行 选择 进行选择图片操作
        [self startPhotoChooser];
        
    }  else if (indexPath.row == 1) {
        
        // 更改剧名
        TheatreNameViewController *nvc = [[TheatreNameViewController alloc]init];
        nvc.dataDic = self.dic;
        if (nvc.isAnimating) {
            return;
        }
        nvc.isAnimating = YES;
        [self.navigationController pushViewController:nvc animated:YES];
    }else if(indexPath.row == 7){
        //阅读权限设置
        
        ReadingAuthoritySettingsViewController * detailViewController = [[ReadingAuthoritySettingsViewController alloc] init];
        detailViewController.theatreid=self.storyID;
        if ([[self.dic objectForKey:@"readautority"]isEqualToString:@"0"]) {
            detailViewController.permission=@"0";
        }else{
            
            detailViewController.permission=@"1";
        }
        [detailViewController changeReadPermisson:^(NSString *readAoturity) {
            //修改权限时，页面刷新展示结果
//            [self.dic setValue:readAoturity forKey:@"readautority"];
            [self.tableView reloadData];
        }];
        //不是管理员时  没有操作权限
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    }else if (indexPath.row == 6){
        MajorPersonViewController * detailViewController = [[MajorPersonViewController alloc] init];
        detailViewController.dic=self.dic;
        detailViewController.hidesBottomBarWhenPushed=YES;
        
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }else if (indexPath.row == 100){
            ApplyTheaterViewController * applyViewController = [[ApplyTheaterViewController alloc]init];
        
          applyViewController.thearterId = self.storyID;
        
        applyViewController.hidesBottomBarWhenPushed = YES;
        
        [applyViewController ifStoryOwnerApply];
        
        [self.navigationController pushViewController:applyViewController animated:YES];


    }
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void) startPhotoChooser
{
    //123..
    UzysAssetsPickerController *picker = [[UzysAssetsPickerController alloc] init];
    picker.delegate = self;
    
    picker.maximumNumberOfSelectionPhoto = 1;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

- (void)uzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    
    
    if([[assets[0] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
    {
        
        [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            ALAsset *representation = obj;
            
            UIImage * original = [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                                     scale:representation.defaultRepresentation.scale
                                               orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
            self.aimageViewPhoto.image=original;

            
            CGSize  imgSize = CGSizeMake(2210, 1280);
            
            original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
            
            [self getServerKey :original];
            
        }];
        
    }
    else //Video
    {
        
        
    }
    
}



#pragma mark 从又拍获取图片字符串
-(void)getServerKey:(UIImage *)image
{
    _isChange = YES;
    [self.tableView reloadData];
    //    [SVProgressHUD showWithStatus:@"上传中" maskType:SVProgressHUDMaskTypeBlack];
    //此处需要换成又拍的云存储
    _upYun = [[UpYun alloc]init];
    
    //上传图片成功
    
    [_upYun uploadImage:image];
    
        
//    [self.circleView setHidden:NO];
////    
//    self.circleView.center = CGPointMake(self.aimageViewPhoto.center.x - 10, self.aimageViewPhoto.center.y - 210);
//    
//    [self.circleView setProgress:0.0 animated:YES];
//    self.circleView.alpha = 1;
//    [self.circleView commonInit];
//    [self.aimageViewPhoto addSubview:self.circleView];
//    [self.aimageViewPhoto bringSubviewToFront:self.circleView];
    
    WEAKSELF
    _upYun.successBlocker = ^(id data){
        if (!weakSelf) {
            return ;
        }
        NSString *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
        imgUrl = [NSString stringWithFormat:@"%@%@", IMAGE_SERVER_BASE_URL, imgUrl];
//        [weakSelf.circleView setHidden:YES];
        weakSelf.imgSerUrl = imgUrl;
        
        NSMutableDictionary *tmp = [NSMutableDictionary dictionaryWithDictionary:weakSelf.dic];
        tmp[@"storyimg"] = imgUrl;
        weakSelf.dic = tmp;
        
        NSMutableDictionary *myDic = [NSMutableDictionary dictionary];
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        myDic[@"uid"] = userid;
        myDic[@"storyid"] = weakSelf.storyID;
        myDic[@"type"] = @"3";
        myDic[@"content"] = imgUrl;
        myDic[@"token"] = @"110";

        [HttpTool postWithPath:@"ModifyStory" params:myDic success:^(id JSON) {
            [[MyLiveViewController shareInstance]getMyStorys];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"NAMECHANGE" object:nil];
            LOG(@"success ---------- %@",JSON);
        } failure:^(NSError *error) {
            
        }];
        
//        [weakSelf.tableView reloadData];
        [SVProgressHUD dismiss];
    };
    _upYun.failBlocker = ^(NSError * error)
    {
        //上传又拍失败的操作
        [SVProgressHUD dismiss];
        [weakSelf.circleView setHidden:YES];
        
        [SVProgressHUD showErrorWithStatus:@"上传失败" duration:2.0];
    };
    
    _upYun.progressBlocker = ^(CGFloat percent,long long requestDidSendBytes)
    {
        [ weakSelf.circleView setProgress:percent animated:YES];
    };
}



@end
