//
//  WorldDetailRuleTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorldDetailRuleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ContentLabel;
@end
