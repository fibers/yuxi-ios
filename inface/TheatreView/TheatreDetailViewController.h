//
//  TheatreDetailViewController.h
//  inface
//
//  Created by Mac on 15/5/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧情视图控制器（世界-剧-剧情）
#import "BaseViewController.h"
#import "HZSButton.h"
#import "ShareModel.h"
#import "TheatreDetailIntroduceTableViewCell.h"
#import "TheatreDetailTableViewCell.h"
#import "MajorPersonViewController.h"
#import "MajorStoryViewController.h"
#import "KindOfGroupViewController.h"
#import "IntroduceViewController.h"
#import "RuleViewController.h"
#import "TheatreDetailNameTableViewCell.h"
#import "TheatreGroupToChatViewController.h"
@interface TheatreDetailViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    float  SCROLLVIEWHEIGHT;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic ,strong)NSMutableDictionary * dic;
@property(strong,nonatomic)NSString * storyID;//剧的id，方便从storymodel中获取剧里面的会话id
@property(strong,nonatomic)NSString * creatorid;//管理员的id；
@property(assign,nonatomic)NSString * isplayingstory;//是否是从正在开戏的群进来的
@property(assign,nonatomic)BOOL       isAnimating;
@property(assign,nonatomic)BOOL       isCreateStory;//是否从创建进来
-(void)showDoneBtn:(BOOL)isShowBtn;
-(void)createType;
@end
