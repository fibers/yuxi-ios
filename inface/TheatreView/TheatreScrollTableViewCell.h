//
//  TheatreScrollTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/5/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//


//剧上面的滚动scrollview
#import <UIKit/UIKit.h>

@interface TheatreScrollTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end
