//
//  TheatreDetailTableViewCell.h
//  inface
//
//  Created by Mac on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧详情图片下面的详情

#import <UIKit/UIKit.h>

@interface TheatreDetailTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *WorkImage;//我的自戏大图

@end
