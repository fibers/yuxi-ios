//
//  PolymerizationViewController.m
//  inface
//
//  Created by 邢程 on 15/8/19.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//#define Menu_Custom_File [[NSString documentPath] stringByAppendingPathComponent:@"/menu_custom.tmp"]
#import "PolymerizationViewController.h"
#import "PolymerizationNodeViewController.h"
#import "Menu.h"
#import "LjjUISegmentedControl.h"
#import "YXMainSearchViewController.h"
#import "PolymerizationHomeViewController.h"
#import "Familytype.h"
@interface PolymerizationViewController ()<UIScrollViewDelegate,LjjUISegmentedControlDelegate>
@property(strong,nonatomic)UIScrollView * mainScrollV;
@property(strong,nonatomic)Menu *customMenu;
@property(strong,nonatomic)LjjUISegmentedControl *ljjuisement;
@end

@implementation PolymerizationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadMenuSucceed:^(BOOL isSuccess) {
        [self setNavigationBar];
        [self.view addSubview:self.mainScrollV];
        [self initNodeVc];
    }];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notf:) name:@"HomePageMoreButtnClickedNotification" object:nil];
}

-(void)initCustomMenu:(Menu *)menu{
//    Menu * customMenu = [NSKeyedUnarchiver unarchiveObjectWithFile:Menu_Full_File];
//    if(!customMenu){
        Menu *  customMenu = menu;
        for (Showtype * showType in customMenu.showtype) {
            NSInteger len = [showType.nodes count];
            for(int i = 0;i<(len >7?7:len);i++){
                Nodes * node = [showType.nodes objectAtIndex:i];
                node.userSelected = YES;
                [showType.nodes replaceObjectAtIndex:i withObject:node];
            }
        }
        [NSKeyedArchiver archiveRootObject:customMenu toFile:Menu_Full_File];
//    }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)notf:(NSNotification*)notif{
    NSString * str = notif.object;
    for (Showtype * showType in self.customMenu.showtype) {
        Nodes * node = [showType.nodes firstObject];
        if ([node.id isEqualToString:str]) {
            [self.ljjuisement selectTheSegument:[self.customMenu.showtype indexOfObject:showType]];
        }
    }

}
-(void)setNavigationBar{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]init];
    NSMutableArray* showTypeStrArrM = [NSMutableArray array];
    for (Showtype * showType in self.customMenu.showtype) {
        [showTypeStrArrM addObject:showType.name];
    }
    self.ljjuisement=[[LjjUISegmentedControl alloc]initWithFrame:CGRectMake(77, 0, ScreenWidth-154, 44)];
    self.ljjuisement.titleFont = [UIFont systemFontOfSize:17];
    self.ljjuisement.titleColor = [UIColor colorWithHexString:@"4a4a4a"];
    _ljjuisement.delegate = self;
    NSArray* ljjarray=showTypeStrArrM   ;
    [self.ljjuisement AddSegumentArray:ljjarray];
    self.navigationItem.titleView=self.ljjuisement;
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"zs_sousuo"] style:UIBarButtonItemStylePlain target:self action:@selector(searchBtnClicked:)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
}
-(void)initNodeVc{
    for (Showtype * showType in self.customMenu.showtype) {
        if([showType.id isEqualToString:@"1"]){
            PolymerizationHomeViewController * homeVC = [[PolymerizationHomeViewController alloc]init];
            [self addChildViewController:homeVC];
            homeVC.view.frame = CGRectMake(ScreenWidth *[self.customMenu.showtype indexOfObject:showType], 0, ScreenWidth, ScreenHeight-64-44);
            [self.mainScrollV addSubview:homeVC.view];
            
        }else{
            PolymerizationNodeViewController * childVC = [[PolymerizationNodeViewController alloc]init];
            [self addChildViewController:childVC];
            [childVC setShowType:showType];
            childVC.view.frame = CGRectMake(ScreenWidth *[self.customMenu.showtype indexOfObject:showType], 0, ScreenWidth, ScreenHeight-64-44);
            [self.mainScrollV addSubview:childVC.view];
        }
    }
}

#pragma mark -
#pragma mark 代理
-(void)uisegumentSelectionChange:(NSInteger)selection{
    [self.mainScrollV setContentOffset:CGPointMake(ScreenWidth*selection, 0)];
}
#pragma mark -
#pragma mark 懒加载
-(UIScrollView *)mainScrollV{
    if (!_mainScrollV) {
        _mainScrollV = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64-44)] ;
        _mainScrollV.contentSize = CGSizeMake(ScreenWidth*[self.customMenu.showtype count], ScreenWidth - 64-44);
        [_mainScrollV setScrollEnabled:NO];
        [_mainScrollV setPagingEnabled:YES];
        _mainScrollV.delegate = self;
    }
    return _mainScrollV;
}
/**
 *  加载菜单
 *
 *  @param success 是否加载成功回调
 */
-(void)loadMenuSucceed:(void (^)(BOOL isSuccess))success{
    __block BOOL isReturn = NO;
    Menu * customMenu = [NSKeyedUnarchiver unarchiveObjectWithFile:Menu_Full_File];
    if (customMenu) {
        _customMenu = customMenu;
        isReturn = YES;
        success(YES);
    }
    [HttpTool postWithPath:@"GetStoryShowType" params:@{@"uid":USERID} success:^(id JSON) {
        Menu * menu  =[Menu objectWithKeyValues:JSON];
        Menu * oldMenu = [NSKeyedUnarchiver unarchiveObjectWithFile:Menu_Full_File];
        if(menu&& [menu.result isEqualToString:@"1"]){
            BOOL isSuccess = NO;
            
            if(![self isMenuEqualWithOldMenu:menu andNewMenu:oldMenu]){//如果加载的菜单和缓存的菜单不等。需要重新缓存
               isSuccess = [NSKeyedArchiver archiveRootObject:menu toFile:Menu_Full_File];
                [self initCustomMenu:menu];
            }
            _customMenu = menu;
        if (!isReturn) {
            success(isSuccess);
             isReturn = YES;
        }
        }else{
            if (!isReturn) {
                success(NO);
                isReturn = YES;
            }
            
        }
    } failure:^(NSError *error) {
        if (!isReturn) {
            success(NO);
            isReturn = YES;
        }
    }];
    
}

-(void)searchBtnClicked:(id)sender{
    YXMainSearchViewController * mainSearchVC = [[YXMainSearchViewController alloc]init];
    [self.navigationItem.backBarButtonItem setTitle:@""];
    if (mainSearchVC.isAnimating) {
        return;
    }
    mainSearchVC.isAnimating = YES;
    [self.navigationController pushViewController:mainSearchVC animated:YES];
}


/**
 *  判断新旧model是否相等。用来验证是否需要重新缓存
 *
 *  @param oldMenu 一个model
 *  @param newMenu 另一个model
 *
 *  @return 是否相等
 */
-(BOOL)isMenuEqualWithOldMenu:(Menu *)oldMenu andNewMenu:(Menu *)newMenu{
    if([oldMenu.showtype count]!=[newMenu.showtype count])return NO;
    for (int i = 0; i<[oldMenu.showtype count]; i++) {
        Showtype * oldShowType =[oldMenu.showtype objectAtIndex:i];
        Showtype * newShowType = [newMenu.showtype objectAtIndex:i];
        BOOL parmIsEqual = [oldShowType.id isEqualToString:newShowType.id]&&[oldShowType.name isEqualToString:newShowType.name]&&[oldShowType.nodes count]==[newShowType.nodes count]&&[oldMenu.familytype count]==[newMenu.familytype count];
        if (!parmIsEqual) return NO;
        for (int j = 0; j<[oldShowType.nodes count]; j++) {
            Nodes * oldNodes = [oldShowType.nodes objectAtIndex:j];
            Nodes * newNodes = [newShowType.nodes objectAtIndex:j];
            if (!([oldNodes.id isEqualToString:newNodes.id]&&[oldNodes.name isEqualToString:newNodes.name])) {
                return NO;
            }
        }
        for(int k = 0; k<[oldMenu.familytype count];k++){
            Familytype * oldType = [oldMenu.familytype objectAtIndex:k];
            Familytype * newType = [newMenu.familytype objectAtIndex:k];
            if(![oldType.typeid isEqualToString:newType.typeid]&&[oldType.typename isEqualToString:newType.typename]){
                return NO;
            }
            
        }
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
