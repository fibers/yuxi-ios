//
//  PolymerizationHomeViewController.m
//  inface
//
//  Created by 邢程 on 15/8/21.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PolymerizationHomeViewController.h"
#import "HomeEntity.h"
#import "SDCycleScrollView.h"

#import "YXHomeTableviewHeaderView.h"
#import "Selectedcontents.h"
#import "Storys.h"
#import "MBProgressHUD+YXStyle.h"
#import "NSString+YXExtention.h"
#import "TheatreScrollDetailViewController.h"
#import "SquareScrollTableViewCell.h"
#import "TheatreScrollDetailViewController.h"
#import "YXBindMobilePhoneViewController.h"
#define HomeTMPFilePath [[NSString documentPath] stringByAppendingString:@"/homePage.tmp"]
#define HomeBannerTMPFilePath [[NSString documentPath] stringByAppendingString:@"/homeBanner.tmp"]
@interface PolymerizationHomeViewController ()<SDCycleScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,YXHomeTableviewHeaderViewDelegate,PolymerizationTableViewCellDelegate>{
    NSMutableArray *scrollArray;               // 存放所有需要滚动的图片 UIImage
    float  SCROLLVIEWHEIGHT;
}
@property(nonatomic,strong)HomeEntity * homeEntity;
@property(nonatomic,strong)UITableView*tableView;
@end

@implementation PolymerizationHomeViewController
-(instancetype)init{
    if (self = [super init]) {
        tmp = 0;
        [self verifyPhoneIsBinding];
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refreshDate];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
    [self.view addSubview:self.tableView];
    [self getBanners];
    [self createScrollView];
    [self loadData];
}


#pragma mark 验证手机是否绑定
-(void)verifyPhoneIsBinding{
    
    [HttpTool postWithPath:@"GetRegisteredMobile" params:@{@"uid":USERID,@"token":@"110"} success:^(id JSON) {
            if([[JSON objectForKey:@"result"]boolValue]){
                [[NSUserDefaults standardUserDefaults]setObject:[JSON objectForKey:@"mobile"] forKey:kBindPhoneNumber];
            }else{
                YXBindMobilePhoneViewController * bindVC = [[YXBindMobilePhoneViewController alloc]init];
                bindVC.showCloseBar = YES;
                [self presentViewController:bindVC animated:YES completion:^{
                    
                }];
            }
        } failure:^(NSError *error) {
            
        }];
    }
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self showAlertMessage];
}
-(void)showAlertMessage{
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"joinQunFlag"]boolValue]){
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithBool:YES] forKey:@"joinQunFlag"];
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"亲，小伙伴们加入你的群，会在“我的”页面“角色申请”选项中有提示哦，快去看看和更多小伙伴们对戏吧\(^o^)/" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
    }
}
int tmp;
-(void)refreshDate{
    if (tmp == 0) {
        tmp++;
        return;
    }
    [HttpTool postWithPath:@"GetSelectedStorys" params:@{@"uid":USERID} success:^(id JSON) {
        HomeEntity * homeEntity = [HomeEntity objectWithKeyValues:JSON];
        if ([homeEntity.result isEqualToString:@"1"]) {
            [NSKeyedArchiver archiveRootObject:homeEntity toFile:HomeTMPFilePath];
            _homeEntity = homeEntity;
                [self.tableView reloadData];
            
        }else{
            MBProgressHUD * hud = [MBProgressHUD HUDForView:self.view];
            [hud setMode:MBProgressHUDModeText];
            [hud setLabelText:@"服务器错误！"];
            [hud show:YES];
            [hud hide:YES afterDelay:2];
        }
        
    } failure:^(NSError *error) {
        
    }];
    NSDictionary *params = @{@"uid":USERID,@"token":@"110"};
    [HttpTool postWithPath:@"GetBanners" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
//            if (scrollArray) {
                scrollArray=[JSON objectForKey:@"bannaers"];
                if (scrollArray.count>0) {
                    [NSKeyedArchiver archiveRootObject:scrollArray toFile:HomeBannerTMPFilePath];
                    [self createScrollView];
                }
//            }else{
//                scrollArray=[JSON objectForKey:@"bannaers"];
//                if (scrollArray.count>0) {
//                    [NSKeyedArchiver archiveRootObject:scrollArray toFile:HomeBannerTMPFilePath];
//                    [self createScrollView];
//                }
//            }
            
        }
    } failure:^(NSError *error) {
    }];
}
-(void)loadData{
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    HomeEntity * homeEntity = [NSKeyedUnarchiver unarchiveObjectWithFile:HomeTMPFilePath];
    if (homeEntity) {
        _homeEntity = homeEntity;
        [self.tableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    [HttpTool postWithPath:@"GetSelectedStorys" params:@{@"uid":USERID} success:^(id JSON) {
        HomeEntity * homeEntity = [HomeEntity objectWithKeyValues:JSON];
        if ([homeEntity.result isEqualToString:@"1"]) {
            [NSKeyedArchiver archiveRootObject:homeEntity toFile:HomeTMPFilePath];
            if (self.homeEntity) {
                _homeEntity = homeEntity;
            }else{
                 _homeEntity = homeEntity;
                [self.tableView reloadData];
            }
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }else{
            MBProgressHUD * hud = [MBProgressHUD HUDForView:self.view];
            [hud setMode:MBProgressHUDModeText];
            [hud setLabelText:@"服务器错误！"];
            [hud show:YES];
            [hud hide:YES afterDelay:2];
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];

}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64-44)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableView setShowsVerticalScrollIndicator:NO];
        [_tableView setBackgroundColor:[UIColor clearColor]];
    }
    return _tableView;
}

#pragma mark -
#pragma mark tableView Delegate & DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    Selectedcontents* content =  [self.homeEntity.selectedcontents objectAtIndex:section];
    return [content.storys count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    Selectedcontents* content =  [self.homeEntity.selectedcontents objectAtIndex:indexPath.section];
//    Storys * story = [content.storys objectAtIndex:indexPath.row];
//    if ([story.storyimg isEqualToString:@""]) {
//        return 165;
//    }else{
        return 200;
//    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Selectedcontents* content =  [self.homeEntity.selectedcontents objectAtIndex:indexPath.section];
    if(indexPath.row > [content.storys count]-1){
        return [[UITableViewCell alloc]init];
    }
    
    Storys * story = [content.storys objectAtIndex:indexPath.row];
    NSAssert(story!=nil, @"story 不能为空");
    if([story.storyimg isEqualToString:@""] ){//无图
        PolymerizationFontTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PolymerizationFontTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"PolymerizationFontTableViewCell" owner:self options:nil]lastObject];
        }
        [cell setContentWithStory:story];
        return cell;
    }else{
        PolymerizationTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PolymerizationTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"PolymerizationTableViewCell" owner:self options:nil]lastObject];
        }
        [cell setContentWithStory:story];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.delegate = self;
        return cell;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.homeEntity.selectedcontents count];
 
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 42;

}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    YXHomeTableviewHeaderView * headerV  = [[[NSBundle mainBundle]loadNibNamed:@"YXHomeTableviewHeaderView" owner:self options:nil]lastObject];
    headerV.delegate= self;
    Selectedcontents* content =  [self.homeEntity.selectedcontents objectAtIndex:section];
    [headerV setContentWithSelectedContent:content];
    return headerV;
}
//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    Selectedcontents* content =  [self.homeEntity.selectedcontents objectAtIndex:indexPath.section];
    Storys * storys = [content.storys objectAtIndex:indexPath.row];
    NSString *type= storys.type;
    if ([type isEqualToString:@"0"]) {
        //剧0
        WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc] init];
        detailViewController.storyID = storys.storyid;
        //        detailViewController.delegate = self;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    } else {
        //我的自戏1
        DramaDetailViewController * detailViewController = [[DramaDetailViewController alloc] init];
        detailViewController.storyID=storys.storyid;
        detailViewController.commendgroupid = storys.commentgroupid;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        //        detailViewController.delegate = self;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
#pragma mark -
#pragma mark 其他代理
-(void)yXHomeTableviewHeaderDidClickedWithSelectedContents:(Selectedcontents *)content{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"HomePageMoreButtnClickedNotification" object:content.selectedtype];
}
-(void)polymerizationTableViewCellGoBtnClicked:(Storys *)story{
    TheatreGroupToChatViewController * detailViewController = [[TheatreGroupToChatViewController alloc]init];
    detailViewController.theatreid = story.storyid;
    detailViewController.checkGroupId = story.checkgroupid;
    detailViewController.checkTitle = story.checkgrouptitle;
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;

    [self.navigationController pushViewController:detailViewController animated:YES];
}
#pragma mark -
#pragma mark 轮播条
-(void)getBanners
{
    //获取广告滚动剧列表(Post)
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary *params = @{@"uid":userid,@"token":@"110"};
    NSMutableArray * arrM = [NSKeyedUnarchiver unarchiveObjectWithFile:HomeBannerTMPFilePath];
    if (arrM) {
        scrollArray = arrM;
        if (scrollArray.count>0) {
            [self createScrollView];
        }
    }
    [HttpTool postWithPath:@"GetBanners" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            if (scrollArray) {
                scrollArray=[JSON objectForKey:@"bannaers"];
                if (scrollArray.count>0) {
                    [NSKeyedArchiver archiveRootObject:scrollArray toFile:HomeBannerTMPFilePath];
                }
            }else{
                scrollArray=[JSON objectForKey:@"bannaers"];
                if (scrollArray.count>0) {
                    [NSKeyedArchiver archiveRootObject:scrollArray toFile:HomeBannerTMPFilePath];
                    [self createScrollView];
                }
            }
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
        LOG(@"%@",error.description);
    }];
    
}

-(void)createScrollView{
    
    UINib *nib=[UINib nibWithNibName:@"SquareScrollTableViewCell" bundle:nil];
    SquareScrollTableViewCell *headerview=[[nib instantiateWithOwner:nil options:nil] firstObject];
    
    SCROLLVIEWHEIGHT=ScreenWidth/375.0*150.0;
    
    headerview.frame=CGRectMake(0, 0, ScreenWidth, SCROLLVIEWHEIGHT);
    
    self.tableView.tableHeaderView=headerview;
    
    /* 以下为scrollview */
    // 先放最后一张 然后放后台图片 最后放第一张图片
    
    NSMutableArray *imagesURLStrings =[[NSMutableArray alloc]init];
    NSMutableArray *titles =[[NSMutableArray alloc]init];
    for (int i=0; i<scrollArray.count; i++) {
        NSString *banner_portrait=[[scrollArray objectAtIndex:i]objectForKey:@"banner_portrait"];
        [imagesURLStrings addObject:[banner_portrait stringOfW900ImgUrl] ];
        NSString *banner_title=[[scrollArray objectAtIndex:i]objectForKey:@"banner_title"];
        [titles addObject:banner_title];
    }
    
    //网络加载 --- 创建带标题的图片轮播器
    SDCycleScrollView *cycleScrollView2 = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, ScreenWidth, SCROLLVIEWHEIGHT) imageURLStringsGroup:nil]; // 模拟网络延时情景
    cycleScrollView2.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    cycleScrollView2.delegate = self;
    cycleScrollView2.titlesGroup = titles;
    cycleScrollView2.autoScrollTimeInterval = 6;
    cycleScrollView2.titleLabelTextFont=[UIFont boldSystemFontOfSize:16];
    cycleScrollView2.backgroundColor=[UIColor clearColor];
    cycleScrollView2.dotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
    cycleScrollView2.placeholderImage = [UIImage imageNamed:@"scrollzhanwei"];
    [headerview addSubview:cycleScrollView2];
    cycleScrollView2.imageURLStringsGroup = imagesURLStrings;

}



#pragma mark  SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSInteger i = index;
     NSString *isurl= @"";
    if (i < scrollArray.count) {
        
       isurl=[[scrollArray objectAtIndex:i]objectForKey:@"isurl"];
    }else{
        return;
    }
    if ([isurl isEqualToString:@"1"]) {
        //跳网页
        TheatreScrollDetailViewController * detailViewController = [[TheatreScrollDetailViewController alloc] init];
        detailViewController.navTitle=[[scrollArray objectAtIndex:i]objectForKey:@"banner_title"];
        detailViewController.contentUrl=[[scrollArray objectAtIndex:i]objectForKey:@"url"];
        detailViewController.storyID = [[scrollArray objectAtIndex:i]objectForKey:@"storyid"];
        detailViewController.hidesBottomBarWhenPushed=YES;
        detailViewController.type = [[scrollArray objectAtIndex:i]objectForKey:@"isselfstory"];
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;

        [self.navigationController pushViewController:detailViewController animated:YES];
        
        
    } else {
        NSString * type = [[scrollArray objectAtIndex:i]objectForKey:@"isselfstory"];
        if([type isEqualToString:@"0"])
        {
            //跳详情
            WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc] init];
            detailViewController.storyID = [[scrollArray objectAtIndex:i]objectForKey:@"storyid"];
            detailViewController.hidesBottomBarWhenPushed=YES;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;

            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        if ([type isEqualToString:@"1"]) {
            DramaDetailViewController * detailViewController = [[DramaDetailViewController
                                                                 alloc] init];
            detailViewController.storyID = [[scrollArray objectAtIndex:i]objectForKey:@"storyid"];
            detailViewController.hidesBottomBarWhenPushed=YES;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
