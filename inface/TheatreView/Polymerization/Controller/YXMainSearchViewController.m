//
//  YXMainSearchViewController.m
//  inface
//
//  Created by 邢程 on 15/8/21.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXMainSearchViewController.h"
#import "MBProgressHUD+YXStyle.h"
#import "Polymerization.h"
#import "Storys.h"
#import "IQKeyboardManager.h"
@interface YXMainSearchViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,PolymerizationTableViewCellDelegate>
@property(strong,nonatomic)UITextField * tf;
@property(strong,nonatomic)UITableView * tableView;
@property(strong,nonatomic)Polymerization *pol;
@end

@implementation YXMainSearchViewController
-(instancetype)init{
    if (self = [super init]) {
        [self setHidesBottomBarWhenPushed:YES];
        [[IQKeyboardManager sharedManager] setEnable:NO];
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBar];
    UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64)];
    [imgV setImage:[UIImage imageNamed:@"bigbackground"]];
    [self.view addSubview:imgV];
    [self.view addSubview:self.tableView];
    [self.tf becomeFirstResponder];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.tf resignFirstResponder];
//    [self.tf endEditing:YES];
}

/**
 *  设置navbar
 */
-(void)setNavBar{
    self.navigationItem.titleView = self.tf;
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
#pragma mark -
#pragma mark delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self searchWithContent:textField.text];
    [self.tf endEditing:YES];
    return YES;
}

-(void)polymerizationTableViewCellGoBtnClicked:(Storys *)story{
    TheatreGroupToChatViewController * detailViewController = [[TheatreGroupToChatViewController alloc]init];
    detailViewController.theatreid = story.storyid;
    detailViewController.checkTitle = story.checkgrouptitle;
    detailViewController.checkGroupId = story.checkgroupid;
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;

    [self.navigationController pushViewController:detailViewController animated:YES];
}
#pragma mark -
#pragma mark init

-(UITextField *)tf{
    if (!_tf) {
        _tf = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth - 50, 30)];
        [_tf setBorderStyle:UITextBorderStyleRoundedRect];
        [_tf setBackgroundColor:[UIColor whiteColor]];
        [_tf setReturnKeyType:UIReturnKeySearch];
        [_tf setPlaceholder:@"请输入搜索内容"];
        _tf.delegate= self;

    }
    return _tf;
}

-(UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableView setBackgroundColor:[UIColor clearColor]];
    }
    return _tableView;
}

#pragma mark -
#pragma mark tableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    Polymerization * polymerization = self.pol;
    if (polymerization) {
        return [polymerization.storys count];
    }else{
        return 0;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    Polymerization * polymerization = self.pol;
//    Storys * story = [polymerization.storys objectAtIndex:indexPath.row];
//    if ([story.storyimg isEqualToString:@""]) {
//        return 165;
//    }else{
        return 200;
//    }
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Polymerization * pol              = self.pol;
    NSAssert(pol!=nil, @"pol 不能为空");
    Storys* story                     = [pol.storys objectAtIndex:indexPath.row];
    NSAssert(story!=nil, @"story 不能为空");
    if([story.storyimg isEqualToString:@""] ){//无图
        PolymerizationFontTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PolymerizationFontTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"PolymerizationFontTableViewCell" owner:self options:nil]lastObject];
        }
        [cell setContentWithStory:story];
        return cell;
    }else{
        PolymerizationTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PolymerizationTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"PolymerizationTableViewCell" owner:self options:nil]lastObject];
        }
        cell.delegate = self;

        [cell setContentWithStory:story];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
}

//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    Polymerization * pol = self.pol;
    Storys *storys = [pol.storys objectAtIndex:indexPath.row];
    NSString *type= storys.type;
    if ([type isEqualToString:@"0"]) {
        //剧0
        WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc] init];
        detailViewController.storyID = storys.storyid;
        //        detailViewController.delegate = self;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;

        [self.navigationController pushViewController:detailViewController animated:YES];
        
    } else {
        //我的自戏1
        DramaDetailViewController * detailViewController = [[DramaDetailViewController alloc] init];
        detailViewController.storyID=storys.storyid;
        detailViewController.commendgroupid = storys.commentgroupid;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;

        //        detailViewController.delegate = self;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


-(void)searchWithContent:(NSString*)content{
    self.pol.storys = [NSMutableArray array];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
    content = [content trim];
    if ([content isEqualToString:@""]) {
        [self showTextHUDWithContent:@"总要写点什么吧╭(╯^╰)╮" andHideTime:1.5];
        return;
    }
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    [HttpTool postWithPath:@"SearchStory" params:@{@"uid":USERID,
                                                  @"cond":content,
                                                  @"category":@"",
                                                   @"type":@"2"} success:^(id JSON) {
                                                     Polymerization * poly =  [Polymerization objectWithKeyValues:JSON];
                                                      if ([poly.storys count] ==0) {
                                                          [self showTextHUDWithContent:@"没有搜索到内容%>_<%" andHideTime:1.5];
                                                      }else{
                                                          _pol = poly;
                                                          [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                      }
                                                  } failure:^(NSError *error) {
                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                      [self showTextHUDWithContent:@"网络错误" andHideTime:1.5];
                                                  }];
}
/**
 *  显示hud并在对应时间隐藏
 *
 *  @param content hud 中的文字描述
 *  @param time    显示的时间
 */
-(void)showTextHUDWithContent:(NSString*)content andHideTime:(CGFloat)time{
    MBProgressHUD * hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    hud.labelText = content;
    [hud show:YES];
    [hud hide:YES afterDelay:time];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
