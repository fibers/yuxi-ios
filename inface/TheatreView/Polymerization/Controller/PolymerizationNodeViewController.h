//
//  PolymerizationNodeViewController.h
//  inface
//
//  Created by 邢程 on 15/8/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Showtype;
@interface PolymerizationNodeViewController : UIViewController
-(void)setShowType:(Showtype *)showType;
@end
