//
//  PolymerizationNodeViewController.m
//  inface
//
//  Created by 邢程 on 15/8/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PolymerizationNodeViewController.h"
#import "Showtype.h"
#import "Nodes.h"
#import "Storys.h"
#import "MenuView.h"
#import "MJRefresh.h"
#import "Polymerization.h"
#import "PolymerizationTableViewCell.h"
#import "PolymerizationFontTableViewCell.h"
#import "MBProgressHUD.h"
#import "Masonry.h"
#import "YXMenuDetailView.h"
#import "Menu.h"
#import "YXButton.h"
#define Menu_Full_File [[NSString documentPath] stringByAppendingPathComponent:@"/menu.tmp"]
@interface PolymerizationNodeViewController ()<UIScrollViewDelegate,MenuViewDelegate,UITableViewDelegate,UITableViewDataSource,PolymerizationTableViewCellDelegate,YXMenuDetailViewDelegate,UpdateStoryInfoDelegate,UpdateSelfStoryInfoDelegate>
@property (nonatomic,strong) Showtype            * showType;
@property (nonatomic,strong) MenuView            * menuView;
@property (nonatomic,strong) UIScrollView        * scrollView;
@property (nonatomic,strong) NSMutableArray      * tableViewArrM;
@property (nonatomic,strong) NSMutableDictionary * polymerizationDicM;
@property (nonatomic,strong) NSMutableArray      * userSelectedNodesArrM;
@property (nonatomic,strong) UIButton            * menuRightBtn;
@property (nonatomic,strong) YXMenuDetailView    * menuDetailV;
@property (nonatomic,strong) NSString            * updateStoryId;

@property (nonatomic,strong) UILabel             * maskHeaderMenuL;
@end

@implementation PolymerizationNodeViewController
-(void)setShowType:(Showtype *)showType{
    _showType = showType;
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    UIImageView * bgIMGv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bigbackground"]];
//    bgIMGv.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
//    [bgIMGv setContentMode:UIViewContentModeScaleAspectFill];
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
//    [self.view addSubview:bgIMGv];
    [self addHeaderView];
    [self initTableViews];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.tableViewArrM enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//        if(self.scrollView.contentOffset.x /ScreenWidth == idx){
//            [obj reloadData];
//        }
//    }];
//    if ((_tableViewArrM != nil) && [_tableViewArrM count]>0) {
//        [self.tableViewArrM enumerateObjectsUsingBlock:^(UITableView* tableView, NSUInteger idx, BOOL * _Nonnull stop) {
//            if(!tableView.headerRefreshing){
//                [tableView headerBeginRefreshing];
//            }
//        }];
//    }
}
/**
 *  重新渲染界面
 */
-(void)reRenderView{
    [self.menuView removeFromSuperview];
    [self.tableViewArrM enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [(UIView*)obj removeFromSuperview];
    }];
    [self.scrollView removeFromSuperview];
    _tableViewArrM = nil;
    _userSelectedNodesArrM = nil;
    _polymerizationDicM = nil;
    _scrollView = nil;
    Menu *menu = [NSKeyedUnarchiver unarchiveObjectWithFile:Menu_Full_File];
    for (Showtype * showType in menu.showtype) {
        if ([showType.name isEqualToString: self.showType.name]) {
            _showType = showType;
            break;
        }
    }
    
    [self addHeaderView];
    [self initTableViews];
    
}
/**
 *  下拉详情菜单中被点击
 *
 *  @param btn 被点击的按钮
 */
-(void)yxMenuDetailViewDidClickButton:(YXButton *)btn{
    Nodes * node = btn.model;
    node.userSelected = !node.isUserSelected;
    Menu *menu = [NSKeyedUnarchiver unarchiveObjectWithFile:Menu_Full_File];
    for (Showtype * showType in menu.showtype) {
        if ([showType.name isEqualToString: self.showType.name]) {
            for(Nodes* oldNode in showType.nodes){
                if ([oldNode.id isEqualToString:node.id]) {
                    NSUInteger index = [showType.nodes indexOfObject:oldNode];
                    [showType.nodes replaceObjectAtIndex:index withObject:node];
                    break;
                }
            }
        }
    }
    [NSKeyedArchiver archiveRootObject:menu toFile:Menu_Full_File];
    
}
/**
 *  添加头部菜单。
 */
-(void)addHeaderView{
    if ([self.showType.nodes count]>1) {
        NSMutableArray * arrM = [NSMutableArray array];
        for(Nodes *node in self.showType.nodes){
            if(node.isUserSelected){
                [arrM addObject:node.name];
            }
        }
        _menuView          = [[MenuView alloc]initWithMneuViewStyle:MenuViewStyleDefault AndTitles:arrM];
        _menuView.frame    = CGRectMake(0, 0, ScreenWidth-28, 40);
        _menuView.delegate = self;
        [self.scrollView setFrame:CGRectMake(0, 40, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
 
    }
    [self.view addSubview:self.scrollView];
    [self.view addSubview:_menuView];
    [self menBtnAddFireIcon:self.showType.nodes];
    
    
    _userSelectedNodesArrM = [NSMutableArray array];
    for(Nodes * node in self.showType.nodes){
        if (node.isUserSelected) {
            [self.userSelectedNodesArrM addObject:node];
        }
    }
    ///如果按钮个数超过十个，添加右侧展示详情三角按钮。
//    if([self.showType.nodes count]>10){
    
        [self.view addSubview: self.menuRightBtn];
        WS(weakSelf);
        [self.menuRightBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [self.menuRightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@28);
            make.height.equalTo(weakSelf.menuView.mas_height);
            make.right.equalTo(weakSelf.view.mas_right);
            make.top.equalTo(weakSelf.menuView.mas_top);
        }];
//    }
   
}

-(void)menBtnAddFireIcon:(NSArray*)nodes{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (UIView * view in [self.menuView subviews]) {
            if ([view isKindOfClass:[UIScrollView class]]) {
                for (UIButton *btn in [view subviews]) {
                    if ([btn isKindOfClass:[UIButton class]]) {
                        for (Nodes * node in nodes) {
                            if([btn.titleLabel.text isEqualToString:node.name]&&[node.ishighlight isEqualToString:@"1"]){
                                UIImageView * imgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"fire"]];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [btn addSubview:imgV];
                                    imgV.center = CGPointMake(CGRectGetWidth(btn.frame), 10);
                                });
                            }
                        }
                    }
                }
            }
        }
    });
    
}
#pragma mark -
#pragma mark delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
   
    int index = (int)(scrollView.contentOffset.x/self.view.frame.size.width);
    CGFloat rate = scrollView.contentOffset.x/self.view.frame.size.width;
    [self.menuView SelectedBtnMoveToCenterWithIndex:index WithRate:rate];
    
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int index = (int)(scrollView.contentOffset.x/self.view.frame.size.width);
    UITableView *tableView = [self.tableViewArrM objectAtIndex:index];
    if(!tableView.headerRefreshing&&![self.polymerizationDicM objectForKey:[NSNumber numberWithInt:index]]){
        [tableView headerBeginRefreshing];
    }
    
    if ([self.tableViewArrM count] >index+1) {
        int nextIndex = index +1;
        UITableView *tableView = [self.tableViewArrM objectAtIndex:nextIndex];
        if(!tableView.headerRefreshing&&![self.polymerizationDicM objectForKey:[NSNumber numberWithInt:nextIndex]]){
            [tableView headerBeginRefreshing];
        }
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if([scrollView isKindOfClass:[UITableView class]])return;
    if (scrollView.contentOffset.x < 0 || scrollView.contentOffset.x > scrollView.contentSize.width )return;
    
    if(decelerate){
        int Page = (int)(scrollView.contentOffset.x/ScreenWidth);
        
        if (Page == 0) {
            [self.menuView selectWithIndex:Page AndOtherIndex:Page + 1 ];
        }else if (Page == self.showType.nodes.count - 1){
            [self.menuView selectWithIndex:Page AndOtherIndex:Page - 1];
        }else{
//            [self.menuView selectWithIndex:Page AndOtherIndex:Page + 1 ];
            [self.menuView selectWithIndex:Page AndOtherIndex:Page - 1];
        }
    }
}

-(void)polymerizationTableViewCellGoBtnClicked:(Storys *)story{
    TheatreGroupToChatViewController * detailViewController = [[TheatreGroupToChatViewController alloc]init];
    detailViewController.theatreid = story.storyid;
    detailViewController.checkGroupId = story.checkgroupid;
    detailViewController.checkTitle = story.checkgrouptitle;
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)MenuViewDelegate:(MenuView *)menuciew WithIndex:(int)index
{
    [self.scrollView setContentOffset:CGPointMake(ScreenWidth*index, 0)];
    NSInteger maxCount = [self.tableViewArrM count];
    if (index +1 <maxCount) {
        UITableView *tableView = [self.tableViewArrM objectAtIndex:index+1];
        if(!tableView.headerRefreshing&&![self.polymerizationDicM objectForKey:[NSNumber numberWithInt:index+1]]){
            [tableView headerBeginRefreshing];
        }
    }
    if(index-1 >0){
        UITableView *tableView = [self.tableViewArrM objectAtIndex:index-1];
        if(!tableView.headerRefreshing&&![self.polymerizationDicM objectForKey:[NSNumber numberWithInt:index-1]]){
            [tableView headerBeginRefreshing];
        }
    }
    UITableView *tableView = [self.tableViewArrM objectAtIndex:index];
    if(!tableView.headerRefreshing&&![self.polymerizationDicM objectForKey:[NSNumber numberWithInt:index]]){
        [tableView headerBeginRefreshing];
    }
}
/**
 *  加载最新的数据（下拉刷新）
 *
 *  @param headerView 被下拉的headerView 根据这个才能对应到被下拉的tableView
 */
-(void)tableViewLoadNewData:(NSInteger )tag{
    
    UITableView * tableV = [self.tableViewArrM objectAtIndex:tag];
//    NSInteger tag = tableV.tag;
    Nodes * node = [self.showType.nodes objectAtIndex:[self getUserSelectedIndexWithFullIndex:tag]];
    
    [HttpTool postWithPath:@"GetStorysByType" params:@{@"index":@"0",
                                                    @"uid":USERID,
                                                    @"count":@"20",
                                                    @"type":node.id}  success:^(id JSON) {
                                                        Polymerization *poly = [Polymerization objectWithKeyValues:JSON];
                                                        if (!poly) {
                                                            return ;
                                                        }
                                                        [self.polymerizationDicM setObject:poly forKey:[NSNumber numberWithInteger:tableV.tag]];
                                                        [tableV reloadData];
                                                        [tableV headerEndRefreshing];
                                                        
                                                    } failure:^(NSError *error) {
                                                        
                                                    }];
}

/**
 *  根据全部的按钮索引，获取被用户选择的按钮索引
 *
 *  @param tag 全部按钮的索引
 *
 *  @return 被用户选择的按钮索引
 */
-(NSInteger)getUserSelectedIndexWithFullIndex:(NSInteger)tag{
    Nodes * nodeInFull = [self.userSelectedNodesArrM objectAtIndex:tag];
    for (Nodes * node in self.showType.nodes) {
        if ([nodeInFull.name isEqualToString:node.name]) {
            return [self.showType.nodes indexOfObject:node];
        }
    }
    return 0;
}
/**
 *  加载历史数据 （上拉刷新）
 *
 *  @param footerView 被上拉的footerView 根据这个才能对应到被上啦的tableView
 */
-(void)tableViewLoadOldData:(NSInteger)tag{
    UITableView * tableView = [self.tableViewArrM objectAtIndex:tag];
    NSInteger index = [self getUserSelectedIndexWithFullIndex:tag];
    Nodes * node = [self.showType.nodes objectAtIndex:index];
    Polymerization * pol = [self.polymerizationDicM objectForKey:[NSNumber numberWithInteger:tableView.tag]];
    [HttpTool postWithPath:@"GetStorysByType" params:@{@"index":[NSNumber numberWithInteger:[pol.storys count]],
                                                       @"uid":USERID,
                                                       @"count":@"20",
                                                       @"type":node.id}  success:^(id JSON) {
                                                           Polymerization *poly = [self.polymerizationDicM objectForKey:[NSNumber numberWithInteger:tableView.tag]];
                                                           if ([poly.storys count]==0) {
                                                               MBProgressHUD * hud = [MBProgressHUD HUDForView:self.view];
                                                               [hud setMode:MBProgressHUDModeText];
                                                               [hud setLabelText:@"没有更多数据了(⊙o⊙)"];
                                                               [hud show:YES];
                                                               [hud hide:YES afterDelay:1.5];
                                                           }
                                                           NSAssert(poly !=nil, @"pol 不能为 nil");
                                                           Polymerization *newPoly = [Polymerization objectWithKeyValues:JSON];
                                                           [poly.storys addObjectsFromArray:newPoly.storys];
                                                           [self.polymerizationDicM setObject:poly forKey:[NSNumber numberWithInteger:tableView.tag]];
                                                           [tableView reloadData];
                                                           [tableView footerEndRefreshing];
                                                           
                                                       } failure:^(NSError *error) {
                                                           MBProgressHUD * hud = [MBProgressHUD HUDForView:self.view];
                                                           [hud setMode:MBProgressHUDModeText];
                                                           [hud setLabelText:@"数据请求错误，请重试"];
                                                           [hud show:YES];
                                                           [hud hide:YES afterDelay:1.5];
                                                       }];
    

}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger tag = tableView.tag;
    Polymerization * polymerization = [self.polymerizationDicM objectForKey:[NSNumber numberWithInteger:tag]];
    if (polymerization) {
        return [polymerization.storys count];
    }else{
        return 0;
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSInteger tag = tableView.tag;
//    Polymerization * polymerization = [self.polymerizationDicM objectForKey:[NSNumber numberWithInteger:tag]];
//    Storys * story = [polymerization.storys objectAtIndex:indexPath.row];
//    if ([story.storyimg isEqualToString:@""]) {
//        return 165;
//    }else{
        return 200;
//    }

}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   Polymerization * pol              = [self.polymerizationDicM objectForKey:[NSNumber numberWithInteger:tableView.tag]];
    NSAssert(pol!=nil, @"pol 不能为空");
   Storys* story                     = [pol.storys objectAtIndex:indexPath.row];
    NSAssert(story!=nil, @"story 不能为空");
    if([story.storyimg isEqualToString:@""] ){//无图
        PolymerizationFontTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PolymerizationFontTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"PolymerizationFontTableViewCell" owner:self options:nil]lastObject];
        }
        [cell setContentWithStory:story];
        return cell;
    }else{
        PolymerizationTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PolymerizationTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"PolymerizationTableViewCell" owner:self options:nil]lastObject];
            cell.delegate = self;
        }
        [cell setContentWithStory:story];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
}

//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    Polymerization * pol = [self.polymerizationDicM objectForKey:[NSNumber numberWithInteger:tableView.tag]];
    Storys *storys = [pol.storys objectAtIndex:indexPath.row];
    NSString *type= storys.type;
    if ([type isEqualToString:@"0"]) {
        //剧0
        WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc] init];
        detailViewController.storyID = storys.storyid;
        _updateStoryId = storys.storyid;
        [detailViewController setStory:storys];
//        detailViewController.delegate = self;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    } else {
        //我的自戏1
        DramaDetailViewController * detailViewController = [[DramaDetailViewController alloc] init];
        detailViewController.storyID=storys.storyid;
        _updateStoryId = storys.storyid;
        [detailViewController setStory:storys];
        detailViewController.commendgroupid = storys.commentgroupid;
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
//        detailViewController.delegate = self;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark -
#pragma mark init
/**
 *  初始化所有tableview，并加到scrollview中，以及数组中。
 */
-(void)initTableViews{
    
    int count = 0;
    for(Nodes * node in self.showType.nodes){
        if(node.isUserSelected)count++;
    }
    for (int i = 0; i<count; i++) {
        UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(ScreenWidth*i, 0, ScreenWidth, ScreenHeight-64-44-40)];
        tableView.tag = i;
        tableView.delegate = self;
        tableView.dataSource = self;
        [tableView setBackgroundColor:[UIColor clearColor]];
        WS(weakSelf);
        __block UITableView *blockTableView = tableView;
        [tableView addHeaderWithCallback:^{
            [weakSelf tableViewLoadNewData:blockTableView.tag];
        }];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [tableView addFooterWithCallback:^{
            [weakSelf tableViewLoadOldData:blockTableView.tag];
        }];
        [self.scrollView addSubview:tableView];
        [self.tableViewArrM addObject:tableView];
        if (i<3) {
            [tableView headerBeginRefreshing];
        }
        
    }
}
-(UIButton *)menuRightBtn{
    if (!_menuRightBtn) {
        _menuRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_menuRightBtn setImage:[UIImage imageNamed:@"menu_right_down_btn"] forState:UIControlStateNormal];
        [_menuRightBtn setImage:[UIImage imageNamed:@"menu_right_up_btn"] forState:UIControlStateSelected];
        [_menuRightBtn setBackgroundColor:[UIColor colorWithHexString:@"ebebeb"]];
        [_menuRightBtn addTarget:self action:@selector(showMenuBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _menuRightBtn;
}

-(YXMenuDetailView *)menuDetailV{
    if (!_menuDetailV) {
        _menuDetailV = [[[NSBundle mainBundle]loadNibNamed:@"YXMenuDetailView" owner:self options:nil]lastObject];
        _menuDetailV.delegate = self;
         [self.view addSubview:_menuDetailV];
        WS(weakSelf);
        [_menuDetailV mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.menuView.mas_bottom);
            make.left.equalTo(weakSelf.view.mas_left);
            make.width.equalTo(weakSelf.view.mas_width);
//            make.height.equalTo(weakSelf.view.mas_height);
            make.bottom.equalTo(weakSelf.view.mas_bottom);
        }];
        Menu *menu = [NSKeyedUnarchiver unarchiveObjectWithFile:Menu_Full_File];
        for (Showtype * showType in menu.showtype) {
            if ([showType.name isEqualToString: self.showType.name]) {
                [_menuDetailV setContentWithMenus:showType.nodes];
            }
        }
    }
    return _menuDetailV;
}
-(void)showMenuBtnClicked:(UIButton *)btn{

        if (!btn.isSelected) {
            [self.maskHeaderMenuL setHidden:NO];
            [self.menuDetailV setHidden:NO];
            [self.view bringSubviewToFront:self.maskHeaderMenuL];
        }else{
            [self.maskHeaderMenuL setHidden:YES];
            [self.menuDetailV setHidden:YES];
            [self reRenderView];
            [self.view bringSubviewToFront:self.menuDetailV];
            [self.view bringSubviewToFront:self.menuRightBtn];
            
            WS(weakSelf);
            [_menuDetailV mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(weakSelf.menuView.mas_bottom);
                make.left.equalTo(weakSelf.view.mas_left);
                make.width.equalTo(weakSelf.view.mas_width);
                make.height.equalTo(weakSelf.view.mas_height);
            }];
        }
    
    _userSelectedNodesArrM = [NSMutableArray array];
//    [self.menuView addSubview: self.menuRightBtn];
    for(Nodes * node in self.showType.nodes){
        if (node.isUserSelected) {
            [self.userSelectedNodesArrM addObject:node];
        }
    }
    
    btn.selected=!btn.isSelected;
}
-(UIScrollView *)scrollView{
    if (!_scrollView) {
        int count = 0;
        for(Nodes * node in self.showType.nodes){
            if (node.isUserSelected) {
                count++;
            }
        }
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth , ScreenHeight-64-44)];
        [_scrollView setContentSize:CGSizeMake(ScreenWidth* count, ScreenHeight-64-44)];
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate =self;
        [_scrollView setBounces:NO];
        
    }
    return _scrollView;
    
}
-(UILabel *)maskHeaderMenuL{
    if (!_maskHeaderMenuL) {
        _maskHeaderMenuL =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth - 28, 35)];
        [_maskHeaderMenuL setBackgroundColor:[UIColor colorWithHexString:@"ededed"]];
        [_maskHeaderMenuL setTextColor:[UIColor colorWithHexString:@"a9a8a6"]];
        [_maskHeaderMenuL setFont:[UIFont systemFontOfSize:12]];
        [_maskHeaderMenuL setText:@"   我的频道"];
        [self.view addSubview:_maskHeaderMenuL];
    }
    return _maskHeaderMenuL;
}
-(NSMutableArray *)tableViewArrM{
    if (!_tableViewArrM) {
        _tableViewArrM = [NSMutableArray array];
    }
    return _tableViewArrM;
}
-(NSMutableDictionary *)polymerizationDicM{
    if (!_polymerizationDicM) {
        _polymerizationDicM = [NSMutableDictionary dictionary];
    }
    return _polymerizationDicM;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)updateStoryInfo:(NSDictionary*)storyinfo storyid:(NSString*)storyid
{
    
}
@end
