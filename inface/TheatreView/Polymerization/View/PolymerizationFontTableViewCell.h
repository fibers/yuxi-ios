//
//  PolymerizationFontTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/23.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerticalAlignmentLabel.h"
@interface PolymerizationFontTableViewCell : UITableViewCell
-(void)setContentWithStory:(Storys *)story;
@end
