//
//  YXHomeTableviewHeaderView.m
//  inface
//
//  Created by 邢程 on 15/8/21.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXHomeTableviewHeaderView.h"
#import "Selectedcontents.h"
@interface YXHomeTableviewHeaderView()
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property(strong,nonatomic)Selectedcontents * content;
@end

@implementation YXHomeTableviewHeaderView
- (IBAction)moreBtnClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(yXHomeTableviewHeaderDidClickedWithSelectedContents:)]) {
        [self.delegate yXHomeTableviewHeaderDidClickedWithSelectedContents:self.content];
    }
}
-(void)setContentWithSelectedContent:(Selectedcontents*)content{
    self.titleL.text = content.selectedtypetitle;
    self.content = content;
}
@end
