//
//  YXCustomMenuView.m
//  inface
//
//  Created by 邢程 on 15/11/2.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXCustomMenuView.h"
#import "YXButton.h"
#import "CustomMenuModel.h"
#import "Masonry.h"
#define BTN_W @60
#define BTN_H @24
#define BTN_Margin_L 13
#define View_Margin_L 14

@interface YXCustomMenuView()
@property(strong,nonatomic)NSMutableArray * menuModelArr;
@property(strong,nonatomic)NSMutableArray * btnArrM;
@property(assign,nonatomic)CGFloat viewH;
@property(assign,nonatomic)NSInteger maxCount;
@property(assign,nonatomic)BOOL isShowAlertView;
@property(assign,nonatomic)BOOL isShowAddBtn;
@end
@implementation YXCustomMenuView
-(CGFloat)viewH{
    return _viewH;
}
-(void)setShowAddBtn:(BOOL)ishowBtn{
    _isShowAddBtn = ishowBtn;

}
-(void)setMenusWithCustomMenuModelArr:(NSArray *)menuModelArr{
    _viewH = 0;
    if ([menuModelArr count]>0) {
        _menuModelArr = [NSMutableArray arrayWithArray:menuModelArr];
        if(_isShowAddBtn){
            CustomMenuModel * m = [[CustomMenuModel alloc]init];
            m.isAddBtn = YES;
            [_menuModelArr addObject:m];
        }
        [self setCustomColumnView];
    }
}
-(void)setMaxSelectedBtnsCount:(NSInteger)maxCount andShouldShowAlertView:(BOOL)isShow{
    _maxCount = maxCount;
    _isShowAlertView = isShow;

}
-(void)setCustomColumnView{
    int btnW = ([BTN_W intValue]+ BTN_Margin_L);
    int btnNum = ScreenWidth/btnW;
    CGFloat leftMargin = (ScreenWidth - ([BTN_W intValue]*btnNum) - (BTN_Margin_L *(btnNum-1)))/2;
    for (int i=0; i<[_menuModelArr count]; i++) {
        YXButton * btn = [self getCustomButtonWithCustomMenuModel:[_menuModelArr objectAtIndex:i]];
        btn.tag = i;
        [self addSubview:btn];
        [self.btnArrM addObject:btn];
        WS(weakSelf);
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i==0) {
                make.left.equalTo(weakSelf.mas_left).offset(leftMargin);
                make.top.equalTo(weakSelf.mas_top).offset(View_Margin_L);
                make.width.equalTo(BTN_W);
                make.height.equalTo(BTN_H);
            }else{
                YXButton * lastBtn = [weakSelf.btnArrM objectAtIndex:i-1];
                int hang = (ScreenWidth -View_Margin_L) / ([BTN_W intValue]+BTN_Margin_L-1);
                if((i) %hang ==0){
                    make.left.equalTo(weakSelf.mas_left).offset(leftMargin);
                    make.top.equalTo(lastBtn.mas_bottom).offset(12);
                    make.width.equalTo(BTN_W);
                    make.height.equalTo(BTN_H);
                }else{
                    make.left.equalTo(lastBtn.mas_right).offset(BTN_Margin_L);
                    make.top.equalTo(lastBtn.mas_top);
                    make.width.equalTo(BTN_W);
                    make.height.equalTo(BTN_H);
                    
                }
            }
        }];
        
    }
     int totalMenuBtnRow = ceil(([self.menuModelArr count] / btnNum));
    _viewH = totalMenuBtnRow* (12+[BTN_H floatValue])+64+20;
}

/**
 *  创建一个button
 *
 *  @param node Nodes class
 *
 *  @return new custom Button
 */
-(YXButton *)getCustomButtonWithCustomMenuModel:(CustomMenuModel*)model{
    if(model.isAddBtn){///是否为添加按钮
        YXButton * newBtn         = [YXButton buttonWithType:UIButtonTypeCustom];
        [newBtn setAdjustsImageWhenDisabled:NO];
        [newBtn setAdjustsImageWhenHighlighted:NO];
        [newBtn setBackgroundImage:[self createImageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [newBtn setTitleColor:[UIColor colorWithHexString:@"656464"] forState:UIControlStateNormal];
        [newBtn setTitle:@"+" forState:UIControlStateNormal];
        [newBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
        newBtn.layer.cornerRadius = 4;
        [newBtn setClipsToBounds:YES];
        newBtn.layer.borderColor  = [UIColor colorWithHexString:@"d1d1d1"].CGColor;
        newBtn.layer.borderWidth  = 0.5;
        newBtn.model              = model;
        [newBtn addTarget:self action:@selector(addBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        return  newBtn;
    }else{
        YXButton * newBtn         = [YXButton buttonWithType:UIButtonTypeCustom];
        [newBtn setAdjustsImageWhenDisabled:NO];
        [newBtn setAdjustsImageWhenHighlighted:NO];
        [newBtn setBackgroundImage:[self createImageWithColor:[UIColor colorWithHexString:@"eb71a8"]] forState:UIControlStateSelected];
        [newBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        [newBtn setBackgroundImage:[self createImageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [newBtn setTitleColor:[UIColor colorWithHexString:@"656464"] forState:UIControlStateNormal];
        [newBtn setTitle:model.name forState:UIControlStateNormal];
        [newBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
        newBtn.layer.cornerRadius = 4;
        [newBtn setClipsToBounds:YES];
        newBtn.selected           = model.isSelected;
        newBtn.layer.borderColor  = [UIColor colorWithHexString:@"d1d1d1"].CGColor;
        newBtn.layer.borderWidth  = 0.5;
        newBtn.model              = model;
        [newBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        return  newBtn;
    }
    
}
- (UIImage*) createImageWithColor: (UIColor*) color{
    CGRect rect          = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage    = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}
-(void)btnClicked:(YXButton *)btn{
    if (!btn.isSelected) {
        __block int count = 0;
         _selectedButtons = [NSMutableArray array];
        [self.btnArrM enumerateObjectsUsingBlock:^(YXButton*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.isSelected) {
                [_selectedButtons addObject:obj];
                count++;
            }
        }];
        if (count < _maxCount) {
            btn.selected = !btn.isSelected;
            CustomMenuModel * model = (CustomMenuModel*)btn.model;
            model.isSelected = btn.isSelected;
            [_selectedButtons addObject:btn];
        }else{
            if (_isShowAlertView) {
                UIAlertView * alertV = [[UIAlertView alloc]initWithTitle:@"O__O\"…" message:[NSString stringWithFormat:@"说好的，最多只可以可选 %ld 项 T^T",_maxCount] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertV show];
            }
        }
    }else{
        CustomMenuModel * model = (CustomMenuModel*)btn.model;
        if (model.isIrreversible) {
            [[[UIAlertView alloc]initWithTitle:@"提示" message:[NSString stringWithFormat:@"%@为必选！",model.name]delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
        }else{
            [_selectedButtons removeObject:btn];
            btn.selected = !btn.isSelected;
            
            model.isSelected = btn.isSelected;
        }
    }
    
}
-(void)addBtnClicked:(YXButton*)btn{
    if ([self.delegate respondsToSelector:@selector(addBtnClicked)]) {
        [self.delegate addBtnClicked];
    }
    
}
-(NSMutableArray *)btnArrM{
    if (!_btnArrM) {
        _btnArrM = [NSMutableArray array];
    }
    return _btnArrM;
}
@end
