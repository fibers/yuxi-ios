//
//  PolymerizationTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/5/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//百老汇的cell
#import <UIKit/UIKit.h>
@class Storys;
@protocol PolymerizationTableViewCellDelegate<NSObject>
@optional
-(void)polymerizationTableViewCellGoBtnClicked:(Storys *)story;
@end
@interface PolymerizationTableViewCell : UITableViewCell
-(void)setContentWithStory:(Storys *)story;
@property(nonatomic,weak)id<PolymerizationTableViewCellDelegate> delegate;
@end
