//
//  YXMenuDetailView.m
//  inface
//
//  Created by 邢程 on 15/8/31.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXMenuDetailView.h"
#import "Nodes.h"
#import "Masonry.h"
#import "YXButton.h"
#define BTN_W @60
#define BTN_H @24
#define BTN_Margin_L 13
#define View_Margin_L 14
@interface YXMenuDetailView()
@property(nonatomic,strong)NSArray * menuArr;
@property (weak, nonatomic) IBOutlet UIView *headerDetailBGV;
@property (weak, nonatomic) IBOutlet UIScrollView *menuDetailScrollBGV;
@property (strong,nonatomic) NSMutableArray * btnArrM;
@property (strong,nonatomic) NSMutableDictionary * dicM;
@end
@implementation YXMenuDetailView

-(void)setContentWithMenus:(NSArray *)menuArr{
    if ([[menuArr lastObject]isKindOfClass:[Nodes class]]) {
        _menuArr = menuArr;
        [self setDefaultColumnHeader];
        [self setCustomColumnView];
    }
    
}
-(NSMutableArray *)btnArrM{
    if (!_btnArrM) {
        _btnArrM = [NSMutableArray array];
    }
    return _btnArrM;
}
-(void)setDefaultColumnHeader{
    int btnW = ([BTN_W intValue]+ BTN_Margin_L);
    int btnNum = ScreenWidth/btnW;
    CGFloat leftMargin = (ScreenWidth - ([BTN_W intValue]*btnNum) - (BTN_Margin_L *(btnNum-1)))/2;
    for (int i=0; i<([_menuArr count]>3?3:[_menuArr count]); i++) {
        YXButton * btn = [self getCustomButtonWithNodeModel:[_menuArr objectAtIndex:i]];
        btn.selected = YES;
        [self.headerDetailBGV addSubview:btn];
        [self.btnArrM addObject:btn];
        WS(weakSelf);
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i==0) {
                make.left.equalTo(weakSelf.headerDetailBGV.mas_left).offset(leftMargin);
                make.top.equalTo(weakSelf.headerDetailBGV.mas_top).offset(View_Margin_L);
                make.width.equalTo(BTN_W);
                make.height.equalTo(BTN_H);
            }else{
                YXButton * lastBtn = [weakSelf.btnArrM objectAtIndex:i-1];
                make.left.equalTo(lastBtn.mas_right).offset(BTN_Margin_L);
                make.top.equalTo(weakSelf.headerDetailBGV.mas_top).offset(View_Margin_L);
                make.width.equalTo(BTN_W);
                make.height.equalTo(BTN_H);
            }
        }];
        
    }
}

-(void)setCustomColumnView{
    int btnW = ([BTN_W intValue]+ BTN_Margin_L);
    int btnNum = ScreenWidth/btnW;
    CGFloat leftMargin = (ScreenWidth - ([BTN_W intValue]*btnNum) - (BTN_Margin_L *(btnNum-1)))/2;
    for (int i=3; i<[_menuArr count]; i++) {
        YXButton * btn = [self getCustomButtonWithNodeModel:[_menuArr objectAtIndex:i]];
        btn.tag = i;
//        if (btn.tag <7) {
//            btn.selected = YES;
//        }
        [self.menuDetailScrollBGV addSubview:btn];
        [self.btnArrM addObject:btn];
        WS(weakSelf);
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i-3==0) {
                make.left.equalTo(weakSelf.menuDetailScrollBGV.mas_left).offset(leftMargin);
                make.top.equalTo(weakSelf.menuDetailScrollBGV.mas_top).offset(View_Margin_L);
                make.width.equalTo(BTN_W);
                make.height.equalTo(BTN_H);
            }else{
                YXButton * lastBtn = [weakSelf.btnArrM objectAtIndex:i-1];
                int hang = (ScreenWidth -View_Margin_L) / ([BTN_W intValue]+BTN_Margin_L-1);
                if((i-3) %hang ==0){
                    make.left.equalTo(weakSelf.menuDetailScrollBGV.mas_left).offset(leftMargin);
                    make.top.equalTo(lastBtn.mas_bottom).offset(12);
                    make.width.equalTo(BTN_W);
                    make.height.equalTo(BTN_H);
                }else{
                    make.left.equalTo(lastBtn.mas_right).offset(BTN_Margin_L);
                    make.top.equalTo(lastBtn.mas_top);
                    make.width.equalTo(BTN_W);
                    make.height.equalTo(BTN_H);
                    
                }
            }
        }];
        
    }
    if([self.menuArr count]>3){
        int totalMenuBtnRow = ceil(([self.menuArr count] / btnNum));
        self.menuDetailScrollBGV.contentSize = CGSizeMake(ScreenWidth, totalMenuBtnRow* (12+[BTN_H floatValue])+64+20);
    }
    
}
/**
 *  创建一个button
 *
 *  @param node Nodes class
 *
 *  @return new custom Button
 */
-(YXButton *)getCustomButtonWithNodeModel:(Nodes*)node{
    YXButton * newBtn         = [YXButton buttonWithType:UIButtonTypeCustom];
    [newBtn setAdjustsImageWhenDisabled:NO];
    [newBtn setAdjustsImageWhenHighlighted:NO];
    [newBtn setBackgroundImage:[self createImageWithColor:[UIColor colorWithHexString:@"eb71a8"]] forState:UIControlStateSelected];
    [newBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

    [newBtn setBackgroundImage:[self createImageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [newBtn setTitleColor:[UIColor colorWithHexString:@"656464"] forState:UIControlStateNormal];
    [newBtn setTitle:node.name forState:UIControlStateNormal];
    [newBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    newBtn.layer.cornerRadius = 4;
    [newBtn setClipsToBounds:YES];
    newBtn.selected = node.isUserSelected;
//    newBtn.layer.borderColor  = [UIColor colorWithHexString:@"d1d1d1"].CGColor;
//    newBtn.layer.borderWidth  = 0.5;
    newBtn.model = node;
    [newBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    return  newBtn;
}

-(void)btnClicked:(YXButton *)btn{
    if(btn.tag <3)return;
    btn.selected = !btn.isSelected;
    if([self.delegate respondsToSelector:@selector(yxMenuDetailViewDidClickButton:)]){
        [self.delegate yxMenuDetailViewDidClickButton:btn];
    }

}
- (UIImage*) createImageWithColor: (UIColor*) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}
@end
