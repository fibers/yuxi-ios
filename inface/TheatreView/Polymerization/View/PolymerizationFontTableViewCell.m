//
//  PolymerizationFontTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/7/23.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PolymerizationFontTableViewCell.h"
#import "Storys.h"
#import "NSString+YXExtention.h"
#import "NSString+Extention.h"
@interface PolymerizationFontTableViewCell()
@property (strong, nonatomic) IBOutlet UILabel *WhoLabel;//谁的剧
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *whoLabelCW;
@property (strong, nonatomic) IBOutlet UIImageView *IconImageView;//头像
@property (strong, nonatomic) IBOutlet VerticalAlignmentLabel *BigDetailLabel;//标题
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeBtnCW;
@property (strong, nonatomic) IBOutlet VerticalAlignmentLabel *DetailLabel;//详情
@property (weak, nonatomic) IBOutlet UIButton *wordCountBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *wordCountCW;
@property (weak, nonatomic) IBOutlet UIButton *likeCountBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likeCountBtnCW;
@property (weak, nonatomic) IBOutlet UIButton *commentCountBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentCountBtnWC;
@property (strong, nonatomic) IBOutlet UIButton *typebtn1;//类别1
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typeBtnCW;
@property (weak, nonatomic) IBOutlet UIView *BackgroundView;

@property (weak, nonatomic) IBOutlet UIImageView *isVipImage;

@end
@implementation PolymerizationFontTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
//    self.WhoLabel.textColor=BLACK_CONTENT_COLOR;
//    self.BigDetailLabel.textColor=BLACK_COLOR;
//    self.DetailLabel.textColor=BLACK_COLOR;
//    
//    self.BackgroundView.layer.borderWidth = 0;
//    self.BackgroundView.layer.borderColor = BLUECOLOR.CGColor;
//    self.BackgroundView.layer.cornerRadius = 4;
//    self.BackgroundView.clipsToBounds = YES;
    [self.timeBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.wordCountBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.likeCountBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.commentCountBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    self.IconImageView.contentMode=UIViewContentModeScaleAspectFill;
    self.IconImageView.clipsToBounds=YES;
    self.IconImageView.layer.cornerRadius = CGRectGetHeight(self.IconImageView.bounds)/2;

    
    [self.BigDetailLabel setVerticalAlignment:VerticalAlignmentTop];
    [self.DetailLabel setVerticalAlignment:VerticalAlignmentTop];
    

    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
-(void)setContentWithStory:(Storys *)story{
    
    
    NSString * typestr = story.category;
    typestr = [typestr stringByReplacingOccurrencesOfString:@"," withString:@"、"];
    [self.typebtn1 setTitle:typestr forState:UIControlStateNormal];
    self.typeBtnCW.constant =   [typestr sizeWithfont:[UIFont systemFontOfSize:12] MaxX:100].width;
    NSURL * url = [NSURL URLWithString:[story.creatorportrait stringOfW60ImgUrl]];
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
    int  isVip = [story.creatorportrait isSignedUser];
    if (isVip == 0) {
        [_isVipImage setHidden:YES];
    }else{
        if (isVip == 1) {
            _isVipImage.hidden = NO;
            _isVipImage.image = image;
        }
        if (isVip == 2||isVip == 3) {
            _isVipImage.hidden = NO;
            _isVipImage.image = officalImage;
        }
    }
    [self.IconImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
    self.IconImageView.contentMode=UIViewContentModeScaleAspectFill;
    self.IconImageView.layer.cornerRadius =  CGRectGetHeight(self.IconImageView.bounds)/2;
    
    self.IconImageView.clipsToBounds=YES;
    //        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapfriendpage:)];
    //        [self.IconImageView addGestureRecognizer:tapGestureRecognizer];
    UIButton * tapButton = [[UIButton alloc]initWithFrame:self.IconImageView.frame];
//    [tapButton addTarget:self action:@selector(tapfriendpage) forControlEvents:UIControlEventTouchUpInside];
//    tapButton.tag = indexPath.row +10000;

    tapButton.backgroundColor = [UIColor clearColor];
    [self addSubview:tapButton];
    [self bringSubviewToFront:tapButton];
    
    self.BigDetailLabel.text=story.title;
    
    self.DetailLabel.text=story.intro;
    
    NSString *createtime=story.updatetime;
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[createtime doubleValue]];
    [self.timeBtn setTitle:[date promptDateString] forState:UIControlStateNormal];
    self.timeBtnCW.constant = [[date promptDateString] sizeWithfont:[UIFont systemFontOfSize:10] MaxX:100].width +20+7;
    
    NSString *ispraisedbyuser=story.ispraised;
    if ([ispraisedbyuser isEqualToString:@"1"]) {
        [self.likeCountBtn setSelected:YES];
    } else {
        [self.likeCountBtn setSelected:NO];
    }
    [self.likeCountBtn setTitle:story.praisenum forState:UIControlStateNormal];
    self.likeCountBtnCW.constant = [story.praisenum sizeWithfont:[UIFont systemFontOfSize:11] MaxX:100].width+20+5;
//    self.PeopleCountLabel.text=story.commentnum;
    [self.commentCountBtn setTitle:story.commentnum forState:UIControlStateNormal];
    self.commentCountBtnWC.constant = [story.commentnum sizeWithfont:[UIFont systemFontOfSize:10] MaxX:100].width+20+5;
    
    NSString *wordcounts=story.wordcounts;
//    self.FontLabel.text=[NSString stringWithFormat:@"字数:%@",wordcounts];
    [self.wordCountBtn setTitle:wordcounts forState:UIControlStateNormal];
    self.wordCountCW.constant = [wordcounts sizeWithfont:[UIFont systemFontOfSize:10] MaxX:100].width+100;
    NSString *creatorname=story.creatorname;
    NSString * type = story.type;
    if ([type isEqualToString:@"0"]) {
        self.WhoLabel.text=[NSString stringWithFormat:@"%@的剧",creatorname];

    }else{
        self.WhoLabel.text=[NSString stringWithFormat:@"%@的自戏",creatorname];
        
    }
    self.whoLabelCW.constant = [self.WhoLabel.text sizeWithfont:[UIFont systemFontOfSize:12] MaxX:300].width+50;
    

    
}
@end
