//
//  YXCustomMenuView.h
//  inface
//
//  Created by 邢程 on 15/11/2.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol YXCustomMenuViewDelegate<NSObject>
@optional
-(void)addBtnClicked;
@end
@interface YXCustomMenuView : UIView
/**
 *  设置菜单内容
 *
 *
 *  @param menuModelArr<CustomMenuModel>数组，如果数组里面不是CustomMenuModel会Crash。
 */
-(void)setMenusWithCustomMenuModelArr:(NSArray *)menuModelArr;
/**
 *  设置最大可选择的菜单数目
 *
 *  @param maxCount 可选择的最大值
 *  @param isShow   是否弹出提示信息 默认NO
 */
-(void)setMaxSelectedBtnsCount:(NSInteger)maxCount andShouldShowAlertView:(BOOL)isShow;
/**
 *  根据屏幕宽度算出的当前View的高度
 *
 *  @return view高度
 */
-(CGFloat)viewH;
/**
 *  被选中的Button<YXButton>数组
 */
@property(nonatomic,strong,readonly)NSMutableArray * selectedButtons;
/**
 *  是否显示增加按钮，默认为NO 在setMenusWithCustomMenuModelArr:前调用
 *
 *  @param ishowBtn 是否显示 默认NO
 */
-(void)setShowAddBtn:(BOOL)ishowBtn;
@property(nonatomic,weak)id<YXCustomMenuViewDelegate>delegate;
@end
