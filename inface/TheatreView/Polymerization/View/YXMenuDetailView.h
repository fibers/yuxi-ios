//
//  YXMenuDetailView.h
//  inface
//
//  Created by 邢程 on 15/8/31.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YXButton;
@protocol YXMenuDetailViewDelegate<NSObject>
@optional
-(void)yxMenuDetailViewDidClickButton:(YXButton*)btn;
@end
@interface YXMenuDetailView : UIView
-(void)setContentWithMenus:(NSArray *)menuArr;
@property(nonatomic,weak)id<YXMenuDetailViewDelegate>delegate;
@end
