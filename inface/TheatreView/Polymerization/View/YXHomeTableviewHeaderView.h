//
//  YXHomeTableviewHeaderView.h
//  inface
//
//  Created by 邢程 on 15/8/21.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Selectedcontents;
@protocol YXHomeTableviewHeaderViewDelegate <NSObject>
@optional
-(void)yXHomeTableviewHeaderDidClickedWithSelectedContents:(Selectedcontents*)content;
@end
@interface YXHomeTableviewHeaderView : UIView
-(void)setContentWithSelectedContent:(Selectedcontents*)content;
@property(nonatomic,weak)id<YXHomeTableviewHeaderViewDelegate>delegate;
@end
