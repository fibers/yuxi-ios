//
//  PolymerizationTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/5/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PolymerizationTableViewCell.h"
#import "VerticalAlignmentLabel.h"
#import "Storys.h"
#import "NSString+Extention.h"
#import "NSString+YXExtention.h"
@interface PolymerizationTableViewCell()
@property (strong, nonatomic) IBOutlet UILabel *WhoLabel;//谁的剧
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *whoLabelCW;
@property (strong, nonatomic) IBOutlet UIImageView *IconImageView;//头像
@property (strong, nonatomic) IBOutlet UIImageView *BigImageView;//中间大图
@property (strong, nonatomic) IBOutlet VerticalAlignmentLabel *BigDetailLabel;//标题
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;

///字数
@property (weak, nonatomic) IBOutlet UIButton *fontBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fontBtnCW;
@property (strong, nonatomic) IBOutlet VerticalAlignmentLabel *DetailLabel;//详情

///喜欢
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likeBtnCW;


@property (weak, nonatomic) IBOutlet UIButton *gotoGroups;
- (IBAction)gotochat:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *typebtn1;//类别1
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typebtn1CW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeBtnCW;
//@property (weak, nonatomic) IBOutlet UILabel *XianLabel;
@property (weak, nonatomic) IBOutlet UIView *BackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *vIcon;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentBtnCW;
@property (weak, nonatomic) IBOutlet UIView *footerBGV;

@property (strong,nonatomic)Storys * story;
@end
@implementation PolymerizationTableViewCell

- (void)awakeFromNib {
//    self.WhoLabel.textColor=BLACK_CONTENT_COLOR;
//    self.BigDetailLabel.textColor=BLACK_COLOR;
//    self.DetailLabel.textColor=BLACK_COLOR;
//    
//    self.BackgroundView.layer.borderWidth = 0;
//    self.BackgroundView.layer.borderColor = BLUECOLOR.CGColor;
//    self.BackgroundView.layer.cornerRadius = 4;
//    self.BackgroundView.clipsToBounds = YES;
    [self.timeBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.fontBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.likeBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.commentBtn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    self.IconImageView.contentMode=UIViewContentModeScaleAspectFill;
    self.IconImageView.clipsToBounds=YES;
    self.IconImageView.layer.cornerRadius = CGRectGetHeight(self.IconImageView.bounds)/2;
    
    [self.BigDetailLabel setVerticalAlignment:VerticalAlignmentTop];
    [self.DetailLabel setVerticalAlignment:VerticalAlignmentTop];
    
//    self.XianLabel.backgroundColor=XIAN_COLOR;
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
- (void)fadeLayer:(CALayer *)layer {
    CATransition *transition  = [CATransition animation];
    transition.duration       = .5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type           = kCATransitionFade;
    [layer addAnimation:transition forKey:@"fade"];
}
-(void)setContentWithStory:(Storys *)story{
    //有图
    _story = story;
    NSString * typestr = story.category;
    NSURL * url = [NSURL URLWithString:[story.creatorportrait stringOfW60ImgUrl]];
    NSURL * storyimg = [NSURL URLWithString:[story.storyimg stringOfW250ImgUrl]];
    NSMutableString *typeStrM = [NSMutableString stringWithString:typestr];
    [self.typebtn1 setTitle:[typeStrM stringByReplacingOccurrencesOfString:@"," withString:@"、"] forState:UIControlStateNormal];
    CGSize size = [self.typebtn1.titleLabel.text sizeWithFont:[UIFont systemFontOfSize:12] maxSize:CGSizeMake(200, 15)];
    self.typebtn1CW.constant = size.width + size.height+10;
    [self.IconImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] options:SDWebImageCacheMemoryOnly progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        NSLog(@"%.2f", (float)receivedSize / (float)expectedSize);
        [self fadeLayer:self.IconImageView.layer];
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    self.IconImageView.contentMode=UIViewContentModeScaleAspectFill;
    self.IconImageView.layer.cornerRadius =  CGRectGetHeight(self.IconImageView.bounds)/2;
    
    self.IconImageView.clipsToBounds=YES;
    
    [self.BigImageView sd_setImageWithURL:storyimg placeholderImage:[UIImage imageNamed:@"juxiangqing"] options:SDWebImageCacheMemoryOnly progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        [self fadeLayer:self.BigImageView.layer];
    } completed:NULL];
    self.BigImageView.contentMode=UIViewContentModeScaleAspectFill;
    self.BigImageView.clipsToBounds=YES;
    
    self.BigDetailLabel.text=story.title;
    
    self.DetailLabel.text=story.intro;
    [self.commentBtn setTitle:story.commentnum forState:UIControlStateNormal];
    self.commentBtnCW.constant = [story.commentnum sizeWithfont:[UIFont systemFontOfSize:10] MaxX:100].width+20+5;
    
    NSString *createtime=story.updatetime;
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[createtime doubleValue]];
    
    [self.timeBtn setTitle:[date promptDateString] forState:UIControlStateNormal];
    self.timeBtnCW.constant =[[date promptDateString]sizeWithfont:[UIFont systemFontOfSize:10] MaxX:100].width+20+7;
    NSString *ispraisedbyuser=story.ispraised;
    if ([ispraisedbyuser isEqualToString:@"1"]) {
        [self.likeBtn setSelected:YES];
    } else {
        [self.likeBtn setSelected:NO];
    }
    
    [self.likeBtn setTitle:story.praisenum forState:UIControlStateNormal];
    self.likeBtnCW.constant = [story.praisenum sizeWithfont:[UIFont systemFontOfSize:11] MaxX:100].width+20+5;
    
    NSString *wordcounts=story.wordcounts;
    //字数
    [self.fontBtn setTitle:[NSString stringWithFormat:@"%@",wordcounts] forState:UIControlStateNormal];
    self.fontBtnCW.constant = [wordcounts sizeWithfont:[UIFont systemFontOfSize:10] MaxX:100].width+20;
    
    NSString *creatorname=story.creatorname;
    NSString * type = story.type;
    if ([type isEqualToString:@"0"]) {
        self.WhoLabel.text=[NSString stringWithFormat:@"%@的剧",creatorname];
        self.whoLabelCW.constant = [self.WhoLabel.text sizeWithfont:[UIFont systemFontOfSize:12] MaxX:300].width+50;
        //无论是否是群成员都是可以进群的，如果不是群成员，就将自己加进去
        self.gotoGroups.hidden = NO;
        [self.gotoGroups setImage:[UIImage imageNamed:@"selectStory"] forState:UIControlStateHighlighted];
        [self bringSubviewToFront:self.gotoGroups];
//        [self.gotoGroups addTarget:self action:@selector(selectStoryGroups:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        self.WhoLabel.text=[NSString stringWithFormat:@"%@的自戏",creatorname];
        self.gotoGroups.hidden = YES;
        self.gotoGroups.userInteractionEnabled = NO;
        self.whoLabelCW.constant = [self.WhoLabel.text sizeWithfont:[UIFont systemFontOfSize:12] MaxX:300].width+50;

        
    }
    
    
    int  isVip = [self.story.creatorportrait isSignedUser];
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];

    if (isVip == 0) {
        [self.vIcon setHidden:YES];
    }else{
        if (isVip == 1) {
            self.vIcon.hidden = NO;
            self.vIcon.image = image;
        }
        if (isVip == 2||isVip == 3) {
            self.vIcon.hidden = NO;
            self.vIcon.image = officalImage;
        }
    }

}

- (IBAction)gotochat:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(polymerizationTableViewCellGoBtnClicked:)]) {
        [self.delegate polymerizationTableViewCellGoBtnClicked:self.story];
    }
}
@end
