//
//  Deletestorys.h
//  inface
//
//  Created by 邢程 on 15/8/19.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@interface Deletestorys : NSObject

@property (nonatomic, copy) NSString *storyid;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *updatetime;

@end