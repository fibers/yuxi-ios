//
//  Polymerization.h
//  inface
//
//  Created by 邢程 on 15/8/19.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@class Deletestorys,Storys;
@interface Polymerization : NSObject

@property (nonatomic, copy) NSString *result;

@property (nonatomic, strong) NSArray *deletestorys;

@property (nonatomic, strong) NSMutableArray *storys;
@end



