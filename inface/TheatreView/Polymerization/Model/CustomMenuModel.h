//
//  CustomMenuModel.h
//  inface
//
//  Created by 邢程 on 15/11/2.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomMenuModel : NSObject
@property (copy,nonatomic  ) NSString * ID;
@property (copy,nonatomic  ) NSString * name;
///是否被选择
@property (assign,nonatomic) BOOL     isSelected;
///是否为添加按钮
@property (assign,nonatomic) BOOL     isAddBtn;
//用户添加
@property (nonatomic,assign) BOOL     isUserAdd;
///不能取消
@property (nonatomic,assign) BOOL     isIrreversible;
@end
