//
//  Selectedcontents.h
//  inface
//
//  Created by 邢程 on 15/8/21.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@interface Selectedcontents : NSObject

@property (nonatomic, copy) NSString *selectedtype;

@property (nonatomic, strong) NSArray *storys;

@property (nonatomic, copy) NSString *selectedtypetitle;

@end
