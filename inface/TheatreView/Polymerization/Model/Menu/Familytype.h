//
//  Familytype.h
//  inface
//
//  Created by 邢程 on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Familytype : NSObject

@property (nonatomic, copy) NSString *typename;

@property (nonatomic, copy) NSString *typeid;

@end
