//
//  Nodes.h
//  inface
//
//  Created by 邢程 on 15/8/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Nodes : NSObject

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *ishighlight;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, assign ,getter=isUserSelected)BOOL    userSelected;

@end
