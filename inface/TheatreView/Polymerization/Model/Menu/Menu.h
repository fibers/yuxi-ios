//
//  Menu.h
//  inface
//
//  Created by 邢程 on 15/8/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
#import "Showtype.h"
#import "Nodes.h"

#define Menu_Full_File [[NSString documentPath] stringByAppendingPathComponent:@"/menu.tmp"]
@class Showtype,Nodes;
@interface Menu : NSObject
@property (nonatomic, copy) NSString *result;

@property (nonatomic, strong) NSArray *showtype;

@property (nonatomic, strong) NSArray *familytype;
@end




