//
//  Storys.h
//  inface
//
//  Created by 邢程 on 15/8/19.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@interface Storys : NSObject

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *relation;

@property (nonatomic, copy) NSString *storygroupid;

@property (nonatomic, copy) NSString *wordcounts;

@property (nonatomic, copy) NSString *storyid;

@property (nonatomic, copy) NSString *commentgroupid;

@property (nonatomic,copy)  NSString * checkgroupid;

@property (nonatomic,copy)  NSString * checkgrouptitle;

@property (nonatomic, copy) NSString *intro;

@property (nonatomic, copy) NSString *category;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *creatorid;

@property (nonatomic, copy) NSString *storyimg;

@property (nonatomic, copy) NSString *praisenum;

@property (nonatomic, copy) NSString *commentnum;

@property (nonatomic, copy) NSString *applynums;

@property (nonatomic, copy) NSString *ispraised;

@property (nonatomic, copy) NSString *ismember;

@property (nonatomic, copy) NSString *creatorportrait;

@property (nonatomic, copy) NSString *iscomment;

@property (nonatomic, copy) NSString *createtime;

@property (nonatomic, copy) NSString *updatetime;

@property (nonatomic, copy) NSString *creatorname;

@end
