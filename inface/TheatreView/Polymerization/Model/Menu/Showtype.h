//
//  Showtype.h
//  inface
//
//  Created by 邢程 on 15/8/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Showtype : NSObject

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) NSMutableArray *nodes;

@end