//
//  TheatreGroupToChatViewController.h
//  inface
//
//  Created by huangzengsong on 15/6/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//从剧到剧群然后到聊天页面中得 剧群页面
#import "BaseViewController.h"
#import "ChatViewController.h"
#import "TheatreGroupToChatTableViewCell.h"
@interface TheatreGroupToChatViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic ,strong)NSString *theatreid;
@property (strong, nonatomic) NSMutableArray *dataArr;
@property (strong,nonatomic)NSString * creatorid;
@property (strong,nonatomic)NSString * checkGroupId;//审核群id
@property (strong,nonatomic)NSDictionary  *storyInfo;//剧的详情
@property (strong,nonatomic)NSString     * checkTitle;//审核群的标题
@property(assign,nonatomic)    BOOL   isAnimating;

@end
