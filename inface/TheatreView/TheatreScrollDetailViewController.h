//
//  TheatreScrollDetailViewController.h
//  inface
//
//  Created by huangzengsong on 15/6/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface TheatreScrollDetailViewController : BaseViewController<UIWebViewDelegate>
{
    UIActivityIndicatorView* activityIndicatorView;
}
@property (strong, nonatomic) IBOutlet UIWebView *aWebView;
@property (nonatomic ,retain) NSString *navTitle;
@property (nonatomic,strong)  NSString * type;
@property (nonatomic ,retain) NSString *contentUrl;
@property (strong,nonatomic)  NSString * storyID;//是否有剧id
@property(assign,nonatomic)    BOOL   isAnimating;

@end
