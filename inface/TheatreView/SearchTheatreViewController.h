//
//  SearchTheatreViewController.h
//  inface
//
//  Created by Mac on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "PolymerizationTableViewCell.h"
@interface SearchTheatreViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableStorys;

@property (nonatomic ,retain) UITextField * SearchText;

@property (nonatomic,assign)   BOOL  isAnimating;
@end
