//
//  roleTypeCell.m
//  inface
//
//  Created by appleone on 15/11/4.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "roleTypeCell.h"
#import "NSAttributedString+CXACoreTextFrameSize.h"
@interface roleTypeCell()

@property (weak, nonatomic) IBOutlet UILabel *roleTitleLal;

@property (weak, nonatomic) IBOutlet UILabel *roleBackLabl;

@property (weak, nonatomic) IBOutlet UILabel *roleIntroLab;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end


@implementation roleTypeCell

- (void)awakeFromNib {
    // Initialization code
    self.frame = CGRectMake(0, 0, ScreenWidth, 100);
}

-(CGFloat)contentHeight:(NSInteger)indexPath storyDetailDic:(NSDictionary *)detailDic
{
    NSDictionary * settingDic = [detailDic objectForKey:@"rolesettings"];
    
    NSArray * types = [settingDic objectForKey:@"roletypes"];
    //第一行是标题
    NSDictionary * roleType = types[indexPath-1];
    
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    
    style.lineSpacing = 5.0f;
    
    NSString * background = [roleType objectForKey:@"background"];
    
    UIFont * font = [UIFont systemFontOfSize:15.0f];

    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:[roleType objectForKey:@"background"]];

    [attribute addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:font} range:NSMakeRange(0, background.length)];
//    CGFloat hei = [attribute cxa_coreTextFrameSizeWithConstraints:CGSizeMake(ScreenWidth -30, 9999)].height;
    
    float hei=[HZSInstances boundAutoStingSize:background uifont:15.0 lines:5 size:CGSizeMake(ScreenWidth- 30, 9999)];

    CGRect labelRect = _roleIntroLab.frame;
    
    CGRect viewRect = _backView.frame;
    
    viewRect.size.height = hei ;
    
    labelRect.size.height = hei;
    
    _roleIntroLab.frame = labelRect;
    
    _backView.frame = viewRect;
    
    return hei-5;
} 

-(void)setRoleType:(NSDictionary *)roleTypeDic index:(NSInteger)indexPath
{
    NSDictionary * settingDic = [roleTypeDic objectForKey:@"rolesettings"];
    
    NSArray * types = [settingDic objectForKey:@"roletypes"];
    //第一行是
    NSDictionary * roleType = types[indexPath-1];
    
    
    int count = [[roleType objectForKey:@"count"]intValue];
    [_roleTitleLal setText:[NSString stringWithFormat:@"%@ x%d",[roleType objectForKey:@"type"],count ]];
    [_roleBackLabl setText:@"身世背景"];
    
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    
    style.lineSpacing = 5.0f;
    
    NSString * background = [roleType objectForKey:@"background"];
    
    UIFont * font = [UIFont systemFontOfSize:15.0f];
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:[roleType objectForKey:@"background"]];
    
   [attribute addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:font} range:NSMakeRange(0, background.length)];
    
//    CGFloat hei = [attribute cxa_coreTextFrameSizeWithConstraints:CGSizeMake(ScreenWidth -30, 9999)].height;
    
    float hei= [attribute boundingRectWithSize:CGSizeMake(ScreenWidth  -30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:NULL].size.height;
    
    CGRect labelRect = _roleIntroLab.frame;
    
    CGRect viewRect = _backView.frame;
    
    viewRect.size.height = hei + 20;
    
    labelRect.size.height = hei;
    
    labelRect.size.width = ScreenWidth - 30;
    
    viewRect.size.width = ScreenWidth;
    
    _roleIntroLab.frame = labelRect;
    
    _backView.frame = viewRect;
    
    [_roleIntroLab setAttributedText:attribute];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
