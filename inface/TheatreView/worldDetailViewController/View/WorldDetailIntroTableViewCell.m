//
//  WorldDetailIntroTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "WorldDetailIntroTableViewCell.h"
#import "NSAttributedString+CXACoreTextFrameSize.h"
@implementation WorldDetailIntroTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.TypeLabel.textColor=BLUE_CONTENT_COLOR;
    self.ContentLabel.textColor=BLACK_COLOR;
    self.categoryLabel.textColor = BLUE_CONTENT_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setContent:(NSString *)content
{
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:content];
    
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    
    style.lineSpacing = 5.0f;
    
    UIFont * font = [UIFont systemFontOfSize:15.0f];
    
    [attribute addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:font} range:NSMakeRange(0, content.length)];
    
    
   CGFloat height = [attribute boundingRectWithSize:CGSizeMake(ScreenWidth -40, 9999) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin   context:NULL ].size.height;
    

    self.contenHeiConstant.constant = height;
    
    [self.ContentLabel setAttributedText:attribute];
    
}



@end
