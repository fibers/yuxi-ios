//
//  WorldDetailRuleTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "WorldDetailRuleTableViewCell.h"
#import "NSAttributedString+CXACoreTextFrameSize.h"
@interface WorldDetailRuleTableViewCell()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellHeiContant;

@end

@implementation WorldDetailRuleTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.ContentLabel.textColor=BLACK_COLOR;
}

-(void)setContent:(NSString *)content
{
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:content];
    
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    
    style.lineSpacing = 5.0f;
    
    UIFont * font = [UIFont systemFontOfSize:15.0f];
    
    [attribute addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:font} range:NSMakeRange(0, content.length)];
    
    _cellHeiContant.constant = [attribute boundingRectWithSize:CGSizeMake(ScreenWidth  -40, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:NULL].size.height;
    [self.ContentLabel setAttributedText:attribute];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
