//
//  RoleSetCell.h
//  inface
//
//  Created by appleone on 15/11/4.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoleSetCell : UITableViewCell

-(void)setRoleNature:(NSArray *)roleNature;

-(void)setApplyRule:(NSDictionary *)dic;

-(CGFloat)contentHeight:(NSInteger)indexPath storyDetailDic:(NSDictionary *)detailDic totalCount:(NSInteger)totalCount;
@end
