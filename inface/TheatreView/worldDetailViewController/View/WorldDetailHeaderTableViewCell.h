//
//  WorldDetailHeaderTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorldDetailHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *TimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *FontCountLabel;
@property (weak, nonatomic) IBOutlet UIView *BtnView;
@end
