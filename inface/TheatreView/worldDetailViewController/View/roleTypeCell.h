//
//  roleTypeCell.h
//  inface
//
//  Created by appleone on 15/11/4.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface roleTypeCell : UITableViewCell
-(void)setRoleType:(NSDictionary *)roleTypeDic index:(NSInteger)indexPath;
-(CGFloat)contentHeight:(NSInteger)indexPath storyDetailDic:(NSDictionary *)detailDic;


@end
