//
//  RoleSetCell.m
//  inface
//
//  Created by appleone on 15/11/4.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "RoleSetCell.h"
#import "DaShedLine.h"
#import "DashesLineView.h"
#import "NSAttributedString+CXACoreTextFrameSize.h"
@interface RoleSetCell()
@property (weak, nonatomic) IBOutlet UILabel *roleNatureLabl;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@end

@implementation RoleSetCell

- (void)awakeFromNib {
    // Initialization code

}

-(void)setRoleNature:(NSArray *)roleNature
{
    NSMutableString * string = [[NSMutableString alloc]init];
    [roleNature enumerateObjectsUsingBlock:^(NSDictionary * roleDic, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString * roleName = [roleDic objectForKey:@"name"];
        
        [string appendString:roleName];
        if (idx != roleNature.count-1) {
            [string appendString:@"、"];

        }
        
    }];
//    [string deleteCharactersInRange:NSMakeRange(string.length -1, 1)];
    
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:string];
    
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    
    style.lineSpacing = 5.0f;
    
    UIFont * font = [UIFont systemFontOfSize:15.0f];
    
    [attribute addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:font} range:NSMakeRange(0, string.length)];
    
    CGFloat height = [attribute boundingRectWithSize:CGSizeMake(ScreenWidth-30, MAXFLOAT) options: NSStringDrawingUsesFontLeading |NSStringDrawingUsesLineFragmentOrigin context:NULL].size.height;
    CGRect rect = _roleNatureLabl.frame;
    
    rect.size.width = ScreenWidth -30;
    
    rect.size.height = height;
    
    _roleNatureLabl.frame = rect;
    if(string && ![string isEqualToString:@""]){
        [_titleLabel setText:@"人物设定"];
        DashesLineView * aDashesLine = [[DashesLineView alloc]initWithFrame:CGRectMake(0, 5, ScreenWidth, 0.5)];
        aDashesLine.backgroundColor=[UIColor clearColor];
        aDashesLine.startPoint=CGPointMake(0, 1);
        aDashesLine.endPoint=CGPointMake(ScreenWidth, 1);
        aDashesLine.lineColor=XIAN_COLOR;
        [self.contentView addSubview:aDashesLine];
    }
    [_roleNatureLabl setNumberOfLines:0];
    [_roleNatureLabl setAttributedText:attribute];
    
}

-(CGFloat)contentHeight:(NSInteger)indexPath storyDetailDic:(NSDictionary *)detailDic totalCount:(NSInteger)totalCount;
{
    NSDictionary * settingDic = [detailDic objectForKey:@"rolesettings"];
    
    NSArray * types = [settingDic objectForKey:@"rolenatures"];
    
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    
    style.lineSpacing = 5.0f;
    
    UIFont * font = [UIFont systemFontOfSize:15.0f];

    if (indexPath == totalCount - 2) {
        //人物设定label
        NSMutableString * string = [[NSMutableString alloc]init];
        [types enumerateObjectsUsingBlock:^(NSDictionary * roleDic, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString * roleName = [roleDic objectForKey:@"name"];
            
            [string appendString:roleName];
            if (idx != types.count-1) {
                [string appendString:@"、"];

            }
            
        }];
//        [string deleteCharactersInRange:NSMakeRange(string.length -1, 1)];
        
        NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:string];
        [attribute addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:font} range:NSMakeRange(0, string.length)];

        return [attribute boundingRectWithSize:CGSizeMake(ScreenWidth  -30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:NULL].size.height;
    }else{
        NSDictionary * settingDic = [detailDic objectForKey:@"rolesettings"];
        
        NSString * string = [settingDic objectForKey:@"rolerule"];
        
        NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:string];
        [attribute addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:font} range:NSMakeRange(0, string.length)];

        return [attribute boundingRectWithSize:CGSizeMake(ScreenWidth  -30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:NULL].size.height;
    }

    
}

-(void)setApplyRule:(NSDictionary *)dic
{
    
    NSDictionary * settingDic = [dic objectForKey:@"rolesettings"];
    
    NSString * roleRule = [settingDic objectForKey:@"rolerule"];
    if (roleRule && ![roleRule isEqualToString:@""]) {
        [_titleLabel setText:@"审核规则"];
        DashesLineView * aDashesLine = [[DashesLineView alloc]initWithFrame:CGRectMake(0, 5, ScreenWidth, 0.5)];
        aDashesLine.backgroundColor=[UIColor clearColor];
        aDashesLine.startPoint=CGPointMake(0, 1);
        aDashesLine.endPoint=CGPointMake(ScreenWidth, 1);
        aDashesLine.lineColor=XIAN_COLOR;
        [self.contentView addSubview:aDashesLine];
    }
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:roleRule];
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    
    style.lineSpacing = 5.0f;
    
    UIFont * font = [UIFont systemFontOfSize:15.0f];
    
    [attribute addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:font} range:NSMakeRange(0, roleRule.length)];
    
    CGFloat height = [attribute boundingRectWithSize:CGSizeMake(ScreenWidth  -30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:NULL].size.height;
    
    CGRect rect = _roleNatureLabl.frame;
    
    rect.size.width = ScreenWidth -30;
    
    rect.size.height = height;
    
    _roleNatureLabl.frame = rect;
    
    [_roleNatureLabl setAttributedText:attribute];
    
   
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
