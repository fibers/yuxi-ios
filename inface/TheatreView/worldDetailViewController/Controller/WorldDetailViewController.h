//
//  WorldDetailViewController.h
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "WorldDetailHeaderTableViewCell.h"
#import "WorldDetailIntroTableViewCell.h"
#import "WorldDetailRuleTableViewCell.h"
#import "WorldScenesTableViewCell.h"
#import "CopyrightInformationViewController.h"
#import "Storys.h"
@protocol UpdateStoryInfoDelegate;

@interface WorldDetailViewController : BaseViewController<UIScrollViewDelegate,UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{

    UIButton * aTopBtn;//滚动到顶端按钮
    
    NSMutableArray * myselfstorys;//主线剧情
    NSMutableArray * topicsArr;//主线人物

    NSString *indexSelected;
    
    NSMutableArray *scrollArray;               // 存放所有需要滚动的图片 UIImage
    float  SCROLLVIEWHEIGHT;
    

    UILabel * mystorysLabeltmp;//我的剧
    UILabel * myselfstorysLabeltmp;//我的自戏
    UILabel * topicsLabeltmp;//我的话题
    UILabel * myfavorLabeltmp;//我的喜欢
//    UIActionSheet* mySheet;
    
    BOOL  hasCurrent;
    
}
@property(assign,nonatomic)    BOOL   switching;
@property(assign,nonatomic)    BOOL   isAnimating;
@property(unsafe_unretained,nonatomic)id<UpdateStoryInfoDelegate>delegate;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSString * storyID;
@property(nonatomic ,strong)NSDictionary * dic;
@property(nonatomic,assign) BOOL postNoty;
@property(strong,nonatomic) NSString * selectStoryId;
@property (weak, nonatomic) IBOutlet UIView *BtnView;

@property(retain,nonatomic)UIScrollView *scrollView;
@property (nonatomic ,retain) UIPageControl * pageControl;
-(void)setStory:(Storys *)story;
@end
@protocol UpdateStoryInfoDelegate <NSObject>

-(void)updateStoryInfo:(NSDictionary*)storyinfo storyid:(NSString*)storyid;

@end