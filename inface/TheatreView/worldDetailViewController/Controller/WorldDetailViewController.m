//
//  WorldDetailViewController.m
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "WorldDetailViewController.h"
#import "GetAllFamilysAndStorys.h"
#import <ShareSDK/ShareSDK.h>
#import "TheatreGroupToChatTableViewCell.h"
#import "YXReaderViewController.h"
#import "MBProgressHUD+YXStyle.h"
#import "YXMyCenterViewController.h"
#import "NSString+YXExtention.h"
#import <IDMPhoto.h>
#import <IDMPhotoBrowser.h>
#import "CurrentGroupModel.h"
#import "YXTopicDetailViewController.h"
#import "StoryRoleSet.h"
#import "mixnature.h"
#import "RoleTypes.h"
#import "RoleNature.h"
#import "roleNatureDetail.h"
#import "roleTypeDetail.h"
#import "roleTitleCell.h"
#import "roleTypeCell.h"
#import "RoleSetCell.h"
@interface WorldDetailViewController ()
{
    UILabel * favor_label;
    int      favorcount;
    
    NSInteger roleCount;
}
@property(nonatomic,copy)NSString* nextNavTileTMPStr;
@property(assign,nonatomic)BOOL isCollected;
@property(nonatomic,strong)UIImageView * imageV;
@property(nonatomic,strong)Storys * storyTMP;
@property(strong,nonatomic)StoryRoleSet * storyRoleSet;
@property(strong,nonatomic)NSDictionary * storySetDic;
@end

@implementation WorldDetailViewController
{
    WorldDetailHeaderTableViewCell * headerViewCell;
}

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)menuItemSelected:(id)sender
{
    KxMenuItem* item = (KxMenuItem*)sender;
    if ([item.title isEqualToString:@"编辑"]) {
        TheatreDetailViewController * detailViewController1 = [[TheatreDetailViewController alloc] init];
        detailViewController1.dic = [[NSMutableDictionary dictionaryWithDictionary:self.dic]mutableCopy];
        detailViewController1.storyID=self.storyID;
        if (detailViewController1.isAnimating) {
            return;
        }
        detailViewController1.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController1 animated:YES];
    } else if ([item.title isEqualToString:@"删除剧"]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"是否确定删除?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag = 22222;
        [alert show];
    } else if ([item.title isEqualToString:@"举报"]) {
        ReportTheatreViewController * detailViewController = [[ReportTheatreViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        detailViewController.reportid = self.storyID;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    } else if ([item.title isEqualToString:@"分享"]) {
        [self shareAction:nil];
    }
    else if ([item.title isEqualToString:@"版权"]){
        [self gobanquan:sender];
    }else if ([item.title isEqualToString:@"收藏"]){
        [self collectAction:sender];
    }else if ([item.title isEqualToString:@"取消收藏"]){
        [self collectAction:sender];
    }
    
}
-(void)setStory:(Storys *)story{
    _storyTMP = story;
}
-(void)collectAction:(id)sender{
    KxMenuItem* item = (KxMenuItem*)sender;
    NSString * behavior = [item.title isEqualToString:@"收藏"]?@"1":@"0";
    [HttpTool postWithPath:@"CollectStory" params:@{@"uid":USERID,
                                                    @"storyid":self.storyID,
                                                    @"behavior":behavior,
                                                    @"type":@"1"
                                                    } success:^(id JSON) {
                                                        _isCollected =!_isCollected;
                                                        [MBProgressHUD showYXHudShowTextAddToView:self.view Title:[JSON objectForKey:@"describe"] andHideTime:1.5];
    } failure:^(NSError *error) {
        [MBProgressHUD showYXHudShowTextAddToView:self.view Title:@"操作失败" andHideTime:1.5];
        
    }];
}
//右上角按钮
-(void)returnBackr:(UIButton *)sender{
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([[self.dic objectForKey:@"creatorid"]isEqualToString:userid]) {
        //右上角按钮
        //我的剧
        

        NSArray *menuItems =
        @[
          [KxMenuItem menuItem:@"编辑"
                         image:[UIImage imageNamed:@"编辑剧大纲"]
                        target:self
                        action:@selector(menuItemSelected:)],
          [KxMenuItem menuItem:@"分享"
                         image:[UIImage imageNamed:@"分享"]
                        target:self
                        action:@selector(shareAction:)],
          
          [KxMenuItem menuItem:@"推荐到招募区"
                         image:[UIImage imageNamed:@"版权"]
                        target:self
                        action:@selector(RecommendAction)],
  
          [KxMenuItem menuItem:@"删除剧"
                         image:[UIImage imageNamed:@"删除剧"]
                        target:self
                        action:@selector(menuItemSelected:)],

          
          [KxMenuItem menuItem:@"版权"
                         image:[UIImage imageNamed:@"版权"]
                        target:self
                        action:@selector(menuItemSelected:)],
          
          ];
           
        KxMenuItem *first = menuItems[0];
        //first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
        first.alignment = NSTextAlignmentLeft;
        
        [KxMenu showMenuInView:self.navigationController.view
                      fromRect:CGRectMake(ScreenWidth-45, 10, 25, 44)
                     menuItems:menuItems];
    }else{
        
        //别人的剧

        NSArray *menuItems =
        @[
          [KxMenuItem menuItem:@"推荐到招募区"
                         image:[UIImage imageNamed:@"版权"]
                        target:self
                        action:@selector(RecommendAction)],
          [KxMenuItem menuItem:@"分享"
                         image:[UIImage imageNamed:@"分享"]
                        target:self
                        action:@selector(shareAction:)],
          _isCollected?

          [KxMenuItem menuItem:@"取消收藏"
                          image:[UIImage imageNamed:@"版权"]
                         target:self action:@selector(menuItemSelected:)]:
          [KxMenuItem menuItem:@"收藏"
                         image:[UIImage imageNamed:@"版权"]
                        target:self action:@selector(menuItemSelected:)],

          [KxMenuItem menuItem:@"举报"
                         image:[UIImage imageNamed:@"举报"]
                        target:self
                        action:@selector(menuItemSelected:)],

          [KxMenuItem menuItem:@"版权"
                         image:[UIImage imageNamed:@"版权"]
                        target:self
                        action:@selector(menuItemSelected:)],
          

          ];
        
        
        KxMenuItem *first = menuItems[0];
        //first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
        first.alignment = NSTextAlignmentLeft;
        
        [KxMenu showMenuInView:self.navigationController.view
                      fromRect:CGRectMake(ScreenWidth-45, 10, 25, 44)
                     menuItems:menuItems];
    }
}


//推荐剧到招募区
-(void)RecommendAction{
    
    NSDictionary * params = @{@"uid":USERID,@"content":@"推荐",@"storyid":self.storyID,@"token":@"110"};
    
    [HttpTool postWithPath:@"RecommendStory" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"推荐成功" message:@"去剧场招募区刷新看看" delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
            [alert show];
        }
        
    } failure:^(NSError *error) {
        
    }];

}


#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==5000) {
        //推荐
        if (buttonIndex== 1) {
            NSString * content = [alertView textFieldAtIndex:0].text;
            NSDictionary * params = @{@"uid":USERID,@"content":content,@"storyid":self.storyID,@"token":@"110"};
            [HttpTool postWithPath:@"RecommendStory" params:params success:^(id JSON) {
                if ([[JSON objectForKey:@"result"]integerValue]==1) {
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"推荐成功" message:@"去广场招募区刷新看看" delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
                    [alert show];
                }
                
            } failure:^(NSError *error) {
                
                
                
            }];

        }
    }else if (alertView.tag==22222) {
        //删除剧
        if (buttonIndex == 1) {
            NSArray * groups = [self.dic objectForKey:@"groups"];
            int totalMembers = 0;
            BOOL  hasCurrenGroup = NO;
            for (int i = 0; i < groups.count; i++) {
                NSDictionary * groupDic = [groups objectAtIndex:i];
                if (![[groupDic objectForKey:@"isplay"]isEqualToString:@"3"]) {
                    NSString * members = [[groups objectAtIndex:i]objectForKey:@"members"];
                    totalMembers += [members intValue];
                }else{
                    hasCurrenGroup = YES;
                }
                
            }
            int groupsCount;
            if (hasCurrenGroup == YES) {
                groupsCount =  (int)groups.count - 1;
            }else{
                groupsCount = (int)groups.count;
            }
            if(groupsCount == 0 || totalMembers <= groupsCount){
                NSDictionary * params = @{@"uid":USERID,@"storyid":self.storyID};
                //先把剧的几个群的成员干掉
                for (int i = 0; i < groups.count; i++) {
                    
                    NSArray * membersArr = [[groups objectAtIndex:i]objectForKey:@"memberlist"];
                    NSString * groupID = [[groups objectAtIndex:i]objectForKey:@"groupid"];
                    NSMutableArray *deleteArr=[[NSMutableArray alloc]init];

                    for (NSDictionary * persondic in membersArr) {
                        NSString * person_id = [NSString stringWithFormat:@"user_%@",[persondic objectForKey:@"userid"]];
                         [deleteArr addObject:person_id];
                    }
                    NSString * group_id = [NSString stringWithFormat:@"group_%@",groupID];
                    [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:deleteArr];
                    
                }
                [HttpTool postWithPath:@"DeleteStory" params:params success:^(id JSON) {
                    //                        NSString * describe = [JSON objectForKey:@"describe"];
                    //删除剧后 重新获取，刷新UI
                    [[ MyLiveViewController shareInstance]getMyStorys];
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alertview show];
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    //个人中心页面需要重新刷新
//                    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:@"key",@"value", nil];
//                    NSNotification *noti = [NSNotification notificationWithName:@"updatacount" object:nil userInfo:dict];
//                    [[NSNotificationCenter defaultCenter]postNotification:noti];
                }
                    failure:^(NSError *error) {
                                   
                }];
            }else{
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"您暂时不能删除该剧" message:@"需要剧中成员都退出才能删除剧" delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
                [alert show];
                
            }
            

        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    [self GetSelfStoryDetail];
    
}
///数据加载成功回调
-(void)dataLoadSuccess{
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"#f5f5f5"];
    indexSelected=@"0";
    hasCurrent = NO;
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 12, 0, 12)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 12, 0, 12)];
    }
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    [self.BtnView setBackgroundColor:[UIColor colorWithHexString:@"#f5f5f5"]];
    
    aTopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    aTopBtn.frame = CGRectMake(ScreenWidth-67,ScreenHeight-64-130, 57, 60);
    aTopBtn.backgroundColor = [UIColor clearColor];
    [aTopBtn setImage:[UIImage imageNamed:@"xuanfubtn"] forState:UIControlStateNormal];
    [aTopBtn addTarget:self action:@selector(aToping:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:aTopBtn];
    aTopBtn.hidden =YES;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
- (void)nameChanged
{
    [self GetSelfStoryDetail];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//回到顶端
-(void)aToping:(UIButton *)sender{
    //去评论，获取剧的评论资源
    YXTopicDetailViewController * Vcontroller = [[YXTopicDetailViewController alloc]init];
    
    [Vcontroller setType:YXTopicDetailTypeStory];
    
    [Vcontroller setTopicID:self.storyID];
    
    [self.navigationController pushViewController:Vcontroller animated:YES];
}



#pragma mark 获取我的剧详情

-(void)GetSelfStoryDetail
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"storyid":self.storyID,@"token":@"110"};
    WS(weakSelf);
    [HttpTool postWithPath:@"GetStoryInfo" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            [weakSelf dataLoadSuccess];
            self.dic=JSON;
            [self setRightBarButtonItem];
            NSArray * groups = [JSON objectForKey:@"groups"];
            
            for (NSDictionary * infoDic in groups) {
                NSString * isplay = [infoDic objectForKey:@"isplay"];
                if ([isplay isEqualToString:@"3"]) {
                    hasCurrent = YES;
                }
            }
            if (hasCurrent) {
                aTopBtn.hidden = YES;
            }
            
            
            _isCollected = ![[self.dic objectForKey:@"isCollectedByUser"] isEqualToString:@"0"];
            NSDictionary * params = @{@"title":[self.dic objectForKey:@"title"],@"wordcount":[self.dic objectForKey:@"wordcount"],@"storyimg":[self.dic objectForKey:@"storyimg"],@"praisenum": [self.dic objectForKey:@"praisenum"] ,@"commentnum":[self.dic objectForKey:@"commentnum"],@"ispraised":[self.dic objectForKey:@"ispraisedbyuser"]};
            favorcount = [[self.dic objectForKey:@"praisenum"]intValue];

            if (self.switching == NO) {
                [self.delegate updateStoryInfo:params storyid:self.storyID];
            }
            //
            myselfstorys=[self.dic objectForKey:@"scenes"];
            topicsArr=[self.dic objectForKey:@"roles"];
            
            scrollArray=[[NSMutableArray alloc]init];
            [scrollArray addObject:[self.dic objectForKey:@"storyimg"]];

            //*******************
            
            UINib *nib=[UINib nibWithNibName:@"WorldDetailHeaderTableViewCell" bundle:nil];
            WorldDetailHeaderTableViewCell *headerview=[[nib instantiateWithOwner:nil options:nil] firstObject];
            
            SCROLLVIEWHEIGHT=ScreenWidth/375.0*270.0;
            
            headerview.frame=CGRectMake(0, 0, ScreenWidth, SCROLLVIEWHEIGHT+100);
            
            self.tableView.tableHeaderView=headerview;
            
            NSString *updatetime=[self.dic objectForKey:@"createtime"];
            NSDate* date = [NSDate dateWithTimeIntervalSince1970:[updatetime doubleValue]];
            
            headerview.TimeLabel.text=[NSString stringWithFormat:@"创建时间:%@",[date detailPromptDateString]];
            
            NSString *wordcount=[self.dic objectForKey:@"wordcount"];
            headerview.FontCountLabel.text=[NSString stringWithFormat:@"创作文字:%@",wordcount];
            headerViewCell = headerview;
            /* 以下为scrollview */
            // 先放最后一张 然后放后台图片 最后放第一张图片
            //定义一个滚动视图
            self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, SCROLLVIEWHEIGHT)];
            self.scrollView.pagingEnabled=YES;//可以翻页
            self.scrollView.userInteractionEnabled=YES;//用户可交互
            self.scrollView.contentOffset=CGPointMake(0, 0);//记录滚动的点
            self.scrollView.showsHorizontalScrollIndicator = NO;
            self.scrollView.showsVerticalScrollIndicator = NO;
            self.scrollView.delegate=self;
            [headerview addSubview:self.scrollView];
            
            self.scrollView.contentSize=CGSizeMake(ScreenWidth*scrollArray.count, SCROLLVIEWHEIGHT);//滚动的内容尺寸
            
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                                            
                                                            initWithTarget:self
                                                            
                                                            action:@selector(handleTap:)];
            [self.scrollView addGestureRecognizer:tapGestureRecognizer];
            
            for (int  i=0; i<scrollArray.count; i++) {
                
                UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(i*ScreenWidth, 0, ScreenWidth, SCROLLVIEWHEIGHT)];
                imageView.userInteractionEnabled=YES;
                NSURL* url = [NSURL URLWithString:[[self.dic objectForKey:@"storyimg"] stringOfW900ImgUrl]];
                [imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"scrollzhanwei"] completed:nil];
                imageView.contentMode=UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds=YES;
                [self.scrollView addSubview:imageView];
                self.imageV = imageView;
                UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, SCROLLVIEWHEIGHT-34, ScreenWidth, 34)];
                aview.backgroundColor = COLOR(0, 0, 0, 0.40);
                [imageView addSubview:aview];
                
                UILabel *contentlabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, ScreenWidth-95, 34)];
                contentlabel.text=[self.dic objectForKey:@"title"];
                self.nextNavTileTMPStr = [self.dic objectForKey:@"title"];
                contentlabel.font = [UIFont systemFontOfSize:18.0f];
                contentlabel.textColor = [UIColor whiteColor];
                contentlabel.backgroundColor = [UIColor clearColor];
                contentlabel.textAlignment = NSTextAlignmentLeft;
                [aview addSubview:contentlabel];
                
            }
            
            float width=(ScreenWidth-40)/4.0;
            float height=30;
            //添加9个按钮
            for (int i=0; i<4; i++) {
                UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
                aButton.tag=i+1000;
                aButton.frame=CGRectMake(20+width*i, 10, width, height);
                
                [headerview.BtnView addSubview:aButton];
                
                UILabel *label1=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, height)];
                label1.backgroundColor =[UIColor clearColor];
                label1.userInteractionEnabled=YES;
                if (i==0) {
                    mystorysLabeltmp=label1;
                    label1.text=@"简介";
                } else if (i==1){
                    myselfstorysLabeltmp=label1;
                    label1.text=@"主线剧情";
                } else if (i==2){
                    topicsLabeltmp=label1;
                    label1.text=@"角色";
                } else if (i==3){
                    myfavorLabeltmp=label1;
                    label1.text=@"规则";
                }
                label1.textColor=GRAY_COLOR;
                label1.font=[UIFont systemFontOfSize:15];
                label1.textAlignment=NSTextAlignmentCenter;
                [aButton addSubview:label1];
                
                UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(NineBTN:)];
                [aButton addGestureRecognizer:tapGestureRecognizer];
                
                
            }
            
            UILabel *xianlabel0=[[UILabel alloc]initWithFrame:CGRectMake(19, 10, ScreenWidth-40, 1)];
            xianlabel0.backgroundColor =BLUECOLOR;
            [headerview.BtnView addSubview:xianlabel0];
            
            UILabel *xianlabel1=[[UILabel alloc]initWithFrame:CGRectMake(19, 39, ScreenWidth-40, 1)];
            xianlabel1.backgroundColor =BLUECOLOR;
            [headerview.BtnView addSubview:xianlabel1];
            
            UILabel *xianlabel02=[[UILabel alloc]initWithFrame:CGRectMake(19+width*0, 10, 1, 30)];
            xianlabel02.backgroundColor =BLUECOLOR;
            [headerview.BtnView addSubview:xianlabel02];
            
            UILabel *xianlabel2=[[UILabel alloc]initWithFrame:CGRectMake(19+width*1, 10, 1, 30)];
            xianlabel2.backgroundColor =BLUECOLOR;
            [headerview.BtnView addSubview:xianlabel2];
            
            UILabel *xianlabel3=[[UILabel alloc]initWithFrame:CGRectMake(19+width*2, 10, 1, 30)];
            xianlabel3.backgroundColor =BLUECOLOR;
            [headerview.BtnView addSubview:xianlabel3];
            
            UILabel *xianlabel4=[[UILabel alloc]initWithFrame:CGRectMake(19+width*3, 10, 1, 30)];
            xianlabel4.backgroundColor =BLUECOLOR;
            [headerview.BtnView addSubview:xianlabel4];
            
            UILabel *xianlabel04=[[UILabel alloc]initWithFrame:CGRectMake(19+width*4-1, 10, 1, 30)];
            xianlabel04.backgroundColor =BLUECOLOR;
            [headerview.BtnView addSubview:xianlabel04];
            
            mystorysLabeltmp.textColor=[UIColor whiteColor];
            myselfstorysLabeltmp.textColor=BLUECOLOR;
            topicsLabeltmp.textColor=BLUECOLOR;
            myfavorLabeltmp.textColor=BLUECOLOR;
            mystorysLabeltmp.backgroundColor=BLUECOLOR;
            myselfstorysLabeltmp.backgroundColor=[UIColor clearColor];
            topicsLabeltmp.backgroundColor=[UIColor clearColor];
            myfavorLabeltmp.backgroundColor=[UIColor clearColor];
            indexSelected=@"0";
            [self.tableView reloadData];
                        float wid_th=95;
                        float heig_ht=34;
                        float widthblank=(ScreenWidth-wid_th*3)/4;
                        //添加2个按钮
                        for (int i=0; i<3; i++) {
                            UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
                            aButton.tag=i+10000;
                            aButton.frame=CGRectMake(wid_th*i + (i+1)*widthblank,8, wid_th, heig_ht);
                            [aButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                            aButton.titleLabel.font=[UIFont systemFontOfSize:12];
                            aButton.titleLabel.textAlignment=NSTextAlignmentCenter;
                            [aButton setBackgroundColor:[UIColor clearColor]];
                            [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
                            //此处需要判断是否在剧的其他群里面
                            [self.BtnView addSubview:aButton];
                            if (i==0) {
                                [aButton setImage:[UIImage imageNamed:@"unSelectGroups"] forState:UIControlStateNormal];
                                [aButton setImage:[UIImage imageNamed:@"selectGroups"] forState:UIControlStateHighlighted];
                                [aButton addTarget:self action:@selector(selectStoryGroup) forControlEvents:UIControlEventTouchUpInside];
                                
                            }
                            if (i==1) {
                                [aButton setImage:[UIImage imageNamed:@"weiyuedu"] forState:UIControlStateNormal];
                                [aButton setImage:[UIImage imageNamed:@"yuedu"] forState:UIControlStateHighlighted];
                                [aButton addTarget:self action:@selector(goyuedu:) forControlEvents:UIControlEventTouchUpInside];
                                
                            } else if (i==2){
                                
                                [aButton setImage:[UIImage imageNamed:@"unSelectComment"] forState:UIControlStateNormal];
                                [aButton setImage:[UIImage imageNamed:@"selectComment"] forState:UIControlStateHighlighted];
                                [aButton addTarget:self action:@selector(aToping:) forControlEvents:UIControlEventTouchUpInside];
                            }
                            
                        }
//                    }
               }
        
                } failure:^(NSError *error) {
                    
                    
                }];
}

-(void)headerImageVClicked{
    IDMPhoto * photo = [IDMPhoto photoWithImage:self.imageV.image];
    IDMPhotoBrowser * browser = [[IDMPhotoBrowser alloc]initWithPhotos:@[photo] animatedFromView:self.imageV];
    [self presentViewController:browser animated:YES completion:nil];
}

-(void)selectStoryGroup
{
    NSArray * groups = [self.dic objectForKey:@"groups"];
    NSString * checkGroupid;
    for (NSDictionary * dic in groups ) {
        NSString * groupid = [dic objectForKey:@"isplay"];
        if ([groupid isEqualToString:@"0"]) {
            checkGroupid = [dic objectForKey:@"groupid"];
        }
    }

    TheatreGroupToChatViewController * detailViewController = [[TheatreGroupToChatViewController alloc]init];
    detailViewController.theatreid = self.storyID;
    detailViewController.storyInfo = self.dic;
    if (checkGroupid) {
        //审核群id
        detailViewController.checkGroupId = checkGroupid;
    }
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}


#pragma  mark 判断是否有阅读权限
-(void)goyuedu:(UIButton *)sender{
    
    NSString * readAutority = [self.dic objectForKey:@"readautority"];
    if ([readAutority isEqualToString:@"0"]) {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"哎哟，剧的主人没有开通阅读权限呢" message:@"" delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
                [alertview show];
        return;
    }
    YXReaderViewController * readerVC = [[YXReaderViewController alloc]init];
    NSString * portrait = [self.dic objectForKey:@"portrait"];
    NSString * storyImage = [self.dic objectForKey:@"storyimg"];
    NSString * baiduUrl = [self.dic objectForKey:@"section"];
    NSString * title = [self.dic objectForKey:@"title"];
    NSString * intro = [self.dic objectForKey:@"intro"];
    NSDictionary * params = @{@"portrait":portrait,@"storyimg":storyImage,@"section":baiduUrl,@"title":title,@"intro":intro};
    readerVC.storyDic = params;
    [readerVC storyId:self.storyID];
    [readerVC setNavTitle:self.nextNavTileTMPStr];
    if (readerVC.isAnimating) {
        return;
    }
    readerVC.isAnimating = YES;
    [self.navigationController pushViewController:readerVC animated:YES];
}

-(void)gobanquan:(UIButton *)sender{
    
    CopyrightInformationViewController * detailViewController = [[CopyrightInformationViewController alloc] init];
    detailViewController.navTitle=@"版权";
    detailViewController.contentUrl=[self.dic objectForKey:@"copyright"];
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

-(void)goshenqing:(UIButton *)sender{
    //申请功能
    NSArray * groups = [self.dic objectForKey:@"groups"];
    NSString * checkGroupid=@"";
//    NSString * title = @"";
    for (NSDictionary * dic in groups ) {
        NSString * groupid = [dic objectForKey:@"isplay"];
        if ([groupid isEqualToString:@"0"]) {
            checkGroupid = [dic objectForKey:@"groupid"];
//            title = [dic objectForKey:@"title"];
        }
    }
    if (checkGroupid) {
        NSString *groupid=[NSString stringWithFormat:@"group_%@",checkGroupid];
        
        DDAddMemberToGroupAPI * addGroup = [[DDAddMemberToGroupAPI alloc]init];

        NSString *user_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];
        [addGroup requestWithObject:@[groupid,@[user_id]] Completion:^(id response, NSError *error) {
            //将自己成功添加到评论群,构造会话体，获取消息
            
        }];
        //直接加到剧中，不需要申请
//        [[AddGroupMemberModule sharedInstance]applyToJoinGroupsessionType:SessionTypeSessionTypeSingle groupId:groupid toUserIds:@[] msgId:@"0" ownerId:[self.dic objectForKey:@"creatorid"]];
//        
//        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"发送请求成功" message:@"请等待群主大人的批准" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//        [alertview show];
    }

}

//6按钮
-(void)NineBTN:(UITapGestureRecognizer*) recognizer
{
    
    NSInteger buttonIndex=recognizer.view.tag-1000;
    
    if (buttonIndex==0) {
        mystorysLabeltmp.textColor=[UIColor whiteColor];
        myselfstorysLabeltmp.textColor=BLUECOLOR;
        topicsLabeltmp.textColor=BLUECOLOR;
        myfavorLabeltmp.textColor=BLUECOLOR;
        mystorysLabeltmp.backgroundColor=BLUECOLOR;
        myselfstorysLabeltmp.backgroundColor=[UIColor clearColor];
        topicsLabeltmp.backgroundColor=[UIColor clearColor];
        myfavorLabeltmp.backgroundColor=[UIColor clearColor];
        indexSelected=@"0";
        [self.tableView reloadData];
    } else if (buttonIndex==1){
        mystorysLabeltmp.textColor=BLUECOLOR;
        myselfstorysLabeltmp.textColor=[UIColor whiteColor];
        topicsLabeltmp.textColor=BLUECOLOR;
        myfavorLabeltmp.textColor=BLUECOLOR;
        mystorysLabeltmp.backgroundColor=[UIColor clearColor];
        myselfstorysLabeltmp.backgroundColor=BLUECOLOR;
        topicsLabeltmp.backgroundColor=[UIColor clearColor];
        myfavorLabeltmp.backgroundColor=[UIColor clearColor];
        indexSelected=@"1";
        [self.tableView reloadData];
    } else if (buttonIndex==2){
        mystorysLabeltmp.textColor=BLUECOLOR;
        myselfstorysLabeltmp.textColor=BLUECOLOR;
        topicsLabeltmp.textColor=[UIColor whiteColor];
        myfavorLabeltmp.textColor=BLUECOLOR;
        mystorysLabeltmp.backgroundColor=[UIColor clearColor];
        myselfstorysLabeltmp.backgroundColor=[UIColor clearColor];
        topicsLabeltmp.backgroundColor=BLUECOLOR;
        myfavorLabeltmp.backgroundColor=[UIColor clearColor];
        indexSelected=@"2";
        [self.tableView reloadData];
    } else if (buttonIndex==3){
        mystorysLabeltmp.textColor=BLUECOLOR;
        myselfstorysLabeltmp.textColor=BLUECOLOR;
        topicsLabeltmp.textColor=BLUECOLOR;
        myfavorLabeltmp.textColor=[UIColor whiteColor];
        mystorysLabeltmp.backgroundColor=[UIColor clearColor];
        myselfstorysLabeltmp.backgroundColor=[UIColor clearColor];
        topicsLabeltmp.backgroundColor=[UIColor clearColor];
        myfavorLabeltmp.backgroundColor=BLUECOLOR;
        indexSelected=@"3";
        [self.tableView reloadData];
    }
    
    
}


/* 以下为scrollview */

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView==self.tableView) {
        return;
    }
    int index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
    self.pageControl.currentPage = index;
    [self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth*index, 0, ScreenWidth, SCROLLVIEWHEIGHT) animated:YES];
    
}


//点击滚动视图跳入详情页

-(void) handleTap:(UITapGestureRecognizer*) recognizer

{
    [self headerImageVClicked];
    int i = self.scrollView.contentOffset.x/ScreenWidth;

}

//6按钮
-(void)tapfriendpage:(UITapGestureRecognizer*) recognizer
{
    
    NSString * FriendID = [self.dic objectForKey:@"creatorid"];
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([userid isEqualToString:FriendID]) {
        //TODO(Xc.) 这里注释，不知道干什么的
//        MyCenterViewController * detailViewController = [[MyCenterViewController alloc]init];
//        detailViewController.fromClass=@"ViewPersonalData";
//        detailViewController.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:detailViewController animated:YES];
        
    } else {
//        
//        FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
//        detailViewController.FriendID = FriendID;
//        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
    [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];

}


-(void)setRightBarButtonItem{
    
    
    UIView *aview=[[UIView alloc]init];
    aview.backgroundColor=[UIColor clearColor];
    aview.frame=CGRectMake(0, 0, 140, 44);
//    self.navigationItem.titleView=aview;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapfriendpage:)];
    
    [aview addGestureRecognizer:tapGestureRecognizer];
    
    UIImageView *imageView=[[UIImageView alloc]init];
    imageView.userInteractionEnabled=YES;
    imageView.frame=CGRectMake(0, 8, 27, 27);
    imageView.layer.borderWidth = 0;
    imageView.layer.borderColor = [UIColor grayColor].CGColor;
    imageView.clipsToBounds =YES;
    imageView.layer.cornerRadius = CGRectGetHeight(imageView.bounds)/2;
    imageView.backgroundColor=[UIColor whiteColor];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[[self.dic objectForKey:@"portrait"] stringOfW100ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
    
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [aview addSubview:imageView];
    
    [self.navigationItem setTitleView:aview];
    
    //定义一个label，显示标题内容
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(35, 8, 100, 14)];
    label.backgroundColor =[UIColor clearColor];
    label.userInteractionEnabled=YES;
    NSString *creatorname=[self.dic objectForKey:@"creatorname"];
    label.text=[NSString stringWithFormat:@"%@的剧",creatorname];
    label.textColor=BLACK_COLOR;
    label.font=[UIFont systemFontOfSize:11];
    label.textAlignment=NSTextAlignmentLeft;
    [aview addSubview:label];
    
    
    //定义一个label，显示标题内容
    UILabel *labeltime=[[UILabel alloc]initWithFrame:CGRectMake(35, 25, 100, 14)];
    labeltime.backgroundColor =[UIColor clearColor];
    labeltime.userInteractionEnabled=YES;
    NSString *createtime=[self.dic objectForKey:@"createtime"];
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[createtime doubleValue]];
    
    labeltime.text=[date promptDateString];
    labeltime.textColor=GRAY_COLOR;
    labeltime.font=[UIFont systemFontOfSize:9];
    labeltime.textAlignment=NSTextAlignmentLeft;
    [aview addSubview:labeltime];
    
    
    UIView *bview=[[UIView alloc]init];
    bview.backgroundColor=[UIColor clearColor];
    bview.frame=CGRectMake(0, 0, 80, 44);
    UIBarButtonItem *barback2 = [[UIBarButtonItem alloc]initWithCustomView:bview];
    self.navigationItem.rightBarButtonItem=barback2;
    
    UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
    aButton.frame=CGRectMake(0,12, 21, 21);
    [aButton setImage:[UIImage imageNamed:@"weishoucang"] forState:UIControlStateNormal];
    [aButton setImage:[UIImage imageNamed:@"shoucang"] forState:UIControlStateSelected];
    if ([[self.dic objectForKey:@"ispraisedbyuser"]isEqualToString:@"1"]) {
        aButton.selected=YES;
    } else {
        aButton.selected=NO;
    }
    [aButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
    [aButton setBackgroundColor:[UIColor clearColor]];
    [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
    
    [bview addSubview:aButton];
    
    UILabel *favorlabel=[[UILabel alloc]initWithFrame:CGRectMake(25, 0, 30, 44)];
    favorlabel.backgroundColor =[UIColor clearColor];
    favorlabel.userInteractionEnabled=YES;
    //收藏个数没给返回
    favorlabel.text=[self.dic objectForKey:@"praisenum"];
    favorlabel.textColor=BLACK_COLOR;
    favorlabel.font=[UIFont systemFontOfSize:11];
    favorlabel.textAlignment=NSTextAlignmentCenter;
    [bview addSubview:favorlabel];
    favor_label = favorlabel;
    UIButton *aButtonmore=[UIButton buttonWithType:UIButtonTypeCustom];
    aButtonmore.frame=CGRectMake(55,0, 25, 44);
    [aButtonmore setImage:[UIImage imageNamed:@"morebtn"] forState:UIControlStateNormal];
    [aButtonmore setImage:[UIImage imageNamed:@"morebtnselect"] forState:UIControlStateHighlighted];
    [aButtonmore addTarget:self action:@selector(returnBackr:) forControlEvents:UIControlEventTouchUpInside];
    [aButtonmore setBackgroundColor:[UIColor clearColor]];
    [aButtonmore setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
    
    [bview addSubview:aButtonmore];
    
    
}


//喜欢
-(void)likeAction:(UIButton *)sender{
    UIButton *aButton=sender;
    NSInteger praiseNum = [_storyTMP.praisenum integerValue];
    if (_storyTMP) {
        _storyTMP.praisenum = (aButton.isSelected?[NSString stringWithFormat:@"%ld",(praiseNum-1)]:[NSString stringWithFormat:@"%ld",(praiseNum+1)]);
        _storyTMP.ispraised = aButton.isSelected?@"0":@"1";
    }
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if (aButton.selected==NO) {
        aButton.selected=YES;
        //由不喜欢变为喜欢
        //storyid剧的id  behavior0: 取消赞  1: 赞
        NSDictionary * parameters = @{@"uid":userid,@"storyid":self.storyID,@"behavior":@"1",@"token":@"110"};
        
        [HttpTool postWithPath:@"PraiseStory" params:parameters success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]integerValue]==1) {

                favorcount +=1;
               favor_label.text= [NSString stringWithFormat:@"%d",favorcount];
                    NSDictionary * params = @{@"title":[self.dic objectForKey:@"title"],@"wordcount":[self.dic objectForKey:@"wordcount"],@"storyimg":[self.dic objectForKey:@"storyimg"],@"praisenum": [NSString stringWithFormat:@"%d",favorcount] ,@"commentnum":[self.dic objectForKey:@"commentnum"],@"ispraised":@"1"};
                if (self.switching == NO) {
                    [self.delegate updateStoryInfo:params storyid:self.storyID];
                    
                }
                [SVProgressHUD showSuccessWithStatus:@"点赞成功" duration:1.0];
                
            }else{
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                
            }
            
        } failure:^(NSError *error) {
        }];
        
    } else {
        aButton.selected=NO;
        //由喜欢变为不喜欢
        //storyid剧的id  behavior0: 取消赞  1: 赞
        
        NSDictionary * parameters = @{@"uid":userid,@"storyid":self.storyID,@"behavior":@"0",@"token":@"110"};
        
        [HttpTool postWithPath:@"PraiseStory" params:parameters success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]integerValue]==1) {

                favorcount-=1;
                favor_label.text= [NSString stringWithFormat:@"%d",favorcount];

                NSDictionary * params = @{@"title":[self.dic objectForKey:@"title"],@"wordcount":[self.dic objectForKey:@"wordcount"],@"storyimg":[self.dic objectForKey:@"storyimg"],@"praisenum": [NSString stringWithFormat:@"%d",favorcount] ,@"commentnum":[self.dic objectForKey:@"commentnum"],@"ispraised":@"0"};
                if (self.switching == NO) {
                    [self.delegate updateStoryInfo:params storyid:self.storyID];

                }

                [SVProgressHUD showSuccessWithStatus:@"取消赞成功" duration:1.0];
                
            }else{
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                
            }
            
        } failure:^(NSError *error) {
        }];
    }
    

}

#pragma mark 分享自戏，暂时给的是百度网址链接
-(void)shareAction:(UIButton *)sender
{
    
//    [mySheet dismissWithClickedButtonIndex:0 animated:YES];
    
    ShareModel * model = [ShareModel sharedInstance];
    NSString * portrait = [self.dic objectForKey:@"portrait"];
    NSString * storyImage = [self.dic objectForKey:@"storyimg"];
    NSString * baiduUrl = [self.dic objectForKey:@"section"];
    NSString * title = [self.dic objectForKey:@"title"];
    NSString * intro = [self.dic objectForKey:@"intro"];
    NSDictionary * params = @{@"portrait":portrait,@"storyimg":storyImage,@"section":baiduUrl,@"title":title,@"intro":intro};
    [model shareToThird :nil dic:params];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([indexSelected isEqualToString:@"0"]) {
        
        return 1;
    } else if ([indexSelected isEqualToString:@"1"]){
        
        return myselfstorys.count;
    } else if ([indexSelected isEqualToString:@"2"]){
        NSDictionary * storySetDic = [self.dic objectForKey:@"rolesettings"];
        
        NSArray * roleTypesArr = [storySetDic objectForKey:@"roletypes"];
        
        NSArray * roleNature = [storySetDic objectForKey:@"rolenatures"];

        roleCount = roleTypesArr.count +3;
        return roleTypesArr.count + 3;
       
    } else if ([indexSelected isEqualToString:@"3"]){
        
        return 1;
    }else{
        
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ([indexSelected isEqualToString:@"0"]) {
        
        static NSString *identifier=@"WorldDetailIntroTableViewCell";
        WorldDetailIntroTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"WorldDetailIntroTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell setContent:[self.dic objectForKey:@"intro"]];

        NSArray * arr = [[self.dic objectForKey:@"category"]componentsSeparatedByString:@","];
        cell.TypeLabel.text=[NSString stringWithFormat:@"类别：%@",arr[0]];
        NSMutableString * categoryStr = [[NSMutableString alloc]init];
        for (int  i = 1; i < arr.count; i++) {
            [categoryStr appendString:arr[i]];
            if (i!=arr.count-1) {
                [categoryStr appendString:@"、"];

            }
        }
//        [categoryStr deleteCharactersInRange:NSMakeRange(categoryStr.length - 1, 1)];
        cell.categoryLabel.text = [NSString stringWithFormat:@"类目：%@",categoryStr];
        return cell;
        
    } else if ([indexSelected isEqualToString:@"1"]){
        
        static NSString *identifier=@"WorldScenesTableViewCell";
        WorldScenesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"WorldScenesTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        NSString * content = [myselfstorys[indexPath.row] objectForKey:@"content"] ;
        [cell setContent:content];
        
        return cell;
        
    } else if ([indexSelected isEqualToString:@"2"]){
        if (indexPath.row == 0) {
            static NSString *identifier=@"roleTitleCell";
            roleTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                cell= [[[NSBundle mainBundle]loadNibNamed:@"roleTitleCell" owner:nil options:nil] firstObject];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            return cell;

        }else if (indexPath.row == roleCount-2 ){
            
            static NSString *identifier=@"RoleSetCell";
            
            RoleSetCell * setCell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (!setCell) {
                setCell =  [[[NSBundle mainBundle]loadNibNamed:@"RoleSetCell" owner:nil options:nil] firstObject];
            }
            setCell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            NSDictionary * storySetDic = [self.dic objectForKey:@"rolesettings"];
            
            NSArray * roleTypesArr = [storySetDic objectForKey:@"rolenatures"];
            
            [setCell setRoleNature:roleTypesArr];
            return setCell;

        }else if (indexPath.row == roleCount - 1){
            
            static NSString *identifier=@"RoleSetCell";
            
            RoleSetCell * setCell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (!setCell) {
                setCell =  [[[NSBundle mainBundle]loadNibNamed:@"RoleSetCell" owner:nil options:nil] firstObject];
            }
            
            setCell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            [setCell setApplyRule:self.dic];
            return setCell;

        }else {
            static NSString *identifier=@"roleTypeCell";

            roleTypeCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell =  [[[NSBundle mainBundle]loadNibNamed:@"roleTypeCell" owner:nil options:nil] firstObject];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [cell setRoleType:self.dic index:indexPath.row];
            return cell;
        }
        
        
    } else {
        
        static NSString *identifier=@"WorldDetailRuleTableViewCell";
        WorldDetailRuleTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"WorldDetailRuleTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [cell setContent:[self.dic objectForKey:@"rule"]];
        return cell;
    }
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([indexSelected isEqualToString:@"0"]) {
        NSString *intro=[self.dic objectForKey:@"intro"];

        float height = [HZSInstances linesAutoStingSize:intro uifont:15.0 lines:5 size:CGSizeMake(ScreenWidth - 40, 9999)];
    
        return height+90;
    } else if ([indexSelected isEqualToString:@"1"]){
        NSString *intro=[[myselfstorys objectAtIndex:indexPath.row] objectForKey:@"content"];
        float height=[HZSInstances linesAutoStingSize :intro uifont:15.0 lines:5 size:CGSizeMake(ScreenWidth- 40, 9999)];
        
        return height+10;

    } else if ([indexSelected isEqualToString:@"2"]){
        if (indexPath.row == 0) {
            return 44;
        }else if (indexPath.row == roleCount -2 || indexPath.row == roleCount -1){
            RoleSetCell * setCell = [[RoleSetCell alloc]init];
          return   [setCell contentHeight:indexPath.row storyDetailDic:self.dic totalCount:roleCount] +40 ;
            
        }else{
            roleTypeCell * setCell = [[roleTypeCell alloc]init];
            
            return   [setCell contentHeight:indexPath.row storyDetailDic:self.dic]+92 ;
        }

//        NSString *intro=[[topicsArr objectAtIndex:indexPath.row] objectForKey:@"intro"];
//        float height=[HZSInstances labelAutoCalculateRectWith:intro FontSize:15.0 MaxSize:CGSizeMake(ScreenWidth-40, 2000)].height;
//        return height+50;
    } else if ([indexSelected isEqualToString:@"3"]){
        
        NSString *intro=[self.dic objectForKey:@"rule"];
        float height= [HZSInstances linesAutoStingSize:intro uifont:15.0 lines:5 size:CGSizeMake(ScreenWidth - 40, 99999)];
        return height+16;
    }else{
        
        return 0;
    }
    
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    if ([indexSelected isEqualToString:@"0"]) {

    } else if ([indexSelected isEqualToString:@"1"]){
        
    } else if ([indexSelected isEqualToString:@"2"]){
        
        
    } else if ([indexSelected isEqualToString:@"3"]){
                
    }else{
        
    }
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y>0) {
        
        
    } else {
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            aTopBtn.frame = CGRectMake(ScreenWidth-67,ScreenHeight-64-130, 57, 60);
            if (hasCurrent) {
                aTopBtn.hidden = NO;
            }
        } completion:^(BOOL finished) {


        }];
        
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{

    if (scrollView.contentOffset.y>0) {
        //改变位置
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            aTopBtn.frame = CGRectMake(ScreenWidth-67,ScreenHeight-64-50-57, 57, 60);
            
        } completion:^(BOOL finished) {
            aTopBtn.hidden=YES;
        }];
        
        
    } else {
        
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
