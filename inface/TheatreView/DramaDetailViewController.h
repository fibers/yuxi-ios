//
//  DramaDetailViewController.h
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//自戏详情
#import "BaseViewController.h"
@protocol UpdateSelfStoryInfoDelegate;

@interface DramaDetailViewController : BaseViewController<UIScrollViewDelegate,UIActionSheetDelegate>
{

    UIButton * aTopBtn;//滚动到顶端按钮
    
    UIButton  *shareBtn;
    UILabel   * titleLabel;//标题label
    UILabel   * wordCountLabel;//创作字数label
    UILabel   * contenLabel  ; //内容label
    UILabel   * timeLabel;//显示时间label
    UILabel  * title_label;
//    UIActionSheet* mySheet;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,nonatomic) NSString * storyID;
@property(nonatomic ,strong)NSDictionary * dic;
@property(nonatomic,assign) BOOL postNoty;
@property(nonatomic,strong) NSString * commendgroupid;
@property(unsafe_unretained,nonatomic)id<UpdateSelfStoryInfoDelegate>delegate;
@property (assign,nonatomic)BOOL switching;
@property(assign,nonatomic)    BOOL   isAnimating;

-(void)setStory:(Storys *)story;
@end
@protocol UpdateSelfStoryInfoDelegate <NSObject>

-(void)updateStoryInfo:(NSDictionary*)storyinfo storyid:(NSString*)storyid;

@end