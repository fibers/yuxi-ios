//
//  TheatreListDetailViewController.m
//  inface
//
//  Created by Mac on 15/5/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "TheatreListDetailViewController.h"

@interface TheatreListDetailViewController ()

@end

@implementation TheatreListDetailViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    UIView *aview=[[UIView alloc]init];
    aNavView=aview;
    aview.backgroundColor=[UIColor clearColor];
    aview.frame=CGRectMake(0, 0, 200, 40);
    self.navigationItem.titleView=aview;
    

    //添加3个按钮
    for (int i=0; i<3; i++) {
        UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
        aButton.tag=i+10000;
        float width=60;
        float height=30;
        float widthBlank=10;
        aButton.frame=CGRectMake((width+widthBlank)*i,5, width, height);
        [aButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
        [aButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        aButton.titleLabel.font=[UIFont boldSystemFontOfSize:16];
        aButton.titleLabel.textAlignment=NSTextAlignmentCenter;
        [aButton setBackgroundColor:[UIColor clearColor]];
        [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
        
        [aview addSubview:aButton];
        
        if (i==0) {

            [aButton setTitle:@"剧情" forState:UIControlStateNormal];
            [aButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
        } else if (i==1){

            [aButton setTitle:@"阅读" forState:UIControlStateNormal];
            [aButton addTarget:self action:@selector(liulanAction:) forControlEvents:UIControlEventTouchUpInside];
        } else if (i==2){

            [aButton setTitle:@"版权" forState:UIControlStateNormal];
            [aButton addTarget:self action:@selector(chatAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    
    UIButton *abutton=(UIButton *)[aNavView viewWithTag:10000];
    abutton.selected=YES;
    UIButton *abutton1=(UIButton *)[aNavView viewWithTag:10001];
    abutton1.selected=NO;
    UIButton *abutton2=(UIButton *)[aNavView viewWithTag:10002];
    abutton2.selected=NO;
    
    
    self.scrollView.delegate = self;
    self.scrollView.scrollEnabled = YES;
    self.scrollView.pagingEnabled=YES;
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.clipsToBounds = NO;
    
    self.scrollView.contentSize=CGSizeMake(ScreenWidth*3, ScreenHeight-64);
    [self getStoryInfo];
    
#pragma mark 获取剧的详情
   
}

#pragma mark 获取剧详情
-(void)getStoryInfo
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"storyid":self.storyID,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetStoryInfo" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            self.dic=JSON;
            [self addScrollSubview];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
        }
    
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)addScrollSubview{

    UIView *navView1=[[UIView alloc]initWithFrame:CGRectMake(ScreenWidth*0, 0, ScreenWidth*1, ScreenHeight-64)];
    TheatreDetailViewController * detailViewController1 = [[TheatreDetailViewController alloc] init];
    detailViewController1.dic = self.dic;
    detailViewController1.storyID=self.storyID;
    detailViewController1.creatorid = self.creatorid;
    if ([self.playingStory isEqualToString:@"yes"]) {
        detailViewController1.isplayingstory = @"yes";
    }
    if ([self.playingStory isEqualToString:@"no"]) {
        detailViewController1.isplayingstory = @"no" ;
    }
    detailViewController1.view.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight-64);
    [navView1 addSubview:detailViewController1.view];
    [self addChildViewController:detailViewController1];
    [self.scrollView addSubview:navView1];
    
    
    UIView *navView2=[[UIView alloc]initWithFrame:CGRectMake(ScreenWidth*1, 0, ScreenWidth*1, ScreenHeight-64)];
//    SectionViewViewController * detailViewController2 = [[SectionViewViewController alloc] init];
//    detailViewController2.contentUrl=[self.dic objectForKey:@"section"];
//    detailViewController2.view.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight-64);
//    
//    [navView2 addSubview:detailViewController2.view];
//    [self addChildViewController:detailViewController2];
    [self.scrollView addSubview:navView2];
    
    UIView *navView3=[[UIView alloc]initWithFrame:CGRectMake(ScreenWidth*2, 0, ScreenWidth*1, ScreenHeight-64)];
    CopyrightInformationViewController * detailViewController3 = [[CopyrightInformationViewController alloc] init];
    detailViewController3.contentUrl=[self.dic objectForKey:@"copyright"];
    detailViewController3.view.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight-64);
    
    [navView3 addSubview:detailViewController3.view];
    [self addChildViewController:detailViewController3];
    [self.scrollView addSubview:navView3];
}



//剧情介绍
-(void)likeAction:(UIButton *)sender{
    UIButton *abutton=(UIButton *)[aNavView viewWithTag:10000];
    abutton.selected=YES;
    UIButton *abutton1=(UIButton *)[aNavView viewWithTag:10001];
    abutton1.selected=NO;
    UIButton *abutton2=(UIButton *)[aNavView viewWithTag:10002];
    abutton2.selected=NO;
    [self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth*0, 0, ScreenWidth, ScreenHeight-64) animated:YES];
}
//章节浏览
-(void)liulanAction:(UIButton *)sender{
    UIButton *abutton=(UIButton *)[aNavView viewWithTag:10000];
    abutton.selected=NO;
    UIButton *abutton1=(UIButton *)[aNavView viewWithTag:10001];
    abutton1.selected=YES;
    UIButton *abutton2=(UIButton *)[aNavView viewWithTag:10002];
    abutton2.selected=NO;
    [self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth*1, 0, ScreenWidth, ScreenHeight-64) animated:YES];
}
//版权信息
-(void)chatAction:(UIButton *)sender{
    UIButton *abutton=(UIButton *)[aNavView viewWithTag:10000];
    abutton.selected=NO;
    UIButton *abutton1=(UIButton *)[aNavView viewWithTag:10001];
    abutton1.selected=NO;
    UIButton *abutton2=(UIButton *)[aNavView viewWithTag:10002];
    abutton2.selected=YES;
    [self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth*2, 0, ScreenWidth, ScreenHeight-64) animated:YES];
}



#pragma mark 已开戏的详情
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView==_scrollView) {
        int index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
        if (index==0) {
            //剧情介绍
            UIButton *abutton=(UIButton *)[aNavView viewWithTag:10000];
            abutton.selected=YES;
            UIButton *abutton1=(UIButton *)[aNavView viewWithTag:10001];
            abutton1.selected=NO;
            UIButton *abutton2=(UIButton *)[aNavView viewWithTag:10002];
            abutton2.selected=NO;
            [self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth*0, 0, ScreenWidth, ScreenHeight-64) animated:YES];
            
        } else if (index==1){
            //章节浏览
            UIButton *abutton=(UIButton *)[aNavView viewWithTag:10000];
            abutton.selected=NO;
            UIButton *abutton1=(UIButton *)[aNavView viewWithTag:10001];
            abutton1.selected=YES;
            UIButton *abutton2=(UIButton *)[aNavView viewWithTag:10002];
            abutton2.selected=NO;
            [self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth*1, 0, ScreenWidth, ScreenHeight-64) animated:YES];
            
        } else if (index==2){
            //版权信息
            UIButton *abutton=(UIButton *)[aNavView viewWithTag:10000];
            abutton.selected=NO;
            UIButton *abutton1=(UIButton *)[aNavView viewWithTag:10001];
            abutton1.selected=NO;
            UIButton *abutton2=(UIButton *)[aNavView viewWithTag:10002];
            abutton2.selected=YES;
            [self.scrollView scrollRectToVisible:CGRectMake(ScreenWidth*2, 0, ScreenWidth, ScreenHeight-64) animated:YES];
            
        }
        
    }
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
