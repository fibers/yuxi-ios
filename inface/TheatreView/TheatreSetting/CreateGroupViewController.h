//
//  CreateGroupViewController.h
//  inface
//
//  Created by Mac on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//各种群添加
#import "BaseViewController.h"
#import "MajorPersonAddTextFieldTableViewCell.h"
#import "MajorPersonAddTextViewTableViewCell.h"
#import "MessageReminderTableViewCell.h"
@interface CreateGroupViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    
    UILabel *remainCountLabel;//剩余字数label
    UITextField *nameTextField;//名称
    PlaceholderTextView *introTextView;//介绍
    UIButton *RightButtonCreate;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic ,strong)NSDictionary * dic;

@property(assign,nonatomic) BOOL  isAnimating;
@end
