//
//  IntroduceViewController.m
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "IntroduceViewController.h"

#define MaxNumberOfDescriptionChars  500//textview最多输入的字数

@interface IntroduceViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomToView;
@property(strong,nonatomic)UILabel *placeLabel;
@end

@implementation IntroduceViewController




//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    
    if ([self.chatInputTextView.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入简介" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }

    //修改简介
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSString * storyID =[self.dic objectForKey:@"id"];
    //type 1简介 2规则
    NSDictionary * dic = @{@"uid":userid,@"storyid":storyID,@"content":self.chatInputTextView.text,@"type":@"1",@"token":@"110"};
    
    [HttpTool postWithPath:@"ModifyStory" params:dic success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            //do any optional......
            [[MyLiveViewController shareInstance]getMyStorys];

            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"修改成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MajorIntroduceAddChanged" object:self userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
        
        
    }];
    

}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textviewEditChanged:) name:UITextViewTextDidChangeNotification object:self.chatInputTextView];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    [IQKeyboardManager sharedManager].enable = NO;
    
    self.navigationItem.title=@"简介";
    
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInputTextView.textColor=[UIColor blackColor];
    self.chatInputTextView.placeholder=@"简介";
    self.chatInputTextView.placeholderFont=[UIFont systemFontOfSize:14];
    self.chatInputTextView.placeholderColor=COLOR(204, 204, 204, 1);
    self.chatInputTextView.text=[self.dic objectForKey:@"intro"];
    self.chatInputTextView.returnKeyType=UIReturnKeyDone;
    self.countLabel.textColor=COLOR(204, 204, 204, 1);
    self.countLabel.text=[NSString stringWithFormat:@"%u",MaxNumberOfDescriptionChars-self.chatInputTextView.text.length];
    
    _placeLabel = [[UILabel alloc]init];
    _placeLabel.frame = CGRectMake(3, 10, self.chatInputTextView.frame.size.width, 14.0);
    _placeLabel.text = @"请输入简介(500字内)";
    _placeLabel.font = [UIFont systemFontOfSize:14.0];
    
    [self.chatInputTextView addSubview:_placeLabel];
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([[self.dic objectForKey:@"creatorid"]isEqualToString:userid]) {
        //右上角按钮
        //是管理员的话
        UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
        [RightButton setTitle:@"完成" forState:UIControlStateNormal];
        RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
        [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
        [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
        self.navigationItem.rightBarButtonItem=barback2;
    }else{
        
        self.chatInputTextView.editable=NO;
        
    }

    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [[self view] addGestureRecognizer:recognizer];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark 增减下滑手势
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gesture
{
    if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
        [self.chatInputTextView resignFirstResponder];
    }
}


/*由于联想输入的时候，函数textView:shouldChangeTextInRange:replacementText:无法判断字数，
 因此使用textViewDidChange对TextView里面的字数进行判断
 */
- (void)textViewDidChange:(UITextView *)textView
{
    
    //该判断用于联想输入
    if (textView.text.length > MaxNumberOfDescriptionChars)
    {
        textView.text = [textView.text substringToIndex:MaxNumberOfDescriptionChars];
        
    }
    self.countLabel.text=[NSString stringWithFormat:@"%u",MaxNumberOfDescriptionChars-textView.text.length];
}


-(void) keyboardWillShow:(NSNotification *) aNotification {
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    float height = keyboardRect.size.height;
    
    if (height == 0) {
        return  ;
        
    }
    
    //键盘没有弹出时
    
    [UIView animateWithDuration:0.3 animations:^{

        _bottomToView.constant = height;
    }];
    
}


-(void) keyboardWillHide:(NSNotification *) note
{
    
    [UIView animateWithDuration:.3f animations:^{
        //改变位置
        _bottomToView.constant = 159;
    }];
    
}

-(void)textviewEditChanged:(NSNotification *)noti{
    
    UITextField *field = (UITextField *)noti.object;
    NSString *toBeString = field.text;
    
    //获取高亮状态
    UITextRange *selectRange = [field markedTextRange];
    UITextPosition *position = [field positionFromPosition:selectRange.start offset:0];
    
    if (!position) {
        if (toBeString.length ==0) {
            
            _placeLabel.textColor = [UIColor grayColor];
            
        }
        else{
            _placeLabel.textColor = [UIColor clearColor];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated{

    if (self.chatInputTextView.text.length == 0) {
        _placeLabel.textColor = [UIColor grayColor];

    }else
        _placeLabel.textColor = [UIColor clearColor];

}




@end
