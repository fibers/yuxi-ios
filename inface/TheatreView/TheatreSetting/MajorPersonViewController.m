//
//  MajorPersonViewController.m
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MajorPersonViewController.h"

@interface MajorPersonViewController ()

@end

@implementation MajorPersonViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{

    MajorPersonAddViewController * detailViewController = [[MajorPersonAddViewController alloc] init];
    detailViewController.IsModify=@"add";//modify修改 否则创建
    detailViewController.storyid=[self.dic objectForKey:@"id"];
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([[self.dic objectForKey:@"creatorid"]isEqualToString:userid]) {
        //右上角按钮
        //是管理员的话
    
        //右上角按钮
        UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
        [RightButton setTitle:@"添加" forState:UIControlStateNormal];
        RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
        [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
        [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
        self.navigationItem.rightBarButtonItem=barback2;
    }

    
    self.navigationItem.title=@"主线人物";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 20)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 20)];
    }

    self.tableView.separatorColor=XIAN_COLOR;
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    //获得主线人物
    [self GetStoryRoles];
    //观察编辑动作的改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadArray:) name:@"MajorPersonAddChanged" object:nil];
}

-(void)reloadArray:(NSNotification *)notification
{
    [self GetStoryRoles];
    
}

//获得主线人物
-(void)GetStoryRoles{

    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSString * storyID =[self.dic objectForKey:@"id"];
    NSDictionary * params = @{@"uid":userid,@"storyid":storyID,@"token":@"110"};
    [HttpTool postWithPath:@"GetStoryRoles" params:params success:^(id JSON) {
        self.dataArr=[JSON objectForKey:@"roles"];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        
        
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"MajorPersonTableViewCell";
    MajorPersonTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"MajorPersonTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    

    cell.UserNameLabel.text=[[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"title"];
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if (![[self.dic objectForKey:@"creatorid"]isEqualToString:userid]) {
        //不是管理员的话
        cell.ArrowImage.hidden=YES;
    }
    
    return cell;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 44;
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([[self.dic objectForKey:@"creatorid"]isEqualToString:userid]) {
        //是管理员的话
        MajorPersonAddViewController * detailViewController = [[MajorPersonAddViewController alloc] init];
        detailViewController.IsModify=@"modify";//modify修改 否则创建
        detailViewController.storyid=[self.dic objectForKey:@"id"];
        detailViewController.dic=[self.dataArr objectAtIndex:indexPath.row];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;

        [self.navigationController pushViewController:detailViewController animated:YES];
    }

    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
