//
//  KindOfGroupViewController.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧情 各种群
#import "BaseViewController.h"
#import "CreateGroupViewController.h"
#import "ChatViewController.h"
#import "KindOfGroupTableViewCell.h"
@interface KindOfGroupViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic ,strong)NSDictionary * dic;
@property (strong, nonatomic) NSMutableArray *dataArr;
@property (strong,nonatomic) NSString * theatreid;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
