//
//  CreateGroupViewController.m
//  inface
//
//  Created by Mac on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "CreateGroupViewController.h"
#define MaxNumberOfDescriptionChars  100//textview最多输入的字数

@interface CreateGroupViewController ()

@property(strong,nonatomic)UILabel *placeLabel;

@end

@implementation CreateGroupViewController



//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    RightButtonCreate.userInteractionEnabled=NO;
    if ([nameTextField.text isEmpty]) {
        RightButtonCreate.userInteractionEnabled=YES;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入群名称" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([introTextView.text isEmpty]) {
        RightButtonCreate.userInteractionEnabled=YES;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入群简介" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if (nameTextField.text.length > 12)
    {
        nameTextField.text = [nameTextField.text substringToIndex:12];
    }
    
    if (introTextView.text.length > MaxNumberOfDescriptionChars)
    {
        introTextView.text = [introTextView.text substringToIndex:MaxNumberOfDescriptionChars];
        
    }
    
    NSString * user_userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];
    
    __block NSMutableString *groupID;
    //创建群
    DDCreateGroupAPI * createGroup = [[DDCreateGroupAPI alloc]init];
    
    NSArray * object = @[nameTextField.text,@"",@[user_userid]];
    [createGroup requestWithObject:object Completion:^(GroupEntity *response, NSError *error) {
        if (!error) {
            groupID=[response.objID mutableCopy];
            
            if ([groupID isEmpty]) {
                RightButtonCreate.userInteractionEnabled=YES;
                return ;
            }

            NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            
            NSRange  range = [groupID rangeOfString:@"group_"];
            if (range.length > 0) {
                [groupID deleteCharactersInRange:range];
            }
            //没法传生成群的名称和简介
            //关联剧和群
            NSDictionary * params = @{@"uid":userid,@"storyid":[self.dic objectForKey:@"id"],@"title":nameTextField.text,@"intro":introTextView.text,@"groupid":groupID,@"isplay":@"4",@"token":@"110"};
            [HttpTool postWithPath:@"AddGroupToStory" params:params success:^(id JSON) {
                if ([[JSON objectForKey:@"result"]integerValue] == 1) {
                    RightButtonCreate.userInteractionEnabled=YES;
                    [[MyLiveViewController shareInstance]getMyStorys];
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"生成群成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alertview show];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"createmoregroup" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"NAMECHANGE" object:nil];

                    [self.navigationController popViewControllerAnimated:YES];
                    
                }
                
            } failure:^(NSError *error) {
                RightButtonCreate.userInteractionEnabled=YES;
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"生成群失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                
            }];
            
            
            
        }else{
            RightButtonCreate.userInteractionEnabled=YES;
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"生成群失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
    }];
    

    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating= NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"完成" forState:UIControlStateNormal];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    RightButtonCreate=RightButton;
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"生成群";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        static NSString *identifier=@"MajorPersonAddTextFieldTableViewCell";
        MajorPersonAddTextFieldTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"MajorPersonAddTextFieldTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.chatInputTextField.placeholder=@"名称(不得超过12个字)";
        cell.chatInputTextField.delegate = self;
        nameTextField=cell.chatInputTextField;

        return cell;
    } else {
        static NSString *identifier=@"MajorPersonAddTextViewTableViewCell";
        MajorPersonAddTextViewTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"MajorPersonAddTextViewTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.chatInputTextView.delegate=self;
        cell.chatInputTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        cell.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
        cell.chatInputTextView.textColor=[UIColor blackColor];
        cell.chatInputTextView.placeholder=@"简介(不得超过100个字)";
        cell.chatInputTextView.placeholderFont=[UIFont systemFontOfSize:14];
        cell.chatInputTextView.placeholderColor=COLOR(204, 204, 204, 1);
        introTextView=cell.chatInputTextView;
        
        remainCountLabel=cell.countLabel;
        cell.countLabel.textColor=COLOR(204, 204, 204, 1);
        cell.countLabel.text=[NSString stringWithFormat:@"%lu",MaxNumberOfDescriptionChars-cell.chatInputTextView.text.length];
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *aview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 20)];
    aview.backgroundColor=[UIColor clearColor];
    
    return aview;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==1) {
        return 100;
    } else {
        return 44;
    }
    
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}



/*由于联想输入的时候，函数textView:shouldChangeTextInRange:replacementText:无法判断字数，
 因此使用textViewDidChange对TextView里面的字数进行判断
 */
- (void)textViewDidChange:(UITextView *)textView
{
    
    //该判断用于联想输入
    if (textView.text.length > MaxNumberOfDescriptionChars)
    {
        textView.text = [textView.text substringToIndex:MaxNumberOfDescriptionChars];
        
    }
    remainCountLabel.text=[NSString stringWithFormat:@"%lu",MaxNumberOfDescriptionChars-textView.text.length];
}






- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==nameTextField) {
        
        if (textField.text.length > 12)
        {
            textField.text = [textField.text substringToIndex:12];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
