//
//  PersonSetViewController.m
//  inface
//
//  Created by appleone on 15/8/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PersonSetViewController.h"

@interface PersonSetViewController ()

@end

@implementation PersonSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor clearColor];
    self.navigationItem.title = @"我的群名片";
    self.lineLabel.backgroundColor = XIAN_COLOR;
    self.scrollView.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
    self.chatInTextView =[[PlaceholderTextView alloc]initWithFrame:CGRectMake(9, 125, ScreenWidth -18,ScreenHeight-145)];
    self.chatInTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInTextView.delegate=self;
    self.chatInTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.chatInTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInTextView.textColor=[UIColor blackColor];
    self.chatInTextView.placeholder=@"人物设置模板";
    self.chatInTextView.placeholderFont=[UIFont systemFontOfSize:14];
    self.chatInTextView.placeholderColor=COLOR(0, 0, 0, 1);
    //    chatInTextView.returnKeyType=UIReturnKeyDone;
    self.chatInTextView.textColor=COLOR(0, 0, 0, 1);
    [self.scrollView addSubview:self.chatInTextView];
    self.scrollView.contentSize = CGSizeMake(ScreenWidth, self.chatInTextView.frame.size.height + 130);
    self.chatInTextView.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
    self.rolesArr = [NSMutableArray array];
    self.isup = NO;
    [self getPersonRoleStory];
}


-(void)returnBackl
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getPersonRoleStory
{
    NSDictionary * params = @{@"uid":USERID,@"storyid":self.theatreid,@"token":@"110"};
    [HttpTool postWithPath:@"GetStoryRoles" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            self.rolesArr = [[JSON objectForKey:@"roles"]mutableCopy];
        }
        
        [HttpTool postWithPath:@"GetUserRoleInfoInStory" params:params success:^(id JSON) {
            NSString * result = [JSON objectForKey:@"result"];
            if ( [result isEqualToString:@"1"]) {
                NSDictionary * roleInfo = [JSON objectForKey:@"roleinfo"];
                self.pibiaoLabel.text = [roleInfo objectForKey:@"roletitle"];
                self.roleTextField.text = [roleInfo objectForKey:@"userrolename"];
                self.chatInTextView.text = [roleInfo objectForKey:@"userroleinfo"];
                
            }else{
                UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alrt show];
            }
            
        } failure:^(NSError *error) {
            
            
        }];
        
    } failure:^(NSError *error) {
        
        
    }];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)pullDown:(id)sender

{
    int roleViewHeight = self.roleView.frame.origin.y;
    int roleLabelHeight = self.roleLabel.frame.origin.y+roleViewHeight;
    int roleTextHeight = self.roleTextField.frame.origin.y +roleViewHeight;
    int chatTextViewHeight = self.chatInTextView.frame.origin.y;
    if (self.isup == NO) {
        
        //此时展开
        if (1) {
            NSInteger roleCount = self.rolesArr.count/2 + self.rolesArr.count %2;
            self.avIew = [[UIView alloc]initWithFrame:CGRectMake(0, 46, ScreenWidth, 29 *roleCount +(roleCount-1)*14 +44)];
            self.avIew.backgroundColor  =[UIColor colorWithHexString:@"#f5f5f5"];
            float width = (ScreenWidth -65)/2.0;
            float  height = 29.0;
            for (int i = 0; i < self.rolesArr.count; i++) {
                NSString * title = [self.rolesArr[i]objectForKey:@"title"];
                int count = [[self.rolesArr[i]objectForKey:@"num"]intValue];
                UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.tag = 10000+1;
                int line = i /2;
                int rank = i %2;
                button.frame = CGRectMake(20 +(width +25)*rank, 22 +(height+14)*line, width, height);
                [button setTitle:title forState:UIControlStateNormal];
                if (count==0) {
                    button.userInteractionEnabled =NO;
                }
                [button addTarget:self action:@selector(selectRole:) forControlEvents:UIControlEventTouchUpInside];
            }
            [self.scrollView addSubview:self.avIew];
            self.roleView.frame = CGRectMake(9, roleViewHeight + self.avIew.frame.size.height, ScreenWidth-18, 44);
            self.roleView.frame = CGRectMake(6, 200, 200, 100);
            self.roleLabel.frame = CGRectMake(self.roleLabel.frame.origin.x,self.avIew.frame.size.height+roleLabelHeight, 60, 21);
            self.roleTextField.frame = CGRectMake(self.roleTextField.frame.origin.x, self.avIew.frame.size.height+roleTextHeight, ScreenWidth -90, 30);
            self.chatInTextView.frame = CGRectMake(self.chatInTextView.frame.origin.x, self.avIew.frame.size.height + chatTextViewHeight,ScreenWidth -18 , self.chatInTextView.frame.size.height);
            self.isup = YES;
        }

    }
    
}

-(void)selectRole:(UIButton*)sender
{
    
}
@end
