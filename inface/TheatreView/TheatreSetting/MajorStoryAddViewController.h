//
//  MajorStoryAddViewController.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//主线剧情添加
#import "BaseViewController.h"

@interface MajorStoryAddViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    
    UILabel *remainCountLabel;//剩余字数label
    UITextField *nameTextField;//名称
    UITextView*introTextView;//介绍
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic ,strong)NSString *IsModify;
@property(nonatomic ,strong)NSDictionary * dic;
@property(nonatomic ,strong)NSString *storyid;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
