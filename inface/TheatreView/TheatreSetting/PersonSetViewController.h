//
//  PersonSetViewController.h
//  inface
//
//  Created by appleone on 15/8/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface PersonSetViewController : BaseViewController<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property (weak, nonatomic) IBOutlet UIView *pibiaoView;

@property (weak, nonatomic) IBOutlet UILabel *pibiaoLabel;
@property (weak, nonatomic) IBOutlet UIButton *pullBTN;
- (IBAction)pullDown:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lineLabel;
@property (weak, nonatomic) IBOutlet UIView *roleView;
@property (weak, nonatomic) IBOutlet UILabel *roleLabel;

@property (weak, nonatomic) IBOutlet UITextField *roleTextField;
@property(nonatomic,strong) NSString * theatreid;
@property(nonatomic,strong) NSString * theatreGroupid;

@property(strong,nonatomic) PlaceholderTextView * chatInTextView;
@property(strong,nonatomic) NSMutableArray   * rolesArr;
@property(nonatomic,assign)  BOOL isup;
@property (nonatomic,strong) UIView * avIew;
@end
