//
//  GroupMessageSetViewController.h
//  inface
//
//  Created by appleone on 15/8/24.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface GroupMessageSetViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSString * group_id;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
