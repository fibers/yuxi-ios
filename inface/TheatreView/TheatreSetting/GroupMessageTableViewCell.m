//
//  GroupMessageTableViewCell.m
//  inface
//
//  Created by appleone on 15/8/24.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GroupMessageTableViewCell.h"

@implementation GroupMessageTableViewCell


- (void)awakeFromNib {
    // Initialization code
}

-(void)setContent:(GroupModel *)model realGroupId:(NSString *)realGroupId;
{
    self.detailLable.text = model.groupname;
    if ([model.groupid isEqualToString:realGroupId]) {
        self.selectImageView.hidden = NO;
    }else{
        self.selectImageView.hidden = YES;

    }
    self.backgroundColor = [UIColor colorWithHexString:@"#f9f9f9"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
