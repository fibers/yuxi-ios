//
//  MajorPersonViewController.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧情 人物
#import "BaseViewController.h"
#import "MajorPersonAddViewController.h"
#import "MajorPersonTableViewCell.h"
@interface MajorPersonViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic ,strong)NSDictionary * dic;
@property (strong, nonatomic) NSMutableArray *dataArr;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
