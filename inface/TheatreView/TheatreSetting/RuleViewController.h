//
//  RuleViewController.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧情 规则
#import "BaseViewController.h"

@interface RuleViewController : BaseViewController<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property(assign,nonatomic)    BOOL   isAnimating;

@property(nonatomic ,strong)NSDictionary * dic;

@end
