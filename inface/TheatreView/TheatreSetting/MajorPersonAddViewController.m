//
//  MajorPersonAddViewController.m
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MajorPersonAddViewController.h"

#define MaxNumberOfDescriptionChars  500//textview最多输入的字数

@interface MajorPersonAddViewController ()

@property (nonatomic,strong)UILabel *placeLabel;


@end

@implementation MajorPersonAddViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    if ([countTextField.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入人物数量" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([nameTextField.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入主线名称" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
//    if ([introTextView.text isEmpty]) {
//        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入人物简介" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//        [alertview show];
//        return;
//        
//    }
    
    if (nameTextField.text.length > 12)
    {
        nameTextField.text = [nameTextField.text substringToIndex:12];
    }
    
    if (introTextView.text.length > MaxNumberOfDescriptionChars)
    {
        introTextView.text = [introTextView.text substringToIndex:MaxNumberOfDescriptionChars];
        
    }

    if ([self.IsModify isEqualToString:@"modify"]) {
        //修改
        
        //修改主线角色
        NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        
        NSDictionary * dic = @{@"uid":userid,@"storyid":self.storyid,@"num":countTextField.text,@"title":nameTextField.text,@"intro":introTextView.text,@"roleid":[self.dic objectForKey:@"id"],@"token":@"110"};
        
        [HttpTool postWithPath:@"ModifyStoryRole" params:dic success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]integerValue]==1) {
                //do any optional......
                [[MyLiveViewController shareInstance]getMyStorys];

                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"修改成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MajorPersonAddChanged" object:self userInfo:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                
            }
            
        } failure:^(NSError *error) {
            
            
        }];

    } else {
        //添加
        //创建主线角色
        NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];

        NSDictionary * dic = @{@"uid":userid,@"storyid":self.storyid,@"num":countTextField.text,@"title":nameTextField.text,@"intro":introTextView.text,@"token":@"110"};
        
        [HttpTool postWithPath:@"CreateStoryRole" params:dic success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]integerValue]==1) {
                //do any optional......
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"创建成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MajorPersonAddChanged" object:self userInfo:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
                
            }
            
        } failure:^(NSError *error) {
            
            
        }];

    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"完成" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"主线人物";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }

    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];

    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        static NSString *identifier=@"MajorPersonAddTextFieldTableViewCell";
        MajorPersonAddTextFieldTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"MajorPersonAddTextFieldTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.chatInputTextField.placeholder=@"人物数量";
        cell.chatInputTextField.keyboardType=UIKeyboardTypeNumberPad;
        cell.chatInputTextField.delegate = self;
        countTextField=cell.chatInputTextField;
        if ([self.IsModify isEqualToString:@"modify"]) {
            //修改
        
            countTextField.text=[self.dic objectForKey:@"num"];
        }
        return cell;
    } else if (indexPath.section==1) {
        static NSString *identifier=@"MajorPersonAddTextFieldTableViewCell";
        MajorPersonAddTextFieldTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"MajorPersonAddTextFieldTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.chatInputTextField.placeholder=@"主线名称(不得超过12个字)";
        cell.chatInputTextField.delegate = self;
        nameTextField=cell.chatInputTextField;
        if ([self.IsModify isEqualToString:@"modify"]) {
            //修改
            
            nameTextField.text=[self.dic objectForKey:@"title"];
        }
        return cell;
    } else {
        static NSString *identifier=@"MajorPersonAddTextViewTableViewCell";
        MajorPersonAddTextViewTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"MajorPersonAddTextViewTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textviewEditChanged:) name:UITextViewTextDidChangeNotification object:cell.chatInputTextView];
        
        
        cell.chatInputTextView.delegate=self;
        cell.chatInputTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        cell.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
        cell.chatInputTextView.textColor=[UIColor blackColor];
        cell.chatInputTextView.placeholder=@"身世背景(不得超过500个字)";
        cell.chatInputTextView.placeholderFont=[UIFont systemFontOfSize:14];
        cell.chatInputTextView.placeholderColor=COLOR(204, 204, 204, 1);
        introTextView=cell.chatInputTextView;
        
        _placeLabel = [[UILabel alloc]init];
        _placeLabel.frame = CGRectMake(3, 10, cell.chatInputTextView.frame.size.width, 14.0);
        _placeLabel.text = @"身世背景(不得超过500个字)";
        _placeLabel.font = [UIFont systemFontOfSize:14.0];
        _placeLabel.textColor = [UIColor clearColor];
        [cell.chatInputTextView addSubview:_placeLabel];
        
        if ([self.IsModify isEqualToString:@"modify"]) {
            //修改
            
            introTextView.text=[self.dic objectForKey:@"intro"];
        }
        
        remainCountLabel=cell.countLabel;
        cell.countLabel.textColor=COLOR(204, 204, 204, 1);
        cell.countLabel.text=[NSString stringWithFormat:@"%lu",MaxNumberOfDescriptionChars-cell.chatInputTextView.text.length];
        return cell;
    }
    

    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *aview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 20)];
    aview.backgroundColor=[UIColor clearColor];
    
    return aview;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==2) {
        return 100;
    } else {
        return 44;
    }
    
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器

    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}



/*由于联想输入的时候，函数textView:shouldChangeTextInRange:replacementText:无法判断字数，
 因此使用textViewDidChange对TextView里面的字数进行判断
 */
- (void)textViewDidChange:(UITextView *)textView
{
    
    //该判断用于联想输入
    if (textView.text.length > MaxNumberOfDescriptionChars)
    {
        textView.text = [textView.text substringToIndex:MaxNumberOfDescriptionChars];
        
    }
    remainCountLabel.text=[NSString stringWithFormat:@"%lu",MaxNumberOfDescriptionChars-textView.text.length];
}





- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==nameTextField) {
        
        if (textField.text.length > 12)
        {
            textField.text = [textField.text substringToIndex:12];
        }
    }
    
}

-(void)textviewEditChanged:(NSNotification *)noti{
    
    UITextField *field = (UITextField *)noti.object;
    NSString *toBeString = field.text;
    
    //获取高亮状态
    UITextRange *selectRange = [field markedTextRange];
    UITextPosition *position = [field positionFromPosition:selectRange.start offset:0];
    
    if (!position) {
        if (toBeString.length ==0) {
            
            _placeLabel.textColor = [UIColor grayColor];
            
        }
        else{
            _placeLabel.textColor = [UIColor clearColor];
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
