//
//  KindOfGroupViewController.m
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "KindOfGroupViewController.h"
#import "StorySessionsModel.h"
#import "GetGroupRoleName.h"
@interface KindOfGroupViewController ()

@end

@implementation KindOfGroupViewController



//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    CreateGroupViewController * detailViewController = [[CreateGroupViewController alloc] init];
    detailViewController.dic=self.dic;
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"添加" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"剧群";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 20)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 20)];
    }

    self.tableView.separatorColor=XIAN_COLOR;
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    self.dataArr = [NSMutableArray arrayWithCapacity:10];
    
    //获得各种群
    [self GetStoryGroups];
    //观察编辑动作的改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadArray:) name:@"createmoregroup" object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


-(void)reloadArray:(NSNotification *)notification
{
    [self GetStoryGroups];
    
}

//获得各种群
-(void)GetStoryGroups{
    
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSString * storyID =[self.dic objectForKey:@"id"];
    NSDictionary * params = @{@"uid":userid,@"storyid":storyID,@"token":@"110"};
    [HttpTool postWithPath:@"GetStoryGroups" params:params success:^(id JSON) {
        NSArray * groups = [JSON objectForKey:@"groups"];
        if (groups.count > 0) {
            [self.dataArr removeAllObjects];
        }
        for (int i = 0; i < groups.count; i++) {

                NSDictionary * storyDic = [groups objectAtIndex:i];
                NSString * isplay = [storyDic objectForKey:@"isplay"];
                if (![isplay isEqualToString:@"3"]) {
                    [self.dataArr addObject:storyDic];
                }
        }
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        
        
    }];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"KindOfGroupTableViewCell";
    KindOfGroupTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"KindOfGroupTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.UserNameLabel.text=[[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"title"];
    
    return cell;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 44;
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    NSMutableString * preStr = [[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"groupid"];
    NSString * ischeck = [[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"ischeck"];
   __block NSString * group_id = [NSString stringWithFormat:@"group_%@",preStr];
   __block NSString * familyName = [[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"title"];
    __block SessionEntity* session= nil;
    [[StorySessionsModel shareInstance]getSessionBySelfGroupId:group_id storyid:[self.dic objectForKey:@"id"] isMulusUnreadCount:@"YES" success:^(id JSON) {
        //已经聊过天，直接从sessionsmodel中获取会话体
        session = (SessionEntity*)JSON;
        session.name = familyName;
        
    } failure:^(id error) {
       //该剧群没有聊过天，需要根据群的id构造会话体
        if ([error isEqualToString:@"Null"]) {
            session  = [[SessionEntity alloc]initWithSessionID:group_id SessionName:familyName type:SessionTypeSessionTypeGroup];
        }
        
    }];
    
    //获取剧群的角色名
    [[GetGroupRoleName shareInstance]getGroupRoleName:self.theatreid groupid:preStr success:^(id JSON) {
        
        ChatViewController * detailViewController = [ChatViewController shareInstance];
        if ([ischeck isEqualToString:@"1"]) {
            detailViewController.isInCheckGroup = YES;
        }
        detailViewController.isChatInStory = YES;

        detailViewController.theartorId = self.theatreid;
        detailViewController.checkGroupId = preStr;

        detailViewController.hidesBottomBarWhenPushed=YES;
        NSArray * viewControllers = self.navigationController.viewControllers;
        
        NSString *havechat=@"0";
        for (UIViewController * aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[ChatViewController class]]) {
                havechat=@"1";
                
                
            }
            
        }
        
        if ([havechat isEqualToString:@"0"]) {
            detailViewController.hidesBottomBarWhenPushed=YES;
            [detailViewController showChattingContentForSession:session];
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];

        } else {
            self.navigationController.navigationBar.hidden=NO;
            detailViewController.hidesBottomBarWhenPushed=YES;
            [detailViewController showChattingContentForSession:session];
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController popToViewController:(ChatViewController*)detailViewController animated:YES];

        }
        //取消选中的行
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    }];
    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
