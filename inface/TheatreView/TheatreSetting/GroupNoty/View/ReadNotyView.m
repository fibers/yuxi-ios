//
//  ReadNotyView.m
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ReadNotyView.h"
#import "NotiftModel.h"
#import "NotyResult.h"
@implementation ReadNotyView
{
    NotyResult * _model;
    
    NSString * _creatorName;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)setContent:(NotyResult *)model creatorName:(NSString *)creatorName
{
    _model = model;
    
    self.titleLabel.text = [model.boardinfo objectForKey:@"title"];
    self.contentTextView.text = [model.boardinfo objectForKey:@"content"];
    self.creatorLabel.text = creatorName;
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:[model.datetime integerValue]];
    NSString * dateStrng = [date getGroupNotyTime];
    NSRange range = [dateStrng rangeOfString:@"日"];
    self.contentTextView.layer.cornerRadius = 4;
    self.contentTextView.clipsToBounds = YES;
    if (range.length > 0) {
        NSString * time1 = [dateStrng substringToIndex:range.location];
        NSString * time2 = [dateStrng substringWithRange:NSMakeRange(range.location, dateStrng.length - range.location)];
        self.dateLabel.text = time1;
        self.timeLabel.text = time2;
    }
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return NO;
}
@end
