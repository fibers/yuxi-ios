//
//  AddGroupNotyView.h
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddGroupNotyView : UIView
@property (weak, nonatomic) IBOutlet UITextField *titltTextField;
@property (weak, nonatomic) IBOutlet PlaceholderTextView *contentTextView;

@end
