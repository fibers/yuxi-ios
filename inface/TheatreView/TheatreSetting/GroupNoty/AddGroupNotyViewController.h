//
//  AddGroupNotyViewController.h
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface AddGroupNotyViewController : BaseViewController
-(void)setContent:(NSString * )groupId;
@end
