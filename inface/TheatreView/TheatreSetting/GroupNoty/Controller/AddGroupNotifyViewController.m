//
//  AddGroupNotifyViewController.m
//  inface
//
//  Created by appleone on 15/9/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "AddGroupNotifyViewController.h"
#import "NotyResult.h"
#import "NotiftModel.h"
@interface AddGroupNotifyViewController ()
{
    NSString  * _groupId;

}
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;

@property (weak, nonatomic) IBOutlet PlaceholderTextView *contentTextView;

@end

@implementation AddGroupNotifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=@"群公告";
    
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"发布" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 30)];
    view.backgroundColor = [UIColor colorWithHexString:@"#f9f9f9"];
    _titleTextField.leftView = view;
    _titleTextField.leftViewMode = UITextFieldViewModeAlways;
//    groupNotyView = [[[NSBundle mainBundle]loadNibNamed:@"AddGroupNotyView" owner:nil options:nil]firstObject];
//    [self.view addSubview:groupNotyView];
    self.view.backgroundColor = [UIColor whiteColor];
    _contentTextView.placeholder = @"请输入公告内容";

    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [[self view] addGestureRecognizer:recognizer];
    // Do any additional setup after loading the view.
    [self getGroupNoty];
    
}


-(void)setContent:(NSString *)groupId
{
    _groupId = groupId;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

-(void)getGroupNoty
{
    //先从数据库获取群公告
    __block NSString * _title = @"";
    __block NSString * _content =@"";
    __block NSString * _dateTime =@"";
    [[DDDatabaseUtil instance]getGroupNotify:_groupId success:^(NSString *title,NSString*content,NSString * dateTime) {
        _title = title;
        
        _content = content;
        
        _dateTime = dateTime;
        
    }];

    if (_dateTime != NULL && ![_dateTime isEqualToString:@""] && ![_dateTime isEqual:nil]) {
        //数据库中已经有值了
//        NotyResult * resultModel = [[NotyResult alloc]init];
//        resultModel.datetime = _dateTime;
//        NSDictionary * notyDic = @{@"title":_title,@"content":_content};
        _titleTextField.text = _title;
        _contentTextView.text = _content;
//        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc]init];
//        
//        paragraphStyle.firstLineHeadIndent = 20.f;
//        
//        paragraphStyle.alignment = NSTextAlignmentJustified;
//        
//        NSDictionary * attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#949494"]};
//        
//        _contentTextView.attributedText = [[NSAttributedString alloc]initWithString:_content attributes:attributes];
        
        
    }else{
        NSDictionary * params = @{@"uid":USERID,@"groupid":_groupId,@"latesttime":@"0",@"token":@"110"};
        [HttpTool postWithPath:@"GetStoryGroupBoard" params:params success:^(id JSON) {
            
            if ([JSON objectForKey:@"1"]) {
                NSDictionary * notyDic = [[JSON objectForKey:@"boardinfo"]objectAtIndex:0];
                _titleTextField.text = [notyDic objectForKey:@"title"];
                
                _contentTextView.text = [notyDic objectForKey:@"content"];
                
//                NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc]init];
//                
//                paragraphStyle.firstLineHeadIndent = 20.f;
//                
//                paragraphStyle.alignment = NSTextAlignmentJustified;
//                
//                NSDictionary * attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#949494"]};
//                
//                _contentTextView.attributedText = [[NSAttributedString alloc]initWithString:[notyDic objectForKey:@"content"] attributes:attributes];
                
                [[DDDatabaseUtil instance]saveGroupNotify:[notyDic objectForKey:@"title"] content:[notyDic objectForKey:@"content"] groupid:_groupId dateTime:[JSON objectForKey:@"datetime"]];
            }
            

        } failure:^(NSError *error) {
            
            
        }];
        
    }
    

    
}

-(void)returnBackr
{
    NSString * title = _titleTextField.text;
    NSString * content = _contentTextView.text;
    if( [title isEqualToString:@""]|| [title isEqual:nil] || title == NULL) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"公告标题不能为空哦" message:nil delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if( [content isEqualToString:@""]|| [content isEqual:nil] || content == NULL) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"公告内容不能为空哦" message:nil delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil];
        [alert show];
        return;
    }
    NSDictionary * params = @{@"uid":USERID,@"groupid":_groupId,@"title":title,@"content":content,@"token":@"110"};
    
    [HttpTool postWithPath:@"UpdateStoryGroupBoard" params:params success:^(id JSON) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil];
        [alert show];
        NSString * dateTime = [JSON objectForKey:@"datetime"];

        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            [[DDDatabaseUtil instance]saveGroupNotify:title content:content groupid:_groupId dateTime:dateTime];

            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}


#pragma mark 增减下滑手势
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gesture
{
    if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
        [_contentTextView resignFirstResponder];
        [_titleTextField resignFirstResponder];
    }
}


-(void)returnBackl
{
    //创建公告
    [self.navigationController popViewControllerAnimated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
