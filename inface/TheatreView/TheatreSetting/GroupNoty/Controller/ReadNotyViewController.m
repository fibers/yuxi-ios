//
//  ReadNotyViewController.m
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ReadNotyViewController.h"
#import "ReadNotyView.h"
#import "NotiftModel.h"
#import "NotyResult.h"
@interface ReadNotyViewController ()
{
    NSString * _groupId;
    
    ReadNotyView * readNotyView;
    
    NSString * _creatorName;
    
    NotyResult * _notyResult;
}
@end

@implementation ReadNotyViewController

-(void)setContent:(NSString *)groupId creatorName:(NSString *)creatorName
{
    _groupId = groupId;
    
    _creatorName = creatorName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=@"公告";
    
    readNotyView = [[[NSBundle mainBundle]loadNibNamed:@"ReadNotyView" owner:nil options:nil]firstObject];
    
    [self.view addSubview:readNotyView];
    [self getGroupNoty];

}

-(void)returnBackl
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setNoty:(id)notyResult creatorName:(NSString *)creatorName
{
    _notyResult = notyResult;
    
    _creatorName = creatorName;
}

-(void)getGroupNoty
{
    NSMutableDictionary  * groupNotyDic = [[NSUserDefaults standardUserDefaults]objectForKey:GROUP_NOTIFY_DIC];
    if(!groupNotyDic){
        groupNotyDic = [NSMutableDictionary dictionary];
        [[NSUserDefaults standardUserDefaults]setObject:groupNotyDic forKey:GROUP_NOTIFY_DIC];
    }
    NSString * timeInter = [groupNotyDic objectForKey:_groupId];
    if (timeInter==NULL || [timeInter isEqual:nil]) {
        timeInter= @"0";
    }
    if (_notyResult) {
        //已经有NotyResult了，不需要获取
        [readNotyView setContent:_notyResult creatorName:_creatorName];
        return;
    }
    NSDictionary * params = @{@"uid":USERID,@"groupid":_groupId,@"latesttime":timeInter,@"token":@"110"};
    [HttpTool  postWithPath:@"GetStoryGroupBoard" params:params success:^(id JSON) {
        NotyResult * resultModel = [NotyResult objectWithKeyValues:JSON];
        [readNotyView setContent:resultModel creatorName:_creatorName];
        NotiftModel * model = [[NotiftModel alloc]init];
        model.title = [resultModel.boardinfo objectForKey:@"title"];
        model.content = [resultModel.boardinfo objectForKey:@"content"];
//        if (model.title!= NULL && ![model.title isEqual:nil] && ![model.title isEqualToString:@""]) {
//            groupNotyView.titltTextField.text = model.title;
//        }
//        if (model.content!= NULL && ![model.content isEqual:nil] && ![model.content isEqualToString:@""]) {
//            groupNotyView.contentTextView.text = model.title;
//        }
        //将时间戳存在字典里
        [groupNotyDic setObject:resultModel.datetime forKey:_groupId];
        NSMutableDictionary * groupContentDic = [[NSUserDefaults standardUserDefaults]objectForKey:GROUP_CONTENT_DIC];
        if(!groupContentDic){
            groupContentDic = [NSMutableDictionary dictionary];
            [[NSUserDefaults standardUserDefaults]setObject:groupContentDic forKey:GROUP_CONTENT_DIC];
        }
        
        [groupContentDic setObject:model.content forKey:_groupId];//将该群的公告内容存在对应的groupid中
        [[NSUserDefaults standardUserDefaults]setObject:groupNotyDic forKey:GROUP_NOTIFY_DIC];
        [[NSUserDefaults standardUserDefaults]setObject:groupContentDic forKey:GROUP_CONTENT_DIC];
        
    } failure:^(NSError *error) {
        
        
    }];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
