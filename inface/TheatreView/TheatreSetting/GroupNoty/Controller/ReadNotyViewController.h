//
//  ReadNotyViewController.h
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
@class NotyResult;
@interface ReadNotyViewController : BaseViewController

-(void)setContent:(NSString *)groupId creatorName:(NSString*)ceratorName;

-(void)setNoty:(NotyResult *)notyResult creatorName:(NSString *)creatorName;
@end
