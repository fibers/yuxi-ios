//
//  AddGroupNotifyViewController.h
//  inface
//
//  Created by appleone on 15/9/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface AddGroupNotifyViewController : BaseViewController

@property(assign,nonatomic)    BOOL   isAnimating;

-(void)setContent:(NSString *)groupId;

@end
