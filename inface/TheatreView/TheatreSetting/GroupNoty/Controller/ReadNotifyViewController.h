//
//  ReadNotifyViewController.h
//  inface
//
//  Created by appleone on 15/9/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
@class NotyResult;
@interface ReadNotifyViewController : BaseViewController

@property(assign,nonatomic)BOOL  isAnimating;
-(void)setContent:(NSString *)groupId creatorName:(NSString*)ceratorName;

-(void)setNoty:(NSString *)title content:(NSString *)content datetime:(NSString*)datetime;
@end
