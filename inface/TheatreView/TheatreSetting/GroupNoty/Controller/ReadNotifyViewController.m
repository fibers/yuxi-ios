//
//  ReadNotifyViewController.m
//  inface
//
//  Created by appleone on 15/9/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ReadNotifyViewController.h"
#import "NotiftModel.h"
#import "NotyResult.h"

@interface ReadNotifyViewController ()<UITextViewDelegate>
{
    NSString * _groupId;
        
    NSString * _creatorName;
    
    NSString * title_;
    
    NSString * content_;
    
    NSString * dateTime_;
    NotyResult * _notyResult;
    
}
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *uiview;



@property (weak, nonatomic) IBOutlet UITextView *contentLabel;

@end

@implementation ReadNotifyViewController


-(void)setContent:(NSString *)groupId creatorName:(NSString *)creatorName
{
    _groupId = groupId;
    
    _creatorName = creatorName;
}

-(void)setNoty:(id)notyResult creatorName:(NSString *)creatorName
{
    _notyResult = notyResult;
    
    _creatorName = creatorName;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title=@"公告";
    
    _contentLabel.layer.cornerRadius = 4.0f;
    [_contentLabel sizeToFit];
    
    self.uiview.layer.cornerRadius = 4.0f;
    self.uiview.layer.borderWidth = 0.5;
    self.uiview.layer.borderColor = [UIColor colorWithHexString:@"#c5c5c6"].CGColor;
    [_contentLabel setEditable:NO];
    [self getGroupNoty];

}

-(void)returnBackl
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getGroupNoty
{

    [[DDDatabaseUtil instance]getGroupNotify:_groupId success:^(NSString *title, NSString *content, NSString *dateTime) {
        
        dateTime_ = dateTime;
        
        title_ = title;
        
        content_ = content;
    }];

    if ([dateTime_ isEqualToString:@""] || [dateTime_ isEqual:nil] || dateTime_ == NULL) {
        dateTime_ = @"0";
    }else{
        [self setNotifyContent];
        return;
    }
    NSDictionary * params = @{@"uid":USERID,@"groupid":_groupId,@"latesttime":dateTime_,@"token":@"110"};
    [HttpTool  postWithPath:@"GetStoryGroupBoard" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            NSDictionary * notyDic = [[JSON objectForKey:@"boardinfo"]objectAtIndex:0];
            title_ = [notyDic objectForKey:@"title"];
            
            content_ = [notyDic objectForKey:@"content"];
            
            dateTime_ = [JSON objectForKey:@"datetime"];
            [self setNotifyContent];
            [[DDDatabaseUtil instance]saveGroupNotify:_groupId content:content_ groupid:_groupId dateTime:dateTime_];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
    
    
}


-(void)setNotifyContent
{
    __block NSString * creatorName = @"";
    [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:_groupId success:^(id JSON) {
        NSDictionary * storyinfo = JSON;
        
        creatorName = [storyinfo objectForKey:@"creatorname"];
        
        
    } failure:^(id Json) {
        
        
    }];

    _titleLabel.text = title_;
    _contentLabel.text = content_;
    _creatorLabel.text = creatorName;
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:[dateTime_ integerValue]];
    NSString * dateStrng = [date getGroupNotyTime];
    NSRange range = [dateStrng rangeOfString:@"日"];
    _contentLabel.layer.cornerRadius = 4;
    _contentLabel.clipsToBounds = YES;
    if (range.length > 0) {
        NSString * time1 = [dateStrng substringToIndex:range.location+1];
        NSString * time2 = [dateStrng substringWithRange:NSMakeRange(range.location+1, dateStrng.length - range.location-1)];
        self.dateLabel.text = time1;
        self.timeLabel.text = time2;
    }

    
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
