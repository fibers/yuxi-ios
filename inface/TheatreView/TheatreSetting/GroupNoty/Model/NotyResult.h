//
//  NotyResult.h
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@interface NotyResult : NSObject
@property(strong,nonatomic)NSString * result;

@property(strong,nonatomic)NSString * datetime;

@property(strong,nonatomic)NSDictionary * boardinfo;


@end
