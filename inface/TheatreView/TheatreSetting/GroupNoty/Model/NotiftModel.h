//
//  NotiftModel.h
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotiftModel : NSObject
@property(strong,nonatomic)NSString * title;//公告标题
@property(strong,nonatomic)NSString * content;//公告内容

@end
