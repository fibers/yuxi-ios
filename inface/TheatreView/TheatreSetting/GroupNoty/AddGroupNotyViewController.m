//
//  AddGroupNotyViewController.m
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "AddGroupNotyViewController.h"
#import "NotyResult.h"
#import "NotiftModel.h"
#import "AddGroupNotyView.h"
@interface AddGroupNotyViewController ()<UIAlertViewDelegate>
{
    NSString  * _groupId;
    
    AddGroupNotyView * groupNotyView;
}
@end

@implementation AddGroupNotyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=@"群公告";

    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"发布" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    groupNotyView = [[[NSBundle mainBundle]loadNibNamed:@"AddGroupNotyView" owner:nil options:nil]firstObject];
    [self.view addSubview:groupNotyView];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    [self getGroupNoty];
    
}

-(void)setContent:(NSString *)groupId
{
    _groupId = groupId;
}

-(void)getGroupNoty
{
//    NSMutableDictionary  * groupNotyDic = [[NSUserDefaults standardUserDefaults]objectForKey:GROUP_NOTIFY_DIC];
//    if(!groupNotyDic){
//        groupNotyDic = [NSMutableDictionary dictionary];
//        [[NSUserDefaults standardUserDefaults]setObject:groupNotyDic forKey:GROUP_NOTIFY_DIC];
//    }
//    NSString * timeInter = [groupNotyDic objectForKey:_groupId];
//    if (timeInter==NULL || [timeInter isEqual:nil]) {
//        timeInter= @"0";
//    }
//    NSDictionary * params = @{@"uid":USERID,@"groupid":_groupId,@"latesttime":timeInter,@"token":@"110"};
//    [HttpTool  postWithPath:@"GetStoryGroupBoard" params:params success:^(id JSON) {
//        NotyResult * resultModel = [NotyResult objectWithKeyValues:JSON];
//        NotiftModel * model = [[NotiftModel alloc]init];
//        model.title = [resultModel.boardinfo objectForKey:@"title"];
//        model.content = [resultModel.boardinfo objectForKey:@"content"];
//        if (model.title!= NULL && ![model.title isEqual:nil] && ![model.title isEqualToString:@""]) {
//            groupNotyView.titltTextField.text = model.title;
//        }
//        if (model.content!= NULL && ![model.content isEqual:nil] && ![model.content isEqualToString:@""]) {
//            groupNotyView.contentTextView.text = model.title;
//        }
//        //将时间戳存在字典里
//        [groupNotyDic setObject:resultModel.datetime forKey:_groupId];
//        NSMutableDictionary * groupContentDic = [[NSUserDefaults standardUserDefaults]objectForKey:GROUP_CONTENT_DIC];
//        if(!groupContentDic){
//            groupContentDic = [NSMutableDictionary dictionary];
//            [[NSUserDefaults standardUserDefaults]setObject:groupContentDic forKey:GROUP_CONTENT_DIC];
//        }
//        
//        [groupContentDic setObject:model.content forKey:_groupId];//将该群的公告内容存在对应的groupid中
//        [[NSUserDefaults standardUserDefaults]setObject:groupNotyDic forKey:GROUP_NOTIFY_DIC];
//        [[NSUserDefaults standardUserDefaults]setObject:groupContentDic forKey:GROUP_CONTENT_DIC];
//        
//    } failure:^(NSError *error) {
//        
//        
//    }];
    
    
}

-(void)returnBackl
{
    NSString * title = groupNotyView.titltTextField.text;
    NSString * content = groupNotyView.contentTextView.text;
    if( [title isEqualToString:@""]|| [title isEqual:nil] || title == NULL) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"公告标题不能为空哦" message:nil delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if( [content isEqualToString:@""]|| [content isEqual:nil] || content == NULL) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"公告内容不能为空哦" message:nil delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil];
        [alert show];
        return;
    }
    NSDictionary * params = @{@"uid":USERID,@"groupid":_groupId,@"title":title,@"content":content,@"token":@"110"};
    
    [HttpTool postWithPath:@"UpdateStoryGroupBoard" params:params success:^(id JSON) {

        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            [self.navigationController popViewControllerAnimated:YES];
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil];
            [alert show];
        }else
        {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil];
            [alert show];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}


-(void)returnBackr
{
    //创建公告
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
