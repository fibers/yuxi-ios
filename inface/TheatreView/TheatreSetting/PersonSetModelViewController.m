//
//  PersonSetModelViewController.m
//  inface
//
//  Created by appleone on 15/8/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PersonSetModelViewController.h"

@interface PersonSetModelViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSuperViewConstant;

@end

@implementation PersonSetModelViewController


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //右上角按钮
    [[IQKeyboardManager sharedManager]setEnable:NO];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];

    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"人物设置模板";
    [self getStoryRoleContent];
    
    //  self.chatInTextView =[[PlaceholderTextView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth,ScreenHeight-20)];
    self.chatInTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInTextView.delegate=self;
    self.chatInTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.chatInTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInTextView.textColor=[UIColor blackColor];
    self.chatInTextView.placeholder=@"人物设置模板";
    self.chatInTextView.placeholderFont=[UIFont systemFontOfSize:14];
    self.chatInTextView.placeholderColor=COLOR(0, 0, 0, 1);
    //    chatInTextView.returnKeyType=UIReturnKeyDone;
    self.chatInTextView.textColor=COLOR(0, 0, 0, 1);
    self.view.backgroundColor =[UIColor whiteColor];
    self.chatInTextView.layer.borderColor = [UIColor colorWithHexString:@"#b0adad"].CGColor;
    //    self.chatInTextView.backgroundColor = [UIColor colorWithHexString:@"#949494"]
    self.chatInTextView.layer.cornerRadius = 7.5;
    self.chatInTextView.layer.borderWidth = 0.8;
    //    self.chatInTextView.contentInset = UIEdgeInsetsMake(10, 10, 27, 10);
    self.chatInTextView.alwaysBounceHorizontal = NO;
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [[self view] addGestureRecognizer:recognizer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void) keyboardWillShow:(NSNotification *) aNotification {
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    float height = keyboardRect.size.height;
    
    if (height == 0) {
        return  ;
        
    }
    
    //键盘没有弹出时
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _bottomSuperViewConstant.constant = height;
        
    }];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    
    [UIView animateWithDuration:.3f animations:^{
        //改变位置
        _bottomSuperViewConstant.constant = 18;
    }];
    
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == self.chatInTextView) {
        [self.chatInTextView resignFirstResponder];
    }
}



#pragma mark 增减下滑手势
-(void)handleSwipeFrom:(UISwipeGestureRecognizer *)gesture
{
    if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
        [self.chatInTextView resignFirstResponder];
    }
}

-(void)getStoryRoleContent
{
    NSDictionary * params = @{@"uid":USERID,@"storyid":self.storyId,@"token":@"110"};
    [HttpTool postWithPath:@"GetStoryRoleTemplate" params:params success:^(id JSON) {
        NSString * content = [JSON objectForKey:@"content"];
        self.chatInTextView.text = content;
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5;// 字体的行间距
        
        NSDictionary *attributes = @{
                                     NSFontAttributeName:[UIFont systemFontOfSize:15],
                                     NSParagraphStyleAttributeName:paragraphStyle
                                     };
        //        self.chatInTextView.attributedText = [[NSAttributedString alloc] initWithString:content attributes:attributes];
        self.chatInTextView.attributedText = [[NSAttributedString alloc] initWithString:content ==nil ?@"":content attributes:attributes];
        //        if(content.length > 0){
        //        CGRect orgRect=self.chatInTextView.frame;//获取原始UITextView的frame
        //        CGSize  size = [content sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(ScreenWidth, 2000) lineBreakMode:UILineBreakModeWordWrap];
        //        orgRect.size.height=size.height+10;//获取自适应文本内容高度
        //
        //        self.chatInTextView.frame=orgRect;//重设UITextView的frame
        //        self.scrollView.contentSize = CGSizeMake(ScreenWidth, self.chatInTextView.frame.size.height + 20);
        //        }
    } failure:^(NSError *error) {
        
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark动态改变高度
-(void)textViewDidChange:(UITextView *)textView
{
    //      CGRect bounds = self.chatInTextView.bounds;
    //      // 计算 text view 的高度
    //      CGSize maxSize = CGSizeMake(ScreenWidth, CGFLOAT_MAX);
    //      CGSize newSize = [self.chatInTextView sizeThatFits:maxSize];
    //    newSize.width = ScreenWidth;
    //      bounds.size = newSize;
    //      self.chatInTextView.bounds = bounds;
    //    self.scrollView.contentSize = CGSizeMake(ScreenWidth, self.chatInTextView.frame.size.height + 20);
    //    float fPadding = 16.0; // 8.0px x 2
    //    CGSize constraint = CGSizeMake(textView.contentSize.width - fPadding, CGFLOAT_MAX);
    //
    //    CGSize size = [self.chatInTextView.text sizeWithFont: textView.font constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    
    //    NSString * text = self.chatInTextView.text;
    //    CGRect orgRect=self.chatInTextView.frame;//获取原始UITextView的frame
    //    CGSize  size = [text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(ScreenWidth, 2000) lineBreakMode:UILineBreakModeWordWrap];
    //    orgRect.size.height=size.height+10;//获取自适应文本内容高度
    //
    //    self.chatInTextView.frame=orgRect;//重设UITextView的frame
    //    self.scrollView.contentSize = CGSizeMake(ScreenWidth, self.chatInTextView.frame.size.height + 20);
    
    //    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //    paragraphStyle.lineSpacing = 5;// 字体的行间距
    //
    //    NSDictionary *attributes = @{
    //                                 NSFontAttributeName:[UIFont systemFontOfSize:15],
    //                                 NSParagraphStyleAttributeName:paragraphStyle
    //                                 };
    //    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:attributes];
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}



//左上角按钮
-(void)returnBackl
{
    if (self.chatInTextView.text.length > 0) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"是否需要保存到服务器" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"保存" ,nil];
        [alert show];
        alert.tag = 10000;
    }else{
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

-(void)returnBackr
{
    //保存人物设置模板
    if (!self.chatInTextView.text || [self.chatInTextView.text isEqualToString:@""]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"人物设置的内容不能为空哦" message:nil delegate:nil cancelButtonTitle:@"好的" otherButtonTitles: nil];
        [alert show];
        return;
    }
    NSDictionary * params = @{@"uid":USERID,@"storyid":self.storyId,@"content":self.chatInTextView.text};
    [HttpTool postWithPath:@"ModifyStoryRoleTemplate" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"人设模板设置成功" message:nil delegate:nil cancelButtonTitle:@"好的" otherButtonTitles: nil];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10000 &&buttonIndex == 1) {
        //保存修改的内容
        NSDictionary * params = @{@"uid":USERID,@"storyid":self.storyId,@"content":self.chatInTextView.text};
        [HttpTool postWithPath:@"ModifyStoryRoleTemplate" params:params success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"修改成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
                [alert show];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } failure:^(NSError *error) {
            
            
        }];
        
    }
    if (alertView.tag==10000&& buttonIndex==0) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
