//
//  IntroduceViewController.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧情 简介
#import "BaseViewController.h"

@interface IntroduceViewController : BaseViewController<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property(nonatomic ,strong)NSDictionary * dic;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
