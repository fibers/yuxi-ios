//
//  MajorPersonAddTextViewTableViewCell.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MajorPersonAddTextViewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end
