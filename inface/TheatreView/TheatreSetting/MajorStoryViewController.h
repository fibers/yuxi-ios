//
//  MajorStoryViewController.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧情 主线剧情
#import "BaseViewController.h"
#import "MajorStoryAddViewController.h"
#import "MajorStoryTableViewCell.h"
@interface MajorStoryViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic ,strong)NSDictionary * dic;
@property (strong, nonatomic) NSMutableArray *dataArr;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
