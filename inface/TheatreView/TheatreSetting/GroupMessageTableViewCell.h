//
//  GroupMessageTableViewCell.h
//  inface
//
//  Created by appleone on 15/8/24.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupModel.h"
#import "FriendModel.h"
@interface GroupMessageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *detailLable;

@property (weak, nonatomic) IBOutlet UIImageView *selectImageView;

-(void)setContent:(GroupModel *)model realGroupId:(NSString*)realGroupId;
@end
