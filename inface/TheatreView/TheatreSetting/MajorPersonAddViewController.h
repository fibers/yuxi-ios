//
//  MajorPersonAddViewController.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//主线人物添加
#import "BaseViewController.h"
#import "MajorPersonAddTextFieldTableViewCell.h"
#import "MajorPersonAddTextViewTableViewCell.h"
@interface MajorPersonAddViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UITextFieldDelegate>
{

    UILabel *remainCountLabel;//剩余字数label
    UITextField *countTextField;//人数
    UITextField *nameTextField;//名称
    PlaceholderTextView *introTextView;//介绍
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic ,strong)NSString *IsModify;
@property(nonatomic ,strong)NSDictionary * dic;
@property(nonatomic ,strong)NSString *storyid;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
