//
//  GroupMessageSetViewController.m
//  inface
//
//  Created by appleone on 15/8/24.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GroupMessageSetViewController.h"
#import "ShieldGroupMessageAPI.h"
#import "GroupMessageTableViewCell.h"
@interface GroupMessageSetViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    NSString * setType;
    
    NSString * selectedType;
}
@end

@implementation GroupMessageSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"群消息权限设置";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self getGroupMessageType];
    
    UITableViewHeaderFooterView * footerView = [[UITableViewHeaderFooterView alloc]init];
    self.tableView.tableFooterView = footerView;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

-(void)returnBackl
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)returnBackr
{
    ShieldGroupMessageAPI *request = [ShieldGroupMessageAPI new];
    NSMutableArray *array = [NSMutableArray new];
    [array addObject:self.group_id];
//    int realValue = 1-[selectedType intValue];
//    NSString * mesageType = [NSString stringWithFormat:@"%d",realValue];
    [array addObject:selectedType];
    [request requestWithObject:array Completion:^(id response, NSError *error) {
        if (error) {
            UIAlertView * alrt = [[UIAlertView alloc]initWithTitle:@"网络连接失败" message:@"操作失败了，确定重新试试" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alrt.tag = 10000;
            [alrt show];
            return ;
        }
        [self.navigationController popViewControllerAnimated:YES];

        [[DDDatabaseUtil instance]saveGroupReceiveMessage:self.group_id setType:selectedType];
    }];
}


-(void)getGroupMessageType
{
   [[DDDatabaseUtil instance]getGroupMessageSetType:self.group_id success:^(id json) {
       
       selectedType = json;
   }];
    if (selectedType ==NULL || [selectedType isEqual:nil]) {
        selectedType = @"0";
    }
    [self.tableView reloadData];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"GroupMessageSet";
    
    GroupMessageTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"GroupMessageTableViewCell" owner:nil options:nil]firstObject];
    }

        int  selectIndex = [selectedType intValue];
        if ( indexPath.row != selectIndex) {
            cell.selectImageView.hidden = YES;
        }else{
            cell.selectImageView.hidden = NO;
        }
   
    if (indexPath.row ==0) {
        cell.detailLable.text = @"提醒并接受消息";
        
    }else{
        cell.detailLable.text = @"不提醒但接受消息";
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedType = [NSString stringWithFormat:@"%ld",indexPath.row];
    [self.tableView reloadData];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}



#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==10000 && buttonIndex== 1) {
        [[DDTcpClientManager instance]disconnect];
        [self returnBackl];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
