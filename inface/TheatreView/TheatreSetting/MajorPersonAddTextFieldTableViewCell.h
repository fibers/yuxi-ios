//
//  MajorPersonAddTextFieldTableViewCell.h
//  inface
//
//  Created by Mac on 15/5/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MajorPersonAddTextFieldTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;

@end
