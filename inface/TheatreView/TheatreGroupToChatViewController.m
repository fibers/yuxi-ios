//
//  TheatreGroupToChatViewController.m
//  inface
//
//  Created by huangzengsong on 15/6/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "TheatreGroupToChatViewController.h"
#import "StorySessionsModel.h"
#import "GetGroupRoleName.h"
#import "MyLiveViewController.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+YXStyle.h"
@interface TheatreGroupToChatViewController ()

@end

@implementation TheatreGroupToChatViewController



//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    self.dataArr = [NSMutableArray array];
    
    self.navigationItem.title=@"聊天群列表";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 20)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 20)];
    }

    self.tableView.separatorColor=XIAN_COLOR;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    //获取知道剧中已经加入的剧群列表列表。
    [self GetEnteredStoryGroups];
}

//获取知道剧中已经加入的剧群列表列表。
-(void)GetEnteredStoryGroups{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"storyid":self.theatreid,@"token":@"110"};
    [HttpTool postWithPath:@"GetEnteredStoryGroups" params:params success:^(id JSON) {
        NSArray * groups=[JSON objectForKey:@"groups"];
        for (int i = 0; i < groups.count; i++) {
            NSString * isplay = [groups[i]objectForKey:@"isplay"];
            if (![isplay isEqualToString:@"3"]) {
                [self.dataArr addObject:groups[i]];
            }
        }
        if (self.dataArr.count > 0) {
            [self.tableView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];

        }
        if (self.dataArr.count == 0) {

            //暂时还不在剧中，需要将自己加到审核群中
            NSString *groupid=[NSString stringWithFormat:@"group_%@",self.checkGroupId];
            
            DDAddMemberToGroupAPI * addGroup = [[DDAddMemberToGroupAPI alloc]init];
            
            NSString *user_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];
            [addGroup requestWithObject:@[groupid,@[user_id]] Completion:^(id response, NSError *error) {
                SessionEntity * session = [[SessionModule sharedInstance]getSessionById:groupid];
                if (!session) {
                    session  = [[SessionEntity alloc]initWithSessionID:groupid type:SessionTypeSessionTypeGroup];
                }
                NSString * msgContent = [[NSUserDefaults standardUserDefaults]objectForKey:@"nickname"];
                NSString * msgtext = [NSString stringWithFormat:@"%@已经加入剧群",msgContent];
                DDMessageContentType msgContentType = DDMessageTypeText;
                session.unReadMsgCount = 1;
                ChattingModule * module = [[ChattingModule alloc]init];
                module.sessionEntity = session;
                DDMessageEntity * message = [DDMessageEntity makeMessage:msgtext Module:module MsgType:msgContentType isSendImage:NO isCurrentGroup:NO];
                [[DDMessageSendManager instance]sendMessage:message isGroup:YES Session:session completion:^(DDMessageEntity *message, NSError *error) {

                    
                } Error:^(NSError *error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"进入该剧的审核群失败" message:nil delegate:nil
                                                          cancelButtonTitle:@"确定" otherButtonTitles: nil];
                    [alert show];

                }];
                
            }];

        [[MyLiveViewController shareInstance]getMyStorySuccess:^{
            [HttpTool postWithPath:@"GetEnteredStoryGroups" params:params success:^(id JSON) {
                
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"已成功进入该剧的审核群" message:nil delegate:nil
                                                      cancelButtonTitle:@"确定" otherButtonTitles: nil];
                [alert show];

                NSArray * groups=[JSON objectForKey:@"groups"];
                for (int i = 0; i < groups.count; i++) {
                    NSString * isplay = [groups[i]objectForKey:@"isplay"];
                    if (![isplay isEqualToString:@"3"]) {
                        [self.dataArr addObject:groups[i]];
                    }
                }
                if (self.dataArr.count > 0) {
                    [self.tableView reloadData];
                    
                }
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            } failure:^(NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"进入该剧的审核群失败" message:nil delegate:nil
                                                      cancelButtonTitle:@"确定" otherButtonTitles: nil];
                [alert show];
                
            }];
            
        }];


            
            
//            //将自己成功添加到评论群,构造会话体，获取消息,列表中显示审核群
//            NSDictionary * groupInfo = @{@"title":self.checkTitle};//群的名字信息
//            
//            
//            NSDictionary * storyInfo  = @{@"title":[self.storyInfo objectForKey:@"title"],@"portrait":[self.storyInfo objectForKey:@"storyimg" ]};//剧的信息
//            
//            [[GetAllFamilysAndStorys shareInstance].theaterDic setObject:groupInfo forKey:self.checkGroupId];
//            
//            [[GetAllFamilysAndStorys shareInstance].groupsInfoDic setObject:storyInfo forKey:self.checkGroupId];
//            
//            //重新获取我的剧列表
//            [[MyLiveViewController shareInstance]getMyStorys];
//            if (self.storyInfo && ![self.storyInfo isEqual:nil]) {
//                //如果是从剧的详情页面进来的，直接获取审核群的信息
//                NSArray * groups = [self.storyInfo objectForKey:@"groups"];
//                for (NSDictionary * dic in groups) {
//                    if ([[dic objectForKey:@"groupid"]isEqualToString:self.checkGroupId]) {
//                        [self.dataArr addObject:dic];
//                        
//                        [self.tableView reloadData];
//                    }
//                }
//            }else{
//                //需要通过审核群的id审核群的标题构造字典
//                NSDictionary * checkGroupInfo = @{@"groupid":self.checkGroupId,@"title":self.checkTitle,@"isplay":@"0"};
//                [self.dataArr addObject:checkGroupInfo];
//                
//                [self.tableView reloadData];
//                
//            }

        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"获取数据失败" message:nil delegate:nil
                                              cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"TheatreGroupToChatTableViewCell";
    TheatreGroupToChatTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"TheatreGroupToChatTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.UserNameLabel.text=[[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"title"];
    NSMutableString * preStr = [[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"groupid"];
    __block NSString * group_id = [NSString stringWithFormat:@"group_%@",preStr];
    __block NSString * familyName = [[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"title"];

    SessionEntity * sessionEntity = [[SessionModule sharedInstance]getSessionById:group_id];
    if (!sessionEntity) {
        sessionEntity = [[SessionEntity alloc]initWithSessionID:group_id SessionName:familyName type:SessionTypeSessionTypeGroup];
//        [[SessionModule sharedInstance]addToSessionModel:sessionEntity];
    }
     int unreadGroupCount = (int)sessionEntity.unReadMsgCount;
    if (unreadGroupCount > 0) {
        cell.unreadCountLabel.text = [NSString stringWithFormat:@"%d",unreadGroupCount];
    
    }else{
        cell.unreadCountLabel.hidden = YES;
    }

    
    return cell;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 44;
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    NSMutableString * preStr = [[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"groupid"];
    NSString * isplay = [[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"isplay"];//判断是否是审核群的聊天
    
    __block NSString * group_id = [NSString stringWithFormat:@"group_%@",preStr];
    __block NSString * familyName = [[self.dataArr objectAtIndex:indexPath.row]objectForKey:@"title"];
    SessionEntity * sessionEntity = [[SessionModule sharedInstance]getSessionById:group_id];
    NSRange range = [sessionEntity.sessionID rangeOfString:@"_"];
    if (range.length == 0) {
        sessionEntity.sessionID = group_id;
    }
    if (!sessionEntity) {
        sessionEntity = [[SessionEntity alloc]initWithSessionID:group_id SessionName:familyName type:SessionTypeSessionTypeGroup];
//        [[SessionModule sharedInstance]addToSessionModel:sessionEntity];
    }
    
    ChatViewController * detailViewController = [ChatViewController shareInstance];
    if ([isplay isEqualToString:@"0"]) {
        detailViewController.isInCheckGroup = YES;//是否是审核群
    }
    detailViewController.isChatInStory = YES;//是否在剧里聊天
    
    detailViewController.checkGroupId = preStr;
    
    detailViewController.theartorId = self.theatreid;
    
    detailViewController.hidesBottomBarWhenPushed=YES;
    NSArray * viewControllers = self.navigationController.viewControllers;
    
    NSString *havechat=@"0";
    for (UIViewController * aViewController in viewControllers) {
        
        if ([aViewController isKindOfClass:[ChatViewController class]]) {
            havechat=@"1";
            
        }
        
    }
    if ([havechat isEqualToString:@"0"]) {
        [detailViewController showChattingContentForSession:sessionEntity];
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    } else {
        [detailViewController showChattingContentForSession:sessionEntity];
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController popToViewController:detailViewController animated:YES];
        
    }        //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //获取剧群的叫角色名
    [[GetGroupRoleName shareInstance]getGroupRoleName:self.theatreid groupid:preStr success:^(id JSON) {
          [[ChatViewController shareInstance].chatTableView reloadData];
    }];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
