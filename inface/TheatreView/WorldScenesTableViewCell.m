//
//  WorldScenesTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "WorldScenesTableViewCell.h"

@implementation WorldScenesTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.ContentLabel.textColor=BLACK_COLOR;
    self.TypeLabel.textColor=BLACK_CONTENT_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
