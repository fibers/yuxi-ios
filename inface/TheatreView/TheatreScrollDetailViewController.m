//
//  TheatreScrollDetailViewController.m
//  inface
//
//  Created by huangzengsong on 15/6/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "TheatreScrollDetailViewController.h"

@interface TheatreScrollDetailViewController ()

@end

@implementation TheatreScrollDetailViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=self.navTitle;
    if(self.storyID && ![self.storyID isEqualToString:@"0"] && ![self.storyID isEqualToString:@""])
    {
      UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 24)];

       [RightButton setImage:[UIImage imageNamed:@"familyunselect"] forState:UIControlStateNormal];
    
       [RightButton setImage:[UIImage imageNamed:@"familyunselect"] forState:UIControlStateHighlighted];

        [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
       self.navigationItem.rightBarButtonItem=barback2;
    }
    if ([self.storyID isEqualToString:@"0"]) {
        //此时加上分享功能
        UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 24)];
        
        [RightButton setImage:[UIImage imageNamed:@"shareImage"] forState:UIControlStateNormal];
        
        
        [RightButton addTarget:self action:@selector(shareBtn) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
        self.navigationItem.rightBarButtonItem=barback2;
    }
    self.aWebView.delegate=self;
    self.aWebView.scrollView.bounces=NO;//不弹跳
    
    activityIndicatorView = [ [ UIActivityIndicatorView  alloc ]
                             initWithFrame:CGRectMake(ScreenWidth/2.0-15,ScreenHeight/2.0,30.0,30.0)];
    activityIndicatorView.activityIndicatorViewStyle= UIActivityIndicatorViewStyleGray;
    [self.view addSubview:activityIndicatorView ];
    
    
    for (UIView *aView in [self.aWebView subviews])
    {
        if ([aView isKindOfClass:[UIScrollView class]])
        {
            //            [(UIScrollView *)aView setShowsVerticalScrollIndicator:NO]; //右侧的滚动条 （水平的类似）
            [(UIScrollView *)aView setShowsHorizontalScrollIndicator:NO]; //下侧的滚动条 （水平的类似）
            for (UIView *shadowView in aView.subviews)
            {
                
                if ([shadowView isKindOfClass:[UIImageView class]])
                {
                    shadowView.hidden = YES;  //上下滚动出边界时的黑色的图片 也就是拖拽后的上下阴影
                }
            }
        }
    }
    
    [self.aWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.contentUrl]]];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


#pragma mark 分享到第三方
-(void)shareBtn
{
    ShareModel * model = [ShareModel sharedInstance];
//    NSString * portrait = [self.dic objectForKey:@"portrait"];
    NSString * baiduUrl = self.contentUrl;
    NSString * title = self.navTitle;
//    NSString * intro = [self.dic objectForKey:@"intro"];
    NSDictionary * params = @{@"section":baiduUrl,@"title":title};
    [model shareToThird :nil dic:params];

}


-(void)returnBackr
{
    if ([self.type isEqualToString:@"0"]) {
        WorldDetailViewController * detailViewController= [[WorldDetailViewController alloc]init];
        detailViewController.storyID = self.storyID;
        detailViewController.hidesBottomBarWhenPushed = YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    if ([self.type isEqualToString:@"1"]) {
        DramaDetailViewController * detailViewController= [[DramaDetailViewController alloc]init];
        detailViewController.storyID = self.storyID;
        detailViewController.hidesBottomBarWhenPushed = YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}


- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [activityIndicatorView startAnimating ];//启动
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [activityIndicatorView stopAnimating ];//停止
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    [activityIndicatorView stopAnimating ];//停止
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
