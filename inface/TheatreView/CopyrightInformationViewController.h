//
//  CopyrightInformationViewController.h
//  inface
//
//  Created by Mac on 15/5/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//版权视图控制器（世界-剧-版权）
#import "BaseViewController.h"

@interface CopyrightInformationViewController : BaseViewController<UIWebViewDelegate>
{
    UIActivityIndicatorView* activityIndicatorView;
}
@property (strong, nonatomic) IBOutlet UIWebView *aWebView;
@property (nonatomic ,retain) NSString *navTitle;

@property (nonatomic ,retain) NSString *contentUrl;
@property(assign,nonatomic)    BOOL   isAnimating;


@end
