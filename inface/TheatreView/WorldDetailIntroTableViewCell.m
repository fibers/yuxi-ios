//
//  WorldDetailIntroTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "WorldDetailIntroTableViewCell.h"

@implementation WorldDetailIntroTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.TypeLabel.textColor=BLUE_CONTENT_COLOR;
    self.ContentLabel.textColor=BLACK_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
