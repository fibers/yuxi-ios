//
//  AppDelegate.m
//  inface
//
//  Created by Mac on 15/4/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
typedef void (^CCWebUrl)( NSURL *URL, NSRange textRange, NSArray *textRects);

#import "AppDelegate.h"
#import "GetUnreadNotify.h"
#import "GetGroupMemberNotify.h"
#import "PreLoginViewController.h"
#import "StorySessionsModel.h"
#import  "GetWordStorysByPagecount.h"
#import  "Util/EncodeUtil.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/SDImageCache.h>
#import "PersonSetModelViewController.h"
#import "LaunchViewController.h"
#import "GeTuiSdk.h"
#import "YXMyCenterViewController.h"
#import "WorldDetailViewController.h"
#import "DramaDetailViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "GroupsKindsViewController.h"
#import "YXTopicDetailViewController.h"
@interface YXAlertView :UIAlertView
@end

@implementation YXAlertView :UIAlertView
-(void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated{
    if(self.tag == 100000){
    }else{
        [super dismissWithClickedButtonIndex:buttonIndex animated:animated];
    }
}
@end

@interface AppDelegate ()<GeTuiSdkDelegate>
@property(nonatomic,copy)CCWebUrl  ccWebUrl;
@property(strong,nonatomic)UINavigationController * naviController;
@property(strong,nonatomic)NSString * clientId;
@property(strong,nonatomic)NSString * deviceId;

@property(strong,nonatomic)RACSignal* registerPushSignal;
@end

@implementation AppDelegate
-(RACSignal *)registerPushSignal{
    if (!_registerPushSignal) {
        
        RACSignal * userIdSingal = [[NSUserDefaults standardUserDefaults]rac_valuesAndChangesForKeyPath:@"userid" options:NSKeyValueObservingOptionInitial observer:nil];
        RACSignal * clientIdSingal = [self rac_valuesAndChangesForKeyPath:@"clientId" options:NSKeyValueObservingOptionInitial observer:nil];
        RACSignal * deviceId = [self rac_valuesAndChangesForKeyPath:@"deviceId" options:NSKeyValueObservingOptionInitial observer:nil];
        _registerPushSignal =[RACSignal combineLatest:@[userIdSingal,clientIdSingal,deviceId] reduce:^id(RACTuple * userIdTuple,RACTuple*clientIdTuple,RACTuple* deviceTuple){
            NSString * userId = [userIdTuple first];
            NSString * clientId = [clientIdTuple first];
            NSString * deviceId = [deviceTuple first];
            if ((userId != nil)&&(clientId!=nil)&&(deviceId != nil)) {
                return @(YES);
            }
            return @(NO);
            
        }];
    }
    return _registerPushSignal;
}
//
//-(RACSignal *)userIDSignal{
//    if (!_userIDSignal) {
//        _userIDSignal =[[NSUserDefaults standardUserDefaults]rac_valuesAndChangesForKeyPath:@"userid" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew observer:nil];
//    }
//    return _userIDSignal;
//}
//-(RACSignal *)clientIdSingal{
//    if (!_clientIdSingal) {
//        _clientIdSingal = RACObserve(self, @"clientId");
//    }
//    return _clientIdSingal;
//}

-(void)GetUpdate

{
    [HttpTool postWithPath:@"GetLatestPackageInfo" params:@{@"devicetype":@"2"} success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            
            adic=[JSON objectForKey:@"pkginfo"];
            NSString * updateMessage = [adic objectForKey:@"desc"];
            NSString *nowVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            NSString * versionStr = [adic objectForKey:@"versionnumber"];
            NSArray * serverVersions = [versionStr componentsSeparatedByString:@"."];
            NSArray * localVersion = [nowVersion componentsSeparatedByString:@"."];
            NSString * mustflag = [adic objectForKey:@"mustflag"];
            int serverNum = 0;
            int localNum = 0 ;
            for (int i = 0; i < serverVersions.count; i++) {
                int currentNum = [[serverVersions objectAtIndex:i]intValue] * pow(10, serverVersions.count - i - 1);
                serverNum += currentNum;
            }
            
            for (int i = 0; i < localVersion.count; i++) {
                int currentNum = [[localVersion objectAtIndex:i]intValue] * pow(10, localVersion.count - i - 1);
                localNum += currentNum;
            }
            
            if (localNum < serverNum) {
                if ([mustflag isEqualToString:@"0"]) {
                    //不需要更新
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:updateMessage message:nil delegate:self cancelButtonTitle:@"不搭理你" otherButtonTitles:@"这就去看", nil];
                    alert.tag=999999;
                    [alert show];
                }
                if ([mustflag isEqualToString:@"1"]) {
                    //必须更新
                    YXAlertView *alert = [[YXAlertView alloc]initWithTitle:updateMessage message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"这就去看", nil];
                    alert.tag=100000;
                    [alert show];
                }
                
            }
        }else{
            
            YXAlertView * alert = [[YXAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
    
    

}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self.registerPushSignal subscribeNext:^(NSNumber* isCanRegister) {
        BOOL  isCanRegisterPush =  [isCanRegister boolValue];
        if(isCanRegisterPush){
            NSDictionary * param = @{@"client_id":self.clientId,@"user_id":USERID,@"device_id":self.deviceId,@"device_type":@"ios"};
            [[AFHTTPSessionManager manager]POST:[NSString stringWithFormat:@"%@/%@",SERVER_PUSH,@"register_push"] parameters:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                LOG(@"注册推送 %@",responseObject);
            } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
                LOG(@"注册登陆失败！");
            }];
        }
//        LOG(@"isCanRegisterPush %@",isCanRegisterPush);
    }];
    // 通过个推平台分配的appId、 appKey 、appSecret 启动SDK，注：该方法需要在主线程中调用
    [GeTuiSdk startSdkWithAppId:kGtAppId appKey:kGtAppKey appSecret:kGtAppSecret delegate:self];
    // 注册APNS
    // 处理远程通知启动APP
    [self receiveNotificationByLaunchingOptions:launchOptions];
    
    
    [[SDImageCache sharedImageCache]setMaxCacheSize:1024*1024*50];
    [[SDImageCache sharedImageCache]setMaxMemoryCost:1024*1024*50];
    //崩溃日志
    [Fabric with:@[[Crashlytics class]]];
    NSString * nickName = [[NSUserDefaults standardUserDefaults] objectForKey:@"nickname"];
    [CrashlyticsKit setUserIdentifier:USERID == nil?@"unknown":USERID];
    [CrashlyticsKit setUserName:nickName == nil ?@"unknown":nickName];
    
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    //版本更新
        [self GetUpdate];
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
    manager.shouldToolbarUsesTextFieldTintColor = YES;
    manager.enableAutoToolbar = YES;
    //聊天页面不用这个键盘
    
    //    //状态栏的字体颜色设置为白色
    //    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    //用户没登录,如果第一次登录或者有过登出操作，需要有登录操作
    if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"loginstatus"]isEqualToString:@"success"]) {
        
        //这里初始化判断变量
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"]) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        }
        else {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLaunch"];
        }
        //这里判断是否为第一次
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]) {
 
            [self firstLaunch];
        }else{
            [self delayMethod];

        }
        
        
    }else{
        PreLoginViewController *aLoginViewController=[[PreLoginViewController alloc]init];
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:aLoginViewController];
        
        self.window.rootViewController=nav;
        
        
        //已经登录过了，再次进入程序时直接跳转到剧场
        NSString * username=[[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
        NSString * password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        
        /*
         临时测试代码
         */
        [self toLoginUserName:username withPassword:password];
        
    }
    
    //  友盟的方法本身是异步执行，所以不需要再异步调用
    [self umengTrack];
    
    // 初始化代码表
    [EncodeUtil getEmojilist];
    
    //替换为语戏的了
    [ShareSDK registerApp:@"6d21e83d535c"];//字符串api20为您的ShareSDK的AppKey
    
    //添加QQ空间应用  注册网址  http://connect.qq.com/intro/login/
    [ShareSDK connectQZoneWithAppKey:@"1104542960"
                           appSecret:@"zzNOx4dxlfcVjfxg"
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
    [ShareSDK connectQQWithQZoneAppKey:@"1104542960" qqApiInterfaceCls:[QQApiInterface class] tencentOAuthCls:[TencentOAuth class]];
    
    //微信登录的时候需要初始化
    [ShareSDK connectWeChatWithAppId:@"wx7c628069dcb97857"
                           appSecret:@"66e16eb2acf92974c55898fd38c959f9"
                           wechatCls:[WXApi class]];
    [ShareSDK connectWeChatWithAppId:@"wx7c628069dcb97857" wechatCls:[WXApi class]];
    [ShareSDK connectWeChatTimelineWithAppId:@"wx7c628069dcb97857" wechatCls:[WXApi class]];
    
    [ShareSDK connectSinaWeiboWithAppKey:@"1952634941" appSecret:@"45c3780b11f2fa0c73b2c1c83f7838c1" redirectUri:@"http://www.sharesdk.cn" weiboSDKCls:[WeiboSDK class]];    //设置导航栏背景字体等等

    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:BLUECOLOR,NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:17],NSFontAttributeName, nil]];
    [[UINavigationBar appearance]  setBarTintColor:TABBAR_COLOR];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
        [UINavigationBar appearance].translucent = NO;
        
    }
    
    [[UINavigationBar appearance]  setTintColor:BLUECOLOR];
    
    //消息推送
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound) categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // for iOS 7 or iOS 6
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    [self.window makeKeyAndVisible];
    
    /// === 点击推送，带参数启动 Begin ===
    static BOOL isShowed = NO;
    NSDictionary * notifDic = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notifDic)
    {
        NSString * payloadStr = [notifDic objectForKey:@"payload"];
        if (!IsStrEmpty(payloadStr)) {
            
            NSData * jsonData = [payloadStr dataUsingEncoding:NSUTF8StringEncoding];
            NSError * error;
            NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
            NSString * actionStr = dic[@"a"];
            
            NSString * actionType = [[actionStr componentsSeparatedByString:@":"]objectAtIndex:0];
            
        
            [[self rac_valuesAndChangesForKeyPath:@"aBaseTabBarViewController" options:NSKeyValueObservingOptionInitial observer:self]subscribeNext:^(id x)
            {
                if (isShowed) {
                    return ;
                }
                 UINavigationController * navTmp = self.aBaseTabBarViewController.SelectNav;
                if (navTmp == nil)
                    return;
                if ([actionStr isEqualToString:@"ma"]) {
                    NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                    DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                    messageEntity.state = DDmessageSendSuccess;
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"GETFRIENDLIST" object:nil];
                    [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                        //在后台点击时跳转到视图列表
                        GroupsKindsViewController *detailViewController = [[GroupsKindsViewController alloc]init];
                        [self.aBaseTabBarViewController tabBarController:self.aBaseTabBarViewController didSelectViewController:[self.aBaseTabBarViewController.viewControllers objectAtIndex:3]];
                        self.aBaseTabBarViewController.selectedIndex = 3;
                        detailViewController.hidesBottomBarWhenPushed = YES;
                        [self.aBaseTabBarViewController.nv2 pushViewController:detailViewController animated:YES];
                        [detailViewController selectAddMasterState];
                        return;
                    
                }
                if ([actionType isEqualToString:@"p" ]) {
                    //调到话题详情页
                    YXTopicDetailViewController * detailViewController = [[YXTopicDetailViewController alloc]init];
                    NSString * topicId = [actionStr stringByReplacingOccurrencesOfString:@"p:" withString:@""];
                    [detailViewController setTopicID:topicId];
                    [detailViewController setType:YXTopicDetailTypeStory];
                    detailViewController.hidesBottomBarWhenPushed = YES;
                    [navTmp pushViewController:detailViewController animated:YES];
                    return;
                }
                if ([actionType isEqualToString:@"s"]) {
                    //跳转到剧的详情
                    WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc]init];
                    NSString * topicId = [actionStr stringByReplacingOccurrencesOfString:@"s:" withString:@""];
                    [detailViewController setStoryID:topicId];
                    detailViewController.hidesBottomBarWhenPushed = YES;
                    [navTmp pushViewController:detailViewController animated:YES];
                    return;
                }
                if([actionType isEqualToString:@"ss"]){
                    //跳转到自戏的详情页
                    DramaDetailViewController * detailViewController = [[DramaDetailViewController alloc]init];
                    NSString * topicId = [actionStr stringByReplacingOccurrencesOfString:@"ss:" withString:@""];
                    [detailViewController setStoryID:topicId];
                    detailViewController.hidesBottomBarWhenPushed = YES;
                    [navTmp pushViewController:detailViewController animated:YES];
                    return;
                }
                if ([actionType isEqualToString:@"u"]) {
                    //跳转到个人主页
                    NSString * topicId = [actionStr stringByReplacingOccurrencesOfString:@"u:" withString:@""];
                    YXMyCenterViewController * myCenterVC = [[YXMyCenterViewController alloc]init];
                    [myCenterVC setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:topicId];
                    myCenterVC.hidesBottomBarWhenPushed = YES;
                    [navTmp pushViewController:myCenterVC animated:YES];
                    return;
                }
                if ([actionType isEqualToString:@"uf1"]) {
                    //同时更新好友列表
                    NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                    DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                    messageEntity.state = DDmessageSendSuccess;
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"GETFRIENDLIST" object:nil];
                    [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"GETFRIENDLIST" object:nil];
                        GroupsKindsViewController *detailViewController = [[GroupsKindsViewController alloc]init];
                        [self.aBaseTabBarViewController tabBarController:self.aBaseTabBarViewController didSelectViewController:[self.aBaseTabBarViewController.viewControllers objectAtIndex:3]];
                        self.aBaseTabBarViewController.selectedIndex = 3;
                        detailViewController.hidesBottomBarWhenPushed = YES;
                        [self.aBaseTabBarViewController.nv2 pushViewController:detailViewController animated:YES];
                        [detailViewController selectAddMasterState];
                        return;
                }
                
            }];
            isShowed = YES;
        }
        
    }
/// === 推送带参数启动 End ===
    return YES;
}

-(void)delayMethod
{
    
    LoginViewController * login = [[LoginViewController alloc]init];
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:login];
    
    self.window.rootViewController = nav;
}




//友盟统计分析
- (void)umengTrack {
    //    [MobClick setCrashReportEnabled:NO]; // 如果不需要捕捉异常，注释掉此行
    //    [MobClick setLogEnabled:YES];  // 打开友盟sdk调试，注意Release发布时需要注释掉此行,减少io消耗
    //    [MobClick setAppVersion:XcodeAppVersion]; //参数为NSString * 类型,自定义app版本信息，如果不设置，默认从CFBundleVersion里取
    //
    [MobClick startWithAppkey:@"55b9c23067e58efc14000203" reportPolicy:(ReportPolicy) REALTIME channelId:nil];
    //   reportPolicy为枚举类型,可以为 REALTIME, BATCH,SENDDAILY,SENDWIFIONLY几种
    //   channelId 为NSString * 类型，channelId 为nil或@""时,默认会被被当作@"App Store"渠道
    
    //      [MobClick checkUpdate];   //自动更新检查, 如果需要自定义更新请使用下面的方法,需要接收一个(NSDictionary *)appInfo的参数
    //    [MobClick checkUpdateWithDelegate:self selector:@selector(updateMethod:)];
    
    [MobClick updateOnlineConfig];  //在线参数配置
    
    //    1.6.8之前的初始化方法
    //    [MobClick setDelegate:self reportPolicy:REALTIME];  //建议使用新方法
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onlineConfigCallBack:) name:UMOnlineConfigDidFinishedNotification object:nil];
    
}
- (void)onlineConfigCallBack:(NSNotification *)note {
    
    NSLog(@"online config has fininshed and note = %@", note.userInfo);
}

-(void)firstLaunch
{
    WelcomeViewController * detailViewController = [[WelcomeViewController alloc] init];
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:detailViewController];
    self.window.rootViewController = nav;
}

- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    if ([[url scheme] isEqualToString:@"wx7c628069dcb97857"]){
        
        
        return [ShareSDK handleOpenURL:url
                            wxDelegate:self];
        
    }else{
        return [TencentOAuth HandleOpenURL:url ];
        
    }
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    ///对语戏协议的响应
    if ([[url scheme] isEqualToString:@"yuxi"]) {
       
        [[self rac_valuesAndChangesForKeyPath:@"aBaseTabBarViewController" options:NSKeyValueObservingOptionInitial observer:self]subscribeNext:^(id x) {
            //            LOG(@"%@",@"-------------------------------------123");
            UINavigationController * navTmp = self.aBaseTabBarViewController.SelectNav;
            if (navTmp != nil)
            {
                if([[url host] isEqualToString:@"story"])
                {
                    NSString * sid = [[url path]stringByReplacingOccurrencesOfString:@"/" withString:@""];
                    if(sid != nil)
                    {
                    WorldDetailViewController * worldVC = [[WorldDetailViewController alloc]init];
                    [worldVC setStoryID:sid];
                    worldVC.hidesBottomBarWhenPushed = YES;
                    [navTmp pushViewController:worldVC animated:YES];
                    }
                }
                if([[url host] isEqualToString:@"selfstory"])
                {
                    NSString * did = [[url path]stringByReplacingOccurrencesOfString:@"/" withString:@""];
                    if(did != nil)
                    {
                        DramaDetailViewController * dramaVC = [[DramaDetailViewController alloc]init];
                        [dramaVC setStoryID:did];
                        dramaVC.hidesBottomBarWhenPushed = YES;
                        [navTmp pushViewController:dramaVC animated:YES];
                    }
                }
                if([[url host] isEqualToString:@"person"])
                {
                    NSString * pid = [[url path]stringByReplacingOccurrencesOfString:@"/" withString:@""];
                    if(pid != nil)
                    {
                        YXMyCenterViewController * myCenterVC = [[YXMyCenterViewController alloc]init];
                        [myCenterVC setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:pid];
                        myCenterVC.hidesBottomBarWhenPushed = YES;
                        [navTmp pushViewController:myCenterVC animated:YES];
                    }
                }
            }
        }];
        return YES;
    }

    
    
    if ([[url scheme] isEqualToString:@"wx7c628069dcb97857"]){
        
        return [ShareSDK handleOpenURL:url
                     sourceApplication:sourceApplication
                            annotation:annotation
                            wxDelegate:self];
        
    }else {
        return [TencentOAuth HandleOpenURL:url ];
        
    }
//    return NO;
    
}



- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    if ([self.delegate respondsToSelector:@selector(setUnreadCount)]) {
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:[self.delegate setUnreadCount]];
    }
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [NSString stringWithFormat:@"%@", deviceToken];
    NSString *dt = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    NSString *dn = [dt stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [RuntimeStatus instance].pushToken= [dn stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    ///个推
    NSString *myToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    myToken = [myToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.deviceId = myToken;
    [GeTuiSdk registerDeviceToken:myToken];    /// 向个推服务器注册deviceToken
    NSLog(@"\n>>>[DeviceToken Success]:%@\n\n",myToken);
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    ///个推
    [GeTuiSdk registerDeviceToken:@""];     /// 如果APNS注册失败，通知个推服务器
    NSLog(@"\n>>>[DeviceToken Error]:%@\n\n",error.description);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    // 处理自定义(个推)推送消息
    UIApplicationState state =application.applicationState;

    NSString * payloadStr = [userInfo objectForKey:@"payload"];
    if (!IsStrEmpty(payloadStr)) {
        
    NSData * jsonData = [payloadStr dataUsingEncoding:NSUTF8StringEncoding];
    NSError * error;
    NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    NSString * actionStr = dic[@"a"];
    if ([actionStr isEqualToString:@"ma"]) {
        NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
        DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
        messageEntity.state = DDmessageSendSuccess;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"GETFRIENDLIST" object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
        if (state == UIApplicationStateInactive) {
            //在后台点击时跳转到视图列表
            GroupsKindsViewController *detailViewController = [[GroupsKindsViewController alloc]init];
            [self.aBaseTabBarViewController tabBarController:self.aBaseTabBarViewController didSelectViewController:[self.aBaseTabBarViewController.viewControllers objectAtIndex:3]];
            self.aBaseTabBarViewController.selectedIndex = 3;
            detailViewController.hidesBottomBarWhenPushed = YES;
            [self.aBaseTabBarViewController.nv2 pushViewController:detailViewController animated:YES];
            [detailViewController selectAddMasterState];
            return;
        }
        return;

    }
 
    NSString * actionType = [[actionStr componentsSeparatedByString:@":"]objectAtIndex:0];

    UINavigationController * navTmp = self.aBaseTabBarViewController.SelectNav;
    if (navTmp == nil)
        return ;
            
    if ([actionType isEqualToString:@"p" ] && state == UIApplicationStateInactive) {
        //调到话题详情页
        YXTopicDetailViewController * detailViewController = [[YXTopicDetailViewController alloc]init];
        NSString * topicId = [actionStr stringByReplacingOccurrencesOfString:@"p:" withString:@""];
        [detailViewController setTopicID:topicId];
        [detailViewController setType:YXTopicDetailTypeStory];
        detailViewController.hidesBottomBarWhenPushed = YES;
        [navTmp pushViewController:detailViewController animated:YES];
        return;
    }
    if ([actionType isEqualToString:@"s"] && state == UIApplicationStateInactive) {
        //跳转到剧的详情
        WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc]init];
        NSString * topicId = [actionStr stringByReplacingOccurrencesOfString:@"s:" withString:@""];
        [detailViewController setStoryID:topicId];
        detailViewController.hidesBottomBarWhenPushed = YES;
        [navTmp pushViewController:detailViewController animated:YES];
        return;
    }
    if([actionType isEqualToString:@"ss"] && state == UIApplicationStateInactive){
        //跳转到自戏的详情页
        DramaDetailViewController * detailViewController = [[DramaDetailViewController alloc]init];
        NSString * topicId = [actionStr stringByReplacingOccurrencesOfString:@"ss:" withString:@""];
        [detailViewController setStoryID:topicId];
        detailViewController.hidesBottomBarWhenPushed = YES;
        [navTmp pushViewController:detailViewController animated:YES];
        return;
    }
    if ([actionType isEqualToString:@"u"] && state == UIApplicationStateInactive) {
        //跳转到个人主页
        NSString * topicId = [actionStr stringByReplacingOccurrencesOfString:@"u:" withString:@""];
        YXMyCenterViewController * myCenterVC = [[YXMyCenterViewController alloc]init];
        [myCenterVC setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:topicId];
        myCenterVC.hidesBottomBarWhenPushed = YES;
        [navTmp pushViewController:myCenterVC animated:YES];
        return;
    }
    if ([actionType isEqualToString:@"uf1"]) {
                //同事更新好友列表
                NSString * sessionId = [NSString stringWithFormat:GROUP_MESSAGE];
                DDMessageEntity * messageEntity = [[DDMessageEntity alloc]initWithMsgID:[DDMessageModule getMessageID] msgType:MsgTypeMsgTypeSingleText msgTime:[[NSDate date]timeIntervalSince1970] sessionID:sessionId senderID:sessionId msgContent:nil toUserID:sessionId];
                messageEntity.state = DDmessageSendSuccess;
                [[NSNotificationCenter defaultCenter]postNotificationName:@"GETFRIENDLIST" object:nil];
                [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationReceiveMessage object:messageEntity];
                
                if (state == UIApplicationStateInactive) {
                    GroupsKindsViewController *detailViewController = [[GroupsKindsViewController alloc]init];
                    [self.aBaseTabBarViewController tabBarController:self.aBaseTabBarViewController didSelectViewController:[self.aBaseTabBarViewController.viewControllers objectAtIndex:3]];
                    self.aBaseTabBarViewController.selectedIndex = 3;
                    detailViewController.hidesBottomBarWhenPushed = YES;
                    [self.aBaseTabBarViewController.nv2 pushViewController:detailViewController animated:YES];
                    [detailViewController selectAddMasterState];
                    return;
                }
            }

    }else{
        
    // 处理IM推送消息

    NSDictionary * bodyDic = [userInfo objectForKey:@"aps"];
    NSString * alert = [[bodyDic objectForKey:@"alert"]objectForKey:@"body"];
    
    NSRange  range = [alert rangeOfString:@":"];
//    NSString * message = [alert substringWithRange:NSMakeRange(range.location + 1, alert.length - range.location -1)];
    
    NSString *jsonString = [userInfo safeObjectForKey:@"custom"];
    NSData* infoData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* info = [NSJSONSerialization JSONObjectWithData:infoData options:0 error:nil];
    
    NSInteger from_id =[[info safeObjectForKey:@"from_id"] integerValue];
    
    NSString  *fromId = [info safeObjectForKey:@"from_id"];
    NSString * session_id = @"";
    SessionType type = (SessionType)[[info safeObjectForKey:@"msg_type"] integerValue];
    
//    if (type ==1) {
//        session_id = [NSString stringWithFormat:@"user_%@",fromId];
//    }else{
//        session_id = [NSString stringWithFormat:@"group_%@",fromId];
//    }
    //
    if (from_id ==1000001) {
        //戏君发的消息，需要判断是否有链接，有链接的话需要跳转到网页
        //        [self pushToWebView:alert];
        //判断消息中是否有超链接
        NSError *error;
        NSMutableArray *urls;
        NSMutableArray *urlRanges;
        
        urlRanges   =   [[NSMutableArray alloc] init];
        urls        =   [[NSMutableArray alloc] init];
        
        NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];
        NSArray *arrayOfAllMatches = [regex matchesInString:alert
                                                    options:0
                                                      range:NSMakeRange(0, [alert length])];
        
        
        for (NSTextCheckingResult *match in arrayOfAllMatches) {
            //没有匹配的场合。
            if (match.range.location == NSNotFound ) {
                continue;
            }
            NSString *substringForMatch =   [alert substringWithRange:match.range];
            NSURL    *suburl            =   [NSURL URLWithString:substringForMatch];
            
            [urls addObject:suburl];
            [urlRanges addObject:[NSValue valueWithRange:match.range]];
            
            
        }
        if (urls.count >0) {
            
            NSString * url = [urls[0] absoluteString];
            NSRange  range  = [url rangeOfString:@"www.yuxip.com"];
            if (range.length > 0) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

            }
            else if (self.aBaseTabBarViewController.SelectNav != nil){
                if (state == UIApplicationStateInactive) {
                    
                HelpInformationViewController * detailViewController = [[HelpInformationViewController alloc] init];
                detailViewController.navTitle   =   url;
                detailViewController.contentUrl =   url;
                detailViewController.hidesBottomBarWhenPushed=YES;
                if (detailViewController.isAnimating) {
                    return;
                }
                detailViewController.isAnimating = YES;
                [self.aBaseTabBarViewController.SelectNav pushViewController:detailViewController animated:YES];
            }
            }
            return;
        }
        
    }else{
                [self.aBaseTabBarViewController tabBarController:self.aBaseTabBarViewController didSelectViewController:[self.aBaseTabBarViewController.viewControllers objectAtIndex:3]];
                self.aBaseTabBarViewController.selectedIndex = 3;
        if ([self.delegate respondsToSelector:@selector(getLocalRecents)]) {
            [self.delegate getLocalRecents];
        }
    }
    }
    
//    NSInteger group_id =[[info safeObjectForKey:@"group_id"] integerValue];
    //如果是普通群的消息则进入
    //    if (fromId) {
    //        NSInteger sessionId = type==1?from_id:group_id;
    //
    //        SessionEntity * session = [[SessionModule sharedInstance]getSessionById:session_id];
    //        if (!session) {
    //            session = [[SessionEntity alloc]initWithSessionID:session_id type:type];
    //        }
    //        ChatViewController * detailViewController = [ChatViewController shareInstance];
    //        detailViewController.hidesBottomBarWhenPushed = YES;
    //        [detailViewController showChattingContentForSession:session];
    //        //要处理锁屏
    ////        [self.aBaseTabBarViewController tabBarController:self.aBaseTabBarViewController didSelectViewController:[self.aBaseTabBarViewController.viewControllers objectAtIndex:3]];
    //        self.aBaseTabBarViewController.selectedIndex = 3;
    //
    //        NSLog(@"当前的视图:%@",[self.aBaseTabBarViewController.viewControllers objectAtIndex:3]);
    //
    //
    //        //需要判断chatView是否已经是在栈顶
    //        NSArray * viewControllers = self.aBaseTabBarViewController.nv2.viewControllers;
    //        BOOL  isExistChat  =NO;
    //        for (UIViewController * aViewController in viewControllers) {
    //            if ([aViewController isKindOfClass:[ChatViewController class]]) {
    //                isExistChat = YES;
    //            }
    //        }
    //        if (isExistChat) {
    //            [self.aBaseTabBarViewController.nv2 popToViewController :detailViewController animated:YES];
    //
    //        }else{
    //            [self.aBaseTabBarViewController.nv2 pushViewController:detailViewController animated:YES
    //             ];
    //
    //        }
    //
    //    }
    //    NSLog(@"收到推送消息:%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]);
    
}



- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"loginstatus"]isEqualToString:@"success"]){
        
        NSString * username=[[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
        NSString * password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        [self connectWithTCPServerName:username withPassword:password];
        if ([self.delegate respondsToSelector:@selector(getLocalRecents)]) {
            [self.delegate getLocalRecents];
        }

    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    //    [[FriendsViewController shareInstance]getLocalSessions];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)connectWithTCPServerName :(NSString*)userName withPassword:(NSString*)password
{
    [[LoginModule instance] loginWithUsername:userName password:password success:^(DDUserEntity *user) {
        [[GetUnreadNotify shareInsatance]getUnreadNotify];
        //        [[GetLocalRecentSessions shareInstance]getLocalSessionSuccess:^(id JSON) {
        //            //再次获取消息 ，此时让friendview刷新表格
        //            [[NSNotificationCenter defaultCenter]postNotificationName:GET_LOCAL_SESSIONS object:nil];
        //        }];
        //
    } failure:^(NSString *error) {
        
        
    }];
    
}

-(void)toLoginUserName:(NSString *)userName withPassword:(NSString *)password{
    
    [[LoginModule instance] loginWithUsername:userName password:password success:^(DDUserEntity *user) {
        if (user) {
            //添加好友或者被添加成员时默认通知
            [GetGroupMemberNotify shareInsatance];
            
            [[GetAllFamilysAndStorys shareInstance]getAllFamilysAndStorysSuccess:^(id json) {
                
                self.window.rootViewController = json;
                self.aBaseTabBarViewController = (BaseTabBarViewController*)json;
                [[GetUnreadNotify shareInsatance]getUnreadNotify];
            } failure:^(id failure) {
                self.window.rootViewController = failure;
                self.aBaseTabBarViewController = (BaseTabBarViewController*)failure;
                
            }];
            
            [[GetAllFamilysAndStorys shareInstance]GetFavoriteEmotionList];
            
            
            
        }
        
        
    } failure:^(NSString *error) {
        
        BaseTabBarViewController * baseViewController = [[BaseTabBarViewController alloc]init];
        self.window.rootViewController =baseViewController;
        self.aBaseTabBarViewController = baseViewController;
        
//        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"当前收不到二次元信号…" message:@"请检查一下自己的网络" delegate:self cancelButtonTitle:nil otherButtonTitles:@"重新连接",nil];
//        alertview.tag=1200;
//        [alertview show];
    }];
    
}


#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==1200) {
        if (buttonIndex== 1) {
            
//            //已经登录过了，再次进入程序时直接跳转到剧场
//            NSString * username=[[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
//            NSString * password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
//            [self toLoginUserName:username withPassword:password];
        }
    }else if (alertView.tag==999999) {
        if(buttonIndex==1)
            
        {
            
            NSURL *url = [NSURL URLWithString:[adic objectForKey:@"downurl"]];
            
            [[UIApplication sharedApplication]openURL:url];
            
        }
    }
    if (alertView.tag == 100000 && buttonIndex == 0) {
        NSURL *url = [NSURL URLWithString:[adic objectForKey:@"downurl"]];
        
        [[UIApplication sharedApplication]openURL:url];
    }
}

#pragma mark -
#pragma mark 个推

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    /// Background Fetch 恢复SDK 运行
    [GeTuiSdk resume];
    completionHandler(UIBackgroundFetchResultNewData);
}


/** SDK启动成功返回cid */
- (void)GeTuiSdkDidRegisterClient:(NSString *)clientId {
    // [4-EXT-1]: 个推SDK已注册，返回clientId
    self.clientId = clientId;
    NSLog(@"\n>>>[GeTuiSdk RegisterClient]:%@\n\n", clientId);
}

/** SDK遇到错误回调 */
- (void)GeTuiSdkDidOccurError:(NSError *)error {
    // [EXT]:个推错误报告，集成步骤发生的任何错误都在这里通知，如果集成后，无法正常收到消息，查看这里的通知。
    NSLog(@"\n>>>[GexinSdk error]:%@\n\n", [error localizedDescription]);
}

/** SDK收到透传消息回调 */
- (void)GeTuiSdkDidReceivePayload:(NSString *)payloadId andTaskId:(NSString *)taskId andMessageId:(NSString *)aMsgId andOffLine:(BOOL)offLine fromApplication:(NSString *)appId {
    
    // [4]: 收到个推消息
    NSData *payload = [GeTuiSdk retrivePayloadById:payloadId];
    NSString *payloadMsg = nil;
    if (payload) {
        payloadMsg = [[NSString alloc] initWithBytes:payload.bytes length:payload.length encoding:NSUTF8StringEncoding];
    }
    
    NSString *msg = [NSString stringWithFormat:@" payloadId=%@,taskId=%@,messageId:%@,payloadMsg:%@%@",payloadId,taskId,aMsgId,payloadMsg,offLine ? @"<离线消息>" : @""];                                          
    NSLog(@"\n>>>[GexinSdk ReceivePayload]:%@\n\n", msg);
    
    /**
     *汇报个推自定义事件
     *actionId：用户自定义的actionid，int类型，取值90001-90999。
     *taskId：下发任务的任务ID。
     *msgId： 下发任务的消息ID。
     *返回值：BOOL，YES表示该命令已经提交，NO表示该命令未提交成功。注：该结果不代表服务器收到该条命令
     **/
    [GeTuiSdk sendFeedbackMessage:90001 taskId:taskId msgId:aMsgId];
}

/** 自定义：APP被“推送”启动时处理推送消息处理（APP 未启动--》启动）*/
- (void)receiveNotificationByLaunchingOptions:(NSDictionary *)launchOptions {
    if (!launchOptions) return;
    
    /*
     通过“远程推送”启动APP
     UIApplicationLaunchOptionsRemoteNotificationKey 远程推送Key
     */
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo) {
        NSLog(@"\n>>>[Launching RemoteNotification]:%@",userInfo);
    }
}


@end
