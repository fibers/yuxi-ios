//
//  friendModel.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendModel : NSObject
@property (nonatomic, copy) NSString *userid;

@property (nonatomic, copy) NSString *realname;

@property (nonatomic, copy) NSString *markname;

@property (nonatomic, copy) NSString *gender;

@property (nonatomic, copy) NSString *portrait;

@property (nonatomic, copy) NSString *groupid;

@end
