//
//  FriendGroupModel.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "FriendGroupModel.h"
#import "GroupModel.h"
@implementation FriendGroupModel
+(instancetype)shareInstance
{
    static  FriendGroupModel * model;
    static dispatch_once_t  oneceToken;
    dispatch_once(&oneceToken,^{
        model = [[FriendGroupModel alloc]init];
    });
    return model;
}

+ (NSDictionary *)objectClassInArray{
    return @{@"friendgroups" : [GroupModel class]};
}
@end
