//
//  IntroductionMasCell.h
//  inface
//
//  Created by appleone on 15/12/7.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroductionMasCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *potraitButton;

@property (weak, nonatomic) IBOutlet UILabel *detailTextLal;
@end
