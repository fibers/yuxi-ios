//
//  FriendsViewController.h
//  inface
//
//  Created by xigesi on 15/4/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsTableViewCell.h"
#import "InformationTableViewCell.h"
#import "SearchFriendsViewController.h"
#import "ChatViewController.h"
#import "DDClientState.h"
#import "NewExamineTableViewCell.h"
#import "SearchFamilyViewController.h"
#import "SearchTheatreViewController.h"
#import "MyFamilyViewController.h"
#import "MyLiveViewController.h"
#import "LjjUISegmentedControl.h"
#import "YXFamilyViewController.h"
@interface FriendsViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,SessionModuelDelegate,UIActionSheetDelegate,LjjUISegmentedControlDelegate>
{
    
    NSString *switching;
    int unreadCount;
    NSMutableArray * sectionsArr;//三个分区
    NSMutableArray * rowsArr;//每个区的联系人
    NSMutableArray * familysArr;// 家族列表
    NSMutableArray * storysArr;//剧列表
    NSMutableArray * myFriends;//好友列表
    NSMutableArray * messagesArr;//消息列表
    NSMutableDictionary * showDic;
    
    UIActionSheet* mySheet;
    
    UIImageView *aimageView;
    UIImageView *bimageView;
}
+(instancetype)shareInstance;
@property (strong,nonatomic) UIRefreshControl *refreshControlR;
@property (strong, nonatomic) IBOutlet UITableView * tableViewOutlet;
@property (strong, nonatomic) NSMutableArray *tableData;
@property (assign,nonatomic)  BOOL  hasCliked;//禁止双击操作
@property (strong, nonatomic) NSMutableArray *tableDataFriends;
@property (strong,nonatomic)  NSMutableArray * currentgroupMessages;//评论群的消息
-(void)sessionUpdate:(SessionEntity *)session Action:(SessionAction)action;
-(void)getLocalSessions;
-(void)addNewFriends:(NSString *)nickname sessioniD:(NSString*)sessionId;
-(void)getUnreadCount:(void(^)(int count))success;//获取未读消息数
-(void)deleteSession:(NSString *)sessionId;//退群后删除相应的会话体
-(void)getFriendsNickName;
@end
