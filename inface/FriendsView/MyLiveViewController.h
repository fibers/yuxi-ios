//
//  MyLiveViewController.h
//  inface
//
//  Created by huangzengsong on 15/7/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "MyLiveTableViewCell.h"
#import "MyLiveHeaderTableViewCell.h"

#import "GetAllFamilysAndStorys.h"
#import "SaveStoryDetailByID.h"

@interface MyLiveViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
+(instancetype)shareInstance;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;
@property (assign,nonatomic) BOOL  isAnimating;
-(void)getMyStorys;


-(void)getMyStorySuccess:(void(^) ())success;

-(void)agreeByStory:(NSString *)groupId;//被剧群同意加到某个剧群
@end
