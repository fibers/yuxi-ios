//
//  SearchFriendsViewController.h
//  inface
//
//  Created by Mac on 15/4/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.


#import <UIKit/UIKit.h>
#import "FriendsSelectTableViewCell.h"
#import "MsgAddFriendReqAPI.h"
//#import "MyCenterViewController.h"
@interface SearchFriendsViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableDataFriends;

@property (nonatomic ,retain) UITextField * SearchText;
@property (strong,nonatomic)  NSString * friendId;
@property(assign,nonatomic)   BOOL        isAnimating;
@end
