//
//  GroupManageController.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendGroupModel.h"
@interface GroupManageController : BaseViewController

@property(assign,nonatomic) BOOL  isAnimating;
-(void)setContent:(FriendGroupModel *)fgModel;
@end
