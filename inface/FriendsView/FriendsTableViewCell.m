//
//  FriendsTableViewCell.m
//  inface
//
//  Created by Mac on 15/4/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "FriendsTableViewCell.h"

@implementation FriendsTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.UserNameLabel.textColor=BLACK_CONTENT_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
