//
//  GroupView.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
@protocol ManagerGroupDelegate <NSObject>


-(void)addGroup:(NSString*)groupName;//增加好友分组

@end
#import <UIKit/UIKit.h>
@interface GroupView : UIView<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *addGroupLabel;
@property(nonatomic,unsafe_unretained)id<ManagerGroupDelegate>delegate;
@end
