//
//  ModifyTableViewCell.h
//  inface
//
//  Created by appleone on 15/8/31.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModifyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *groupName;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end
