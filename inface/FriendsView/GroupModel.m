//
//  GroupModel.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GroupModel.h"
#import "FriendModel.h"
@implementation GroupModel
+(instancetype)shareInstance
{
    static GroupModel * model;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        model  = [[GroupModel alloc]init];
    });
    return model;
}

+ (NSDictionary *)objectClassInArray{
    return @{@"groupmembers" : [FriendModel class]};
}
@end
