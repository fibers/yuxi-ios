//
//  FriendsSelectTableViewCell.h
//  inface
//
//  Created by Mac on 15/5/25.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsSelectTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *UserImage;//头像
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UIButton *SelectButton;//选择按钮
@property (weak, nonatomic) IBOutlet UIImageView *vipWriterImage;

@end
