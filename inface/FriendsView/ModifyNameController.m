//
//  ModifyNameController.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ModifyNameController.h"
#import "ModifyNickNameView.h"
#import "SavePersonPortrait.h"
#import "FriendModel.h"
@interface ModifyNameController ()
@property(strong,nonatomic)ModifyNickNameView * modifyView;
@property(strong,nonatomic)FriendModel * f_model;

@end

@implementation ModifyNameController


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"修改备注名";
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"完成" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    // Do any additional setup after loading the view.
    
    _modifyView = [[ModifyNickNameView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [_modifyView setContent:_f_model];

    [self.view addSubview:_modifyView];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setFriendModel:(FriendModel *)fModel;
{
    _f_model = fModel;
}

-(void)returnBackl
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)returnText:(returnMarkName)block{
    self.returnTextBlock = block;
}

-(void)returnBackr
{
    if (_modifyView.nameField.text.length >12) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"备注名不能超过12个字" message:@"请修改备注名，重新提交" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (_modifyView.nameField.text == NULL || [_modifyView.nameField.text isEqual:nil]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"备注名不能为空" message:nil delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles: nil];
        [alert show];
    }else{
        
        NSDictionary * params = @{@"uid":USERID,@"personid":_f_model.userid,@"personnick":_modifyView.nameField.text,@"token":@"110"};
        [HttpTool postWithPath:@"ModifyUserName" params:params success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                
                DDUserEntity * entity = [[DDUserEntity alloc]initWithUserID:_f_model.userid name:_f_model.realname nick:_modifyView.nameField.text  avatar:_f_model.portrait userRole:100 userUpdated:100];
                [[DDUserModule shareInstance]addMaintanceUser:entity];
                
                if(self.returnTextBlock!=nil){
                    self.returnTextBlock(_modifyView.nameField.text);
                }
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"修改备注名成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alert show];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } failure:^(NSError *error) {
            
            
        }];
    }
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
