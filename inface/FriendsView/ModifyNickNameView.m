//
//  ModifyNickNameView.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ModifyNickNameView.h"
#import "FriendModel.h"

@implementation ModifyNickNameView


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setContent:(FriendModel *)model
{
    fModel = model;
    [_nameField setText:model.markname];
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self  = [super initWithFrame:frame];
    if (self) {
        _nameField = [[UITextField alloc]initWithFrame:CGRectMake(0, 14, ScreenWidth, 44)];
        [_nameField setTextColor:[UIColor colorWithHexString:@"#949494"]];
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 44)];
        _nameField.leftView = view;
        _nameField.leftViewMode = UITextFieldViewModeAlways;
        _nameField.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
        [_nameField setTextColor:[UIColor colorWithHexString:@"#949494"]];
        [_nameField setFont:[UIFont systemFontOfSize:15.0]];
        
        [self addSubview:_nameField];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}




@end
