//
//  NewFriendTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/6/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewFriendTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *UserImage;//头像
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UILabel *UserDetailLabel;//详情
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *refuseBtn;
- (IBAction)agreeSrnder:(id)sender;
- (IBAction)refuseSender:(id)sender;

@end
