//
//  NewExamineTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/6/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//剧家族按钮cell
#import <UIKit/UIKit.h>

@interface NewExamineTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *UserImage;//头像
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字

@property (weak, nonatomic) IBOutlet UIButton *CountButton;


@end
