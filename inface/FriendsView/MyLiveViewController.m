//
//  MyLiveViewController.m
//  inface
//
//  Created by huangzengsong on 15/7/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MyLiveViewController.h"
#import "GetGroupRoleName.h"
#import "ChattingModule.h"
#import "NSString+YXExtention.h"
@interface MyLiveViewController ()

@end

@implementation MyLiveViewController

+(instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    static MyLiveViewController *ashareInstance = nil;
    dispatch_once(&onceToken, ^{
        ashareInstance = [MyLiveViewController new];
    });
    return ashareInstance;
    
}

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=@"剧";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
//    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 12, 0, 12)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 12, 0, 12)];
    }

    self.tableView.separatorColor=XIAN_COLOR;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    
    [self getLocalMyStorys];//获取我的剧

}

#pragma mark 获取本地的剧
-(void)getLocalMyStorys
{
    self.tableData = [[GetAllFamilysAndStorys shareInstance]type:@"2"];
    if (self.tableData.count ==0) {
        [SVProgressHUD showSuccessWithStatus:@"您还没有自己的剧群哦！快去创建或者加入一个吧！" duration:2.0];
        
    }else{
        [self.tableView reloadData];
    }

}

#pragma mark 获取我的剧
-(void)getMyStorySuccess:(void(^)())success;
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary *params = @{@"uid":userid};
    
    [HttpTool postWithPath:@"GetMyStory" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            NSMutableArray *  arr   =[JSON objectForKey:@"storys"];
            //更新本地
            if (arr.count == 0) {
                return ;
            }
            [[GetAllFamilysAndStorys shareInstance]updateFamilysOrSto:arr typestr:@"2"];
            
            self.tableData = [[GetAllFamilysAndStorys shareInstance]type:@"2"];
            
            if (self.tableData.count==0) {
                [SVProgressHUD showSuccessWithStatus:@"您还没有自己的剧群哦！快去创建或者加入一个吧！" duration:2.0];
            }
            success(@"success");
            
            [self.tableView reloadData];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];
    
}

#pragma mark-获取我的剧列表
-(void)getMyStorys
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary *params = @{@"uid":userid};
    
    [HttpTool postWithPath:@"GetMyStory" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {

            NSMutableArray *  arr   =[JSON objectForKey:@"storys"];
            //更新本地
            if (arr.count == 0) {
                return ;
            }
            [[GetAllFamilysAndStorys shareInstance]updateFamilysOrSto:arr typestr:@"2"];
            
            self.tableData = [[GetAllFamilysAndStorys shareInstance]type:@"2"];
            
            if (self.tableData.count==0) {
                [SVProgressHUD showSuccessWithStatus:@"您还没有自己的剧群哦！快去创建或者加入一个吧！" duration:2.0];
            }
            
            [self.tableView reloadData];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];
    
}

#pragma mark 被剧群同意进剧,暂时可能用不到
-(void)agreeByStory:(NSString *)groupId;//被剧群同意加到某个剧群
{
    //重新获取一次我的剧
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary *params = @{@"uid":userid};
    
    [HttpTool postWithPath:@"GetMyStory" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            NSMutableArray *  arr   =[JSON objectForKey:@"storys"];
            //更新本地
            if (arr.count == 0) {
                return ;
            }
            [[GetAllFamilysAndStorys shareInstance]updateFamilysOrSto:arr typestr:@"2"];
            
            self.tableData = [JSON objectForKey:@"storys"];
            
            if (self.tableData.count==0) {
                [SVProgressHUD showSuccessWithStatus:@"您还没有自己的剧群哦！快去创建或者加入一个吧！" duration:2.0];
            }
            //剧比较特殊，如果是被添加到剧群，则不能发送消息，需要判断是否是审核群
            NSDictionary * dic = [[GetAllFamilysAndStorys shareInstance]getStoryGroup:groupId];
            NSString * isInStoryGroup = [dic objectForKey:@"isplay"];
            NSString * groupname = [dic objectForKey:@"title"];
            //需要根据群ID获取剧id
            NSString * storyId = [[GetAllFamilysAndStorys shareInstance]getStoryidByGroupid:groupId];
            //获取剧群的成员信息，保存到本地
            NSDictionary * params = @{@"uid":userid,@"storyid":storyId,@"groupid":groupId,@"token":@"110"};
            [HttpTool postWithPath:@"GetStoryGroupInfo" params:params success:^(id JSON) {
                if ([[JSON objectForKey:@"result"]integerValue]==1) {
                    
                    NSArray * membersArr = [[JSON objectForKey:@"groupinfo"]objectForKey:@"members"];
                    [[GetGroupRoleName shareInstance]saveFamily:groupId membersArr:membersArr];
                    if (![isInStoryGroup isEqualToString:@"1" ]) {
                        NSString *sessionid = [NSString stringWithFormat:@"group_%@",groupId];
                        SessionEntity * entity = [[SessionModule sharedInstance]getSessionById:sessionid];
                        entity = [[SessionEntity alloc]initWithSessionID:sessionid type:SessionTypeSessionTypeGroup];
                        NSString * msgContent = [[NSUserDefaults standardUserDefaults]objectForKey:@"nickname"];
                        NSString * msgtext = [NSString stringWithFormat:@"%@已经是剧群成员了",msgContent];
                        ChattingModule * module = [[ChattingModule alloc]init];
                        DDMessageContentType msgContentType = DDMessageTypeText;
                        entity.unReadMsgCount = 1;
                        module.sessionEntity = entity;
                        
                        DDMessageEntity * message = [DDMessageEntity makeMessage:msgtext Module:module MsgType:msgContentType isSendImage:NO isCurrentGroup:NO];
                        [[DDMessageSendManager instance]sendMessage:message isGroup:YES Session:entity completion:^(DDMessageEntity *message, NSError *error) {
                            
                            
                        } Error:^(NSError *error) {
                            
                            
                        }];

                    }
                }
                
            } failure:^(NSError *error) {
                
                
            }];


            [self.tableView reloadData];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSMutableArray * currentArr = [NSMutableArray array];
    for (int i = 0; i < self.tableData.count; i++) {
        NSArray * groups = [[self.tableData objectAtIndex:i]objectForKey:@"groups"];
        if (groups.count == 0 ) {
            NSString * str = [[groups objectAtIndex:0]objectForKey:@"isplay"];
            if (![str isEqualToString:@"3"]) {
                [currentArr addObject:[self.tableData objectAtIndex:i]];
            }
        }else{
            [currentArr addObject:[self.tableData objectAtIndex:i]];
        }
    }
    self.tableData = [currentArr mutableCopy];
    return self.tableData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *groups=[[self.tableData objectAtIndex:section]objectForKey:@"groups"];
    NSMutableArray * groupsArr = [NSMutableArray array];
    for (int i = 0;i<[groups count];i++) {
        NSDictionary * dic = [groups objectAtIndex:i];
        NSString * isplay = [dic objectForKey:@"isplay"];
        if (![isplay isEqualToString:@"3"]) {
            [groupsArr addObject:dic];
        }
    }
    return groupsArr.count;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"MyLiveTableViewCell";
    MyLiveTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"MyLiveTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSMutableArray *groups=[[self.tableData objectAtIndex:indexPath.section] objectForKey:@"groups"];
    NSString *title=[[groups objectAtIndex:indexPath.row] objectForKey:@"title"];
    
    cell.UserNameLabel.text=title;
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UINib *nib=[UINib nibWithNibName:@"MyLiveHeaderTableViewCell" bundle:nil];
    MyLiveHeaderTableViewCell *headerview=[[nib instantiateWithOwner:nil options:nil] firstObject];

    headerview.contentView.backgroundColor=COLOR(235, 113, 168, 0.7);
    headerview.CountButton.hidden = YES;
    NSString *title=[[self.tableData objectAtIndex:section] objectForKey:@"title"];
    NSString *portrait=[[self.tableData objectAtIndex:section] objectForKey:@"portrait"];
    headerview.UserNameLabel.text=title;
    headerview.UserNameLabel.textColor=[UIColor whiteColor];
    [headerview.UserImage sd_setImageWithURL:[NSURL URLWithString:[portrait stringOfW100ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
    
    headerview.UserImage.layer.cornerRadius = CGRectGetHeight(headerview.UserImage.bounds)/2;
    headerview.UserImage.clipsToBounds = YES;

    
    return headerview;
    
}





- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 62;
}





//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    NSMutableArray *groups=[[self.tableData objectAtIndex:indexPath.section] objectForKey:@"groups"];
    NSString *groupid=[[groups objectAtIndex:indexPath.row] objectForKey:@"groupid"];

    //是剧群
    TheatreInformationViewController * detailViewController = [[TheatreInformationViewController alloc] init];
    detailViewController.theatreGroupid =groupid;
    detailViewController.theatreid=[[self.tableData objectAtIndex:indexPath.section] objectForKey:@"id"];
    
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
