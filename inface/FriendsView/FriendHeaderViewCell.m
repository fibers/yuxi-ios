//
//  FriendHeaderViewCell.m
//  inface
//
//  Created by appleone on 15/9/8.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "FriendHeaderViewCell.h"

@implementation FriendHeaderViewCell

- (IBAction)managerGroup:(id)sender {
    if ([self.delegate respondsToSelector:@selector(manageGroup)]) {
        [self.delegate manageGroup];
    }
}

- (void)awakeFromNib {
    // Initialization code
//    self.headButtn.layer.cornerRadius = 2;
//    
//    self.layer.borderWidth = 1.0f;
//    
//    self.layer.borderColor = [UIColor colorWithHexString:@"#7fb0e9"].CGColor;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
