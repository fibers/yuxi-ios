//
//  InformationTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *UserImage;//头像
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UILabel *UserDetailLabel;//详情
@property (strong, nonatomic) IBOutlet UILabel *TimeLabel;//时间
@property (weak, nonatomic) IBOutlet UIButton *CountButton;

@property (weak, nonatomic) IBOutlet UIImageView *vipWriterImage;

-(void)setShowSession:(SessionEntity *)session;
- (void)setAvatar:(NSString*)avatar placeholderImage:(UIImage *)placeholder;
-(void)setCurrentSession:(SessionEntity*)session storyDic:(NSDictionary*)dic;//评论群聊天列表
-(void)setCurrentGroupsPortrait;//评论群单元格
@end
