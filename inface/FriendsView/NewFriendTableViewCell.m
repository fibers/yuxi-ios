//
//  NewFriendTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/6/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "NewFriendTableViewCell.h"

@implementation NewFriendTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.UserNameLabel.textColor=BLACK_CONTENT_COLOR;
    self.UserDetailLabel.textColor=BLUECOLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)agreeSrnder:(id)sender {
}

- (IBAction)refuseSender:(id)sender {
}
@end
