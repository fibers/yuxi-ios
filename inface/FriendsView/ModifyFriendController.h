//
//  ModifyFriendController.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendModel.h"
#import "GroupModel.h"
@class FriendGroupModel;
typedef void(^returnGroupModel)(GroupModel * groupModel,FriendGroupModel *fg_model);
@interface ModifyFriendController : BaseViewController
@property(assign,nonatomic)    BOOL   isAnimating;

-(void)setFriendModel:(FriendModel *)fModel groupModel:(GroupModel *)groupModel fgModel:(FriendGroupModel *)friendgModel;
-(void)returnGroupModel:(returnGroupModel)block;//
@end
