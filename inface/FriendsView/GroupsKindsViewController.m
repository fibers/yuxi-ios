//
//  GroupsKindsViewController.m
//  inface
//
//  Created by appleone on 15/6/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GroupsKindsViewController.h"
#import "MsgAddGroupConfirmAPI.h"
#import "DDAddMemberToGroupAPI.h"
#import "MsgAddFriendConfirmReqAPI.h"
#import "YXMyCenterViewController.h"
#import "LjjUISegmentedControl.h"
#import <ReactiveCocoa.h>
#import "IntroductionMasCell.h"
#import "AddMasterCell.h"
#import "AgreeMasterCell.h"
#import "MsgAddFriendReqAPI.h"
@interface GroupsKindsViewController ()<LjjUISegmentedControlDelegate,UIAlertViewDelegate>
@property(strong,nonatomic)NSMutableArray * masterApprentMessages;

@property(assign,nonatomic)NSInteger switching;

@property(strong,nonatomic)LjjUISegmentedControl * ljUISegment ;

@property(strong,nonatomic)NSString * addFriendID;

@property(assign,nonatomic)NSInteger  indexPath;

@property(assign,nonatomic)BOOL       fromPushApplication;//是否从推送页面进的
@end

@implementation GroupsKindsViewController

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

-(void)selectAddMasterState
{
    _fromPushApplication = YES;
}
- (void)viewDidLoad {

    [super viewDidLoad];
        // Do any additional setup after loading the view from its nib.

    //分段控件
    
    _ljUISegment = [[LjjUISegmentedControl alloc]initWithFrame:CGRectMake(77, 0, ScreenWidth-154, 44)];
    
    NSArray * ljArrary = [NSArray arrayWithObjects:@"系统消息",@"师徒申请", nil];
    
    [_ljUISegment AddSegumentArray:ljArrary];
    
    _ljUISegment.delegate = self;
    self.navigationItem.titleView = _ljUISegment;
//    self.navigationItem.title = @"系统消息";
    if (_fromPushApplication) {
        [_ljUISegment selectTheSegument:1];
        
        [self uisegumentSelectionChange:1];
    }
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getFamilyAndStoryMessagesFromDB) name:@"NEWADDMESSAGE" object:nil];
        //右上角按钮
//        UIBarButtonItem *barback2 = [[UIBarButtonItem alloc] initWithTitle:@"添加" style:UIBarButtonItemStylePlain target:self action:@selector(returnBackr)];
//        barback2.tintColor=BLUECOLOR;
//        self.navigationItem.rightBarButtonItem=barback2;
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 12, 0, 12)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 12, 0, 12)];
    }

    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
        
    self.tableView.tableFooterView=[[UIView alloc]init];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.masterApprentMessages = [NSMutableArray arrayWithCapacity:10];
    [super viewWillAppear:animated];
    [self getFamilyAndStoryMessagesFromDB];
    //获取我的师徒列表
    NSDictionary * params = @{@"uid":USERID,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetMasterAndApprenticeMessage" params:params success:^(id JSON) {
        NSString * result = [JSON objectForKey:@"result"];
        
        if ([result isEqualToString:@"1"]) {
            //获取师徒请求列表
           self.masterApprentMessages = [NSMutableArray arrayWithArray:[JSON objectForKey:@"MasterAndApprenticeMessage"]];
            if (self.masterApprentMessages.count ==0) {
                [SVProgressHUD showSuccessWithStatus:@"没有更多内容了" duration:3.0];
                return ;
            }
            [self.tableView reloadData];
        }
        
    } failure:^(NSError *error) {
        UIAlertView * alert  = [[UIAlertView alloc]initWithTitle:@"获取师徒请求信息失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        
    }];
}


#pragma mark LjUISegmentControll
-(void)uisegumentSelectionChange:(NSInteger)selection
{
    switch (selection) {
        case 0:
            _switching =0;
            [self.tableView reloadData];
            break;
        case 1:
            _switching = 1;
            [self.tableView reloadData];
        default:
            break;
    }

}

#pragma mark 从数据库拿家族或者剧的添加消息
-(void)getFamilyAndStoryMessagesFromDB
{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    [[DDDatabaseUtil instance]getAddfriendstoid:userid  success:^(id JSON) {
        
        self.tableData = JSON;
        NSSortDescriptor * sortDes = [[NSSortDescriptor alloc]initWithKey:@"time" ascending:NO];
        [self.tableData sortUsingDescriptors:[NSArray arrayWithObject:sortDes] ]    ;
        if (self.tableData.count == 0) {
            [SVProgressHUD showSuccessWithStatus:@"没有更多内容了" duration:2.0];
            return ;

        }
         [self.tableView reloadData];

    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (_switching) {
        case 0:
            return [self.tableData count];
            break;
        case 1:
            return _masterApprentMessages.count ;
            break;
        default:
            break;
    }
    return [self.tableData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_switching==0) {
        
    static NSString *identifier=@"NewFriendTableViewCell";
    NewFriendTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"NewFriendTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    NSDictionary * personDic = [self.tableData objectAtIndex:indexPath.row];
    NSString * type = [personDic objectForKey:@"type"];
    NSString * addMessage = [personDic objectForKey:@"addmessage"];
    if ([type isEqualToString:@"1"]) {
        //好友请求
        if (addMessage && ![addMessage isEqual:nil]) {
            cell.UserNameLabel.text = [personDic objectForKey:@"detailtext"];
            cell.UserDetailLabel.text= addMessage;
        }else{
            cell.UserNameLabel.text = [personDic objectForKey:@"name"];
            cell.UserDetailLabel.text = [personDic objectForKey:@"detailtext"];
        }
    }else{
        cell.UserNameLabel.text = [personDic objectForKey:@"name"];
        cell.UserDetailLabel.text = [personDic objectForKey:@"detailtext"];
    }

    //    NSString * type = [personDic objectForKey:@"type"];
    NSString * haschange = [personDic objectForKey:@"haschange"];
    if ([haschange isEqualToString:@"0"]) {
        cell.agreeBtn.hidden = NO;
        cell.refuseBtn.hidden = NO;

        cell.agreeBtn.userInteractionEnabled = YES;
        [cell.agreeBtn setTitle:@"同意" forState:UIControlStateNormal];
        //        cell.SelectButton.backgroundColor = [UIColor colorWithRed:184 green:227 blue:247 alpha:1];
//        [cell.SelectButton setBackgroundImage:[UIImage imageNamed:@"agreeAdd"] forState:UIControlStateNormal];
        [cell.agreeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.agreeBtn setBackgroundColor:[UIColor colorWithHexString:@"#eb71a8"]];
        cell.agreeBtn.layer.cornerRadius = 4.0;
        cell.agreeBtn.tag = 100000 + indexPath.row;
        cell.refuseBtn.tag = 100000 + indexPath.row;
        [cell.refuseBtn setBackgroundColor:[UIColor colorWithHexString:@"#949494"]];
        cell.refuseBtn.userInteractionEnabled = YES;
        [cell.refuseBtn setTitle:@"拒绝" forState:UIControlStateNormal];
        cell.refuseBtn.layer.borderWidth = 0.8;
        cell.refuseBtn.layer.cornerRadius = 4.0;
        cell.refuseBtn.layer.borderColor = [UIColor colorWithHexString:@"#949494"].CGColor ;
        [cell.refuseBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.refuseBtn addTarget:self action:@selector(refuseAddFriend:) forControlEvents:UIControlEventTouchUpInside];

        [cell.agreeBtn addTarget:self action:@selector(agreeAddFriend:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        if ([haschange isEqualToString:@"1"]) {
        cell.agreeBtn.userInteractionEnabled = NO;
        cell.agreeBtn.hidden = YES;
//        [cell.agreeBtn setTitle:@"已同意" forState:UIControlStateNormal];
        [cell.agreeBtn setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
            [cell.agreeBtn setBackgroundColor:[UIColor whiteColor]];
            cell.refuseBtn.hidden = NO;
            [cell.refuseBtn setTitle:@"已同意" forState:UIControlStateNormal];
            cell.refuseBtn.layer.borderWidth = 0.8;
            cell.refuseBtn.layer.cornerRadius = 4.0;
            cell.refuseBtn.layer.borderColor = [UIColor colorWithHexString:@"#949494"].CGColor ;
            [cell.refuseBtn setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
            [cell.refuseBtn setBackgroundColor:[UIColor whiteColor]];

            cell.refuseBtn.userInteractionEnabled = NO;
        }
       if([haschange isEqualToString:@"2"]){
            cell.agreeBtn.userInteractionEnabled = NO;
            cell.agreeBtn.hidden = YES;
            cell.refuseBtn.hidden = NO;
            cell.agreeBtn.userInteractionEnabled = NO;
            [cell.refuseBtn setTitle:@"已拒绝" forState:UIControlStateNormal];
            CGRect bounds = cell.refuseBtn.frame;
            bounds.size.width = 46;
           cell.refuseBtn.layer.borderWidth = 0.8;
           cell.refuseBtn.layer.cornerRadius = 4.0;
           cell.refuseBtn.layer.borderColor = [UIColor colorWithHexString:@"#949494"].CGColor ;
            cell.refuseBtn.frame = bounds;
           [cell.refuseBtn setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
           [cell.refuseBtn setBackgroundColor:[UIColor whiteColor]];
        }
        if ([haschange isEqualToString:@"3"]) {
            cell.agreeBtn.hidden = YES;
            cell.refuseBtn.hidden = YES;
        }
    }
    
    NSString * portrait = [personDic objectForKey:@"portrait"];
    if ([portrait isEqualToString:@"100"]|| [portrait isEmpty]) {
        [cell.UserImage setImage:[UIImage imageNamed:@"zs_touxianglan"] ];
        
    }else{
        //如果potrit为100 就用默认的头像
        NSURL * url = [NSURL URLWithString:portrait];
        [cell.UserImage sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];

    }
    cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
    cell.UserImage.clipsToBounds=YES;
    cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;
    
    return cell;
    }else{
        //师徒拜师请求
        NSDictionary * addIntro = _masterApprentMessages[indexPath.row];
        
        NSInteger   addIntroType = [[_masterApprentMessages[indexPath.row]objectForKey:@"type"]integerValue];
        
        NSURL * url = [NSURL URLWithString:[addIntro objectForKey:@"portrait"]];
        
        NSString * detailText = [addIntro objectForKey:@"message"];
        
        if ( addIntroType == 1 || addIntroType==2 ||addIntroType == 3 ||addIntroType ==4) {
            //没有处理请求的一类
            IntroductionMasCell * introducCell = [tableView dequeueReusableCellWithIdentifier:@"IntroductionMasCell" ];
            if (!introducCell) {
                introducCell = [[NSBundle mainBundle]loadNibNamed:@"IntroductionMasCell" owner:nil options:nil].firstObject;
                
            }
            [introducCell.potraitButton sd_setBackgroundImageWithURL:url forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
            introducCell.potraitButton.layer.cornerRadius = 21.0f;
            
            introducCell.potraitButton.clipsToBounds = YES;
            introducCell.detailTextLal.text = detailText;
            return introducCell;

        }else{
            
            if (addIntroType == 0) {
                //处理徒弟的申请
                AgreeMasterCell * agreeCell = [tableView dequeueReusableCellWithIdentifier:@"AgreeMasterCell"];
                
                if (!agreeCell) {
                    agreeCell = [[NSBundle mainBundle]loadNibNamed:@"AgreeMasterCell" owner:nil options:nil].firstObject;
                }
                
                [agreeCell.portraitButton sd_setBackgroundImageWithURL:url forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
                agreeCell.portraitButton.layer.cornerRadius = 21.0f;
                agreeCell.portraitButton.clipsToBounds = YES;
                agreeCell.detailTextLal.text = detailText;
                agreeCell.AgreeButton.tag = indexPath.row;
                agreeCell.rejectButton.tag = indexPath.row;
                [agreeCell.AgreeButton addTarget:self action:@selector(agreeAddMater:) forControlEvents:UIControlEventTouchUpInside];
                [agreeCell.rejectButton addTarget:self action:@selector(rejectAddMater:) forControlEvents:UIControlEventTouchUpInside];
                return agreeCell;
                
                
            }else{
                //拜对方为师或者加好友
                AddMasterCell * addMasterCell = [tableView dequeueReusableCellWithIdentifier:@"AddMasterCell"];
                if (!addMasterCell) {
                    addMasterCell = [[NSBundle mainBundle]loadNibNamed:@"AddMasterCell" owner:nil options:nil].firstObject;
                }
                [addMasterCell.portraitButton sd_setBackgroundImageWithURL:url forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
                addMasterCell.portraitButton.layer.cornerRadius = 21.0f;
                addMasterCell.portraitButton.clipsToBounds = YES;
                addMasterCell.detailTextLal.text = detailText;
                addMasterCell.addFriendButton.tag = indexPath.row;
                addMasterCell.addMasterButton.tag = indexPath.row;
                [addMasterCell.addFriendButton addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
                [addMasterCell.addMasterButton addTarget:self action:@selector(applyMaster:) forControlEvents:UIControlEventTouchUpInside];
                return addMasterCell;

            }
        }
        
        
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 70;
    
}

#pragma mark tableviewDelegate
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_switching == 1) {
        return NO;
    }
    return YES;
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}



#pragma mark 滑动表格删除聊天记录
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //删除数据库的对应数据
    NSDictionary * dic = [self.tableData objectAtIndex:indexPath.row];
    [self.tableData removeObjectAtIndex:indexPath.row];

    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];

    NSString * fromid = [dic objectForKey:@"fromid"];
    NSString * toid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSString * groupid = [dic objectForKey:@"groupid"];
    NSString * grouptype = [dic objectForKey:@"grouptype"];
    [[DDDatabaseUtil instance]clearUpAddMessages:fromid toid:toid groupid:groupid type:grouptype];

    //发送TCP请求  do any option.....
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_switching == 0) {
    
    NSDictionary *friendArr;

    friendArr = [self.tableData objectAtIndex:indexPath.row];
 
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSString * FriendID = [friendArr objectForKey:@"fromid"];//对方的id
    NSString * groupid = [friendArr objectForKey:@"groupid"];
    NSString * applyType = [friendArr objectForKey:@"type"];
    NSString * grouptype = [friendArr objectForKey:@"grouptype"];
    NSString * changeType = friendArr[@"haschange"];
    //1 代表已经同意，2是已经拒绝  3是被对移除
    if ([applyType isEqualToString:@"5"]||[applyType isEqualToString:@"1"]) {
        //此时是好友的添加请求或者是申请进群
        
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if ([userid isEqualToString:FriendID]) {
            //TODO(Xc.)这里注释了。
//            MyCenterViewController * detailViewController = [[MyCenterViewController alloc]init];
//            detailViewController.fromClass=@"ViewPersonalData";
//            detailViewController.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:detailViewController animated:YES];
            
        } else {
            
//            FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
//            detailViewController.FriendID = FriendID;
//            [self.navigationController pushViewController:detailViewController animated:YES];
            
//            FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
//            detailViewController.FriendID = FriendID;
//            detailViewController.ownerGroupID = groupid;
//            detailViewController.isAddFriendOrGroup = YES;
//            detailViewController.addType = applyType;
//            detailViewController.grouptype = grouptype;
//            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        
        YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
        [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
        detailViewController.hidesBottomBarWhenPushed = YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];

        //取消选中的行
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }else{
        //根据群id 获取家族或者家族的信息
        NSDictionary * dic = @{@"uid":userid,@"groupid":groupid,@"token":@"110"};
        [HttpTool postWithPath:@"GetGroupDetail" params:dic success:^(id JSON) {
            NSDictionary * dic = [JSON objectForKey:@"groupdetails"];
            NSString * type = [dic objectForKey:@"type"];
            if ([type isEqualToString:@"1"]) {
                //1家族 2剧
                //是家族的群
                FamilyInformationViewController * detailViewController = [[FamilyInformationViewController alloc] init];
                //                long  fid = [[RuntimeStatus instance]changeIDToOriginal:self.module.sessionEntity.sessionID];
                //                NSString * familyid = [NSString stringWithFormat:@"%lu",fid];
                NSString * familyid = [dic objectForKey:@"refid"];
                detailViewController.familyid = familyid;
                detailViewController.fromid = FriendID;
                detailViewController.applyType = applyType;
                detailViewController.isApplyToJoin = YES;
                if ([changeType isEqualToString:@"3"]) {
                    //被管理员移除群
                    detailViewController.isApplyToJoin = NO;
                }
                detailViewController.hidesBottomBarWhenPushed=YES;
                if (detailViewController.isAnimating) {
                    return;
                }
                detailViewController.isAnimating = YES;
                [self.navigationController pushViewController:detailViewController animated:YES];
            } else {
                //1家族 2剧
                //是剧群
                TheatreInformationViewController * detailViewController = [[TheatreInformationViewController alloc] init];
                //                long  fid = [[RuntimeStatus instance]changeIDToOriginal:self.module.sessionEntity.sessionID];
                //                NSString * theatreGroupid = [NSString stringWithFormat:@"%lu",fid];
                NSString * therterid = [dic objectForKey:@"refid"];
                detailViewController.theatreGroupid = groupid;
                detailViewController.theatreid=therterid;
                detailViewController.applyType = applyType;
                detailViewController.fromid = FriendID;
                
                detailViewController.isApplyToJoin = YES;
                if ([changeType isEqualToString:@"3"]) {
                    detailViewController.isApplyToJoin = NO;
                }
                detailViewController.hidesBottomBarWhenPushed=YES;
                if (detailViewController.isAnimating) {
                    return;
                }
                detailViewController.isAnimating = YES;
                [self.navigationController pushViewController:detailViewController animated:YES];
            }
            
            
        } failure:^(NSError *error) {
            
            
        }];
    }
    }else{
         NSString * FriendID = [[_masterApprentMessages objectAtIndex:indexPath.row]objectForKey:@"uid"];

        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if ([userid isEqualToString:FriendID]) {
            //TODO(Xc.)这里注释了。
            //            MyCenterViewController * detailViewController = [[MyCenterViewController alloc]init];
            //            detailViewController.fromClass=@"ViewPersonalData";
            //            detailViewController.hidesBottomBarWhenPushed = YES;
            //            [self.navigationController pushViewController:detailViewController animated:YES];
            
        } else {
            
            //            FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
            //            detailViewController.FriendID = FriendID;
            //            [self.navigationController pushViewController:detailViewController animated:YES];
            
            //            FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
            //            detailViewController.FriendID = FriendID;
            //            detailViewController.ownerGroupID = groupid;
            //            detailViewController.isAddFriendOrGroup = YES;
            //            detailViewController.addType = applyType;
            //            detailViewController.grouptype = grouptype;
            //            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        
        YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
        [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
        detailViewController.hidesBottomBarWhenPushed = YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
        //取消选中的行
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
}



-(void)refuseAddFriend:(UIButton*)sendor
{
    NewFriendTableViewCell * cusCell;
    
    if (IOSSystemVersion >= 7.0 && IOSSystemVersion < 8.0) {
        cusCell = (NewFriendTableViewCell*)sendor.superview.superview.superview;
    }else{
        
        cusCell =(NewFriendTableViewCell*)sendor.superview.superview;
    }
    [cusCell.refuseBtn setTitle:@"已拒绝" forState:UIControlStateNormal];
    cusCell.refuseBtn.userInteractionEnabled = NO;
    cusCell.agreeBtn.hidden = YES;
    cusCell.userInteractionEnabled = NO;
    [cusCell.refuseBtn setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
    [cusCell.refuseBtn setBackgroundColor:[UIColor whiteColor]];

    //    [cusCell.refuseBtn setBackgroundImage:nil forState:UIControlStateNormal];
    
    //    NSIndexPath * indexPath = [self.tableView indexPathForCell:cusCell];
    NSDictionary * persondic = [self.tableData objectAtIndex:sendor.tag-100000];
    
    //需要判断是好友请求还是群添加请求
    NSString * grouptype = [persondic objectForKey:@"grouptype"];
    
    //家族的添加请求
    NSString * addType = [persondic objectForKey:@"type"];
    NSString * sessionid = [persondic objectForKey:@"fromid"];
    NSString * groupid = [persondic objectForKey:@"groupid"];
    [[DDDatabaseUtil instance]deleteAddFriendsMessageFromid:sessionid groupid:groupid friendsType:@"5" result:@"-2" grouptype:grouptype haschange:@"2"];
    if ([addType isEqualToString:@"5"]) {
        //群被申请，处理对方的添加请求
        MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
        NSArray * arrary = @[@(0),sessionid,groupid,@(0),@(SessionTypeSessionTypeSingle),@(0)];
        [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
            
        }];
        
        
    }else{
        if ([addType isEqualToString:@"1"]) {
            //好友添加请求
            NSIndexPath * indexPath = [self.tableView indexPathForCell:cusCell];
            
            NSDictionary * persondic = [self.tableData objectAtIndex:indexPath.row];
            //需要判断是好友请求还是群添加请求
            
            MsgAddFriendConfirmReqAPI * addFriend = [[MsgAddFriendConfirmReqAPI alloc]init];
            NSMutableString * str = [NSMutableString stringWithString:@"user_"];
            NSString * friendid = [persondic objectForKey:@"fromid"];
            [str appendString:friendid];
            [addFriend requestWithObject:@[str,@(0),@(SessionTypeSessionTypeSingle),@(0)] Completion:^(id response, NSError *error) {
                
            }];
            
            
#pragma mark 删除数据库对应的数据
            [[DDDatabaseUtil instance]deleteAddFriendsMessageFromid:friendid groupid:@"-2" friendsType:@"1" result:@"-2" grouptype:@"-2" haschange:@"2"];
            
            
        }else{
            //被群添加，同意进群
            
            MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
            NSArray * arrary = @[@(0),sessionid,groupid,@(0),@(SessionTypeSessionTypeGroup),@(0)];
            [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
                
            }];
#pragma mark 同意进群，将我的家族列表刷新
            
            [[DDDatabaseUtil instance]deleteAddFriendsMessageFromid:sessionid groupid:groupid friendsType:@"3" result:@"-2" grouptype:grouptype haschange:@"2"];
            
        }
    }
    
    [self getFamilyAndStoryMessagesFromDB];

}

#pragma mark 拜师请求
-(void)applyMaster:(UIButton *)sender
{
    NSDictionary * personInfo = _masterApprentMessages[sender.tag];
    
    NSString * masterID = [personInfo objectForKey:@"uid"];
    
    NSDictionary * params = @{@"uid":USERID,@"masterId":masterID,@"token":@"110"};
    
    [HttpTool postWithPath:@"RequireAddMaster" params:params success:^(id JSON) {
        
        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles: nil];
        [alert show];
        
        [_masterApprentMessages removeObjectAtIndex:sender.tag];
        
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        
        
    }];
}

#pragma mark 同意拜师请求
-(void)agreeAddMater:(UIButton *)sender
{
    NSDictionary * personInfo = _masterApprentMessages[sender.tag];
    
    NSString * masterID = [personInfo objectForKey:@"uid"];
    
    NSDictionary * params = @{@"uid":USERID,@"apprenticeId":masterID,@"token":@"110"};
    
    [HttpTool postWithPath:@"AgreeAddMaster" params:params success:^(id JSON) {
            //已经成功同意
            UIAlertView * alrt = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
            [alrt show];
        [_masterApprentMessages removeObjectAtIndex:sender.tag];
        
        [self.tableView reloadData];
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            NSString * senderId = [NSString stringWithFormat:@"user_%@",masterID];
            NSString * msgContent = @"我同意了你的拜师请求";
            SessionEntity * session = [[SessionModule sharedInstance]getSessionById:senderId];
            if (!session) {
                session  = [[SessionEntity alloc]initWithSessionID:senderId type:SessionTypeSessionTypeSingle];
            }
            
            session.unReadMsgCount = 1;
            
            ChattingModule * module = [[ChattingModule alloc]init];
            module.sessionEntity = session;
            DDMessageContentType msgContentType = DDMessageTypeText;
            DDMessageEntity * message = [DDMessageEntity makeMessage:msgContent Module:module MsgType:msgContentType isSendImage:NO isCurrentGroup:NO];
            
            [[DDMessageSendManager instance]sendMessage:message isGroup:NO Session:session completion:^(DDMessageEntity *message, NSError *error) {
                
                
            } Error:^(NSError *error) {
                
                
            }];

            [[NSNotificationCenter defaultCenter]postNotificationName:@"GETFRIENDLIST" object:personInfo];

        }
        
    } failure:^(NSError *error) {
        
        
    }];
}


#pragma mark 拒绝拜师请求
-(void)rejectAddMater:(UIButton*)sender
{
    NSDictionary * personInfo = _masterApprentMessages[sender.tag];
    
    NSString * masterID = [personInfo objectForKey:@"uid"];
    
    NSDictionary * params = @{@"uid":USERID,@"apprenticeId":masterID,@"token":@"110"};
    
    [HttpTool postWithPath:@"RejectAddMaster" params:params success:^(id JSON) {
        //已经成功同意
        UIAlertView * alrt = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
        [alrt show];
        //更新UI
        [_masterApprentMessages removeObjectAtIndex:sender.tag];
        
        [self.tableView reloadData];

        
    } failure:^(NSError *error) {
        
        
    }];

}

#pragma mark 添加好友
-(void)addFriend:(UIButton*)sender
{
    _addFriendID = [NSString stringWithFormat:@"user_%@",[_masterApprentMessages[sender.tag]objectForKey:@"uid"]] ;
    
    _indexPath = sender.tag;
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"验证消息" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"发送", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = @"请输入20字以内的验证信息";
    alert.tag = 20000;
    
    [alert show];
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 20000) {
        MsgAddFriendReqAPI * addFriendReq = [[MsgAddFriendReqAPI alloc]init];
        if (buttonIndex == 1 && [alertView textFieldAtIndex:0].text.length <= 20) {
            
            NSString * addMessage = @"";
            
            if ([alertView textFieldAtIndex:0].text != NULL && ![[alertView textFieldAtIndex:0].text isEqual:nil]) {
                addMessage= [alertView textFieldAtIndex:0].text;
            }
            [addFriendReq requestWithObject:@[_addFriendID,@(0),@(SessionTypeSessionTypeSingle),addMessage] Completion:^(id response, NSError *error) {
                
                
            }];
            //刷新UI，处理的消息就消失
            [_masterApprentMessages removeObjectAtIndex:_indexPath];
            
            [self.tableView reloadData];
        }
        
    }
}

#pragma  mark 同意添加好友
-(void)agreeAddFriend:(UIButton *)sendor
{
    NewFriendTableViewCell * cusCell;
    
    if (IOSSystemVersion >= 7.0 && IOSSystemVersion < 8.0) {
        cusCell = (NewFriendTableViewCell*)sendor.superview.superview.superview;
    }else{
        
        cusCell =(NewFriendTableViewCell*)sendor.superview.superview;
    }
    

    NSDictionary * persondic = [self.tableData objectAtIndex:sendor.tag-100000];

    //需要判断是好友请求还是群添加请求
    NSString * grouptype = [persondic objectForKey:@"grouptype"];

        //家族的添加请求
        NSString * addType = [persondic objectForKey:@"type"];
        NSString * sessionid = [persondic objectForKey:@"fromid"];
        NSString * groupid = [persondic objectForKey:@"groupid"];
//        [[DDDatabaseUtil instance]deleteAddFriendsMessageFromid:sessionid groupid:groupid friendsType:@"5" result:@"-2" grouptype:grouptype haschange:@"1"];
        if ([addType isEqualToString:@"5"]) {
            //群被申请，处理对方的添加请求

            
            DDAddMemberToGroupAPI * addGroupMember = [[DDAddMemberToGroupAPI alloc]init];
            NSMutableString * prestr = [NSMutableString stringWithString:@"group_"];
            [prestr appendString:groupid];
            //                [prestr appendString:@"387"];
            NSMutableString * preuser = [NSMutableString stringWithString:@"user_"];
            [preuser appendString:sessionid];
            //                [preuser appendString:@"50"];
            NSArray * arrarylist = @[preuser];
            NSArray * object = @[prestr,arrarylist];
            [addGroupMember requestWithObject:object Completion:^(id response, NSError *error) {

                if (error) {
                    //同意失败
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"数据请求失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
                    [alert show];
                    [[DDTcpClientManager instance]reconnect];
                    return ;
                }
                cusCell.agreeBtn.hidden = YES;
                cusCell.agreeBtn.userInteractionEnabled = NO;
                [cusCell.agreeBtn setBackgroundImage:nil forState:UIControlStateNormal];
                [cusCell.refuseBtn setTitle:@"已同意" forState:UIControlStateNormal];
                cusCell.refuseBtn.userInteractionEnabled = NO;
                [cusCell.refuseBtn setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
                [cusCell.refuseBtn setBackgroundColor:[UIColor whiteColor]];
                MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
                NSArray * arrary = @[@(0),sessionid,groupid,@(0),@(SessionTypeSessionTypeSingle),@(1)];
                [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
                    
                }];
                [[DDDatabaseUtil instance]deleteAddFriendsMessageFromid:sessionid groupid:groupid friendsType:@"5" result:@"-2" grouptype:grouptype haschange:@"1"];
                return;

            }];
            
#pragma mark 以自己的身份在群里发一条消息
        }else{
            if ([addType isEqualToString:@"1"]) {
                //好友添加请求
                NSIndexPath * indexPath = [self.tableView indexPathForCell:cusCell];
                
                NSDictionary * persondic = [self.tableData objectAtIndex:indexPath.row];
                //需要判断是好友请求还是群添加请求
   
                    MsgAddFriendConfirmReqAPI * addFriend = [[MsgAddFriendConfirmReqAPI alloc]init];
                    NSMutableString * str = [NSMutableString stringWithString:@"user_"];
                    NSString * friendid = [persondic objectForKey:@"fromid"];
                    [str appendString:friendid];
                    [addFriend requestWithObject:@[str,@(0),@(SessionTypeSessionTypeSingle),@(1)] Completion:^(id response, NSError *error) {
                        if (error) {
                            //同意失败
                            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"数据请求失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
                            [alert show];
                            [[DDTcpClientManager instance]reconnect];
                            return ;
                        }
                
                        
                    }];
                cusCell.agreeBtn.hidden = YES;
                cusCell.agreeBtn.userInteractionEnabled = NO;
                [cusCell.agreeBtn setBackgroundImage:nil forState:UIControlStateNormal];
                [cusCell.refuseBtn setBackgroundColor:[UIColor whiteColor]];

                [cusCell.refuseBtn setTitle:@"已同意" forState:UIControlStateNormal];
                cusCell.refuseBtn.userInteractionEnabled = NO;
                [cusCell.refuseBtn setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
                [[DDDatabaseUtil instance]deleteAddFriendsMessageFromid:friendid groupid:@"-2" friendsType:@"1" result:@"-2" grouptype:@"-2" haschange:@"1"];
                [[NSNotificationCenter defaultCenter]postNotificationName:AGREE_ADD_FRIEND object:persondic ];
                
#pragma mark 将处理的消息从列表删除
                
#pragma mark 删除数据库对应的数据
                
 
            }else{
            //被群添加，同意进群
            NSArray * groupUsers = @[[RuntimeStatus instance].user.objID];
            NSMutableString * prestr = [NSMutableString stringWithString:@"group_"];
            [prestr appendString:groupid];
            DDAddMemberToGroupAPI * addGroupMember = [[DDAddMemberToGroupAPI alloc]init];
            NSArray * object = @[prestr,groupUsers];
            __block NSString * msgContent ;
            NSString * nickname = [[NSUserDefaults standardUserDefaults]objectForKey:@"nickname"];
            
            [addGroupMember requestWithObject:object Completion:^(id response, NSError *error) {
                if (error) {
                    //同意失败
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"数据请求失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
                    [alert show];
                    [[DDTcpClientManager instance]reconnect];
                    return ;
                }
                cusCell.agreeBtn.hidden = YES;
                cusCell.agreeBtn.userInteractionEnabled = NO;
                [cusCell.agreeBtn setBackgroundImage:nil forState:UIControlStateNormal];
                [cusCell.refuseBtn setTitle:@"已同意" forState:UIControlStateNormal];
                cusCell.refuseBtn.userInteractionEnabled = NO;
                [cusCell.refuseBtn setTitleColor:GRAY_COLOR forState:UIControlStateNormal];
                [cusCell.refuseBtn setBackgroundColor:[UIColor whiteColor]];

                if ([grouptype isEqualToString:@"1"]) {
                    //重新获取我的家族列表
                    [[MyFamilyViewController shareInstance]agreeByFamilyLeader:groupid];
                    msgContent = [NSString stringWithFormat:@"%@已经是家族成员了",nickname];
                }else{
                    [[MyLiveViewController shareInstance]agreeByStory:groupid];
                    msgContent = [NSString stringWithFormat:@"%@已经是剧成员了",nickname];

                }
                MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
                NSArray * arrary = @[@(0),sessionid,groupid,@(0),@(SessionTypeSessionTypeGroup),@(1)];
                [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
                    
                }];
      #pragma mark 同意进群，将我的家族列表刷新
                
                [[DDDatabaseUtil instance]deleteAddFriendsMessageFromid:sessionid groupid:groupid friendsType:@"3" result:@"-2" grouptype:grouptype haschange:@"1"];
                [self getFamilyAndStoryMessagesFromDB];

                
            }];
        }
        }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
