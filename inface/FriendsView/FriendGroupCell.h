//
//  FriendGroupCell.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
@protocol DeleteGroupDelegate <NSObject>

-(void)deleteGroup:(NSString*)groupid indexPath:(NSIndexPath*)index;//删除好友分组

@end
#import <UIKit/UIKit.h>
#import "GroupModel.h"
@interface FriendGroupCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property(nonatomic,strong) NSIndexPath * index;
@property(unsafe_unretained,nonatomic)id<DeleteGroupDelegate>delegate;
-(void)setContent:(GroupModel *)groupModel indexPath:(NSIndexPath*)index;//设置单元的内容
@end
