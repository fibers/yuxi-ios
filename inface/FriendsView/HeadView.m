//
//  HeadView.m
//  QQ好友列表
//
//  Created by TianGe-ios on 14-8-21.
//  Copyright (c) 2014年 TianGe-ios. All rights reserved.
//

// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com

#import "HeadView.h"
#import "GroupModel.h"

@interface HeadView()
{
    UIButton *_bgButton;
    UILabel *_numLabel;
    UILabel *_countLabel;
    UILongPressGestureRecognizer * longPress;
}
@end

@implementation HeadView

+ (instancetype)headViewWithTableView:(UITableView *)tableView
{
    static NSString *headIdentifier = @"header";
    
    HeadView *headView = [tableView dequeueReusableCellWithIdentifier:headIdentifier];
    if (headView == nil) {
        headView = [[HeadView alloc] initWithReuseIdentifier:headIdentifier];
    }
    
    return headView;
}

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        UIButton *bgButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [bgButton setBackgroundImage:[UIImage imageNamed:@"buddy_header_bg"] forState:UIControlStateNormal];
        [bgButton setBackgroundImage:[UIImage imageNamed:@"buddy_header_bg_highlighted"] forState:UIControlStateHighlighted];
        [bgButton setImage:[UIImage imageNamed:@"headUp"] forState:UIControlStateNormal];
        [bgButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [bgButton setBackgroundColor:[UIColor whiteColor]];
        bgButton.imageView.contentMode = UIViewContentModeCenter;
        bgButton.imageView.clipsToBounds = NO;
        bgButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        bgButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        bgButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [bgButton addTarget:self action:@selector(headBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:bgButton];
        _bgButton = bgButton;
        self.backgroundColor = [UIColor whiteColor];
//        UILabel *numLabel = [[UILabel alloc] init];
//        numLabel.textAlignment = NSTextAlignmentRight;
//        [self addSubview:numLabel];
//        _numLabel = numLabel;
        
        _countLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 40, 10, 40, 20)];
        _countLabel.textAlignment = NSTextAlignmentLeft;
        [_countLabel setTextColor:[UIColor colorWithHexString:@"#949494"]];
        [_countLabel setFont:[UIFont systemFontOfSize:12.0]];
        [self addSubview:_countLabel];
        
        _numLabel = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, 160, 20)];
        _numLabel.textAlignment = NSTextAlignmentLeft;
        _numLabel.textColor = [UIColor colorWithHexString:@"#4a4a4a"];
        _numLabel.font = [UIFont systemFontOfSize:17.0];
        [self  addSubview:_numLabel];
        
        longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)];
        longPress.minimumPressDuration = 1.0;
        [self addGestureRecognizer:longPress];
    }
    return self;
}

- (void)headBtnClick
{
    _groupModel.isOpened = !_groupModel.isOpened;
    if ([_delegate respondsToSelector:@selector(clickHeadView)]) {
        [_delegate clickHeadView];
    }
}


#pragma mark  longPress
-(void)longPress:(UILongPressGestureRecognizer*)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan) {
        if ([_delegate respondsToSelector:@selector(manageGroup:)]) {
            [_delegate manageGroup:longPress];
        }
//        CGPoint locationPoint = [longPress locationInView:self];
//        UIMenuController  * menu = [UIMenuController  sharedMenuController];
//        UIMenuItem   *menuItem1 = [[UIMenuItem alloc]initWithTitle:@"分组管理" action:@selector(manageGroup)];
//        menu.menuItems = [NSArray arrayWithObjects:menuItem1,nil];
//      [menu setTargetRect:CGRectMake(locationPoint.x - 60, locationPoint.y , 120, 44) inView:self];
//        [menu setMenuVisible:YES animated:YES];
    }
}

-(void)manageGroup
{
//    if ([_delegate respondsToSelector:@selector(manageGroup)]) {
//        [_delegate manageGroup];
//    }
    
}
- (void)setFriendGroup:(GroupModel *)friendGroup
{
    _groupModel = friendGroup;
    
//    [_bgButton setTitle:friendGroup.groupname forState:UIControlStateNormal];
    _numLabel.text = friendGroup.groupname;
    _countLabel.text = [NSString stringWithFormat:@"%lu人",_groupModel.groupmembers.count ];
}

- (void)didMoveToSuperview
{
    _bgButton.imageView.transform = _groupModel.isOpened ? CGAffineTransformMakeRotation(M_PI_2) : CGAffineTransformMakeRotation(0);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _bgButton.frame = self.bounds;
}

@end
