//
//  SelectGroupView.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
@protocol ManagerGroupDelegate <NSObject>


-(void)addGroup:(NSString*)groupName;//增加好友分组

@end
#import <UIKit/UIKit.h>

@interface SelectGroupView : UIView<UIAlertViewDelegate>
@property(unsafe_unretained,nonatomic)id<ManagerGroupDelegate>delegate;
@end
