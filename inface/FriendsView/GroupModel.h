//
//  GroupModel.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupModel : NSObject
+(instancetype)shareInstance;

@property (nonatomic, copy) NSString *groupid;

@property (nonatomic, copy) NSString *groupname;

@property (nonatomic, strong) NSArray *groupmembers;

@property (nonatomic,assign)  BOOL isOpened;
@end
