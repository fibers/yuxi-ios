//
//  NewExamineTableViewCell.m
//  inface
//
//  Created by huangzengsong on 15/6/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "NewExamineTableViewCell.h"

@implementation NewExamineTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.UserNameLabel.textColor=BLACK_CONTENT_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
