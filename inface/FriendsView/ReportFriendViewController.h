//
//  ReportFriendViewController.h
//  inface
//
//  Created by huangzengsong on 15/6/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "ReportTableViewCell.h"
#import "HelpInformationViewController.h"
@interface ReportFriendViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;
@property (strong, nonatomic) NSString *reportid;//举报的家族id或者剧群id
@property(assign,nonatomic)    BOOL   isAnimating;

@end
