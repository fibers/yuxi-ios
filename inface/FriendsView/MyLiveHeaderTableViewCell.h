//
//  MyLiveHeaderTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyLiveHeaderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *UserImage;//头像
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字

@property (weak, nonatomic) IBOutlet UIButton *CountButton;
@end
