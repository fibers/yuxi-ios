//
//  GroupManageController.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GroupManageController.h"
#import "GroupView.h"
#import "FriendGroupCell.h"
#import "FriendGroupModel.h"
@interface GroupManageController ()<ManagerGroupDelegate,UITableViewDelegate,UITableViewDataSource,DeleteGroupDelegate,UIAlertViewDelegate>
@property(strong,nonatomic) GroupView  * mainView;
@property(strong,nonatomic) UITableView  * tableView;
@property(strong,nonatomic) FriendGroupModel * fgModel;
@end

@implementation GroupManageController

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    _mainView = [[[NSBundle mainBundle]loadNibNamed:@"GroupView" owner:nil options:nil]firstObject];
//    _mainView.frame = CGRectMake(0, 0, ScreenWidth,50);
//    _mainView.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:_mainView];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 50, ScreenWidth, ScreenHeight - 120)];
    UIView * aview = [[UIView alloc]init];
    _tableView.tableFooterView = aview;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    [_tableView reloadData];
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    self.view.backgroundColor = [UIColor whiteColor];
    
//    UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 50, 0, 30, 30)];
//    [button addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
//    [button setTitle:@"完成" forState:UIControlStateNormal];
//    [button.titleLabel  setFont:[UIFont systemFontOfSize:14.0]];
//    [button setTitleColor:[UIColor colorWithHexString:@"#eb71a8"] forState:UIControlStateNormal];
//    UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithCustomView:button];
    
//    self.navigationItem.rightBarButtonItem = back;
    self.navigationItem.title = @"分组管理";
    
    [self addHeadView];
//    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithHexString:@"#eb71a8"],UITextAttributeTextColor, nil]];
    
    // Do any additional setup after loading the view.
}


-(void)returnBackl
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)addHeadView
{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(12, 6, 40, 37);
    
    [button setImage:[UIImage imageNamed:@"addGroup"] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(addGroupBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(60, 17, 80, 16)];
    
    label.text = @"添加分组";
    
    label.textColor = [UIColor colorWithHexString:@"#4a4a4a"];
    
    [label setFont:[UIFont systemFontOfSize:15.0]];
    
    [self.view addSubview:label];
}



- (void)addGroupBtn:(id)sender
{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"添加分组" message:@"请为新分组输入名称" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder= @"分组名称在10字以内";
    alert.tag = 10000;
    [alert show];
    
}


#pragma mark UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10000) {
        if (buttonIndex == 1 && [alertView textFieldAtIndex:0].text.length > 0 && [alertView textFieldAtIndex:0].text.length <= 10) {
            //此时添加好友分组
            
            [self addGroup:[alertView textFieldAtIndex:0].text];
            }
            
        }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setContent:(FriendGroupModel *)fgModel
{
    _fgModel = fgModel;
}

#pragma mark ManagerGroupDelegate
-(void)deleteGroup:(NSString *)groupid indexPath:(NSIndexPath*)index
{
    //删除分组
    if ([groupid isEqualToString:@"0"]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"默认分组不可以删除哦，亲！" message:nil delegate:nil cancelButtonTitle:@"明白了" otherButtonTitles: nil];
        [alert show];
        return;
    }
    NSDictionary * params = @{@"uid":USERID,@"friendgroupid":groupid,@"token":@"110"};
    [HttpTool postWithPath:@"DelFriendsGroup" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
          //删除成功
            NSMutableArray * groupArr = [NSMutableArray arrayWithArray:_fgModel.friendgroups  ];
            [groupArr removeObjectAtIndex:index.row];
            _fgModel.friendgroups = groupArr;
            [_tableView deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationFade];
            [_tableView reloadData];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"DELETEGROUP" object:nil];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}


-(void)addGroup:(NSString *)groupName
{
    //增加分组
    NSDictionary * params = @{@"uid":USERID,@"friendgroupname":groupName};
    [HttpTool postWithPath:@"CreateFriendsGroup" params:params success:^(id JSON) {
        //创建成功，更新modek的数结构，刷新表格,需要重新获取分组
        if ([[JSON objectForKey:@"result"]intValue] >0 ) {
            NSDictionary * dic = @{@"uid":USERID,@"token":@"110"};
            [HttpTool postWithPath:@"GetFriendListWithGroup" params:dic success:^(id JSON) {
                _fgModel = [FriendGroupModel objectWithKeyValues:JSON];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"DELETEGROUP" object:nil];

                [_tableView reloadData];
                
            } failure:^(NSError *error) {
                
                
            }];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}


#pragma mark  UITableView Delegate


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray * groupArr = _fgModel.friendgroups;
    return groupArr.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellIdentifier = @"FriendGroup";
    FriendGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"FriendGroupCell" owner:nil options:nil]firstObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    GroupModel * model = _fgModel.friendgroups[indexPath.row];
    
    [cell setContent:model indexPath:indexPath];
    cell.delegate = self;
    
    return cell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
