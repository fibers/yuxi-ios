//
//  SelectGroupView.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SelectGroupView.h"

@implementation SelectGroupView
- (IBAction)addNewGroup:(id)sender
{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"添加分组" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"添加", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder= @"分组名称在20字以内";

    alert.tag = 10000;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10000) {
        NSString * text = [alertView textFieldAtIndex:0].text;
        if (text.length > 0 && text.length <= 20 && buttonIndex == 1) {
            if ([self.delegate respondsToSelector:@selector(addGroup:)]) {
                
                [self.delegate addGroup:text];
                }
            [alertView resignFirstResponder];
            }
        }
    if (buttonIndex == 0) {
        [alertView resignFirstResponder];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
