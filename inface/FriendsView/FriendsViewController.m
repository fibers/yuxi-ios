//
//  FriendsViewController.m
//  inface
//
//  Created by xigesi on 15/4/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
#import "FriendsViewController.h"
#import "DDDatabaseUtil.h"
#import "GroupsKindsViewController.h"
#import "SavePersonPortrait.h"
#import "GetCapitalWord.h"
#import "LoginOut.h"
#import "ShareModel.h"
#import "GetGroupRoleName.h"
#import "CurrentGroupModel.h"
#import "ChattingModule.h"
#import "YXMyCenterViewController.h"
#import "FriendGroupModel.h"
#import "GroupModel.h"
#import "HeadView.h"
#import "FriendModel.h"
#import "GroupManageController.h"
#import "FriendHeaderViewCell.h"
#import "NSString+YXExtention.h"
#import "MsgAddGroupConfirmAPI.h"
#import "AppDelegate.h"
#import "MJRefresh.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/NSNotificationCenter+RACSupport.h>
#import "MsgAddFriendConfirmReqAPI.h"
#define TOPSESSIONS   @"topSessions"

@interface FriendsViewController ()<HeadViewDelegate,ManageFriendGroupDelegate,UnreadMessageCount>

@end

@implementation FriendsViewController
{
    FriendGroupModel * friendGroup;
    
    NSMutableDictionary * friendsDic;//存储好友的字典
    
    UIActivityIndicatorView *activity;
    
    NSInteger      _unreadCount;
    
    NSMutableArray * topSessionsArr;//存储置顶的会话
    
}
#define SECTION_TAG  10000;
@synthesize tableViewOutlet;


+(instancetype)shareInstance
{
    static FriendsViewController * friendsView;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        friendsView = [[FriendsViewController alloc] init];
        
    });
    return friendsView ;
}
//右上角按钮
-(void)returnBackr{
    
    mySheet = [[UIActionSheet alloc]
               initWithTitle:nil
               delegate:self
               cancelButtonTitle:@"取消"
               destructiveButtonTitle:nil
               otherButtonTitles:@"添加好友",@"添加家族",@"添加剧群",@"邀请QQ好友",nil];
    
    mySheet.tag=3000;
    //    [mySheet showInView:self.view];
    [mySheet showFromTabBar:self.tabBarController.tabBar];
    
}

-(NSInteger)setUnreadCount
{
    return _unreadCount;
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag==3000) {
        
        if (buttonIndex==0) {
            //搜索好友
            
            SearchFriendsViewController * detailViewController = [[SearchFriendsViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }else if (buttonIndex==1){
            //搜索家族
            SearchFamilyViewController * detailViewController = [[SearchFamilyViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }else if (buttonIndex==2) {
            //搜索剧
            SearchTheatreViewController * detailViewController = [[SearchTheatreViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }else if (buttonIndex==3) {
            //邀请QQ好友
            if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]]) {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"尚未安装QQ客户端" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
                [alert show];
                return;
            }
            [mySheet dismissWithClickedButtonIndex:0 animated:YES];
#pragma mark 邀请QQ好友
            //    NSString * ownerName = [self.dic objectForKey:@"ownername"];
            NSString * title = [NSString stringWithFormat:@"好友的邀请函"];
            NSString * nickname = [[NSUserDefaults standardUserDefaults]objectForKey:@"nickname"];
            NSString * content = [NSString stringWithFormat:@"[语戏]好友[%@]邀请你入驻语戏",nickname];
            NSString * portrait = [[NSUserDefaults standardUserDefaults]objectForKey:@"portrait"];
            NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            NSString * downLoadUrl = [NSString stringWithFormat:@"http://www.yuxip.com/ishare.php?userid=%@&kehuduan=ios",USERID];
            NSDictionary * params = @{@"title":title,@"content":content,@"url":downLoadUrl};
            [[ShareModel sharedInstance]applyQQFriends:nil applyDic:params];
            
        }
        
    }
}


-(void)uisegumentSelectionChange:(NSInteger)selection{
    if (selection==0) {
        switching=@"0";
        self.tableViewOutlet.tableHeaderView=nil;
        self.tableViewOutlet.tableFooterView = [[UIView alloc]init];

        [self.tableViewOutlet reloadData];
    } else {
        switching=@"1";
        UIView *aview=[[UIView alloc]init];
        aview.frame=CGRectMake(0, 0, ScreenWidth, 124);
        self.tableViewOutlet.tableHeaderView=aview;
        UINib *nib=[UINib nibWithNibName:@"NewExamineTableViewCell" bundle:nil];
        NewExamineTableViewCell *headerview=[[nib instantiateWithOwner:nil options:nil] firstObject];
        headerview.frame=CGRectMake(0, 62, ScreenWidth, 62);
        [aview addSubview:headerview];
        
        UINib  * footNib = [UINib nibWithNibName:@"FriendHeaderViewCell" bundle:nil];
        
        FriendHeaderViewCell * footCell = [[footNib instantiateWithOwner:nil options:nil]firstObject];
        
        footCell.delegate = self;
        [footCell.headButtn setImage:[UIImage imageNamed:@"manaGroupSel"] forState:UIControlStateHighlighted];
        self.tableViewOutlet.tableFooterView = footCell;
        
        UINib *nib1=[UINib nibWithNibName:@"NewExamineTableViewCell" bundle:nil];
        NewExamineTableViewCell *headerview1=[[nib1 instantiateWithOwner:nil options:nil] firstObject];
        headerview1.frame=CGRectMake(0, 0, ScreenWidth, 63);
        [aview addSubview:headerview1];
        
        UIView * aView1 = [[UIView alloc] initWithFrame:CGRectMake(12, 62, ScreenWidth-24, 0.5)];
        aView1.backgroundColor= [UIColor colorWithRed:192/255.0 green:193/255.0 blue:194/255.0 alpha:1];
        [aview addSubview:aView1];
        
        UIView * aView2 = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth-15, 0, 15, 124)];
        aView2.backgroundColor=[UIColor whiteColor];
        [aview addSubview:aView2];
        
        if ([self.tableViewOutlet respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.tableViewOutlet setSeparatorInset:UIEdgeInsetsMake(0, 12, 0, 12)];
        }
        
        if ([self.tableViewOutlet respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.tableViewOutlet setLayoutMargins:UIEdgeInsetsMake(0, 12, 0, 12)];
        }
        self.tableViewOutlet.separatorColor=XIAN_COLOR;
        
        headerview.CountButton.hidden = YES;
        headerview.UserNameLabel.text=@"剧";
        headerview.UserImage.image=[UIImage imageNamed:@"jubtn"];
        headerview.UserImage.layer.cornerRadius = CGRectGetHeight(headerview.UserImage.bounds)/2;
        headerview.UserImage.clipsToBounds = YES;
        
        headerview1.CountButton.hidden = YES;
        headerview1.UserNameLabel.text=@"家族";
        headerview1.UserImage.image=[UIImage imageNamed:@"jiazubtn"];
        headerview1.UserImage.layer.cornerRadius = CGRectGetHeight(headerview1.UserImage.bounds)/2;
        headerview1.UserImage.clipsToBounds = YES;
        
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                                        initWithTarget:self
                                                        action:@selector(TheatreHandleTap:)];
        [headerview addGestureRecognizer:tapGestureRecognizer];
        
        
        UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc]
                                                         initWithTarget:self
                                                         action:@selector(FamilyHandleTap:)];
        [headerview1 addGestureRecognizer:tapGestureRecognizer1];
        
        [self.tableViewOutlet reloadData];
    }
    
}

//左上角按钮
-(void)returnBackl{
    YXFamilyViewController * detailViewController = [[YXFamilyViewController alloc] init];
    detailViewController.hidesBottomBarWhenPushed=YES;

    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        [super loadView];
        switching=@"0";
        LjjUISegmentedControl* ljjuisement=[[LjjUISegmentedControl alloc]initWithFrame:CGRectMake(77, 0, ScreenWidth-154, 44)];
        NSArray* ljjarray=[NSArray arrayWithObjects:@"消息",@"通讯录",nil];
        [ljjuisement AddSegumentArray:ljjarray];
        ljjuisement.delegate=self;
        self.navigationItem.titleView=ljjuisement;
        AppDelegate * delegate =    (AppDelegate *)[UIApplication sharedApplication].delegate;
        delegate.delegate = self;
        topSessionsArr = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:TOPSESSIONS]]  ;
        if (!topSessionsArr) {
            topSessionsArr = [NSMutableArray arrayWithCapacity:10];
        }
        [self getMasterApprenticeList];
//        DDCreateGroupAPI * createGroup = [[DDCreateGroupAPI alloc]init];
//        
//        NSString * user_id = [NSString stringWithFormat:@"user_%d",1003455];
//          NSArray * object = @[@"评论群",@"",@[user_id]];
//        
//        [createGroup requestWithObject:object Completion:^(GroupEntity * response, NSError *error) {
//            
//            NSString * groupId = [response.objID componentsSeparatedByString:@"_"][1];
//            
//                      NSDictionary * params = @{@"uid":@"1003455",@"storyid":@"8491",@"title":@"评论群",@"intro":@"",@"groupid":groupId,@"isplay":@"3"};
//            [HttpTool postWithPath:@"AddGroupToStory" params:params success:^(id JSON) {
//                
//                
//            } failure:^(NSError *error) {
//                
//                
//            }];
//        }];
//        user_id = [NSString stringWithFormat:@"user_%d",1010423];
//        object = @[@"评论群",@"",@[user_id]];
//        [createGroup requestWithObject:object Completion:^(GroupEntity * response, NSError *error) {
//            
//            NSString * groupId = [response.objID componentsSeparatedByString:@"_"][1];
//            
//            NSDictionary * params = @{@"uid":@"1010423",@"storyid":@"8870",@"title":@"评论群",@"intro":@"",@"groupid":groupId,@"isplay":@"3"};
//            [HttpTool postWithPath:@"AddGroupToStory" params:params success:^(id JSON) {
//                
//                
//            } failure:^(NSError *error) {
//                
//                
//            }];
//        }];
        
        //左上角按钮
        UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
        [LeftButton setImage:[UIImage imageNamed:@"jiazulist"] forState:UIControlStateNormal];
        [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
        self.navigationItem.leftBarButtonItem=barback;
        
        
        //右上角按钮
        UIImage *RightImage=[UIImage imageNamed:@"addmore"];
        
        UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
        [RightButton setImage:RightImage forState:UIControlStateNormal];
        [RightButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -20)];
        [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
        self.navigationItem.rightBarButtonItem=barback2;
        self.tableViewOutlet.delegate=self;
        self.tableViewOutlet.dataSource=self;
        
        if ([self.tableViewOutlet respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.tableViewOutlet setSeparatorInset:UIEdgeInsetsMake(0, 12, 0, 12)];
        }
        
        if ([self.tableViewOutlet respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.tableViewOutlet setLayoutMargins:UIEdgeInsetsMake(0, 12, 0, 12)];
        }
        
        self.tableViewOutlet.separatorColor=XIAN_COLOR;
        self.tableViewOutlet.tableHeaderView=nil;
        
        self.tableViewOutlet.tableFooterView=[[UIView alloc]init];
        self.tableViewOutlet.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        self.tableViewOutlet.sectionIndexColor=[UIColor blackColor];
        self.tableViewOutlet.sectionIndexBackgroundColor=[UIColor clearColor];
        
        //获取好友列表
        [SessionModule sharedInstance].delegate = self;
        self.tableData=[NSMutableArray arrayWithCapacity:10];
        myFriends = [NSMutableArray new];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deleteSessionByNoty:) name:DELETE_SESSION object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deleteFriend:) name:DELETE_FRIEND object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addfriend:) name:AGREE_ADD_FRIEND object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(beAgreeFriend:) name:BE_AGREE_FRIEND object:nil];
        //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getTotalNotDealNotifyMessages) name:@"newaddmessages" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getLocalSessions) name:GET_LOCAL_SESSIONS object:nil];
//        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sessionUpdate:Action:) name:SESSION_UPDATE object:nil];
        WS(weakSelf);
        [[[NSNotificationCenter defaultCenter]rac_addObserverForName:SESSION_UPDATE object:nil] subscribeNext:^(NSNotification * notification) {
            SessionEntity * session = notification.object;
            [weakSelf sessionUpdate:session Action:ADD];
            
        }];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getFriendsNickName) name:GET_FRIENDS_NICKNAME object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getFriendGroups) name:@"DELETEGROUP" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadReadSession:) name:@"UPDATESESSION" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getFriendGroups) name:@"GETFRIENDLIST" object:nil];
//        self.refreshControlR = [[UIRefreshControl alloc] init];
//        self.refreshControlR.tintColor = BLUECOLOR;
//        [self.refreshControlR addTarget:self action:@selector(refreshActionR) forControlEvents:UIControlEventValueChanged];
//        [self.tableViewOutlet addSubview:self.refreshControlR];
        activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activity.frame=CGRectMake(ScreenWidth/2, 10, 20, 20);
        
        [self.view addSubview:activity];
        self.currentgroupMessages = [NSMutableArray arrayWithCapacity:10];
        sectionsArr = [NSMutableArray arrayWithObjects:@"家族", @"剧",@"好友",nil];
//        UIView * footView = [[UIView alloc]init];
//        self.tableViewOutlet.tableFooterView = footView;
        [self.tableViewOutlet addHeaderWithTarget:self action:@selector(refreshActionR)];
        NSString * firstcomein=[[NSUserDefaults standardUserDefaults]objectForKey:@"firstcomein3"];
        if (![firstcomein isEqualToString:@"yes"]) {
            aimageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
            aimageView.contentMode = UIViewContentModeScaleAspectFit;
            aimageView.backgroundColor=COLOR(0, 0, 0, 0.75);
            aimageView.userInteractionEnabled=YES;
            [self.tabBarController.view addSubview:aimageView];
            bimageView=[[UIImageView alloc]initWithFrame:CGRectMake(17, 32, 295, 211)];
            bimageView.contentMode = UIViewContentModeScaleAspectFit;
            bimageView.backgroundColor=[UIColor clearColor];
            bimageView.image=[UIImage imageNamed:@"mask5"];
            bimageView.userInteractionEnabled=YES;
            [aimageView addSubview:bimageView];
            
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeImage:)];
            //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
            tapGestureRecognizer.cancelsTouchesInView = NO;
            //将触摸事件添加到当前view
            [bimageView addGestureRecognizer:tapGestureRecognizer];
            
        }
        [self getFriendGroups];
        
        [self getHasLoadSessions];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableViewOutlet addHeaderWithTarget:self action:@selector(refreshActionR)];
    
    // Do any additional setup after loading the view from its nib.
    
}


#pragma MARK 获取师徒列表
-(void)getMasterApprenticeList
{
    
    NSDictionary * params = @{@"uid":USERID,@"token":@"110"};
    NSMutableArray * apprenceLists = [NSMutableArray arrayWithCapacity:10];
    [HttpTool postWithPath:@"GetMasterAndApprenticeList" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            NSArray * masterAndApprence = [JSON objectForKey:@"MasterAndApprenticeList"];
            [masterAndApprence enumerateObjectsUsingBlock:^(NSDictionary * apprences, NSUInteger idx, BOOL * _Nonnull stop) {
                
                if ([[apprences objectForKey:@"relationship"]isEqualToString:@"apprentice"]) {
                    //将徒弟信息保存起来
                    [apprenceLists addObject:apprences];
                }
            }];
            [[NSUserDefaults standardUserDefaults]setObject:apprenceLists forKey:@"apprencelist"];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}
#pragma mark 点击推送消息进来是获取recent
-(void)getLocalRecents
{
    [self getLocalSessions];
}

#pragma mark 获取好友分组列表
-(void)getFriendGroups
{
    NSDictionary * params = @{@"uid":USERID,@"token":@"110"};
    [HttpTool postWithPath:@"GetFriendListWithGroup" params:params success:^(id JSON) {
        friendGroup = [FriendGroupModel objectWithKeyValues:JSON];
        [self deleteSessionNotFriendSession];
        [self.tableViewOutlet reloadData];
        
    } failure:^(NSError *error) {
        
        
    }];
    [self getMasterApprenticeList];
}


#pragma mark 删除非好友的会话体
-(void)deleteSessionNotFriendSession
{
    NSMutableDictionary * idsDic = [NSMutableDictionary dictionaryWithCapacity:10];
    for (int i = 0; i < friendGroup.friendgroups.count; i++) {
        GroupModel * gmodel = friendGroup.friendgroups[i];
        for (int j = 0; j < gmodel.groupmembers.count; j++) {
            FriendModel * fmodel = gmodel.groupmembers[j];
            NSString * user_id = [NSString stringWithFormat:@"user_%@",fmodel.userid];
            DDUserEntity * entity = [[DDUserEntity alloc]initWithUserID:user_id name:fmodel.realname nick:fmodel.markname avatar:fmodel.portrait userRole:100 userUpdated:100];
            [[DDUserModule shareInstance] addMaintanceUser:entity];
            [idsDic setObject:fmodel.userid forKey:fmodel.userid];
        }
    }


    for (int i = 0; i < [self.tableData count]; i++) {
        id obj = self.tableData[i];
        if (![obj isKindOfClass:[CurrentGroupModel class]]) {
            
        SessionEntity * session = self.tableData[i];
            if (session.sessionType != SessionTypeSessionTypeGroup && ![session.sessionID isEqualToString:GROUP_MESSAGE]) {
                NSInteger friendId =  [[RuntimeStatus instance]changeIDToOriginal:session.sessionID];
                
                NSString * realValue = [idsDic objectForKey:[NSString stringWithFormat:@"%lu",friendId]];
                
                if (!realValue) {
                    MsgReadACKAPI* readACK = [[MsgReadACKAPI alloc] init];
                    [readACK requestWithObject:@[session.sessionID,@(session.lastMsgID),@(session.sessionType)] Completion:nil];
                    [[SessionModule sharedInstance]removeSessionByServer:session];
                    [self.tableData removeObject:session];
                    
                }
            }

        }
    }
    NSUInteger count =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
    _unreadCount = count;
    [self setToolbarBadge:count];
    
    [self.tableViewOutlet reloadData];
    
    
}


-(void)deleteSessionByNoty:(NSNotification *)noty
{
    NSString * sessionId = noty.object;
    for (int i = 0; i < self.tableData.count; i++) {
        id object = [self.tableData objectAtIndex:i];
        if ([object isKindOfClass:[SessionEntity class]]) {
            SessionEntity * session = object;
            NSString * sessionID = session.sessionID;
            if (sessionID && [sessionID isKindOfClass:[NSString class]]) {
                if ([sessionId isEqualToString:sessionID]) {
                    MsgReadACKAPI* readACK = [[MsgReadACKAPI alloc] init];
                    [readACK requestWithObject:@[session.sessionID,@(session.lastMsgID),@(session.sessionType)] Completion:nil];
                    [[SessionModule sharedInstance]removeSessionByServer:session];
                    [topSessionsArr removeObject:session.sessionID];//取消置顶的会话体
                    [[NSUserDefaults standardUserDefaults]setObject:topSessionsArr forKey:TOPSESSIONS];
                    [self.tableData removeObjectAtIndex:i];
                    NSUInteger count =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
                    [self setToolbarBadge:count];
                    _unreadCount = count;
                    [self.tableViewOutlet reloadData];
                }
            }
            
        }
    }
    
    
}



-(void)removeImage:(UITapGestureRecognizer*)tap{
    [aimageView removeFromSuperview];
    [[NSUserDefaults standardUserDefaults]setValue:@"yes" forKey:@"firstcomein3"];
}




//剧点击
-(void)TheatreHandleTap:(UITapGestureRecognizer *)recognizer
{
    
    MyLiveViewController * detailViewController = [MyLiveViewController shareInstance];
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

//家族点击
-(void)FamilyHandleTap:(UITapGestureRecognizer *)recognizer
{
    MyFamilyViewController * detailViewController = [MyFamilyViewController shareInstance];
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


-(void)refreshActionR
{
    if ([switching isEqualToString:@"0"]) {
        //消息
        [self getLocalSessions];
    }else if ([switching isEqualToString:@"1"]) {
        [self.tableViewOutlet headerEndRefreshing];
        //获取好友列表
//        [self getFriendList :YES];
    }
    
}




#pragma mark 被对方同意添加好友
-(void)beAgreeFriend :(NSNotification*)noty
{
    NSDictionary * personDic = noty.object ;
    NSString * userid = [NSString stringWithFormat:@"user_%@",[personDic objectForKey:@"id"]];
    DDUserEntity * userEntiry = [[DDUserEntity alloc]initWithUserID:userid name:nil nick:[personDic objectForKey:@"nickname"] avatar:[personDic objectForKey:@"portrait"] userRole:100 userUpdated:100];
    [[DDUserModule shareInstance]addMaintanceUser:userEntiry];
    [[DDDatabaseUtil instance]insertUser:userEntiry];
    
    [self getFriendList :YES];
    //在好友列表里加上此人
}

#pragma mark 同意添加对方为好友
-(void)addfriend:(NSNotification*)noti
{
    //    [self getFriendList :NO];
    NSDictionary * personDic = noti.object ;
    NSString * fromUserId = [personDic objectForKey:@"fromid"];
    NSString *    msgContent = @"我们已经是好友了，立即开始聊天吧";
    FriendModel * model = [[FriendModel alloc]init];
    model.realname  = [personDic objectForKey:@"name"];
    model.portrait  = [personDic objectForKey:@"portrait"];
    model.userid = fromUserId;
    NSString* senderId = [NSString stringWithFormat:@"user_%@",fromUserId];

        //    [self addNewFriends:name sessioniD:[personDic objectForKey:@"fromid"] portrait:portrait firstLetter:letter];
        [self addNewFriend:model];
    DDUserEntity * userEntity = [[DDUserEntity alloc]initWithUserID:senderId name:[personDic objectForKey:@"name"] nick:[personDic objectForKey:@"name"] avatar:[personDic objectForKey:@"portrait"] userRole:100 userUpdated:100];
        
    [[DDUserModule shareInstance]addMaintanceUser:userEntity];
    [[DDDatabaseUtil instance]insertUser:userEntity];
    
    SessionEntity * session = [[SessionModule sharedInstance]getSessionById:senderId];
    if (!session) {
        session  = [[SessionEntity alloc]initWithSessionID:senderId type:SessionTypeSessionTypeSingle];
    }

    session.unReadMsgCount = 1;
    
    ChattingModule * module = [[ChattingModule alloc]init];
    module.sessionEntity = session;
    DDMessageContentType msgContentType = DDMessageTypeText;
    DDMessageEntity * message = [DDMessageEntity makeMessage:msgContent Module:module MsgType:msgContentType isSendImage:NO isCurrentGroup:NO];
    
    [[DDMessageSendManager instance]sendMessage:message isGroup:NO Session:session completion:^(DDMessageEntity *message, NSError *error) {
        
        
    } Error:^(NSError *error) {
        
        
    }];
    [self getFriendList:NO];
    //手动添加好友
    //    __block NSString * letter;
    //    [[GetCapitalWord shareInstance]getFirstLetter:[personDic objectForKey:@"name"] success:^(id json) {
    //
    //       letter = (NSString*)json;
    //    }];

    
}


#pragma mark 手动添加好友
-(void)addNewFriend:(FriendModel *)fmodel
{
    GroupModel * groupModel = friendGroup.friendgroups[0];
    NSMutableArray * gFriends = [NSMutableArray arrayWithArray:groupModel.groupmembers] ;
    [gFriends addObject:fmodel];
    groupModel.groupmembers = gFriends;
    [tableViewOutlet reloadData];
}


#pragma mark 手动添加好友
-(void)addNewFriends:(NSString *)nickname sessioniD:(NSString*)sessionId portrait:(NSString*)portrait firstLetter:(NSString*)firstLetter
{
    NSString * user_userid = [NSString stringWithFormat:@"user_%@",sessionId];
    DDUserEntity * entity = [[DDUserEntity alloc]initWithUserID:user_userid name:nil nick:nickname avatar:portrait userRole:100 userUpdated:100];
    [[DDUserModule shareInstance]addMaintanceUser:entity];
    //看添加的好友的首字母是否已经存在
    BOOL isExit = NO;
    for (int i = 0; i < [myFriends count] ; i++) {
        NSMutableDictionary * dicInfo = [NSMutableDictionary dictionaryWithDictionary:[myFriends objectAtIndex:i]];
        NSString * letter = [dicInfo objectForKey:@"firstLetter"];
        if ([[firstLetter uppercaseString] isEqualToString:letter]) {
            NSMutableArray * friendsArr = [NSMutableArray arrayWithArray:[dicInfo objectForKey:@"friends"]];
            NSDictionary * dic = @{@"FirstLetter":[firstLetter uppercaseString],@"gender":@"1",@"id":sessionId,@"name":nickname,@"portrait":portrait};
            [friendsArr addObject:dic];
            [dicInfo setValue:friendsArr forKey:@"friends"];
            [myFriends replaceObjectAtIndex:i withObject:dicInfo];
            isExit = YES;
            
        }
    }
    if (isExit == NO) {
        //如果是新人 需要重新构造
        if ([firstLetter isEqualToString:@"#"]) {
            NSDictionary * dic = @{@"FirstLetter":firstLetter,@"gender":@"1",@"id":sessionId,@"name":nickname,@"portrait":portrait};
            NSMutableArray * arr = [NSMutableArray arrayWithObject:dic];
            NSMutableDictionary * persondic = [NSMutableDictionary dictionaryWithObjectsAndKeys:firstLetter,@"firstLetter" ,arr,@"friends", nil];
            [myFriends addObject:persondic];
            NSArray * sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"firstLetter" ascending:YES]];
            [myFriends sortUsingDescriptors:sortDescriptors];
        }else{
            NSDictionary * dic = @{@"FirstLetter":[firstLetter uppercaseString],@"gender":@"1",@"id":sessionId,@"name":nickname,@"portrait":portrait};
            NSMutableArray * arr = [NSMutableArray arrayWithObject:dic];
            NSMutableDictionary * persondic = [NSMutableDictionary dictionaryWithObjectsAndKeys:firstLetter,@"firstLetter" ,arr,@"friends", nil];
            [myFriends addObject:persondic];
            NSArray * sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"firstLetter" ascending:YES]];
            [myFriends sortUsingDescriptors:sortDescriptors];
        }
    }
    NSString * session_id = [NSString stringWithFormat:@"user_%@",sessionId];
    [[SavePersonPortrait shareInstance]setSessionid:session_id portraitImage:portrait];
    
    [self.tableViewOutlet reloadData];
}



#pragma mark 删除不是好友的会话体
-(void)deleteUnFriendSession
{
//    NSMutableDictionary * idsDic = [NSMutableDictionary dictionaryWithCapacity:10];
//    NSArray * gModel = friendGroup.friendgroups;
//    
//    for (int i = 0; i < gModel.count; i++) {
//        GroupModel * gModel = friendGroup.friendgroups[i];
//        NSArray * Fmodel = gModel.groupmembers;
//        for (int j = 0; j < Fmodel.count; j++) {
//            FriendModel * fmodel = Fmodel[j];
//            [idsDic setObject:fmodel.userid forKey:fmodel.userid];
//
//        }
//    }
//    for (int i = 0; i < self.tableData.count; i++) {
//        NSString * user_id = self.tableData[i];
//        NSArray * ids = [user_id componentsSeparatedByString:@"_"];
//
//        NSString * uid = @"";
//        if (ids.count > 1) {
//            uid = ids[1];
//        }else{
//            uid = ids[0];
//        }
//        if (![idsDic objectForKey:uid]) {
//            [self.tableData removeObjectAtIndex:i];
//        }
//    }
//
//    [self.tableViewOutlet reloadData];
}

#pragma 删除好友后将好友从列表去

-(void)deleteFriend :(NSNotification*)notification
{
    NSString * deleteId = notification.object;
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    [[DDDatabaseUtil instance]clearUpAddMessages:deleteId toid:userid groupid:@"-2" type:@"-2"];
    [self getFriendGroups];
    
    //从会话体中删除
    //删除数据库的对应数据
    NSString * otherId = [NSString stringWithFormat:@"user_%@",deleteId];
    for (int i = 0; i < [self.tableData count]; i++) {
        id  object = [self.tableData objectAtIndex:i];
        if ([object isKindOfClass:[SessionEntity class]]) {
            SessionEntity *session = object;
            NSString * sessionId = session.sessionID;
            if ([otherId isEqualToString:sessionId]) {
                MsgReadACKAPI* readACK = [[MsgReadACKAPI alloc] init];
                [readACK requestWithObject:@[session.sessionID,@(session.lastMsgID),@(session.sessionType)] Completion:nil];
                [[SessionModule sharedInstance]removeSessionByServer:session];
                [self.tableData removeObjectAtIndex:i];
                [[DDDatabaseUtil instance]deleteSessionEntity:sessionId];
                [self.tableViewOutlet reloadData];
                long count = [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"]integerValue];
                _unreadCount = count;
                [self setToolbarBadge:count];
            }
        }
        
        
    }
    
}


-(void)GetEnteredStoryGroups:(void(^)(int count))success;//获取未读消息数
{
    NSArray *ChatArr=[[SessionModule sharedInstance]getAllSessions];
    NSMutableArray * countArr = [NSMutableArray array];
    for (int i=0; i<ChatArr.count; i++) {
        SessionEntity *sessionEntity=[ChatArr objectAtIndex:i];
        //sessionType 1为单个用户会话2为群会话
        if (sessionEntity.sessionType==1) {
            [countArr addObject:sessionEntity];
        }
    }
    
    NSUInteger unreadcount =  [[countArr valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
    _unreadCount = unreadcount;
    [self setToolbarBadge:unreadcount];
    
}

-(void)getFriendList :(BOOL)getLocalSessions
{
    
    //获取好友列表接口(Post)
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary *params = @{@"uid":userid,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetFriendListWithGroup" params:params success:^(id JSON) {
        self.tableViewOutlet.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight-108);

        [self.tableViewOutlet headerEndRefreshing];
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            friendGroup = [FriendGroupModel objectWithKeyValues:JSON];
            
            [self deleteSessionNotFriendSession];

            [self.tableViewOutlet reloadData];
            if (getLocalSessions == YES) {
//                [self savePortraitImage];
                [self getLocalSessions];
            }
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
        self.tableViewOutlet.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight-108);

        [self.tableViewOutlet headerEndRefreshing];

    }];
}



#pragma mark 获取我的家族列表
-(void)getMyFamilies
{
    
    //获取家族列表接口(Post)
    familysArr =    [[[GetAllFamilysAndStorys shareInstance]type:@"1"]mutableCopy];
    
    if (familysArr.count==0) {
        [SVProgressHUD showSuccessWithStatus:@"您还没有自己的家族哦！快去创建或者加入一个吧！" duration:2.0];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.tableViewOutlet reloadData];
        
    });
}



#pragma mark 从getallFamilysAndStory中拿到我的剧
-(void)getMyLocalStorys
{
    storysArr = [NSMutableArray arrayWithArray:[[GetAllFamilysAndStorys shareInstance]type:@"2"]];
}


#pragma mark 储存好友头像信息
-(void)savePortraitImage
{
    for (int j = 0; j < [myFriends count]; j++) {
        NSDictionary * personDic = [myFriends objectAtIndex:j];
        NSArray * persons = [personDic objectForKey:@"friends"];
        for (int i = 0; i < [persons count]; i++) {
            NSDictionary * personDic = [persons objectAtIndex:i];
            
            NSString * sessionId = [NSString stringWithFormat:@"user_%@",[personDic objectForKey:@"id"]];
            NSString * urlString = [personDic objectForKey:@"portrait"];
            [[SavePersonPortrait shareInstance]setSessionid:sessionId portraitImage:urlString];
        }
    }
}




-(void)sessionUpdate:(SessionEntity *)noty Action:(SessionAction)action
{
    //sessionType 1为单个用户会话2为群会话
    topSessionsArr = [[NSUserDefaults standardUserDefaults]objectForKey:TOPSESSIONS];

        SessionEntity * session = noty;
        if ([self.tableData containsObject:session]) {
            if (![topSessionsArr containsObject:session.sessionID]) {
                NSUInteger index = [self.tableData indexOfObject:session];
                [self.tableData removeObjectAtIndex:index];
                [self.tableData insertObject:session atIndex:topSessionsArr.count];
            }

        }else{
            //需要判断一下，如果是我的剧或者我的家族消息，才会存到好友消息列表里,针对的是群消息
            if (session.sessionType == SessionTypeSessionTypeGroup) {
                NSString * sessionId = session.sessionID;
                NSArray * originalId = [sessionId componentsSeparatedByString:@"_"];
                NSString * groupid = [originalId objectAtIndex:1];
                //如果是我的家族或者我的剧群里的消息，则保存到消息列表
                [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:groupid success:^(id JSON) {
                    if (JSON && ![topSessionsArr containsObject:sessionId]) {
                        //如果是在置顶的数组里，则不用管顺序，否则插到置顶数组下边
                        self.tableData = [NSMutableArray arrayWithArray:self.tableData];

                        [self.tableData insertObject:session atIndex:topSessionsArr.count];
                        
                    }
                    
                }failure:^(id Json) {

                }];
            }else{
                
                //个人消息直接添加到消息列表
                if (![topSessionsArr containsObject:session.sessionID]) {
                    [self.tableData insertObject:session atIndex:topSessionsArr.count];

                }
                
            }
            
        }
//          NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeInterval" ascending:NO];
//        
//          [self.tableData sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
        [self.tableViewOutlet reloadData];
        
        NSUInteger count =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
        _unreadCount = count;
        [self setToolbarBadge:count];

   
}

-(void)viewWillAppear:(BOOL)animated
{
//    [self getLocalSessions];
    
    [super viewWillAppear:YES];
    self.hasCliked = NO;

}


#pragma mark 获取已经获取到的sessions
-(void)getHasLoadSessions
{
    NSArray *ChatArr=[[SessionModule sharedInstance]getAllSessions];
    for (int i=0; i<ChatArr.count; i++) {
        SessionEntity *sessionEntity=[ChatArr objectAtIndex:i];
        
        [[ DDDatabaseUtil instance]getSessionTimeInterval:sessionEntity.sessionID success:^(NSInteger time) {
            
            sessionEntity.timeInterval = time;
        }];
        //如果是群消息，需要判断是非临时群的消息
        if (sessionEntity.sessionType == SessionTypeSessionTypeGroup) {
            NSString * session_id = sessionEntity.sessionID;
            NSArray * sessionArr = [session_id componentsSeparatedByString:@"_"];
            NSString * groupid;
            if (sessionArr.count== 1) {
                groupid = sessionArr[0];
            }
            if (sessionArr.count > 1) {
                groupid = sessionArr[1];
            }
            [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:groupid success:^(id JSON) {
                
                //如果此群消息为非临时群消息时加载到消息列表
                if (JSON) {
                    [self.tableData addObject:sessionEntity];
                }
            }failure:^(id Json) {
                
                
            }];
        }else{
            [self.tableData addObject:sessionEntity];
        }
        //sessionType 1为单个用户会话2为群会话
    }
    
    [self sortAllSessions];
    
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeInterval" ascending:NO];
//    [self.tableData sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
//    self.tableViewOutlet.tableFooterView  = [[UIView alloc]init];
//    [self.tableViewOutlet reloadData];
//    
//    NSUInteger unreadcount =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
//    
//    _unreadCount = unreadcount;
//    [self setToolbarBadge:unreadcount];

}


#pragma mark 所有会话体排序
-(void)sortAllSessions
{
    topSessionsArr = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:TOPSESSIONS]];
//    [self.tableData enumerateObjectsUsingBlock:^(SessionEntity *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        
//        if ([topSessionsArr containsObject:obj.sessionID]) {
//            //如果在被置顶的数组里
//            NSInteger topIndex = [topSessionsArr indexOfObject:obj.sessionID];
//            
//            obj.timeInterval = obj.timeInterval + (topSessionsArr.count - topIndex) *obj.timeInterval;
//            
//            [self.tableData replaceObjectAtIndex:idx withObject:obj];
//        }
//    }];
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeInterval" ascending:NO];
//    [self.tableData sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSArray * currentSessions = [self.tableData sortedArrayUsingComparator:^NSComparisonResult(SessionEntity *  _Nonnull obj1, SessionEntity *  _Nonnull obj2) {
        //需要判断是否是置顶会话体
        if ([topSessionsArr containsObject:obj1.sessionID] && [topSessionsArr containsObject:obj2.sessionID]) {
            //都是置顶会话体
            NSInteger index1 = [topSessionsArr indexOfObject:obj1.sessionID];
            
            NSInteger index2 = [topSessionsArr indexOfObject:obj2.sessionID];
            
            long result = index1 <= index2 ? NSOrderedAscending : NSOrderedDescending;
            return result;
        }
        else
        {
            if ([topSessionsArr containsObject:obj1.sessionID] && ![topSessionsArr containsObject:obj2.sessionID]) {
                return NSOrderedAscending;
            }
            else if
                (![topSessionsArr containsObject:obj1.sessionID] && [topSessionsArr containsObject:obj2.sessionID]){
                    return NSOrderedDescending;
            }else{
                //都没有置顶
                NSNumber * number1 = [NSNumber numberWithInteger:-obj1.timeInterval];
                
                NSNumber * number2 = [NSNumber numberWithInteger:-obj2.timeInterval];
                
                return [number1 compare:number2];
            }
        }
    }];
    self.tableData = [NSMutableArray arrayWithArray:currentSessions];
    NSUInteger unreadcount =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
    _unreadCount = unreadcount;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.tableViewOutlet.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight-108);
        [self.tableViewOutlet reloadData];
        [self setToolbarBadge:unreadcount];
        
        [self.tableViewOutlet headerEndRefreshing];
    });
    
}


#pragma mark 获取本地的消息
-(void)getLocalSessions
{
//    [activity startAnimating];
    [[SessionModule sharedInstance]loadLocalSession:^(bool isok) {
        if (isok) {
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [[SessionModule sharedInstance]getRecentSession:^(NSUInteger count) {
//                    [activity stopAnimating];
                    [self.tableData removeAllObjects];
                    [[ CurrentGroupModel sharedInstance].currentArr removeAllObjects];
                    NSArray *ChatArr=[[SessionModule sharedInstance]getAllSessions];
                    for (int i=0; i<ChatArr.count; i++) {
                        SessionEntity *sessionEntity=[ChatArr objectAtIndex:i];

                        [[ DDDatabaseUtil instance]getSessionTimeInterval:sessionEntity.sessionID success:^(NSInteger time) {
                            
                            sessionEntity.timeInterval = time;
                        }];
                        //如果是群消息，需要判断是非临时群的消息
                        if (sessionEntity.sessionType == SessionTypeSessionTypeGroup) {
                            NSString * session_id = sessionEntity.sessionID;
                            NSArray * sessionArr = [session_id componentsSeparatedByString:@"_"];
                            NSString * groupid;
                            if (sessionArr.count== 1) {
                                groupid = sessionArr[0];
                            }
                            if (sessionArr.count > 1) {
                                groupid = sessionArr[1];
                            }
                            [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:groupid success:^(id JSON) {
                                
                                //如果此群消息为非临时群消息时加载到消息列表
                                if (JSON) {
                                    [self.tableData addObject:sessionEntity];
                                }
                            }failure:^(id Json) {
//                                [[CurrentGroupModel sharedInstance].currentArr addObject:sessionEntity];
                                
                                
                            }];
                        }else{
                            [self.tableData addObject:sessionEntity];
                        }
                        //sessionType 1为单个用户会话2为群会话
                    }
                    [self sortAllSessions];
                    
                }];
                
            });
        }
        
    }];
    
        
    
}



-(void)setToolbarBadge:(NSUInteger)count
{
    
    if (count !=0) {
        if (count > 99)
        {
//            [self.parentViewController.tabBarItem setBadgeValue:@"99+"];
            [self.parentViewController.tabBarItem setBadgeValue:[NSString stringWithFormat:@"%ld",(unsigned long)count]];

        }
        else
        {
            [self.parentViewController.tabBarItem setBadgeValue:[NSString stringWithFormat:@"%ld",(unsigned long)count]];
        }
    }else
    {
        [self.parentViewController.tabBarItem setBadgeValue:nil];
    }
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([switching isEqualToString:@"0"]) {
        //消息
        
        return 1;
        
    }else{
        
        //好友
        return friendGroup.friendgroups.count;
    }
    
}


//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//
//    if ([switching isEqualToString:@"0"]) {
//        //消息
//
//        return 1;
//
//    }else{
//
//        //好友
//        return myFriends.count;
//    }
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([switching isEqualToString:@"0"]) {
        //消息
        
        return self.tableData.count;
        
    }else{
        
        //好友
        GroupModel * groupModel = friendGroup.friendgroups[section];
        NSInteger count = groupModel.isOpened ? groupModel.groupmembers.count : 0;
        return count;
    }
}




//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    if ([switching isEqualToString:@"0"]) {
//        //消息
//
//        return self.tableData.count;
//
//    }else{
//
//        //好友
//        NSMutableArray *friendArr=[[myFriends objectAtIndex:section]objectForKey:@"friends"];
//        return friendArr.count;
//
//}
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([switching isEqualToString:@"0"]) {
        //消息
        
        static NSString *identifier=@"InformationTableViewCell";
        InformationTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"InformationTableViewCell" owner:nil options:nil] firstObject];
//            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        NSInteger row = [indexPath row];
        if (indexPath.row >= self.tableData.count) {
            return [[UITableViewCell alloc]init];
        }
        id  object = [self.tableData objectAtIndex:indexPath.row];

        if ([object isKindOfClass:[SessionEntity class]]) {
            [cell setShowSession:self.tableData[row]];
            SessionEntity * session = object;
            if ([topSessionsArr containsObject:session.sessionID]) {
                cell.backgroundColor = [UIColor colorWithHexString:@"#f0f0f6"];
            }
            
            else{
                cell.backgroundColor = [UIColor whiteColor];
            }
        }
//        else{
//            [cell setCurrentGroupsPortrait];
//        }
        //头像从好友里面获取
        return cell;
    } else {
        //通讯录h
        
        static NSString *identifier=@"FriendsTableViewCell";
        FriendsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FriendsTableViewCell" owner:nil options:nil] firstObject];
//            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.SelectButton.hidden=YES;
        
        GroupModel * groupModel = friendGroup.friendgroups[indexPath.section];
        NSArray * friendMembers = groupModel.groupmembers;
        FriendModel * fModel = friendMembers[indexPath.row];
        NSString * userid = fModel.userid;
        //        [[SavePersonPortrait shareInstance]getPersonNickName:userid success:^(id JSON) {
        //
        //            cell.UserNameLabel.text=JSON;
        //
        //        } failure:^(id json) {
        //
        //            cell.UserNameLabel.text= fModel.markname;
        //
        //        }];
        NSString * realName = fModel.realname;
        NSString * markName = fModel.markname;
        if (markName && markName != NULL && ![markName isEqual:nil] &&![markName isEqualToString:@""]) {
            cell.UserNameLabel.text = markName;
        }else{
            cell.UserNameLabel.text = realName;
        }
        
        //        NSString * name = [[friendArr objectAtIndex:indexPath.row]objectForKey:@"name"];
        NSString *urlString=fModel.portrait;
        
        int isVip = [urlString isSignedUser];
        UIImage * image = [UIImage imageNamed:@"vipWriter"];
        UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
        switch (isVip) {
            case 0:
                cell.vipWriterImage.hidden = YES;
                break;
            case 1:
                cell.vipWriterImage.hidden = NO;
                
                cell.vipWriterImage.image = image;
                
                break;
            case 2:
                cell.vipWriterImage.hidden = NO;
                
                cell.vipWriterImage.image = officalImage;
                
                break;
            case 3:
                cell.vipWriterImage.hidden = NO;
                
                cell.vipWriterImage.image = officalImage;
                
                break;
            default:
                break;
        }
        //        NSString * sessionId = [NSString stringWithFormat:@"user_%@",[[friendArr objectAtIndex:indexPath.row]objectForKey:@"id"]];
        [cell.UserImage sd_setImageWithURL:[NSURL URLWithString:[urlString stringOfW100ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
        
        cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;
        cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
        cell.UserImage.clipsToBounds=YES;
        return cell;
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([switching isEqualToString:@"0"]) {
        //消息
        
        return 0;
        
    }else{
        
        //好友
        
        return 44;
    }
}



#pragma mark UITableView DataSource and Delegate
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([switching isEqualToString:@"0"]) {
        return YES;
        
    }
    return NO;
}


#pragma mark 编辑状态
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewCellEditingStyleDelete;
}


-(void)deleteSession:(NSString *)sessionId;//退群后删除相应的会话体
{
    for (int i = 0; i < self.tableData.count; i++) {
        id object = [self.tableData objectAtIndex:i];
        if ([object isKindOfClass:[SessionEntity class]]) {
            SessionEntity * session = object;
            NSString * sessionID = session.sessionID;
            if (sessionID && [sessionID isKindOfClass:[NSString class]]) {
                if ([sessionId isEqualToString:sessionID]) {
                    MsgReadACKAPI* readACK = [[MsgReadACKAPI alloc] init];
                    [readACK requestWithObject:@[session.sessionID,@(session.lastMsgID),@(session.sessionType)] Completion:nil];
                    [[SessionModule sharedInstance]removeSessionByServer:session];
                    [self.tableData removeObjectAtIndex:i];
                    NSUInteger count =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
                    [self setToolbarBadge:count];
                    _unreadCount = count;
                    [self.tableViewOutlet reloadData];
                }
            }
            
        }
    }
    
}

#pragma mark 滑动表格删除聊天记录
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //删除数据库的对应数据
    id  objecct = [self.tableData objectAtIndex:indexPath.row];
    if ([objecct isKindOfClass:[SessionEntity class]]) {
        SessionEntity *entity = objecct;
        if (![entity.sessionID isEqualToString:GROUP_MESSAGE]) {
            MsgReadACKAPI* readACK = [[MsgReadACKAPI alloc] init];
            [readACK requestWithObject:@[entity.sessionID,@(entity.lastMsgID),@(entity.sessionType)] Completion:nil];
            [[SessionModule sharedInstance]removeSessionByServer:entity];
            [self.tableData removeObjectAtIndex:indexPath.row];
            NSUInteger count =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
            [self setToolbarBadge:count];
            [self.tableViewOutlet deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }else{
            [self.tableData removeObjectAtIndex:indexPath.row];
            NSUInteger count =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
            [self setToolbarBadge:count];
            [self.tableViewOutlet deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }else{
        
        [self.tableData removeObjectAtIndex:indexPath.row];
        NSUInteger count =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
        [self setToolbarBadge:count];
        _unreadCount = count;
        [self.tableViewOutlet deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    //发送TCP请求  do any option.....
    
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    HeadView *headView = [HeadView headViewWithTableView:tableViewOutlet];
    headView.backgroundColor = [UIColor whiteColor];
    headView.delegate = self;
    headView.groupModel = friendGroup.friendgroups[section];
    //    [headView setGroupModel:friendGroup.friendgroups[section]];
    [headView setFriendGroup:friendGroup.friendgroups[section]];
    return headView;
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([switching isEqualToString:@"0"]) {
        //消息
        
        return 65;
        
    }else{
        
        //好友
        
        return 62;
    }
    
}


#pragma mark 刷新已经度过的消息
-(void)reloadReadSession:(NSNotification *)noty
{
    SessionEntity * session = noty.object;
    
        //此时刷新页面
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            [self.tableData enumerateObjectsUsingBlock:^(SessionEntity *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([session.sessionID isEqualToString:obj.sessionID]) {
                    obj.unReadMsgCount = 0;
                    NSUInteger count =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
                    _unreadCount = count;

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setToolbarBadge:count];

                        [tableViewOutlet reloadData];
                    });
                }
            }];
        });
    
}

//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //跳转到详细的视图控制器
    if ([switching isEqualToString:@"0"]) {
        id object = [self.tableData objectAtIndex:indexPath.row];
//        if ([object isKindOfClass:[CurrentGroupModel class]]) {
//            //跳转到评论列表
//            UnreadMessagesViewController * readMessages = [UnreadMessagesViewController shareInstance];
//            readMessages.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:readMessages animated:YES];
//            
//            return;
//        }
        //消息
        if (!self.hasCliked) {
            
            InformationTableViewCell * cell = (InformationTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
            [cell.CountButton setHidden:YES];
            [self.tableViewOutlet reloadData];
            NSInteger row = [indexPath row];
            SessionEntity *session = self.tableData[row];

            if ([session.sessionID isEqualToString:GROUP_MESSAGE]) {
                //如果是群的通知，则进入家族和剧的列表
                GroupsKindsViewController * groupViewController = [[GroupsKindsViewController alloc]initWithNibName:@"GroupsKindsViewController" bundle:nil];
                session.unReadMsgCount = 0;
                [[DDDatabaseUtil instance] updateRecentSession:session completion:^(NSError *error) {
                    
                }];
                groupViewController.hidesBottomBarWhenPushed = YES;
                if (groupViewController.isAnimating) {
                    return;
                }
                groupViewController.isAnimating = YES;
                [self.navigationController pushViewController:groupViewController animated:YES];
                self.hasCliked = YES;
            }else{
                //需要区分家族和剧群的聊天消息
                ChatViewController * detailViewController = [ChatViewController shareInstance];
                
                detailViewController.hidesBottomBarWhenPushed=YES;
                if (session.sessionType == 1)
                    //好友聊天
                {
                    detailViewController.isChatInFamily = NO;
                    detailViewController.isInCheckGroup = NO;
                    NSArray * viewControllers = self.navigationController.viewControllers;
                    
                    NSString *havechat=@"0";
                    for (UIViewController * aViewController in viewControllers) {
                        
                        if ([aViewController isKindOfClass:[ChatViewController class]]) {
                            havechat=@"1";
                            
                        }
                    }
                    if ([havechat isEqualToString:@"0"]) {
                        
                        detailViewController.hidesBottomBarWhenPushed=YES;
                        [detailViewController showChattingContentForSession:session];
                        if (detailViewController.isAnimating) {
                            return;
                        }
                        detailViewController.isAnimating = YES;
                        [self.navigationController pushViewController:detailViewController animated:YES];
                        
                        self.hasCliked = YES;
                        
                    } else {
 
                        self.navigationController.navigationBar.hidden=NO;
                        detailViewController.hidesBottomBarWhenPushed=YES;
                        [detailViewController showChattingContentForSession:session];
                        if (detailViewController.isAnimating) {
                            return;
                        }
                        detailViewController.isAnimating = YES;
                        [self.navigationController popToViewController:(ChatViewController*)detailViewController animated:YES];
                        self.hasCliked = YES;
                        
                    }
                    
                }else{
                    //家族或者剧群聊天页面
                    NSArray * grouparr = [session.sessionID componentsSeparatedByString:@"_"];
                    NSString * groupid = [grouparr objectAtIndex:1];
                    [[GetAllFamilysAndStorys shareInstance]getGroupTypeStringgroupid:groupid success:^(id JSON) {
                        if ([JSON isEqualToString:@"1"]) {
                            //家族聊天页面
                            detailViewController.isChatInFamily = YES;
                            detailViewController.checkGroupId = groupid;
                            /*  获取家族成员列表的昵称
                             */
                            [[GetGroupRoleName shareInstance]getFamilyInfo:groupid userid:nil success:^(id JSON) {
                                
                                [[ChatViewController shareInstance].chatTableView reloadData];
                            }];
                            
                            NSArray * viewControllers = self.navigationController.viewControllers;
                            
                            NSString *havechat=@"0";
                            for (UIViewController * aViewController in viewControllers) {
                                
                                if ([aViewController isKindOfClass:[ChatViewController class]]) {
                                    havechat=@"1";
                                    
                                }
                                
                            }
                            
                            if ([havechat isEqualToString:@"0"]) {
                                detailViewController.hidesBottomBarWhenPushed=YES;
                                [detailViewController showChattingContentForSession:session];
                                if (detailViewController.isAnimating) {
                                    return;
                                }
                                detailViewController.isAnimating = YES;
                                [self.navigationController pushViewController:detailViewController animated:YES];
                                self.hasCliked = YES;
                                
                                
                            } else {
                                self.navigationController.navigationBar.hidden=NO;
                                detailViewController.hidesBottomBarWhenPushed=YES;
                                [detailViewController showChattingContentForSession:session];
                                if (detailViewController.isAnimating) {
                                    return;
                                }
                                detailViewController.isAnimating = YES;
                                [self.navigationController popToViewController:(ChatViewController*)detailViewController animated:YES];
                                self.hasCliked = YES;
                            }

                        }else{
                            //剧群聊天页面
                            NSString * storyId = [[GetAllFamilysAndStorys shareInstance]getStoryidByGroupid:groupid];//剧Id
                            NSString * userName  =[[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
                            NSDictionary * storyDic = [[GetAllFamilysAndStorys shareInstance]getStoryDetail:storyId];
                            detailViewController.theartorId = storyId;
                            NSArray * groupsArr = [storyDic objectForKey:@"groups"];
                            detailViewController.isChatInStory = YES;
                            detailViewController.checkGroupId = groupid;
//                            NSString * msgContent = @"";
                            for (NSDictionary * dic in groupsArr) {
                                if ([[dic objectForKey:@"groupid"]isEqualToString:groupid]) {
                                    //进入的是剧的某个群
                                    NSString * isplay = [dic objectForKey:@"isplay"];
                                    if ([isplay isEqualToString:@"0"]) {
                                        //进入的是审核群
                                        detailViewController.isInCheckGroup = YES;
//                                        msgContent = [NSString stringWithFormat:@"师父[%@]正在剧[%@]的审核群中",userName,storyName];
                                    }
                                }
                            }
                            //获取剧群的成员信息
                            [[GetGroupRoleName shareInstance]getGroupRoleName:storyId groupid:groupid success:^(id JSON) {
                                [[ChatViewController shareInstance].chatTableView reloadData];
                            
                            }];
                            NSArray * viewControllers = self.navigationController.viewControllers;
                            
                            NSString *havechat=@"0";
                            for (UIViewController * aViewController in viewControllers) {
                                
                                if ([aViewController isKindOfClass:[ChatViewController class]]) {
                                    havechat=@"1";
                                }
                                
                            }
                            
                            if ([havechat isEqualToString:@"0"]) {
                                detailViewController.hidesBottomBarWhenPushed=YES;
                                [detailViewController showChattingContentForSession:session];
                                if (detailViewController.isAnimating) {
                                    return;
                                }
                                detailViewController.isAnimating = YES;
                                [self.navigationController pushViewController:detailViewController animated:YES];
                                self.hasCliked = YES;
                            } else {
                                self.navigationController.navigationBar.hidden=NO;
                                detailViewController.hidesBottomBarWhenPushed=YES;
                                [detailViewController showChattingContentForSession:session];
                                if (detailViewController.isAnimating) {
                                    return;
                                }
                                detailViewController.isAnimating = YES;
                                [self.navigationController popToViewController:(ChatViewController*)detailViewController animated:YES];
                                self.hasCliked = YES;
                                
                            }
                        }
                        
                    }];
                }

            }
            
        }
        
    }else{
        
        //        NSDictionary * friends = [myFriends objectAtIndex:indexPath.section];
        //
        //        NSArray * friendArr = [friends objectForKey:@"friends"];
        
        GroupModel * groupModel = friendGroup.friendgroups[indexPath.section];
        NSArray * friendMembers = groupModel.groupmembers;
        FriendModel * fModel = friendMembers[indexPath.row];
        
        NSString * FriendID = fModel.userid;
        //TODO(Xc.)这里注释了
        YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
        [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
        [detailViewController setFriendModel:fModel groupModel:groupModel fgModel:friendGroup];
        detailViewController.hidesBottomBarWhenPushed = YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
        
        //        //也是跳转到聊天页面，需要判断之前是否和对方说过话，如果有发过消息，直接进入聊天页面，否则先加载历史历史聊天记录
        //        [self determineHasChatted:indexPath];
    }
    //取消选中的行
    
    [self.tableViewOutlet deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark 添加置顶按钮
-(NSArray *)tableView:(UITableView*)tableView editActionsForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    topSessionsArr = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:TOPSESSIONS]];
    
    if (!topSessionsArr) {
        topSessionsArr = [NSMutableArray arrayWithCapacity:10];
    }
    //先判断是否是系统消息
      id  object = [self.tableData objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[SessionEntity class]]) {
        SessionEntity * entity = object;
        if (![entity.sessionID isEqualToString:GROUP_PRE]) {
            //不是系统消息才可以有删除和置顶功能
            UITableViewRowAction * deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
                
                        MsgReadACKAPI* readACK = [[MsgReadACKAPI alloc] init];
                        [readACK requestWithObject:@[entity.sessionID,@(entity.lastMsgID),@(entity.sessionType)] Completion:nil];
                        [[SessionModule sharedInstance]removeSessionByServer:entity];
                        [self.tableData removeObjectAtIndex:indexPath.row];
                        NSUInteger count =  [[self.tableData valueForKeyPath:@"@sum.unReadMsgCount"] integerValue];
                        [self setToolbarBadge:count];
                        [self.tableViewOutlet deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                
                [topSessionsArr removeObject:entity.sessionID];
                
    
                [[NSUserDefaults standardUserDefaults] setObject:topSessionsArr forKey:TOPSESSIONS];

            }];
            
            //先判断该消息是否已经置顶，已经置顶title就是取消置顶
            UITableViewRowAction * topRowAction = nil;
            
            if ([topSessionsArr containsObject:entity.sessionID]) {
                topRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"取消置顶" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
                    //取消置顶

                    [topSessionsArr removeObject:entity.sessionID];
                    [[NSUserDefaults standardUserDefaults] setObject:topSessionsArr forKey:TOPSESSIONS];
                    [self sortAllSessions];
//                    [self.tableViewOutlet reloadData];
                }];
            }else{
                //没有置顶
                topRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"置顶" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
                    
                    [topSessionsArr insertObject:entity.sessionID atIndex:0];
//                    [self.tableViewOutlet reloadData];

                    [[NSUserDefaults standardUserDefaults] setObject:topSessionsArr forKey:TOPSESSIONS];
//                    NSIndexPath * firstIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//                    [tableView moveRowAtIndexPath:indexPath toIndexPath:firstIndexPath];
                    [self sortAllSessions];

                }];
            }
            topRowAction.backgroundColor = [UIColor colorWithHexString:@"#c5c5c6"];
        
            return @[deleteRowAction,topRowAction];
        }else{
            return nil;
        }
    }else{
        return nil;
    }
    
    //增加一个置顶按钮

    
}

- (void)clickHeadView
{
    [self.tableViewOutlet reloadData];
}

#pragma mark 管理分组
-(void)manageGroup:(UILongPressGestureRecognizer *)longPress
{
    CGPoint locationPoint = [longPress locationInView:self.tableViewOutlet];
    UIMenuController  * menu = [UIMenuController  sharedMenuController];
    UIMenuItem   *menuItem1 = [[UIMenuItem alloc]initWithTitle:@"分组管理" action:@selector(manageGroup)];
    menu.menuItems = [NSArray arrayWithObjects:menuItem1,nil];
    [menu setTargetRect:CGRectMake(locationPoint.x - 60, locationPoint.y-50 , 120, 44) inView:self.tableViewOutlet];
    [tableViewOutlet becomeFirstResponder];
    [menu setMenuVisible:YES animated:YES];
    GroupManageController * groupManager = [[GroupManageController alloc]init];
    groupManager.hidesBottomBarWhenPushed = YES;
    [groupManager setContent:friendGroup];
    if (groupManager.isAnimating) {
        return;
    }
    groupManager.isAnimating = YES;
    [self.navigationController pushViewController:groupManager animated:YES];
    
}

-(void)manageGroup
{
    GroupManageController * groupManager = [[GroupManageController alloc]init];
    groupManager.hidesBottomBarWhenPushed = YES;
    [groupManager setContent:friendGroup];
    if (groupManager.isAnimating) {
        return;
    }
    groupManager.isAnimating = YES;
    [self.navigationController pushViewController:groupManager animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
