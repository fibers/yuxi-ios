//
//  GroupsKindsViewController.h
//  inface
//
//  Created by appleone on 15/6/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//系统消息
#import "BaseViewController.h"
#import "NewFriendTableViewCell.h"
@interface GroupsKindsViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;
@property (assign,nonatomic)  BOOL  isAnimating;
-(void)selectAddMasterState;
@end
