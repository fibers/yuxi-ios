//
//  FriendHeaderViewCell.h
//  inface
//
//  Created by appleone on 15/9/8.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

@protocol ManageFriendGroupDelegate <NSObject>

-(void)manageGroup;

@end
#import <UIKit/UIKit.h>

@interface FriendHeaderViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *headButtn;
@property (weak,nonatomic) id<ManageFriendGroupDelegate>delegate;
@end
