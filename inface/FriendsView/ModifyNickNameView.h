//
//  ModifyNickNameView.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FriendModel;
@interface ModifyNickNameView : UIView
{
    FriendModel * fModel;
}
@property(strong,nonatomic)UITextField * nameField;

-(void)setContent:(FriendModel *)model;
@end
