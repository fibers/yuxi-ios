//
//  MyFamilyViewController.h
//  inface
//
//  Created by huangzengsong on 15/7/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "FamilyAllTableViewCell.h"

#import "FamilyInformationViewController.h"


@interface MyFamilyViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,EGORefreshTableHeaderDelegate,EGORefreshTableFootDelegate,SessionModuelDelegate>
{
    
    
    EGORefreshTableHeaderView *_refreshHeaderView;
    EGORefreshTableFootView* _footerView;
    //  Reloading var should really be your tableviews datasource
    //  Putting it here for demo purposes
    BOOL _reloading;
    BOOL _headerRefresh;
    
    NSString *count;
    NSString *fromid;
    NSString *direction;
}
+(instancetype)shareInstance;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong,nonatomic) NSMutableArray * familyList;//我的家族列表

@property (assign,nonatomic) BOOL  isAnimating;
-(void)getMyFamilies;

-(void)agreeByFamilyLeader:(NSString *)familyid;//被家族同意后重新获取我的家族列表

@end
