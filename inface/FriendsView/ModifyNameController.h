//
//  ModifyNameController.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
typedef void(^returnMarkName)(NSString *showText);
#import <UIKit/UIKit.h>
@class FriendModel;

@interface ModifyNameController : BaseViewController

@property(nonatomic,copy) returnMarkName  returnTextBlock;
-(void)setFriendModel:(FriendModel *)fModel;
-(void)returnText:(returnMarkName)block;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
