//
//  ModifyFriendController.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ModifyFriendController.h"
#import "ReportFriendViewController.h"
#import "ModifyTableViewCell.h"
#import "ModifyNameController.h"
#import "SelectGroupController.h"
#import "FriendGroupModel.h"
@interface ModifyFriendController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView * tableView;
    FriendModel * model;
    GroupModel * gModel;
    FriendGroupModel *fgModel;
    returnGroupModel groupBlock;
}

@end

@implementation ModifyFriendController
//好友的更多页面
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"更多";
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"举报" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    tableView.delegate = self;
    tableView.dataSource = self;
    UIView * aview = [[UIView alloc]init];
    tableView.tableFooterView = aview;
    [self.view addSubview:tableView];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)returnBackl
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)returnBackr
{
    //举报
    ReportFriendViewController * detailViewController = [[ReportFriendViewController alloc] init];
    detailViewController.hidesBottomBarWhenPushed=YES;
    detailViewController.reportid = model.userid;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;

    [self.navigationController pushViewController:detailViewController animated:YES];
}


-(void)setFriendModel:(FriendModel *)fModel groupModel:(GroupModel *)groupModel fgModel:(FriendGroupModel *)friendgModel
{
    model = fModel;
    gModel = groupModel;
    fgModel = friendgModel;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"ModifyTableViewCell";
    
    ModifyTableViewCell * cell = [tableView  dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ModifyTableViewCell" owner:nil options:nil]firstObject];
    }
    if (indexPath.row == 0) {
        cell.groupName.text = @"备注";
        cell.detailLabel.text = model.markname;
    }else{
        cell.groupName.text = @"好友分组";
        cell.detailLabel.text = gModel.groupname;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        ModifyNameController * nameViewController = [[ModifyNameController alloc]init];
        [nameViewController setFriendModel:model];
        NSString * nickname = model.markname;
        nameViewController.hidesBottomBarWhenPushed = YES;
        [nameViewController returnText:^(NSString *showText) {
            model.markname = showText;
            [tableView reloadData];
        }];
        if (nameViewController.isAnimating) {
            return;
        }
        nameViewController.isAnimating = YES;
        [self.navigationController pushViewController:nameViewController animated:YES];
    }else{
        SelectGroupController * nameViewController = [[SelectGroupController alloc]init];
        nameViewController.hidesBottomBarWhenPushed = YES;
        [nameViewController returnText:^(GroupModel *groupModel,FriendGroupModel* fg_model) {
            
            gModel = groupModel;
            fgModel = fg_model;
            [tableView reloadData];
            groupBlock(groupModel,fgModel);
        }];
        if (nameViewController.isAnimating) {
            return;
        }
        nameViewController.isAnimating = YES;
        [nameViewController setContent:gModel friendModel:model fgModel:fgModel];
        [self.navigationController pushViewController:nameViewController animated:YES];
        
    }
}

-(void)returnGroupModel:(returnGroupModel)block
{
    groupBlock = block;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
