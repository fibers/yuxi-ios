//
//  AgreeMasterCell.h
//  inface
//
//  Created by appleone on 15/12/7.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgreeMasterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *portraitButton;

@property (weak, nonatomic) IBOutlet UILabel *detailTextLal;
@property (weak, nonatomic) IBOutlet UIButton *AgreeButton;

@property (weak, nonatomic) IBOutlet UIButton *rejectButton;
@end
