//
//  InformationTableViewCell.m
//  inface
//
//  Created by Mac on 15/4/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "InformationTableViewCell.h"
#import "SavePersonPortrait.h"
#import "CurrentGroupModel.h"
#import "NSString+YXExtention.h"
@interface InformationTableViewCell()
@property(nonatomic,strong)UIImageView * countImageView;
@property (weak, nonatomic) IBOutlet UIImageView *msgContIma;
@property (weak, nonatomic) IBOutlet UILabel *msgCountLab;
@property (weak, nonatomic) IBOutlet UIButton *btn;

@property(nonatomic,strong)UILabel  * countLabel;

@property (weak, nonatomic) IBOutlet UIButton *countBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnHeightConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnWidthConstant;

@end
@implementation InformationTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    _UserDetailLabel.textColor=BLUECOLOR;
    _TimeLabel.textColor=GRAY_COLOR;
    
}

#pragma mark - public
- (void)setName:(NSString*)name
{
    if (!name)
    {
        [_UserNameLabel setText:@""];
    }
    else
    {
        [_UserNameLabel setText:name];
    }
}

- (void)setTimeStamp:(NSUInteger)timeStamp
{
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    NSString* dateString = [date transformToFuzzyDate];
    [_TimeLabel setText:dateString];
}

- (void)setLastMessage:(NSString*)message
{
    if (!message)
    {
        [_UserDetailLabel setText:@"."];
    }
    else
    {
        [_UserDetailLabel setText:message];
    }
}

- (void)setAvatar:(NSString*)avatar placeholderImage:(UIImage *)placeholder
{
    
    [[_UserImage subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [(UIView*)obj removeFromSuperview];
    }];
    
    NSURL* avatarURL = [NSURL URLWithString:[avatar stringOfW100ImgUrl]];
    _UserImage.layer.cornerRadius = CGRectGetHeight(_UserImage.bounds)/2;
    _UserImage.contentMode=UIViewContentModeScaleAspectFill;
    _UserImage.clipsToBounds=YES;
    
    [_UserImage sd_setImageWithURL:avatarURL placeholderImage:placeholder];
}

- (void)setUnreadMessageCount:(NSUInteger)messageCount
{
    NSString * imsCount = [NSString stringWithFormat:@"%ld",messageCount];
    _msgCountLab.textAlignment = NSTextAlignmentCenter;
    if (messageCount == 0)
    {
//        _msgContIma.hidden = YES;
//        _btn.hidden = YES;
//        _msgCountLab.hidden = YES;
        _countBtn.hidden = YES;
        return;
    }
    
    else if (messageCount < 10)
    {
//        _msgContIma.hidden = NO;
//        
//        _msgCountLab.hidden = NO;
//        
//        _msgContIma.frame = CGRectMake(37, 4, 8, 8);
//        _msgContIma.image = [UIImage imageNamed:@"redRect"];
//
//        _msgCountLab.text = imsCount;
        [_countBtn setTitle:imsCount forState:UIControlStateNormal];

        _countBtn.hidden = NO;
       
        [_countBtn setBackgroundImage:[UIImage imageNamed:@"redRect"] forState:UIControlStateNormal];

        _btnWidthConstant.constant = 16;
        
        _btnHeightConstant.constant = 16;

        _countBtn.layer.cornerRadius = 8;
        
        _countBtn.clipsToBounds = YES;
     }
    else if (messageCount < 99 && messageCount >= 10)
    {
//        _msgContIma.hidden = NO;
//        
//        _msgCountLab.hidden = NO;
//        
//        _msgContIma.frame = CGRectMake(37, 4, 15, 9);
//
//        _msgContIma.image = [UIImage imageNamed:@"zs_hongdian"];
//        _msgCountLab.text = imsCount;
        [_countBtn setTitle:imsCount forState:UIControlStateNormal];
        [_countBtn setBackgroundImage:[UIImage imageNamed:@"zs_hongdian"] forState:UIControlStateNormal];

        _countBtn.hidden = NO;
        _countBtn.clipsToBounds = NO;

        _btnWidthConstant.constant = 22;
        
       _btnHeightConstant.constant = 16;

    }
    else
    {
//        _msgContIma.hidden = NO;
//        
//        _msgCountLab.hidden = NO;
//        
//        _msgContIma.frame = CGRectMake(37, 4, 18, 9);
//
//        _msgContIma.image = [UIImage imageNamed:@"bigNum"];
//        
//        _msgCountLab.text = imsCount;
        
        [_countBtn setTitle:imsCount forState:UIControlStateNormal];

//        _countBtn.layer.cornerRadius = 12;

        _countBtn.hidden = NO;
        
        _countBtn.clipsToBounds = NO;

        [_countBtn setBackgroundImage:[UIImage imageNamed:@"bigNum"] forState:UIControlStateNormal];
        
        _btnWidthConstant.constant = 28;
        
        _btnHeightConstant.constant = 18;

    }}


-(void)setCurrentSession:(SessionEntity*)session storyDic:(NSDictionary*)dic;//评论群聊天列表
{
    self.vipWriterImage.hidden  =YES;
    [self setUnreadMessageCount:session.unReadMsgCount];
    [self setTimeStamp:session.timeInterval];
    [self setName:[dic objectForKey:@"title"]];
    [self setLastMessage:session.lastMsg ];
    UIImage* placeholder = [UIImage imageNamed:@"zs_touxianglanfamily"];
    [self setAvatar:[dic objectForKey:@"storyimg"] placeholderImage:placeholder];
}


//-(void)setCurrentGroupsPortrait//评论群单元格
//{
//    [self setName:@"评论消息"];
//    [self layoutIfNeeded];
//
//    self.UserImage.image = [UIImage imageNamed:@"currentMessages"];
//    self.UserNameLabel.frame = CGRectMake(75, 20, 165, 25);
//    self.UserDetailLabel.text = nil;
//    self.vipWriterImage.hidden = YES;
//    [self setUnreadMessageCount:[[[CurrentGroupModel sharedInstance].currentArr valueForKeyPath:@"@sum.unReadMsgCount"]integerValue]];
//    
//}

-(void)setShowSession:(SessionEntity *)session
{
    [self setUnreadMessageCount:session.unReadMsgCount];
    [self setTimeStamp:session.timeInterval];
    NSString * portrait = session.sessionID;
    NSArray * ids = [session.sessionID componentsSeparatedByString:@"_"];
    NSString * user_id =@"";
    if (ids.count > 1) {
        user_id = ids[1];
    }else{
        user_id = ids[0];
    }
    if ([session.lastMsg isKindOfClass:[NSString class]]) {
        if ([session.lastMsg rangeOfString:DD_MESSAGE_IMAGE_PREFIX].location != NSNotFound) {
            NSArray *array = [session.lastMsg componentsSeparatedByString:DD_MESSAGE_IMAGE_PREFIX];
            NSString *string = [array lastObject];
            if ([string rangeOfString:DD_MESSAGE_IMAGE_SUFFIX].location != NSNotFound) {
                [self setLastMessage:@"[图片]"];
            }else{
                [self setLastMessage:string];
            }
            
        }else if ([session.lastMsg hasSuffix:@".spx"])
        {
            [self setLastMessage:@"[图片]"];
        }
        else{

            [self setLastMessage:session.lastMsg];
            
        }
    }
    if ([session.sessionID isEqualToString:GROUP_MESSAGE]) {
        
        self.vipWriterImage.hidden = YES;
        [self setName:@"系统消息"];
        //设定默认图像
        [self layoutIfNeeded];

       self.UserImage.image = [UIImage imageNamed:@"groupMessage"];
        self.UserNameLabel.frame = CGRectMake(75, 20, 165, 25);
        self.UserDetailLabel.text = nil;
        
    }else{
    
    if (session.sessionType == SessionTypeSessionTypeSingle) {

//        [self setName:session.name];

        [[DDUserModule shareInstance] getUserForUserID:session.sessionID Block:^(DDUserEntity *user) {
            NSString * markName = user.nick;
            
            NSString * realName = user.name;
            
            if (markName && ![markName isEqualToString:@""]) {
                [self setName:markName];
            }else{
                [self setName:realName];
            }
                
          
            UIImage* placeholder = [UIImage imageNamed:@"zs_touxianglan.png"];
            NSString * porUrl = user.avatar;
            
            int isVip = [porUrl isSignedUser];
            UIImage * image = [UIImage imageNamed:@"vipWriter"];
            UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
            switch (isVip) {
                case 0:
                    self.vipWriterImage.hidden = YES;
                    break;
                case 1:
                    self.vipWriterImage.hidden = NO;
                    
                    self.vipWriterImage.image = image;
                    
                    break;
                case 2:
                    self.vipWriterImage.hidden = NO;
                    
                    self.vipWriterImage.image = officalImage;
                    
                    break;
                case 3:
                    self.vipWriterImage.hidden = NO;
                    
                    self.vipWriterImage.image = officalImage;
                    
                    break;
                default:
                    break;
            }
            [self setAvatar:porUrl placeholderImage:placeholder];
//           [[ SavePersonPortrait shareInstance]getPersonNickName:user_id success:^(id JSON) {
//               [self setName:JSON];
//               
//           } failure:^(id json) {
//               [self setName:user.nick];
//               
//           }];
            
        }];

//        [[SavePersonPortrait shareInstance]getPortraitImage:portrait success:^(id JSON) {
//            UIImage* placeholder = [UIImage imageNamed:@"zs_touxianglan.png"];
//            NSString * porUrl = (NSString*)JSON;
//            [self setAvatar:porUrl placeholderImage:placeholder];
//
//        }];
    }else if (session.sessionType == SessionTypeSessionTypeGroup){
        self.vipWriterImage.hidden = YES;//签约写手的标志隐藏
        NSArray * groupArr = [session.sessionID componentsSeparatedByString:@"_"];
        NSString * groupid = [groupArr objectAtIndex:1];
        [[GetAllFamilysAndStorys shareInstance]getGroupTypeStringgroupid:groupid success:^(id JSON) {
            //根据json类型判断是家族还是剧群
            if ([JSON isEqualToString:@"1"]) {
                //家族信息
                [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:groupid success:^(id JSON) {
                    UIImage* placeholder = [UIImage imageNamed:@"zs_touxianglanfamily"];
                    [self setAvatar:[JSON objectForKey:@"portrait"] placeholderImage:placeholder];
                    [self setName:[JSON objectForKey:@"name"]];
                }failure:^(id Json) {
                    
                    
                }];
            }else{
               __block NSString * storyName;
                [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:groupid success:^(id JSON) {
                    UIImage* placeholder = [UIImage imageNamed:@"zs_touxianglanfamily"];
                    [self setAvatar:[JSON objectForKey:@"portrait"] placeholderImage:placeholder];
                    storyName =  [JSON objectForKey:@"title"];
                }failure:^(id Json) {
                    
                    
                }];
                NSDictionary * groupDetail = [[GetAllFamilysAndStorys shareInstance]getStoryGroup:groupid];
                NSString * groupName  = [groupDetail objectForKey:@"title"];
                NSString * blendName = [NSString stringWithFormat:@"[%@]%@",storyName,groupName];
                [self setName:blendName];
            }
            
        }];
 

    }
    
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
