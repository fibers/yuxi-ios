//
//  MyFamilyViewController.m
//  inface
//
//  Created by huangzengsong on 15/7/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MyFamilyViewController.h"
#import "GetGroupRoleName.h"
#import "ChattingModule.h"
#import "NSString+YXExtention.h"
@interface MyFamilyViewController ()

@end

@implementation MyFamilyViewController

+(instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    static MyFamilyViewController *ashareInstance = nil;
    dispatch_once(&onceToken, ^{
        ashareInstance = [MyFamilyViewController new];
    });
    return ashareInstance;
    
}

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.navigationItem.title=@"我的家族";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 12, 0, 12)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 12, 0, 12)];
    }

    self.tableView.separatorColor=XIAN_COLOR;

    //此时获取我得家族列表
    [self getMyLocalFamilys];
    
    count=@"0";
    fromid=@"0";
    direction=@"0";
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getMyFamilies) name:@"CreateMyFamilies" object:nil];
    
    _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height)];
    _refreshHeaderView.delegate = self;
    [self.tableView addSubview:_refreshHeaderView];
    
    
    _footerView = [[EGORefreshTableFootView alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width, 0)];
    _footerView.delegate = self;
    self.tableView.tableFooterView = _footerView;
    
    [_refreshHeaderView refreshLastUpdatedDate];
    
}



#pragma mark - 下拉刷新
- (void)finishRefresh{
    //LOG_METHOD;
    
    [self.tableView reloadData];
    [_footerView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    _reloading = NO;
}

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view{
    //LOG_METHOD;
    _reloading = YES;
    
    [self getMyFamilies];
    [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(finishRefresh) userInfo:nil repeats:NO];
    
}


- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view{
    //   NSLog(@"isLoading");
    //LOG_METHOD;
    return _reloading;
}

- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view{
    //LOG_METHOD;
    return [NSDate date];
}

#pragma mark - Footer Refresh delegate methods

- (void)egoRefreshTableFootDidTriggerRefresh:(EGORefreshTableFootView*)view{
    //LOG_METHOD;
    _reloading = YES;

    [self getMyFamilies];
    
    [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(finishRefresh) userInfo:nil repeats:NO];
    
}
- (BOOL)egoRefreshTableFootDataSourceIsLoading:(EGORefreshTableFootView*)view{
    //LOG_METHOD;
    return _reloading;
    
}

- (NSDate*)egoRefreshTableFootDataSourceLastUpdated:(EGORefreshTableFootView*)view{
    //LOG_METHOD;
    return  [NSDate date];
}


#pragma mark - scroll delegate methods

//scrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //LOG_METHOD;
    
    CGPoint offset1 = scrollView.contentOffset;
    CGRect bounds1 = scrollView.bounds;
    UIEdgeInsets inset1 = scrollView.contentInset;
    float y1 = offset1.y + bounds1.size.height - inset1.bottom;
    
    if (y1 > self.tableView.frame.size.height) { //判断是下拉刷新 还是上拉加载
        _headerRefresh = NO;    //上拉加载
        [_footerView egoRefreshScrollViewDidScroll:scrollView];
    }
    else if (y1 < self.tableView.frame.size.height) {
        _headerRefresh = YES;   //下拉刷新
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
    else if (y1 == self.tableView.frame.size.height) {
        //        DLog(@"%@", flagShuaxin ? @"上拉刷新" : @"下拉刷新");
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    //LOG_METHOD;
    
    if (YES == _headerRefresh ) { //下拉刷新
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    else{ //上拉加载更多
        [_footerView egoRefreshScrollViewDidEndDragging:scrollView];
    }
}

#pragma 从本地获取我的家族列biao
-(void)getMyLocalFamilys
{
    self.familyList = [[GetAllFamilysAndStorys shareInstance]type:@"1"];
    if (self.familyList.count==0) {
        [SVProgressHUD showSuccessWithStatus:@"您还没有自己的家族哦！快去创建或者加入一个吧！" duration:2.0];
    }
    [self.tableView reloadData];
}


#pragma mark 获取我得家族列表
-(void)getMyFamilies
{

    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params =@{@"uid":userid,@"token":@"110"};
    //获取我的家族
    [HttpTool postWithPath:@"GetMyFamilyList" params:params success:^(id JSON) {

        NSMutableArray *  arr   =[JSON objectForKey:@"familylist"];
        //更新本地
        if (arr.count == 0) {
            return ;
        }
        [[GetAllFamilysAndStorys shareInstance]updateFamilysOrSto:arr typestr:@"1"];
        
        self.familyList = [JSON objectForKey:@"familylist"];
        
        if (self.familyList.count==0) {
            [SVProgressHUD showSuccessWithStatus:@"您还没有自己的家族哦！快去创建或者加入一个吧！" duration:2.0];
        }

        [self.tableView reloadData];
    } failure:^(NSError *error) {
        
        
    }];
}

#pragma mark 被族长同意后，获取我的家族列表，并在群里发一条消息

-(void)agreeByFamilyLeader:(NSString *)familyid;//被家族同意后重新获取我的家族列表
{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params =@{@"uid":userid,@"token":@"110"};
    //获取我的家族
    [HttpTool postWithPath:@"GetMyFamilyList" params:params success:^(id JSON) {
        
        NSMutableArray *  arr   =[JSON objectForKey:@"familylist"];
        //更新本地
        if (arr.count == 0) {
            return ;
        }
        [[GetAllFamilysAndStorys shareInstance]updateFamilysOrSto:arr typestr:@"1"];
        
        self.familyList = [JSON objectForKey:@"familylist"];
        
        //获取家族的成员详情,将家族成员信息存放到本地
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        NSDictionary * params = @{@"uid":userid,@"familyid":familyid,@"token":@"110"};
        [HttpTool postWithPath:@"GetFamilyInfo" params:params success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]integerValue] == 1) {
                NSDictionary * dic = [JSON objectForKey:@"familyinfo"];
                [[GetGroupRoleName shareInstance]saveFamily:familyid membersArr:[dic objectForKey:@"members"]];
                if (familyid) {
                    NSString *sessionid = [NSString stringWithFormat:@"group_%@",familyid];
                    SessionEntity * session = [[SessionModule sharedInstance]getSessionById:sessionid];
                    if (!session) {
                        session  = [[SessionEntity alloc]initWithSessionID:sessionid type:SessionTypeSessionTypeGroup];
                    }

                    NSString * msgContent = [[NSUserDefaults standardUserDefaults]objectForKey:@"nickname"];
                    NSString * msgtext = [NSString stringWithFormat:@"%@已经是家族成员了",msgContent];
                    DDMessageContentType msgContentType = DDMessageTypeText;
                    session.unReadMsgCount = 1;
                    ChattingModule * module = [[ChattingModule alloc]init];
                    module.sessionEntity = session;
                    DDMessageEntity * message = [DDMessageEntity makeMessage:msgtext Module:module MsgType:msgContentType isSendImage:NO isCurrentGroup:NO];
                    [[DDMessageSendManager instance]sendMessage:message isGroup:YES Session:session completion:^(DDMessageEntity *message, NSError *error) {
                        
                        
                    } Error:^(NSError *error) {
                        
                        
                    }];
//                    [[ChatViewController shareInstance]sendMessage:msgtext messageEntity:message];
//                    [ChatViewController shareInstance].module.sessionEntity = nil;
                    
                }
            }
            
        } failure:^(NSError *error) {
            
            
        }];
        
//获取新家族的信息，将家族成员数组存放在本地
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        
        
    }];

        //以自己的身份在群里说一句话
  
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return self.familyList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"FamilyAllTableViewCell";
    FamilyAllTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyAllTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }

    NSURL* url = [NSURL URLWithString:[[[self.familyList objectAtIndex:indexPath.row] objectForKey:@"portrait"] stringOfW100ImgUrl]];
    [cell.UserImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"zs_touxianglanfamily"] completed:nil];
    cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;
    cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
    cell.UserImage.clipsToBounds=YES;
    NSString *isvipfamliy=[[self.familyList objectAtIndex:indexPath.row] objectForKey:@"isvip"];
    if ([isvipfamliy isEqualToString:@"1"]) {
        UIImageView *vipimageView=[[UIImageView alloc]initWithFrame:CGRectMake(cell.UserImage.frame.origin.x-1.5, cell.UserImage.frame.origin.y -1.5, 46, 46)];
        vipimageView.image=[UIImage imageNamed:@"isvipfamliy"];
        vipimageView.userInteractionEnabled=YES;
        [cell addSubview:vipimageView];
    }
    
    cell.TitleLabel.text=[[[self.familyList objectAtIndex:indexPath.row] objectForKey:@"name"] description];

    return cell;

    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 62;
}


//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器

    
    //是家族的群
    FamilyInformationViewController * detailViewController = [[FamilyInformationViewController alloc] init];
    NSString * familyid = [[self.familyList objectAtIndex:indexPath.row]objectForKey:@"id"];
    detailViewController.familyid = familyid;
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
