//
//  FriendGroupCell.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "FriendGroupCell.h"

@implementation FriendGroupCell
{
    GroupModel * model;
}
- (IBAction)deleteFriendBtn:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(deleteGroup:indexPath:)]) {
        [self.delegate deleteGroup:model.groupid indexPath:self.index];
    }

}

- (void)awakeFromNib {
    // Initialization code
}

#pragma mark  设置单元格的内容
-(void)setContent:(GroupModel *)groupModel indexPath:(NSIndexPath *)index
{
    self.groupNameLabel.text = groupModel.groupname;
    self.index = index;
    model = groupModel;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
