//
//  GroupView.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "GroupView.h"

@implementation GroupView
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        
    }
    
    return self;
}


- (IBAction)addGroupBtn:(id)sender
{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"添加分组" message:@"请为新分组输入名称" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder= @"分组名称在20字以内";
    alert.tag = 10000;
    [alert show];
    
}


#pragma mark UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10000) {
        if (buttonIndex == 1 && [alertView textFieldAtIndex:0].text.length > 0 && [alertView textFieldAtIndex:0].text.length <= 20) {
            //此时添加好友分组
            if ([self.delegate respondsToSelector:@selector(addGroup:)]) {
                [ self.delegate addGroup:[alertView textFieldAtIndex:0].text];

            }
            
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
