//
//  FriendGroupModel.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@interface FriendGroupModel : NSObject
+(instancetype)shareInstance;
@property (nonatomic, copy) NSString *result;

@property (nonatomic, strong) NSArray *friendgroups;


@end
