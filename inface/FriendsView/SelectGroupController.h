//
//  SelectGroupController.h
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "SelectGroupView.h"
@class FriendModel;

@class GroupModel;

@class FriendGroupModel;
@class FriendGroupModel;
typedef void(^returnGroupName)(GroupModel * groupModel,FriendGroupModel * fg_Model);

@interface SelectGroupController : BaseViewController<ManagerGroupDelegate>
//@property(nonatomic,strong)GroupModel * groupModel;
//@property(strong,nonatomic)FriendModel * friendModel;
@property(assign,nonatomic)    BOOL   isAnimating;

-(void)setContent:(GroupModel *)groupModel  friendModel:(FriendModel*)friendModel fgModel:(FriendGroupModel*)friendgModel;
-(void)returnText:(returnGroupName)block;
@end
