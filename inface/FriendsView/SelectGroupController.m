//
//  SelectGroupController.m
//  inface
//
//  Created by appleone on 15/8/29.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SelectGroupController.h"
#import "FriendGroupModel.h"
#import "GroupMessageTableViewCell.h"
#import "FriendModel.h"
#import "GroupModel.h"
#import "FriendGroupModel.h"
#import "SelectGroupView.h"
@interface SelectGroupController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    UITableView * tableView;
    FriendModel * fModel;
    GroupModel  * gModel;
    FriendGroupModel * fgModel;
    SelectGroupView *selectGroup;
    NSString        * currentString;
    returnGroupName  returnBlock;
}
@end

@implementation SelectGroupController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"好友分组";
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    selectGroup = [[[NSBundle mainBundle]loadNibNamed:@"SelectGroupView" owner:nil options:nil]firstObject];
    selectGroup.frame = CGRectMake(0, 14, ScreenWidth, 44);
//    selectGroup.backgroundColor = [UIColor colorWithHexString:@"#f9f9f9"];
//    [self.view addSubview:selectGroup];
    
    UIView * headView = [[UIView alloc]initWithFrame:CGRectMake(0, 14, ScreenWidth, 44)];
    headView.backgroundColor = [UIColor colorWithHexString:@"#f9f9f9"];
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(20, 14, 160, 20)];
    label.text = @"添加到新分组";
    [label setTextColor:[UIColor colorWithHexString:@"#4a4a4a"]];
    [label setFont:[UIFont systemFontOfSize:15.0]];
    [headView addSubview:label];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = headView.bounds;
    [button addTarget:self action:@selector(addNewGroup:) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:button];
    
    UIImage * image = [UIImage imageNamed:@"zs_jiantou"];
    
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 30, 6, 31, 31)];
    imageView.image = image;
    [headView addSubview:imageView];
    
    UILabel * lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 44, ScreenWidth, 1)];
    lineLabel.backgroundColor = [UIColor colorWithHexString:@"#c5c5c6"];
    [headView addSubview:lineLabel];
    
    [self.view addSubview:headView];
    tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 72, ScreenWidth, ScreenHeight-150)];
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    tableView.delegate = self;
    tableView.dataSource = self;
    UIView * aview = [[UIView alloc]init];
    tableView.tableFooterView = aview;
    [self.view addSubview:tableView];
    [tableView reloadData];
    self.view.backgroundColor = [UIColor whiteColor];

    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setContent:(GroupModel *)groupModel  friendModel:(FriendModel*)friendModel fgModel:(FriendGroupModel*)friendgModel
{
    fModel = friendModel;
    gModel = groupModel;
    fgModel = friendgModel;
    currentString = gModel.groupid;
}

-(void)returnBackl
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addNewGroup:(UIButton *)sender
{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"添加分组" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"添加", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder= @"分组名称在10字以内";
    
    alert.tag = 10000;
    [alert show];
}

#pragma mark UIAlertView  Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10000) {
        NSString * text = [alertView textFieldAtIndex:0].text;
        if (text.length > 0 && text.length <= 10 && buttonIndex == 1) {
            [self addGroup:text];
            [alertView resignFirstResponder];
        }
    }
    if (buttonIndex == 0) {
        [alertView resignFirstResponder];
    }
}

-(void)addGroup:(NSString *)groupName
{
    //创建分组，并将好友加到分组里
    NSDictionary * params = @{@"uid":USERID,@"friendgroupname":groupName};
    [HttpTool postWithPath:@"CreateFriendsGroup" params:params success:^(id JSON) {
        //创建成功，更新modek的数结构，刷新表格,需要重新获取分组
        if ([[JSON objectForKey:@"result"]intValue] > 0 ) {
            NSDictionary * dic = @{@"uid":USERID,@"token":@"110"};
            [HttpTool postWithPath:@"GetFriendListWithGroup" params:dic success:^(id JSON) {
//                FriendGroupModel * model = [[FriendGroupModel alloc]init];
                fgModel = [FriendGroupModel objectWithKeyValues:JSON];
                
                GroupModel * lastModel = [fgModel.friendgroups lastObject];
                NSString * newGroupId = lastModel.groupid;
                
                NSDictionary * params = @{@"uid":USERID,@"orignfriendgroupid":gModel.groupid,@"newfriendgroupid":newGroupId,@"frienduserid":fModel.userid, @"token":@"110"};
                [HttpTool postWithPath:@"ChangeFriendsGroupMember" params:params success:^(id JSON) {
                    if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
                        currentString = newGroupId;
                        [tableView reloadData];
                        if (returnBlock) {
                            returnBlock(lastModel,fgModel);
                        }
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"DELETEGROUP" object:nil];

                    }
                    UIAlertView * alrt = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles: nil];
                    [alrt show];
                    
                } failure:^(NSError *error) {
                    
                }];

                
            } failure:^(NSError *error) {
                
                
            }];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark   UITableView Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return fgModel.friendgroups.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"GroupMessageSet";
    GroupMessageTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"GroupMessageTableViewCell" owner:nil options:nil]firstObject];
    }
    GroupModel * model = fgModel.friendgroups[indexPath.row];
    [cell setContent:model realGroupId:currentString];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //将好友
    GroupModel * model = fgModel.friendgroups[indexPath.row];
    
    //将好友移到新的分组里
    NSDictionary * params = @{@"uid":USERID,@"orignfriendgroupid":currentString,@"newfriendgroupid":model.groupid,@"frienduserid":fModel.userid, @"token":@"110"};
    [HttpTool postWithPath:@"ChangeFriendsGroupMember" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            currentString = model.groupid;
            [tableView reloadData];
            if (returnBlock) {
                returnBlock(model,fgModel);
            }
            [[NSNotificationCenter defaultCenter]postNotificationName:@"DELETEGROUP" object:nil];
        }
        UIAlertView * alrt = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles: nil];
        [alrt show];
        
    } failure:^(NSError *error) {
        
    }];
}


-(void)returnText:(returnGroupName)block
{
    returnBlock = block;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
