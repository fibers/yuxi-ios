//
//  ChatViewController.h
//  inface
//
//  Created by Mac on 15/4/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatTableViewCell.h"
#import "ChatImgTableViewCell.h"
#import "AZXEmojiManagerView.h"
#import "SGXEmotionContent.h"
#import "AddDoViewController.h"
#import "PanelViewController.h"
#import "UzysAssetsPickerController.h"
#import "FamilyInformationViewController.h"
#import "TheatreInformationViewController.h"
#import "ChatIndexTableViewCell.h"
#import "ChattingModule.h"
#import "MsgReadACKAPI.h"
#import "DDDatabaseUtil.h"
#import "DDMessageSendManager.h"
#import "PhotosCache.h"
#import "Photo.h"
#import "DDSendPhotoMessageAPI.h"
#import "YXSelectImagesView.h"
#import "DDChatImageCell.h"

typedef NS_ENUM(NSUInteger, DDBottomShowComponent)
{
    DDInputViewUp                       = 1,
    DDShowKeyboard                      = 1 << 1,
    DDShowEmotion                       = 1 << 2,
    DDShowUtility                       = 1 << 3
};

typedef NS_ENUM(NSUInteger, DDBottomHiddComponent)
{
    DDInputViewDown                     = 14,
    DDHideKeyboard                      = 13,
    DDHideEmotion                       = 11,
    DDHideUtility                       = 7
};
//

typedef NS_ENUM(NSUInteger, DDInputType)
{
    DDVoiceInput,
    DDTextInput
};

typedef NS_ENUM(NSUInteger, PanelStatus)
{
    VoiceStatus,
    TextInputStatus,
    EmotionStatus,
    ImageStatus
};

#define DDINPUT_MIN_HEIGHT          44.0f
#define DDINPUT_HEIGHT              self.chatInputTextView.frame.size.height

#define DDINPUT_BOTTOM_FRAME        CGRectMake(0, CONTENT_HEIGHT -  self.chatInputTextView.frame.size.height + NAVBAR_HEIGHT,FULL_WIDTH,self.chatInputTextView.frame.size.height)
#define DDINPUT_TOP_FRAME           CGRectMake(0, CONTENT_HEIGHT -  self.chatInputTextView.frame.size.height + NAVBAR_HEIGHT - 216, FULL_WIDTH, self.chatInputTextView.frame.size.height)
#define DDUTILITY_FRAME             CGRectMake(0, CONTENT_HEIGHT + NAVBAR_HEIGHT -216, FULL_WIDTH, 216)
#define DDEMOTION_FRAME             CGRectMake(0, CONTENT_HEIGHT + NAVBAR_HEIGHT-216, FULL_WIDTH, 216)
#define DDCOMPONENT_BOTTOM          CGRectMake(0, CONTENT_HEIGHT + NAVBAR_HEIGHT, FULL_WIDTH, 216)

@interface ChatViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,AZXEmojiManagerViewDelegate,UzysAssetsPickerControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,ChatImgSaveAndSendDelegate,UIGestureRecognizerDelegate,YXSelectImagesDelegate,ChatImgLongPressDelegate,UIAlertViewDelegate>
{

    UIImagePickerController    *imagepicker;
    UIImageView *SwipeimageView;//左右横扫图片切换图片模式和标签模式
    BOOL indexMode;//NO为图片模式 YES为标签模式
    NSString * local_keyPath;
    NSMutableArray * percentImages;
    UIButton *RightButtonCreate;
    BOOL keyboarding;//键盘已经出现了
    
    BOOL keyShowAndClickEmotion;//键盘已经弹出，并且点击表情键盘
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *InputViewSpace;
@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UIButton *emojiBtn;
- (IBAction)emojiAction:(UIButton *)sender;
- (IBAction)emojiMoreAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatInputTextViewHeight;//textview高
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *InputViewHeight;//黑条的高
@property (weak, nonatomic) IBOutlet UIButton *addImage;
@property (weak, nonatomic) IBOutlet UIView *chatInputView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatViewBottomToSuperView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tradingTop;
@property (weak, nonatomic) IBOutlet UITableView *chatTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatTableviewAndViewHeight;

@property (strong, nonatomic) NSMutableArray *dataArr;//聊天数组
@property (strong,nonatomic)      NSMutableArray * selectPersons;//@的人员
@property (strong,nonatomic)  NSString * imgSerUrl;
@property (strong, nonatomic) AZXEmojiManagerView *emojiView;
@property(nonatomic,strong)ChattingModule* module;
@property(assign,nonatomic)BOOL isInCheckGroup;//是否在审核群里聊天
@property(strong,nonatomic)NSString * checkGroupId;//审核群id
@property(strong,nonatomic)NSString * theartorId;//剧的id
@property(nonatomic,strong) NSArray  * groupsArr;//获取剧对应的各个群
@property(nonatomic,assign)BOOL     isReadyAddPerson;//是否准备将人加到戏群或者水聊群
@property(nonatomic,strong)NSString * addMmebersStr;
@property(nonatomic,assign)BOOL     isCheck;//是否继续坚挺@
@property(nonatomic,assign)BOOL      isChatInStory;//是否在剧群聊天
@property(nonatomic,assign)BOOL      isChatInFamily;
@property(strong,nonatomic)UILongPressGestureRecognizer *longPressGesture;
@property(strong,nonatomic) NSIndexPath * editingIndexPath;
@property(assign,nonatomic) BOOL  hadLoadHistory;
@property(assign,nonatomic) BOOL  hasSelectGroupMembers;//是否选择了群成员
@property(strong,nonatomic) YXSelectImagesView * selectImagesVew;
@property(assign,nonatomic) BOOL  isAnimating;
/**
 *  任意页面跳转到聊天界面并开始一个会话
 *
 *  @param session 传入一个会话实体
 */
- (void)showChattingContentForSession:(SessionEntity*)session;
-(void) saveAndSendMes:(NSString *)savekey :(UIImage *)image;
-(void)sendMessage:(NSString *)msg messageEntity:(DDMessageEntity *)message;
-(void)changeLoadingState;
+(instancetype )shareInstance;

-(void)GetFavoriteEmotionList;//获得表情列表

-(void)setTextView:(NSMutableString *)str;


-(void)popSelf;
@end
