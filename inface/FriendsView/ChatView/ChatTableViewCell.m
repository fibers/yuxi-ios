//
//  ChatTableViewCell.m
//  inface
//
//  Created by Mac on 15/4/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ChatTableViewCell.h"
#import "GetGroupRoleName.h"
#import "DiscussModel.h"
#import "SavePersonPortrait.h"

@implementation ChatTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.countLbl.textColor=GRAY_COLOR;
}

- (void)sendImageAgain:(DDMessageEntity*)message
{
    //子类去继承
//    [self showSending];
    NSDictionary* dic = [NSDictionary initWithJsonString:message.msgContent];
    NSString* locaPath = dic[DD_IMAGE_LOCAL_KEY];
    __block UIImage* image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:locaPath];
    if (!image)
    {
        NSData* data = [[PhotosCache sharedPhotoCache] photoFromDiskCacheForKey:locaPath];
        image = [[UIImage alloc] initWithData:data];
        if (!image) {
//            [self showSendFailure];
            return ;
        }
    }
    [[DDSendPhotoMessageAPI sharedPhotoCache] uploadImage:locaPath success:^(NSString *imageURL) {
        NSDictionary* tempMessageContent = [NSDictionary initWithJsonString:message.msgContent];
        NSMutableDictionary* mutalMessageContent = [[NSMutableDictionary alloc] initWithDictionary:tempMessageContent];
        [mutalMessageContent setValue:imageURL forKey:DD_IMAGE_URL_KEY];
        NSString* messageContent = [mutalMessageContent jsonString];
        message.msgContent = messageContent;
        image = nil;
        [[DDMessageSendManager instance] sendMessage:message isGroup:[message isGroupMessage] Session:[[SessionModule sharedInstance] getSessionById:message.sessionId] completion:^(DDMessageEntity* theMessage,NSError *error) {
            if (error)
            {
                message.state = DDMessageSendFailure;
                //刷新DB
                [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
                    if (result)
                    {
//                        [self showSendFailure];
                    }
                }];
            }
            else
            {
                //刷新DB
                message.state = DDmessageSendSuccess;
                //刷新DB
                [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
                    if (result)
                    {
//                        [self showSendSuccess];
                    }
                }];
            }
        } Error:^(NSError *error) {
            [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
                if (result)
                {
//                    [self showSendFailure];
                }
            }];
        }];
        
    } failure:^(id error) {
        message.state = DDMessageSendFailure;
        //刷新DB
        [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
            if (result)
            {
//                [self showSendFailure];
            }
        }];
    }];
    
}

#pragma mark 剧群的聊天页面
-(void)setContentLbl:(DDMessageEntity *)contnent groupid:(NSString*)groupid storyid:(NSString*)storyid
{
    [[DDUserModule shareInstance]getUserForUserID:contnent.sessionId Block:^(DDUserEntity *user) {
        
        NSString * userid = contnent.senderId;
        NSString *user_id;
        NSRange range = [userid rangeOfString:@"_"];
        if (range.length > 0) {
            NSArray * arr = [userid componentsSeparatedByString:@"_"];
            user_id = [arr objectAtIndex:1];
            
        }else{
            user_id = userid;
        }
        //获取该人物的角色名
        [[GetGroupRoleName shareInstance]ReturnRoleName:groupid stroryid:storyid userid:user_id success:^(NSDictionary *personDic) {
            
            NSString * rolename = [personDic objectForKey:@"rolename"];
            NSString * nickname = [personDic objectForKey:@"nickname"];

            NSString * preName;
            NSString * extrenName;
            NSString * rockname;
            if (rolename.length > 0 && ![rolename isEqualToString:@""] && ![rolename isEqual:[NSNull null]] && ![rolename isEqual:nil]) {
                preName = rolename;
            }
            if (nickname.length > 0 && ![nickname isEqualToString:@""] && ![nickname isEqual:[NSNull null]] && ![nickname isEqual:nil]) {
                extrenName = nickname;
            }
            if(preName && extrenName)
            {
                rockname = [NSString stringWithFormat:@"%@+%@",preName,extrenName];
            } else
              {
                if (preName) {
                    rockname = preName;
                }else{
                    rockname = extrenName;
                }
            }
            [self.nameLbl setText:rockname];
        }];
//        [self.contentLbl setText:contnent.msgContent];
        if(contnent.msgContent !=  NULL && !contnent.msgContent!= nil) {
            [self setContentAttributedText:contnent];
            [self.countLbl setText:[NSString stringWithFormat:@"%d个字",[HZSInstances countWord:contnent.msgContent]]];
        }
        else {
        }
    }];
    
}


-(void)setFamilyContentLbl:(DDMessageEntity *)content groupid:(NSString *)groupid;
{
    [[DDUserModule shareInstance]getUserForUserID:content.sessionId Block:^(DDUserEntity *user) {
        
        NSString * userid = content.senderId;
        NSString *user_id;
        NSRange range = [userid rangeOfString:@"_"];
        if (range.length > 0) {
            NSArray * arr = [userid componentsSeparatedByString:@"_"];
            user_id = [arr objectAtIndex:1];
            
        }else{
            user_id = userid;
        }
        //获取该人物的角色名
        [[GetGroupRoleName shareInstance]ReturnFamilyInfo:groupid userid:user_id success:^(NSDictionary *dic) {
            
            NSString * nickname = [dic objectForKey:@"title"];
            if (nickname && ![nickname isEqualToString:@""]) {
                [self.nameLbl setText:nickname];
            }else{
                nickname = [dic objectForKey:@"nickname"];
                [self.nameLbl setText:nickname];
            }
        }];
         
    //     [self.contentLbl setText:content.msgContent];
        if (content.msgContent != NULL && ![content.msgContent isEqual:nil]) {
            [self setContentAttributedText:content];
            [self.countLbl setText:[NSString stringWithFormat:@"%d个字",[HZSInstances countWord:content.msgContent]]];
        }

    }];

}


-(void)setCurrentGroupName:(DDMessageEntity *)message groupid:(NSString *)groupid
{
    NSString * userid = message.senderId;
    NSString *user_id;
    NSRange range = [userid rangeOfString:@"_"];
    if (range.length > 0) {
        NSArray * arr = [userid componentsSeparatedByString:@"_"];
        user_id = [arr objectAtIndex:1];
        
    }else{
        user_id = userid;
    }
    //根据群id和userid获取消息发送者在群里的名字
    [[DiscussModel shareInstance]returnGroupMembers:groupid userid:user_id success:^(NSDictionary *personDic) {
        NSString * nickname = [personDic objectForKey:@"nickname"];
//        [self setTimeStamp:message.msgTime];
        NSString * timeStr = [self setTimeStamp:message.msgTime];
        NSString * name_time = [NSString stringWithFormat:@"%@       %@",nickname,timeStr];
        [self.nameLbl setText:name_time];
        
    }];
    //[self.contentLbl setText:message.msgContent];
    if (message.msgContent != NULL && ![message.msgContent isEqual:nil]) {
        [self setContentAttributedText:message];

    }
}



- (NSString *)setTimeStamp:(NSUInteger)timeStamp
{
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    NSString* dateString = [date transformToFuzzyDate];
    return dateString;
}

- (void)setContent:(DDMessageEntity*)content
{
    NSArray * ids = [content.senderId componentsSeparatedByString:@"_"];
    NSString * userid = @"";
    if (ids.count > 1) {
        userid = ids[1];
    }else{
        userid = ids[0];
    }
    [[DDUserModule shareInstance] getUserForUserID:content.senderId Block:^(DDUserEntity *user) {
//        NSDate* date = [NSDate dateWithTimeIntervalSince1970:content.msgTime];
//        [self.timeLbl setText:[date promptDateString]];
        //[self.contentLbl setText:content.msgContent];
        if (content.msgContent != NULL && ![content.msgContent isEqual:nil]) {
            [self setContentAttributedText:content];
            //        [self.countLbl setText:[NSString stringWithFormat:@"%d个字",[HZSInstances countWord:content.msgContent]]];
            [self.countLbl setText:[NSString stringWithFormat:@"%ld个字",content.msgContent.length]];
        }

    
   [[SavePersonPortrait shareInstance]getPersonNickName:userid success:^(id JSON) {
    
       [self.nameLbl setText:JSON];

    } failure:^(id json) {
    
        [self.nameLbl setText:user.nick];

    }];
    }];
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


/**
 *  设定URL字段
 *
 */
- (void) setContentAttributedText : (DDMessageEntity *) message
{
    NSError *error;
    NSMutableArray *urls;
    NSMutableArray *urlRanges;
    NSString * text = message.msgContent;
    urlRanges   =   [[NSMutableArray alloc] init];
    urls        =   [[NSMutableArray alloc] init];
    
    NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";

    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray *arrayOfAllMatches = [regex matchesInString:text
                                                options:0
                                                  range:NSMakeRange(0, [text length])];
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:text];
    for (NSTextCheckingResult *match in arrayOfAllMatches) {
        //没有匹配的场合。
        if (match.range.location == NSNotFound ) {
            continue;
        }
        NSString *substringForMatch =   [text substringWithRange:match.range];
        NSURL    *suburl            =   [NSURL URLWithString:substringForMatch];
        
        [urls addObject:suburl];
        [urlRanges addObject:[NSValue valueWithRange:match.range]];
        [attribute addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:match.range];
        [attribute addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:match.range];
        //[attribute addAttribute:NSLinkAttributeName value:substringForMatch range:match.range];

    }
    [self.contentLbl setAttributedText:attribute];
    [self.contentLbl setURLs:urls forRanges:urlRanges];
    
    self.contentLbl.URLClickHandler = ^(CXAHyperlinkLabel *label, NSURL *URL, NSRange range, NSArray *textRects){
        NSString    *url    =   [URL absoluteString];
        NSRange rangeUrl = [url rangeOfString:@"www.yuxip.com"];
        if(rangeUrl.length > 0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }
        else if (_parentController != nil){
            HelpInformationViewController * detailViewController = [[HelpInformationViewController alloc] init];
            detailViewController.navTitle   =   url;
            detailViewController.contentUrl =   url;
            detailViewController.hidesBottomBarWhenPushed=YES;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [_parentController pushViewController:detailViewController animated:YES];
        }
        
    };
    
}

- (void) setNaviController : (UINavigationController *) parentController
{
    _parentController   =   parentController;
}





@end
