//
//  DDChatImageCell.h
//  IOSDuoduo
//
//  Created by Michael Scofield on 2014-06-09.
//  Copyright (c) 2014 murray. All rights reserved.
//

#import "DDChatBaseCell.h"
//#import "MWPhotoBrowser.h"
//MWPhotoBrowserDelegate
typedef void(^DDPreview)();
typedef void(^DDTapPreview)();
@protocol  ChatImgLongPressDelegate;

@interface DDChatImageCell : DDChatBaseCell<DDChatCellProtocol>
@property(nonatomic,strong)UIImageView *msgImgView;
@property(nonatomic,strong)NSMutableArray *photos;
@property(nonatomic,strong)DDPreview preview;
-(void)showPreviewMessage:(DDMessageEntity *)message;
- (void)sendImageAgain:(DDMessageEntity*)message;
@property (nonatomic, retain) id<ChatImgLongPressDelegate> delegate;

@end
@protocol ChatImgLongPressDelegate <NSObject>

-(void)saveImage:(UIImage*)image;

@end