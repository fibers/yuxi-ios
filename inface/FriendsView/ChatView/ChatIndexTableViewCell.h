//
//  ChatIndexTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatIndexTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;//时间
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;//姓名
@end
