//
//  HeadGroupView.h
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//
@protocol ReadGroupNotyDelegate <NSObject>

-(void)readNoty;

-(void)removeView;

@end
#import <UIKit/UIKit.h>

@interface HeadGroupView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;


@property (weak, nonatomic) IBOutlet UILabel *dateLabel;


@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet PlaceholderTextView *contentLabel;

@property (weak, nonatomic) IBOutlet UIButton *headButton;

@property(unsafe_unretained,nonatomic)id<ReadGroupNotyDelegate>delegate;


-(void)setTile:(NSString *)title content:(NSString *)content dateTime:(NSString * )dateTime groupId:(NSString *)groupId;
@end
