//
//  ZhouYuViewController.m
//  inface
//
//  Created by appleone on 15/7/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ZhouYuViewController.h"

@interface ZhouYuViewController ()

@end

@implementation ZhouYuViewController


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    self.navigationItem.title = @"施法说明";
    if (self.commentGroup) {
        self.zhouyuIntroduct.image = [UIImage imageNamed:@"introductionzhou"];
    }

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)returnBackl{

    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
