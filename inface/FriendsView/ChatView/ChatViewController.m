//
//  ChatViewController.m
//  inface
//
//  Created by Mac on 15/4/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ChatViewController.h"
#import "MJPhoto.h"
#import "MJPhotoBrowser.h"
#import "UpYun.h"
#import "AddCheckGroupViewController.h"
#import "SaveStoryDetailByID.h"
#import "FamilyMembersViewController.h"
#define MaxNumberOfDescriptionChars  1000//textview最多输入的字数
#import "ZhouYuViewController.h"
#import "SavePersonPortrait.h"
#import "YXGroupCardViewController.h"
#import "OtherPersonRolrView.h"
#import "GetGroupRoleName.h"
#import "DDChatBaseCell.h"
#import "DDChatTextCell.h"
#import "UnAckMessageManager.h"
#import "DDPromptCell.h"
#import "DDChatImageCell.h"
#import "YXMyCenterViewController.h"
#import "NotyResult.h"
#import "HeadGroupView.h"
#import "ReadNotyViewController.h"
#import "ReadNotifyViewController.h"
#import "CCFuckKeyBoardView.h"
#import "MsgAddGroupConfirmAPI.h"
@interface ChatViewController ()<ReadGroupNotyDelegate>
{
    UpYun * _upYun;
    
    BOOL     isLoadingImage;//是否正在上传图片
    BOOL     isUpdatingImage;//正在上传时，图片表格不刷新
    NotyResult * notyResult;
    
    HeadGroupView * headView;
    
    NSString * title;
    
    NSString * content;
    
    NSString * dateTime;
    BOOL     isKeyBoardShow;
    
    CGFloat  keyBoardHeight;//键盘高度
    
    DDBottomShowComponent _bottomShowComponent;
    
    CCFuckKeyBoardView    * fuckView;
    
    NSInteger             currentDeltaHeight;
}
@property(nonatomic,strong)UIActivityIndicatorView *activity;
///用来缓存
@property(nonatomic,strong)UIImage* saveImgTMP;
@end

@implementation ChatViewController
+(instancetype )shareInstance
{
    static dispatch_once_t onceToken;
    static ChatViewController *_sharedManager = nil;
    dispatch_once(&onceToken, ^{
        _sharedManager = [ChatViewController new];
    });
    return _sharedManager;
}

//左上角按钮
-(void)returnBackl{
    
    self.isChatInStory = NO;
    self.isChatInFamily = NO;
    self.checkGroupId = nil;
    self.isInCheckGroup = NO;
    [self.selectPersons removeAllObjects];
    self.addMmebersStr = nil;
    
    self.emojiBtn.selected=NO;
    [self.chatInputTextView resignFirstResponder];
    self.InputViewSpace.constant = 0;
    
    self.chatInputTextViewHeight.constant = 30;
    
    self.InputViewHeight.constant = 45;
    [self.module.ids removeAllObjects];
    headView.frame = CGRectMake(0, -140, ScreenWidth, 100);
    //    self.module.sessionEntity.sessionID = @"user_00010010000";
    [self.navigationController popViewControllerAnimated:YES];
    
}

//右上角按钮
-(void)returnBackr{
    RightButtonCreate.userInteractionEnabled=NO;
    self.emojiBtn.selected=NO;
    [self.chatInputTextView resignFirstResponder];
    
    self.InputViewSpace.constant = 0;
    if ([self.module.sessionEntity isGroup]) {
        
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        NSArray * groupArr = [self.module.sessionEntity.sessionID componentsSeparatedByString:@"_"];
        NSString * groupid = [groupArr objectAtIndex:1];
        NSDictionary * dic = @{@"uid":userid,@"groupid":groupid};
        
        [HttpTool postWithPath:@"GetGroupDetail" params:dic success:^(id JSON) {
            if ([[JSON objectForKey:@"result"]integerValue]==1) {
                
                RightButtonCreate.userInteractionEnabled=YES;
                NSMutableDictionary *dic=[JSON objectForKey:@"groupdetails"];
                if ([[dic objectForKey:@"type"]isEqualToString:@"1"]) {
                    
                    //是家族的群
                    FamilyInformationViewController * detailViewController = [[FamilyInformationViewController alloc] init];
                    long  fid = [[RuntimeStatus instance]changeIDToOriginal:self.module.sessionEntity.sessionID];
                    NSString * familyid = [NSString stringWithFormat:@"%lu",fid];
                    detailViewController.chatSession = self.module.sessionEntity;
                    detailViewController.canChat = YES;
//                    [[DDDatabaseUtil instance]insertDraftMessage:self.chatInputTextView.text sessionId:self.module.sessionEntity.sessionID];
                    //                    self.module.sessionEntity = nil;
                    //                    [self.module.ids removeAllObjects];
                    //
                    detailViewController.isNeedShowchatting = YES;
                    detailViewController.familyid = familyid;
                    detailViewController.hidesBottomBarWhenPushed=YES;
                    if (detailViewController.isAnimating) {
                        return;
                    }
                    detailViewController.isAnimating = YES;
                    [self.navigationController pushViewController:detailViewController animated:YES];
                } else {
                    
                    //是剧群
                    
                    NSArray *menuItems =
                    @[
                      [KxMenuItem menuItem:@"群资料"
                                     image:[UIImage imageNamed:@"群资料"]
                                    target:self
                                    action:@selector(pushToGroupDetail)],
                      
                      [KxMenuItem menuItem:@"剧资料"
                                     image:[UIImage imageNamed:@"剧资料"]
                                    target:self
                                    action:@selector(pushToStoryDetail)],
                      
                      
                      ];
                    KxMenuItem *first = menuItems[0];
                    //first.foreColor = [UIColor colorWithRed:47/255.0f green:112/255.0f blue:225/255.0f alpha:1.0];
                    first.alignment = NSTextAlignmentLeft;
                    
                    [KxMenu showMenuInView:self.navigationController.view
                                  fromRect:CGRectMake(ScreenWidth-45, 10, 25, 44)
                                 menuItems:menuItems];
                    
                }
            }else{
                
                RightButtonCreate.userInteractionEnabled=YES;
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
            }
            
        } failure:^(NSError *error) {
            
            RightButtonCreate.userInteractionEnabled=YES;
        }];
        
        
        //群会话
        
    }else{
        
        RightButtonCreate.userInteractionEnabled=YES;
        //私聊
        [self.module getCurrentUser:^(DDUserEntity *user) {
            //TODO(Xc.)这里注释了
            //            FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc] init];
            //            detailViewController.FriendID = self.module.sessionEntity.sessionID;
            //            detailViewController.chatSession = self.module.sessionEntity;
            //            detailViewController.iscanchat = YES;
//            [[DDDatabaseUtil instance]insertDraftMessage:self.chatInputTextView.text sessionId:self.module.sessionEntity.sessionID];
            
            //            self.module.sessionEntity = nil;
            //            [self.module.ids removeAllObjects];
            //            detailViewController.isNeedShowchatting = YES;
            //            detailViewController.hidesBottomBarWhenPushed=YES;
            //            [self.navigationController pushViewController:detailViewController animated:YES];
            YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
            NSArray * ids = [self.module.sessionEntity.sessionID componentsSeparatedByString:@"_"];
            NSString * friendId = @"";
            if (ids.count >1) {
                friendId = ids[1];
            }else{
                friendId = ids[0];
            }
            [detailViewController setChatSession:self.module.sessionEntity];
            [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:friendId];
            detailViewController.hidesBottomBarWhenPushed = YES;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
            
        }];
    }
}


#pragma mark 跳转到剧详情
-(void)pushToStoryDetail
{
    WorldDetailViewController * detailViewController = [[WorldDetailViewController alloc] init];
//    [[DDDatabaseUtil instance]insertDraftMessage:self.chatInputTextView.text sessionId:self.module.sessionEntity.sessionID];
    
    detailViewController.storyID = self.theartorId;
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


#pragma mark 跳转到群详情
-(void)pushToGroupDetail
{
    TheatreInformationViewController * detailViewController = [[TheatreInformationViewController alloc] init];
    long  fid = [[RuntimeStatus instance]changeIDToOriginal:self.module.sessionEntity.sessionID];
    detailViewController.chatSession = self.module.sessionEntity;
    detailViewController.isNeedShowchatting = YES;
//    [[DDDatabaseUtil instance]insertDraftMessage:self.chatInputTextView.text sessionId:self.module.sessionEntity.sessionID];
    
    detailViewController.canChat = YES;
    //    self.module.sessionEntity = nil;
    //    [self.module.ids removeAllObjects];
    NSString * theatreGroupid = [NSString stringWithFormat:@"%lu",fid];
    detailViewController.theatreGroupid =theatreGroupid;
    detailViewController.theatreid=self.theartorId;
    
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager]setEnable:NO];
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activity.frame=CGRectMake(ScreenWidth/2, 10, 20, 20);
    
    [self.view addSubview:self.activity];
    
    self.chatTableView.delegate=self;
    self.chatTableView.dataSource=self;
    
    if ([self.chatTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.chatTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 15)];
    }
    
    if ([self.chatTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.chatTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 15)];
    }
    [self.chatTableView setSeparatorColor:BLUECOLOR];
    self.chatTableView.showsVerticalScrollIndicator=NO;
    [self.chatTableView reloadData];
    self.chatTableView.tableFooterView=[[UIView alloc]init];
    
    indexMode=NO;//图片模式
    
    UINib *nibfoot=[UINib nibWithNibName:@"ChatImgTableViewCell" bundle:nil];
    ChatImgTableViewCell *footerview=[[nibfoot instantiateWithOwner:nil options:nil] firstObject];
    footerview.img.userInteractionEnabled=YES;
    footerview.img.backgroundColor=[UIColor clearColor];
    footerview.img.image=[UIImage imageNamed:@"addimage"];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(handleTap:)];
    
    [footerview.img addGestureRecognizer:tapGestureRecognizer];
    
    
    self.dataArr=[[NSMutableArray alloc]init];
    
    
    _longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)];
    _longPressGesture.minimumPressDuration = 1.0;
    [_chatTableView addGestureRecognizer:_longPressGesture];    //设置发文字框为圆角矩形
    self.chatInputTextView.layer.borderWidth = 1;
    self.chatInputTextView.layer.borderColor = COLOR(239, 239, 239, 1).CGColor;
    self.chatInputTextView.layer.cornerRadius = 6;
    self.chatInputTextView.clipsToBounds = YES;
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.inputAccessoryView = [[UIView alloc] init];
    //表情按钮更换状态
    [self.emojiBtn setImage:[UIImage imageNamed:@"emotions"] forState:UIControlStateNormal];
    [self.emojiBtn setImage:[UIImage imageNamed:@"keyboard"] forState:UIControlStateSelected];
    
    [self module];
    imagepicker = [[UIImagePickerController alloc]init];
    [self.module addObserver:self forKeyPath:@"showingMessages" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:NULL];
    
    [self addObserver:self forKeyPath:@"_inputViewY" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    percentImages = [NSMutableArray new];
    isUpdatingImage = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reLoadChatMessages:) name:@"ReloadSession" object:nil];
    _selectPersons = [NSMutableArray array];
    self.chatInputTextView.returnKeyType = UIReturnKeySend;
    self.dataArr = [NSMutableArray arrayWithCapacity:10];
    __block NSString * draftStr;
    [[DDDatabaseUtil instance]getDraftMessage:self.module.sessionEntity.sessionID  success:^(id json) {
        
        draftStr = json;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.chatInputTextView.text = draftStr;
            
        });
        
    }];
    
    headView = [[[NSBundle mainBundle]loadNibNamed:@"HeadGroupView" owner:nil options:nil]firstObject];
    if (ScreenWidth > 375) {
        headView.frame= CGRectMake(12, -240, ScreenWidth -24, 100);
        
    }else{
        headView.frame= CGRectMake(12, -150, ScreenWidth -24, 100);
        
    }
    headView.layer.cornerRadius = 4.0;
    headView.delegate = self;
    headView.headButton.userInteractionEnabled = YES;
    headView.contentLabel.userInteractionEnabled = YES;
    headView.layer.cornerRadius = 4.0f;
    headView.layer.borderWidth = 0.5;
    headView.layer.borderColor = [UIColor colorWithHexString:@"#949494"].CGColor;
    [self.view addSubview:headView];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //    self.chatTableviewAndViewHeight.isActive = YES;
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.chatInputTextView resignFirstResponder];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

-(void)getGroupNoty:(NSString*)creatorName
{
    __block NSString * _dateTime = @"";
    [[DDDatabaseUtil instance]getGroupNotify:self.checkGroupId success:^(NSString *title, NSString *content, NSString *dateTime) {
        
        _dateTime = dateTime;
    }];
    
    if ([_dateTime isEqualToString:@""] || [_dateTime isEqual:nil] || _dateTime == NULL) {
        _dateTime = @"0";
    }
    
    NSDictionary * params = @{@"uid":USERID,@"groupid":self.checkGroupId,@"latesttime":_dateTime,@"token":@"110"};
    [HttpTool postWithPath:@"GetStoryGroupBoard" params:params success:^(id JSON) {
        NSDictionary * notyDic = [[JSON objectForKey:@"boardinfo"]objectAtIndex:0];
        title = [notyDic objectForKey:@"title"];
        
        content = [notyDic objectForKey:@"content"];
        
        dateTime = [JSON objectForKey:@"datetime"];
        if (content != NULL && ![content isEqualToString:@""] && ![content isEqual:nil]) {
            //此时显示群公告
            [headView setTile:title content:content dateTime:dateTime groupId:self.checkGroupId];
            
            [[DDDatabaseUtil instance]saveGroupNotify:title content:content groupid:self.checkGroupId dateTime:dateTime];
            //            [self.chatTableView layoutIfNeeded];
            
            [UIView animateWithDuration:0.4 animations:^{
                headView.frame= CGRectMake(12, 6, ScreenWidth -24, 100);
                //                self.chatTableView.frame = CGRectMake(0, 177, ScreenWidth, self.chatTableView.frame.size.height);
            } completion:^(BOOL finished) {
                
                
            }];
            
            //            self.tradingTop.constant = 170;
            //            NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval) target:<#(id)#> selector:<#(SEL)#> userInfo:<#(id)#> repeats:<#(BOOL)#>
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)readNoty
{
    //查看公告
    
    //   __block NSString * creatorName = @"";
    //    [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:self.checkGroupId success:^(id JSON) {
    //        NSDictionary * storyinfo = JSON;
    //
    //       creatorName = [storyinfo objectForKey:@"creatorname"];
    //
    //
    //    } failure:^(id Json) {
    //
    //
    //    }];
    ReadNotifyViewController * readView = [[ReadNotifyViewController alloc]init];
    
    readView.hidesBottomBarWhenPushed = YES;
    if (readView.isAnimating) {
        return;
    }
    readView.isAnimating = YES;
    [readView setContent:self.checkGroupId creatorName:nil];
    [self.navigationController pushViewController:readView animated:YES];
    
}


-(void)removeView
{
    [UIView animateWithDuration:0.4 animations:^{
        if (ScreenWidth > 375) {
            headView.frame = CGRectMake(0, -250, ScreenWidth, 100);
            
        }else{
            headView.frame = CGRectMake(0, -140, ScreenWidth, 100);
            
        }
        
        
    }];
    
}


//-(void) keyboardWillShow:(NSNotification *) aNotification
//{
//    CGRect keyboardRect;
//    keyboardRect = [(aNotification.userInfo)[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
//    _bottomShowComponent = _bottomShowComponent | DDShowKeyboard;
//    [UIView animateWithDuration:0.25 animations:^{
//        [self.chatInputView setFrame:CGRectMake(0, keyboardRect.origin.y - DDINPUT_HEIGHT, ScreenWidth, DDINPUT_HEIGHT)];
//    }];
//
//    [self setValue:@(keyboardRect.origin.y - DDINPUT_HEIGHT) forKeyPath:@"_inputViewY"];

//}

-(void) keyboardWillShow:(NSNotification *) aNotification {
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    float height = keyboardRect.size.height;
    
    if (height == 0) {
        return  ;
        
    }
    
    keyBoardHeight = height;
    //键盘没有弹出时
    NSInteger  rows = [_chatTableView numberOfRowsInSection:0];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.chatViewBottomToSuperView.constant = height;
        if (rows> 0) {
            [self changeTabelViewContentInset];
            
            [self scrollToBottomAnimated:NO];
            
        }
        isKeyBoardShow = YES;
        
    }];
}

//-(void) keyboardDidShow:(NSNotification *) aNotification {
//    //获取键盘的高度
//    NSDictionary *userInfo = [aNotification userInfo];
//    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey];
//    CGRect keyboardRect = [aValue CGRectValue];
//    float height = keyboardRect.size.height;
//    NSLog(@"keyboardDidShow keyBoard*/*//**/*//**//*/%f",height);
//
//    aValue  =[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
//    keyboardRect = [aValue CGRectValue];
//    NSLog(@"keyboardDidShow UIKeyboardFrameEndUserInfoKey*/*//**/*//**//*/%f", keyboardRect.size.height);
//
//
//}



-(void) keyboardWillHide:(NSNotification *) note
{
    
    isKeyBoardShow = NO;
    keyShowAndClickEmotion = NO;
    keyBoardHeight = 0;
    [UIView animateWithDuration:.3f animations:^{
        //改变位置
        self.chatViewBottomToSuperView.constant = 0;
        [self changeTabelViewContentInset];//改变表格的位置
        
        self.addImage.selected = NO;
        self.emojiBtn.selected=NO;
        self.chatInputTextView.inputView=nil;
    }];
    
}


#pragma amrk 改变TableView的contentInset
-(void)changeTabelViewContentInset
{
    if (isKeyBoardShow) {
        _chatTableView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight - keyBoardHeight-110);
        
        _chatTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        [self scrollToBottomAnimated:NO];
        
    }else{
        _chatTableView.contentInset = UIEdgeInsetsMake(0, 0, keyBoardHeight, 0);
        
    }
    
}

-(void)setRightBarButtonItem{
    
    
    UIView *aview=[[UIView alloc]init];
    aview.frame=CGRectMake(0,0,70,30);
    aview.backgroundColor=[UIColor clearColor];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:aview];
    self.navigationItem.rightBarButtonItem=barback2;
    
    
    //添加2个按钮
    for (int i=0; i<2; i++) {
        UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
        aButton.tag=i+10000;
        [aButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        aButton.titleLabel.font=[UIFont systemFontOfSize:12];
        aButton.titleLabel.textAlignment=NSTextAlignmentCenter;
        [aButton setBackgroundColor:[UIColor clearColor]];
        [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
        
        [aview addSubview:aButton];
        
        if (i==0) {
            if (self.module.sessionEntity.sessionType == SessionTypeSessionTypeSingle) {
                aButton.hidden = YES;
            }
            [aButton setImage:[UIImage imageNamed:@"magicunselect"] forState:UIControlStateNormal];
            [aButton setImage:[UIImage imageNamed:@"magicselect"] forState:UIControlStateHighlighted];
            aButton.frame = CGRectMake(0,0, 27, 27);
            [aButton addTarget:self action:@selector(readZhouYu) forControlEvents:UIControlEventTouchUpInside];
            
        } else if (i==1){
            aButton.frame=CGRectMake(45,0, 30, 24);
            
            [aButton setImage:[UIImage imageNamed:@"familyunselect"] forState:UIControlStateNormal];
            [aButton setImage:[UIImage imageNamed:@"familyselect"] forState:UIControlStateHighlighted];
            
            [aButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    
    
}



#pragma mark 进入咒语说明页
-(void)readZhouYu
{
    self.emojiBtn.selected=NO;
    [self.chatInputTextView resignFirstResponder];
    isKeyBoardShow = NO;
    keyShowAndClickEmotion = NO;
    self.InputViewSpace.constant = 0;
    
    ZhouYuViewController * zhouyu = [[ZhouYuViewController alloc]init];
    if (zhouyu.isAnimating) {
        return;
    }
    zhouyu.isAnimating = YES;
    zhouyu.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:zhouyu animated:YES];
}



//添加图片
-(void) handleTap:(UITapGestureRecognizer*) recognizer
{
    
    BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    //如果支持相机，那么多个选项
    if (isCameraSupport) {
        UIActionSheet* mySheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"挑照片" otherButtonTitles:@"现在拍", nil];
        [mySheet showInView:self.view];
        
    }
    else {
        [self startPhotoChooser];
    }
    
    
}





#pragma mark 重新根据session构造会话
-(void)reLoadChatMessages:(NSNotification*)noty
{
    SessionEntity * session = noty.object;
    [self showChattingContentForSession:session];
    
}


#pragma mark 发送照片
-(void) saveAndSendMes:(NSString *)savekey :(UIImage *)image
{
    NSDictionary* messageContentDic = @{DD_IMAGE_LOCAL_KEY:local_keyPath};
    NSString* messageContent = [messageContentDic jsonString];
    
    DDMessageEntity *message = [DDMessageEntity makeMessage:messageContent Module:self.module MsgType:DDMessageTypeImage isSendImage:YES isCurrentGroup:NO];
    //    [self scrollToBottomAnimated:YES];
    NSData *photoData = UIImagePNGRepresentation(image);
    [[PhotosCache sharedPhotoCache] storePhoto:photoData forKey:local_keyPath toDisk:YES];
#pragma mark 成功前不插入数据库
    
    NSString * sendImagePath;
    
    //由返回的字符串拼接成图片特有的key
    sendImagePath  =   [NSString stringWithFormat:@"%@%@%@%@",DD_MESSAGE_IMAGE_PREFIX, IMAGE_SERVER_BASE_URL, savekey,DD_MESSAGE_IMAGE_SUFFIX];
    message.state=DDMessageSending;
    NSDictionary* tempMessageContent = [NSDictionary initWithJsonString:message.msgContent];
    NSMutableDictionary* mutalMessageContent = [[NSMutableDictionary alloc] initWithDictionary:tempMessageContent];
    [mutalMessageContent setValue:sendImagePath forKey:DD_IMAGE_URL_KEY];
    NSString* message_con = [mutalMessageContent jsonString];
    message.msgContent = message_con;
    [self sendMessage:sendImagePath messageEntity:message];
}


-(void)changeLoadingState;
{
    isUpdatingImage = YES;
}

- (void)uzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    
    
    if([[assets[0] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
    {
        
        [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            ALAsset *representation = (ALAsset*)obj;
            UIImage * image =  [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                                   scale:representation.defaultRepresentation.scale
                                             orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
            
            
            local_keyPath = [[PhotosCache sharedPhotoCache]getKeyName];
            NSString * keyName = [[PhotosCache sharedPhotoCache]getKeyName];
            [self sendImageMessage:keyName Image:[HZSInstances fixOrientation:image]];
            dispatch_async(dispatch_get_main_queue(), ^{
            });
            
            
        }];
    }
    else //Video
    {
        
        
    }
    
}




#pragma mark actionSheet Delegate

- (void) startPhotoChooser
{
    UzysAssetsPickerController *picker = [[UzysAssetsPickerController alloc] init];
    picker.delegate = self;
    
    picker.maximumNumberOfSelectionPhoto = 3;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (666==actionSheet.tag) { //保存图片
        switch (buttonIndex) {
            case 0:
                if(!self.saveImgTMP)return;
                [self saveImgtoAlbum:self.saveImgTMP];
                break;
            case 1:
                break;
                
            default:
                break;
        }
        
    }else{
        //choose photo
        if (buttonIndex == 0) {
            
            [self startPhotoChooser];
            
        } else if (buttonIndex == 1) {
            //take photo.
            //指定使用照相机模式,可以指定使用相册／照片库
            imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            //设置当拍照完或在相册选完照片后，是否跳到编辑模式进行图片剪裁。只有当showsCameraControls属性为true时才有效果
            imagepicker.allowsEditing = NO;
            //设置拍照时的下方的工具栏是否显示，如果需要自定义拍摄界面，则可把该工具栏隐藏
            imagepicker.showsCameraControls  = YES;
            //设置使用后置摄像头，可以使用前置摄像头
            imagepicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
            //设置闪光灯模式
            /*
             typedef NS_ENUM(NSInteger, UIImagePickerControllerCameraFlashMode) {
             UIImagePickerControllerCameraFlashModeOff  = -1,
             UIImagePickerControllerCameraFlashModeAuto = 0,
             UIImagePickerControllerCameraFlashModeOn   = 1
             };
             */
            imagepicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
            //设置相机支持的类型，拍照和录像
            imagepicker.mediaTypes = @[(NSString*)kUTTypeImage];
            //设置拍摄时屏幕的view的transform属性，可以实现旋转，缩放功能
            // imagepicker.cameraViewTransform = CGAffineTransformMakeRotation(M_PI*45/180);
            // imagepicker.cameraViewTransform = CGAffineTransformMakeScale(1.5, 1.5);
            //所有含有cameraXXX的属性都必须要sourceType是UIImagePickerControllerSourceTypeCamera时设置才有效果，否则会有异常
            //设置UIImagePickerController的代理
            imagepicker.delegate = self;
            [self presentViewController:imagepicker animated:YES completion:^{
                
            }];
            
        }
    }
}

#pragma take photos image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
    NSString* type = [info objectForKey:UIImagePickerControllerMediaType];
    //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    if ([type isEqualToString:(NSString*)kUTTypeImage]&&picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
        //获取照片的原图
        UIImage* original = [info objectForKey:UIImagePickerControllerOriginalImage];
        original = [HZSInstances scaleImage:original toScale:0.8];
        //        NSData *imageData = UIImageJPEGRepresentation(original, (CGFloat)1.0);
        //        UIImage * m_selectImage = [UIImage imageWithData:imageData];
        //        Photo *photo = [Photo new];
        NSString * keyName = [[PhotosCache sharedPhotoCache]getKeyName];
        [self sendImageMessage:keyName Image:original];
        NSData *photoData = UIImagePNGRepresentation(original);
        [[PhotosCache sharedPhotoCache] storePhoto:photoData forKey:keyName toDisk:YES];
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //            isLoadingImage = YES;
        //            //上传图 刷新tableview
        //            [self.chatImgTable reloadData];
        //        });
        //        //如果是拍照的照片，则需要手动保存到本地，系统不会自动保存拍照成功后的照片
        UIImageWriteToSavedPhotosAlbum(original, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
    }else{
        
    }
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//取消照相机的回调
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//add for photo functions end


- (void)n_receiveMessage:(NSNotification*)notification
{
    if (![self.navigationController.topViewController isEqual:self])
    {
        //当前不是聊天界面直接返回
        return;
    }
    DDMessageEntity* message = [notification object];
    
    //显示消息
    UIApplicationState state =[UIApplication sharedApplication].applicationState;
    if (state == UIApplicationStateBackground) {
        if([message.sessionId isEqualToString:self.module.sessionEntity.sessionID])
        {
            [self.module addShowMessage:message];
            [self.module updateSessionUpdateTime:message.msgTime];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self scrollToBottomAnimated:YES];
            });
        }
        return;
    }
    
    if([message.sessionId isEqualToString:self.module.sessionEntity.sessionID])
    {
        [self.module addShowMessage:message];
        [self.module updateSessionUpdateTime:message.msgTime];
        [[DDMessageModule shareInstance]  sendMsgRead:message];
    }
    
}




-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setRightBarButtonItem];

    [_emojiView removeFromSuperview];
    _emojiView=nil;
    [_selectImagesVew removeFromSuperview];
    _selectImagesVew = nil;
    [self CreateEmojiView];
    isKeyBoardShow = NO;
    keyShowAndClickEmotion = NO;
    //如果是剧群中聊天，需要获取群公告
    if (self.isChatInStory) {
        [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:self.checkGroupId success:^(id JSON) {
            NSDictionary * storyinfo = JSON;
            if ([[storyinfo objectForKey:@"creatorid"]isEqualToString:USERID]) {
                //是自己
            }else{
                NSString * creatorName = [storyinfo objectForKey:@"creatorname"];
                [self getGroupNoty:creatorName];
            }
            
        } failure:^(id Json) {
            
            
        }];
    }
    
    [self createSelectImagesView];
    
    __block NSString * draftString;
    //获取草稿
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(n_receiveMessage:)
                                                 name:DDNotificationReceiveMessage
                                               object:nil];
    
    if(self.hasSelectGroupMembers == NO){
        [[DDDatabaseUtil instance]getDraftMessage:self.module.sessionEntity.sessionID success:^(id json) {
            draftString = json;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.chatInputTextView.text = draftString;
                
            });
            
        }];
    }
    //    keyboarding=NO;
    
    //    [self.module addObserver:self forKeyPath:@"showingMessages" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:NULL];
    
    //KVO
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //    [self.module.ids removeAllObjects];
    
    
}


-(void)dealloc
{
    [self.module removeObserver:self forKeyPath:@"showingMessages"];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        //该判断用于联想输入
        if (textView.text.length > MaxNumberOfDescriptionChars)
        {
            textView.text = [textView.text substringToIndex:MaxNumberOfDescriptionChars];
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"文本内容不能超过1000字哦" message:@"程序猿们11111111正在改善中" delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
            [alert show];
            
        }
        
        [self textViewEnterSend];
        
        return NO;
        
    }
    
    return YES;
    
}



-(void)sendMessage:(NSString *)msg messageEntity:(DDMessageEntity *)message
{
    BOOL isGroup = [self.module.sessionEntity isGroup];
    if (self.module.sessionEntity) {
        [[DDMessageSendManager instance] sendMessage:message isGroup:isGroup Session:self.module.sessionEntity  completion:^(DDMessageEntity* theMessage,NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                message.state= theMessage.state;
                [self.chatTableView reloadData];
                //                [self scrollToBottomAnimated:YES];
            });
        } Error:^(NSError *error) {
            
            [self.chatTableView reloadData];
            
        }];
    }
    
}

-(void)sendImageMessage:(NSString *)keyPath Image:(UIImage *)image
{
    
    
    NSDictionary* messageContentDic = @{DD_IMAGE_LOCAL_KEY:keyPath};
    NSString* messageContent = [messageContentDic jsonString];
    
    DDMessageEntity *message = [DDMessageEntity makeMessage:messageContent Module:self.module MsgType:DDMessageTypeImage isSendImage:YES isCurrentGroup:NO];
    //    [self scrollToBottomAnimated:YES];
    NSData *photoData = UIImagePNGRepresentation(image);
    [[PhotosCache sharedPhotoCache] storePhoto:photoData forKey:keyPath toDisk:YES];
    
#pragma -mark 发送成功前不插入数据库
    //    [SVProgressHUD showWithStatus:@"上传中" maskType:SVProgressHUDMaskTypeBlack];
    //此处需要换成又拍的云存储
    _upYun = [[UpYun alloc]init];
    [_upYun uploadImage:image];
    
    //上传图片成功
    WEAKSELF
    _upYun.successBlocker = ^(id data){
        if (!weakSelf) {
            return ;
        }
        
        [SVProgressHUD dismiss];
        NSString        *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
        imgUrl  =   [NSString stringWithFormat:@"%@%@%@%@",DD_MESSAGE_IMAGE_PREFIX, IMAGE_SERVER_BASE_URL, imgUrl,DD_MESSAGE_IMAGE_SUFFIX];
        [weakSelf scrollToBottomAnimated:YES];
        message.state=DDMessageSending;
        NSDictionary* tempMessageContent = [NSDictionary initWithJsonString:message.msgContent];
        NSMutableDictionary* mutalMessageContent = [[NSMutableDictionary alloc] initWithDictionary:tempMessageContent];
        [mutalMessageContent setValue:imgUrl forKey:DD_IMAGE_URL_KEY];
        NSString* messageContent = [mutalMessageContent jsonString];
        message.msgContent = messageContent;
        [weakSelf sendMessage:imgUrl messageEntity:message];
        //        [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
        //
        //        }];
        
    };
    
    _upYun.failBlocker = ^(NSError * error)
    {
        [SVProgressHUD dismiss];
        [SVProgressHUD showErrorWithStatus:@"上传失败" duration:2.0];
        message.state = DDMessageSendFailure;
        //刷新DB
        [_chatTableView reloadData];
        [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
            if (result)
            {
                
            }
        }];
    };
    
}

#pragma -mark 输入文字时输入框高度的变化
- (void)textViewDidChange:(UITextView *)textView{
    
    //该判断用于联想输入
    if (textView.text.length > MaxNumberOfDescriptionChars)
    {
        textView.text = [textView.text substringToIndex:MaxNumberOfDescriptionChars];
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"文本内容不能超过1000字哦" message:@"程序猿们正在改善中" delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
        [alert show];
        
    }
    [[DDDatabaseUtil instance]insertDraftMessage:self.chatInputTextView.text sessionId:self.module.sessionEntity.sessionID ];

    
    NSInteger maxCharactersPerLine=17;//一行里地最大字母数
    NSInteger numberOfLinesOfText=(textView.text.length / maxCharactersPerLine) + 1;
    
    NSInteger maxLines=3;//最多显示的行数
    
    [UIView animateWithDuration:0.3 animations:^{
        //改变位置
        if (numberOfLinesOfText>=maxLines) {
            self.InputViewHeight.constant = 45+(maxLines-1)*15.0;
            self.chatInputTextViewHeight.constant = 30+(maxLines-1)*15.0;
            NSInteger  deltaHeight = (maxLines-1)*15.0 - currentDeltaHeight;
            if (deltaHeight > 0) {
                //输入框的高度变化了
                                keyBoardHeight += deltaHeight;
                                [self changeTabelViewContentInset];
            }
        } else {
            self.InputViewHeight.constant = 45+(numberOfLinesOfText-1)*15.0;
            self.chatInputTextViewHeight.constant = 30+(numberOfLinesOfText-1)*15.0;
            NSInteger  deltaHeight = (numberOfLinesOfText-1)*15.0 - currentDeltaHeight;
            
            if (deltaHeight > 0) {
                //输入框的高度变化
                                keyBoardHeight += deltaHeight;
                                [self changeTabelViewContentInset];
            }
        }
        
        currentDeltaHeight = self.InputViewHeight.constant -45;
        
    }];
    
    //如果是家族的聊天页面
    if (self.isChatInFamily == YES ) {
        //在家族的聊天页面
        [self checkFamilyChat];
    }
    if (self.isChatInStory == YES) {
        //剧群中聊天
        [self checkTextView];
    }
}

#pragma mark 给文本框赋值
-(void)setTextView:(NSMutableString *)str
{
    NSString * originText = self.chatInputTextView.text;
    self.chatInputTextView.text = [NSString stringWithFormat:@"%@%@ ",originText,str];
    self.addMmebersStr = str;
    self.isReadyAddPerson = YES;
}



#pragma mark家族的群聊天时监听到@字符时
-(void)checkFamilyChat
{
    NSInteger textLength = [self.chatInputTextView.text length];
    NSRange range = NSMakeRange(textLength - 1, 1);
    NSString * lastChar = [self.chatInputTextView.text substringWithRange:range];
    if ([lastChar isEqualToString:@"@"]) {
        //弹出家族成员列表
        
        FamilyMembersViewController * familyView = [[FamilyMembersViewController alloc]init];
        familyView.familyid = self.checkGroupId;
        familyView.hidesBottomBarWhenPushed = YES;
        if (familyView.isAnimating) {
            return;
        }
        familyView.isAnimating = YES;
        [self.navigationController pushViewController:familyView animated:YES];
        if (IOSSystemVersion < 8.0) {
            [self.navigationController setNavigationBarHidden:YES];
        }
    }
}

#pragma mark 监测聊天键盘输入的文字是否含有@
-(void)checkTextView
{
    NSInteger textLength = [self.chatInputTextView.text length];
    NSRange range = NSMakeRange(textLength - 1, 1);
    NSString * lastChar = [self.chatInputTextView.text substringWithRange:range];
    
    if ([lastChar isEqualToString:@"@"]) {
        //测试弹出审核群的好友
        AddCheckGroupViewController * addCheckGroupView = [[AddCheckGroupViewController alloc]init];
        addCheckGroupView.theatreGroupid = self.checkGroupId;
        addCheckGroupView.theatreid = self.theartorId;
        addCheckGroupView.hidesBottomBarWhenPushed = YES;
        if (IOSSystemVersion < 8.0) {
            [self.navigationController setNavigationBarHidden:YES];
        }
        if (addCheckGroupView.isAnimating) {
            return;
        }
        addCheckGroupView.isAnimating = YES;
        [self.navigationController pushViewController:addCheckGroupView animated:YES];
    }
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.module.showingMessages.count;
    
}


#pragma mark 长按手势
-(void)longPress:(UILongPressGestureRecognizer*)longpress
{
    if(longpress.state == UIGestureRecognizerStateBegan){
        CGPoint locationPoint = [longpress locationInView:_chatTableView];
        
        //        _editingIndexPath = [_chatTableView indexPathForRowAtPoint:_locationPoint];
        NSIndexPath  * indexPath = [_chatTableView indexPathForRowAtPoint:locationPoint];
        if(indexPath!= nil){
            
            id object = self.module.showingMessages[indexPath.row];
            if ([object isKindOfClass:[DDMessageEntity class]]) {
                DDMessageEntity * entity = object;
                if (entity.msgContentType == DDMessageTypeText) {
                    [_chatInputTextView resignFirstResponder];
                    isKeyBoardShow = NO;
                    keyShowAndClickEmotion = NO;
                    UIMenuController  * menu = [UIMenuController  sharedMenuController];
                    
                    UIMenuItem   *menuItem1 = [[UIMenuItem alloc]initWithTitle:@"复制" action:@selector(copyContent)];
                    _editingIndexPath = indexPath;
                    menu.menuItems = [NSArray arrayWithObjects:menuItem1, nil];
                    
                    [self.chatTableView becomeFirstResponder];
                    [menu setTargetRect:CGRectMake(locationPoint.x - 60, locationPoint.y , 120, 44) inView:self.chatTableView];
                    
                    [menu setMenuVisible:YES animated:YES];
                }
            }
        }
    }
}


-(BOOL)canBecomeFirstResponder{
    return YES;
}


-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    return action == @selector(copyContent) ?YES:NO;
}



-(void)copyContent
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    DDMessageEntity * message = self.module.showingMessages[_editingIndexPath.row];
    
    NSString * str = message.msgContent ;
    
    [pasteboard setString:str];
    
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
    if (translation.y > 0 || translation.y < 0) {
        [UIView animateWithDuration:0.5 animations:^{
            [self.chatInputTextView resignFirstResponder];
            isKeyBoardShow = NO;
            keyShowAndClickEmotion = NO;
        } completion:^(BOOL finished) {
            
            
        }];
        
    }
    
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    static BOOL loadingHistory = NO;
    CGFloat offsetY = scrollView.contentOffset.y;
    
    
    if (scrollView.contentOffset.y < -100 && [self.module.showingMessages count] > 0 && !loadingHistory)
    {
        loadingHistory = YES;
        self.hadLoadHistory=YES;
        [self.activity startAnimating];
        self.module.isLoadOldMessages = YES;
        NSInteger preCount = [self.module.showingMessages count];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.module loadMoreHistoryCompletion:^(NSUInteger addCount,NSError *error) {
                [self.activity stopAnimating];
                loadingHistory=NO;
                [self.chatTableView reloadData];
                if (addCount) {
                    NSInteger index = [self.module.showingMessages count]-preCount;
                    [self.chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                }
                
            } ifLoadldMessage:YES];
        });
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    id object = self.module.showingMessages[indexPath.row];
    UITableViewCell* cell = nil;
    if ([object isKindOfClass:[DDMessageEntity class]])
    {
        DDMessageEntity* message = (DDMessageEntity*)object;
        if (message.msgContentType == DDMessageTypeText ) {
            cell = [self p_textCell_tableView:tableView cellForRowAtIndexPath:indexPath message:message];
            
        }
        else if(message.msgContentType == DDMessageTypeImage)
        {
            cell = [self p_imageCell_tableView:tableView cellForRowAtIndexPath:indexPath message:message];
        }
        //        }else if (message.msgContentType == DDMEssageEmotion)
        //        {
        //            cell = [self p_emotionCell_tableView:tableView cellForRowAtIndexPath:indexPath message:message];
        //        }
        else
        {
            cell = [self p_textCell_tableView:tableView cellForRowAtIndexPath:indexPath message:message];
        }
        
    }
    else if ([object isKindOfClass:[DDPromptEntity class]])
    {
        DDPromptEntity* prompt = (DDPromptEntity*)object;
        cell = [self p_promptCell_tableView:tableView cellForRowAtIndexPath:indexPath message:prompt];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
    
    
}



#pragma mark PrivateAPI


- (UITableViewCell*)p_textCell_tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath message:(DDMessageEntity*)message
{
    static NSString* identifier = @"DDChatTextCellIdentifier";
    DDChatBaseCell* cell = (DDChatBaseCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell = [[DDChatTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [(DDChatTextCell *)cell setNaviController:self.navigationController];
    }
    cell.session =self.module.sessionEntity;
    NSString* myUserID = [RuntimeStatus instance].user.objID;
    if ([message.senderId isEqualToString:myUserID])
    {
        [cell setLocation:DDBubbleRight];
    }
    else
    {
        [cell setLocation:DDBubbleLeft];
    }
    
    if (![[UnAckMessageManager instance] isInUnAckQueue:message] && message.state == DDMessageSending && [message isSendBySelf]) {
        message.state=DDMessageSendFailure;
    }
    [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
        
    }];
    
    [cell setContent:message];
    
    __weak DDChatTextCell* weakCell = (DDChatTextCell*)cell;
    cell.sendAgain = ^{
        [weakCell showSending];
        [weakCell sendTextAgain:message];
    };
    
    return cell;
}



- (UITableViewCell*)p_promptCell_tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath message:(DDPromptEntity*)prompt
{
    static NSString* identifier = @"DDPromptCellIdentifier";
    DDPromptCell* cell = (DDPromptCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell = [[DDPromptCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    NSString* promptMessage = prompt.message;
    [cell setprompt:promptMessage];
    return cell;
}


- (UITableViewCell*)p_imageCell_tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath message:(DDMessageEntity*)message
{
    static NSString* identifier = @"DDImageCellIdentifier";
    DDChatImageCell* cell = (DDChatImageCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell = [[DDChatImageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.session =self.module.sessionEntity;
    NSString* myUserID =[RuntimeStatus instance].user.objID;
    if ([message.senderId isEqualToString:myUserID])
    {
        [cell setLocation:DDBubbleRight];
    }
    else
    {
        [cell setLocation:DDBubbleLeft];
    }
    
    [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
        
    }];
    [cell setContent:message];
    __weak DDChatImageCell* weakCell = cell;
    
    [cell setSendAgain:^{
        [weakCell sendImageAgain:message];
        
    }];
    
    [cell setTapInBubble:^{
        [weakCell showPreviewMessage:message];
    }];
    
    [cell setPreview:cell.tapInBubble];
    cell.delegate = self;
    return cell;
}




#pragma mark -
#pragma mark 保存图片
///保存图片到相册
-(void)saveImgtoAlbum:(UIImage*)img{
    UIImageWriteToSavedPhotosAlbum(img, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    self.saveImgTMP = nil;
}
///图片长按事件
-(void)saveImage:(UIImage *)img{
    UIActionSheet * actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"保存到相册", nil];
    actionSheet.tag = 666;
    self.saveImgTMP = img;
    [actionSheet showInView:self.view];
}
///保存图片回调
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString *msg = nil ;
    if(error != NULL){
        msg = @"保存图片失败" ;
    }else{
        msg = @"保存图片成功" ;
    }
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
    //                                                    message:msg
    //                                                   delegate:self
    //                                          cancelButtonTitle:@"确定"
    //                                          otherButtonTitles:nil];
    //    [alert show];
}

#pragma mark -
#pragma mark tableview代理
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //        if (self.dataArr.count > 0) {
    //            id object = self.dataArr[indexPath.row];
    //            DDMessageEntity* message = (DDMessageEntity*)object;
    //            NSString *contentLblDetail=message.msgContent;
    //            float contentLblDetailHeight=[HZSInstances labelAutoCalculateRectWith:contentLblDetail FontSize:14.0 MaxSize:CGSizeMake(ScreenWidth-120, 2000)].height;
    //            return 30+contentLblDetailHeight+10+30;
    //        }else{
    //            return 0;
    //        }
    float height = 0;
    id object = self.module.showingMessages[indexPath.row];
    if ([object isKindOfClass:[DDMessageEntity class]])
    {
        
        DDMessageEntity* message = object;
        NSString* myUserID =[RuntimeStatus instance].user.objID;
        height = [self.module messageHeight:message];
        
        if ([message.senderId isEqualToString:myUserID]) {
            height -= 15;
        }
        //单聊对方单元格高度
        NSRange  range = [message.sessionId rangeOfString:@"user"];
        if ( range.length > 0 && ![message.senderId isEqualToString:myUserID]) {
            height -= 15;
            
        }
        NSRange range1= [message.sessionId rangeOfString:@"group_"];
        if (range1.length > 0 &&![message.senderId isEqualToString:myUserID] ) {
            height+=5;
        }
        
    }
    else if([object isKindOfClass:[DDPromptEntity class]])
    {
        height = 30;
    }
    return height+10;
    
}



//选中一行的时候

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器

    [self.chatInputTextView resignFirstResponder];
    isKeyBoardShow = NO;
    keyShowAndClickEmotion = NO;
    [self.chatTableView deselectRowAtIndexPath:indexPath animated:YES];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (AZXEmojiManagerView *)CreateEmojiView
{
    if (!_emojiView) {
        _emojiView = [[AZXEmojiManagerView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 200)];
        _emojiView.backgroundColor = TABBAR_COLOR;
        _emojiView.delegate = self;
    }
    return _emojiView;
}


-(YXSelectImagesView *)createSelectImagesView
{
    if (!_selectImagesVew) {
        _selectImagesVew = [[YXSelectImagesView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 200)];
        _selectImagesVew.backgroundColor = TABBAR_COLOR;
        _selectImagesVew.delegate = self;
        
    }
    return _selectImagesVew;
}

- (IBAction)emojiAction:(UIButton *)sender {
    
    
    //表情
    if (sender.selected==NO) {
        sender.selected=YES;
        self.chatInputTextView.inputView=self.emojiView;
        [self.chatInputTextView becomeFirstResponder];
        [self.chatInputTextView reloadInputViews];
        [UIView animateWithDuration:.3f animations:^{
            //改变位置
            self.chatViewBottomToSuperView.constant = 200;
            
        }];
    } else {
        keyShowAndClickEmotion = NO;
        sender.selected=NO;
        self.chatInputTextView.inputView=nil;
        [self.chatInputTextView reloadInputViews];
    }
}

- (IBAction)emojiMoreAction:(UIButton *)sender {
    
    if (sender.selected==NO) {
        
        sender.selected=YES;
        self.chatInputTextView.inputView=self.selectImagesVew;
        [self.chatInputTextView becomeFirstResponder];
        
        [self.chatInputTextView reloadInputViews];
        [UIView animateWithDuration:.3f animations:^{
            //改变位置
            self.chatViewBottomToSuperView.constant = 200;
            
        }];
    } else {
        sender.selected=NO;
        self.chatInputTextView.inputView=nil;
        [self.chatInputTextView reloadInputViews];
        
    }
    
}

-(void)selectImages
{
    [self startPhotoChooser];
    
    
}

-(void)takePhotos
{
    imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    //设置当拍照完或在相册选完照片后，是否跳到编辑模式进行图片剪裁。只有当showsCameraControls属性为true时才有效果
    imagepicker.allowsEditing = NO;
    //设置拍照时的下方的工具栏是否显示，如果需要自定义拍摄界面，则可把该工具栏隐藏
    imagepicker.showsCameraControls  = YES;
    //设置使用后置摄像头，可以使用前置摄像头
    imagepicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    //设置闪光灯模式
    /*
     typedef NS_ENUM(NSInteger, UIImagePickerControllerCameraFlashMode) {
     UIImagePickerControllerCameraFlashModeOff  = -1,
     UIImagePickerControllerCameraFlashModeAuto = 0,
     UIImagePickerControllerCameraFlashModeOn   = 1
     };
     */
    imagepicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
    //设置相机支持的类型，拍照和录像
    imagepicker.mediaTypes = @[(NSString*)kUTTypeImage];
    //设置拍摄时屏幕的view的transform属性，可以实现旋转，缩放功能
    // imagepicker.cameraViewTransform = CGAffineTransformMakeRotation(M_PI*45/180);
    // imagepicker.cameraViewTransform = CGAffineTransformMakeScale(1.5, 1.5);
    //所有含有cameraXXX的属性都必须要sourceType是UIImagePickerControllerSourceTypeCamera时设置才有效果，否则会有异常
    //设置UIImagePickerController的代理
    imagepicker.delegate = self;
    [self presentViewController:imagepicker animated:YES completion:^{
        
    }];
    
}

-(void)clickWriteboard
{
    self.emojiBtn.selected=NO;
    [self.chatInputTextView resignFirstResponder];
    isKeyBoardShow = NO;
    keyShowAndClickEmotion = NO;
    self.InputViewSpace.constant = 0;
    PanelViewController * detailViewController = [[PanelViewController alloc] init];
    detailViewController.textcontent=self.chatInputTextView.text;
    detailViewController.module=self.module;
    detailViewController.hidesBottomBarWhenPushed=YES;
    
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    self.chatInputTextView.text=@"";
    
    
}


#pragma mark 点击皮表代理方法
-(void)skinSurfaceView:(UIView *)skinSurfaceView didSendItem:(NSString *)item
{
    self.chatInputTextView.text = [self.chatInputTextView.text stringByAppendingString:item];
    [[DDDatabaseUtil instance]insertDraftMessage:self.chatInputTextView.text sessionId:self.module.sessionEntity.sessionID ];
    self.emojiBtn.selected = NO;
    self.chatInputTextView.inputView = nil;
    [self.chatInputTextView reloadInputViews];
}


#pragma mark emojiManagerView delegate
- (void)emojiManagerView:(UIView *)emojiManagerView didSelectItem:(NSString *)item AtIndex:(NSUInteger)index
{
    NSMutableArray *aimgArray=[[SGXEmotionContent sharedClient]getEmotionArr];
    if (index==aimgArray.count-1) {
        [self.chatInputTextView resignFirstResponder];
        AddDoViewController * detailViewController = [[AddDoViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAinmating) {
            return;
        }
        detailViewController.isAinmating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    } else {
        //取得文字
        NSMutableArray *aimgArray=[[SGXEmotionContent sharedClient]getEmotionArr];
        NSString    *emotiContent   =   [[aimgArray objectAtIndex:index] objectForKey:@"content"];
        
        if (index<6) {
            
            self.chatInputTextView.text    =   [self.chatInputTextView.text stringByAppendingString:[NSString stringWithFormat:@"%@",emotiContent]];
        } else {
            
            self.chatInputTextView.text    =   [self.chatInputTextView.text stringByAppendingString:[NSString stringWithFormat:@"[%@]",emotiContent]];
        }
        [[DDDatabaseUtil instance]insertDraftMessage:self.chatInputTextView.text sessionId:self.module.sessionEntity.sessionID ];

//        keyShowAndClickEmotion = NO;
        self.emojiBtn.selected=NO;
        self.chatInputTextView.inputView=nil;
        [self.chatInputTextView reloadInputViews];
    }
    
}


- (void)emojiManagerView:(UIView *)emojiManagerView didDeleteItem:(NSString *)item
{
    //删除文字
    if (self.chatInputTextView.text.length > 0) {
        NSMutableString * currentStr = [NSMutableString stringWithString:self.chatInputTextView.text];
        [currentStr deleteCharactersInRange:NSMakeRange(self.chatInputTextView.text.length-1, 1)];
        self.chatInputTextView.text = currentStr;
        
    }
    
}


- (void)emojiManagerView:(UIView *)emojiManagerView didSendItem:(NSString *)item
{
    [self textViewEnterSend];
}



-(void)textViewEnterSend
{
    //发送消息，需要区分家族和剧的各类群群的情况
    if ([self.chatInputTextView.text isEmpty]) {
        //        [self.chatInputTextView resignFirstResponder];
        self.chatInputTextViewHeight.constant = 30;
        
        self.InputViewHeight.constant = 45;
        
        return;
    }
    NSString * text;
    BOOL isSendMessage = YES;
    __block BOOL  sendMessage = YES;
    //***************************************
    if (self.isChatInFamily == YES) {
        //在家族中聊天，管理员拥有删人功能，所有成员拥有加好友功能
        //先获取该家族的信息，判断自己是否是管理员
        [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:self.checkGroupId success:^(id JSON) {
            
            NSDictionary * familyInfo = JSON;
            NSString * creatorid = [familyInfo objectForKey:@"creatorid"];
            NSRange range = [self.chatInputTextView.text rangeOfString:@"离开皮卡丘"];
            NSRange range1 = [self.chatInputTextView.text rangeOfString:@"离开,皮卡丘"];
            NSRange range2 = [self.chatInputTextView.text rangeOfString:@"离开，皮卡丘"];
            
            if ([creatorid isEqualToString:USERID] && self.selectPersons.count > 0) {
                if (range.length > 0 ||range1.length > 0 || range2.length > 0) {
                    //管理员施展了删人功能
                    [self deleteMembers];
                    self.chatInputTextView.text = nil;
                    //                    [self.chatInputTextView resignFirstResponder];
                    self.chatInputTextViewHeight.constant = 30;
                    
                    self.InputViewHeight.constant = 45;
                    
                    self.addMmebersStr = nil;
                    sendMessage = NO;
                }
            }
            
        }failure:^(id Json) {
            
            
        }];
        NSRange range1 = [self.chatInputTextView.text rangeOfString:@"一套带走"];
        
        if (self.selectPersons.count >0 &&range1.length > 0) {
            //任何成员开启添加好友咒语
            [self addFriends];
            self.chatInputTextView.text = nil;
            //            [self.chatInputTextView resignFirstResponder];
            self.chatInputTextViewHeight.constant = 30;
            
            self.InputViewHeight.constant = 45;
            
            self.addMmebersStr = nil;
            sendMessage = NO;
            
        }
    }
    ///***********************************************
#pragma mark 剧群的聊天页面
    __block  NSDictionary* storyInfo;
    if (self.isChatInStory) {
        [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:self.checkGroupId success:^(id JSON) {
            storyInfo = JSON;
            
        }failure:^(id Json) {
            
            
        }];
        NSString * creatorid = [storyInfo objectForKey:@"creatorid"];
        NSRange range = [self.chatInputTextView.text rangeOfString:@"去吧皮卡丘"];
        NSRange  range1 = [self.chatInputTextView.text rangeOfString:@"去吧,皮卡丘"];
        NSRange  range2 = [self.chatInputTextView.text rangeOfString:@"去吧，皮卡丘"];
        
        if (self.isInCheckGroup && [creatorid isEqualToString:USERID] && self.selectPersons.count > 0) {
            if (range.length > 0 ||range1.length > 0 ||range2.length > 0) {
                [self addGroupsToGroup:storyInfo];
                
                text = [NSString stringWithFormat:@"%@ 恭喜你们已经成功地穿越到了水聊群和对戏群了",self.chatInputTextView.text];
                NSRange  range = [text rangeOfString:@"去吧皮卡丘"];
                if (range.length > 0) {
                    [text stringByReplacingCharactersInRange:range withString:@""];
                }
                isSendMessage = NO;
            }
            
        }
        NSRange range3 = [self.chatInputTextView.text rangeOfString:@"离开皮卡丘"];
        NSRange range4 = [self.chatInputTextView.text rangeOfString:@"离开,皮卡丘"];
        NSRange range5 = [self.chatInputTextView.text rangeOfString:@"离开，皮卡丘"];
        
        if ( [USERID isEqualToString:creatorid] &&self.selectPersons.count > 0){
            if (range3.length > 0 || range4.length > 0 ||range5.length > 0) {
                [self deleteMembers];
                self.chatInputTextView.text = nil;
                //                [self.chatInputTextView resignFirstResponder];
                //
                self.chatInputTextViewHeight.constant = 30;
                
                self.InputViewHeight.constant = 45;
                
                [self.selectPersons removeAllObjects];
                self.addMmebersStr = nil;
                sendMessage = NO;
            }
            
        }
    }
    NSRange range10 = [self.chatInputTextView.text rangeOfString:@"一套带走"];
    
    if (range10.length > 0 && self.selectPersons.count > 0) {
        [self addFriends];
        sendMessage = NO;
    }
    if (text.length > MaxNumberOfDescriptionChars)
    {
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"聊天内容暂时不能超过1000字哦" message:@"您可以选择分次发送内容" delegate:nil cancelButtonTitle:@"好吧" otherButtonTitles:nil];
        [alertView show];
        [self.chatInputTextView.text substringToIndex:MaxNumberOfDescriptionChars];
        return;
        sendMessage = NO;
    }
    DDMessageContentType msgContentType = DDMessageTypeText;
    if (isSendMessage == YES) {
        text = self.chatInputTextView.text;
    }
    if (sendMessage) {
        DDMessageEntity *message = [DDMessageEntity makeMessage:text Module:self.module MsgType:msgContentType isSendImage:NO isCurrentGroup:NO];
        [[DDDatabaseUtil instance] insertMessages:@[message] success:^{
        } failure:^(NSString *errorDescripe) {
        }];
        [self sendMessage:text messageEntity:message];
        [self.selectPersons removeAllObjects];
        self.addMmebersStr = nil;
        self.chatInputTextView.text = @"";
        [[DDDatabaseUtil instance]insertDraftMessage:self.chatInputTextView.text sessionId:self.module.sessionEntity.sessionID ];

        //        [self.chatInputTextView resignFirstResponder];
        self.chatInputTextViewHeight.constant = 30;
        
        self.InputViewHeight.constant = 45;
        
    }
    
}




-(void)addFriends
{
    [self.view endEditing:YES];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"验证消息" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"发送", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = @"请输入20字以内的验证信息";
    alert.tag = 20000;
    
    [alert show];
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==20000) {
        MsgAddFriendReqAPI * addFriendReq = [[MsgAddFriendReqAPI alloc]init];
        
        if (buttonIndex == 1 &&  [alertView textFieldAtIndex:0].text.length <=20) {
            NSString * addmessage = @"";
            if ([alertView textFieldAtIndex:0].text != NULL && ![[alertView textFieldAtIndex:0].text isEqual:nil]) {
                addmessage= [alertView textFieldAtIndex:0].text;
            }
            for (int i = 0; i < self.selectPersons.count; i++) {
                NSString * add_id = [self.selectPersons objectAtIndex:i];
                [addFriendReq requestWithObject:@[add_id,@(0),@(SessionTypeSessionTypeSingle),addmessage] Completion:^(id response, NSError *error) {
                    
                }];
            }
            
            [self.selectPersons removeAllObjects];
            self.chatInputTextView.text = nil;
            [self.chatInputTextView resignFirstResponder];
            self.addMmebersStr = nil;

            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"添加请求已发送" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
        }
    }
    
}


#pragma  mark 根据两个群的id，将选择的人员加到分别得群
-(void)addGroupsToGroup:(NSDictionary *)storyGroups
{
    if ([self.selectPersons count]> 0) {
        NSArray * arr = [storyGroups objectForKey:@"groups"];
        NSMutableArray * groupId = [NSMutableArray array];
        for (int i = 0; i < [arr count]; i++) {
            NSDictionary * dic = [arr objectAtIndex:i];
            NSString * isplay = [dic objectForKey:@"isplay"];
            if([isplay isEqualToString:@"1"]||[isplay isEqualToString:@"2"]){
                //是水群或者戏群
                NSString  * groupid = [dic objectForKey:@"groupid"];
                NSString * group_id =[NSString stringWithFormat:@"group_%@",groupid];
                [groupId addObject:group_id];
            }
        }
        if ([groupId count]>0) {
            for (int i = 0; i < [groupId count]; i++) {
                NSString * grouuID = [groupId objectAtIndex:i];
                NSString * originalGid = [grouuID componentsSeparatedByString:@"_"][1];
                [[ChangeGroupMemberModel shareInsatance]addMemberNotNeedNotifygroupid:grouuID users:self.selectPersons];
                MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
                [self.selectPersons enumerateObjectsUsingBlock:^(NSString * obj, NSUInteger idx, BOOL *stop) {
                    NSString * sessionid=[obj componentsSeparatedByString:@"_"][1];
                    
                    NSArray * arrary = @[@(0),sessionid,originalGid,@(0),@(SessionTypeSessionTypeSingle),@(1)];
                    [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
                        
                    }];

                }];

            }
        }
    }
    
    [self.selectPersons removeAllObjects];
    
    UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:@"穿越成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}


#pragma mark 删除群成员
-(void)deleteMembers
{
    if(self.selectPersons.count > 0)
    {
        NSString * group_id = [NSString stringWithFormat:@"group_%@",self.checkGroupId];
        [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:self.selectPersons];
    }
    [self.selectPersons removeAllObjects];

    UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:@"删除成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}


#pragma mark scroll Action
- (void)scrollToBottomAnimated:(BOOL)animated {
    //强制取消滚动动画，未来做的更科学一些。
    NSInteger rows = [self.chatTableView numberOfRowsInSection:0];
    
    if (rows > 0) {
        
        [self.chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rows - 1 inSection:0]
                                  atScrollPosition:UITableViewScrollPositionBottom
                                          animated:NO];
        
        
    }
    
    
}



- (ChattingModule*)module
{
    if (!_module)
    {
        _module = [[ChattingModule alloc] init];
    }
    return _module;
}


#pragma mark PublicAPI
- (void)showChattingContentForSession:(SessionEntity*)session
{
    __block  NSString * name ;
    self.hadLoadHistory=NO;
    NSArray * ids = [session.sessionID componentsSeparatedByString:@"_"];
    NSString * userid =@"";
    if (ids.count > 1) {
        userid = ids[1];
    }else{
        userid = ids[0];
    }
    
    //    self.navigationItem.title=session.name;
    
    [[SavePersonPortrait shareInstance]getPersonNickName:userid success:^(id JSON) {
        name= JSON;
        
    } failure:^(id json) {
        [[DDUserModule shareInstance] getUserForUserID:session.sessionID Block:^(DDUserEntity *user) {
            //        NSDate* date = [NSDate dateWithTimeIntervalSince1970:content.msgTime];
            //        [self.timeLbl setText:[date promptDateString]];
            name = user.nick;
            
        }];
        
    }];
    
    if (name) {
        self.navigationItem.title = name;
    }else{
        self.navigationItem.title = session.name;
    }
    [self.module.showingMessages removeAllObjects];
    [self.module.ids removeAllObjects];
    [self.chatTableView reloadData];
    self.hasSelectGroupMembers = NO;
    
    self.module.sessionEntity = session;
    
    //    self.module.sessionEntity.sessionID = session.sessionID;
    
    self.module.isLoadOldMessages = NO;//不是加载更多历史消息
    [self.activity startAnimating];

    //在加载消息的时候需要考虑是否加载历史消息
    [self.module loadMoreHistoryCompletion:^(NSUInteger addcount, NSError *error)  {
        [self.activity stopAnimating];

        if (session.unReadMsgCount !=0 && self.module.sessionEntity) {
            MsgReadACKAPI* readACK = [[MsgReadACKAPI alloc] init];
            [readACK requestWithObject:@[self.module.sessionEntity.sessionID,@(self.module.sessionEntity.lastMsgID),@(self.module.sessionEntity.sessionType)] Completion:nil];
            self.module.sessionEntity.unReadMsgCount=0;

            [[DDDatabaseUtil instance] updateRecentSession:self.module.sessionEntity completion:^(NSError *error) {
                
            }];
            
        }
    } ifLoadldMessage:NO];
    
}



#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"showingMessages"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //区分消息类型
            //            [self DistinguishMessageType];
            [self.chatTableView reloadData];
            if (self.hadLoadHistory == NO) {
                
                [self scrollToBottomAnimated:YES];
                self.chatTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            }
        });
    }
    
    
}


//区分消息类型







@end
