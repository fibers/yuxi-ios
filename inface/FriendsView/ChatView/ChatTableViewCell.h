//
//  ChatTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CXAHyperlinkLabel.h"

@interface ChatTableViewCell : UITableViewCell {
    UINavigationController * _parentController;
}
//@property (weak, nonatomic) IBOutlet UILabel *timeLbl;//时间
@property (weak, nonatomic) IBOutlet UILabel            *nameLbl;//姓名
@property (weak, nonatomic) IBOutlet CXAHyperlinkLabel *contentLbl;//内容
@property (weak, nonatomic) IBOutlet UILabel            *countLbl;//字数

-(void)setContentLbl:(DDMessageEntity *)contnent groupid:(NSString*)groupid storyid:(NSString*)storyid;
- (void)setContent:(DDMessageEntity*)content;
-(void)setFamilyContentLbl:(DDMessageEntity *)content groupid:(NSString *)groupid;
-(void)setCurrentGroupName:(DDMessageEntity *) message groupid:(NSString *)groupid;//获取临时群成员名字

- (void) setNaviController : (UINavigationController *) parentController;
@end
