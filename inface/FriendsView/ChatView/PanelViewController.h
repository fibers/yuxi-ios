//
//  PanelViewController.h
//  inface
//
//  Created by Mac on 15/4/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PanelViewController : BaseViewController<UITextViewDelegate,AZXEmojiManagerViewDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatInputTextViewHeight;//textview高
@property(nonatomic,strong)ChattingModule* module;
@property (strong, nonatomic) UIButton *emojiBtn;
@property (strong, nonatomic) AZXEmojiManagerView *emojiView;
@property (strong,nonatomic)  NSString * textcontent;
@property (strong,nonatomic)  NSString * sesionId;
@property (assign,nonatomic)  BOOL       isAnimating;
@end
