//
//  ChatImgTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EVCircularProgressView.h"

typedef void(^DDSendAgain)();
@protocol  ChatImgSaveAndSendDelegate;
@interface ChatImgTableViewCell : UITableViewCell
{
    NSString      *_key;

}

@property (nonatomic, retain) id<ChatImgSaveAndSendDelegate> delegate;

@property (nonatomic, retain) IBOutlet  UIImageView * img;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *imgHeightConstraint;
@property (nonatomic,copy)DDSendAgain sendAgain;
@property (nonatomic, retain) NSNumber  *isUpload;
@property (nonatomic ,strong) EVCircularProgressView * evprogressView;
- (void)sendImageAgain:(DDMessageEntity*)message;
- (void)setContent:(DDMessageEntity*)content;
- (NSString *) displayAndUpload : (UIImage *)img;//上传到又拍云图片

@end
@protocol ChatImgSaveAndSendDelegate <NSObject>

-(void) saveAndSendMes:(NSString *)savekey :(UIImage *)image;
/**
 *  cell被长按点击
 *
 *  @param img 被点击cell 中的图片
 */
-(void) chatImgCellDidLongPressedWithImg:(UIImage *)img;

@end