//
//  SearchFriendsViewController.m
//  inface
//
//  Created by Mac on 15/4/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SearchFriendsViewController.h"
#import "YXMyCenterViewController.h"
#import "NSString+YXExtention.h"
#import "MBProgressHUD+YXStyle.h"

@interface SearchFriendsViewController ()
@property (strong,nonatomic)UIButton   * _searchButton;

@end

@implementation SearchFriendsViewController

//左上角按钮
-(void)returnBackl{
    [_SearchText resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    if ([_SearchText.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入关键字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    [_SearchText resignFirstResponder];
    
    [self searchList];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
     __searchButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [__searchButton setTitle:@"搜索" forState:UIControlStateNormal];
    __searchButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [__searchButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [__searchButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:__searchButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    
    UIImageView *imageView=[[UIImageView alloc]init];
    imageView.userInteractionEnabled=YES;
    imageView.frame=CGRectMake(0, 0, ScreenWidth-90, 36);
    imageView.layer.borderWidth = 1;
    imageView.layer.borderColor = XIAN_COLOR.CGColor;
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = 4;
    imageView.backgroundColor=[UIColor whiteColor];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView=imageView;
    
    UIImageView *imageViewtmp=[[UIImageView alloc]init];
    imageViewtmp.userInteractionEnabled=YES;
    imageViewtmp.image=[UIImage imageNamed:@"sousuoju"];
    imageViewtmp.frame=CGRectMake(10, 9, 22, 18);
    imageViewtmp.clipsToBounds = YES;
    imageViewtmp.contentMode = UIViewContentModeScaleAspectFit;
    [imageView addSubview:imageViewtmp];
    
    _SearchText = [[UITextField alloc] initWithFrame:CGRectMake(37, 0, ScreenWidth-90-40, 36)];
    _SearchText.borderStyle = UITextBorderStyleNone;
    _SearchText.backgroundColor = [UIColor clearColor];
    _SearchText.placeholder = @"请输入好友昵称或ID号";
    _SearchText.text=@"";
    _SearchText.tintColor=BLUECOLOR;
    _SearchText.font = [UIFont systemFontOfSize:17];
    _SearchText.returnKeyType = UIReturnKeyDefault;
    _SearchText.delegate = self;
    _SearchText.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [imageView addSubview:_SearchText];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    
    self.tableView.separatorColor=XIAN_COLOR;
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    
}

-(void)searchList
{
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    //获取好友列表接口(Post)
    __searchButton.userInteractionEnabled = NO;
    NSDictionary *params = @{@"uid":userid,@"cond":_SearchText.text,@"token":@"110"};
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    
    [HttpTool postWithPath:@"SearchPerson" params:params success:^(id JSON) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        __searchButton.userInteractionEnabled = YES;

        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            NSMutableArray *afriendArr=[[JSON objectForKey:@"personlist"]mutableCopy];
            
            for (int i=0; i<afriendArr.count; i++) {
                NSString *friendid=[[[afriendArr objectAtIndex:i]objectForKey:@"id"]description];
                if ([friendid isEqualToString:userid]) {
                    [afriendArr removeObjectAtIndex:i];
                }
            }
            self.tableDataFriends=afriendArr;
            
            [self.tableView reloadData];
            
            if (self.tableDataFriends.count==0) {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"该用户还没有，搜索其他用户试试吧~" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
            }
        }else{
            
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
        }
        
    } failure:^(NSError *error) {
        __searchButton.userInteractionEnabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];
    
    
    
    
    
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.tableDataFriends.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"FriendsSelectTableViewCell";
    FriendsSelectTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"FriendsSelectTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    cell.UserNameLabel.text=[[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"name"];
    
    NSString *urlString=[[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"portrait"];
    
    int isVip = [urlString isSignedUser];
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
    switch (isVip) {
        case 0:
            cell.vipWriterImage.hidden = YES;
            break;
        case 1:
            cell.vipWriterImage.hidden = NO;
            
            cell.vipWriterImage.image = image;
            
            break;
        case 2:
            cell.vipWriterImage.hidden = NO;
            
            cell.vipWriterImage.image = officalImage;
            
            break;
        case 3:
            cell.vipWriterImage.hidden = NO;
            
            cell.vipWriterImage.image = officalImage;
            
            break;
        default:
            break;
    }
    
    
    [cell.UserImage sd_setImageWithURL:[NSURL URLWithString:[urlString stringOfW100ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
    //    cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
    
    cell.UserImage.clipsToBounds=YES;
    cell.UserImage.layer.cornerRadius = cell.UserImage.bounds.size.width/2.0;
    cell.SelectButton.tag=10000+indexPath.row;
    cell.SelectButton.layer.cornerRadius = 4.0f;
    [cell.SelectButton.layer setBorderWidth:0.6f];
    [cell.SelectButton.layer setBorderColor:[UIColor colorWithRed: 235.0/255.0 green:113.0/255 blue:168.0/255.0 alpha:1].CGColor];
    NSString *isfriend=[[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"isfriend"];
    if ([isfriend isEqualToString:@"1"]) {
        [cell.SelectButton setTitle:@"已添加" forState:UIControlStateNormal];
        cell.SelectButton.userInteractionEnabled = NO;
        [cell.SelectButton setTitleColor:[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1] forState:UIControlStateNormal];
        [cell.SelectButton.layer setBorderColor:[UIColor colorWithRed: 74/255.0 green:74/255.0 blue:74/255.0 alpha:1].CGColor];

        //        [cell.SelectButton setImage:[UIImage imageNamed:@"heijia"] forState:UIControlStateNormal];
    } else {
        cell.SelectButton.userInteractionEnabled = YES;

        [cell.SelectButton setTitle:@"加好友" forState:UIControlStateNormal];
        [cell.SelectButton setTitleColor:FENHONG_COLOR forState:UIControlStateNormal];
        //        [cell.SelectButton setImage:[UIImage imageNamed:@"lanjia"] forState:UIControlStateNormal];
        
        
        [cell.SelectButton addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 70;
    
}


//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    NSDictionary *friendArr=[self.tableDataFriends objectAtIndex:indexPath.row];
    NSNumber * fid = [friendArr objectForKey:@"id"];
    NSString * FriendID = [NSString stringWithFormat:@"%@",fid];
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([userid isEqualToString:FriendID]) {
        //TODO(Xc.) 这里注释了。不知道干什么的。
        //        MyCenterViewController * detailViewController = [[MyCenterViewController alloc]init];
        //        detailViewController.fromClass=@"ViewPersonalData";
        //        detailViewController.hidesBottomBarWhenPushed = YES;
        //        [self.navigationController pushViewController:detailViewController animated:YES];
        
    } else {
        
        //        FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
        //        detailViewController.FriendID = FriendID;
        //        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
    [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


//选择按钮 //添加好友
-(void)SelectButton:(UIButton *)sender{
    UIButton *btn = (UIButton *)sender;
    NSInteger index=btn.tag-10000;
    [_SearchText resignFirstResponder];
    NSDictionary *friendArr=[self.tableDataFriends objectAtIndex:index];
    NSString * fid = [friendArr objectForKey:@"id"];
    self.friendId = [NSString stringWithFormat:@"user_%@",fid];
    [self.view endEditing:YES];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"验证消息" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"发送", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = @"请输入20字以内的验证信息";
    alert.tag = 20000;
    
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==20000) {
        
        if (buttonIndex == 1 &&  [alertView textFieldAtIndex:0].text.length <=20) {
            [alertView resignFirstResponder];
            MsgAddFriendReqAPI * addFriendReq = [[MsgAddFriendReqAPI alloc]init];
            NSString * addmessage = @"";
            if ([alertView textFieldAtIndex:0].text != NULL && ![[alertView textFieldAtIndex:0].text isEqual:nil]) {
                addmessage= [alertView textFieldAtIndex:0].text;
            }
            [addFriendReq requestWithObject:@[_friendId,@(0),@(SessionTypeSessionTypeSingle),addmessage] Completion:^(id response, NSError *error) {
                
                
            }];
            NSString * friendName = [NSString stringWithFormat:@"您的请求已发送给,请耐心等待对方的确认"] ;
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:friendName message:@"" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }else{
            if (buttonIndex ==0) {
            }
        }
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
