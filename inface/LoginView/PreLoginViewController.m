//
//  PreLoginViewController.m
//  inface
//
//  Created by xigesi on 15/5/19.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PreLoginViewController.h"

@interface PreLoginViewController ()

@end

@implementation PreLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;

    NSString * key = [NSString stringWithFormat:@"%d_%d",(int)ScreenWidth,(int)ScreenHeight];
    if ([key isEqualToString:@"320_480"]) {

        self.loginBackGround.image = [UIImage imageNamed:@"640_960"];
    } else if ([key isEqualToString:@"320_568"]){

        self.loginBackGround.image = [UIImage imageNamed:@"640_1136"];
    } else if ([key isEqualToString:@"375_667"]){

        self.loginBackGround.image = [UIImage imageNamed:@"750_1334"];
    } else if ([key isEqualToString:@"414_736"]){

        self.loginBackGround.image = [UIImage imageNamed:@"1242_2208"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
