//
//  LaunchViewController.m
//  inface
//
//  Created by appleone on 15/10/29.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "LaunchViewController.h"

@interface LaunchViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *launchImageView;

@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.launchImageView.image = [UIImage imageNamed:@"guanggao"];
    self.navigationController.navigationBarHidden = YES;
    [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(disMIssSelf) userInfo:nil repeats:NO];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)disMIssSelf
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
