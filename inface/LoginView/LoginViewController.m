//
//  LoginViewController.m
//  inface
//
//  Created by Mac on 15/4/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "LoginViewController.h"

#import "GetWordStorysByPagecount.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import <Masonry/Masonry.h>
@interface LoginViewController ()
@property(nonatomic,copy)MBProgressHUD * hud;
@end

@implementation LoginViewController
-(MBProgressHUD *)hud{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc]initWithView:self.view];
    }
    return _hud;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
//    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"loginBackGround"]];
    
    self.aBackgroundView.layer.borderWidth = 1;
    self.aBackgroundView.layer.borderColor = BLUECOLOR.CGColor;
    self.aBackgroundView.layer.cornerRadius = 4;
    self.aBackgroundView.clipsToBounds = YES;
    self.aBackgroundView.backgroundColor=[UIColor whiteColor];
    
    self.XianLabel.backgroundColor=BLUECOLOR;
    
    
    self.registerBtn.layer.borderWidth = 1;
    self.registerBtn.layer.borderColor = BLUECOLOR.CGColor;
    self.registerBtn.layer.cornerRadius = 4;
    self.registerBtn.clipsToBounds = YES;
    [self.registerBtn addTarget:self action:@selector(registerBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.registerBtn setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    self.registerBtn.titleLabel.font=[UIFont systemFontOfSize:17];
    [self.registerBtn setBackgroundColor:[UIColor whiteColor]];
    
    self.loginBtn.layer.borderWidth = 1;
    self.loginBtn.layer.borderColor = BLUECOLOR.CGColor;
    self.loginBtn.layer.cornerRadius = 4;
    self.loginBtn.clipsToBounds = YES;
    [self.loginBtn addTarget:self action:@selector(loginBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.loginBtn.titleLabel.font=[UIFont systemFontOfSize:17];
    [self.loginBtn setBackgroundColor:BLUECOLOR];
    
    self.aDashLine.startPoint=CGPointMake(0, 1);
    self.aDashLine.endPoint=CGPointMake(0, ScreenHeight-40);
    self.aDashLine.lineColor=BLUECOLOR;
    
    self.navigationItem.title=@"登录";

    _tencentOAuth = [[TencentOAuth alloc]initWithAppId:@"1104542960" andDelegate:self];
    
    
    _permissions = [NSMutableArray arrayWithObjects:
                    kOPEN_PERMISSION_GET_USER_INFO,
                    kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                    kOPEN_PERMISSION_ADD_ALBUM,
                    kOPEN_PERMISSION_ADD_IDOL,
                    kOPEN_PERMISSION_ADD_ONE_BLOG,
                    kOPEN_PERMISSION_ADD_PIC_T,
                    kOPEN_PERMISSION_ADD_SHARE,
                    kOPEN_PERMISSION_ADD_TOPIC,
                    kOPEN_PERMISSION_CHECK_PAGE_FANS,
                    kOPEN_PERMISSION_DEL_IDOL,
                    kOPEN_PERMISSION_DEL_T,
                    kOPEN_PERMISSION_GET_FANSLIST,
                    kOPEN_PERMISSION_GET_IDOLLIST,
                    kOPEN_PERMISSION_GET_INFO,
                    kOPEN_PERMISSION_GET_OTHER_INFO,
                    kOPEN_PERMISSION_GET_REPOST_LIST,
                    kOPEN_PERMISSION_LIST_ALBUM,
                    kOPEN_PERMISSION_UPLOAD_PIC,
                    kOPEN_PERMISSION_GET_VIP_INFO,
                    kOPEN_PERMISSION_GET_VIP_RICH_INFO,
                    kOPEN_PERMISSION_GET_INTIMATE_FRIENDS_WEIBO,
                    kOPEN_PERMISSION_MATCH_NICK_TIPS_WEIBO,
                    nil];
    
    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]]) {
        //未安装QQ
        self.QQLoginBtn.hidden=YES;
    }

    
    self.phoneTextField.keyboardType=UIKeyboardTypeNumberPad;
    self.phoneTextField.delegate = self;
    self.passwordTextField.delegate = self;
  
    
    [self.forgetBtn addTarget:self action:@selector(forgetBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.QQLoginBtn addTarget:self action:@selector(tencentLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary * dic = @{@"320_480":@"640_960",@"320_568":@"640_1136",@"375_667":@"750_1334",@"414_736":@"1242_2208"};
    NSString * key = [NSString stringWithFormat:@"%d_%d",(int)ScreenWidth,(int)ScreenHeight];
    if ([key isEqualToString:@"320_480"]) {
        self.QQandRegistBtn.constant=20;
        self.QQandForgetBtn.constant=20;
    } else if ([key isEqualToString:@"320_568"]){
        self.QQandRegistBtn.constant=60;
        self.QQandForgetBtn.constant=50;
    } else if ([key isEqualToString:@"375_667"]){
        self.QQandRegistBtn.constant=75;
        self.QQandForgetBtn.constant=113;
    } else if ([key isEqualToString:@"414_736"]){
        self.QQandRegistBtn.constant=120;
        self.QQandForgetBtn.constant=80;
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)loginBtnAction:(UIButton *)sender{
    
    if ([self.phoneTextField.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入手机号" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if(self.phoneTextField.text.length<11){
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"手机号输入有误" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([self.passwordTextField.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入密码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    [self toLoginUserName:[NSString stringWithFormat:@"mb:%@",self.phoneTextField.text]  withPassword:self.passwordTextField.text];
    
}


- (void)forgetBtnAction:(UIButton *)sender{
    
    RegisterViewController * detailViewController = [[RegisterViewController alloc] init];
    detailViewController.fromForgetPassword=@"ForgetPassword";
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}
- (void)registerBtnAction:(UIButton *)sender{
    
    RegisterViewController * detailViewController = [[RegisterViewController alloc] init];
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}





-(void)toLoginUserName:(NSString *)userName withPassword:(NSString *)password{
    [self.view endEditing:YES];
    if ([userName rangeOfString:@"mb:"].length!=0&&[userName rangeOfString:@"mb:"].location == 0) {
        [self.hud setLabelText:@"正在登陆，请稍后."];
        [self.hud setMode:MBProgressHUDModeIndeterminate];
        [self.view addSubview:self.hud];
        [self.hud show:YES];
        [self.view bringSubviewToFront:self.hud];
    }
    [[LoginModule instance] loginWithUsername:userName password:password success:^(DDUserEntity *user) {
        if (user) {
            //            BaseTabBarViewController *aBaseTabBarViewController = [[BaseTabBarViewController alloc] init];
            //            [self presentViewController:aBaseTabBarViewController animated:YES completion:nil];
            NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            NSString * token = [RuntimeStatus instance].token;
            if (userid && token) {
                NSDictionary * deviceDic = @{@"uid":[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"],@"device":[RuntimeStatus instance].token};
                [HttpTool postWithPath:@"SubmitDeiceInfo" params:deviceDic success:^(id JSON) {
                    
                    
                } failure:^(NSError *error) {
                    
                    
                }];
            }

            [GetGroupMemberNotify shareInsatance];
            
 
                [[GetAllFamilysAndStorys shareInstance]getAllFamilysAndStorysSuccess:^(id json) {
                    [self.hud setHidden:YES];
                    BaseTabBarViewController * baseViewController = json;
                    [self presentViewController:baseViewController animated:YES completion:nil];
                    [[GetUnreadNotify shareInsatance]getUnreadNotify];
                    AppDelegate * delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                    
                    delegate.aBaseTabBarViewController = json;

                }failure:^(id failure) {
                    [self.hud setHidden:YES];

                    
                } ];
            
            
            [[GetAllFamilysAndStorys shareInstance]GetFavoriteEmotionList];
            

        }
    } failure:^(NSString *error) {
        [self.hud setLabelText:@"密码错误"];
        [self.hud setMode:MBProgressHUDModeText];
        [self.hud hide:YES afterDelay:2];
        BaseTabBarViewController * baseViewController = [[BaseTabBarViewController alloc]init];
        [self presentViewController:baseViewController animated:YES completion:nil];
        AppDelegate * delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        delegate.aBaseTabBarViewController = baseViewController;

//        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"密码错误" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//        [alertview show];
    }];
    
}




-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden=YES;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    self.navigationController.navigationBar.hidden=NO;

}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//腾讯登录
- (void)tencentLogin:(id)sender
{
    [_tencentOAuth authorize:_permissions inSafari:NO];

}

//#pragma-mark QQ登录成功后的回调
-(void)tencentDidLogin
{
    if (_tencentOAuth.accessToken && [_tencentOAuth.accessToken length]!= 0) {
        [self.hud setLabelText:@"正在登陆，请稍后"];
        [self.view addSubview:self.hud];
        [self.view bringSubviewToFront:self.hud];
         [self.hud setMode:MBProgressHUDModeIndeterminate];
        [self.hud show:YES];
        if ([_tencentOAuth isSessionValid]==YES) {
            
            
        }else{
            
        }
        if ([_tencentOAuth getUserInfo]==YES) {
            
        }
    }else{
        
    }
}

#pragma-mark 没有网络连接
-(void)tencentDidNotNetWork
{

}



#pragma-mark  QQ登录用户信息的获取
-(void)getUserInfoResponse:(APIResponse *)response
{
    NSString * qqUserName = [response.jsonResponse objectForKey:@"nickname"];
    
    NSString * qqGender = [response.jsonResponse objectForKey:@"gender"];
    
    NSString * imageStr = [response.jsonResponse objectForKey:@"figureurl_qq_1"];
    
    NSString *sex=@"0";
    if ([qqGender isEqualToString:@"男"]) {
        sex=@"1";
    }
    
    NSString *rid=[NSString stringWithFormat:@"qq:%@",_tencentOAuth.openId];
    if (!(qqUserName != nil&&sex!=nil&&imageStr!=nil&&rid!=nil)) {
        return;
    }
    //注册接口(Post)  bd: 百度账号 qq: QQ账号 sn: 新浪 mb: 手机
    NSDictionary *params = @{@"nickname":qqUserName,@"gender":sex,@"portraitImg":imageStr,@"rid":rid,@"password":[MyMD5 md5:@"yuxi"],@"token":@"110"};

    [HttpTool postWithPath:@"Register" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {

            [self toLoginUserName:rid withPassword:@"yuxi"];
            
        }else if ([[JSON objectForKey:@"result"]integerValue]==2) {
            
            [self toLoginUserName:rid withPassword:@"yuxi"];
            
        }else{

//            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//            [alertview show];
            [self.hud setLabelText:[JSON objectForKey:@"describe"]];
            [self.hud hide:YES afterDelay:2];

        }

    } failure:^(NSError *error) {
    }];

}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.phoneTextField resignFirstResponder];
    
    [self.passwordTextField resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
