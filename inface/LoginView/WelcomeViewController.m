 //
//  WelcomeViewController.m
//  NewTeacher
//
//  Created by define on 14-9-18.
//  Copyright (c) 2014年 dante. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden=YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.navigationController.navigationBar.hidden=NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.view.backgroundColor=[UIColor whiteColor];
    

    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    self.scrollView.clipsToBounds = YES;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    
    [self.view addSubview:self.scrollView];
    
    NSMutableArray *aimageArr=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"loading1"],[UIImage imageNamed:@"loading2"],[UIImage imageNamed:@"loading3"],[UIImage imageNamed:@"loading4"], nil];

    
    for (int i = 0; i < aimageArr.count; i++) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(ScreenWidth * i, 0, ScreenWidth, ScreenHeight)];
        imageView.userInteractionEnabled=YES;
        imageView.tag=i+1000;
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        imageView.image = [aimageArr objectAtIndex:i];
        
        [self.scrollView addSubview:imageView];
        
        if (i==aimageArr.count-1) {
            //由此进入
            UIButton *aButtonzhuce=[UIButton buttonWithType:UIButtonTypeCustom];
            
            if (ScreenHeight == 480.0) {
                aButtonzhuce.frame=CGRectMake(ScreenWidth/2.0-135/2.0,ScreenHeight-70,135,40);
            }else{
                aButtonzhuce.frame=CGRectMake(ScreenWidth/2.0-135/2.0,ScreenHeight-120,135,40);
            }
            [aButtonzhuce setImage:[UIImage imageNamed:@"lijikaiqi"] forState:UIControlStateNormal];
            [aButtonzhuce setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
            [aButtonzhuce addTarget:self action:@selector(callAction) forControlEvents:UIControlEventTouchUpInside];
            
            [imageView addSubview:aButtonzhuce];
        }
        
    }
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, ScreenHeight-30, ScreenWidth, 30)];  //创建UIPageControl，位置在屏幕最下方
    self.pageControl.numberOfPages = aimageArr.count;
    self.pageControl.currentPage = 0;
    self.pageControl.pageIndicatorTintColor=GRAY_COLOR;
    self.pageControl.currentPageIndicatorTintColor=BLUECOLOR;
    self.pageControl.center=CGPointMake(ScreenWidth/2.0, ScreenHeight-15);
    [self.pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_pageControl];  //将UIPageControl添加到主界面上。
    
    
    self.scrollView.contentSize = CGSizeMake(ScreenWidth*aimageArr.count, ScreenHeight);

    

    

}

//立即体验
- (void)callAction{
    if ([RuntimeStatus instance].token) {
        NSDictionary * deviceDic = @{@"uid":@"0",@"device":[RuntimeStatus instance].pushToken};
        [HttpTool postWithPath:@"SubmitDeiceInfo" params:deviceDic success:^(id JSON) {
            
            
        } failure:^(NSError *error) {
            
            
        }];

    }
    LoginViewController * login = [[LoginViewController alloc]init];
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:login];
    [self presentViewController:nav animated:YES completion:nil];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)ascrollView
{
    //更新UIPageControl的当前页
    CGPoint offset = _scrollView.contentOffset;
    CGRect bounds = _scrollView.frame;
    [_pageControl setCurrentPage:offset.x / bounds.size.width];
}

- (void)pageTurn:(UIPageControl*)sender
{
    //令UIScrollView做出相应的滑动显示

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
