//
//  LoginViewController.h
//  inface
//
//  Created by Mac on 15/4/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//三方登录和手机登录入口页面
#import <UIKit/UIKit.h>
#import <TencentOpenAPI/TencentOAuth.h>

#import <TencentOpenAPI/TencentOAuthObject.h>
#import <TencentOpenAPI/TencentMessageObject.h>

#import <TencentOpenAPI/sdkdef.h>

#import "SendPushTokenAPI.h"

#import "RegisterViewController.h"
#import "LoginModule.h"
#import "BaseTabBarViewController.h"
#import "GetGroupMemberNotify.h"
#import "GetUnreadNotify.h"
#import "GetAllFamilysAndStorys.h"
#import "DashesLineView.h"
@interface LoginViewController : BaseViewController<UITextFieldDelegate>

{
    
    TencentOAuth * _tencentOAuth;
    NSArray  * _permissions;
}


@property (weak, nonatomic) IBOutlet UIButton *QQLoginBtn;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgetBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UILabel *XianLabel;
@property (weak, nonatomic) IBOutlet DashesLineView *aDashLine;
@property (weak, nonatomic) IBOutlet UIView *aBackgroundView;
@property(assign,nonatomic)    BOOL   isAnimating;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *QQandRegistBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *QQandForgetBtn;


@end
