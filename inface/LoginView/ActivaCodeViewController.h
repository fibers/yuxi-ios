//
//  ActivaCodeViewController.h
//  inface
//
//  Created by appleone on 15/6/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "PlaceholderTextView.h"
@interface ActivaCodeViewController : BaseViewController<UITextViewDelegate>
@property (weak, nonatomic) PlaceholderTextView *chatInputTextView;
- (IBAction)activeSender:(id)sender;

@end
