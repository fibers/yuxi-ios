//
//  YXBindMobilePhoneViewController.h
//  inface
//
//  Created by 邢程 on 15/12/8.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kBindPhoneNumber @"bindPhoneNumber"
@interface YXBindMobilePhoneViewController : BaseViewController
///是否显示关闭Bar 默认NO
@property(nonatomic,assign,getter=isShowCloseBar)BOOL showCloseBar;
@end
