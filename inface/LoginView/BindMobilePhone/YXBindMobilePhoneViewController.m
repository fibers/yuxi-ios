//
//  YXBindMobilePhoneViewController.m
//  inface
//
//  Created by 邢程 on 15/12/8.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXBindMobilePhoneViewController.h"

@interface YXBindMobilePhoneViewController (){
    NSTimer *timers;
    BOOL timeStart;
    NSString *vcode;//获取的验证码
}
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *codeTextfield;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property (weak, nonatomic) IBOutlet UIImageView *PhoneView;
@property (weak, nonatomic) IBOutlet UIImageView *CodeView;

@property (weak,nonatomic)IBOutlet UIButton * verifyCodeBtn;
@property (weak, nonatomic) IBOutlet UIView *closeV;


@end

@implementation YXBindMobilePhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.closeV setHidden:!self.isShowCloseBar];
    [self.navigationItem setTitle:@"绑定手机"];
    self.PhoneView.layer.borderWidth = 1;
    self.PhoneView.layer.borderColor = BLUECOLOR.CGColor;
    self.PhoneView.layer.cornerRadius = 4;
    self.PhoneView.clipsToBounds = YES;
    
    
    self.CodeView.layer.borderWidth = 1;
    self.CodeView.layer.borderColor = BLUECOLOR.CGColor;
    self.CodeView.layer.cornerRadius = 4;
    self.CodeView.clipsToBounds = YES;
    
    self.okBtn.layer.borderWidth = 1;
    self.okBtn.layer.borderColor = BLUECOLOR.CGColor;
    self.okBtn.layer.cornerRadius = 4;
    self.okBtn.clipsToBounds = YES;
    self.okBtn.backgroundColor=BLUECOLOR;
    [self.okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.verifyCodeBtn.layer.borderWidth = 1;
    self.verifyCodeBtn.layer.borderColor = BLUECOLOR.CGColor;
    self.verifyCodeBtn.layer.cornerRadius = 4;
    self.verifyCodeBtn.clipsToBounds = YES;
    self.verifyCodeBtn.backgroundColor=BLUECOLOR;
    [self.verifyCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.verifyCodeBtn addTarget:self action:@selector(verifyCodeBtClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.okBtn addTarget:self action:@selector(okBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.phoneTextField.keyboardType=UIKeyboardTypeNumberPad;
    
}
//选择按钮
-(void)okBtnClicked:(UIButton *)sender{
    
    
    if ([self.phoneTextField.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入手机号" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if(self.phoneTextField.text.length<11){
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"手机号输入有误" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([self.codeTextfield.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入验证码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    //验证码检测接口(Post)
    NSDictionary *params = @{@"uid":USERID,@"mobileno":self.phoneTextField.text,@"vcode":self.codeTextfield.text,@"token":@"110"};
    
    [HttpTool postWithPath:@"VCodeCheck" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            //验证通过
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"绑定成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            [[NSUserDefaults standardUserDefaults]setObject:self.phoneTextField.text forKey:kBindPhoneNumber];
            [self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];

        }else{
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"验证码错误，请重试！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
        }
    } failure:^(NSError *error) {
        
    }];
    
    
}

- (IBAction)closeViewController:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    
}
//获取验证码
-(void)verifyCodeBtClicked:(id)sender{
    if([self.phoneTextField.text length]<11){
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入正确手机号码！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
    }
    
    //请求验证码接口(Post)
    NSDictionary *params = @{@"mobileno":self.phoneTextField.text,@"token":@"110"};
    
    [HttpTool postWithPath:@"VCodeRequest" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            //            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"验证码已经发送到您的手机上" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            //            [alertview show];
            
            timers=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
            
            self.verifyCodeBtn.userInteractionEnabled=NO;
            timeStart = YES;
            
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];
    
    
}



//60s倒计时
- (void)timerFireMethod:(NSTimer *)theTimer

{
    
    NSCalendar *cal = [NSCalendar currentCalendar];//定义一个NSCalendar对象
    
    NSDateComponents *endTime = [[NSDateComponents alloc] init];    //初始化目标时间...
    
    NSDate *today = [NSDate date];    //得到当前时间
    
    
    
    NSDate *date = [NSDate dateWithTimeInterval:60 sinceDate:today];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    
    
    static int year;
    
    static int month;
    
    static int day;
    
    static int hour;
    
    static int minute;
    
    static int second;
    
    if(timeStart) {//从NSDate中取出年月日，时分秒，但是只能取一次
        
        year = [[dateString substringWithRange:NSMakeRange(0, 4)] intValue];
        
        month = [[dateString substringWithRange:NSMakeRange(5, 2)] intValue];
        
        day = [[dateString substringWithRange:NSMakeRange(8, 2)] intValue];
        
        hour = [[dateString substringWithRange:NSMakeRange(11, 2)] intValue];
        
        minute = [[dateString substringWithRange:NSMakeRange(14, 2)] intValue];
        
        second = [[dateString substringWithRange:NSMakeRange(17, 2)] intValue];
        
        timeStart= NO;
        
    }
    
    
    
    [endTime setYear:year];
    
    [endTime setMonth:month];
    
    [endTime setDay:day];
    
    [endTime setHour:hour];
    
    [endTime setMinute:minute];
    
    [endTime setSecond:second];
    
    NSDate *todate = [cal dateFromComponents:endTime]; //把目标时间装载入date
    //用来得到具体的时差，是为了统一成北京时间
    
    unsigned int unitFlags = NSYearCalendarUnit| NSMonthCalendarUnit| NSDayCalendarUnit| NSHourCalendarUnit| NSMinuteCalendarUnit| NSSecondCalendarUnit;
    
    NSDateComponents *d = [cal components:unitFlags fromDate:today toDate:todate options:0];
    
    
    
    if([d second] > 0) {
        
        
        [self.verifyCodeBtn setTitle:[NSString stringWithFormat:@"%ld 秒",(long)[d second]] forState:UIControlStateNormal];
        //计时尚未结束，do_something
        
        
    } else if([d second] == 0) {
        
        [self.verifyCodeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
        self.verifyCodeBtn.userInteractionEnabled=YES;
        
        //计时1分钟结束，do_something
        
        
        
    } else{
        
        [theTimer invalidate];
        
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
