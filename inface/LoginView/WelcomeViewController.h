//
//  WelcomeViewController.h
//  NewTeacher
//
//  Created by define on 14-9-18.
//  Copyright (c) 2014年 dante. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : BaseViewController<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;

@end
