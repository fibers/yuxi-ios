//
//  ActivaCodeViewController.m
//  inface
//
//  Created by appleone on 15/6/12.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ActivaCodeViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
@interface ActivaCodeViewController ()

@end

@implementation ActivaCodeViewController
{
    AppDelegate * appDelegate;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    self.navigationItem.title=@"激活账号";

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ditu"]];
    
    PlaceholderTextView *atextview=[[PlaceholderTextView alloc]initWithFrame:CGRectMake(10, 0, ScreenWidth-70, 80)];
    if (ScreenHeight<=480) {
        //iphone4
        atextview.frame=CGRectMake(70, 85, 100, 50);
    } else if (ScreenHeight>480&&ScreenHeight<=568){
        //iphone5
        atextview.frame=CGRectMake(70, 105, 100, 50);
    } else if (ScreenHeight>568&&ScreenHeight<=667){
        //iphone6
        atextview.frame=CGRectMake(70, 125, 140, 50);
    } else if (ScreenHeight>667&&ScreenHeight<=960){
        //iphone6plus
        atextview.frame=CGRectMake(80, 150, 160, 50);
    }
    [self.view addSubview:atextview];
    self.chatInputTextView=atextview;
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.font = [UIFont systemFontOfSize:20.0f];
    self.chatInputTextView.placeholder = @"指码开门";
    self.chatInputTextView.textColor=[UIColor whiteColor];
    self.chatInputTextView.placeholderFont=[UIFont boldSystemFontOfSize:20];
    self.chatInputTextView.placeholderColor=[UIColor whiteColor];
    self.chatInputTextView.backgroundColor=[UIColor clearColor];

//    self.chatInputTextView.keyboardType=UIKeyboardTypeNumberPad;
    // Do any additional setup after loading the view from its nib.
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)activeSender:(id)sender
{

    if ([self.chatInputTextView.text isEmpty]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请输入指码" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [HttpTool postWithPath:@"VerifyInviteCode" params:@{@"code":self.chatInputTextView.text} success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
//            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
//            [alert show];
            [[NSUserDefaults standardUserDefaults]setObject:@"true" forKey:@"activeSuccess"];
            LoginViewController * login = [[LoginViewController alloc]init];
            
            appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
            appDelegate.window.rootViewController = nav;
            
            
        }else{

            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
        }
        
    } failure:^(NSError *error) {
        
        
    }];

}
@end
