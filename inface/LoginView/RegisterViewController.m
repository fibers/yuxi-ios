//
//  RegisterViewController.m
//  inface
//
//  Created by Mac on 15/4/27.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [timers invalidate];
    self.loginBtn.userInteractionEnabled=YES;
    [self.loginBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
}

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    self.PhoneView.layer.borderWidth = 1;
    self.PhoneView.layer.borderColor = BLUECOLOR.CGColor;
    self.PhoneView.layer.cornerRadius = 4;
    self.PhoneView.clipsToBounds = YES;
    
    
    self.CodeView.layer.borderWidth = 1;
    self.CodeView.layer.borderColor = BLUECOLOR.CGColor;
    self.CodeView.layer.cornerRadius = 4;
    self.CodeView.clipsToBounds = YES;
    
    
    self.PasswordView.layer.borderWidth = 1;
    self.PasswordView.layer.borderColor = BLUECOLOR.CGColor;
    self.PasswordView.layer.cornerRadius = 4;
    self.PasswordView.clipsToBounds = YES;
    
    
    if ([self.fromForgetPassword isEqualToString:@"ForgetPassword"]) {
        
        self.navigationItem.title=@"忘记密码";
        
        fuxuankuang=YES;
        self.fuxuanbtn.hidden=YES;
        self.agreeLabel.hidden=YES;
        self.xieyiBtn.hidden=YES;
    }else{
        
        self.navigationItem.title=@"注册";
        
        fuxuankuang=YES;
        self.fuxuanbtn.hidden=NO;
        self.agreeLabel.hidden=NO;
        self.xieyiBtn.hidden=NO;
        
        [self.fuxuanbtn setImage:[UIImage imageNamed:@"zhuceed"] forState:UIControlStateNormal];
        [self.fuxuanbtn addTarget:self action:@selector(tongyi:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.xieyiBtn addTarget:self action:@selector(kanxieyi:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.loginBtn.layer.borderWidth = 1;
    self.loginBtn.layer.borderColor = BLUECOLOR.CGColor;
    self.loginBtn.layer.cornerRadius = 4;
    self.loginBtn.clipsToBounds = YES;
    self.loginBtn.backgroundColor=BLUECOLOR;
    [self.loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.loginBtn addTarget:self action:@selector(loginBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    self.phoneTextField.keyboardType=UIKeyboardTypeNumberPad;
    
    self.SaveBtn.layer.borderWidth = 1;
    self.SaveBtn.layer.borderColor = BLUECOLOR.CGColor;
    self.SaveBtn.layer.cornerRadius = 4;
    self.SaveBtn.clipsToBounds = YES;
    self.SaveBtn.backgroundColor=BLUECOLOR;
    [self.SaveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.SaveBtn addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
}

//选择按钮
-(void)SelectButton:(UIButton *)sender{

    if (fuxuankuang==NO) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"请同意语戏用户协议" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    if ([self.phoneTextField.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入手机号" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if(self.phoneTextField.text.length<11){
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"手机号输入有误" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([self.codeTextfield.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入验证码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([self.passwordTextField.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入密码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    
    //验证码检测接口(Post)
    NSDictionary *params = @{@"mobileno":self.phoneTextField.text,@"vcode":self.codeTextfield.text,@"token":@"110"};
    
    [HttpTool postWithPath:@"VCodeCheck" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            //验证通过
            
            
            if ([self.fromForgetPassword isEqualToString:@"ForgetPassword"]) {
                
                //修改密码接口(Post)
                
                //        BaseTabBarViewController *aBaseTabBarViewController = [[BaseTabBarViewController alloc] init];
                //        [self presentViewController:aBaseTabBarViewController animated:YES completion:nil];
                
                
                //修改密码接口(Post)
                
                NSString *rid=[NSString stringWithFormat:@"mb:%@",self.phoneTextField.text];
                //注册接口(Post)  bd: 百度账号 qq: QQ账号 sn: 新浪 mb: 手机
                NSDictionary *params = @{@"rid":rid,@"password":[MyMD5 md5:self.passwordTextField.text],@"token":@"110"};
                
                [HttpTool postWithPath:@"ChangePass" params:params success:^(id JSON) {
                    if ([[JSON objectForKey:@"result"]integerValue]==1) {
                        
                        [self toLoginUserName:[NSString stringWithFormat:@"mb:%@",self.phoneTextField.text] withPassword:self.passwordTextField.text];
                        
                    }else if ([[JSON objectForKey:@"result"]integerValue]==2){
                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertview show];
                        
                    }
                    
                } failure:^(NSError *error) {
                }];
                
                
                
            } else {
                //用户注册接口(Post)
                
                NSString *rid=[NSString stringWithFormat:@"mb:%@",self.phoneTextField.text];
                //注册接口(Post)  bd: 百度账号 qq: QQ账号 sn: 新浪 mb: 手机
                NSDictionary *params = @{@"nickname":@"",@"gender":@"0",@"portraitImg":@"",@"rid":rid,@"password":[MyMD5 md5:self.passwordTextField.text],@"token":@"110"};
                
                [HttpTool postWithPath:@"Register" params:params success:^(id JSON) {
                    if ([[JSON objectForKey:@"result"]integerValue]==1) {
                        
                        [self toLoginUserName:[NSString stringWithFormat:@"mb:%@",self.phoneTextField.text] withPassword:self.passwordTextField.text];
                    }else if ([[JSON objectForKey:@"result"]integerValue]==2){
                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertview show];
                        
                    }
                    
                } failure:^(NSError *error) {
                }];
                
            }
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"验证码输入有误" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];
    
}

//获取验证码
-(void)loginBtn:(id)sender{
    
    
    //请求验证码接口(Post)
    NSDictionary *params = @{@"mobileno":self.phoneTextField.text,@"token":@"110"};
    
    [HttpTool postWithPath:@"VCodeRequest" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
//            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"验证码已经发送到您的手机上" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//            [alertview show];
            
            timers=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
            
            self.loginBtn.userInteractionEnabled=NO;
            timeStart = YES;
            
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];


}



//60s倒计时
- (void)timerFireMethod:(NSTimer *)theTimer

{
    
    NSCalendar *cal = [NSCalendar currentCalendar];//定义一个NSCalendar对象
    
    NSDateComponents *endTime = [[NSDateComponents alloc] init];    //初始化目标时间...
    
    NSDate *today = [NSDate date];    //得到当前时间
    
    
    
    NSDate *date = [NSDate dateWithTimeInterval:60 sinceDate:today];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    
    
    static int year;
    
    static int month;
    
    static int day;
    
    static int hour;
    
    static int minute;
    
    static int second;
    
    if(timeStart) {//从NSDate中取出年月日，时分秒，但是只能取一次
        
        year = [[dateString substringWithRange:NSMakeRange(0, 4)] intValue];
        
        month = [[dateString substringWithRange:NSMakeRange(5, 2)] intValue];
        
        day = [[dateString substringWithRange:NSMakeRange(8, 2)] intValue];
        
        hour = [[dateString substringWithRange:NSMakeRange(11, 2)] intValue];
        
        minute = [[dateString substringWithRange:NSMakeRange(14, 2)] intValue];
        
        second = [[dateString substringWithRange:NSMakeRange(17, 2)] intValue];
        
        timeStart= NO;
        
    }
    
    
    
    [endTime setYear:year];
    
    [endTime setMonth:month];
    
    [endTime setDay:day];
    
    [endTime setHour:hour];
    
    [endTime setMinute:minute];
    
    [endTime setSecond:second];
    
    NSDate *todate = [cal dateFromComponents:endTime]; //把目标时间装载入date
    //用来得到具体的时差，是为了统一成北京时间
    
    unsigned int unitFlags = NSYearCalendarUnit| NSMonthCalendarUnit| NSDayCalendarUnit| NSHourCalendarUnit| NSMinuteCalendarUnit| NSSecondCalendarUnit;
    
    NSDateComponents *d = [cal components:unitFlags fromDate:today toDate:todate options:0];
    
    
    
    if([d second] > 0) {
        
        
        [self.loginBtn setTitle:[NSString stringWithFormat:@"%ld 秒",(long)[d second]] forState:UIControlStateNormal];
        //计时尚未结束，do_something
        
        
    } else if([d second] == 0) {
        
        [self.loginBtn setTitle:@"重新获取" forState:UIControlStateNormal];
        self.loginBtn.userInteractionEnabled=YES;
        
        //计时1分钟结束，do_something
        
        
        
    } else{
        
        [theTimer invalidate];
        
    }
    
    
    
}


-(void)tongyi:(id)sender{
    
    if (fuxuankuang==NO) {
        [self.fuxuanbtn setImage:[UIImage imageNamed:@"zhuceed"] forState:UIControlStateNormal];
        
        fuxuankuang=YES;
        
    } else {
        [self.fuxuanbtn setImage:[UIImage imageNamed:@"unzhuce"] forState:UIControlStateNormal];
        
        fuxuankuang=NO;
    }
    
    
}

-(void)kanxieyi:(id)sender{
    
    HelpInformationViewController * detailViewController = [[HelpInformationViewController alloc] init];
    detailViewController.navTitle=@"语戏用户协议";
    detailViewController.contentUrl=[NSString stringWithFormat:@"%@/GetContact",ROOT_URL];
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    
}


-(void)toLoginUserName:(NSString *)userName withPassword:(NSString *)password{
    
    [[LoginModule instance] loginWithUsername:userName password:password success:^(DDUserEntity *user) {
        if (user) {
            
            //            BaseTabBarViewController *aBaseTabBarViewController = [[BaseTabBarViewController alloc] init];
            //            [self presentViewController:aBaseTabBarViewController animated:YES completion:nil];
            
            [GetGroupMemberNotify shareInsatance];
            
            [[GetAllFamilysAndStorys shareInstance]getAllFamilysAndStorysSuccess:^(id json) {
                
                BaseTabBarViewController * baseViewController = json;
                [self presentViewController:baseViewController animated:YES completion:nil];
                [[GetUnreadNotify shareInsatance]getUnreadNotify];

            }failure:^(id failure) {
                
                BaseTabBarViewController * baseViewController = failure;
                [self presentViewController:baseViewController animated:YES completion:nil];
            }];
            
            [[GetAllFamilysAndStorys shareInstance]GetFavoriteEmotionList];
            

            
        }
    } failure:^(NSString *error) {
//注册失败不让进app
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"错误" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
    }];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.phoneTextField resignFirstResponder];
    
    [self.codeTextfield resignFirstResponder];
    
    [self.passwordTextField resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
