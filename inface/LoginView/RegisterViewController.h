//
//  RegisterViewController.h
//  inface
//
//  Created by Mac on 15/4/27.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//通过手机号获得验证码页面 注册和忘记密码流程需要
#import "BaseViewController.h"
#import "HelpInformationViewController.h"

@interface RegisterViewController : BaseViewController
{
    NSTimer *timers;
    BOOL timeStart;
    BOOL fuxuankuang;
    NSString *vcode;//获取的验证码
    
}
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *codeTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@property (strong, nonatomic) IBOutlet UIButton *fuxuanbtn;
@property (strong, nonatomic) IBOutlet UIButton *xieyiBtn;
@property (strong, nonatomic) IBOutlet UILabel *agreeLabel;

@property (strong, nonatomic)NSString *fromForgetPassword;
@property (weak, nonatomic) IBOutlet UIButton *SaveBtn;
@property (weak, nonatomic) IBOutlet UIImageView *PhoneView;
@property (weak, nonatomic) IBOutlet UIImageView *CodeView;
@property (weak, nonatomic) IBOutlet UIImageView *PasswordView;
@property(assign,nonatomic)    BOOL   isAnimating;

@end
