//
//  HZSButton.m
//  TabbarDemo
//
//  Created by Mac on 15/4/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "HZSButton.h"

@implementation HZSButton


#pragma mark 设置Button内部的image的范围

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageW = contentRect.size.width* 0.6;
    CGFloat imageH = contentRect.size. height* 0.6;
    return CGRectMake(contentRect.size.width/2.0-contentRect.size. height* 0.6/2.0, 5, imageW, imageH);
}

//设置Button内部的title的范围

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleY = contentRect.size.height * 0.7;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height - titleY;
    return CGRectMake(0, titleY, titleW, titleH);
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
