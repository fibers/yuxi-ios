//
//  SGXEmotionContent.h
//  SoMateSecrect
//
//  Created by guoq on 15-1-17.
//  Copyright (c) 2015年 SiGeXi Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SGXEmotionContent : NSObject

+ (instancetype)sharedClient;

- (NSMutableArray *) getEmotionArr;


@end
