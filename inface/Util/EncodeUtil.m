//
//  EncodeUtil.m
//  inface
//
//  Created by dev01 on 15/8/16.
//  Copyright (c) 2015年 Gouq. All rights reserved.
//

#import "EncodeUtil.h"



@interface EncodeUtil() {

}



@end

static NSMutableDictionary * emojiDic;

@implementation EncodeUtil



+ (void) getEmojilist
{
    NSString *filePath  = [[NSBundle mainBundle] pathForResource:@"emj" ofType:@"plist"];
    NSArray *decodeDict = [[NSArray alloc] initWithContentsOfFile:filePath];
    
    if (emojiDic.count > 0) {
        return;
    }
    
    emojiDic            = [[NSMutableDictionary alloc] init];
    
    int i = 0;
    for (NSDictionary *dic in decodeDict) {
        NSString    *utf16code   =   [dic objectForKey:@"UTF16_Code"];
        NSString    *utf8code    =   [dic objectForKey:@"UTF8_Code"];
        utf16code   =   [utf16code stringByReplacingOccurrencesOfString:@"'"
                                                             withString:@""];
        utf16code   =   [utf16code stringByReplacingOccurrencesOfString:@" "
                                                             withString:@""];
        utf16code   =   [utf16code stringByReplacingOccurrencesOfString:@"0x"
                                                             withString:@""];
        
        //对于数字的场合，不进行处理 比如 0x31 0x20 0xeb
        if ([utf16code length] % 4 != 0) {
            continue;
        }
        NSString    *temp   =   [self intFromHexString:utf16code];
        utf8code    =   [utf8code stringByReplacingOccurrencesOfString:@"'" withString:@""];
        utf8code    =   [utf8code stringByReplacingOccurrencesOfString:@" " withString:@""];
        utf8code    =   [utf8code stringByReplacingOccurrencesOfString:@"0x" withString:@""];
        [self getUtf8buf:utf8code];
        [emojiDic setObject:utf8code forKey:temp];
    }
}


void UTF16ToUTF8(UTF16* pUTF16Start, UTF16* pUTF16End, UTF8* pUTF8Start, UTF8* pUTF8End)
{
    UTF16* pTempUTF16 = pUTF16Start;
    UTF8* pTempUTF8 = pUTF8Start;
    
    while (pTempUTF16 < pUTF16End)
    {
        if (*pTempUTF16 <= UTF8_ONE_END
            && pTempUTF8 + 1 < pUTF8End)
        {
            //0000 - 007F  0xxxxxxx
            *pTempUTF8++ = (UTF8)*pTempUTF16;
        }
        else if(*pTempUTF16 >= UTF8_TWO_START && *pTempUTF16 <= UTF8_TWO_END
                && pTempUTF8 + 2 < pUTF8End)
        {
            //0080 - 07FF 110xxxxx 10xxxxxx
            *pTempUTF8++ = (*pTempUTF16 >> 6) | 0xC0;
            *pTempUTF8++ = (*pTempUTF16 & 0x3F) | 0x80;
        }
        else if(*pTempUTF16 >= UTF8_THREE_START && *pTempUTF16 <= UTF8_THREE_END
                && pTempUTF8 + 3 < pUTF8End)
        {
            //0800 - FFFF 1110xxxx 10xxxxxx 10xxxxxx
            *pTempUTF8++ = (*pTempUTF16 >> 12) | 0xE0;
            *pTempUTF8++ = ((*pTempUTF16 >> 6) & 0x3F) | 0x80;
            *pTempUTF8++ = (*pTempUTF16 & 0x3F) | 0x80;
        }
        else
        {
            break;
        }
        pTempUTF16++;
    }
    *pTempUTF8 = 0;
}

void UTF8ToUTF16(UTF8* pUTF8Start, UTF8* pUTF8End, UTF16* pUTF16Start, UTF16* pUTF16End)
{
    UTF16* pTempUTF16 = pUTF16Start;
    UTF8* pTempUTF8 = pUTF8Start;
    
    while (pTempUTF8 < pUTF8End && pTempUTF16+1 < pUTF16End)
    {
        if (*pTempUTF8 >= 0xE0 && *pTempUTF8 <= 0xEF)//是3个字节的格式
        {
            //0800 - FFFF 1110xxxx 10xxxxxx 10xxxxxx
            *pTempUTF16 |= ((*pTempUTF8++ & 0xEF) << 12);
            *pTempUTF16 |= ((*pTempUTF8++ & 0x3F) << 6);
            *pTempUTF16 |= (*pTempUTF8++ & 0x3F);
            
        }
        else if (*pTempUTF8 >= 0xC0 && *pTempUTF8 <= 0xDF)//是2个字节的格式
        {
            //0080 - 07FF 110xxxxx 10xxxxxx
            *pTempUTF16 |= ((*pTempUTF8++ & 0x1F) << 6);
            *pTempUTF16 |= (*pTempUTF8++ & 0x3F);
        }
        else if(*pTempUTF8 >= 0 && *pTempUTF8 <= 0x7F)//是1个字节的格式
        {
            //0000 - 007F  0xxxxxxx
            *pTempUTF16 = *pTempUTF8++;
        }
        else
        {
            break;
        }
        pTempUTF16++;
    }
    *pTempUTF16 = 0;
}


- (NSString *)stringFromHexString:(NSString *)hexString { //
    
    char *myBuffer = (char *)malloc((int)[hexString length] / 4 + 1);
    bzero(myBuffer, [hexString length] / 2 + 1);
    for (int i = 0; i < [hexString length] - 1; i += 4) {
        unsigned int anInt;
        NSString * hexCharStr = [hexString substringWithRange:NSMakeRange(i, 4)];
        NSScanner * scanner = [[NSScanner alloc] initWithString:hexCharStr];
        [scanner scanHexInt:&anInt];
        myBuffer[i / 4] = (char)anInt;
    }
    NSString *unicodeString = [NSString stringWithCString:myBuffer encoding:4];
    return unicodeString;
    
    
}

/*
 把UTF16的字符串，转化为utf8的字符串。比如 D83DDE04 -> EDA0BDEDB884
 */
+ (NSString *)intFromHexString:(NSString *)hexString { //
    
    int length  =(int)[hexString length] / 4 + 1;
    UTF16 *myBuffer = (UTF16 *)malloc(length);
    bzero(myBuffer, length);
    for (int i = 0; i < [hexString length] - 1; i += 4) {
        NSString * hexCharStr = [hexString substringWithRange:NSMakeRange(i, 4)];
        NSScanner * scanner = [[NSScanner alloc] initWithString:hexCharStr];
        unsigned int temp;
        [scanner scanHexInt:&temp];
        *(myBuffer + i / 4) = (UTF16) temp;
        
    }
    
    int newLength = (int)[hexString length] / 4 * 3;
    UTF8 * buf = malloc(newLength);
    UTF16ToUTF8(myBuffer, myBuffer + length, buf, buf + length * 3 + 1);
    
    NSString * result = @"";
    for (int i = 0; i < newLength; i++) {
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%X", *(buf+i)]];
    }
    return result;
}


/*
 根据utf8字符串获取对应的内存映射
 */

+ (UTF8 *) getUtf8buf :(NSString *) utf8Str
{
    UTF8 *myBuffer = (UTF8 *)malloc((int)[utf8Str length] / 2 + 1);
    bzero(myBuffer, [utf8Str length] / 2 + 1);
    for (int i = 0; i < [utf8Str length] - 1; i += 2) {
        unsigned int anInt;
        NSString * hexCharStr = [utf8Str substringWithRange:NSMakeRange(i, 2)];
        NSScanner * scanner = [[NSScanner alloc] initWithString:hexCharStr];
        [scanner scanHexInt:&anInt];
        myBuffer[i / 2] = (char)anInt;
    }
    return myBuffer;
}

/*
 获得一个Buf对应的字符串。
 */
+ (NSString *) getStringByBuf : (UTF8 *) buf   :(int)length
{
    NSString *result = @"";
    for (int i = 0; i < length; i++) {
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%X", *(buf+i)]];
    }
    
    return result;
}





/*
 把Android中的表情字符，转化为IOS可识别的utf8字符。
 */

+ (int) convertUft8ToIOSUtf8 : (UTF8 *) inbuf   :(UTF8 **)pOutbuf  :(int) outBufLen
{
    int outLen  =   0;
    
    UTF8 *  outbuf  =   malloc(outBufLen);
    memset(outbuf, 0, outBufLen);
    *pOutbuf    =   outbuf;
    
    while (*inbuf != 0x0) {
        if (*inbuf == 0xED && *(inbuf + 1) == 0xA0  &&
            *(inbuf + 2) >= 0xBC && *(inbuf + 2 ) <= 0xBD) {
            NSString *oringUtf8key  =   [self getStringByBuf:inbuf :6];
            NSString *newUft8key    =   [emojiDic objectForKey:oringUtf8key];
            if (newUft8key != NULL) {
                UTF8 * newbuf   =   [EncodeUtil getUtf8buf:newUft8key];
                memcpy(outbuf, newbuf, [newUft8key length] / 2);
                outbuf += [newUft8key length] / 2;
                outLen  +=[newUft8key length] / 2;
            }
//TODO  过滤掉没有编码的字符，未来加上一个默认字符

            inbuf += 6;
        }
        else {
            *outbuf =   *inbuf;
            outbuf++;
            inbuf++;
            outLen++;
        }
    }
    
    return outLen;
}


@end
