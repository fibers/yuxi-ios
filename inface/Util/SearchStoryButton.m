//
//  SearchStoryButton.m
//  inface
//
//  Created by huangzengsong on 15/7/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SearchStoryButton.h"

@implementation SearchStoryButton
#pragma mark 设置Button内部的image的范围

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(contentRect.size.width/2.0-44/2.0, 4, 44, 44);
}

//设置Button内部的title的范围

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleY = 44;
    CGFloat titleH = contentRect.size.height-titleY;
    CGFloat titleW = contentRect.size.width;
    
    return CGRectMake(0, titleY, titleW, titleH);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
