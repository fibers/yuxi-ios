//
//  HZSFamilyButton.m
//  inface
//
//  Created by Mac on 15/5/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "HZSFamilyButton.h"

@implementation HZSFamilyButton


#pragma mark 设置Button内部的image的范围

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageW = contentRect.size.width;
    CGFloat imageH = contentRect.size.width;
    return CGRectMake(0, 0, imageW, imageH);
}

//设置Button内部的title的范围

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleH = contentRect.size.height;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleY = contentRect.size.height - 45;
    return CGRectMake(0, titleY, titleW, titleH);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
