//
//  HZSTitleButton.m
//  inface
//
//  Created by Mac on 15/5/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "HZSTitleButton.h"

@implementation HZSTitleButton


//设置Button内部的title的范围

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleY = contentRect.size.width;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height - titleY;
    return CGRectMake(0, titleY, titleW, titleH);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
