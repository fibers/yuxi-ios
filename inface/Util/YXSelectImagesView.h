//
//  YXSelectImagesView.h
//  inface
//
//  Created by appleone on 15/8/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCSelectButton.h"
@protocol YXSelectImagesDelegate <NSObject>

-(void)selectImages;

-(void)takePhotos;

-(void)clickWriteboard;

@end
@interface YXSelectImagesView : UIView
@property (assign,nonatomic) id<YXSelectImagesDelegate> delegate;
@property (nonatomic,strong) CCSelectButton  * selectImages;
@property (nonatomic,strong) CCSelectButton  * takePhotos;
@property (nonatomic,strong) CCSelectButton  * writeBoard;

@end
