
//
//  Constant.h
//  inface
//
//  Created by xigesi on 15/4/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#ifndef inface_Constant_h
#define inface_Constant_h

//注释掉这个宏，是真实环境。
//#define YX_DEV_MODEL

#ifdef YX_DEV_MODEL
/*测试环境*/

#define ROOT_URL             @"http://112.126.67.151:10001"  //http请求的环境
#define CHAT_SERVER_BASE_URL @"http://123.56.154.93:8080/msg_server" //聊天得总服务器地址由此获得ip地址和端口号
#define SHARE_SELFSTORY_URL  @"http://123.56.154.93:88"
#define SERVER_PUSH          @"http://push.halfcookie.cn:8080"

/// 个推开发者网站中申请App时注册的AppId、AppKey、AppSecret
#define kGtAppId           @"2JCl8DfD2K8toDghXpKpN2"
#define kGtAppKey          @"1ZVpt2BQfy56dw7mu8pSB2"
#define kGtAppSecret       @"hGdQFJNHHo8yIQtOj3dr33"

#else
/*真实环境*/
#define ROOT_URL             @"http://123.57.136.1:10001"  //http请求的环境
#define CHAT_SERVER_BASE_URL @"http://123.56.129.7:8080/msg_server" //聊天得总服务器地址由此获得ip地址和端口号
#define SHARE_SELFSTORY_URL  @"http://123.56.154.93:86"
#define SERVER_PUSH          @"http://push.halfcookie.cn"

/// 个推开发者网站中申请App时注册的AppId、AppKey、AppSecret
#define kGtAppId           @"xLQCHkmPON7vEobn9hU8b6"
#define kGtAppKey          @"da3TZxVR1u6au2Ql5WTct"
#define kGtAppSecret       @"dYoaWmOWIM8VvV3SXq9Uv5"
#endif

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

//是否为空或是[NSNull null]
#define NotNilAndNull(_ref)  (((_ref) != nil) && (![(_ref) isEqual:[NSNull null]]))
#define IsNilOrNull(_ref)   (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]))

//字符串是否为空
#define IsStrEmpty(_ref)    (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]) ||([(_ref)isEqualToString:@""]))

//数组是否为空
#define IsArrEmpty(_ref)    (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]) ||([(_ref) count] == 0))

#define ScreenHeight [[UIScreen mainScreen] bounds].size.height//获取屏幕高度，兼容性测试
#define ScreenWidth [[UIScreen mainScreen] bounds].size.width//获取屏幕宽度，兼容性测试
#define IOSSystemVersion [[[UIDevice currentDevice] systemVersion] floatValue]//系统版本号
#define USERID               [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]
#define USER_ID              [[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"]
//设置颜色RGB
#define COLOR(R, G, B, A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
//常用蓝色@"eb71a8"
#define BLUECOLOR [UIColor colorWithRed:235/255.0 green:113/255.0 blue:168/255.0 alpha:1]
//每个页面的背景颜色
#define BACKGROUND_COLOR [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]

//tabbar的背景颜色
#define TABBAR_COLOR [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1]
//字灰颜色@"949494"
#define GRAY_COLOR [UIColor colorWithRed:148/255.0 green:148/255.0 blue:148/255.0 alpha:1]
//标题黑颜色@"0e0e0e"
#define BLACK_COLOR [UIColor colorWithRed:14/255.0 green:14/255.0 blue:14/255.0 alpha:1]
//内容黑颜色@"4a4a4a"
#define BLACK_CONTENT_COLOR [UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1]
//灰黑颜色@"656464"
#define BLACK_GRAY_COLOR [UIColor colorWithRed:101/255.0 green:100/255.0 blue:100/255.0 alpha:1]
//细线颜色@"c5c5c6"
#define XIAN_COLOR [UIColor colorWithRed:197/255.0 green:197/255.0 blue:198/255.0 alpha:1]
//粉红颜色@"f1b4d0"
#define FENHONG_COLOR [UIColor colorWithRed:241/255.0 green:180/255.0 blue:208/255.0 alpha:1]
//蓝色颜色@"7fb0e9"
#define BLUE_CONTENT_COLOR [UIColor colorWithRed:127/255.0 green:176/255.0 blue:233/255.0 alpha:1]
//清除背景色
#define CLEARCOLOR [UIColor clearColor]

#define PUSH_CHATVIEW_NUMBER          @"chat_view_number"

//上传图片到又拍的地址
#define IMAGE_SERVER_BASE_URL           @"http://cosimage.b0.upaiyun.com"

#define DELETE_SESSION                  @"deleteSession"

#define SESSION_UPDATE                  @"sessionUpdate"

#define GET_LOCAL_SESSIONS              @"getLocalSession"

#define GET_FRIENDS_NICKNAME           @"getFriendsNick"

#define WEAKSELF typeof(self) __weak weakSelf = self;
#define DELETE_FRIEND                  @"deleteFriend"
#define AGREE_ADD_FRIEND               @"agreeaddfriend"
#define BE_AGREE_FRIEND                @"beagreefriend"
#define GROUP_MESSAGE                  @"user_00001001000"
#define GROUP_NOTIFY_DIC               @"group_notify_dic"
#define GROUP_CONTENT_DIC              @"group_content_dic"
#define IM_PDU_HEADER_LEN   16
#define IM_PDU_VERSION      1

#define NAVBAR_HEIGHT         44.f
#define FULL_WIDTH            ScreenWidth

#define FULL_HEIGHT           ScreenHeight

#define CONTENT_HEIGHT        (FULL_HEIGHT - NAVBAR_HEIGHT)


#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "HZSInstances.h"
#import "MyMD5.h"//md5
#import "SVProgressHUD.h"//SVProgressHUD
#define AFNETWORKING_ALLOW_INVALID_SSL_CERTIFICATES
#import <SystemConfiguration/SystemConfiguration.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "HttpTool.h"
#import "AFNetworking.h"//数据请求
#import "EGORefreshTableHeaderView.h"//下拉刷新
#import "EGORefreshTableFootView.h"//上拉加载
#import "JSON.h"//json解析
#import "BaseViewController.h"//父视图
#import "HexColor.h"//十六进制或者红绿蓝颜色
#import "PlaceholderTextView.h"//占位符的TextView
#import "UIImageView+WebCache.h"//SDImageView
#import "UIButton+WebCache.h"
#import "SDImageCache.h"
#import "NSString+YXExtention.h"
#import "DDSundriesCenter.h"
#import "DDNotificationHelp.h"
#import "RuntimeStatus.h"
#import "NSDictionary+Safe.h"
#import "SessionModule.h"
#import "NSDate+DDAddition.h"
#import "DDMessageModule.h"
#import "DDUserModule.h"
#import "PhotosCache.h"
#import "NSDictionary+JSON.h"
#import "LoginViewController.h"
#import "VerticalAlignmentLabel.h"//从顶部开始文字label
#import "DashesLineView.h"//虚线label
#import "HZSUnderLineLabel.h"//下划线label
#import "CreatKindsOfModel.h"
#import "GetAllFamilysAndStorys.h"
#import "GetLocalRecentSessions.h"
#import "KeyboardManager.h"
#import "NSString+Additions.h"//字符串扩展 判断是否为空等等
#import "ZWCollectionViewFlowLayout.h"
#import "DiscussModel.h"
#import "DDAddMemberToGroupAPI.h"
#import "KxMenu.h"//下拉菜单
#endif



#define PhotosMessageDir ([[NSString documentPath] stringByAppendingPathComponent:@"/PhotosMessageDir/"])
#define VoiceMessageDir ([[NSString documentPath] stringByAppendingPathComponent:@"/VoiceMessageDir/"])
#define BlacklistDir ([[NSString documentPath] stringByAppendingPathComponent:@"/BlacklistDir/"])
#define Departmentlist ([[NSString documentPath] stringByAppendingPathComponent:@"/department.plist"])
#define fixedlist ([[NSString documentPath] stringByAppendingPathComponent:@"/fixed.plist"])
#define shieldinglist ([[NSString documentPath] stringByAppendingPathComponent:@"/shieldingArray.plist"])
#define ReaderCacheFile ([[NSString documentPath] stringByAppendingPathComponent:@"/Article.db"])

#ifdef DEBUG
#define LOG(...) NSLog(__VA_ARGS__);
#define LOG_METHOD NSLog(@"%s", __func__);
#else
#define LOG(...); 
#define LOG_METHOD;
#endif

#endif
