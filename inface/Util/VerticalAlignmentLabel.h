//
//  VerticalAlignmentLabel.h
//  inface
//
//  Created by huangzengsong on 15/5/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//自定义文字顶部展示label
#import <UIKit/UIKit.h>


typedef enum
{
    VerticalAlignmentTop = 0, // default
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;
@interface VerticalAlignmentLabel : UILabel

{
@private
    VerticalAlignment _verticalAlignment;
}

@property (nonatomic) VerticalAlignment verticalAlignment;

@end
