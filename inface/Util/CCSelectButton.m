//
//  CCSelectButton.m
//  inface
//
//  Created by appleone on 15/8/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "CCSelectButton.h"

@implementation CCSelectButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
#pragma mark 设置Button内部的image的范围

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageW = contentRect.size.width;
    CGFloat imageH = contentRect.size.width;
    return CGRectMake(0, 0, imageW, imageH);
}

//设置Button内部的title的范围

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleH = contentRect.size.height;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleY = contentRect.size.height - 10;
    return CGRectMake(0, titleY, titleW, titleH);
}

@end
