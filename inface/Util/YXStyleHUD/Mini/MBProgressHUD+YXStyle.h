//
//  MBProgressHUD+YXStyle.h
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (YXStyle)
/**
 *  显示YX风格的加载动画添加在相应的view上。
 *
 *  @param view    需要添加加载动画的View
 *  @param animate 是否动画展现
 *
 *  @return MBProgressHUD
 */

+(instancetype)showYXLoadingMiniViewAddToView:(UIView * )view animated:(BOOL)animate;

+(instancetype)showYXHudShowTextAddToView:(UIView * )view Title:(NSString *)title andHideTime:(CGFloat)time;

@end
