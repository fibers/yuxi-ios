//
//  MBProgressHUD+ISMStyle.m
//  晒米
//
//  Created by shaimi on 15/6/29.
//  Copyright (c) 2015年 ishareme. All rights reserved.
//

#import "MBProgressHUD+YXStyle.h"
#import "YXLoadingMiniViewView.h"
@implementation MBProgressHUD (YXStyle)


+(instancetype)showYXLoadingMiniViewAddToView:(UIView * )view animated:(BOOL)animate{
    YXLoadingMiniViewView * miniView = [[YXLoadingMiniViewView alloc]init];
    [miniView addLoadingAnimation];
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:view animated:animate];
    hud.mode = MBProgressHUDModeCustomView;
    hud.opacity = 0.0;
    [hud setCustomView:miniView];
    [hud show:YES];
    return hud;
}

+(instancetype)showYXHudShowTextAddToView:(UIView * )view Title:(NSString *)title andHideTime:(CGFloat)time{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    [hud setMode:MBProgressHUDModeText];
    [hud setDetailsLabelText:title];
    
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:time];
    return hud;
}

@end
