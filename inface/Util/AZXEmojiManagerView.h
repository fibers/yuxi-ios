//
//  AZXEmojiManagerView.h
//  SoMateSecrect
//
//  Created by azx on 14/11/26.
//  Copyright (c) 2014年 SiGeXi Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AZXEmojiManagerViewDelegate <NSObject>

@optional
/**
 点击表情回调方法
 */
- (void)emojiManagerView:(UIView *)emojiManagerView didSelectItem:(NSString *)item AtIndex:(NSUInteger)index;

/**
 点击删除表情回调方法
 */
- (void)emojiManagerView:(UIView *)emojiManagerView didDeleteItem:(NSString *)item ;

/**
 点击发送表情回调方法
 */
- (void)emojiManagerView:(UIView *)emojiManagerView didSendItem:(NSString *)item ;

//点击言文字回调方法
- (void)skinSurfaceView:(UIView *)skinSurfaceView didSendItem:(NSString *)item ;

@end

@interface AZXEmojiManagerView : UIView<UIScrollViewDelegate>

@property (assign, nonatomic) id <AZXEmojiManagerViewDelegate> delegate;

@property(retain,nonatomic)UIScrollView *scrollViewLiu;
@property (nonatomic ,retain) UIPageControl * pageControlLiu;
@property (strong,nonatomic)  NSMutableDictionary * skinSurface;//皮表字典
@property (assign,nonatomic)  NSInteger   pageIndex;//当前滚动的页数
@property(nonatomic,retain)UICollectionView * collectView;
@property(nonatomic,assign)   NSInteger   page;
@property(strong,nonatomic)    UIView   * buttonsView;
@property(strong,nonatomic)    UIButton * skinSurfaceBtn;
@property(strong,nonatomic)    UIButton * emotionsBtn;
@property(strong,nonatomic)    UIButton * cancleBtn;
@property(strong,nonatomic)    UIButton * sendBtn;
@property(strong,nonatomic)   NSMutableArray * skinSurfaceArr;

@end
