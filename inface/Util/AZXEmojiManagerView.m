//
//  AZXEmojiManagerView.m
//  SoMateSecrect
//
//  Created by azx on 14/11/26.
//  Copyright (c) 2014年 SiGeXi Inc. All rights reserved.
//

#import "AZXEmojiManagerView.h"
#import "SGXEmotionContent.h"
#import "SkinSurfaceViewCell.h"
@interface AZXEmojiManagerView()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ZWwaterFlowDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *colorPool;

@end

@implementation AZXEmojiManagerView

// TODO:临时解决方案,创建按钮
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.colorPool = @[@"#7ecef4", @"#84ccc9", @"#88abda",@"#7dc1dd",@"#b6b8de"];
        self.skinSurfaceArr = [NSMutableArray array];
        self.skinSurfaceArr = [NSMutableArray arrayWithObjects:@"(^^)",@"ʕ•ᴥ•ʔ",@"('-‘*)",@"（◞‸◟）",@"( ´_ゝ`)",@"( ꒪⌓꒪)",@"( ･∀･)",@"(*>ω<*)",@"(´･ε･̥ˋ๑)",@"ヽ(•̀ω•́ )ゝ",@"(´・ω・｀)",@"(∩`ω´)⊃))",@"(°ω°)",@"(๑•﹏•)",@"(・_・　)",@"(･ิω･ิ)",@"(ﾟ∀ﾟ)",@"( ˘•ω•˘ )",@"( ￣ー￣)",@"(｡･ω･｡)",@"(ﾟoﾟ)",@"( つ•̀ω•́)つ",@"(ﾉ_･｡)",@"(・ε・｀)",@"(  p_q)",@"(○´―`)",@"(｀Д´)",@"(｡◕ ∀ ◕｡)",@"(´∀`)",@"(･ω<)",@"(´ｰ｀)",@"(｡╹ω╹｡)",@"(-''-;)",@"(o´∀｀o)",@"(･∀･｀*)",@"(´；д；`)", nil];
        [self setUpPibiaoView];
//        [self setupTagView];
        
    }
    return self;
}



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.skinSurfaceArr.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SkinSurfaceViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SkinSurfaceViewCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor colorWithHexString:@"#f7f7f7"];
    cell.layer.borderColor = [UIColor colorWithHexString:@"#d7d7d7"].CGColor;
    cell.layer.borderWidth = 0.3;
    cell.titleLabel.textColor = [UIColor colorWithHexString:@"#a1a1a1"];
    cell.titleLabel.text = [self.skinSurfaceArr objectAtIndex:indexPath.row];
    
    return cell;
}

//-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
//{
//    return CGSizeMake(ScreenWidth/4.0, 45.0);
//}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return CGSizeMake(ScreenWidth/4.0, 45.0);

}


//-(CGFloat)ZWwaterFlow:(ZWCollectionViewFlowLayout *)waterFlow heightForWidth:(CGFloat)width atIndexPath:(NSIndexPath *)indexPach
//{
//    float contentCollectionView = 45;
//    return contentCollectionView;
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * skinSurface = [self.skinSurfaceArr objectAtIndex:indexPath.row];
    [self.delegate skinSurfaceView:self didSendItem:skinSurface];
}

//
//-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0, 0, 0, 0);
//}

-(BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    return YES;
}

-(void)setUpPibiaoView
{

    NSInteger skinsCount = 36;
    int countPage = 12;//一页12个皮表
    int scrollviewHeight = 160;
    NSInteger a = skinsCount /countPage;
    NSInteger b = skinsCount %countPage;
   
    if (b==0) {
       self.page  = a;
    }else{
       self.page = a+1;
    }

    UICollectionViewFlowLayout * layOut = [[UICollectionViewFlowLayout alloc]init];
    layOut.minimumInteritemSpacing = 0.0;
    layOut.minimumLineSpacing = 0.0;
    [layOut setMinimumInteritemSpacing:0];
    [layOut setMinimumLineSpacing:0];
    layOut.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    if (!self.collectView) {
       self.collectView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0,ScreenWidth,135) collectionViewLayout:layOut];
        self.collectView.backgroundColor=[UIColor clearColor];
        self.collectView.delegate =self;
        self.collectView.dataSource =self;
        [self  addSubview:self.collectView];
        [self.collectView registerNib:[UINib nibWithNibName:@"SkinSurfaceViewCell" bundle:nil] forCellWithReuseIdentifier:@"SkinSurfaceViewCell"];
        
    }

    self.collectView.pagingEnabled = YES;
    self.collectView.contentSize  =CGSizeMake(ScreenWidth*self.page, 135);

    self.collectView.scrollEnabled = YES;
    self.collectView.alwaysBounceHorizontal = YES;
    [self.collectView reloadData];

    if (!self.pageControlLiu) {
        
    self.pageControlLiu=[[UIPageControl alloc]initWithFrame:CGRectMake(0,  0, ScreenWidth, 30)];

    self.pageControlLiu.backgroundColor=[UIColor clearColor];
    self.pageControlLiu.currentPageIndicatorTintColor=BLUECOLOR;
    self.pageControlLiu.pageIndicatorTintColor=[UIColor colorWithHexString:@"#c5c5c6"];
    self.pageControlLiu.userInteractionEnabled=NO;
    self.pageControlLiu.currentPage = 0;
    self.pageControlLiu.center=CGPointMake(ScreenWidth/2.0, scrollviewHeight-16); // 设置pageControl的位置
    [self addSubview:self.pageControlLiu];
    }
    self.pageControlLiu.numberOfPages=self.page;

    //发送按钮
    if (!self.sendBtn) {

    self.sendBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.sendBtn];
    [self.sendBtn setFrame:CGRectMake(0,0,ScreenWidth/4.0,44)];
    self.sendBtn.center=CGPointMake(ScreenWidth/8.0*7.0, scrollviewHeight+20);
    [self.sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.sendBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [self.sendBtn setBackgroundColor:[UIColor colorWithHexString:@"#eb71a5"]];
    [self.sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [self.sendBtn addTarget:self
                   action:@selector(SendClick:)
         forControlEvents:UIControlEventTouchUpInside];
    }
    //皮表按钮
    if (!self.skinSurfaceBtn) {
        
    self.skinSurfaceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.skinSurfaceBtn];
    [self.skinSurfaceBtn setFrame:CGRectMake(0, scrollviewHeight , ScreenWidth /4.0,44 )];
    [self.skinSurfaceBtn setTitleColor:[UIColor colorWithHexString:@"#a1a1a1"] forState:UIControlStateNormal];
    self.skinSurfaceBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [self.skinSurfaceBtn setBackgroundColor:[UIColor colorWithHexString:@"e6e4e4"]];
    [self.skinSurfaceBtn setTitle:@"表情" forState:UIControlStateNormal];
    [self.skinSurfaceBtn addTarget:self
               action:@selector(clickPiBiao:)
     forControlEvents:UIControlEventTouchUpInside];
    }
    //表情按钮
    if (!self.emotionsBtn) {
        
    self.emotionsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.emotionsBtn];
    [self.emotionsBtn setFrame:CGRectMake(ScreenWidth/4.0, scrollviewHeight , ScreenWidth /4.0,44 )];
    [self.emotionsBtn setTitleColor:[UIColor colorWithHexString:@"#a1a1a1"] forState:UIControlStateNormal];
    self.emotionsBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [self.emotionsBtn setBackgroundColor:[UIColor colorWithHexString:@"#eeecec"]];
    [self.emotionsBtn setTitle:@"套" forState:UIControlStateNormal];
    [self.emotionsBtn addTarget:self
                 action:@selector(clickEmotion:)
       forControlEvents:UIControlEventTouchUpInside];
    }
    //删除按钮
    if (!self.cancleBtn) {
    
    self.cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.cancleBtn];
    [self.cancleBtn setFrame:CGRectMake(ScreenWidth/4.0 *2, scrollviewHeight , ScreenWidth /4.0,44 )];
    [self.cancleBtn setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    self.cancleBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [self.cancleBtn setImage:[UIImage imageNamed:@"cancleMessage"] forState:UIControlStateNormal];
    [self.cancleBtn setBackgroundColor:[UIColor colorWithHexString:@"#f5f5f5"]];
    [self.cancleBtn addTarget:self
                  action:@selector(DeleteClick:)
        forControlEvents:UIControlEventTouchUpInside];
    }
}


- (void)setupTagView
{
    NSMutableArray *aimgArray=[[SGXEmotionContent sharedClient]getEmotionArr];
    int countPage=12;//一页多少个表情
    int scrollviewHeight=160;//滚动视图的高
    int a=aimgArray.count/countPage;
    int b=aimgArray.count%countPage;
    int ye=0;
    if (b==0) {
        ye=a;
    } else {
        ye=a+1;
    }
    
    //定义一个滚动视图
    if (!self.scrollViewLiu) {
        
    self.scrollViewLiu=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0, ScreenWidth, scrollviewHeight)];
    self.scrollViewLiu.pagingEnabled=YES;//可以翻页
    self.scrollViewLiu.userInteractionEnabled=YES;//用户可交互
    self.scrollViewLiu.contentOffset=CGPointMake(0, 0);//记录滚动的点
    self.scrollViewLiu.showsHorizontalScrollIndicator = NO;
    self.scrollViewLiu.showsVerticalScrollIndicator = NO;
    self.scrollViewLiu.delegate=self;
    [self addSubview:self.scrollViewLiu];
    
    
    }
    self.scrollViewLiu.contentSize=CGSizeMake(ScreenWidth*ye, scrollviewHeight);//滚动的内容尺寸

    for (int m=0; m<ye; m++) {
        //输入的View1
        self.buttonsView = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth*m, 0, ScreenWidth, scrollviewHeight)];
        self.buttonsView.backgroundColor=[UIColor clearColor];
        
        [self.scrollViewLiu addSubview:self.buttonsView];
        
        //添加16个按钮
        for (int i=0; i<countPage; i++) {
            
            if ((i+countPage*m)<aimgArray.count) {
                UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
                aButton.tag=i+10000+countPage*m;
                float width=(ScreenWidth-50)/4.0;
                float height=30;
                
                int j=i/4;//第几行
                int k=i%4;//第几列
                
                aButton.frame=CGRectMake(10+(width+10)*k,10+(height+10)*j, width, height);
                aButton.layer.borderWidth = 1;
                if ((i+countPage*m)==aimgArray.count-1) {
                    aButton.layer.borderColor = BLUECOLOR.CGColor;
                } else {
                    aButton.layer.borderColor = BLUECOLOR.CGColor;
                }
                
                aButton.layer.cornerRadius = 15;
                aButton.clipsToBounds = YES;
                [aButton addTarget:self action:@selector(handleBtn:) forControlEvents:UIControlEventTouchUpInside];
                [aButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
                aButton.titleLabel.font=[UIFont systemFontOfSize:14];
                [aButton setBackgroundColor:[UIColor colorWithHexString:self.colorPool[(i+countPage*m) % self.colorPool.count]]];
            
                [aButton setBackgroundColor:[UIColor clearColor]];
                [aButton setTitle:[[aimgArray objectAtIndex:i+countPage*m] objectForKey:@"title"] forState:UIControlStateNormal];
                [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
                
                [self.buttonsView addSubview:aButton];
                
            }
        }
    
    }
    
    if (!self.pageControlLiu) {
        UIPageControl *pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0,  0, ScreenWidth, 30)];
        self.pageControlLiu=pageControl;
        pageControl.backgroundColor=[UIColor clearColor];
        pageControl.currentPageIndicatorTintColor=BLUECOLOR;
        self.pageControlLiu.pageIndicatorTintColor=[UIColor colorWithHexString:@"#c5c5c6"];
        pageControl.userInteractionEnabled=NO;
        pageControl.currentPage = 0;
        [self addSubview:self.pageControlLiu];
    }
       self.pageControlLiu.center=CGPointMake(ScreenWidth/2.0, scrollviewHeight-16); // 设置pageControl的位置
        self.pageControlLiu.numberOfPages = ye;

//    //输入的View1
//    UIView * aView1 = [[UIView alloc] initWithFrame:CGRectMake(0, scrollviewHeight, ScreenWidth, 1)];
//    aView1.backgroundColor=BLUECOLOR;
//    
//    [self addSubview:aView1];
//    
//    //输入的View1
//    UIView * aView2 = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth/8.0*6.0, scrollviewHeight, 1, 200-scrollviewHeight)];
//    aView2.backgroundColor=BLUECOLOR;
//    
//    [self addSubview:aView2];
    
    //发送按钮) {
    if (!self.sendBtn) {
    
     self.sendBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.sendBtn];
    [self.sendBtn setFrame:CGRectMake(0,0,ScreenWidth/4.0,44)];
    self.sendBtn.center=CGPointMake(ScreenWidth/8.0*7.0, scrollviewHeight+20);
    [self.sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.sendBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [self.sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [self.sendBtn setBackgroundColor:[UIColor colorWithHexString:@"#eb71a5"]];

    [self.sendBtn addTarget:self
                         action:@selector(SendClick:)
               forControlEvents:UIControlEventTouchUpInside];
    }
    
    //皮表按钮
    if (!self.skinSurfaceBtn) {
        
    self.skinSurfaceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.skinSurfaceBtn];
    [self.skinSurfaceBtn setFrame:CGRectMake(0, scrollviewHeight , ScreenWidth /4.0,44 )];
    [self.skinSurfaceBtn setTitleColor:[UIColor colorWithHexString:@"#a1a1a1"] forState:UIControlStateNormal];
    self.skinSurfaceBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [self.skinSurfaceBtn setBackgroundColor:[UIColor colorWithHexString:@"e6e4e4"]];
    [self.skinSurfaceBtn setTitle:@"表情" forState:UIControlStateNormal];
    [self.skinSurfaceBtn addTarget:self
                   action:@selector(clickPiBiao:)
         forControlEvents:UIControlEventTouchUpInside];
    }
    //表情按钮
    if (!self.emotionsBtn) {
    
    self.emotionsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.emotionsBtn];
    [self.emotionsBtn setFrame:CGRectMake(ScreenWidth/4.0, scrollviewHeight , ScreenWidth /4.0,44 )];
    [self.emotionsBtn setTitleColor:[UIColor colorWithHexString:@"#a1a1a1"] forState:UIControlStateNormal];
    self.emotionsBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [self.emotionsBtn setBackgroundColor:[UIColor colorWithHexString:@"#eeecec"]];
    [self.emotionsBtn setTitle:@"套" forState:UIControlStateNormal];
    [self.emotionsBtn addTarget:self
               action:@selector(clickEmotion:)
     forControlEvents:UIControlEventTouchUpInside];
    }
    //删除按钮
    if (!self.cancleBtn) {
    
    self.cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.cancleBtn];
    [self.cancleBtn setFrame:CGRectMake(ScreenWidth/4.0 *2, scrollviewHeight , ScreenWidth /4.0,44 )];
    [self.cancleBtn setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    self.cancleBtn.titleLabel.font=[UIFont systemFontOfSize:16];
    [self.cancleBtn setImage:[UIImage imageNamed:@"cancleMessage"] forState:UIControlStateNormal];
    [self.cancleBtn setBackgroundColor:[UIColor colorWithHexString:@"#f5f5f5"]];
    [self.cancleBtn addTarget:self
                 action:@selector(DeleteClick:)
       forControlEvents:UIControlEventTouchUpInside];
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    

    if (scrollView==self.collectView) {
        self.pageIndex = scrollView.contentOffset.x / scrollView.frame.size.width;
        
        self.pageControlLiu.currentPage = self.pageIndex;
    }else{
        self.pageIndex = scrollView.contentOffset.x / scrollView.frame.size.width;
        
        self.pageControlLiu.currentPage = self.pageIndex;
    }
    
    
    
}

- (void)handleBtn:(UIButton *)btn
{
    NSMutableArray *aimgArray=[[SGXEmotionContent sharedClient]getEmotionArr];
    NSString    *mCommand = [[aimgArray objectAtIndex:btn.tag-10000]objectForKey:@"title"];
    [self.delegate emojiManagerView:self didSelectItem:mCommand AtIndex:btn.tag-10000];
}


- (void)DeleteClick:(UIButton *)btn
{
    [self.delegate emojiManagerView:self didDeleteItem:nil];
}

-(void)clickPiBiao:(UIButton *)sender
{
    [self setUpPibiaoView];
    self.buttonsView.hidden = YES;
    self.collectView.hidden = NO;
    self.pageControlLiu.currentPage = 0;
    self.scrollViewLiu.hidden = YES;

    
}

-(void)clickEmotion:(UIButton*)sender
{
    [self setupTagView];
    self.collectView.hidden = YES;
    self.buttonsView.hidden = NO;
    self.scrollViewLiu.hidden = NO;
}

- (void)SendClick:(UIButton *)btn
{
    [self.delegate emojiManagerView:self didSendItem:nil];
}

/*
 
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
