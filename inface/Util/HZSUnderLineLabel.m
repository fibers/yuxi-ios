//
//  HZSUnderLineLabel.m
//  inface
//
//  Created by huangzengsong on 15/5/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "HZSUnderLineLabel.h"

@implementation HZSUnderLineLabel


- (void)dealloc{
    
    self.lineColor = nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.lineColor=[UIColor grayColor];
        self.lineType = LineTypeDown;
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */


- (void)drawTextInRect:(CGRect)rect{
    [super drawTextInRect:rect];
    
//    CGSize textSize = [[self text] sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[self font],NSFontAttributeName, nil]];
//    //根据文字长度划线
//    CGFloat strikeWidth = textSize.width;
    //根据label长度划线
    CGFloat strikeWidth = rect.size.width;
    CGRect lineRect;
    CGFloat origin_x = 0.0;
    CGFloat origin_y = 0.0;
    
    
//    if ([self textAlignment] == NSTextAlignmentRight) {
//        
//        origin_x = rect.size.width - strikeWidth;
//        
//    } else if ([self textAlignment] == NSTextAlignmentCenter) {
//        
//        origin_x = (rect.size.width - strikeWidth)/2 ;
//        
//    } else {
//        
//        origin_x = 0;
//    }
    
    
    if (self.lineType == LineTypeUp) {
        
        origin_y =  2;
    }
    
    if (self.lineType == LineTypeMiddle) {
        
        origin_y =  rect.size.height/2;
    }
    
    if (self.lineType == LineTypeDown) {//下画线
        
        origin_y = rect.size.height - 2;
    }
    
    lineRect = CGRectMake(origin_x , origin_y, strikeWidth, 1);
    
    if (self.lineType != LineTypeNone) {
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGFloat R, G, B;
        CGColorRef color = self.lineColor.CGColor;
        NSUInteger numComponents = CGColorGetNumberOfComponents(color);
        
        if( numComponents == 4)
        {
            const CGFloat *components = CGColorGetComponents(color);
            R = components[0];
            G = components[1];
            B = components[2];
            
            CGContextSetRGBFillColor(context, R, G, B, 1.0);
            
        }
        
        CGContextFillRect(context, lineRect);
    }
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
