//
//  SkinSurfaceViewCell.h
//  inface
//
//  Created by appleone on 15/8/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkinSurfaceViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
