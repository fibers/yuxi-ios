//
//  HZSInstances.m
//  SuBangDai
//
//  Created by define on 14/12/2.
//  Copyright (c) 2014年 huangzengsong. All rights reserved.
//

#import "HZSInstances.h"
#import "NSAttributedString+CXACoreTextFrameSize.h"
@implementation HZSInstances


//tabbar xcode6 图标不显示
+ (UIImage *)fetUIImage:(NSString *)imageName{
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7) {
        UIImage *image=[[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        return image;
    }else{
        UIImage *image=[UIImage imageNamed:imageName];
        return image;
    }
    
}


#pragma mark- 缩放图片 对大小压缩
+(UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize
{
    //设置图片尺寸
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width*scaleSize,image.size.height*scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height *scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //对图片包得大小进行压缩
    NSData *imageData = UIImageJPEGRepresentation([self fixOrientation:scaledImage],0.0001);
    UIImage *m_selectImage = [UIImage imageWithData:imageData];
    return m_selectImage;
    
    
}


#pragma mark 压缩图片

+ (UIImage *)compressImage:(UIImage *)image toSize:(CGSize)size withCompressionQuality:(CGFloat)quality{
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = size.height;
    float maxWidth = size.width;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, quality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}


//修复照片方向
+ (UIImage *)fixOrientation:(UIImage *)aImage {
    
    // No-op if the orientation is already correct
    if (aImage.imageOrientation == UIImageOrientationUp)
        return aImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,
                                             CGImageGetBitsPerComponent(aImage.CGImage), 0,
                                             CGImageGetColorSpace(aImage.CGImage),
                                             CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}


//三个参数，第一个参数：要计算的字符串，第二个参数，lalbel的字体大小，第三个参数，label允许的最大尺寸。返回的为label的尺寸
+ (CGSize)labelAutoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize

{
    
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragraphStyle.lineBreakMode=NSLineBreakByWordWrapping;
    
    paragraphStyle.lineSpacing = 5.0f;
    
    NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
    if ([text isKindOfClass:[NSString class]] || [text isKindOfClass:[NSMutableString class]]) {
        if (text.length > 0 && ![text isEqualToString:@""]) {
            CGSize labelSize = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
            
            labelSize.height=ceil(labelSize.height);
            
            labelSize.width=ceil(labelSize.width);
            
            return labelSize;
        }else{
            return CGSizeMake(0, 0);
        }
    }else{
        return CGSizeMake(0, 0);
    }

    
}


+(CGFloat)linesAutoStingSize:(NSString *)content uifont:(float)font lines:(NSInteger)lineSpacing size:(CGSize)cgSize
{
    if(!content ||[content isEqualToString:@""])
    {
        return 0;
    }
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:content];
    
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    
    style.lineSpacing = lineSpacing;

    UIFont * uiFont = [UIFont systemFontOfSize:font];
    [attribute addAttributes:@{NSParagraphStyleAttributeName:style,NSFontAttributeName:uiFont} range:NSMakeRange(0, content.length)];
    
    return [attribute boundingRectWithSize:cgSize options:NSStringDrawingUsesFontLeading| NSStringDrawingUsesLineFragmentOrigin context:NULL].size.height;
}


+(CGFloat)boundAutoStingSize:(NSString *)content uifont:(float)font lines:(NSInteger)lineSpacing size:(CGSize)cgSize
{
    if(!content ||[content isEqualToString:@""])
    {
        return 0;
    }
    

    CGFloat   labelHeight = [content boundingRectWithSize:cgSize options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:NULL].size.height;
    
    int row = ceil(labelHeight / (font));

    CGFloat  totalH = (lineSpacing*(row+ 0))+labelHeight;

    return totalH;
    
}

//字数统计
+ (int)countWord:(NSString*)s

{
    
    int i,n=[s length],l=0,a=0,b=0;
    
    unichar c;
    
    for(i=0;i<n;i++){
        
        c=[s characterAtIndex:i];
        
        if(isblank(c)){
            
            b++;
            
        }else if(isascii(c)){
            
            a++;
            
        }else{
            
            l++;
            
        }
        
    }
    
    if(a==0 && l==0) return 0;
    
    return l+(int)ceilf((float)(a+b)/2.0);
    
}

+ (void)roundImage:(UIImageView *)imageView {
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = imageView.frame.size.width/2;
}




@end
