//
//  HZSInstances.h
//  SuBangDai
//
//  Created by define on 14/12/2.
//  Copyright (c) 2014年 huangzengsong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZSInstances : NSObject
//tabbar xcode6 图标不显示
+ (UIImage *)fetUIImage:(NSString *)imageName;

//缩放图片上传
+(UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize;

//修复照片方向
+ (UIImage *)fixOrientation:(UIImage *)aImage;

//三个参数，第一个参数：要计算的字符串，第二个参数，lalbel的字体大小，第三个参数，label允许的最大尺寸。返回的为label的尺寸
+ (CGSize)labelAutoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize;

+ (int)countWord:(NSString*)s;//字数统计

+ (void)roundImage:(UIImageView *)imageView;//图片圆形

+ (UIImage *)compressImage:(UIImage *)image toSize:(CGSize)size withCompressionQuality:(CGFloat)quality;//压缩图片

+(CGFloat)linesAutoStingSize:(NSString *)content uifont:(float)font lines:(NSInteger)lineSpacing size:(CGSize)cgSize;

+(CGFloat)boundAutoStingSize:(NSString *)content uifont:(float)font lines:(NSInteger)lineSpacing size:(CGSize)cgSize;
@end
