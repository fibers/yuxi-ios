//
//  SGXEmotionContent.m
//  SoMateSecrect
//
//  Created by guoq on 15-1-17.
//  Copyright (c) 2015年 SiGeXi Inc. All rights reserved.
//

#import "SGXEmotionContent.h"

static SGXEmotionContent *_sharedClient = nil;

static dispatch_once_t onceToken;

@interface SGXEmotionContent() {
    NSMutableArray    *_emotionArr;
    NSMutableArray    *_contentArr;
}

@end

@implementation SGXEmotionContent


+ (instancetype)sharedClient {

    dispatch_once(&onceToken, ^{
        
        _sharedClient = [[SGXEmotionContent alloc] init];
        
    });
    
    return _sharedClient;
}

+ (void)reset {
    _sharedClient   =   nil;
    
}

- (NSMutableArray *) getEmotionArr
{

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *fullpath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"GetFavoriteEmotionList.plist"];
    
    _emotionArr=[[NSMutableArray alloc]initWithContentsOfFile:fullpath];
    return  _emotionArr;
}


@end
