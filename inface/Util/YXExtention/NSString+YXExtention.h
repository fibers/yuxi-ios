//
//  NSString+YXExtention.h
//  inface
//
//  Created by 邢程 on 15/9/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (YXExtention)
/**
 *  判断 是否为签约用户
 *
 *  @return 是否是签约
 */
-(int)isSignedUser;

/*
 
 *  根据图片字符串格式化60px宽度图片路径
 *
 *  @return 格式化后的图片路径
 */
-(instancetype)stringOfW60ImgUrl;
/**
 *  根据图片字符串格式化100x宽度图片路径
 *
 *  @return 格式化后的图片路径
 */
-(instancetype)stringOfW100ImgUrl;
/**
 *  根据图片字符串格式化250x宽度图片路径
 *
 *  @return 格式化后的图片路径
 */
-(instancetype)stringOfW250ImgUrl;
/**
 *  根据图片字符串格式化750px宽度图片路径
 *
 *  @return 格式化后的图片路径
 */
-(instancetype)stringOfW750ImgUrl;
/**
 *  根据图片字符串格式化900px宽度图片路径
 *
 *  @return 格式化后的图片路径
 */
-(instancetype)stringOfW900ImgUrl;
/**
 *  根据毫秒值返回时间字符串
 *
 *  @return 时间字符串
 */
-(NSString *)dateTimeString;

/**
 *  字符长度（中文算一个字，两个英文算一个字）
 *
 *  @return 长度
 */
-(NSInteger)charLength;
@end
