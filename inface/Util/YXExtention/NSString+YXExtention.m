//
//  NSString+YXExtention.m
//  inface
//
//  Created by 邢程 on 15/9/9.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "NSString+YXExtention.h"
@implementation NSString (YXExtention)
-(int)isSignedUser{
    @try {
        if(self == nil|| [self isEqualToString:@""])return NO;
        NSRange range = [self rangeOfString:@"issigned="];
        if(range.length == 0)return NO;
        NSString * str = [self substringWithRange:NSMakeRange(range.location+range.length, 1)];
        return [str intValue];
    }
    @catch (NSException *exception) {
        LOG(@"%@",exception); 
        return NO;
    }
    @finally {
        
    }
}



-(instancetype)stringOfW60ImgUrl{
    
    return [self getImageUrlStringWithWidth:@"!w60"];
}

-(instancetype)stringOfW100ImgUrl{
    
    return [self getImageUrlStringWithWidth:@"!w100"];;
}
-(instancetype)stringOfW250ImgUrl{
    
    return [self getImageUrlStringWithWidth:@"!w250"];;
}
-(instancetype)stringOfW750ImgUrl{
    
    return [self getImageUrlStringWithWidth:@"!w750"];;
}


-(instancetype)stringOfW900ImgUrl{
    
    return [self getImageUrlStringWithWidth:@"!w900"];;
}

/**
 *  根据图片字符串返回相应宽度格式化后的字符串
 *
 *  @param pxw 宽度代码
 *
 *  @return 格式化后的字符串
 */
-(NSString *)getImageUrlStringWithWidth:(NSString *)pxw{
    if(self == nil|| [self isEqualToString:@""])return self;
    NSRange range = [self rangeOfString:@".png"].length == 0 ?[self rangeOfString:@".jpg"]:[self rangeOfString:@".png"];
    if (range.length == 0 ){
//        LOG(@"%@ 不是图片路径",self);
        return self;
    }
    NSMutableString * strTMPM = [NSMutableString stringWithString:self];
    [strTMPM insertString:pxw atIndex:(range.length+range.location)];
    return strTMPM;
}

-(NSString *)dateTimeString{
    NSDate * date =  [NSDate dateWithTimeIntervalSince1970:[self integerValue]];
    return [date transformToFuzzyDate];
}


-(NSInteger)charLength{
    return [self convertToInt:self];
}

-  (NSInteger)convertToInt:(NSString*)strtemp {
    int strlength = 0;
    char* p = (char*)[strtemp cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0 ; i<[strtemp lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
    }
    return (strlength+1)/2;
    
}
@end
