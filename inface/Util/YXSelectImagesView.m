//
//  YXSelectImagesView.m
//  inface
//
//  Created by appleone on 15/8/20.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXSelectImagesView.h"

@implementation YXSelectImagesView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createButtons];
    }
    return self;
}


-(void)createButtons
{
    //创建各类按钮
    float cc_button_width = 60.0f;
    float cc_button_height = 60.0f;
    float button_top_height = 45.0;
    float btn_blank        = (ScreenWidth -3*cc_button_width)/4.0;
    if (!self.takePhotos) {

        _takePhotos = [CCSelectButton buttonWithType:UIButtonTypeCustom];
        self.takePhotos.frame = CGRectMake(btn_blank, button_top_height, cc_button_width, cc_button_height);
        [self.takePhotos setImage:[UIImage imageNamed:@"imagePicker"] forState:UIControlStateNormal];
        [_takePhotos setTitleColor:[UIColor colorWithHexString:@"#4a4a4a"] forState:UIControlStateNormal];
        _takePhotos.titleLabel.font=[UIFont systemFontOfSize:14];
        _takePhotos.titleLabel.textAlignment=NSTextAlignmentCenter;
        [_takePhotos setTitle:@"拍照" forState:UIControlStateNormal];
        [self addSubview:self.takePhotos];

    }
    [self.takePhotos addTarget:self action:@selector(setTakePhotos:) forControlEvents:UIControlEventTouchUpInside];
    
    if (!self.selectImages) {
        self.selectImages = [CCSelectButton buttonWithType:UIButtonTypeCustom];
        [self.selectImages setFrame:CGRectMake(btn_blank*2+cc_button_width, button_top_height, cc_button_width, cc_button_height)];
        [self.selectImages setImage:[UIImage imageNamed:@"imagePhotos"] forState:UIControlStateNormal];
        [_selectImages setTitleColor:[UIColor colorWithHexString:@"#4a4a4a"] forState:UIControlStateNormal];
        _selectImages.titleLabel.font=[UIFont systemFontOfSize:14];
        _selectImages.titleLabel.textAlignment=NSTextAlignmentCenter;
        [_selectImages setTitle:@"相册" forState:UIControlStateNormal];
        [self addSubview:self.selectImages];

    }
    [self.selectImages addTarget:self action:@selector(selectPhotos:) forControlEvents:UIControlEventTouchUpInside];
    
    if (!self.writeBoard) {
        _writeBoard = [CCSelectButton buttonWithType:UIButtonTypeCustom];

        [self.writeBoard setFrame:CGRectMake(btn_blank*3+cc_button_width*2, button_top_height, cc_button_width, cc_button_height)];
        [self.writeBoard setImage:[UIImage imageNamed:@"writeBoard"] forState:UIControlStateNormal];
        [_writeBoard setTitleColor:[UIColor colorWithHexString:@"#4a4a4a"] forState:UIControlStateNormal];
        _writeBoard.titleLabel.font=[UIFont systemFontOfSize:14];
        _writeBoard.titleLabel.textAlignment=NSTextAlignmentCenter;
        [_writeBoard setTitle:@"面板" forState:UIControlStateNormal];

        [self addSubview:self.writeBoard];

    }
    [self.writeBoard addTarget:self action:@selector(setWriteBoard:) forControlEvents:UIControlEventTouchUpInside];
    

}

-(void)setTakePhotos:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(takePhotos)]) {
        //
        [self.delegate takePhotos];
        
    }
}

-(void)selectPhotos:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(selectImages)]) {
        //
        [self.delegate selectImages];
        
    }
    
}

-(void)setWriteBoard:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(clickWriteboard)]) {
        //
        [self.delegate clickWriteboard];
        
    }
    
}
@end
