//
//  EncodeUtil.h
//  inface
//
//  Created by dev01 on 15/8/16.
//  Copyright (c) 2015年 Guoq. All rights reserved.
//
typedef unsigned long   UTF32;  /* at least 32 bits */
typedef unsigned short  UTF16;  /* at least 16 bits */
typedef unsigned char   UTF8;   /* typically 8 bits */
typedef unsigned int    INT;

#define UTF8_ONE_START      (0xOOO1)
#define UTF8_ONE_END        (0x007F)
#define UTF8_TWO_START      (0x0080)
#define UTF8_TWO_END        (0x07FF)
#define UTF8_THREE_START    (0x0800)
#define UTF8_THREE_END      (0xFFFF)

@interface EncodeUtil : NSObject

+ (int) convertUft8ToIOSUtf8 : (UTF8 *) inbuf   :(UTF8 *)outbuf;
+ (int) convertUft8ToIOSUtf8 : (UTF8 *) inbuf   :(UTF8 **)pOutbuf  :(int) outBufLen;
+ (void) getEmojilist;

@end
