//
//  HZSUnderLineLabel.h
//  inface
//
//  Created by huangzengsong on 15/5/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//自定义下划线label
#import <UIKit/UIKit.h>
typedef enum{
    
    LineTypeNone,//没有画线
    LineTypeUp ,// 上边画线
    LineTypeMiddle,//中间画线
    LineTypeDown,//下边画线
    
} LineType ;
@interface HZSUnderLineLabel : UILabel
@property (assign, nonatomic) LineType lineType;
@property (strong, nonatomic) UIColor * lineColor;
@end
