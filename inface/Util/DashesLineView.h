//
//  DashesLineView.h
//  inface
//
//  Created by huangzengsong on 15/5/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//自定义划虚线view
#import <UIKit/UIKit.h>

@interface DashesLineView : UIView
@property(nonatomic)CGPoint startPoint;//虚线起点
@property(nonatomic)CGPoint endPoint;//虚线终点
@property(nonatomic,strong)UIColor* lineColor;//虚线颜色
@end
