//
//  UIBarButtonItem+Extention.h
#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Extention)

+ (UIBarButtonItem *)itemWithtTarget:(id)target anction:(SEL)action image:(NSString *)image highlightimage:(NSString*)highlightimage;
@end
