//
//  view.h

#import <UIKit/UIKit.h>

@interface FloodView : UIView

@property (nonatomic,weak)UIColor  *color;

@property (nonatomic,assign)BOOL isStroke;

@property (nonatomic,assign)BOOL isLine;

@property (nonatomic,assign)CGColorRef FillColor;
@end
