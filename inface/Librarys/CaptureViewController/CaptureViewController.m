//
//  CaptureViewController.m
//  CameraAndPhotolibraryDemo
//
//  Created by 黄增松 on 13-12-9.
//  Copyright (c) 2013年 shanxun. All rights reserved.
//

#import "CaptureViewController.h"

@interface CaptureViewController ()

@end

@implementation CaptureViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];

    UINavigationBar *naviBar= [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 64)];
    [naviBar  setBarTintColor:TABBAR_COLOR];
    [naviBar  setTintColor:[UIColor whiteColor]];
    [self.view addSubview:naviBar];
    
    UINavigationItem *naviItem = [[UINavigationItem alloc] initWithTitle:@"图片裁剪"];
    [naviBar pushNavigationItem:naviItem animated:YES];
    
    [naviBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:BLUECOLOR,NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:17],NSFontAttributeName, nil]];
    
    //返回按钮
    UIBarButtonItem *returnItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(returnButton)];
    returnItem.tintColor=BLUECOLOR;
    naviItem.leftBarButtonItem = returnItem;
    
    //保存按钮
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveButton)];
    doneItem.tintColor=BLUECOLOR;
    naviItem.rightBarButtonItem = doneItem;
    [doneItem release];
    [naviItem release];
    [naviBar release];
    [returnItem release];
    
    
    
    //image为上一个界面传过来的图片资源
    editorView = [[AGSimpleImageEditorView alloc] initWithImage:self.image];
    editorView.frame = CGRectMake(0, 0, self.view.frame.size.width ,  self.view.frame.size.width);
    editorView.center = self.view.center;
    
    //外边框的宽度及颜色
    editorView.borderWidth = 1.f;
    editorView.borderColor = GRAY_COLOR;
    
    //截取框的宽度及颜色
    editorView.ratioViewBorderWidth = 2.f;
    editorView.ratioViewBorderColor = BLUECOLOR;
    
    //截取比例，我这里按正方形1:1截取（可以写成 3./2. 16./9. 4./3.）
    editorView.ratio = 1./1.;
    
    [self.view addSubview:editorView];
}

//返回按钮
-(void)returnButton
{

    [self dismissViewControllerAnimated:YES completion:nil];
    
}

//完成截取
-(void)saveButton
{
    //output为截取后的图片，UIImage类型
    UIImage *resultImage = editorView.output;
    
    //通过代理回传给上一个界面显示
    [self.delegate passImage:resultImage];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
