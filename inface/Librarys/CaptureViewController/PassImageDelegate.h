//
//  PassImageDelegate.h
//  CameraAndPhotolibraryDemo
//
//  Created by 黄增松 on 13-12-9.
//  Copyright (c) 2013年 shanxun. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PassImageDelegate <NSObject>
-(void)passImage:(UIImage *)image;
@end
