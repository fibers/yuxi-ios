//
//  CaptureViewController.h
//  CameraAndPhotolibraryDemo
//
//  Created by 黄增松 on 13-12-9.
//  Copyright (c) 2013年 shanxun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGSimpleImageEditorView.h"
#import "PassImageDelegate.h"
@interface CaptureViewController : UIViewController
{

    AGSimpleImageEditorView *editorView;
}
@property(assign,nonatomic)    BOOL   isAnimating;

@property(nonatomic,assign) id<PassImageDelegate>delegate;
@property(retain,nonatomic)UIImage *image;
@end
