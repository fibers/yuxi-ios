//
//  std.h
//  IOSDuoduo
//
//  Created by murray on 14-5-23.
//  Copyright (c) 2014年 murray. All rights reserved.
//


#import <Foundation/Foundation.h>


extern NSString * const kInvited;
extern NSString * const kUserSetting;
extern NSString * const kLastLoginUser;
extern NSString * const kHasAlertVIP;
extern NSString * const kLastPosition;
extern NSString * const kAccessToken;
extern NSString * const kRefreshToken;
extern NSString * const kTokenExpiredTime;
extern NSString * const kAppVersion;
extern NSString * const kArrowCount;

#define PhotosMessageDir ([[NSString documentPath] stringByAppendingPathComponent:@"/PhotosMessageDir/"])
#define VoiceMessageDir ([[NSString documentPath] stringByAppendingPathComponent:@"/VoiceMessageDir/"])
#define BlacklistDir ([[NSString documentPath] stringByAppendingPathComponent:@"/BlacklistDir/"])
#define Departmentlist ([[NSString documentPath] stringByAppendingPathComponent:@"/department.plist"])
#define fixedlist ([[NSString documentPath] stringByAppendingPathComponent:@"/fixed.plist"])
#define shieldinglist ([[NSString documentPath] stringByAppendingPathComponent:@"/shieldingArray.plist"])


