
//
//  DDChattingModule.m
//  IOSDuoduo
//
//  Created by murray on 14-5-28.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "ChattingModule.h"
#import "DDDatabaseUtil.h"
#import "NSDate+DDAddition.h"
#import "DDUserModule.h"
#import "GetMessageQueueAPI.h"
#import "DDMessageModule.h"
#import "MsgReadACKAPI.h"
#import <math.h>
#import "GetMsgByMsgIDsAPI.h"
#import "DDClientState.h"
#import "DDChatTextCell.h"
static NSUInteger const showPromptGap = 300;
@interface ChattingModule(privateAPI)
- (NSUInteger)p_getMessageCount;
- (void)p_addHistoryMessages:(NSArray*)messages Completion:(DDChatLoadMoreHistoryCompletion)completion;

@end

@implementation ChattingModule
{
    DDChatTextCell* _textCell;

    NSUInteger _earliestDate;
    NSUInteger _lastestDate;
    
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.showingMessages = [[NSMutableArray alloc] init];
        self.ids = [NSMutableArray new];
    }
    return self;
}

- (void)setSessionEntity:(SessionEntity *)sessionEntity
{
    _sessionEntity = sessionEntity;
    
    self.showingMessages = nil;
    self.showingMessages = [[NSMutableArray alloc] init];
}
-(void)getNewMsg:(DDChatLoadMoreHistoryCompletion)completion
{
#warning mark 获取新消息
    [[DDMessageModule shareInstance] getMessageFromServer:0 currentSession:self.sessionEntity count:20 Block:^(NSMutableArray *response, NSError *error) {
        //[self p_addHistoryMessages:response Completion:completion];
        NSUInteger msgID = [[response valueForKeyPath:@"@max.msgID"] integerValue];
        if ( msgID !=0) {
            if (response) {
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"msgTime" ascending:YES];
                [response sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                [[DDDatabaseUtil instance] insertMessages:response success:^{
                    MsgReadACKAPI* readACK = [[MsgReadACKAPI alloc] init];
                    [readACK requestWithObject:@[self.sessionEntity.sessionID,@(msgID),@(self.sessionEntity.sessionType)] Completion:nil];
                    
                } failure:^(NSString *errorDescripe) {
                    
                }];
                [response enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    [self addShowMessage:obj];
                }];
                completion([response count],error);
                
            }
            
            
        }else{
            completion(0,error);
        }
        
    }];
}


-(void)loadHisToryMessageFromServer:(NSUInteger)FromMsgID loadCount:(NSUInteger)count Completion:(DDChatLoadMoreHistoryCompletion)completion
{
    if (self.sessionEntity.sessionID != NULL && ![self.sessionEntity isEqual:nil] && ![self.sessionEntity.sessionID isEqualToString:@""] && self.sessionEntity.sessionID ) {
        //此处之前FromMsgID为1，现在改为0
        if (FromMsgID !=0) {

            [[DDMessageModule shareInstance] getMessageFromServer:FromMsgID currentSession:self.sessionEntity count:count Block:^(NSArray *response, NSError *error) {
                //[self p_addHistoryMessages:response Completion:completion];
                NSUInteger msgID = [[response valueForKeyPath:@"@max.msgID"] integerValue];
                
                if ( msgID !=0) {
                    if (response) {
                           [[DDDatabaseUtil instance] insertMessages:response success:^{
                               MsgReadACKAPI* readACK = [[MsgReadACKAPI alloc] init];
                               [readACK requestWithObject:@[self.sessionEntity.sessionID,@(msgID),@(self.sessionEntity.sessionType)] Completion:nil];
                               
                           } failure:^(NSString *errorDescripe) {
                               
                           }];
                        //不从数据库load  去掉线程阻塞
                        if ([self.sessionEntity.sessionID isEqualToString:[ChatViewController shareInstance].chatSession]) {
                            if (!_isLoadOldMessages) {
                                [self.ids removeAllObjects];
                                [self.showingMessages removeAllObjects];
                            }
                            [self p_addHistoryMessages:response Completion:completion];
                            completion([response count],error);
                            
                        }
                            NSUInteger count = [self p_getMessageCount];
                            [[DDDatabaseUtil instance] loadMessageForSessionID:self.sessionEntity.sessionID pageCount:DD_PAGE_ITEM_COUNT index:count completion:^(NSArray *messages, NSError *error) {
                                //如果是加载历史消息，不能移除当前已经显示的消息
                                //远没有那么简单，若数据没有插进数据库，则不能直接移除掉数据的
                                
                                if (messages.count == 0) {
                                    if ([self.sessionEntity.sessionID isEqualToString:[ChatViewController shareInstance].chatSession]) {
                                        if (!_isLoadOldMessages) {
                                            [self.ids removeAllObjects];
                                            [self.showingMessages removeAllObjects];
                                        }
                                        [self p_addHistoryMessages:response Completion:completion];
                                        completion([response count],error);
                                        
                                    }
                                }else{
                                    if ([self.sessionEntity.sessionID isEqualToString:[ChatViewController shareInstance].chatSession]) {
                                        if (!_isLoadOldMessages) {
                                            [self.ids removeAllObjects];
                                            [self.showingMessages removeAllObjects];
                                        }
                                        [self p_addHistoryMessages:messages Completion:completion];//填消息
                                           completion([messages count],error);
                                    }
                                    
                                }
                                
                            }];

                            

   
                    }
                    
                }else{
                    completion(0,error);
                }
                
            }];
        }else{
            
            completion(0,nil);
        }
    }
    
}

#pragma mark  从网络获取数据
-(void)loadHostoryMessageFromServer:(NSUInteger)FromMsgID Completion:(DDChatLoadMoreHistoryCompletion)completion{
    
    [self loadHisToryMessageFromServer:FromMsgID loadCount:19 Completion:completion];

}



#pragma mark获取历史消息，chattingModule获取历史消息
- (void)loadMoreHistoryCompletion:(DDChatLoadMoreHistoryCompletion)completion ifLoadldMessage:(BOOL)loadOldMessage
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    
    NSUInteger count = [self p_getMessageCount];
    //从数据库获取已有的消息
    [[DDDatabaseUtil instance] loadMessageForSessionID:self.sessionEntity.sessionID pageCount:DD_PAGE_ITEM_COUNT index:count completion:^(NSArray *messages, NSError *error) {

        //after loading finish ,then add to messages
        if ([DDClientState shareInstance].networkState == DDNetWorkDisconnect) {
            //需要判断返回消息时是否已经切换了聊天对象
            if ([self.sessionEntity.sessionID isEqualToString:[ChatViewController shareInstance].chatSession]) {
                [self p_addHistoryMessages:messages Completion:completion];

            }
        }
        
        else{
            if ([messages count] !=0) {
                if (!self.isLoadOldMessages) {
                    //如果在加载历史数据，此时不用把数据库的消息加进来
                    if ([self.sessionEntity.sessionID isEqualToString:[ChatViewController shareInstance].chatSession]) {
                        [self p_addHistoryMessages:messages Completion:completion];
                        
                    }
                }

                BOOL isHaveMissMsg = [self p_isHaveMissMsg:messages];
                if (isHaveMissMsg || ([self getMiniMsgId] - [self getMaxMsgId:messages] !=0)) {
                    //有消息遗漏
                        [self loadHostoryMessageFromServer:[self getMiniMsgId] Completion:^(NSUInteger addcount, NSError *error) {
                        if (addcount) {
                            completion(addcount,error);
                        }else{
                            if ([self.sessionEntity.sessionID isEqualToString:[ChatViewController shareInstance].chatSession]) {
                                [self p_addHistoryMessages:messages Completion:completion];
        
                            }
                        }
                    }];

                }
                else{
                    //检查消息是否连续,如果不是加载历史数据不需要再次执行此方法
                    if ([self.sessionEntity.sessionID isEqualToString:[ChatViewController shareInstance].chatSession]  && self.isLoadOldMessages) {
                        [self p_addHistoryMessages:messages Completion:completion];
                        
                    }
                }
            }
            else{
               // messages count = 0的时候
                //数据库中已获取不到消息
                //拿出当前最小的msgid去服务端取

                    [self loadHostoryMessageFromServer:[self getMiniMsgId] Completion:^(NSUInteger addcount, NSError *error) {
                        completion(addcount,error);
                    }];
    
            }
            

        }
        
    }];
    });
}


#pragma mark 最小id的问题
-(NSUInteger )getMiniMsgId
{
    if ([self.showingMessages count] == 0 || !self.isLoadOldMessages) {
        return self.sessionEntity.lastMsgID;
    }
    

    __block NSInteger minMsgID =[self getMaxMsgId:self.showingMessages];

    [self.showingMessages enumerateObjectsUsingBlock:^(DDMessageEntity * obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[DDMessageEntity class]]) {
            if(obj.msgID <minMsgID)
            {
                minMsgID = obj.msgID;
            }
        }
    }];
    return minMsgID;
}



- (float)messageHeight:(DDMessageEntity*)message
{
    
    if (message.msgContentType == DDMessageTypeText ) {
            if (!_textCell)
            {
                _textCell = [[DDChatTextCell alloc] init];
            }
            return [_textCell cellHeightForMessage:message];

    }else if (message.msgContentType == DDMessageTypeVoice )
    {
        return 60;
    }else if(message.msgContentType == DDMessageTypeImage || message.msgContentType == DDMEssageEmotion)
    {
        //需要判断是否是群聊，并且是否是别人发的
        NSString * senderid = message.senderId;
        NSRange  range = [message.sessionId rangeOfString:@"group_"];
        if(![senderid isEqualToString:USER_ID] && range.length >0)
        {
            return 176;
        }
         return 165;
    }
    else
    {
        return 135;
    }

}

- (void)addShowMessage:(DDMessageEntity*)message
{
    if (![self.ids containsObject:@(message.msgID)]) {
        if (message.msgTime - _lastestDate > showPromptGap)
        {
            _lastestDate = message.msgTime;
            DDPromptEntity* prompt = [[DDPromptEntity alloc] init];
            NSDate* date = [NSDate dateWithTimeIntervalSince1970:message.msgTime];
            prompt.message = [date promptDateString];
            [self.showingMessages addObject:prompt];

        }
        [self.ids addObject:@(message.msgID)];

        NSArray *array = [[self class] p_spliteMessage:message];
        [array enumerateObjectsUsingBlock:^(DDMessageEntity* obj, NSUInteger idx, BOOL *stop) {
            [[self mutableArrayValueForKeyPath:@"showingMessages"] addObject:obj];
        }];
        
   
    }
}


-(void)addCurrentShowMessages:(DDMessageEntity *)message;
{
    if (![self.ids containsObject:@(message.msgID)]) {
//        [self.showingMessages addObject:message];
        [self.ids addObject:@(message.msgID)];

        NSArray *array = [[self class] p_spliteMessage:message];
        [array enumerateObjectsUsingBlock:^(DDMessageEntity* obj, NSUInteger idx, BOOL *stop) {
            [[self mutableArrayValueForKeyPath:@"showingMessages"] addObject:obj];
        }];
        
    }

}

- (void)addShowMessages:(NSArray*)messages
{

    [[self mutableArrayValueForKeyPath:@"showingMessages"] addObjectsFromArray:messages];
}
-(void)getCurrentUser:(void(^)(DDUserEntity *))block
{
    [[DDUserModule shareInstance] getUserForUserID:self.sessionEntity.sessionID  Block:^(DDUserEntity *user) {
         block(user);
    }];
   
}


- (void)updateSessionUpdateTime:(NSUInteger)time
{
    [self.sessionEntity updateUpdateTime:time];
    _lastestDate = time;
}


#warning 获取历史消息存在疑问？？？？
#pragma mark PrivateAPI
- (NSUInteger)p_getMessageCount
{
    if (!self.isLoadOldMessages) {
        return 0;
    }

    __block NSUInteger count = 0;
    [self.showingMessages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:NSClassFromString(@"DDMessageEntity")])
        {
            count ++;
        }
    }];
    return count;
}

- (void)p_addHistoryMessages:(NSArray*)messages Completion:(DDChatLoadMoreHistoryCompletion)completion
{

        __block NSUInteger tempEarliestDate = [[messages valueForKeyPath:@"@min.msgTime"] integerValue];
        __block NSUInteger tempLasteestDate = 0;
        NSUInteger itemCount = [self.showingMessages count];
    NSMutableArray *tmp = [NSMutableArray arrayWithArray:messages];
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"msgTime" ascending:YES];
//    [tmp sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        NSMutableArray* tempMessages = [[NSMutableArray alloc] init];
        for (NSInteger index = [tmp count] - 1; index >= 0;index --)
        {
            
            DDMessageEntity* message = tmp[index];
            
            if ([self.ids containsObject:@(message.msgID)]) {
                continue;
            }

            if (message.msgTime - tempLasteestDate > showPromptGap)
            {
                DDPromptEntity* prompt = [[DDPromptEntity alloc] init];
                NSDate* date = [NSDate dateWithTimeIntervalSince1970:message.msgTime];
                prompt.message = [date promptDateString];
                [tempMessages addObject:prompt];
            }
            tempLasteestDate = message.msgTime;
            NSArray *array = [[self class] p_spliteMessage:message];
            [array enumerateObjectsUsingBlock:^(DDMessageEntity * obj, NSUInteger idx, BOOL *stop) {
    
                [self.ids addObject:@(message.msgID)];
                [tempMessages addObject:obj];
            }];
            
        }
        
        if ([self.showingMessages count] == 0)
        {
            [[self mutableArrayValueForKeyPath:@"showingMessages"] addObjectsFromArray:tempMessages];
            _earliestDate = tempEarliestDate;
            _lastestDate = tempLasteestDate;
        }
        else
        {
            if (!self.isLoadOldMessages) {
                //获取新消息
                [[self mutableArrayValueForKeyPath:@"showingMessages"] addObjectsFromArray:tempMessages];

            }else{
                //加载历史消息
                [self.showingMessages insertObjects:tempMessages atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [tempMessages count])]];

            }
            
            _earliestDate = tempEarliestDate;
        }
        NSUInteger newItemCount = [self.showingMessages count];
        completion(newItemCount - itemCount,nil);
}


+ (NSArray*)p_spliteMessage:(DDMessageEntity*)message
{
    NSMutableArray* messageContentArray = [[NSMutableArray alloc] init];

    if ( [ message.msgContent rangeOfString:DD_MESSAGE_IMAGE_PREFIX].length > 0)
    {
        NSString* messageContent = [message msgContent];
        if ([messageContent rangeOfString:DD_MESSAGE_IMAGE_PREFIX].length > 0 && [messageContent rangeOfString:DD_IMAGE_LOCAL_KEY].length > 0 && [messageContent rangeOfString:DD_IMAGE_URL_KEY].length > 0) {
            DDMessageEntity* messageEntity = [[DDMessageEntity alloc] initWithMsgID:[DDMessageModule getMessageID] msgType:message.msgType msgTime:message.msgTime sessionID:message.sessionId senderID:message.senderId msgContent:messageContent toUserID:message.toUserID];
            messageEntity.msgContentType = DDMessageTypeImage;
            messageEntity.state = DDmessageSendSuccess;
        }else{

            NSArray* tempMessageContent = [messageContent componentsSeparatedByString:DD_MESSAGE_IMAGE_PREFIX];
            [tempMessageContent enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSString* content = (NSString*)obj;
                if ([content length] > 0)
                {
                    NSRange suffixRange = [content rangeOfString:DD_MESSAGE_IMAGE_SUFFIX];
                    if (suffixRange.length > 0)
                    {
                        //是图片,再拆分
                        NSString* imageContent = [NSString stringWithFormat:@"%@%@",DD_MESSAGE_IMAGE_PREFIX,[content substringToIndex:suffixRange.location + suffixRange.length]];
                        DDMessageEntity* messageEntity = [[DDMessageEntity alloc] initWithMsgID:[DDMessageModule getMessageID] msgType:message.msgType msgTime:message.msgTime sessionID:message.sessionId senderID:message.senderId msgContent:imageContent toUserID:message.toUserID];
                        messageEntity.msgContentType = DDMessageTypeImage;
                        messageEntity.state = DDmessageSendSuccess;
                        [messageContentArray addObject:messageEntity];
                        
                        
                        NSString* secondComponent = [content substringFromIndex:suffixRange.location + suffixRange.length];
                        if (secondComponent.length > 0)
                        {
                            DDMessageEntity* secondmessageEntity = [[DDMessageEntity alloc] initWithMsgID:[DDMessageModule getMessageID] msgType:message.msgType msgTime:message.msgTime sessionID:message.sessionId senderID:message.senderId msgContent:secondComponent toUserID:message.toUserID];
                            secondmessageEntity.msgContentType = DDMessageTypeText;
                            secondmessageEntity.state = DDmessageSendSuccess;
                            [messageContentArray addObject:secondmessageEntity];
                        }
                    }
                    else
                    {
                        
                        DDMessageEntity* messageEntity = [[DDMessageEntity alloc] initWithMsgID:[DDMessageModule getMessageID] msgType:message.msgType msgTime:message.msgTime sessionID:message.sessionId senderID:message.senderId msgContent:content toUserID:message.toUserID];
                        messageEntity.state = DDmessageSendSuccess;
                        [messageContentArray addObject:messageEntity];
                    }
                }
            }];
        }
    }
   
    if ([messageContentArray count] == 0)
    {
        [messageContentArray addObject:message];
    }
    
    return messageContentArray;
        
}
-(NSInteger)getMaxMsgId:(NSArray *)array
{
    __block NSInteger maxMsgID =0;
    [array enumerateObjectsUsingBlock:^(DDMessageEntity * obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[DDMessageEntity class]]) {
            if (obj.msgID > maxMsgID && obj.msgID<LOCAL_MSG_BEGIN_ID) {
                maxMsgID =obj.msgID;
            }
        }
        }
     ];
    return maxMsgID;
}
- (BOOL)p_isHaveMissMsg:(NSArray*)messages
{
  
    __block NSInteger maxMsgID =[self getMaxMsgId:messages];//会话的最大ID
    __block NSInteger minMsgID =[self getMiniMsgId];//会话的最小ID
    [messages enumerateObjectsUsingBlock:^(DDMessageEntity * obj, NSUInteger idx, BOOL *stop) {
        if (obj.msgID > maxMsgID && obj.msgID<LOCAL_MSG_BEGIN_ID) {
//            maxMsgID =obj.msgID;
        }else if(obj.msgID <minMsgID)
        {
            minMsgID = obj.msgID;
        }
    }];
    
//   NSUInteger maxMsgID = [msgIds valueForKeyPath:@"@max"];
//    NSUInteger minMsgID = [msgIds valueForKeyPath:@"@min"];
 
    NSUInteger diff = maxMsgID - minMsgID;
    if (diff != 19) {
        return YES;
    }
    return NO;
}

-(void)checkMsgList:(DDChatLoadMoreHistoryCompletion)completion
{
    NSMutableArray *tmp = [NSMutableArray arrayWithArray:self.showingMessages];
    [tmp enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[DDPromptEntity class]]) {
            [tmp removeObject:obj];
        }else{
            DDMessageEntity *msg = obj;
            if (msg.msgID>=LOCAL_MSG_BEGIN_ID) {
                [tmp removeObject:obj];
            }
        }
       
    }];
    
    [tmp enumerateObjectsUsingBlock:^(DDMessageEntity *obj, NSUInteger idx, BOOL *stop) {
        if (idx +1 < [tmp count]) {
            DDMessageEntity * msg = [tmp objectAtIndex:idx+1];
            if ((obj.msgID - msg.msgID) !=1) {
                [self loadHisToryMessageFromServer:MIN(obj.msgID, msg.msgID) loadCount:(obj.msgID - msg.msgID) Completion:^(NSUInteger addcount, NSError *error) {
                    completion(addcount,error);
                }];
            }
        }
        
    }];
}
@end

@implementation DDPromptEntity

@end
