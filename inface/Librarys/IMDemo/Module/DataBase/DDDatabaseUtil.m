//  DDDatabaseUtil.m
//  Duoduo
//
//  Created by murray on 14-3-21.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDDatabaseUtil.h"
#import "DDMessageEntity.h"
#import "DDUserEntity.h"
#import "DDUserModule.h"
#import "GroupEntity.h"
#import "NSString+DDPath.h"
#import "NSDictionary+Safe.h"
#import "DDepartment.h"
#import "SessionEntity.h"
#define DB_FILE_NAME                    @"tt.sqlite"//数据库路径后缀
#define TABLE_MESSAGE                   @"message"//聊天页面聊天信息
#define TABLE_ALL_CONTACTS              @"allContacts"//所有联系人
#define TABLE_DEPARTMENTS               @"departments"//部门
#define TABLE_GROUPS                    @"groups"//群组
#define TABLE_RECENT_SESSION            @"recentSession"//首页消息列表
#define TABLE_ADD_FRIENDS               @"addfriend"   //被添加为好友
#define TABLE_CURRETGROUP_NAMES         @"currentGroups"//存储临时群的群名
#define TABLE_DRAFT_MESSAGE             @"draftMessges" //保存草稿

#define TABLE_EDIT_MESSAGE             @"editMessages" //保存草稿

#define TABLE_GROUP_SET                 @"setGroupMessage"//群的消息屏蔽

#define TABLE_GROUP_NOTY                @"groupNotify"  //群公告

#define SQL_CREATE_MESSAGE              [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (messageID integer,sessionId text ,fromUserId text,toUserId text,content text, status integer, msgTime integer, sessionType integer,messageContentType integer,messageType integer,info text,reserve1 integer,reserve2 text,primary key (messageID,sessionId))",TABLE_MESSAGE]
#define SQL_CREATE_MESSAGE_INDEX        [NSString stringWithFormat:@"CREATE INDEX msgid on %@(messageID)",TABLE_MESSAGE]


#define SQL_CREATE_DEPARTMENTS      [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID integer UNIQUE,parentID integer,title text, status integer,priority integer)",TABLE_DEPARTMENTS]


#define SQL_CREATE_ALL_CONTACTS      [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID text UNIQUE,Name text,Nick text,Avatar text, Department text,DepartID text, Email text,Postion text,Telphone text,Sex integer,updated real,pyname text)",TABLE_ALL_CONTACTS]

#define SQL_CREATE_GROUPS     [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID text UNIQUE,Avatar text, GroupType integer, Name text,CreatID text,Users Text,LastMessage Text,updated real,isshield integer,version integer)",TABLE_GROUPS]

#define SQL_CREATE_CONTACTS_INDEX        [NSString stringWithFormat:@"CREATE UNIQUE ID on %@(ID)",TABLE_ALL_CONTACTS]

#define SQL_CREATE_RECENT_SESSION     [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID text UNIQUE,avatar text, type integer, name text,updated real,isshield integer,users Text , unreadCount integer, lasMsg text , lastMsgId integer)",TABLE_RECENT_SESSION]
#define SQL_CREATE_ADDFRIENDS_LIST    [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (fromid text, toid text,groupid text,result text,type text,name text,portrait text,haschange text,grouptype text,detailtext text,addmessage text,timeinterval text)",TABLE_ADD_FRIENDS]
#define SQL_CREATE_CURRENT_GROUP     [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (groupid text, name text,creatorname text,portrait text,creatortime text,creatorid text)",TABLE_CURRETGROUP_NAMES]
#define SQL_CREATE_DRAFT_MESSAGE    [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (message text,sessionid text)",TABLE_DRAFT_MESSAGE]

#define SQL_CREATE_EDIT_MESSAGE    [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (message text,sessionid text)",TABLE_EDIT_MESSAGE]

#define SQL_CREATE_SET_GROUPMESSAGE    [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (receivetype text,sessionid text)",TABLE_GROUP_SET]

#define SQL_CREATE_GROUP_NOTIFY            [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (title text,content  text, datetime text,groupid  text )",TABLE_GROUP_NOTY]


@implementation DDDatabaseUtil
{
    FMDatabase* _database;
    FMDatabaseQueue* _dataBaseQueue;
}
+ (instancetype)instance
{
    static DDDatabaseUtil* g_databaseUtil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_databaseUtil = [[DDDatabaseUtil alloc] init];
        [NSString stringWithFormat:@""];
    });
    return g_databaseUtil;
}
-(void)reOpenNewDB
{
    
    [self openCurrentUserDB];
}
- (id)init
{
    self = [super init];
    if (self)
    {
        //初始化数据库
        [self openCurrentUserDB];
    }
    return self;
}

- (void)openCurrentUserDB
{
    if (_database)
    {
        [_database close];
        _database = nil;
    }
    _dataBaseQueue = [FMDatabaseQueue databaseQueueWithPath:[DDDatabaseUtil dbFilePath]];
    _database = [FMDatabase databaseWithPath:[DDDatabaseUtil dbFilePath]];
    
    if (![_database open])
    {
    }
    else
    {
        //创建
        [_dataBaseQueue inDatabase:^(FMDatabase *db) {
            if (![_database tableExists:TABLE_MESSAGE])
            {
                [self createTable:SQL_CREATE_MESSAGE];
            }
            if (![_database tableExists:TABLE_DEPARTMENTS])
            {
                [self createTable:SQL_CREATE_DEPARTMENTS];
            }
            if (![_database tableExists:TABLE_ALL_CONTACTS]) {
                [self createTable:SQL_CREATE_ALL_CONTACTS];
            }
            if (![_database tableExists:SQL_CREATE_GROUPS]) {
                [self createTable:SQL_CREATE_GROUPS];
            }
            if (![_database tableExists:SQL_CREATE_RECENT_SESSION]) {
                [self createTable:SQL_CREATE_RECENT_SESSION];
            }
            if (![_database tableExists:SQL_CREATE_ADDFRIENDS_LIST]) {
                [self createTable:SQL_CREATE_ADDFRIENDS_LIST];
            }
            if (![_database tableExists:SQL_CREATE_CURRENT_GROUP]) {
                [self createTable:SQL_CREATE_CURRENT_GROUP];
            }
            if (![_database tableExists:SQL_CREATE_DRAFT_MESSAGE]) {
                [self createTable:SQL_CREATE_DRAFT_MESSAGE];
            }
            if (![_database tableExists:SQL_CREATE_EDIT_MESSAGE]) {
                [self createTable:SQL_CREATE_EDIT_MESSAGE];
            }
            if (![_database tableExists:SQL_CREATE_SET_GROUPMESSAGE]) {
                [self createTable:SQL_CREATE_SET_GROUPMESSAGE];
            }
            if (![_database tableExists:SQL_CREATE_GROUP_NOTIFY]) {
                [self createTable:SQL_CREATE_GROUP_NOTIFY];
            }
        }];
    }
}



+(NSString *)dbFilePath
{
    NSString* directorPath = [NSString userExclusiveDirection];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    //用户的db是否存在，若不存在则创建相应的DB目录
    BOOL isDirector = NO;
    BOOL isExiting = [fileManager fileExistsAtPath:directorPath isDirectory:&isDirector];
    
    if (!isExiting)
    {
        BOOL createDirection = [fileManager createDirectoryAtPath:directorPath
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:nil];
        if (!createDirection)
        {
        }
    }
    
    NSString *dbPath = [directorPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@",[RuntimeStatus instance].user.objID,DB_FILE_NAME]];
    return dbPath;
}

-(BOOL)createTable:(NSString *)sql          //创建表
{
    BOOL result = NO;
    [_database setShouldCacheStatements:YES];
    NSString *tempSql = [NSString stringWithFormat:@"%@",sql];
    result = [_database executeUpdate:tempSql];
    // [_database executeUpdate:SQL_CREATE_MESSAGE_INDEX];
    //BOOL dd =[_database executeUpdate:SQL_CREATE_CONTACTS_INDEX];
    
    return result;
}
-(BOOL)clearTable:(NSString *)tableName
{
    BOOL result = NO;
    [_database setShouldCacheStatements:YES];
    NSString *tempSql = [NSString stringWithFormat:@"DELETE FROM %@",tableName];
    result = [_database executeUpdate:tempSql];
    //    [_database executeUpdate:SQL_CREATE_MESSAGE_INDEX];
    //    //BOOL dd =[_database executeUpdate:SQL_CREATE_CONTACTS_INDEX];
    //
    return result;
}
- (DDMessageEntity*)messageFromResult:(FMResultSet*)resultSet
{
    
    NSString* sessionID = [resultSet stringForColumn:@"sessionId"];
    NSString* fromUserId = [resultSet stringForColumn:@"fromUserId"];
    NSString* toUserId = [resultSet stringForColumn:@"toUserId"];
    NSString* content = [resultSet stringForColumn:@"content"];
    NSTimeInterval msgTime = [resultSet doubleForColumn:@"msgTime"];
    MsgType messageType = [resultSet intForColumn:@"messageType"];
    NSUInteger messageContentType = [resultSet intForColumn:@"messageContentType"];
    NSUInteger messageID = [resultSet intForColumn:@"messageID"];
    NSUInteger messageState = [resultSet intForColumn:@"status"];
    
    DDMessageEntity* messageEntity = [[DDMessageEntity alloc] initWithMsgID:messageID
                                                                    msgType:messageType
                                                                    msgTime:msgTime
                                                                  sessionID:sessionID
                                                                   senderID:fromUserId
                                                                 msgContent:content
                                                                   toUserID:toUserId];
    messageEntity.state = messageState;
    messageEntity.msgContentType = messageContentType;
    NSString* infoString = [resultSet stringForColumn:@"info"];
    if (infoString)
    {
        NSData* infoData = [infoString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* info = [NSJSONSerialization JSONObjectWithData:infoData options:0 error:nil];
        NSMutableDictionary* mutalInfo = [NSMutableDictionary dictionaryWithDictionary:info];
        messageEntity.info = mutalInfo;
        
    }
    return messageEntity;
}

- (DDUserEntity*)userFromResult:(FMResultSet*)resultSet
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic safeSetObject:[resultSet stringForColumn:@"Name"] forKey:@"name"];
    [dic safeSetObject:[resultSet stringForColumn:@"Nick"] forKey:@"nickName"];
    [dic safeSetObject:[resultSet stringForColumn:@"ID"] forKey:@"userId"];
    [dic safeSetObject:[resultSet stringForColumn:@"Department"] forKey:@"department"];
    [dic safeSetObject:[resultSet stringForColumn:@"Postion"] forKey:@"position"];
    [dic safeSetObject:[NSNumber numberWithInt:[resultSet intForColumn:@"Sex"]] forKey:@"sex"];
    [dic safeSetObject:[resultSet stringForColumn:@"DepartID"] forKey:@"departId"];
    [dic safeSetObject:[resultSet stringForColumn:@"Telphone"] forKey:@"telphone"];
    [dic safeSetObject:[resultSet stringForColumn:@"Avatar"] forKey:@"avatar"];
    [dic safeSetObject:[resultSet stringForColumn:@"Email"] forKey:@"email"];
    [dic safeSetObject:@([resultSet longForColumn:@"updated"]) forKey:@"lastUpdateTime"];
    [dic safeSetObject:[resultSet stringForColumn:@"pyname"] forKey:@"pyname"];
    DDUserEntity* user = [DDUserEntity dicToUserEntity:dic];
    
    return user;
}

-(GroupEntity *)groupFromResult:(FMResultSet *)resultSet
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic safeSetObject:[resultSet stringForColumn:@"Name"] forKey:@"name"];
    [dic safeSetObject:[resultSet stringForColumn:@"ID"] forKey:@"groupId"];
    [dic safeSetObject:[resultSet stringForColumn:@"Avatar"] forKey:@"avatar"];
    [dic safeSetObject:[NSNumber numberWithInt:[resultSet intForColumn:@"GroupType"]] forKey:@"groupType"];
    [dic safeSetObject:@([resultSet longForColumn:@"updated"]) forKey:@"lastUpdateTime"];
    [dic safeSetObject:[resultSet stringForColumn:@"CreatID"] forKey:@"creatID"];
    [dic safeSetObject:[resultSet stringForColumn:@"Users"] forKey:@"Users"];
    [dic safeSetObject:[resultSet stringForColumn:@"LastMessage"] forKey:@"lastMessage"];
    [dic safeSetObject:[NSNumber numberWithInt:[resultSet intForColumn:@"isshield"]] forKey:@"isshield"];
    [dic safeSetObject:[NSNumber numberWithInt:[resultSet intForColumn:@"version"]] forKey:@"version"];
    GroupEntity* group = [GroupEntity dicToGroupEntity:dic];
    
    return group;
}

- (DepartInfo*)departmentFromResult:(FMResultSet*)resultSet
{
    
//    NSDictionary *dic = @{@"departID":@( [resultSet intForColumn:@"ID"]),
//                          @"title":[resultSet stringForColumn:@"title"],
//                          @"description":[resultSet stringForColumn:@"description"],
//                          @"leader":[resultSet stringForColumn:@"leader"],
//                          @"parentID":[resultSet stringForColumn:@"parentID"],
//                          @"status":[NSNumber numberWithInt:[resultSet intForColumn:@"status"]],
//                          @"count":[NSNumber numberWithInt:[resultSet intForColumn:@"count"]],
//                          };
    DepartInfoBuilder *info = [DepartInfo builder];
    [info setDeptId:[resultSet intForColumn:@"ID"]];
    [info setParentDeptId:[resultSet intForColumn:@"parentID"]];
    [info setPriority:[resultSet intForColumn:@"priority"]];
    [info setDeptName:[resultSet stringForColumn:@"title"]];
    [info setDeptStatus:[resultSet intForColumn:@"status"]];
    DepartInfo *deaprtment = [info build];
    return deaprtment;
}
#pragma mark Message


#pragma mark 数据库修改线程优先级
- (void)loadMessageForSessionID:(NSString*)sessionID pageCount:(int)pagecount index:(NSInteger)index completion:(LoadMessageInSessionCompletion)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        NSMutableArray* array = [[NSMutableArray alloc] init];
        [db open];
        if ([db tableExists:TABLE_MESSAGE])
        {
            [db setShouldCacheStatements:YES];
            
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM message where sessionId=? ORDER BY msgTime DESC limit ?,?"];
            FMResultSet* result = [db executeQuery:sqlString,sessionID,[NSNumber numberWithInteger:index],[NSNumber numberWithInteger:pagecount]];
            while ([result next])
            {
                DDMessageEntity* message = [self messageFromResult:result];
                [array addObject:message];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
            
                completion(array,nil);
            });
        }
        [db close];

    }];
}

- (void)loadMessageForSessionID:(NSString*)sessionID afterMessage:(DDMessageEntity*)message completion:(LoadMessageInSessionCompletion)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSMutableArray* array = [[NSMutableArray alloc] init];
        if ([_database tableExists:TABLE_MESSAGE])
        {
            [_database setShouldCacheStatements:YES];
            NSString* sqlString = [NSString stringWithFormat:@"select * from %@ where sessionId = ? AND messageID >= ? order by msgTime DESC,rowid DESC",TABLE_MESSAGE];
            FMResultSet* result = [_database executeQuery:sqlString,sessionID,message.msgID];
            while ([result next])
            {
                DDMessageEntity* message = [self messageFromResult:result];
                [array addObject:message];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(array,nil);
            });
        }
        [db close];
    }];
}

- (void)getLasetCommodityTypeImageForSession:(NSString*)sessionID completion:(DDGetLastestCommodityMessageCompletion)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([_database tableExists:TABLE_MESSAGE])
        {
            [_database setShouldCacheStatements:YES];
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * from %@ where sessionId=? AND messageType = ? ORDER BY msgTime DESC,rowid DESC limit 0,1",TABLE_MESSAGE];
            FMResultSet* result = [_database executeQuery:sqlString,sessionID,@(4)];
            DDMessageEntity* message = nil;
            while ([result next])
            {
                message = [self messageFromResult:result];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(message);
            });
        }
        [db close];
    }];
}

- (void)getLastestMessageForSessionID:(NSString*)sessionID completion:(DDDBGetLastestMessageCompletion)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([_database tableExists:TABLE_MESSAGE])
        {
            [_database setShouldCacheStatements:YES];
            
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ where sessionId=? and status = 2 ORDER BY messageId DESC limit 0,1",TABLE_MESSAGE];
            
            FMResultSet* result = [_database executeQuery:sqlString,sessionID];
            DDMessageEntity* message = nil;
            while ([result next])
            {
                message = [self messageFromResult:result];
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(message,nil);
                });
                
                break;
            }
            if(message == nil){
                completion(message,nil);
            }
        }
        [db close];
    }];
}


#pragma mark 存储群公告内容
-(void)saveGroupNotify:(NSString *)title content:(NSString*)content  groupid:(NSString *)groupId  dateTime:(NSString*)dateTime
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_GROUP_NOTY]) {
            NSString * sql = [NSString stringWithFormat:@"delete from groupNotify where groupid = ?"];
            BOOL delete = [db executeUpdate:sql,groupId];
            NSString * insert = [NSString stringWithFormat:@"insert into %@ values(?,?,?,?)",TABLE_GROUP_NOTY];
            [ db executeUpdate:insert,title,content,dateTime,groupId ];
        }
        [db close];
    }];
}



#pragma mark  获取群公告内容
-(void)getGroupNotify:(NSString*)groupId success:(void(^)(NSString * title,NSString * content,NSString * dateTime))success
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        
        [db open];
        
        if ([db tableExists:TABLE_GROUP_NOTY]) {
            NSString * sql = [NSString stringWithFormat:@"select * from %@ where groupid = ?",TABLE_GROUP_NOTY];
            
            FMResultSet * result = [db executeQuery:sql,groupId];
            
            NSString * title = @"";
            
            NSString * content = @"";
            
            NSString * dateTime = @"";
            while ([result next]) {
                title = [result stringForColumn:@"title"];
                
                content = [result stringForColumn:@"content"];
                
                dateTime = [result stringForColumn:@"datetime"];
            }
            if (![content isEqualToString:@""] && content != NULL && ![content isEqual:nil]) {
                success(title,content,dateTime);

            }
        }
    }];
    
}


#pragma mark  存储面板草稿
-(void)insertEditMessage:(NSString *)message sessionId:(NSString*)sessionId //存储草稿
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        
        if ([db tableExists:TABLE_EDIT_MESSAGE]) {
            
            NSString * sql = [NSString stringWithFormat:@"DELETE  from editMessages where sessionid = ? "];
            BOOL set = [db executeUpdate:sql,sessionId];
            NSString * sqlUrl =  [NSString stringWithFormat:@"INSERT  into %@ VALUES(?,?)",TABLE_EDIT_MESSAGE] ;
            [db executeUpdate:sqlUrl,message,sessionId];
            
        }
        [db close];
        
    }];
    
    
}


#pragma mark  获取面板草稿
-(void)getEditMessage:(NSString*)sessionId  success:(void (^)(id message))success//获取草稿消息
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_EDIT_MESSAGE])
        {
            [db setShouldCacheStatements:YES];
            
            
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ where sessionid=?  ",TABLE_EDIT_MESSAGE];
            
            FMResultSet* result = [db executeQuery:sqlString,sessionId];
            NSString * message = @"";
            while ([result next])
            {
                message = [result stringForColumn:@"message"];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                success(message);
            });
        }
        [db close];
        
    }];
    
    
}




#pragma mark 存储草稿消息

-(void)insertDraftMessage:(NSString *)message sessionId:(NSString*)sessionId //存储草稿
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];


    if ([db tableExists:TABLE_DRAFT_MESSAGE]) {
        
        NSString * sql = [NSString stringWithFormat:@"DELETE  from draftMessges where sessionid = ? "];
        BOOL set = [db executeUpdate:sql,sessionId];
        NSString * sqlUrl =  [NSString stringWithFormat:@"INSERT  into %@ VALUES(?,?)",TABLE_DRAFT_MESSAGE] ;
        [db executeUpdate:sqlUrl,message,sessionId];
        
    }
    [db close];
    
}];


}



#pragma mark 获取草稿消息
-(void)getDraftMessage:(NSString*)sessionId  success:(void (^)(id message))success//获取草稿消息
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_DRAFT_MESSAGE])
        {
            [db setShouldCacheStatements:YES];
  
            
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ where sessionid=?  ",TABLE_DRAFT_MESSAGE];
            
            FMResultSet* result = [db executeQuery:sqlString,sessionId];
            NSString * message = @"";
            while ([result next])
            {
                message = [result stringForColumn:@"message"];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                success(message);
            });
        }
        [db close];
    }];
}


#pragma mark 保存群消息设置的状态
-(void)saveGroupReceiveMessage:(NSString *)sessionId setType:(NSString*)setType;
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_GROUP_SET]) {
            //
            NSString * sqlString = [NSString stringWithFormat:@"select * from %@ where sessionid= ?",TABLE_GROUP_SET];
            
            FMResultSet * result = [db executeQuery:sqlString,sessionId];
            NSString  * groupid =@"";
            while ([result next]) {
                groupid = [result stringForColumn:@"sessionid"];
            }
            if (groupid && ![groupid isEqualToString:@""]) {
                //已经存在，替换掉之前的
                NSString * str = [NSString stringWithFormat:@"update %@ set receivetype= ? where sessionid = ?",TABLE_GROUP_SET];
                BOOL set = [db executeUpdate:str,setType,sessionId];
            }else{
                
                //插入数据
                NSString * insertStr = [NSString stringWithFormat:@"insert into  %@ values(?,?)",TABLE_GROUP_SET];
                BOOL set = [db executeUpdate:insertStr,setType,sessionId];
            }
        }
        [db close];
    }];
    
}


#pragma mark 获取群的消息设置
-(void)getGroupMessageSetType:(NSString*)sessionId success:(void(^)(id json))success
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        
        [db open];
        if ([db tableExists:TABLE_GROUP_SET]) {
            NSString * sqlString = [NSString stringWithFormat:@"select * from %@ where sessionid= ?",TABLE_GROUP_SET];
            
            FMResultSet * result = [db executeQuery:sqlString,sessionId];
            while ([result next]) {
                NSString * setType = [result stringForColumn:@"receivetype"];
                if (setType) {
                    success(setType);
                }
            }
        }
        
        [db close];
        
    }];
}


- (void)getMessagesCountForSessionID:(NSString*)sessionID completion:(MessageCountCompletion)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_MESSAGE])
        {
            [db setShouldCacheStatements:YES];
            
            NSString* sqlString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ where sessionId=?",TABLE_MESSAGE];
            
            FMResultSet* result = [db executeQuery:sqlString,sessionID];
            int count = 0;
            while ([result next])
            {
                count = [result intForColumnIndex:0];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(count);
            });
        }
        [db close];

    }];
}

-(void)insertMessages:(NSArray*)messages
               success:(void(^)())success
               failure:(void(^)(NSString* errorDescripe))failure
{
    
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
          BOOL hand =  [_database open];
        [db beginTransaction];
        __block BOOL isRollBack = NO;
        @try {
            [messages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                DDMessageEntity* message = (DDMessageEntity*)obj;
                NSString* sql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",TABLE_MESSAGE];

                NSData* infoJsonData = [NSJSONSerialization dataWithJSONObject:message.info options:NSJSONWritingPrettyPrinted error:nil];
                NSString* json = [[NSString alloc] initWithData:infoJsonData encoding:NSUTF8StringEncoding];
                BOOL result   ;
                if (hand) {
                    //如果消息为空，则不加进数据库
                    if (![message.msgContent isEqualToString:@""] && message.msgContent) {
                        result = [_database executeUpdate:sql,@(message.msgID),message.sessionId,message.senderId,message.toUserID,message.msgContent,@(message.state),@(message.msgTime),@(1),@(message.msgContentType),@(message.msgType),json,@(0),@""];
                    }
                }
                if (!result)
                {
                    isRollBack = YES;
                    *stop = YES;
                }
            }];
            
        }

        @catch (NSException *exception) {
            [_database rollback];
            
            failure(@"插入数据失败");
        }
        
        @finally {
            [_database close];

            if (isRollBack)
            {
                [db rollback];
                LOG(@"插入数据失败、、、、、、、、");
                failure(@"插入数据失败");
            }
            else
            {
                [_database commit];
                success();
            }
        }
    }];
}



- (void)deleteMesagesForSession:(NSString*)sessionID completion:(DeleteSessionCompletion)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString* sql = @"DELETE FROM message WHERE sessionId = ?";
        BOOL result = [_database executeUpdate:sql,sessionID];
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(result);
        });
        [db close];
    }];
}


#pragma mark 实例代码
/*  [_dataBaseQueue inDatabase:^(FMDatabase *db) {
BOOL res = [db open];

NSMutableArray* array = [[NSMutableArray alloc] init];
if ([db tableExists:TABLE_RECENT_SESSION])
{
    [db setShouldCacheStatements:YES];
    
    if (res) {
        
        NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ order BY updated DESC",TABLE_RECENT_SESSION];
        FMResultSet* result = [db executeQuery:sqlString];
        while ([result next])
        {
            SessionEntity* session = [self sessionFromResult:result];
            [array addObject:session];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        completion(array,nil);
    });
}
[db close];

}];
*/

#pragma mark 删除对应的会话题
-(void)deleteSessionEntity :(NSString *)sessionID//删除数据库对应的会话题;
{
    ;
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        
        BOOL rep = [db open];
        if ([_database tableExists:TABLE_RECENT_SESSION]) {
            
            if (rep) {
                
                NSString * sql = [NSString stringWithFormat:@"DELETE  FROM recentSession where ID = ?"];
                [ _database executeUpdate:sql,sessionID ];
            }
        }
        [db close];
        
    }];
}


#pragma mark 储存添加好友的消息
-(void)updateNewAddMessagesFromdic :(NSMutableDictionary *)dictionary//被添加的类型
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_ADD_FRIENDS]) {

            if ([db columnExists:@"addmessage" inTableWithName:TABLE_ADD_FRIENDS]) {
                
            }else{
                NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ text ",TABLE_ADD_FRIENDS,@"addmessage"];
                [db executeUpdate:sql];
            }
            if ([db columnExists:@"timeinterval" inTableWithName:TABLE_ADD_FRIENDS]) {


            }else
            {
                NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ text ",TABLE_ADD_FRIENDS,@"timeinterval"];
                [db executeUpdate:sql];
                
            }
            NSString * fromid = [dictionary objectForKey:@"fromid"];
            NSString * toid = [dictionary objectForKey:@"toid"];
            NSString * groupid = [dictionary objectForKey:@"groupid"];
            NSString * result = [dictionary objectForKey:@"result"];
            NSString * type = [dictionary objectForKey:@"type"];
            NSString * name = [dictionary objectForKey:@"name"];
            NSString * portrait = [dictionary objectForKey:@"portrait"];
            NSString * haschange = [dictionary objectForKey:@"haschange"];
            NSString * grouptype = [dictionary objectForKey:@"grouptype"];
            NSString * text =     [dictionary objectForKey:@"detailtext"];
            NSString * addMessage = [dictionary objectForKey:@"addmessage"];
            NSString * time = [dictionary objectForKey:@"time"];
            //先扫描数据库，如果已经加过就不在数据库保存
            NSString * querySql = [NSString stringWithFormat:@"select * from addfriend where  toid = ? and grouptype = ? and fromid=? and type = ?"] ;
            FMResultSet * ret =   [db executeQuery:querySql,toid,grouptype,fromid,type];

            NSString * sql = [NSString stringWithFormat:@"DELETE  from addfriend where fromid = ? and toid = ? and type = ? and groupid = ? and grouptype = ?"];
            
         BOOL set = [db executeUpdate:sql,[dictionary objectForKey:@"fromid"],[dictionary objectForKey:@"toid"],[dictionary objectForKey:@"type"],[dictionary objectForKey:@"groupid"],[dictionary objectForKey:@"grouptype"]];

            NSString * sqlUrl =  [NSString stringWithFormat:@"INSERT  into %@ VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",TABLE_ADD_FRIENDS] ;
    
            [db executeUpdate:sqlUrl,fromid,toid,groupid,result,type,name,portrait,haschange,grouptype,text,addMessage,time];
        }
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"NEWADDMESSAGE" object:nil];
        
        [db close];
    }];
    
}

#pragma mark 判断用户或者群已经邀请过自己
-(BOOL)determineMemerHasApplyGroup:(NSString *)userid  groupid:(NSString*)groupId applyType:(NSString*)applyType ;
{
    __block NSString * name = @"";
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        
        [db open];
        if ([db tableExists:TABLE_ADD_FRIENDS]) {
            NSString * sql = [NSString stringWithFormat:@"select * from %@ where fromid=? and groupid =? and type = ?",TABLE_ADD_FRIENDS];
            FMResultSet * set = [db executeQuery:sql,userid,groupId,applyType];
            while ([set next]) {
                 name = [set stringForColumn:@"name"];
     
            }
        }
        [db close];
    }];
    if (name && ![name isEqualToString:@""]) {
        return YES;
    }else{
        return NO;
    }
}


-(void)saveCurrentGroupName:(NSString *)storyName groupid:(NSString *)storyid creatorname:(NSString *)creatorname portrait :(NSString *)portrait createtime :(NSString *)creatortime creatorid:(NSString *)creatorid//存储临时群对应的话题或者剧的名字
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_CURRETGROUP_NAMES]) {
  
            NSString * sql = [NSString stringWithFormat:@"DELETE  from currentGroups where groupid = ? "];
            BOOL set = [db executeUpdate:sql,storyid];
            NSString * sqlUrl =  [NSString stringWithFormat:@"INSERT  into %@ VALUES(?,?,?,?,?,?)",TABLE_CURRETGROUP_NAMES] ;
            [db executeUpdate:sqlUrl,storyid,storyName,creatorname,portrait,creatortime,creatorid];
            
        }
        [db close];
        
    }];
}

-(void)getCurrentGroupName:(NSString *)storyid success:(void(^)(id json))success failure:(void(^)(id  json))failure
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        
        [db open];
        if ([db tableExists:TABLE_CURRETGROUP_NAMES]) {
            NSString * querySql = [NSString stringWithFormat:@"select * from currentGroups where  groupid = ? "] ;
            FMResultSet * set = [db executeQuery:querySql,storyid];
            NSDictionary * params;
            while ([set next]) {
                NSString * storyname = [set stringForColumn:@"name"];
                NSString * creatorname = [set stringForColumn:@"creatorname"];
                NSString * portrait = [set stringForColumn:@"portrait"];
                NSString * creatortime = [set stringForColumn:@"creatortime"];
                NSString * creatorid = [set stringForColumn:@"creatorid"];
                params = @{@"storyname":storyname,@"creatorname":creatorname,@"portrait":portrait,@"creatortime":creatortime,@"creatorid":creatorid};
            }
            if (params) {
                success(params);
            }else{
                failure(@"");
            }
        }
    }];
}

#pragma mark 好友删除以后对应的存储添加请求也删除
//[NSString stringWithFormat:@"DELETE FROM %@",TABLE_RECENT_CONTACTS];
-(void)clearUpAddMessages:(NSString *)fromId toid:(NSString*)toid groupid:(NSString*)groupid type:(NSString*)grouptype;//删除对应的添加信息
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_ADD_FRIENDS]) {
            NSString * deleteSql = [NSString stringWithFormat:@"DELETE from %@ where fromid = ? and toid = ? and groupid= ? and grouptype = ?",TABLE_ADD_FRIENDS];
            
            BOOL result = [db executeUpdate:deleteSql,fromId,toid,groupid,grouptype];
            if (result) {
            }
        }
        [db close];
    }];
    
}

#pragma mark 修改已经处理的添加信息的状态
-(void)deleteAddFriendsMessageFromid :(NSString*)fromid groupid:(NSString*)groupid friendsType:(NSString*)friendsType result:(NSString*)result grouptype:(NSString*)grouptype haschange:(NSString *)haschange//删除处理过的添加请求
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        
        BOOL rem = [db open];
        
        if ([db tableExists:TABLE_ADD_FRIENDS]) {
            
            if (rem) {
                NSString * sql = [NSString stringWithFormat:@"update  %@ set haschange = ? where fromid = ? and type = ? and groupid = ? and result = ?",TABLE_ADD_FRIENDS];
             [db executeUpdate:sql,haschange,fromid,friendsType,groupid,result];
            }
        }
        [db close];
    }];
    
}


-(void)getNotDealMessagescountsuccess:(void(^)(int totalCount))success;//获取未处理的添加信息的条数
{
    
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        int totalCount = 0;
        NSString * sql = [NSString stringWithFormat:@"select * from addfriend where grouptype = ?"];
        FMResultSet * result = [db executeQuery:sql,@"-2"];
        while ([result next]) {
            NSString * haschange = [result stringForColumn:@"haschange"];
            if ([haschange isEqualToString:@"0"]) {
                totalCount++;
            }
            
        }
        success(totalCount);
        
        [db close];
    }];
    
}


//  NSString* sql = [NSString stringWithFormat:@"UPDATE %@ set sessionId = ? , fromUserId = ? , toUserId = ? , content = ? , status = ? , msgTime = ? , sessionType = ? , messageType = ? ,messageContentType = ? , info = ? where messageID = ?",TABLE_MESSAGE];

#pragma mark 获取被添加的信息
-(void)getAddfriendstoid:(NSString*)toid  success:(void(^)(id JSON))success;//从数据库获取被添加的消息
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString * sql = [NSString stringWithFormat:@"select * from addfriend where  toid = ? "] ;
        FMResultSet * result =   [db executeQuery:sql,toid];
        NSMutableArray * arrary = [NSMutableArray array];
        while ([result next]) {
            NSMutableDictionary * dic = [NSMutableDictionary new];
            NSString * from_id = [result stringForColumn:@"fromid"];
            NSString * group_id = [result stringForColumn:@"groupid"];
            NSString * friend_type = [result stringForColumn:@"type"];
            NSString * result_value = [result stringForColumn:@"result"];
            NSString * name = [result stringForColumn:@"name"];
            NSString * portrait = [result stringForColumn:@"portrait"];
            NSString * grouptype = [result stringForColumn:@"grouptype"];
            NSString * haschange = [result stringForColumn:@"haschange"];
            NSString * text      = [result stringForColumn:@"detailtext"];
            NSString * addMessage = [result stringForColumn:@"addmessage"];
            NSString * time = [result stringForColumn:@"timeinterval"];
            if (addMessage && addMessage!=NULL && ![addMessage isEqual:nil]) {
                [dic setObject:addMessage forKey:@"addmessage"];
            }
            if (!grouptype || [grouptype isEqualToString:@""]) {
                grouptype = @"2";
            }
            if(!text || [text isEqualToString:@""]){
                text = @"邀请你进群";
            }
            if (!time || [time isEqualToString:@""]) {
                time = @"145520222";
            }
            [dic setObject:from_id forKey:@"fromid"];
            [dic setObject:group_id forKey:@"groupid"];
            [dic setObject:friend_type forKey:@"type"];
            [dic setObject:result_value forKey:@"result"];
            [dic setObject:name forKey:@"name"];
            [dic setObject:portrait forKey:@"portrait"];
            [dic setObject:grouptype forKey:@"grouptype"];
            [dic setObject:haschange forKey:@"haschange"];
            [dic setObject:text forKey:@"detailtext"];
            [dic setObject:time forKey:@"time"];
            [arrary addObject:dic];
        }
        [db close];
        success(arrary);
    }];
    
}

- (void)deleteMesages:(DDMessageEntity * )message completion:(DeleteSessionCompletion)completion
{
    ;
    
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString* sql = @"DELETE FROM message WHERE messageID = ?";
        BOOL result = [db executeUpdate:sql,@(message.msgID)];
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(result);
        });
        [db close];
    }];
}


-(void)deleteCurrentGroup:(NSString *)groupid
{;
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString * sql = [NSString stringWithFormat:@"DELETE  from currentGroups where groupid = ? "];
        BOOL set = [db executeUpdate:sql,groupid];

        [db close];
    }];

    
}

- (void)updateMessageForMessage:(DDMessageEntity*)message completion:(DDUpdateMessageCompletion)completion
{
    //(messageID integer,sessionId text,fromUserId text,toUserId text,content text, status integer, msgTime real, sessionType integer,messageType integer,reserve1 integer,reserve2 text)
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [_database open];
        NSString* sql = [NSString stringWithFormat:@"UPDATE %@ set sessionId = ? , fromUserId = ? , toUserId = ? , content = ? , status = ? , msgTime = ? , sessionType = ? , messageType = ? ,messageContentType = ? , info = ? where messageID = ?",TABLE_MESSAGE];
        
        NSData* infoJsonData = [NSJSONSerialization dataWithJSONObject:message.info options:NSJSONWritingPrettyPrinted error:nil];
        NSString* json = [[NSString alloc] initWithData:infoJsonData encoding:NSUTF8StringEncoding];
        BOOL result = [_database executeUpdate:sql,message.sessionId,message.senderId,message.toUserID,message.msgContent,@(message.state),@(message.msgTime),@(message.sessionType),@(message.msgType),@(message.msgContentType),json,@(message.msgID)];
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(result);
        });
        [_database close];
    }];
}

#pragma mark - Users
//- (void)loadContactsCompletion:(LoadRecentContactsComplection)completion
//{
//     [_dataBaseQueue inDatabase:^(FMDatabase *db) {
//        NSMutableArray* array = [[NSMutableArray alloc] init];
//        if ([_database tableExists:TABLE_RECENT_CONTACTS])
//        {
//            [_database setShouldCacheStatements:YES];
//
//            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@",TABLE_RECENT_CONTACTS];
//            FMResultSet* result = [_database executeQuery:sqlString];
//            while ([result next])
//            {
//                DDUserEntity* user = [self userFromResult:result];
//                [array addObject:user];
//            }
//            dispatch_async(dispatch_get_main_queue(), ^{
//                completion(array,nil);
//            });
//        }
//    }];
//}
//
//- (void)updateContacts:(NSArray*)users inDBCompletion:(UpdateRecentContactsComplection)completion
//{
//     [_dataBaseQueue inDatabase:^(FMDatabase *db) {
//        NSString* sql = [NSString stringWithFormat:@"DELETE FROM %@",TABLE_RECENT_CONTACTS];
//        BOOL result = [_database executeUpdate:sql];
//        if (result)
//        {
//            //删除原先数据成功，添加新数据
//            [_database beginTransaction];
//            __block BOOL isRollBack = NO;
//            @try {
//                [users enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                    DDUserEntity* user = (DDUserEntity*)obj;
//                    NSString* sql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ VALUES(?,?,?,?,?,?,?)",TABLE_RECENT_CONTACTS];
//                    //ID,Name,Nick,Avatar,Role,updated,reserve1,reserve2
//                    BOOL result = [_database executeUpdate:sql,user.objID,user.name,user.nick,user.avatar,@(user.lastUpdateTime),@(0),@""];
//                    if (!result)
//                    {
//                        isRollBack = YES;
//                        *stop = YES;
//                    }
//                }];
//            }
//            @catch (NSException *exception) {
//                [_database rollback];
//            }
//            @finally {
//                if (isRollBack)
//                {
//                    [_database rollback];
//                    NSLog(@"insert to database failure content");
//                    NSError* error = [NSError errorWithDomain:@"插入最近联系人用户失败" code:0 userInfo:nil];
//                    completion(error);
//                }
//                else
//                {
//                    [_database commit];
//                    completion(nil);
//                }
//            }
//        }
//        else
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                NSError* error = [NSError errorWithDomain:@"清除数据失败" code:0 userInfo:nil];
//                completion(error);
//            });
//        }
//
//    }];
//}
//
//- (void)updateContact:(DDUserEntity*)user inDBCompletion:(UpdateRecentContactsComplection)completion
//{
//     [_dataBaseQueue inDatabase:^(FMDatabase *db) {
//
//        //#define SQL_CREATE_RECENT_CONTACTS      [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID text,Name text,Nick text,Avatar text, Role integer, updated real,reserve1 integer,reserve2 text)",TABLE_RECENT_CONTACTS]
//
//        NSString* sql = [NSString stringWithFormat:@"UPDATE %@ set Name = ? , Nick = ? , Avatar = ? ,  updated = ? , reserve1 = ? , reserve2 = ?where ID = ?",TABLE_RECENT_CONTACTS];
//
//        BOOL result = [_database executeUpdate:sql,user.name,user.nick,user.avatar,@(user.lastUpdateTime),@(1),@(1),user.objID];
//        if (result)
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                completion(nil);
//            });
//        }
//        else
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                NSError* error = [NSError errorWithDomain:@"更新数据失败" code:0 userInfo:nil];
//                completion(error);
//            });
//        }
//
//    }];
//}
//
//- (void)insertUsers:(NSArray*)users completion:(InsertsRecentContactsCOmplection)completion
//{
//    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
//        [_database beginTransaction];
//        __block BOOL isRollBack = NO;
//        @try {
//            [users enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                DDUserEntity* user = (DDUserEntity*)obj;
//                NSString* sql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ VALUES(?,?,?,?,?,?,?)",TABLE_RECENT_CONTACTS];
//                //ID,Name,Nick,Avatar,Role,updated,reserve1,reserve2
//                BOOL result = [_database executeUpdate:sql,user.objID,user.name,user.nick,user.avatar,@(user.lastUpdateTime),@(0),@""];
//                if (!result)
//                {
//                    isRollBack = YES;
//                    *stop = YES;
//                }
//            }];
//        }
//        @catch (NSException *exception) {
//            [_database rollback];
//        }
//        @finally {
//            if (isRollBack)
//            {
//                [_database rollback];
//                NSLog(@"insert to database failure content");
//                NSError* error = [NSError errorWithDomain:@"插入最近联系人用户失败" code:0 userInfo:nil];
//                completion(error);
//            }
//            else
//            {
//                [_database commit];
//                completion(nil);
//            }
//        }
//    }];
//}
- (void)insertDepartments:(NSArray*)departments completion:(InsertsRecentContactsCOmplection)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        [db beginTransaction];
        __block BOOL isRollBack = NO;
        @try {
            [departments enumerateObjectsUsingBlock:^(DepartInfo *obj, NSUInteger idx, BOOL *stop) {
              
                NSString* sql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ VALUES(?,?,?,?,?)",TABLE_DEPARTMENTS];
                //ID,Name,Nick,Avatar,Role,updated,reserve1,reserve2
                BOOL result = [_database executeUpdate:sql,@(obj.deptId),@(obj.parentDeptId),obj.deptName,@(obj.deptStatus),@(obj.priority)];
                if (!result)
                {
                    isRollBack = YES;
                    *stop = YES;
                }
            }];
        }
        @catch (NSException *exception) {
            [db rollback];
        }
        @finally {
            if (isRollBack)
            {
                [_database rollback];
//                NSLog(@"insert to database failure content");
                NSError* error = [NSError errorWithDomain:@"批量插入部门信息失败" code:0 userInfo:nil];
                completion(error);
            }
            else
            {
                [db commit];
                completion(nil);
            }
        }
        [db close];
    }];
}
- (void)getDepartmentFromID:(NSString*)departmentID completion:(void(^)(DepartInfo *department))completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
       [db open];
        if ([db tableExists:TABLE_DEPARTMENTS])
        {
            [db setShouldCacheStatements:YES];
            
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ where ID=?",TABLE_DEPARTMENTS];
            
            FMResultSet* result = [db executeQuery:sqlString,departmentID];
            DepartInfo* department = nil;
            while ([result next])
            {
                department = [self departmentFromResult:result];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(department);
            });
        }
        [db close];
    }];
}

-(void)insertUser:(DDUserEntity *)user
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        [db beginTransaction];
        __block BOOL isRollBack = NO;
        @try {
            user.department = @"";
            user.position = @" ";
            NSString * sql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ values(?,?,?,?)",TABLE_ALL_CONTACTS];
            BOOL result = [db executeUpdate:sql,user.objID,user.nick,user.name,user.avatar];
            if (!result)
            {
                isRollBack = YES;
            }
        }
        @catch (NSException *exception) {
            [db rollback];

        }
        @finally {
            if (isRollBack)
            {
                [db rollback];
//                NSLog(@"insert to database failure content");
                NSError* error = [NSError errorWithDomain:@"批量插入全部用户信息失败" code:0 userInfo:nil];
            }
            else
            {
                [db commit];
            }
        }
        [db close];
    }];
}

- (void)insertAllUser:(NSArray*)users completion:(InsertsRecentContactsCOmplection)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        [db beginTransaction];
        __block BOOL isRollBack = NO;
        @try {
            
            [users enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                DDUserEntity* user = (DDUserEntity *)obj;
                user.department=@" ";
                user.position=@" ";
                    NSString* sql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",TABLE_ALL_CONTACTS];
                    //ID,Name,Nick,Avatar,Role,updated,reserve1,reserve2
                    BOOL result = [db executeUpdate:sql,user.objID,user.name,user.nick,user.avatar,user.department,@(user.departId),user.email,user.position,user.telphone,@(user.sex),user.lastUpdateTime,user.pyname];
                    
                    if (!result)
                    {
                        isRollBack = YES;
                        *stop = YES;
                    }
            }];
            
        }
        @catch (NSException *exception) {
            [db rollback];
        }
        @finally {
            if (isRollBack)
            {
                [db rollback];
//                NSLog(@"insert to database failure content");
                NSError* error = [NSError errorWithDomain:@"批量插入全部用户信息失败" code:0 userInfo:nil];
                completion(error);
            }
            else
            {
                [db commit];
                completion(nil);
            }
        }
        [db close];
    }];
}

- (void)getAllUsers:(LoadAllContactsComplection )completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_ALL_CONTACTS])
        {
            [db setShouldCacheStatements:YES];
            NSMutableArray* array = [[NSMutableArray alloc] init];
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ ",TABLE_ALL_CONTACTS];
            FMResultSet* result = [db executeQuery:sqlString];
            DDUserEntity* user = nil;
            while ([result next])
            {
                user = [self userFromResult:result];
                if (user.userStatus != 3) {
                    [array addObject:user];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(array,nil);
            });
        }
        [db close];
    }];
}

- (void)getUserFromID:(NSString*)userID completion:(void(^)(DDUserEntity *user))completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_ALL_CONTACTS])
        {
            [db setShouldCacheStatements:YES];
            
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ where ID= ?",TABLE_ALL_CONTACTS];
            FMResultSet* result = [db executeQuery:sqlString,userID];
            DDUserEntity* user = nil;
            while ([result next])
            {
                user = [self userFromResult:result];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(user);
            });
        }
        [db close];
    }];
}
- (void)loadGroupByIDCompletion:(NSString *)groupID Block:(LoadRecentContactsComplection)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSMutableArray* array = [[NSMutableArray alloc] init];
        if ([db tableExists:TABLE_GROUPS])
        {
            [db setShouldCacheStatements:YES];
            
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ where ID= ? ",TABLE_GROUPS];
            FMResultSet* result = [db executeQuery:sqlString,groupID];
            while ([result next])
            {
                GroupEntity* group = [self groupFromResult:result];
                [array addObject:group];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(array,nil);
            });
        }
        [db close];
    }];
}

- (void)loadGroupsCompletion:(LoadRecentContactsComplection)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSMutableArray* array = [[NSMutableArray alloc] init];
        if ([db tableExists:TABLE_GROUPS])
        {
            [db setShouldCacheStatements:YES];
            
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@",TABLE_GROUPS];
            FMResultSet* result = [db executeQuery:sqlString];
            while ([result next])
            {
                GroupEntity* group = [self groupFromResult:result];
                [array addObject:group];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(array,nil);
            });
        }
        [db close];
    }];
}

- (void)updateRecentGroup:(GroupEntity *)group completion:(InsertsRecentContactsCOmplection)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        [db beginTransaction];
        __block BOOL isRollBack = NO;
        @try {
            NSString* sql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ VALUES(?,?,?,?,?,?,?,?,?,?)",TABLE_GROUPS];
            NSString *users = @"";
            if ([group.groupUserIds count]>0) {
                users=[group.groupUserIds componentsJoinedByString:@"-"];
            }
            BOOL result = [db executeUpdate:sql,group.objID,group.avatar,@(group.groupType),group.name,group.groupCreatorId,users,group.lastMsg,@(group.lastUpdateTime),@(group.isShield),@(group.objectVersion)];
            if (!result)
            {
                isRollBack = YES;
            }
            
        }
        @catch (NSException *exception) {
            [db rollback];
        }
        @finally {
            if (isRollBack)
            {
                [db rollback];
//                NSLog(@"insert to database failure content");
                NSError* error = [NSError errorWithDomain:@"插入最近群失败" code:0 userInfo:nil];
                completion(error);
            }
            else
            {
                [db commit];
                completion(nil);
            }
        }
        [db close];
    }];
}

- (void)updateRecentSession:(SessionEntity *)session completion:(InsertsRecentContactsCOmplection)completion
{
    /*
     ID text UNIQUE,Avatar text, Type integer, Name text,LastMessage Text,updated real,isshield intege  Users Text
     */
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        [db beginTransaction];
        __block BOOL isRollBack = NO;
        @try {
            NSString* sql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ VALUES(?,?,?,?,?,?,?,?,?,?)",TABLE_RECENT_SESSION];
            
//            NSLog(@"session.name:%@ session.users :%@",session.name,session.sessionUsers);
            //ID Avatar GroupType Name CreatID Users  LastMessage
            NSString *users = @"";
            if ([session.sessionUsers count]>0) {
                users=[session.sessionUsers componentsJoinedByString:@"-"];
            }
            BOOL result = [db executeUpdate:sql,session.sessionID,session.avatar,@(session.sessionType),session.name,@(session.timeInterval),@(session.isShield),users,@(session.unReadMsgCount),session.lastMsg,@(session.lastMsgID)];
            if (!result)
            {
                isRollBack = YES;
            }
            
        }
        @catch (NSException *exception) {
            [db rollback];
        }
        @finally {
            if (isRollBack)
            {
                [db rollback];
//                NSLog(@"insert to database failure content");
                NSError* error = [NSError errorWithDomain:@"插入最近Session失败" code:0 userInfo:nil];
                completion(error);
            }
            else
            {
                [db commit];
                completion(nil);
            }
        }
        [db close];
    }];
}

#pragma session
- (void)loadSessionsCompletion:(LoadAllSessionsComplection)completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        BOOL res = [db open];

        NSMutableArray* array = [[NSMutableArray alloc] init];
        if ([db tableExists:TABLE_RECENT_SESSION])
        {
            [db setShouldCacheStatements:YES];
        
            if (res) {
                
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ order BY updated DESC",TABLE_RECENT_SESSION];
            FMResultSet* result = [db executeQuery:sqlString];
            while ([result next])
            {
                SessionEntity* session = [self sessionFromResult:result];
                [array addObject:session];
            }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(array,nil);
            });
        }
        [db close];

    }];
}

-(SessionEntity *)sessionFromResult:(FMResultSet *)resultSet
{
    /*
     ID text UNIQUE,Avatar text, Type integer, Name text,updated real,isshield integer,Users Text
     */
    SessionType type =(SessionType)[resultSet intForColumn:@"type"];
    SessionEntity* session = [[SessionEntity alloc] initWithSessionID:[resultSet stringForColumn:@"ID"] SessionName:[resultSet stringForColumn:@"name"] type:type];
    session.avatar=[resultSet stringForColumn:@"avatar"];
    session.timeInterval=[resultSet longForColumn:@"updated"];
//    NSLog(@"构造session的时间戳:%ld",[resultSet longForColumn:@"updated"]);
    session.lastMsg = [resultSet stringForColumn:@"lasMsg"];
    session.lastMsgID = [resultSet longForColumn:@"lastMsgId"];
    session.unReadMsgCount = [resultSet longForColumn:@"unreadCount"];
    return session;
}


-(void)removeSession:(NSString *)sessionID
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString* sql = @"DELETE FROM recentSession WHERE ID = ?";
        BOOL result = [db executeUpdate:sql,sessionID];
        if(result)
        {
            NSString* sql = @"DELETE FROM message WHERE sessionId = ?";
            BOOL set = [_database executeUpdate:sql,sessionID];
        }        [db close];
    }];
    
}



-(void)getSessionTimeInterval:(NSString*)sessionId success:(void(^)(NSInteger timeInterval))success//通过sessionId获取会话体的时间戳
{
    [_database close];
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        [db beginTransaction];

        NSString * sql = [NSString stringWithFormat:@"select * from recentSession where ID = ?"];
        FMResultSet * result = [db executeQuery:sql,sessionId];
        while ([result next]) {
            NSInteger time = [result longForColumn:@"updated"];
            success(time);
        }
        
        [db close];
    }];
    
}


- (void)getAllDeprt:(LoadAllContactsComplection )completion
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_DEPARTMENTS])
        {
            [db setShouldCacheStatements:YES];
            NSMutableArray* array = [[NSMutableArray alloc] init];
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ ",TABLE_DEPARTMENTS];
            FMResultSet* result = [db executeQuery:sqlString];
            DepartInfo* department = nil;
            while ([result next])
            {
                department = [self departmentFromResult:result];
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(array,nil);
            });
        }
        [db close];
    }];
}

-(void)getDepartmentTitleById:(NSInteger )departmentid Block:(void(^)(NSString *title))block
{
    [_dataBaseQueue inDatabase:^(FMDatabase *db) {
        [db open];
        if ([db tableExists:TABLE_DEPARTMENTS])
        {
            [db setShouldCacheStatements:YES];
            NSString* sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ where ID = ?",TABLE_DEPARTMENTS];
            FMResultSet* result = [db executeQuery:sqlString,@(departmentid)];
            DepartInfo* department = nil;
            while ([result next])
            {
                department = [self departmentFromResult:result];
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                block(department.deptName);
            });
        }
        [db close];
    }];
}
@end
