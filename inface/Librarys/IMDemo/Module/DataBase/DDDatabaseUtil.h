//
//  DDDatabaseUtil.h
//  Duoduo
//
//  Created by murray on 14-3-21.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"
@class DDepartment;
@class DDMessageEntity;
@class GroupEntity;
@class SessionEntity;
@class MessageEntity,DDUserEntity;

@interface DDDatabaseUtil : NSObject
@property(strong)NSString *recentsession;
//在数据库上的操作
@property (nonatomic,readonly)dispatch_queue_t databaseMessageQueue;


+ (instancetype)instance;

- (void)openCurrentUserDB;

@end

typedef void(^LoadMessageInSessionCompletion)(NSArray* messages,NSError* error);
typedef void(^MessageCountCompletion)(NSInteger count);
typedef void(^DeleteSessionCompletion)(BOOL success);
typedef void(^DDDBGetLastestMessageCompletion)(DDMessageEntity* message,NSError* error);
typedef void(^DDUpdateMessageCompletion)(BOOL result);
typedef void(^DDGetLastestCommodityMessageCompletion)(DDMessageEntity* message);

@interface DDDatabaseUtil(Message)

/**
 *  在|databaseMessageQueue|执行查询操作，分页获取聊天记录
 *
 *  @param sessionID  会话ID
 *  @param pagecount  每页消息数
 *  @param page       页数
 *  @param completion 完成获取
 */
- (void)loadMessageForSessionID:(NSString*)sessionID pageCount:(int)pagecount index:(NSInteger)index completion:(LoadMessageInSessionCompletion)completion;

- (void)loadMessageForSessionID:(NSString*)sessionID afterMessage:(DDMessageEntity*)message completion:(LoadMessageInSessionCompletion)completion;

/**
 *  获取对应的Session的最新的自己发送的商品气泡
 *
 *  @param sessionID  会话ID
 *  @param completion 完成获取
 */
- (void)getLasetCommodityTypeImageForSession:(NSString*)sessionID completion:(DDGetLastestCommodityMessageCompletion)completion;

/**
 *  在|databaseMessageQueue|执行查询操作，获取DB中
 *
 *  @param sessionID  sessionID
 *  @param completion 完成获取最新的消息
 */
- (void)getLastestMessageForSessionID:(NSString*)sessionID completion:(DDDBGetLastestMessageCompletion)completion;

/**
 *  在|databaseMessageQueue|执行查询操作，分页获取聊天记录
 *
 *  @param sessionID  会话ID
 *  @param completion 完成block
 */
- (void)getMessagesCountForSessionID:(NSString*)sessionID completion:(MessageCountCompletion)completion;

/**
 *  批量插入message，需要用户必须在线，避免插入离线时阅读的消息
 *
 *  @param messages message集合
 *  @param success 插入成功
 *  @param failure 插入失败
 */
- (void)insertMessages:(NSArray*)messages
               success:(void(^)())success
               failure:(void(^)(NSString* errorDescripe))failure;

/**
 *  删除相应会话的所有消息
 *
 *  @param sessionID  会话
 *  @param completion 完成删除
 */
- (void)deleteMesagesForSession:(NSString*)sessionID completion:(DeleteSessionCompletion)completion;

/**
 *  更新数据库中的某条消息
 *
 *  @param message    更新后的消息
 *  @param completion 完成更新
 */
- (void)updateMessageForMessage:(DDMessageEntity*)message completion:(DDUpdateMessageCompletion)completion;
@end

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

typedef void(^LoadRecentContactsComplection)(NSArray* contacts,NSError* error);
typedef void(^LoadAllContactsComplection)(NSArray* contacts,NSError* error);
typedef void(^LoadAllSessionsComplection)(NSArray* session,NSError* error);
typedef void(^UpdateRecentContactsComplection)(NSError* error);
typedef void(^InsertsRecentContactsCOmplection)(NSError* error);

@interface DDDatabaseUtil(Users)

/**
 *  加载本地数据库的最近联系人列表
 *
 *  @param completion 完成加载
 */
- (void)loadContactsCompletion:(LoadRecentContactsComplection)completion;

/**
 *  更新本地数据库的最近联系人信息
 *
 *  @param completion 完成更新本地数据库
 */
- (void)updateContacts:(NSArray*)users inDBCompletion:(UpdateRecentContactsComplection)completion;

/**
 *  更新本地数据库某个用户的信息
 *
 *  @param user       某个用户
 *  @param completion 完成更新本地数据库
 */
/**
 *  插入本地数据库的最近联系人信息
 *
 *  @param users      最近联系人数组
 *  @param completion 完成插入
 */
- (void)insertUsers:(NSArray*)users completion:(InsertsRecentContactsCOmplection)completion;
-(void)insertUser:(DDUserEntity *)user;//插入用户

/**
 *  插入组织架构信息
 *
 *  @param departments 组织架构数组
 *  @param completion  完成插入
 */
- (void)insertDepartments:(NSArray*)departments completion:(InsertsRecentContactsCOmplection)completion;
-(void)deleteSessionEntity :(NSString *)sessionID ;//删除数据库对应的会话题;
- (void)getDepartmentFromID:(NSString*)departmentID completion:(void(^)(DDepartment *department))completion;
- (void)insertAllUser:(NSArray*)users completion:(InsertsRecentContactsCOmplection)completion;

- (void)getAllUsers:(LoadAllContactsComplection )completion;

- (void)getUserFromID:(NSString*)userID completion:(void(^)(DDUserEntity *user))completion;

- (void)updateRecentGroup:(GroupEntity *)group completion:(InsertsRecentContactsCOmplection)completion;
- (void)updateRecentSession:(SessionEntity *)session completion:(InsertsRecentContactsCOmplection)completion;
- (void)loadGroupsCompletion:(LoadRecentContactsComplection)completion;
- (void)loadSessionsCompletion:(LoadAllSessionsComplection)completion;
-(void)removeSession:(NSString *)sessionID;
- (void)deleteMesages:(DDMessageEntity * )message completion:(DeleteSessionCompletion)completion;
- (void)loadGroupByIDCompletion:(NSString *)groupID Block:(LoadRecentContactsComplection)completion;
- (void)getAllDeprt:(LoadAllContactsComplection )completion;
-(void)getDepartmentTitleById:(NSInteger )departmentid Block:(void(^)(NSString *title))block;
-(void)updateNewAddMessagesFromdic :(NSMutableDictionary *)dictionary;//被添加的类型
-(void)deleteAddFriendsMessageFromid :(NSString*)fromid groupid:(NSString*)groupid friendsType:(NSString*)friendsType result:(NSString*)result grouptype:(NSString*)grouptype haschange:(NSString *)haschange;//删除处理过的添加请求

-(void)getAddfriendstoid:(NSString*)toid  success:(void(^)(id JSON))success;//从数据库获取被添加的消息
-(void)clearUpAddMessages:(NSString *)fromId toid:(NSString*)toid groupid:(NSString*)groupid type:(NSString*)grouptype;//删除对应的添加信息
-(void)getNotDealMessagescountsuccess:(void(^)(int totalCount))success;//获取未处理的添加信息的条数

-(void)saveCurrentGroupName:(NSString *)storyName groupid:(NSString *)storyid creatorname:(NSString *)creatorname portrait :(NSString *)portrait createtime :(NSString *)creatortime creatorid :(NSString *)creatorid;//存储临时群对应的话题或者剧的名字
//获取临时群的名字和创建者神马的
-(void)getCurrentGroupName:(NSString *)storyid success:(void(^)(id json))success failure:(void(^)(id  json))failure;


-(void)deleteCurrentGroup:(NSString *)groupid;

-(void)insertDraftMessage:(NSString *)message sessionId:(NSString*)sessionId ;//存储草稿

-(void)getDraftMessage:(NSString*)sessionId  success:(void(^)(id  json))success;//获取草稿消息

-(void)insertEditMessage:(NSString *)message sessionId:(NSString*)sessionId ;//存储草稿

-(void)getEditMessage:(NSString*)sessionId  success:(void (^)(id))success;//获取草稿消息

-(void)saveGroupReceiveMessage:(NSString *)sessionId setType:(NSString*)setType;//保存群消息设置的状态

-(void)getGroupMessageSetType:(NSString*)sessionId success:(void(^)(id json))success;


-(void)saveGroupNotify:(NSString *)title content:(NSString*)content  groupid:(NSString *)groupId  dateTime:(NSString*)dateTime;

-(void)getGroupNotify:(NSString*)groupId success:(void(^)(NSString * title,NSString * content,NSString * dateTime))success;

-(void)getSessionTimeInterval:(NSString*)sessionId success:(void(^)(NSInteger timeInterval))success;//通过sessionId获取会话体的时间戳


-(BOOL)determineMemerHasApplyGroup:(NSString *)userid  groupid:(NSString*)groupId applyType:(NSString*)applyType ;//判断用户或者群已经邀请过自己
@end