
//
//  DDMessageSendManager.m
//  Duoduo
//
//  Created by murray on 14-3-30.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDMessageSendManager.h"
#import "DDUserModule.h"
#import "DDMessageEntity.h"
#import "DDMessageModule.h"
#import "DDTcpClientManager.h"
#import "SendMessageAPI.h"
#import "RuntimeStatus.h"
#import "UnAckMessageManager.h"
#import "DDGroupModule.h"
#import "DDClientState.h"
#import "DDDatabaseUtil.h"
#import "security.h"
static uint32_t seqNo = 0;

@interface DDMessageSendManager(PrivateAPI)


@end

@implementation DDMessageSendManager
{
    NSUInteger _uploadImageCount;
}
+ (instancetype)instance
{
    static DDMessageSendManager* g_messageSendManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_messageSendManager = [[DDMessageSendManager alloc] init];
    });
    return g_messageSendManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _uploadImageCount = 0;
        _waitToSendMessage = [[NSMutableArray alloc] init];
        _sendMessageSendQueue = dispatch_queue_create("com.murray.Duoduo.sendMessageSend", NULL);
        
    }
    return self;
}

- (void)sendMessage:(DDMessageEntity *)message isGroup:(BOOL)isGroup Session:(SessionEntity*)session completion:(DDSendMessageCompletion)completion Error:(void (^)(NSError *))block
{
    
    dispatch_async(self.sendMessageSendQueue, ^{
        SendMessageAPI* sendMessageAPI = [[SendMessageAPI alloc] init];
        uint32_t nowSeqNo = ++seqNo;
        message.seqNo=nowSeqNo;
   
        NSString* newContent = message.msgContent;
        if ([message isImageMessage]) {
            NSDictionary* dic = [NSDictionary initWithJsonString:message.msgContent];
            NSString* urlPath = dic[DD_IMAGE_URL_KEY];
            newContent=urlPath;
        }
        char* pOut;
        uint32_t nOutLen;
        const char *test =[newContent cStringUsingEncoding:NSUTF8StringEncoding];
        uint32_t nInLen  = strlen(test);
        EncryptMsg(test, nInLen, &pOut, nOutLen);
        NSData *data = [[NSString stringWithCString:pOut encoding:NSUTF8StringEncoding] dataUsingEncoding:NSUTF8StringEncoding];
        Free(pOut);
        if (!session.sessionID) {
            return ;
        }
        NSArray* object = @[[RuntimeStatus instance].user.objID,session.sessionID,data,@(message.msgType),@(message.msgID)];
//        NSArray* object = @[[RuntimeStatus instance].user.objID,@"group_592",data,@(17),@(message.msgID)];

        if ([message isImageMessage]) {
            session.lastMsg=@"[图片]";
        }else if ([message isVoiceMessage])
        {
             session.lastMsg=@"[语言]";
        }else
        {
             session.lastMsg=message.msgContent;
        }
        [[UnAckMessageManager instance] addMessageToUnAckQueue:message];
        [sendMessageAPI requestWithObject:object Completion:^(id response, NSError *error) {
            if (!error)
            {
                [[UnAckMessageManager instance] removeMessageFromUnAckQueue:message];

#pragma mark 暂时不用删除
                    [[DDDatabaseUtil instance] deleteMesages:message completion:^(BOOL success){
                       
                    }];
                
                    [[UnAckMessageManager instance] removeMessageFromUnAckQueue:message];
//                    NSUInteger messageTime = [[NSDate date] timeIntervalSince1970];
//                    message.msgTime=messageTime;
                    message.msgID=[response[0] integerValue];
                    message.state=DDmessageSendSuccess;
                    session.lastMsgID=message.msgID;
                    session.timeInterval=message.msgTime;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"SentMessageSuccessfull" object:session];
                     [[DDDatabaseUtil instance] insertMessages:@[message] success:^{
                         

                     } failure:^(NSString *errorDescripe) {
                         
                     }];
                    completion(message,nil);
            }
            else
            {

                message.state=DDMessageSendFailure;
                [[DDDatabaseUtil instance] insertMessages:@[message] success:^{
                    
                } failure:^(NSString *errorDescripe) {
                    
                }];
                NSError* error = [NSError errorWithDomain:@"发送消息失败" code:0 userInfo:nil];
                block(error);
            }
        }];
        
    });
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10000 && buttonIndex == 0) {
        [[DDTcpClientManager instance]reconnect];

    }
}

-(void)autoDismissAlert:(NSTimer *)timer
{
    UIAlertView * alert = (UIAlertView*)timer.userInfo;
    [UIView animateWithDuration:0.4 animations:^{
        
        [alert dismissWithClickedButtonIndex:0 animated:YES];
        
    } completion:^(BOOL finished) {
        
        
    }];
    
}

- (void)sendVoiceMessage:(NSData*)voice filePath:(NSString*)filePath forSessionID:(NSString*)sessionID isGroup:(BOOL)isGroup Message:(DDMessageEntity *)msg Session:(SessionEntity*)session completion:(DDSendMessageCompletion)completion
{
    dispatch_async(self.sendMessageSendQueue, ^{
        SendMessageAPI* sendVoiceMessageAPI = [[SendMessageAPI alloc] init];

        NSString* myUserID = [RuntimeStatus instance].user.objID;
        NSArray* object = @[myUserID,sessionID,voice,@(msg.msgType),@(0)];
        [sendVoiceMessageAPI requestWithObject:object Completion:^(id response, NSError *error) {
            if (!error)
            {
              
                
//                NSLog(@"发送消息成功");
//                [[DDDatabaseUtil instance] deleteMesages:msg completion:^(BOOL success){
//                    
//                }];
                
              
                NSUInteger messageTime = [[NSDate date] timeIntervalSince1970];
                msg.msgTime=messageTime;
                msg.msgID=[response[0] integerValue];
                msg.state=DDmessageSendSuccess;
                session.lastMsg=@"[语音]";
                session.lastMsgID=msg.msgID;
                session.timeInterval=msg.msgTime;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SentMessageSuccessfull" object:session];
                [[DDDatabaseUtil instance] insertMessages:@[msg] success:^{
                    
                } failure:^(NSString *errorDescripe) {
                    
                }];

                    completion(msg,nil);
                
            }
            else
            {
                NSError* error = [NSError errorWithDomain:@"发送消息失败" code:0 userInfo:nil];
                completion(nil,error);
            }
        }];

    });
}



@end
