//
//  DDUserModule.h
//  IOSDuoduo
//
//  Created by murray on 14-5-26.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDUserEntity.h"

typedef void(^DDLoadRecentUsersCompletion)();


@interface DDUserModule : NSObject

@property (nonatomic,strong)NSString* currentUserID;
@property (nonatomic,strong)NSMutableDictionary* recentUsers;
+ (instancetype)shareInstance;
- (void)addMaintanceUser:(DDUserEntity*)user;
- (void )getUserForUserID:(NSString*)userID Block:(void(^)(DDUserEntity *user))block;
- (void)addRecentUser:(DDUserEntity*)user;
- (void)loadAllRecentUsers:(DDLoadRecentUsersCompletion)completion;
-(void)clearRecentUser;
-(NSArray *)getAllMaintanceUser;
-(void)changeUseerName:(NSString*)nickname ;//修改自己的昵称
-(void)changeOtherPersonInfo:(NSString *)sessionId userEntiry:(DDUserEntity *)user;
@end
