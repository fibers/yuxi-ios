//
//  DDUserModule.m
//  IOSDuoduo
//
//  Created by murray on 14-5-26.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDUserModule.h"
#import "DDDatabaseUtil.h"
@interface DDUserModule(PrivateAPI)

- (void)n_receiveUserLogoutNotification:(NSNotification*)notification;
- (void)n_receiveUserLoginNotification:(NSNotification*)notification;
@end

@implementation DDUserModule
{
    NSMutableDictionary* _allUsers;
}

+ (instancetype)shareInstance
{
    static DDUserModule* g_userModule;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_userModule = [[DDUserModule alloc] init];
    });
    return g_userModule;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _allUsers = [[NSMutableDictionary alloc] init];
        _recentUsers = [[NSMutableDictionary alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(n_receiveUserLoginNotification:) name:DDNotificationUserLoginSuccess object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(n_receiveUserLoginNotification:) name:DDNotificationUserReloginSuccess object:nil];
        

    }
    return self;
}


- (void)addMaintanceUser:(DDUserEntity*)user
{
    
    if (!user)
    {
        return;
    }
    if (!_allUsers)
    {
        _allUsers = [[NSMutableDictionary alloc] init];
    }
    [_allUsers setValue:user forKey:user.objID];
    
}

-(NSArray *)getAllMaintanceUser
{
    return [_allUsers allValues];
}


-(void)changeUseerName :(NSString*)nickname//修改自己的昵称
{
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];
    DDUserEntity * user = [_allUsers objectForKey:userid];
    if (user) {
        user.nick = nickname;
        [_allUsers setValue:user forKey:userid];

    }else{
        DDUserEntity * userEntity = [[DDUserEntity alloc]initWithUserID:userid name:nickname nick:nickname avatar:@"" userRole:100 userUpdated:100];
        [_allUsers setObject:userEntity forKey:userid];
    }
    
}


-(void)changeOtherPersonInfo:(NSString *)sessionId userEntiry:(DDUserEntity *)user
{
    if (user) {
        [_allUsers setObject:user forKey:sessionId];
    }
}

- (void )getUserForUserID:(NSString*)userID Block:(void(^)(DDUserEntity *user))block
{
    return block(_allUsers[userID]);

}

- (void)addRecentUser:(DDUserEntity*)user
{
    if (!user)
    {
        return;
    }
    if (!self.recentUsers)
    {
        self.recentUsers = [[NSMutableDictionary alloc] init];
    }
    NSArray* allKeys = [self.recentUsers allKeys];
    if (![allKeys containsObject:user.objID])
    {
        [self.recentUsers setValue:user forKey:user.objID];
        [[DDDatabaseUtil instance] insertUsers:@[user] completion:^(NSError *error) {
            
        }];
    }
   
}


- (void)loadAllRecentUsers:(DDLoadRecentUsersCompletion)completion
{
    
    //加载本地最近联系人
}

#pragma mark - 
#pragma mark PrivateAPI
- (void)n_receiveUserLogoutNotification:(NSNotification*)notification
{
    //用户登出
    _recentUsers = nil;
}

- (void)n_receiveUserLoginNotification:(NSNotification*)notification
{
    if (!_recentUsers)
    {
        _recentUsers = [[NSMutableDictionary alloc] init];
        [self loadAllRecentUsers:^{
            [DDNotificationHelp postNotification:DDNotificationRecentContactsUpdate userInfo:nil object:nil];
        }];
    }
}

-(void)clearRecentUser
{
    DDUserModule* userModule = [DDUserModule shareInstance];
    [[userModule recentUsers] removeAllObjects];
}

@end
