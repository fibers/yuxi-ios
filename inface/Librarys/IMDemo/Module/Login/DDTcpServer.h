//
//  DDTcpServer.h
//  Duoduo
//
//  Created by murray on 14-4-5.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^ClientSuccess)();
@interface DDTcpServer : NSObject
{
    
}
- (void)loginTcpServerIP:(NSString*)ip port:(NSInteger)point Success:(void(^)())success failure:(void(^)())failure;
@end
