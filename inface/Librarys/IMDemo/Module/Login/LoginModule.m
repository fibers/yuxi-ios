//
//  DDLoginManager.m
//  Duoduo
//
//  Created by murray on 14-4-5.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "LoginModule.h"
#import "DDHttpServer.h"
#import "DDMsgServer.h"
#import "DDTcpServer.h"
#import "DDUserModule.h"
#import "DDUserEntity.h"
#import "DDClientState.h"
#import "RuntimeStatus.h"
#import "ContactsModule.h"
#import "DDDatabaseUtil.h"
#import "DDAllUserAPI.h"
#import "LoginAPI.h"
#import "FriendGroupModel.h"
#import "FriendModel.h"
#import "GroupModel.h"
@interface LoginModule(privateAPI)

- (void)p_loadAfterHttpServerWithToken:(NSString*)token userID:(NSString*)userID dao:(NSString*)dao password:(NSString*)password uname:(NSString*)uname success:(void(^)(DDUserEntity* loginedUser))success failure:(void(^)(NSString* error))failure;
- (void)reloginAllFlowSuccess:(void(^)())success failure:(void(^)())failure;

@end

@implementation LoginModule
{
    NSString* _lastLoginUser;       //最后登录的用户ID
    NSString* _lastLoginPassword;
    NSString* _lastLoginUserName;
    NSString* _dao;
    NSString * _priorIP;
    NSInteger _port;
}
+ (instancetype)instance
{
    static LoginModule *g_LoginManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_LoginManager = [[LoginModule alloc] init];
    });
    return g_LoginManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _httpServer = [[DDHttpServer alloc] init];
        _msgServer = [[DDMsgServer alloc] init];
        _tcpServer = [[DDTcpServer alloc] init];
    }
    return self;
}


#pragma mark Public API
- (void)loginWithUsername:(NSString*)name password:(NSString*)password success:(void(^)(DDUserEntity* loginedUser))success failure:(void(^)(NSString* error))failure
{

    //_httpServer getMsgIp聊天得总服务器地址由此获得ip地址和端口号
    [_httpServer getMsgIp:^(NSDictionary *dic) {
        NSInteger code  = [[dic objectForKey:@"code"] integerValue];
        if (code == 0) {
            _priorIP = [dic objectForKey:@"priorIP"];
            _port    =  [[dic objectForKey:@"port"] integerValue];
            [[NSUserDefaults standardUserDefaults]setObject:_priorIP forKey:@"priorIP"];
            [[NSUserDefaults standardUserDefaults]setObject:[dic objectForKey:@"port"] forKey:@"port"];
            [RuntimeStatus instance].msfs=[dic objectForKey:@"msfsPrior"];
            [RuntimeStatus instance].discoverUrl=[dic objectForKey:@"discovery"];
            //_tcpServer loginTcpServerIP通过ip地址和端口号登录TCP服务器
            [_tcpServer loginTcpServerIP:_priorIP port:_port Success:^{
                [_msgServer checkUserID:name Pwd:password token:@"" success:^(id object) {
                    [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
                    [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"username"];
                    [[NSUserDefaults standardUserDefaults]setValue:@"success" forKey:@"loginstatus"];//已经登录成功
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    _lastLoginPassword=password;
                    _lastLoginUserName=name;
                    [DDClientState shareInstance].userState=DDUserOnline;
                    DDUserEntity* user = object[@"user"];
                    [[NSUserDefaults standardUserDefaults] setObject:[user.objID substringFromIndex:5] forKey:@"userid"];
                    [[NSUserDefaults standardUserDefaults] setObject:user.objID forKey:@"user_userid"];
                    [RuntimeStatus instance].user=user;
                    [[NSUserDefaults standardUserDefaults] setObject:user.nick forKey:@"nickname"];
                    [[NSUserDefaults standardUserDefaults]setObject:user.avatar forKey:@"portrait"];
                    [[RuntimeStatus instance] updateData];
                    [[DDDatabaseUtil instance] openCurrentUserDB];
//                    [self p_loadAllUsersCompletion:^{
//                        
//                    }];
                    [self p_loadAllHttpUsers];
                    success(user);
                     [DDNotificationHelp postNotification:DDNotificationUserLoginSuccess userInfo:nil object:user];
                } failure:^(id object) {
                    failure(@"登陆失败");
                }];
                
            } failure:^{
                failure(@"登陆失败");

            }];
        }else{
        
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"连接服务器失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
        }
    } failure:^(NSString *error) {
        failure(@"登陆失败");

    }];
    
}

- (void)reloginSuccess:(void(^)())success failure:(void(^)(NSString* error))failure
{
    if ([DDClientState shareInstance].userState == DDUserOffLine && _lastLoginPassword && _lastLoginUserName) {
        
        [self loginWithUsername:_lastLoginUserName password:_lastLoginPassword success:^(DDUserEntity *user) {
            [[NSNotificationCenter defaultCenter] postNotificationName:DDNotificationUserReloginSuccess object:nil];
            success(YES);
        } failure:^(NSString *error) {
            failure(@"重新登录失败");
        }];

    }
}

- (void)offlineCompletion:(void(^)())completion
{
    completion();
}


#pragma -mark通过http获取所有的联系人
-(void)p_loadAllHttpUsers
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary *params = @{@"uid":userid,@"token":@"110"};
    
    [HttpTool postWithPath:@"GetFriendListWithGroup" params:params success:^(id JSON) {
        FriendGroupModel * fgModel = [FriendGroupModel objectWithKeyValues:JSON];
        //存储好友的信息
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [fgModel.friendgroups enumerateObjectsUsingBlock:^(GroupModel * obj, NSUInteger idx, BOOL *stop) {
                [obj.groupmembers enumerateObjectsUsingBlock:^(FriendModel * fModel, NSUInteger idx, BOOL *stop) {
                      NSString * user_id = [NSString stringWithFormat:@"user_%@",fModel.userid];
                    DDUserEntity * entity = [[DDUserEntity alloc]initWithUserID:user_id name:fModel.realname nick:fModel.markname avatar:fModel.portrait userRole:100 userUpdated:100];
                    [[DDUserModule shareInstance] addMaintanceUser:entity];

                    
                }];
                
            }];

            
        });
    } failure:^(NSError *error) {
        
        
    }];
}

/**
 *  登录成功后获取所有用户
 *
 *  @param completion 异步执行的block
 */
- (void)p_loadAllUsersCompletion:(void(^)())completion
{
    __block NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    __block NSInteger version = [[defaults objectForKey:@"alllastupdatetime"] integerValue];
    [[DDDatabaseUtil instance] getAllUsers:^(NSArray *contacts, NSError *error) {
        if ([contacts count] !=0) {
            [contacts enumerateObjectsUsingBlock:^(DDUserEntity *obj, NSUInteger idx, BOOL *stop) {
                [[DDUserModule shareInstance] addMaintanceUser:obj];
            }];
        }else{
            version=0;
            DDAllUserAPI* api = [[DDAllUserAPI alloc] init];
            [api requestWithObject:@[@(version)] Completion:^(id response, NSError *error) {
                if (!error)
                {
                    
                    NSUInteger responseVersion = [[response objectForKey:@"alllastupdatetime"] integerValue];
                    if (responseVersion == version && responseVersion !=0) {
                        
                        return ;
                        
                    }
                    [defaults setObject:@(responseVersion) forKey:@"alllastupdatetime"];
                    NSMutableArray *array = [response objectForKey:@"userlist"];
                    [[DDDatabaseUtil instance] insertAllUser:array completion:^(NSError *error) {
                        
                    }];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        [array enumerateObjectsUsingBlock:^(DDUserEntity *obj, NSUInteger idx, BOOL *stop) {
                            [[DDUserModule shareInstance] addMaintanceUser:obj];
                        }];
                    });
                    
                    
                }
            }];
        }
    }];
    
    DDAllUserAPI* api = [[DDAllUserAPI alloc] init];
    [api requestWithObject:@[@(version)] Completion:^(id response, NSError *error) {
        if (!error)
        {
            NSUInteger responseVersion = [[response objectForKey:@"alllastupdatetime"] integerValue];
            if (responseVersion == version && responseVersion !=0) {
                
                return ;

            }
            [defaults setObject:@(responseVersion) forKey:@"alllastupdatetime"];
            NSMutableArray *array = [response objectForKey:@"userlist"];
            [[DDDatabaseUtil instance] insertAllUser:array completion:^(NSError *error) {
                
            }];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [array enumerateObjectsUsingBlock:^(DDUserEntity *obj, NSUInteger idx, BOOL *stop) {
                    [[DDUserModule shareInstance] addMaintanceUser:obj];
                }];
            });
            
            
        }
    }];
    
}

@end
