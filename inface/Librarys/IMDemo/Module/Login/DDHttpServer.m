//
//  DDHttpServer.m
//  Duoduo
//
//  Created by murray on 14-4-5.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDHttpServer.h"
#import "DDAFClient.h"
@implementation DDHttpServer
- (void)loginWithUserName:(NSString*)userName
                 password:(NSString*)password
                  success:(void(^)(id respone))success
                  failure:(void(^)(id error))failure
{
    //    DDHttpModule* module = [DDHttpModule shareInstance];
    NSMutableDictionary* dictParams = [NSMutableDictionary dictionary];
    [dictParams setObject:userName forKey:@"user_email"];
    [dictParams setObject:password forKey:@"user_pass"];
    [dictParams setObject:@"ooxx" forKey:@"macim"];
    [dictParams setObject:@"1.0" forKey:@"imclient"];
    [dictParams setObject:@"1" forKey:@"remember"];
    [DDAFClient jsonFormPOSTRequest:@"user/zlogin/" param:dictParams success:^(id result) {
        success(result);
    } failure:^(NSError * error) {
        failure(error);
    }];
    
}
-(void)getMsgIp:(void(^)(NSDictionary *dic))block failure:(void(^)(NSString* error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //CHAT_SERVER_BASE_URL聊天得总服务器地址由此获得ip地址和端口号
    [manager GET:[NSString stringWithFormat:@"%@",CHAT_SERVER_BASE_URL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        block(responseDictionary);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *errordes = error.domain;
//        NSLog(@"error%@",error);
        failure(errordes);
    } ];
    
    
    
}
@end