//
//  DDReceiveKickoffAPI.h
//  murray4iPhone
//
//  Created by murray on 14-6-29.
//  Copyright (c) 2014年 juangua. All rights reserved.
//

#import "DDUnrequestSuperAPI.h"

@interface ReceiveKickoffAPI : DDUnrequestSuperAPI<DDAPIUnrequestScheduleProtocol>

@end
