//
//  DDReceiveMessageAPI.h
//  IOSDuoduo
//
//  Created by murray on 14-6-5.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDUnrequestSuperAPI.h"

@interface DDReceiveMessageAPI : DDUnrequestSuperAPI<DDAPIUnrequestScheduleProtocol>

@end
