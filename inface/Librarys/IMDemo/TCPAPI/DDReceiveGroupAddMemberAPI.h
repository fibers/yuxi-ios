//
//  DDReceiveGroupAddMemberAPI.h
//  Duoduo
//
//  Created by murray on 14-5-8.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDUnrequestSuperAPI.h"

@interface DDReceiveGroupAddMemberAPI : DDUnrequestSuperAPI<DDAPIUnrequestScheduleProtocol>

@end
