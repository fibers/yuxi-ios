//
//  DDTcpClientManager.h
//  Duoduo
//
//  Created by murray on 14-4-12.
//  Copyright (c) 2014年 murray. All rights reserved.
//

//socket tcp连接建立 传数据 关闭等等
#import <Foundation/Foundation.h>
@class DDSendBuffer;
@interface DDTcpClientManager : NSObject<NSStreamDelegate>
{
@private
    NSInputStream *_inStream;
    NSOutputStream *_outStream;
    NSLock *_receiveLock;
	NSMutableData *_receiveBuffer;
    NSLock *_sendLock;
	NSMutableArray *_sendBuffers;
	DDSendBuffer *_lastSendBuffer;
	BOOL _noDataSent;
    int32_t cDataLen;

}

+ (instancetype)instance;

-(void)connect:(NSString *)ipAdr port:(NSInteger)port status:(NSInteger)status;
-(void)disconnect;
-(void)writeToSocket:(NSMutableData *)data;

-(void)reconnect;//断线重连
@end
