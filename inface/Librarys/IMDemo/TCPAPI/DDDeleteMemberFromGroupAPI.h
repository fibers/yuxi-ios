//
//  DDDeleteMemberFromGroupAPI.h
//  Duoduo
//
//  Created by murray on 14-5-8.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDSuperAPI.h"
/**
 *  移除讨论组中的某个用户,index1:groupId,index2:userlist
 */
@interface DDDeleteMemberFromGroupAPI : DDSuperAPI

@end
