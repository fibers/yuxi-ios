//
//  DDAddFriendAPI.m
//  inface
//
//  Created by xigesi on 15/5/5.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "DDAddFriendAPI.h"

@implementation DDAddFriendAPI

-(int)requestTimeOutTimeInterval
{
    return 10.0;
}

-(int)responseServiceID
{
    return DDSERVICE_FRI;
}

-(int)requestServiceID
{
    return DDSERVICE_FRI;
}



@end
