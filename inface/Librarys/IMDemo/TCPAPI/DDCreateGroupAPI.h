//
//  DDCreateGroupAPI.h
//  Duoduo
//
//  Created by murray on 14-5-8.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDSuperAPI.h"

/**
 *  创建讨论组，object为数组，index1:groupName,index2:groupAvatar,index3:userlist
 */
@interface DDCreateGroupAPI : DDSuperAPI

@end
