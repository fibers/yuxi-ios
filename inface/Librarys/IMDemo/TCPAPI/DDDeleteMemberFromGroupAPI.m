//
//  DDDeleteMemberFromGroupAPI.m
//  Duoduo
//
//  Created by murray on 14-5-8.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDDeleteMemberFromGroupAPI.h"
#import "GroupEntity.h"
#import "DDGroupModule.h"
#import "IMGroup.pb.h"
@implementation DDDeleteMemberFromGroupAPI
/**
 *  请求超时时间
 *
 *  @return 超时时间
 */
- (int)requestTimeOutTimeInterval
{
    return TimeOutTimeInterval;
}

/**
 *  请求的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)requestServiceID
{
    return SERVICE_GROUP;
}

/**
 *  请求返回的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)responseServiceID
{
    return SERVICE_GROUP;
}

/**
 *  请求的commendID
 *
 *  @return 对应的commendID
 */
- (int)requestCommendID
{
    return CMD_ID_GROUP_CHANGE_GROUP_REQ;
}

/**
 *  请求返回的commendID
 *
 *  @return 对应的commendID
 */
- (int)responseCommendID
{
    return CMD_ID_GROUP_CHANGE_GROUP_RES;
}

/**
 *  解析数据的block
 *
 *  @return 解析数据的block
 */
- (Analysis)analysisReturnData
{
    Analysis analysis = (id)^(NSData* data)
    {
        
        IMGroupChangeMemberRsp *rsp = [IMGroupChangeMemberRsp parseFromData:data];

        uint32_t result =rsp.resultCode;
        GroupEntity *groupEntity = nil;
        if (result != 0)
        {
            return groupEntity;
        }
        NSString *groupId =[GroupEntity pbGroupIdToLocalID:rsp.groupId];
        //NSArray *currentUserIds = rsp.curUserIdList;

        groupEntity =  [[DDGroupModule instance] getGroupByGId:groupId];
        NSMutableArray *array = [NSMutableArray new];
        for (uint32_t i = 0; i < [rsp.curUserIdList count]; i++) {
            NSString* userId = [[RuntimeStatus instance] changeOriginalToLocalID:[rsp.curUserIdList[i] integerValue] SessionType:SessionTypeSessionTypeSingle];
            [array addObject:userId];
        }
        groupEntity.groupUserIds=array;
        return groupEntity;
        
    };
    return analysis;
}

/**
 *  打包数据的block
 *
 *  @return 打包数据的block
 */
- (Package)packageRequestObject
{
    Package package = (id)^(id object,uint16_t seqNo)
    {
        NSArray* array = (NSArray*)object;
        NSString* groupId = array[0];
//        NSUInteger userid = [[RuntimeStatus instance] changeIDToOriginal:array[1]];        
        IMGroupChangeMemberReqBuilder *memberChange = [IMGroupChangeMemberReq builder];
        [memberChange setUserId:[[RuntimeStatus instance] changeIDToOriginal:[RuntimeStatus instance].user.objID]];
        [memberChange setChangeType:GroupModifyTypeGroupModifyTypeDel];
        [memberChange setGroupId:[[RuntimeStatus instance] changeIDToOriginal:groupId]];
        NSMutableArray * originalId = [NSMutableArray array];
        if ([array[1]count] >0) {
            NSArray * groupList = array[1];
            for (NSString * localID in groupList) {
                 [originalId addObject:@([[RuntimeStatus instance] changeIDToOriginal:localID])];
            }
        }
        
        [memberChange setMemberIdListArray:originalId];
        
        DDDataOutputStream *dataout = [[DDDataOutputStream alloc] init];
        [dataout writeInt:0];
        [dataout writeTcpProtocolHeader:SERVICE_GROUP cId:CMD_ID_GROUP_CHANGE_GROUP_REQ seqNo:seqNo];
        [dataout directWriteBytes:[memberChange build].data];
        [dataout writeDataCount];
        return [dataout toByteArray];
    };
    return package;
}
@end
