//
//  DDAddMemberToGroupAPI.h
//  Duoduo
//
//  Created by murray on 14-5-8.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDSuperAPI.h"

/**
 *  添加用户到讨论组，object 为数组，index1:groupId,index2:userIdList
 */
@interface DDAddMemberToGroupAPI : DDSuperAPI

@end
