//
//  MsgAddGroupMemberReqAPI.m
//  inface
//
//  Created by xigesi on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MsgAddGroupMemberReqAPI.h"
#import "IMMessage.pb.h"
@implementation MsgAddGroupMemberReqAPI
/**
 *  请求超时时间
 *
 *  @return 超时时间
 */
- (int)requestTimeOutTimeInterval
{
    return 0;
}

/**
 *  请求的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)requestServiceID
{
    return DDSERVICE_MESSAGE;
}

/**
 *  请求返回的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)responseServiceID
{
    return 0;
}

/**
 *  请求的commendID
 *
 *  @return 对应的commendID
 */
- (int)requestCommendID
{
    return MSG_ADD_GROUP_MEMBER_REQ;
}

/**
 *  请求返回的commendID
 *
 *  @return 对应的commendID
 */
- (int)responseCommendID
{
    return 0;
}

/**
 *  解析数据的block
 *
 *  @return 解析数据的block
 */
- (Analysis)analysisReturnData
{
    Analysis analysis = (id)^(NSData* data)
    {
        
    };
    return analysis;
}

/**
 *  打包数据的block
 *
 *  @return 打包数据的block
 */



- (Package)packageRequestObject
{
    Package package = (id)^(id object,uint16_t seqNo)
    {

        NSArray * arrary = (NSArray *)object;
        NSMutableArray * originalID = [NSMutableArray new];
        if ([arrary[1]count] >0) {
            NSArray * groupList = arrary[1];
            for (NSString * localID in groupList) {
                [ originalID addObject:@([[RuntimeStatus instance] changeIDToOriginal:localID])];
            }
        }
  
        IMMsgAddGroupMemberReqBuilder * addGroupReq = [IMMsgAddGroupMemberReq builder];
        [addGroupReq setUserId:0];
        [addGroupReq setSessionId:[arrary[4]integerValue]];
        [addGroupReq setMsgId:[arrary[3]integerValue]];
        [addGroupReq setSessionType:[arrary[2]integerValue]];
        [addGroupReq setGroupId:[[RuntimeStatus instance] changeIDToOriginal:arrary[0] ]];
        [addGroupReq setMemberIdListArray:originalID];
        DDDataOutputStream * dataout = [[DDDataOutputStream alloc]init];
        [dataout writeInt:0];
        [dataout writeTcpProtocolHeader:DDSERVICE_MESSAGE cId:MSG_ADD_GROUP_MEMBER_REQ seqNo:seqNo];
        [dataout directWriteBytes:[addGroupReq build].data];
        [dataout writeDataCount];
        return [dataout toByteArray];
    };
    return package;
}

@end
