//
//  MsgAddGroupConfirmNotify.m
//  inface
//
//  Created by xigesi on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MsgAddGroupConfirmNotify.h"
#import "IMMessage.pb.h"
#import "DDTcpProtocolHeader.h"

@implementation MsgAddGroupConfirmNotify
- (int)responseServiceID
{
    return DDSERVICE_MESSAGE;
}

- (int)responseCommandID
{
    return MSG_ADD_GROUP_MEMBER_CONFIRM_NOTIFY;
}

- (UnrequestAPIAnalysis)unrequestAnalysis
{
    UnrequestAPIAnalysis analysis = (id)^(NSData *data)
    {
        IMMsgAddGroupMemberConfirmNotify *notify = [IMMsgAddGroupMemberConfirmNotify parseFromData:data];
        NSMutableDictionary *dic = [NSMutableDictionary new];
        UInt32   sessionType =notify.sessionType;
        NSString *from_id = [[RuntimeStatus instance] changeOriginalToLocalID:notify.sessionId SessionType:sessionType];
        NSString * group_id = [[RuntimeStatus instance]changeOriginalToLocalID:notify.groupId SessionType:sessionType];
        UInt32   msgId    = notify.msgId;
        [dic setObject:from_id forKey:@"from_id"];
        [dic setObject:@(msgId) forKey:@"msgId"];
        [dic setObject:@(sessionType) forKey:@"type"];
        [dic setObject:group_id forKey:@"group_id"];
        NSString * result = [NSString stringWithFormat:@"%d",(unsigned int)notify.result];
        [dic setObject:result forKey:@"result"];
        return dic;
    };
    return analysis;
}
@end
