//
//  MsgAddGroupConfirmNotify.h
//  inface
//
//  Created by xigesi on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "DDUnrequestSuperAPI.h"

@interface MsgAddGroupConfirmNotify : DDUnrequestSuperAPI<DDAPIUnrequestScheduleProtocol>

@end
