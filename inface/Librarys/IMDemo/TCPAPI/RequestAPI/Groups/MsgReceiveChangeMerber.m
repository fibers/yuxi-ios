//
//  MsgReceiveChangeMerber.m
//  inface
//
//  Created by xigesi on 15/5/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MsgReceiveChangeMerber.h"
#import "IMMessage.pb.h"
#import "IMGroup.pb.h"
@implementation MsgReceiveChangeMerber
- (int)responseServiceID
{
    return SERVICE_GROUP;
}

- (int)responseCommandID
{
    return CID_GROUP_CHANGE_MEMBER_NOTIFY;
}


- (UnrequestAPIAnalysis)unrequestAnalysis
{
    UnrequestAPIAnalysis analysis = (id)^(NSData* data)
    {
        IMGroupChangeMemberNotify * notify = [IMGroupChangeMemberNotify parseFromData:data];
        
        NSMutableDictionary *dic = [NSMutableDictionary new];
        //发起人的id；
       UInt32 ownerid = notify.userId;
        UInt32 changeType = notify.changeType;
        
        UInt32 groupid = notify.groupId;
         PBAppendableArray * useridList = (PBAppendableArray *)[notify curUserIdList];
        PBAppendableArray * chagArray = (PBAppendableArray *)[notify chgUserIdList];
        NSMutableArray * changArr = [NSMutableArray array];
        NSMutableArray * usersArr = [NSMutableArray array];
        for (int index = 0; index < useridList.count; index++) {
            int value = [useridList uint32AtIndex:index];
            NSString * curUsers = [NSString stringWithFormat:@"%d",value];
            [usersArr addObject:curUsers];
        }
        for (int j = 0; j < [chagArray count]; j++) {
            int  value = [chagArray uint32AtIndex:j];
            NSString * changeUses = [NSString stringWithFormat:@"%d",value];
            [changArr addObject:changeUses];
            
        }
//        NSLog(@"curarrary :%@ chagarrary :%@",curarrary,chagArray);
        [dic setObject:[NSString stringWithFormat:@"%d",(unsigned int)ownerid] forKey:@"ownerid"];
        [dic setObject:[NSString stringWithFormat:@"%d",(unsigned int)changeType] forKey:@"changeType"];
        [dic setObject:[NSString stringWithFormat:@"%d",(unsigned int)groupid] forKey:@"groupid"];
        [dic setObject:usersArr forKey:@"usersArr"];
        [dic setObject:changArr forKey:@"changArr"];

        return dic;
    
    };
    return analysis;
}

@end
