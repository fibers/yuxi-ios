//
//  MsgAddGroupNotifyAPI.m
//  inface
//
//  Created by xigesi on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MsgAddGroupNotifyAPI.h"
#import "IMMessage.pb.h"
#import "DDTcpProtocolHeader.h"
@implementation MsgAddGroupNotifyAPI
- (int)responseServiceID
{
    return DDSERVICE_MESSAGE;
}

- (int)responseCommandID
{
    return MSG_ADD_GROUP_MEMBER_NOTIFY;
}
/*
 userId: 47.
 sessionId: 57
 groupId: 63
 msgId: 0
 sessionType: 2
 
 */
- (UnrequestAPIAnalysis)unrequestAnalysis
{
    UnrequestAPIAnalysis analysis = (id)^(NSData *data)
    {
        IMMsgAddGroupMemberNotify *notify = [IMMsgAddGroupMemberNotify parseFromData:data];
        NSMutableDictionary *dic = [NSMutableDictionary new];
        UInt32   sessionType =notify.sessionType;
        NSString *from_id = [[RuntimeStatus instance] changeOriginalToLocalID:notify.sessionId SessionType:sessionType];
        UInt32   msgId    = notify.msgId;
        NSString * group_id = [NSString stringWithFormat:@"%d",(unsigned int)notify.groupId];
        
        [dic setObject:from_id forKey:@"from_id"];
        [dic setObject:@(msgId) forKey:@"msgId"];
        [dic setObject:@(sessionType) forKey:@"type"];
        [dic setObject:group_id forKey:@"group_id"];
        return dic;
    };
    return analysis;
}
@end
