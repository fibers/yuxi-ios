//
//  MsgAddGroupConfirmAPI.m
//  inface
//
//  Created by xigesi on 15/5/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MsgAddGroupConfirmAPI.h"
#import "IMMessage.pb.h"
@implementation MsgAddGroupConfirmAPI

/**
 *  请求超时时间
 *
 *  @return 超时时间
 */
- (int)requestTimeOutTimeInterval
{
    return 0;
}

/**
 *  请求的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)requestServiceID
{
    return DDSERVICE_MESSAGE;
}

/**
 *  请求返回的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)responseServiceID
{
    return 0;
}

/**
 *  请求的commendID
 *
 *  @return 对应的commendID
 */
- (int)requestCommendID
{
    return MSG_ADD_GROUP_MEMBER_CONFIRM_REQ;
}

/**
 *  请求返回的commendID
 *
 *  @return 对应的commendID
 */
- (int)responseCommendID
{
    return 0;
}

/**
 *  解析数据的block
 *
 *  @return 解析数据的block
 */
- (Analysis)analysisReturnData
{
    Analysis analysis = (id)^(NSData* data)
    {
        
    };
    return analysis;
}

/**
 *  打包数据的block
 *
 *  @return 打包数据的block
 */
- (Package)packageRequestObject
{
    Package package = (id)^(id object,uint16_t seqNo)
    {
        IMMsgAddGroupMemberConfirmReqBuilder *addGroupConfirmReq = [IMMsgAddGroupMemberConfirmReq builder];
        [addGroupConfirmReq setUserId:0];
        
        [addGroupConfirmReq setSessionId:[object[1]integerValue]];
         [addGroupConfirmReq setMsgId:[object[3] integerValue]];
        [addGroupConfirmReq setSessionType:[object[4] integerValue]];
        [addGroupConfirmReq setResult:[object[5] integerValue]];
        [addGroupConfirmReq setGroupId:[object[2]integerValue]];
        DDDataOutputStream *dataout = [[DDDataOutputStream alloc] init];
        [dataout writeInt:0];
        [dataout writeTcpProtocolHeader:DDSERVICE_MESSAGE cId:MSG_ADD_GROUP_MEMBER_CONFIRM_REQ seqNo:seqNo];
        [dataout directWriteBytes:[addGroupConfirmReq build].data];
        [dataout writeDataCount];
        return [dataout toByteArray];
    };
    return package;
}


@end
