//
//  MsgReadNotify.h
// IM
//
//  Created by Michael Scofield on 2014-12-19.
//  Copyright (c) 2014 murray. All rights reserved.
//

#import "DDUnrequestSuperAPI.h"

@interface MsgAddFriendNotifyAPI : DDUnrequestSuperAPI<DDAPIUnrequestScheduleProtocol>

@end
