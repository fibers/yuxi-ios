//
//  DDGroupMsgReadACKAPI.m
//  Duoduo
//
//  Created by murray on 14-5-7.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "MsgAddFriendConfirmReqAPI.h"
#import "IMMessage.pb.h"
@implementation MsgAddFriendConfirmReqAPI
/**
 *  请求超时时间
 *
 *  @return 超时时间
 */
- (int)requestTimeOutTimeInterval
{
    return 0;
}

/**
 *  请求的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)requestServiceID
{
    return DDSERVICE_MESSAGE;
}

/**
 *  请求返回的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)responseServiceID
{
    return 0;
}

/**
 *  请求的commendID
 *
 *  @return 对应的commendID
 */
- (int)requestCommendID
{
    return MSG_ADD_FRIEND_CONFIRM_REQ;
}

/**
 *  请求返回的commendID
 *
 *  @return 对应的commendID
 */
- (int)responseCommendID
{
    return 0;
}

/**
 *  解析数据的block
 *
 *  @return 解析数据的block
 */
- (Analysis)analysisReturnData
{
    Analysis analysis = (id)^(NSData* data)
    {
        
    };
    return analysis;
}

/**
 *  打包数据的block
 *
 *  @return 打包数据的block
 */
- (Package)packageRequestObject
{
    Package package = (id)^(id object,uint16_t seqNo)
    {
        IMMsgAddFriendConfirmReqBuilder *addFriendConfirmReq = [IMMsgAddFriendConfirmReq builder];
        [addFriendConfirmReq setUserId:0];
        
        [addFriendConfirmReq setSessionId:[[RuntimeStatus instance] changeIDToOriginal:object[0]]];
        [addFriendConfirmReq setMsgId:[object[1] integerValue]];
        [addFriendConfirmReq setSessionType:[object[2] integerValue]];
        [addFriendConfirmReq setResult:[object[3] integerValue]];
        DDDataOutputStream *dataout = [[DDDataOutputStream alloc] init];
        [dataout writeInt:0];
        [dataout writeTcpProtocolHeader:DDSERVICE_MESSAGE cId:MSG_ADD_FRIEND_CONFIRM_REQ seqNo:seqNo];
        [dataout directWriteBytes:[addFriendConfirmReq build].data];
        [dataout writeDataCount];
        return [dataout toByteArray];
    };
    return package;
}
@end
