//
//  MsgReadNotify.m
// IM
//
//  Created by Michael Scofield on 2014-12-19.
//  Copyright (c) 2014 murray. All rights reserved.
//

#import "MsgAddFriendNotifyAPI.h"
#import "IMMessage.pb.h"
#import "DDTcpProtocolHeader.h"
@implementation MsgAddFriendNotifyAPI
- (int)responseServiceID
{
    return DDSERVICE_MESSAGE;
}

- (int)responseCommandID
{
    return MSG_ADD_FRIEND_NOTIFY;
}

- (UnrequestAPIAnalysis)unrequestAnalysis
{
    UnrequestAPIAnalysis analysis = (id)^(NSData *data)
    {
        IMMsgAddFriendNotify *notify = [IMMsgAddFriendNotify parseFromData:data];
        NSMutableDictionary *dic = [NSMutableDictionary new];
        UInt32   sessionType =notify.sessionType;
        NSString *from_id = [[RuntimeStatus instance] changeOriginalToLocalID:notify.sessionId SessionType:sessionType];
        UInt32   msgId    = notify.msgId;
        NSString * addMessage = notify.msgData;
        [dic setObject:from_id forKey:@"from_id"];
        [dic setObject:@(msgId) forKey:@"msgId"];
        [dic setObject:@(sessionType) forKey:@"type"];
        [dic setObject:addMessage forKey:@"addMessage"];
        return dic;
    };
    return analysis;
}
@end
