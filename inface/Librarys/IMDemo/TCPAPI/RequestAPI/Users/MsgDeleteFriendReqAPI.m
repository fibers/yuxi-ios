//
//  MsgDeleteFriendReqAPI.m
//  inface
//
//  Created by xigesi on 15/5/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "MsgDeleteFriendReqAPI.h"
#import "IMMessage.pb.h"
@implementation MsgDeleteFriendReqAPI

/**
 *  请求超时时间
 *
 *  @return 超时时间
 */
- (int)requestTimeOutTimeInterval
{
    return 0;
}


/**
 *  请求的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)requestServiceID
{
    return DDSERVICE_MESSAGE;
}


/**
 *  请求返回的serviceID
 *
 *  @return 对应的serviceID
 */
- (int)responseServiceID
{
    return 0;
}


/**
 *  请求的commendID
 *
 *  @return 对应的commendID
 */
- (int)requestCommendID
{
    return MSG_DEL_FRIEND_REQ;
}

/**
 *  请求返回的commendID
 *
 *  @return 对应的commendID
 */
- (int)responseCommendID
{
    return 0;
}

/**
 *  解析数据的block
 *
 *  @return 解析数据的block
 */
- (Analysis)analysisReturnData
{
    Analysis analysis = (id)^(NSData* data)
    {
        
    };
    return analysis;
}

/**
 *  打包数据的block
 *
 *  @return 打包数据的block
 */
- (Package)packageRequestObject
{
    Package package = (id)^(id object,uint16_t seqNo)
    {
        IMMsgDelFriendReqBuilder * deleteFriendReq = [IMMsgDelFriendReq builder];
        [deleteFriendReq setUserId:0];
        [deleteFriendReq setSessionId:[[RuntimeStatus instance] changeIDToOriginal:object[0]]];
        [deleteFriendReq setMsgId:[object[1]intValue]];
        [deleteFriendReq setSessionType:[object[2]intValue]];
        DDDataOutputStream * dataout = [[DDDataOutputStream alloc]init];
        [dataout writeInt:0];
        [dataout writeTcpProtocolHeader:DDSERVICE_MESSAGE cId:MSG_DEL_FRIEND_REQ seqNo:seqNo];
        [dataout directWriteBytes:[deleteFriendReq build].data];
        [dataout writeDataCount];
  
        return [dataout toByteArray];
    };
    return package;
}
@end
