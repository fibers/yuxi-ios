//
//  DDSendMessageAPI.h
//  Duoduo
//
//  Created by murray on 14-5-8.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDSuperAPI.h"

/**
 *  发送消息，object 为一个数组，index1:fromID,index2:toID,index3:messageSeqNo,index4:messageType,index5:messageSenderType,index6:messageContent,index7:messageAttachContent
 */
@interface SendMessageAPI : DDSuperAPI

@end
