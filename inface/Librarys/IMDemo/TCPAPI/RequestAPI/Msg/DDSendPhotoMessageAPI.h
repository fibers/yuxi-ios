//
//  DDSendPhotoMessageAPI.h
//  IOSDuoduo
//
//  Created by murray on 14-6-6.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDSuperAPI.h"

@interface DDSendPhotoMessageAPI : NSObject
+ (DDSendPhotoMessageAPI *)sharedPhotoCache;
- (void)uploadImage:(UIImage *)image success:(void(^)(NSString* imageURL))success failure:(void(^)(id error))failure;
@end
