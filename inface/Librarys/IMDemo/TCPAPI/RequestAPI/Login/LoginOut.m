//
//  LoginOut.m
//  inface
//
//  Created by xigesi on 15/5/17.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "LoginOut.h"
#import "IMLogin.pb.h"
@implementation LoginOut
- (int)requestTimeOutTimeInterval
{
    return 5;
}

- (int)requestServiceID
{
    return DDSERVICE_LOGIN;
}

- (int)responseServiceID
{
    return DDSERVICE_LOGIN;
}

- (int)requestCommendID
{
    return CID_LOGIN_REQ_LOGINOUT;
}

- (int)responseCommendID
{
    return CID_LOGIN_RES_LOGINOUT;
}

- (Analysis)analysisReturnData
{
    Analysis analysis = (id)^(NSData* data)
    {
        
        IMLogoutRsp *rsp =[IMLogoutRsp parseFromData:data];
        int isok =rsp.resultCode;
        return isok;
    };
    return analysis;
}

- (Package)packageRequestObject
{
    Package package = (id)^(id object,uint32_t seqNo)
    {
        IMLogoutReqBuilder *logoutbuilder = [IMLogoutReq builder];
        DDDataOutputStream *dataout = [[DDDataOutputStream alloc] init];
        [dataout writeInt:0];
        [dataout writeTcpProtocolHeader:DDSERVICE_LOGIN cId:CID_LOGIN_REQ_LOGINOUT seqNo:seqNo];
        [dataout directWriteBytes:[logoutbuilder build].data];
        [dataout writeDataCount];
        return [dataout toByteArray];
    };
    return package;
}

@end
