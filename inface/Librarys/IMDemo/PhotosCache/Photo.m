//
//  DDPhoto.m
//  IOSDuoduo
//
//  Created by murray on 14-6-6.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "Photo.h"

@implementation Photo
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.localPath=nil;
        self.resultUrl=nil;
        self.imageRef = nil;
        self.image= nil;
    }
    return self;
}
- (void)dealloc
{
    self.localPath=nil;
    self.resultUrl=nil;
    CGImageRelease(_imageRef);
    self.image= nil;
}
@end
