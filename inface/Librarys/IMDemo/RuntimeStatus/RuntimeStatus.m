//
//  RuntimeStatus.m
//  IOSDuoduo
//
//  Created by Michael Scofield on 2014-07-31.
//  Copyright (c) 2014 murray. All rights reserved.
//
#define SHIELDINGKEY @"shieldingkey"
#import "RuntimeStatus.h"
#import "DDUserEntity.h"
#import "DDGroupModule.h"

#import "DDMessageModule.h"
#import "DDClientStateMaintenanceManager.h"
#import "LoginViewController.h"
#import "NSString+Additions.h"
#import "ReceiveKickoffAPI.h"
#import "LogoutAPI.h"
#import "DDClientState.h"
#import "IMLogin.pb.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "LoginOut.h"
@interface RuntimeStatus()
@property(strong)NSMutableArray *userDefaults;
@property(strong)NSMutableArray *shieldingArray;
@property (strong,nonatomic)AppDelegate * appDelegate;
@end
@implementation RuntimeStatus

+ (instancetype)instance
{
    static RuntimeStatus* g_runtimeState;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_runtimeState = [[RuntimeStatus alloc] init];
        
    });
    return g_runtimeState;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.user = [DDUserEntity new];
        self.userDefaults =[NSMutableArray arrayWithContentsOfFile:fixedlist];
        self.shieldingArray = [NSMutableArray arrayWithContentsOfFile:shieldinglist];
        [self registerAPI];
    }
    return self;
}
-(void)registerAPI
{
    //接收踢出
    ReceiveKickoffAPI *receiveKick = [ReceiveKickoffAPI new];
    [receiveKick registerAPIInAPIScheduleReceiveData:^(id object, NSError *error) {
        KickReasonType type = [object integerValue];
        [[NSNotificationCenter defaultCenter] postNotificationName:DDNotificationUserKickouted object:@(type)];
        
        LoginOut * loginOut = [[LoginOut alloc]init];
        [loginOut requestWithObject:nil Completion:^(id response, NSError *error) {
            
            [[NSNotificationCenter defaultCenter]postNotificationName:DDNotificationLogout object:nil];
            [RuntimeStatus instance].user = nil;
            [RuntimeStatus instance].userID = nil;
            [DDClientState shareInstance].userState = DDUserOffLineInitiative;
            [[DDTcpClientManager instance]disconnect];
            
        }];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"username"];
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"loginstatus"];//已经登录成功
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"userid"];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"user_userid"];
        
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"gender"];//性别
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"nickname"];//昵称
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"portrait"];//头像
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"intro"];//简介
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"wordcount"];//创作字数
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"newfriendaddmessages"];//新好友添加
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"storycount"];//我的剧的个数
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"likeStorys"];//喜欢得剧的个数
        [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"VOICE"];
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"port"];
        [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"priorIP"];
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:PUSH_CHATVIEW_NUMBER];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        LoginViewController * login = [[LoginViewController alloc]init];
        self.appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
        self.appDelegate.window.rootViewController = nav;
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"注意" message:@"你的账号在其他设备登陆了" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        
        [alertview show];
        
    }
     
    ];
}


-(void)updateData
{
    [DDMessageModule shareInstance];
    [DDClientStateMaintenanceManager shareInstance];
    [DDGroupModule instance];
}
-(void)insertToFixedTop:(NSString *)idString
{
    
    if (self.userDefaults == nil || [self.userDefaults count] == 0) {
        self.userDefaults = [NSMutableArray new];
        [self.userDefaults addObject:idString];
       
    }else
    {
        if (![self.userDefaults containsObject:idString]) {
            [self.userDefaults addObject:idString];
            
        }
    }
    [self.userDefaults writeToFile:fixedlist atomically:YES];
}
-(void)removeFromFixedTop:(NSString *)idString
{
    
    if (self.userDefaults != nil) {
        [self.userDefaults removeObject:idString];
       
    }
     [self.userDefaults writeToFile:fixedlist atomically:YES];
    // [self.userDefaults synchronize];
}
-(BOOL)isInFixedTop:(NSString *)idString
{

    if (self.userDefaults == nil) {
        return NO;
    }else
    {
        if (![self.userDefaults containsObject:idString]) {
            return NO;
        }else
        {
            return YES;
        }
    }
    return NO;

}
-(NSUInteger)getFixedTopCount
{

    return [self.userDefaults count];
}
-(void)addToShielding:(NSString *)string
{
 
    
    if (self.shieldingArray == nil || [self.shieldingArray count] == 0) {
        self.shieldingArray =[NSMutableArray new];
        [self.shieldingArray addObject:string];
 
    }else
    {
        if (![self.shieldingArray containsObject:string]) {
            [self.shieldingArray addObject:string];
           // [self.userDefaults setObject:array forKey:SHIELDINGKEY];
        }
    }
    [self.shieldingArray writeToFile:shieldinglist atomically:YES];
    //[self.userDefaults synchronize];
}
-(void)removeIDFromShielding:(NSString *)idString
{
       if (self.shieldingArray != nil) {
        [self.shieldingArray removeObject:idString];
       // [self.userDefaults setObject:array forKey:SHIELDINGKEY];
    }
    //[self.userDefaults synchronize];
    [self.shieldingArray writeToFile:shieldinglist atomically:YES];
}
-(BOOL)isInShielding:(NSString *)idString
{
   // NSMutableArray *array = [self.userDefaults objectForKey:SHIELDINGKEY];
    if (self.shieldingArray == nil) {
        return NO;
    }else
    {
        if (![self.shieldingArray containsObject:idString]) {
            return NO;
        }else
        {
            return YES;
        }
    }
    return NO;
    
}
-(void)showAlertView:(NSString *)title Description:(NSString *)string
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:string delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}
-(NSInteger)changeIDToOriginal:(NSString *)sessionID
{
    NSArray *array = [sessionID componentsSeparatedByString:@"_"];
    if (array.count > 1) {
        return [array[1]integerValue];
    }else{
        return [array[0]integerValue];
    }

}
-(NSString *)changeOriginalToLocalID:(NSUInteger)orignalID SessionType:(SessionType)sessionType
{
    if(sessionType == SessionTypeSessionTypeSingle)
    {
        return [DDUserEntity pbUserIdToLocalID:orignalID];
    }
    return [GroupEntity pbGroupIdToLocalID:orignalID];
}
@end
