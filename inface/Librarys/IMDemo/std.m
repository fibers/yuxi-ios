//
//  std.m
//  IOSDuoduo
//
//  Created by murray on 14-5-23.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "std.h"
NSString * const kInvited             = @"kInvited";
NSString * const kUserSetting         = @"kUserSetting";
NSString * const kLastLoginUser       = @"kLastLoginUser";
NSString * const kLastPosition        = @"last_pos";
NSString * const kAccessToken         = @"access_token";
NSString * const kRefreshToken        = @"refresh_token";
NSString * const kTokenExpiredTime    = @"expires_in";
NSString * const kAppVersion           = @"kAppVersion";
NSString * const kArrowCount           = @"kArrowCount";



