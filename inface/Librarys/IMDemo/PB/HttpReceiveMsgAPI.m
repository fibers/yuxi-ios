//
//  MsgReadNotify.m
// IM
//
//  Created by Michael Scofield on 2014-12-19.
//  Copyright (c) 2014 murray. All rights reserved.
//

#import "HttpReceiveMsgAPI.H"
#import "IMMessage.pb.h"
@implementation HttpReceiveMsgAPI
- (int)responseServiceID
{
    return DDSERVICE_MESSAGE;
}

- (int)responseCommandID
{
    return MSG_HTTP_RECEIVE_MSG_DATA;
}

//msg2.set_from_user_id(from_user_id);
//msg2.set_to_session_id(to_session_id);
//msg2.set_msg_id(msg_id);
//msg2.set_group_id(group_id);
//msg2.set_reserver(reserver);
//msg2.set_create_time(create_time);
//msg2.msg_type(msg_type);
//msg2.set_msg_data(msg_data);

- (UnrequestAPIAnalysis)unrequestAnalysis
{
    UnrequestAPIAnalysis analysis = (id)^(NSData *data)
    {
        IMHttpReceiveMsgData *notify =[IMHttpReceiveMsgData parseFromData:data];

        NSMutableDictionary *dic = [NSMutableDictionary new];
        
        UInt32 from_user_id = notify.fromUserId;
        UInt32 to_session_id = notify.toSessionId;
        UInt32 msg_id = notify.msgId;
        UInt32 group_id = notify.groupId;
        UInt32 reserver = notify.reserver;
        UInt32 create_time = notify.createTime;
        UInt32   msgType =notify.msgType;
        NSData* msgData =  notify.msgData;
        NSString * tempStr = [[NSString alloc]initWithData:msgData encoding:NSUTF8StringEncoding];
        NSString * msgconstant = [DDMessageEntity makeContentText:tempStr];
        [dic setObject:@(from_user_id) forKey:@"fromfrom_user_id_id"];
        [dic setObject:@(to_session_id) forKey:@"to_session_id"];
        [dic setObject:@(msg_id) forKey:@"msg_id"];
        [dic setObject:@(group_id) forKey:@"group_id"];
        [dic setObject:@(reserver) forKey:@"reserver"];
        [dic setObject:@(create_time) forKey:@"create_time"];
        [dic setObject:@(msgType) forKey:@"msgType"];
        if (![msgconstant isEqualToString:@""] && ![msgconstant isEqual:[NSNull null]] && ![msgconstant isEqual:nil]) {
            [dic setObject:msgconstant forKey:@"msgContant"];
            return dic;

        }else{
            [dic setObject:@"欢迎来到语戏の世界~" forKey:@"msgContant"];
            return dic;
        }
        
    };
    return analysis;
}
@end
