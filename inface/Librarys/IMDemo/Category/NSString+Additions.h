//
//  NSString+Additions.h
//  IOSDuoduo
//
//  Created by murray on 14-5-23.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (murrayString)

+(NSString *)documentPath;
+(NSString *)cachePath;
+(NSString *)formatCurDate;
+(NSString *)formatCurDay;
+(NSString *)getAppVer;
- (NSString*)removeAllSpace;
+(NSString *)formatCurDayForVersion;
- (NSURL *) toURL;
- (BOOL) isEmail;
- (BOOL) isEmpty;
-(NSString *)trim;

-(BOOL) isOlderVersionThan:(NSString*)otherVersion;
-(BOOL) isNewerVersionThan:(NSString*)otherVersion;
@end
