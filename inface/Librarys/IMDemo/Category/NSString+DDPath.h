//
//  NSString+DDPath.h
//  IOSDuoduo
//
//  Created by murray on 14-6-3.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DDPath)
+ (NSString*)userExclusiveDirection;
@end
