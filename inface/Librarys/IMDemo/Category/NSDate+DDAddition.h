//
//  NSDate+DDAddition.h
//  IOSDuoduo
//
//  Created by murray on 14-6-5.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (DDAddition)
- (NSString*)transformToFuzzyDate;
- (NSString*)promptDateString;
- (NSString *)detailPromptDateString;
-(NSString *)getGroupNotyTime;
@end
