//
//  NSDictionary+JSON.h
//  IOSDuoduo
//
//  Created by murray on 14-6-15.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSON)

- (NSString*)jsonString;
+ (NSDictionary*)initWithJsonString:(NSString*)json;

@end
