//
//  BaseViewController.m
//  UUDiffuse
//
//  Created by huangzengsong on 15/1/1.
//  Copyright (c) 2015年 huangzengsong. All rights reserved.
//

#import "BaseViewController.h"
#import "MobClick.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;//不自动延展
    //所有视图的主色调
    self.view.backgroundColor=BACKGROUND_COLOR;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;

    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:[NSString stringWithFormat:@"%@",[self class]]];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:[NSString stringWithFormat:@"%@",[self class]]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
