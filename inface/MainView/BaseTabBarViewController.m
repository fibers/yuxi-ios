

//
//  BaseTabBarViewController.m
//  SuBangDai
//
//  Created by define on 14/12/16.
//  Copyright (c) 2014年 huangzengsong. All rights reserved.
//

#import "BaseTabBarViewController.h"
#import "POP.h"
#import "YXMyCenterViewController.h"
#import "YXSquareViewController.h"
#import "YXFrontCoverViewController.h"
@interface BaseTabBarViewController ()
@property(strong,nonatomic)UIImageView * closeBtnView;
@end

@implementation BaseTabBarViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   __block NSString * currentCount =@"0";
    __block    NSString * normalCount = @"0";
   [[GetLocalRecentSessions shareInstance]totalGroupsUnreadMessageCountSuccess:^(id json) {
      
       normalCount = json;
   }];
    
    //世界
    PolymerizationViewController *aPolymerizationViewController=[[PolymerizationViewController alloc]init];
    UITabBarItem *item2=[[UITabBarItem alloc]initWithTitle:@"剧场" image:[HZSInstances fetUIImage:@"shijieqian"] selectedImage:[HZSInstances fetUIImage:@"shijiehou"]];
    
    item2.tag=0;
    [item2 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:GRAY_COLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [item2 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:BLUECOLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateSelected];
    aPolymerizationViewController.tabBarItem=item2;
    UINavigationController *nav2=[[UINavigationController alloc]initWithRootViewController:aPolymerizationViewController];
    
    //广场
    YXSquareViewController *aSquareViewController=[[YXSquareViewController alloc]init];
    UITabBarItem *item0=[[UITabBarItem alloc]initWithTitle:@"广场" image:[HZSInstances fetUIImage:@"guangchangqian"] selectedImage:[HZSInstances fetUIImage:@"guangchanghou"]];
  
    [item0 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:GRAY_COLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateNormal];

    [item0 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:BLUECOLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateSelected];
    aSquareViewController.tabBarItem=item0;
    UINavigationController *nav0=[[UINavigationController alloc]initWithRootViewController:aSquareViewController];
    
    //创建
    CreateViewController *aCreateViewController=[[CreateViewController alloc]init];
    UITabBarItem *item4=[[UITabBarItem alloc]initWithTitle:@"" image:[HZSInstances fetUIImage:@"fabiaoqian"] selectedImage:[HZSInstances fetUIImage:@"fabiaoqian"]];
    item4.tag=2;
    [item4 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:GRAY_COLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [item4 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:BLUECOLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateSelected];
    aCreateViewController.tabBarItem=item4;
    UINavigationController *nav4=[[UINavigationController alloc]initWithRootViewController:aCreateViewController];
    item4.imageInsets=UIEdgeInsetsMake(6, 0, -6, 0);
    navLogin2=nav4;
    
    
    //聊天
    FriendsViewController *aFriendsViewController=[[ FriendsViewController alloc]init];
    UITabBarItem *item1=[[UITabBarItem alloc]initWithTitle:@"聊天" image:[HZSInstances fetUIImage:@"liaotianqian"] selectedImage:[HZSInstances fetUIImage:@"liaotianhou"]];
    item1.tag=3;
    [item1 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:GRAY_COLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateNormal];
    [item1 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:BLUECOLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateSelected];
    if ( ![normalCount isEqualToString:@"0"]) {
        [item1 setBadgeValue:normalCount];
    }

    aFriendsViewController.tabBarItem=item1;
    UINavigationController *nav1=[[UINavigationController alloc]initWithRootViewController:aFriendsViewController];
    self.nv2= nav1;

    //
    YXMyCenterViewController *aMyCenterViewController=[[YXMyCenterViewController alloc]init];
    [aMyCenterViewController setFromType:YXMyCenterViewControllerTypeFromTabBar andPersonID:USERID];
    [aMyCenterViewController initData];
    UITabBarItem *item3=[[UITabBarItem alloc]initWithTitle:@"我的" image:[HZSInstances fetUIImage:@"zhuyeqian"] selectedImage:[HZSInstances fetUIImage:@"zhuyehou"]];
    item3.tag=4;
    [item3 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:GRAY_COLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [item3 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:BLUECOLOR,NSForegroundColorAttributeName,[UIFont systemFontOfSize:12],NSFontAttributeName, nil] forState:UIControlStateSelected];
    aMyCenterViewController.tabBarItem=item3;
    UINavigationController *nav3=[[UINavigationController alloc]initWithRootViewController:aMyCenterViewController];
    self.viewControllers=[NSArray arrayWithObjects:nav2,nav0,nav4,nav1,nav3,nil];
    
    self.tabBar.translucent = NO;
    self.tabBar.barTintColor=[UIColor whiteColor];
    self.tabBar.backgroundColor = [UIColor clearColor];
    self.delegate=self;
    self.SelectNav=[self.viewControllers objectAtIndex:0];
    UIView * tabBarView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-49, ScreenWidth, 49)];
    
    [self.tabBarController.tabBar insertSubview:tabBarView atIndex:0];
    
    self.tabBarController.tabBar.opaque = YES;
    
}


-(void)loadView
{
    [super loadView];
}


//
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
 
    if (viewController==navLogin2)
    {
//        UIActionSheet* mySheet = [[UIActionSheet alloc]
//                                  initWithTitle:nil
//                                  delegate:self
//                                  cancelButtonTitle:@"取消"
//                                  destructiveButtonTitle:nil
//                                  otherButtonTitles:@"创建剧",@"创建家族",@"写出我的自戏",@"发起话题",nil];
//
//        mySheet.tag=3000;
//        [mySheet showInView:self.view];
        
//        imageViewBackground.hidden=NO;


        //背景
        UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        imageViewBackground=imageView;
//        imageViewBackground.hidden=YES;
        imageViewBackground.backgroundColor=[UIColor colorWithWhite:0 alpha:0.75];
        imageView.userInteractionEnabled=YES;
        [self.view addSubview:imageView];
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(NineBTN:)];
        
        [imageViewBackground addGestureRecognizer:tapGestureRecognizer];
        
        
        NSMutableArray *imagearr=[[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"cjjz"],[UIImage imageNamed:@"cjj"],[UIImage imageNamed:@"xgs"],[UIImage imageNamed:@"fqht"], nil];
        NSMutableArray *titlearr=[[NSMutableArray alloc]initWithObjects:@"创建家族",@"创建剧",@"创建自戏",@"发起话题",nil];
        
        float width=100;
        float height=90;
        //添加9个按钮
        for (int i=0; i<titlearr.count; i++) {
            UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
            aButton.tag=i+1000;
            aButton.frame=CGRectMake(ScreenWidth/2.0-width/2.0, ScreenHeight, width, height);
            
            [imageViewBackground addSubview:aButton];
            
            UIImageView *aimageView=[[UIImageView alloc]initWithFrame:CGRectMake(width/2.0-45.0/2.0, 0, 45, 45)];
            aimageView.userInteractionEnabled=YES;
            aimageView.contentMode = UIViewContentModeScaleAspectFit;
            aimageView.image=[imagearr objectAtIndex:i];
            [aButton addSubview:aimageView];
            
            
            //定义一个label，显示标题内容
            UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, 57, 100, 23)];
            label.backgroundColor =[UIColor clearColor];
            label.userInteractionEnabled=YES;
            label.text=[titlearr objectAtIndex:i];
            label.textColor=[UIColor whiteColor];
            label.font=[UIFont systemFontOfSize:15];
            label.textAlignment=NSTextAlignmentCenter;
            [aButton addSubview:label];
            
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(NineBTN:)];
            [aButton addGestureRecognizer:tapGestureRecognizer];
            
            if (i==0) {
                aButton1=aButton;
            } else if (i==1){
                aButton2=aButton;
            } else if (i==2){
                aButton3=aButton;
            } else if (i==3){
                aButton4=aButton;
            }
            
        }
        ///菜单动画
        POPSpringAnimation * springAnimation1 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        springAnimation1.toValue              = @(ScreenHeight-49-(4-0)*height);
        springAnimation1.springBounciness     = 14;
        [aButton1.layer pop_addAnimation:springAnimation1 forKey:@"position"];
        POPSpringAnimation * springAnimation2 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        springAnimation2.toValue              = @(ScreenHeight-49-(4-1)*height);
        springAnimation2.springBounciness     = 14;
        [aButton2.layer pop_addAnimation:springAnimation2 forKey:@"aButton2"];
        
        
        POPSpringAnimation * springAnimation3 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        springAnimation3.toValue              = @(ScreenHeight-49-(4-2)*height);
        springAnimation3.springBounciness     = 14;
        [aButton3.layer pop_addAnimation:springAnimation3 forKey:@"aButton3"];
        
        POPSpringAnimation * springAnimation4 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        springAnimation4.toValue              = @(ScreenHeight-49-(4-3)*height);
        springAnimation4.springBounciness     = 14;
        [aButton4.layer pop_addAnimation:springAnimation4 forKey:@"aButton4"];
        
        //关闭按钮
        UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
        aButton.tag=1004;
        float width1=49;
        float height1=49;
        aButton.frame=CGRectMake(ScreenWidth/2.0-width1/2.0, ScreenHeight-49, width1, height1);
        [imageViewBackground addSubview:aButton];
        
        
        UIImageView *aimageView=[[UIImageView alloc]initWithFrame:CGRectMake(9, 9, 31, 31)];
        self.closeBtnView = aimageView;
        aimageView.userInteractionEnabled=YES;
        aimageView.contentMode = UIViewContentModeScaleAspectFit;
        aimageView.image=[UIImage imageNamed:@"gb"];
        [aButton addSubview:aimageView];
        POPSpringAnimation * springAnimation5 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerRotation];
        springAnimation5.toValue = @(M_PI);
        springAnimation5.springBounciness = 14;
        [aimageView.layer pop_addAnimation:springAnimation5 forKey:@"aButton5"];
        
        POPSpringAnimation * springAnimation6 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
        springAnimation6.fromValue  = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
        springAnimation6.toValue  = [NSValue valueWithCGSize:CGSizeMake(1.1f, 1.1f)];
        springAnimation6.springBounciness = 20;
        springAnimation6.springSpeed = 12;
        [aimageView.layer pop_addAnimation:springAnimation6 forKey:@"aButton6"];
        
        //这儿设置成YES 就会先跳到下个页面 然后弹出UIActionSheet 设置成NO 就会在当前页面 弹出UIActionSheet
        return NO;
    }
//    imageViewBackground.hidden=YES;
    [imageViewBackground removeFromSuperview];
    return YES;


}


- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    self.SelectNav = (UINavigationController *)viewController;
//    self.selectedIndex = 3;
//    self.selectedViewController = (UInavigationController *)viewController;
}

//- (void)actionSheetCancel:(UIActionSheet *)actionSheet{
//    //
//}
//- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (actionSheet.tag==3000) {
//        
//        if (buttonIndex==0) {
//            //剧按钮
//            TheatreTypeViewController * detailViewController = [[TheatreTypeViewController alloc] init];
//            detailViewController.hidesBottomBarWhenPushed=YES;
//            [SelectNav pushViewController:detailViewController animated:YES];
//            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
//        }else if (buttonIndex==1){
//            //家族按钮
//            CreateFamilyViewController * detailViewController = [[CreateFamilyViewController alloc] init];
//            detailViewController.hidesBottomBarWhenPushed=YES;
//            [SelectNav pushViewController:detailViewController animated:YES];
//            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
//        }else if (buttonIndex==2) {
//            //我的自戏按钮
//            CreateMyStoryViewController * detailViewController = [[CreateMyStoryViewController alloc] init];
//            detailViewController.hidesBottomBarWhenPushed=YES;
//            [SelectNav pushViewController:detailViewController animated:YES];
//            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
//        }else if (buttonIndex==3){
//            //话题按钮
//            CreateTopicViewController * detailViewController = [[CreateTopicViewController alloc] init];
//            detailViewController.hidesBottomBarWhenPushed=YES;
//            [SelectNav pushViewController:detailViewController animated:YES];
//            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
//        }
//    }
//}
//-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
//    //
//}
//-(void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex{
//    //
//}
//


//6按钮
-(void)NineBTN:(UITapGestureRecognizer*) recognizer
{
    
    NSInteger buttonIndex=recognizer.view.tag-1000;
//    imageViewBackground.hidden=YES;
    ///菜单动画
    POPSpringAnimation * springAnimation5 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerRotation];
    springAnimation5.toValue              = @(-M_PI*2-M_PI_4);
    springAnimation5.springBounciness     = 14;
    POPSpringAnimation * springAnimation6 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    springAnimation6.fromValue  = [NSValue valueWithCGSize:CGSizeMake(1.1f, 1.1f)];
    springAnimation6.toValue  = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    springAnimation6.springBounciness = 20;
    springAnimation6.springSpeed = 12;
    [self.closeBtnView.layer pop_addAnimation:springAnimation6 forKey:@"aButton6"];
    [self.closeBtnView.layer pop_addAnimation:springAnimation5 forKey:@"closeView"];
    POPSpringAnimation * springAnimation1 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    springAnimation1.toValue              = @(ScreenHeight+60);
    [aButton1.layer pop_addAnimation:springAnimation1 forKey:@"position"];
    POPSpringAnimation * springAnimation2 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    springAnimation2.toValue              = @(ScreenHeight+60);
    [aButton2.layer pop_addAnimation:springAnimation2 forKey:@"aButton2"];
    POPSpringAnimation * springAnimation3 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    springAnimation3.toValue              = @(ScreenHeight+60);
    [aButton3.layer pop_addAnimation:springAnimation3 forKey:@"aButton3"];
    POPSpringAnimation * springAnimation4 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    springAnimation4.toValue              = @(ScreenHeight+60);
    [springAnimation4 setCompletionBlock:^(POPAnimation * a, BOOL b) {
        [imageViewBackground removeFromSuperview];
    }];
    
    [aButton4.layer pop_addAnimation:springAnimation4 forKey:@"aButton4"];
    POPSpringAnimation *baseAnimationBGC  = [POPSpringAnimation animationWithPropertyNamed:kPOPViewBackgroundColor];
    baseAnimationBGC.toValue              = [UIColor colorWithWhite:0 alpha:0];
    [imageViewBackground pop_addAnimation:baseAnimationBGC forKey:@"bgColor"];
    [baseAnimationBGC setCompletionBlock:^(POPAnimation * a, BOOL b) {
        [imageViewBackground removeFromSuperview];
    }];
    
    if (buttonIndex==1) {
        //剧按钮
//        TheatreTypeViewController * detailViewController = [[TheatreTypeViewController alloc] init];
//        detailViewController.hidesBottomBarWhenPushed=YES;
//        if (detailViewController.isAnimating) {
//            return;
//        }
//        detailViewController.isAnimating = YES;
        YXFrontCoverViewController *detailViewController = [[YXFrontCoverViewController alloc]init];
        [self.SelectNav pushViewController:detailViewController animated:YES];

    }else if (buttonIndex==0){
        //家族按钮
        CreateFamilyViewController * detailViewController = [[CreateFamilyViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.SelectNav pushViewController:detailViewController animated:YES];
    }else if (buttonIndex==2) {
        //我的自戏按钮
        CreateMyStoryViewController * detailViewController = [[CreateMyStoryViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.SelectNav pushViewController:detailViewController animated:YES];
    }else if (buttonIndex==3){
        //话题按钮
        CreateTopicViewController * detailViewController = [[CreateTopicViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.SelectNav pushViewController:detailViewController animated:YES];
    }
    

}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
