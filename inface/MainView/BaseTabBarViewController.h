//
//  BaseTabBarViewController.h
//  SuBangDai
//
//  Created by define on 14/12/16.
//  Copyright (c) 2014年 huangzengsong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsViewController.h"
#import "PolymerizationViewController.h"
//#import "MyCenterViewController.h"
#import "CreateViewController.h"
#import "CreateFamilyViewController.h"
#import "TheatreTypeViewController.h"
#import "CreateMyStoryViewController.h"
#import "CreateTopicViewController.h"

@interface BaseTabBarViewController : UITabBarController<UITabBarControllerDelegate,UIActionSheetDelegate>
{

    UINavigationController *navLogin2;

//    UINavigationController *SelectNav;
    
    UIImageView *imageViewBackground;
    
    UIButton *aButton1;
    UIButton *aButton2;
    UIButton *aButton3;
    UIButton *aButton4;
}

@property(strong,nonatomic)UINavigationController * SelectNav;
@property(strong)UINavigationController *nv2;


- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController;


@end
