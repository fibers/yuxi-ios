//
//  FamilyIDTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FamilyIDTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UILabel *UserDetailLabel;//详情

@property (strong, nonatomic) IBOutlet UIImageView *ArrowImage;//箭头
@end
