//
//  EditTextViewViewController.h
//  inface
//
//  Created by Mac on 15/4/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//修改家族简介
#import "BaseViewController.h"

@interface EditTextViewViewController : BaseViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (strong,nonatomic)NSString *familyid;
@property (strong,nonatomic) NSString *content;
@property (assign,nonatomic)BOOL isAnimating;
@end
