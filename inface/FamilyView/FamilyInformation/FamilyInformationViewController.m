//
//  FamilyInformationViewController.m
//  inface
//
//  Created by Mac on 15/4/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "FamilyInformationViewController.h"
#import "DDAddMemberToGroupAPI.h"
#import "MsgAddGroupConfirmAPI.h"
#import "GetGroupRoleName.h"
#import "YXMyCenterViewController.h"
@interface FamilyInformationViewController ()

@end

@implementation FamilyInformationViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];

//    if (self.isNeedShowchatting == YES) {
//        //发通知chattingviewcontroller根据session重新加载会话
//        [[ChatViewController shareInstance]showChattingContentForSession:self.chatSession];
//    }
}

//右上角按钮
-(void)returnBackr{
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
        //右上角按钮
        //是管理员的话
        
      mySheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"删除家族成员",@"删除并退出家族",@"举报",nil];
        mySheet.tag=3000;
        [mySheet showFromTabBar:self.tabBarController.tabBar];
    }else  if ([[self.dic objectForKey:@"ismember"]isEqualToString:@"1"]){
        
        //是成员
         mySheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"退出家族",@"举报",nil];
        mySheet.tag=6000;
        [mySheet showFromTabBar:self.tabBarController.tabBar];
    }else{
        
        //不是成员
        UIActionSheet* mySheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"举报",nil];
        mySheet.tag=5000;
        [mySheet showFromTabBar:self.tabBarController.tabBar];
    }


}


- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    [mySheet dismissWithClickedButtonIndex:0 animated:YES];

    if (actionSheet.tag==3000) {
        //是管理员的话
        if (buttonIndex==0) {

            //删除家族成员
            DeleteFamilyFriendViewController * detailViewController = [[DeleteFamilyFriendViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.familyid = self.familyid;
            detailViewController.fromType=@"FamilyInformationViewController";
            detailViewController.tableDataFriends= [NSMutableArray arrayWithArray: [self.dic objectForKey:@"members"]] ;
            
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
            
        }else if (buttonIndex==1){
            //删除并退出群
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"确定解散家族吗" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = 9999;
            [alert show];
        }else if (buttonIndex==2){
            //举报
            ReportViewController * detailViewController = [[ReportViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.reportid = self.familyid;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        
        }
    }else if (actionSheet.tag==4000) {
        
    
        //choose photo
        if (buttonIndex == 0) {
            
            [self startPhotoChooser];
            
        } else if (buttonIndex == 1) {
            //take photo.
            //指定使用照相机模式,可以指定使用相册／照片库
            if (!imagepicker) {
                imagepicker = [[UIImagePickerController alloc]init];
            }
            imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            //设置当拍照完或在相册选完照片后，是否跳到编辑模式进行图片剪裁。只有当showsCameraControls属性为true时才有效果
            imagepicker.allowsEditing = NO;
            //设置拍照时的下方的工具栏是否显示，如果需要自定义拍摄界面，则可把该工具栏隐藏
            imagepicker.showsCameraControls  = YES;
            //设置使用后置摄像头，可以使用前置摄像头
            imagepicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
            //设置闪光灯模式
            /*
             typedef NS_ENUM(NSInteger, UIImagePickerControllerCameraFlashMode) {
             UIImagePickerControllerCameraFlashModeOff  = -1,
             UIImagePickerControllerCameraFlashModeAuto = 0,
             UIImagePickerControllerCameraFlashModeOn   = 1
             };
             */
            imagepicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
            //设置相机支持的类型，拍照和录像
            imagepicker.mediaTypes = @[(NSString*)kUTTypeImage];
            //设置拍摄时屏幕的view的transform属性，可以实现旋转，缩放功能
            // imagepicker.cameraViewTransform = CGAffineTransformMakeRotation(M_PI*45/180);
            // imagepicker.cameraViewTransform = CGAffineTransformMakeScale(1.5, 1.5);
            //所有含有cameraXXX的属性都必须要sourceType是UIImagePickerControllerSourceTypeCamera时设置才有效果，否则会有异常
            //设置UIImagePickerController的代理
            imagepicker.delegate = self;
            [self presentViewController:imagepicker animated:YES completion:^{
                
            }];
            
        }

    }else if (actionSheet.tag==6000) {
        //是成员
        if (buttonIndex==0) {

            //退出群
            NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            NSString * user_id = [NSString stringWithFormat:@"user_%@",userid];
            NSString *group_id=[NSString stringWithFormat:@"group_%@",self.familyid];
            [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:@[user_id]];
            [[MyFamilyViewController shareInstance]getMyFamilies];
//            [[FriendsViewController shareInstance]deleteSession:group_id];
            [[NSNotificationCenter defaultCenter]postNotificationName:DELETE_SESSION object:group_id];
            [self.navigationController popToRootViewControllerAnimated:YES];
            NSString * title = [NSString stringWithFormat:@"您已退出[%@]",[self.dic objectForKey:@"name"]];
            UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:title message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alertView show];

        }else if (buttonIndex==1) {
        
            //举报
            ReportViewController * detailViewController = [[ReportViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.reportid = self.familyid;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
    }else{
    
        //不是成员
    
        if (buttonIndex==0) {
            //举报
            ReportViewController * detailViewController = [[ReportViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed=YES;
            detailViewController.reportid = self.familyid;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
            
        }
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIImage *RightImage=[UIImage imageNamed:@"morebtn"];
    
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 44)];
    [RightButton setImage:RightImage forState:UIControlStateNormal];
    [RightButton setImage:[UIImage imageNamed:@"morebtnselect"] forState:UIControlStateHighlighted];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"家族资料";
    aimgArray = [NSMutableArray array];
    personsArr = [NSMutableArray array];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    

    //获取家族信息
    [self GetFamilyInfo];
    //添加圆形进度条
    self.circleView = [[EVCircularProgressView alloc]initWithFrame:CGRectMake(36, 31, 28, 28)];
    self.circleView.hidden = YES;
    self.hasDelete = NO;
    //观察编辑动作的改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadArray:) name:@"FamilyInformationChanged" object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deleteSession:) name:DELETE_SESSION object:nil];
}

-(void)reloadArray:(NSNotification *)notification
{
    [self GetFamilyInfo];
    
}


//获取家族信息，存储家族成员信息
-(void)GetFamilyInfo
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    NSDictionary * params = @{@"uid":userid,@"familyid":self.familyid,@"token":@"110"};
    [HttpTool postWithPath:@"GetFamilyInfo" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue] == 1) {
            self.dic = [JSON objectForKey:@"familyinfo"];
            //家族成员数组
            aimgArray = [NSMutableArray arrayWithArray:[self.dic objectForKey:@"members"]];
            //存储家族成员信息
            [[GetGroupRoleName shareInstance]saveFamily:self.familyid membersArr:[self.dic objectForKey:@"members"]];
            [aimgArray addObject:@"+"];
            [aimgArray addObject:@"+"];//邀请qq好友
            personsArr = [NSMutableArray arrayWithArray:[self.dic objectForKey:@"members"]];
            [self.tableView reloadData];

            UIButton *aButton=[UIButton buttonWithType:UIButtonTypeCustom];
            aButton.frame=CGRectMake(0,ScreenHeight-64-50, ScreenWidth, 50);
            //    aButton.layer.cornerRadius = 15;
            //    aButton.clipsToBounds = YES;
            
            [aButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            aButton.titleLabel.font=[UIFont systemFontOfSize:20];
            
            [aButton setBackgroundColor:BLUECOLOR];
            
            if ([[self.dic objectForKey:@"ismember"]isEqualToString:@"1"]) {
                //是家族成员了
//                if (self.canChat == YES) {
//                    aButton.hidden = YES;
//                }else{
                     [aButton addTarget:self action:@selector(handleBtnChat:) forControlEvents:UIControlEventTouchUpInside];
                     [aButton setTitle:@"聊天" forState:UIControlStateNormal];
//                }
            } else {
                //不是家族成员
                if (self.isApplyToJoin) {
                    [aButton addTarget:self action:@selector(agreeJoinGroup) forControlEvents:UIControlEventTouchUpInside];
                    [aButton setTitle:@"同意" forState:UIControlStateNormal];

                }else{
                    [aButton addTarget:self action:@selector(handleBtnAdd:) forControlEvents:UIControlEventTouchUpInside];
                    [aButton setTitle:@"申请" forState:UIControlStateNormal];
                }
            }
            
            [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
            
            [self.view addSubview:aButton];
            

        }else{
        
        }
    } failure:^(NSError *error) {
        
        
    }];
    
}



//邀请好友事件,点击我在本家族的昵称
- (void)AddFriendPeople:(UIButton *)btn
{
    
    //添加群成员
    AddFamilyFriendViewController * detailViewController = [[AddFamilyFriendViewController alloc] init];
    detailViewController.hidesBottomBarWhenPushed=YES;
    detailViewController.fromType=@"FamilyInformationViewController";
    detailViewController.familyid = self.familyid;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    
}

//查看好友详情事件
- (void)FriendPeopleDetail:(UIButton *)btn
{
    NSInteger index=btn.tag-10000;
    //好友

    NSString *FriendID=[[aimgArray objectAtIndex:index] objectForKey:@"id"];
    YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
    [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 8;
}


#pragma mark 邀请QQ好友
-(void)applyQQFriends:(UIButton * )sender
{

    if (self.hasDelete == NO) {
        self.hasDelete = YES;
        for (int i = 0; i < personsArr.count; i++) {
            //
            UIButton * deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            float width = 25;
            float height = 25;
            int line = i /4;//行数
            int brank = i%4;//列数
            float widthBlank = (ScreenWidth - 50*4)/5.0;
            float heightBlank = 10;
            deleteBtn.frame = CGRectMake(widthBlank+35 +(50+widthBlank)*brank, 5 +80*line, width, height);
            [deleteBtn setImage:[UIImage imageNamed:@"deleteGroupsMember"] forState:UIControlStateNormal];
            [deleteBtn addTarget:self action:@selector(deleteGroupMember:) forControlEvents:UIControlEventTouchUpInside];
            deleteBtn.tag = 10000+i;
            NSDictionary * dic = [personsArr objectAtIndex:i];
            if (![[dic objectForKey:@"id"]isEqualToString:USERID]) {
                [self.deleteGroups.contentView addSubview:deleteBtn];
                
            }
            
        }
    }else{
        [self GetFamilyInfo];
        self.hasDelete = NO;
    }



    
}


-(void)deleteGroupMember:(UIButton*)sender
{
    NSDictionary * personDic = personsArr[sender.tag-10000];
    NSString * user_id = [NSString stringWithFormat:@"user_%@",[personDic objectForKey:@"id"]];
    NSString * group_id = [NSString stringWithFormat:@"group_%@",self.familyid];
    [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:@[user_id]];
    [self GetFamilyInfo];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0){
        static NSString *identifier=@"FamilyNameTableViewCell";
        FamilyNameTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyNameTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
 
        cell.UserImage.userInteractionEnabled=YES;
        
        cell.UserNameLabel.hidden=YES;
        NSURL* url = [NSURL URLWithString:[[self.dic objectForKey:@"portrait"] stringOfW100ImgUrl]];
        [cell.UserImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
        cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
        cell.UserImage.clipsToBounds=YES;
        cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;
        [cell.UserImage addSubview:self.circleView];
        NSString *isvipfamliy=[self.dic objectForKey:@"isvip"];
        if ([isvipfamliy isEqualToString:@"1"]) {
            UIImageView *vipimageView=[[UIImageView alloc]initWithFrame:CGRectMake(cell.UserImage.frame.origin.x-1.5, cell.UserImage.frame.origin.y - 1.5, 76, 76)];
            vipimageView.image=[UIImage imageNamed:@"centerVip"];
            vipimageView.userInteractionEnabled=YES;
            [cell addSubview:vipimageView];
        }
        
        [cell bringSubviewToFront:self.circleView];
        
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            cell.ArrowImage.hidden=YES;
        }
        self.aimageViewPhoto = cell.UserImage;
        self.familyCell = cell;
        return cell;
        
    }else if (indexPath.row==1){
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if ([[self.dic objectForKey:@"ismember"]isEqualToString:@"1"]) {
            //是家族成员了
            cell.UserNameLabel.text=@"我的昵称";
            cell.UserDetailLabel.text=[self.dic objectForKey:@"nickname"];
        }else{
            
            for (UIView *acellview in cell.contentView.subviews) {
                [acellview removeFromSuperview];
            }
        }


        
        return cell;
        
    }else if (indexPath.row==2) {
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        cell.ArrowImage.hidden=YES;
        cell.UserNameLabel.text=@"家族成员";
        NSInteger count=[[self.dic objectForKey:@"members"]count];
        cell.UserDetailLabel.text=[NSString stringWithFormat:@"%ld人",(long)count];
        
        return cell;
        
        
    } else  if (indexPath.row==3){
        static NSString *identifier=@"UITableViewCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if ([[self.dic objectForKey:@"ismember"]isEqualToString:@"1"]) {
            //是家族成员了
        
            for (UIView *acellview in cell.contentView.subviews) {
                [acellview removeFromSuperview];
            }
            
            
            //添加12个按钮
            for (int i=0; i<aimgArray.count; i++) {
                HZSFamilyButton *aButton=[HZSFamilyButton buttonWithType:UIButtonTypeCustom];
                aButton.tag=i+10000;
                float width=50;
                float height=70;
                
                int j=i/4;//第几行
                int k=i%4;//第几列
                float widthBlank=(ScreenWidth-50*4)/5.0;
                float heightBlank=10;
                aButton.frame=CGRectMake(widthBlank+(width+widthBlank)*k,heightBlank+(height+heightBlank)*j, width, height);
                aButton.imageView.layer.borderWidth = 0;
                aButton.imageView.layer.borderColor = BLUECOLOR.CGColor;
                aButton.imageView.layer.cornerRadius = 25;
                aButton.imageView.clipsToBounds = YES;
                [aButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                aButton.titleLabel.font=[UIFont systemFontOfSize:14];
                aButton.titleLabel.textAlignment=NSTextAlignmentCenter;
                [aButton setBackgroundColor:[UIColor clearColor]];
                [aButton setShowsTouchWhenHighlighted:YES];//按的时候设置高亮
                
                [cell.contentView addSubview:aButton];
                self.deleteGroups = cell;
                
                if (i==aimgArray.count-1) {
                    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
                    if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
                        //不是管理员的话
                        aButton.hidden=YES;
                    }
                    [aButton setImage:[UIImage imageNamed:@"addFriends"] forState:UIControlStateNormal];
                    [aButton setTitle:@"" forState:UIControlStateNormal];
                    [aButton addTarget:self action:@selector(AddFriendPeople:) forControlEvents:UIControlEventTouchUpInside];
                 
                    
                }else{
                    if (i == [aimgArray count]-2) {
                        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
                        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
                            //不是管理员的话
                            aButton.hidden=YES;
                        }
                        [aButton setImage:[UIImage imageNamed:@"deleteMembers"] forState:UIControlStateNormal];
                        [aButton addTarget:self action:@selector(applyQQFriends:) forControlEvents:UIControlEventTouchUpInside];
//                        [aButton setTitle:@"QQ好友" forState:UIControlStateNormal];
                        
                    }else{
                    NSString *portrait=[[aimgArray objectAtIndex:i] objectForKey:@"portrait"];
                        int isVip = [portrait isSignedUser];
                        if (isVip == 1) {
                            //如果是vip写手
                            UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(35, 35, 15, 15)];
                            
                            imageView.image = [UIImage imageNamed:@"vipWriter"];
                            
                            [aButton addSubview:imageView];
                        }else{
                            if (isVip == 2 || isVip == 3) {
                                UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(35, 35, 15, 15)];
                                
                                imageView.image = [UIImage imageNamed:@"maxOffical"];
                                
                                [aButton addSubview:imageView];
                            }
                        }
                    [aButton sd_setImageWithURL:[NSURL URLWithString:portrait] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
                    [aButton setTitle:[[aimgArray objectAtIndex:i] objectForKey:@"nickname"] forState:UIControlStateNormal];
                    [aButton addTarget:self action:@selector(FriendPeopleDetail:) forControlEvents:UIControlEventTouchUpInside];
                    }
            }
            }

        }
        
        
        return cell;
        
        
    }else  if (indexPath.row==4){
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }

        cell.UserNameLabel.text=@"家族名称";
        cell.UserDetailLabel.text=[self.dic objectForKey:@"name"];
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            cell.ArrowImage.hidden=YES;
        }
        return cell;

    }else  if (indexPath.row==5){
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.ArrowImage.hidden=YES;
        cell.UserNameLabel.text=@"家族ID";
        cell.UserDetailLabel.text=[self.dic objectForKey:@"id"];
        return cell;

    }else  if (indexPath.row==6){
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.ArrowImage.hidden=YES;
        cell.UserNameLabel.text=@"族长";
        cell.UserDetailLabel.text=[self.dic objectForKey:@"ownernickname"];
        return cell;

    }else{
        static NSString *identifier=@"FamilyIDTableViewCell";
        FamilyIDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyIDTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
    
        cell.UserNameLabel.text=@"家族简介";
        cell.UserDetailLabel.textAlignment=NSTextAlignmentLeft;
        NSString *FamilyDetail=[self.dic objectForKey:@"intro"];
        
        cell.UserDetailLabel.numberOfLines=0;
        cell.UserDetailLabel.text=FamilyDetail;
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            cell.ArrowImage.hidden=YES;
        }
        return cell;
    }
}


#pragma mark聊天,会话体不能重构
- (void)handleBtnChat:(UIButton *)btn
{
    
    NSString * familyName = [self.dic objectForKey:@"name"];
    NSString * group_id = [NSString stringWithFormat:@"group_%@",self.familyid];
#pragma mark 都从最原始的地方获取
    SessionEntity * sessionEntity = [[SessionModule sharedInstance]getSessionById:group_id];
    if (!sessionEntity) {
         sessionEntity = [[SessionEntity alloc]initWithSessionID:group_id SessionName:familyName type:SessionTypeSessionTypeGroup];
//        [[SessionModule sharedInstance]addToSessionModel:sessionEntity];
    }
    
    ChatViewController * detailViewController = [ChatViewController shareInstance];
    detailViewController.checkGroupId = self.familyid;
    detailViewController.isChatInFamily = YES;
    detailViewController.hidesBottomBarWhenPushed=YES;
    NSArray * viewControllers = self.navigationController.viewControllers;
    
    NSString *havechat=@"0";
    for (UIViewController * aViewController in viewControllers) {
        
        if ([aViewController isKindOfClass:[ChatViewController class]]) {
            havechat=@"1";
        }
        
    }
    
    if ([havechat isEqualToString:@"0"]) {
        detailViewController.hidesBottomBarWhenPushed=YES;
        [detailViewController showChattingContentForSession:sessionEntity];
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];

    } else {
        self.navigationController.navigationBar.hidden=NO;
    
        detailViewController.hidesBottomBarWhenPushed=YES;
        [detailViewController showChattingContentForSession:sessionEntity];
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController popToViewController:(ChatViewController*)detailViewController animated:YES];

    }
    
}

#pragma mark申请加入群
- (void)handleBtnAdd:(UIButton *)btn
{
    NSString *groupid=[NSString stringWithFormat:@"group_%@",self.familyid];
    [[AddGroupMemberModule sharedInstance]applyToJoinGroupsessionType:SessionTypeSessionTypeSingle groupId:groupid toUserIds:@[] msgId:@"0" ownerId:[self.dic objectForKey:@"ownerid"]];


    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"发送请求成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertview show];

}


#pragma mark 同意进群
-(void)agreeJoinGroup
{
    NSString *groupid=[NSString stringWithFormat:@"group_%@",self.familyid];
    NSArray * groupUsers = @[[RuntimeStatus instance].user.objID];
    DDAddMemberToGroupAPI * addGroupMember = [[DDAddMemberToGroupAPI alloc]init];
    NSArray * object = @[groupid,groupUsers];

    [addGroupMember requestWithObject:object Completion:^(id response, NSError *error) {
        if (error) {
            //同意失败
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"数据请求失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alert show];
            [[DDTcpClientManager instance]reconnect];
            return ;
        }
           //重新获取我的家族列表
    [[MyFamilyViewController shareInstance]agreeByFamilyLeader:self.familyid];
            [[DDDatabaseUtil instance]deleteAddFriendsMessageFromid:self.fromid groupid:self.familyid friendsType:@"3" result:@"-2" grouptype:@"1" haschange:@"1"];
        MsgAddGroupConfirmAPI * addGroupConfirm = [[MsgAddGroupConfirmAPI alloc]init];
        NSArray * arrary = @[@(0),self.fromid,self.familyid,@(0),@(SessionTypeSessionTypeGroup),@(1)];
        [addGroupConfirm requestWithObject:arrary Completion:^(id response, NSError *error) {
            
        }];

    }];
#pragma mark 同意进群，将我的家族列表刷新
    


}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0){

        
        return 90;
        
    }else if (indexPath.row==1){
    
        if ([[self.dic objectForKey:@"ismember"]isEqualToString:@"1"]) {
            //是家族成员了
            return 44;
        } else {
            //不是家族成员
            
            return 0;
        }

        
    }else if (indexPath.row==3){
        
        if ([[self.dic objectForKey:@"ismember"]isEqualToString:@"1"]) {
            //是家族成员了

            NSInteger j=aimgArray.count/4;//第几行
            NSInteger k=aimgArray.count%4;//第几列
            if (k==0) {
                return j*80+10;
            } else {
                return (j+1)*80+10;
            }
        } else {
            //不是家族成员
        
            return 0;
        }
        

    }else if (indexPath.row==7) {

        NSString * intro=[self.dic objectForKey:@"intro"];
        float height=[HZSInstances labelAutoCalculateRectWith:intro FontSize:14.0 MaxSize:CGSizeMake(ScreenWidth-140, 2000)].height;
        if (height>30) {
            return height+14;
        } else {
            return 44;
        }

    }else{
        
        return 44;
    }
    
    
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器

    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0){
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            return;
        }
        
        BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        
        //如果支持相机，那么多个选项
        if (isCameraSupport) {
            UIActionSheet* mySheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"挑照片" otherButtonTitles:@"现在拍", nil];
            mySheet.tag=4000;
            [mySheet showInView:self.view];
            
        }
        else {
            [self startPhotoChooser];
        }

    }else if (indexPath.row==1){
          //我在家族的昵称
        ModifyFamilyMyNicknameViewController * detailViewController = [[ModifyFamilyMyNicknameViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        detailViewController.content=[self.dic objectForKey:@"nickname"];
        detailViewController.familyid = self.familyid;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];

    }else if (indexPath.row==4) {
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            return;
        }
        //家族名称
        EditTextfieldViewController * detailViewController = [[EditTextfieldViewController alloc] init];
        detailViewController.familyid = self.familyid;
        detailViewController.content=[self.dic objectForKey:@"name"];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    } else if (indexPath.row==7) {
        NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if (![[self.dic objectForKey:@"ownerid"]isEqualToString:userid]) {
            //不是管理员的话
            return;
        }
        //修改简介
        EditTextViewViewController * detailViewController = [[EditTextViewViewController alloc] init];
        detailViewController.familyid = self.familyid;
        detailViewController.content=[self.dic objectForKey:@"intro"];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAnimating) {
            return;
        }
        detailViewController.isAnimating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
   

    
    
}


- (void)uzysAssetsPickerController:(UzysAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    
    
    if([[assets[0] valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) //Photo
    {

        [assets enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            ALAsset *representation = obj;
            
            UIImage * original = [UIImage imageWithCGImage:representation.defaultRepresentation.fullResolutionImage
                                                scale:representation.defaultRepresentation.scale
                                          orientation:(UIImageOrientation)representation.defaultRepresentation.orientation];
            self.aimageViewPhoto.image = original;

            
            CGSize  imgSize = CGSizeMake(2210, 1280);
            
            original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
            //更新家族信息
            [self modifyFamilyImage:original];
            
        }];

    }
    else //Video
    {
        
        
    }
    
}


#pragma-mark 上传家族头像
-(void)modifyFamilyImage:(UIImage *)image
{
//    [SVProgressHUD showWithStatus:@"上传中" maskType:SVProgressHUDMaskTypeBlack];
    //此处需要换成又拍的云存储
    UpYun *_upYun = [[UpYun alloc]init];
    [_upYun uploadImage:image];
    [self.circleView setHidden:NO];
    
    self.circleView.center = CGPointMake(self.aimageViewPhoto.center.x , self.aimageViewPhoto.center.y );
    
    [self.circleView setProgress:0.0 animated:YES];
    self.circleView.alpha = 1;
    [self.circleView commonInit];
//    [self.aimageViewPhoto addSubview:self.circleView];
//    [self.aimageViewPhoto bringSubviewToFront:self.circleView];
    [self.familyCell addSubview:self.circleView];
    [self.familyCell bringSubviewToFront:self.circleView];
    //上传图片成功
    WEAKSELF
    _upYun.successBlocker = ^(id data){
        if (!weakSelf) {
            return ;
        }
        NSString        *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
        imgUrl  =   [NSString stringWithFormat:@"%@%@", IMAGE_SERVER_BASE_URL, imgUrl];
        [weakSelf postPersonImage:imgUrl];
        [weakSelf.circleView setHidden:YES];

//        [SVProgressHUD dismiss];
    };
    _upYun.failBlocker = ^(NSError * error)
    {
        //上传又拍失败的操作
        [weakSelf.circleView setHidden:YES];

//        [SVProgressHUD dismiss];
        [SVProgressHUD showErrorWithStatus:@"上传失败" duration:2.0];
    };
    _upYun.progressBlocker = ^(CGFloat percent,long long requestDidSendBytes)
    {
        [ weakSelf.circleView setProgress:percent animated:YES];
    };
}

#pragma -发送请求
#pragma mark 更新个人头像
-(void)postPersonImage :(NSString *)imgUrl
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary *params = @{@"uid":userid,@"portrait":imgUrl,@"familyid":self.familyid,@"token":@"110"};
    [HttpTool postWithPath:@"ModifyFamilyInfo" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
//            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:@"修改成功"delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//            [alertview show];
            
            //观察编辑动作的改变
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadArray:) name:@"FamilyInformationChanged" object:nil];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];
}


#pragma mark actionSheet Delegate

- (void) startPhotoChooser
{
    UzysAssetsPickerController *picker = [[UzysAssetsPickerController alloc] init];
    picker.delegate = self;
    
    picker.maximumNumberOfSelectionPhoto = 1;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}


#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==9999) {
        //退出家族操作
        if (buttonIndex== 1) {
            
            NSMutableArray *selectArr=[self.dic objectForKey:@"members"];
            
            NSMutableArray *deleteArr=[[NSMutableArray alloc]init];
            for (int i=0; i<selectArr.count; i++) {
                NSString * selectid=[[selectArr objectAtIndex:i]objectForKey:@"id"];
                NSString * select_id = [NSString stringWithFormat:@"user_%@",selectid];

                [deleteArr addObject:select_id];
            }
            NSString *group_id=[NSString stringWithFormat:@"group_%@",self.familyid];
            [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:deleteArr];
            

            NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
            NSDictionary * params = @{@"uid":userid,@"familyid":self.familyid,@"token":@"110"};
        
           [HttpTool postWithPath:@"ExitFamily" params:params success:^(id JSON) {
               //删除成功
               if ([[JSON objectForKey:@"result"]integerValue]==1) {
                   UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"解散家族成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                   [alertview show];
#pragma mark 删除表格里的群
//                   [[FriendsViewController shareInstance]deleteSession:group_id];
                   [[NSNotificationCenter defaultCenter]postNotificationName:DELETE_SESSION object:group_id];
                   [[MyFamilyViewController shareInstance]getMyFamilies];
    
                   [self.navigationController popToRootViewControllerAnimated:YES];

               }else{
                   [[NSNotificationCenter defaultCenter]postNotificationName:DELETE_SESSION object:group_id];
                   [[MyFamilyViewController shareInstance]getMyFamilies];
                   
                   [self.navigationController popToRootViewControllerAnimated:YES];
                   UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                   [alertview show];
                   
               }


           } failure:^(NSError *error) {
               
               
           }];
        }
    }
}

#pragma take photos image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
    NSString* type = [info objectForKey:UIImagePickerControllerMediaType];
    //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
    if ([type isEqualToString:(NSString*)kUTTypeImage]&&picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
        //获取照片的原图
        UIImage* original = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        self.aimageViewPhoto.image = original;
        
        CGSize  imgSize = CGSizeMake(2210, 1280);
        
        original = [HZSInstances compressImage:original toSize:imgSize withCompressionQuality:0.0];
        
        [self modifyFamilyImage:original];

        //如果是拍照的照片，则需要手动保存到本地，系统不会自动保存拍照成功后的照片
        UIImageWriteToSavedPhotosAlbum(original, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
    }else{
        
    }
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//取消照相机的回调
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //模态方式退出uiimagepickercontroller
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        
    }];
}
//保存照片成功后的回调
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo{
    
}
//add for photo functions end





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
