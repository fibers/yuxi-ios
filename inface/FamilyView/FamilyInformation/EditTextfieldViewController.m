//
//  EditTextfieldViewController.m
//  inface
//
//  Created by Mac on 15/4/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "EditTextfieldViewController.h"

@interface EditTextfieldViewController ()

@end

@implementation EditTextfieldViewController

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr
{
    if ([self.chatInputTextField.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入名称" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }

    if (self.chatInputTextField.text.length > 12)
    {
        self.chatInputTextField.text = [self.chatInputTextField.text substringToIndex:12];
    }

    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary * dic = @{@"uid":userid,@"familyid":self.familyid,@"familyname":self.chatInputTextField.text,@"token":@"110"};
    
    [HttpTool postWithPath:@"ModifyFamilyInfo" params:dic success:^(id JSON) {
        //修改成功
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:@"修改成功"delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FamilyInformationChanged" object:self userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
    } failure:^(NSError *error) {
        
    }];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"修改家族名称";
    
    self.chatInputTextField.text=self.content;
}





- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==self.chatInputTextField) {
        
        if (textField.text.length > 12)
        {
            textField.text = [textField.text substringToIndex:12];
        }
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
