//
//  DeleteFamilyFriendViewController.m
//  inface
//
//  Created by Mac on 15/5/21.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "DeleteFamilyFriendViewController.h"
#import "YXMyCenterViewController.h"
@interface DeleteFamilyFriendViewController ()

@end

@implementation DeleteFamilyFriendViewController

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    if (selectArr.count==0) {
        UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:@"请选择要删除的群成员" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    
    NSMutableArray *deleteArr=[[NSMutableArray alloc]init];
    for (int i=0; i<selectArr.count; i++) {
        NSDictionary * dic = [selectArr objectAtIndex:i];
        int indexValue  = [[[dic allValues]objectAtIndex:0]integerValue];
        NSString * selectid=[[self.tableDataFriends objectAtIndex:indexValue]objectForKey:@"id"];
        NSString * select_id = [NSString stringWithFormat:@"user_%@",selectid];

        [deleteArr addObject:select_id];
    }
    NSString *group_id=[NSString stringWithFormat:@"group_%@",self.familyid];
    [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:deleteArr];
    NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    if ([self.fromType isEqualToString:@"FamilyInformationViewController"]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FamilyInformationChanged" object:self userInfo:nil];
        for (int i = 0; i < [deleteArr count]; i++) {
            [[DDDatabaseUtil instance]clearUpAddMessages:[deleteArr objectAtIndex:i] toid:userid groupid:self.familyid type:@"1"];
        }
    } else {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StoryInformationChanged" object:self userInfo:nil];
        for (int i = 0; i < [deleteArr count]; i++) {
            [[DDDatabaseUtil instance]clearUpAddMessages:[deleteArr objectAtIndex:i] toid:userid groupid:self.familyid type:@"2"];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
    
    UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:@"删除成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"删除" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    if ([self.fromType isEqualToString:@"FamilyInformationViewController"]) {
    self.navigationItem.title=@"删除家族成员";
    }else{
        self.navigationItem.title=@"删除剧群成员";

    }
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView=[[UIView alloc]init];
    selectArr=[[NSMutableArray alloc]init];
    self.currentFriends = [NSMutableArray array];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString * userID = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    self.currentFriends = [self.tableDataFriends  mutableCopy];
    for (int i = 0; i < [self.tableDataFriends count]; i++) {
        NSDictionary * personDic = [self.tableDataFriends objectAtIndex:i];
        NSString * user_id= [personDic objectForKey:@"id"];
        if ([user_id isEqualToString:userID]) {
            [self.tableDataFriends removeObjectAtIndex:i];
        }
    }
    return  [self.tableDataFriends count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"FriendsSelectTableViewCell";
    FriendsSelectTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"FriendsSelectTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    cell.UserNameLabel.text=[[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"nickname"];

    NSString *urlString=[[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"portrait"];
    int isVip = [urlString isSignedUser];
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
    switch (isVip) {
        case 0:
            cell.vipWriterImage.hidden = YES;
            break;
        case 1:
            cell.vipWriterImage.hidden = NO;
            
            cell.vipWriterImage.image = image;
            
            break;
        case 2:
            cell.vipWriterImage.hidden = NO;
            
            cell.vipWriterImage.image = officalImage;
            
            break;
        case 3:
            cell.vipWriterImage.hidden = NO;
            
            cell.vipWriterImage.image = officalImage;
            
            break;
        default:
            break;
    }
    
    [cell.UserImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
    cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
    cell.UserImage.clipsToBounds=YES;
    cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;
    BOOL isexit = NO;
    for (NSDictionary * dic in selectArr) {
        NSString * index_path =[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        if ([[dic allKeys]containsObject:index_path]) {
            isexit = YES;
        }
    }
    if (isexit == YES)
    {
        [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang-1"] forState:UIControlStateNormal];
    }
    
    cell.SelectButton.tag=10000+indexPath.row;
    [cell.SelectButton addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}
#pragma mark 复选逻辑
-(void)SelectButton:(UIButton *)sender{
    UIButton *btn = (UIButton *)sender;
    NSInteger index=btn.tag-10000;
    NSString * index_key = [NSString stringWithFormat:@"%ld",(long)index];
    BOOL  isExit = NO;
    for (int i = 0; i < [selectArr count]; i++) {
        NSDictionary * dic = [selectArr objectAtIndex:i];
        int indexValue = [[[dic allValues]objectAtIndex:0]integerValue];
        if (index == indexValue) {
            //已经被选中
            isExit = YES;
            [selectArr removeObjectAtIndex:i];
        }
    }
    if (isExit == NO) {
        //没被选过
        NSDictionary * dic = @{index_key:index_key};
        [selectArr addObject:dic];
    }
    
    [self.tableView reloadData];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 70;
    
}


//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    NSDictionary *friendArr=[self.tableDataFriends objectAtIndex:indexPath.row];
    NSNumber * fid = [friendArr objectForKey:@"id"];
    NSString * FriendID = [NSString stringWithFormat:@"%@",fid];

    YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
    [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:FriendID];
    detailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    //取消选中的行
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
