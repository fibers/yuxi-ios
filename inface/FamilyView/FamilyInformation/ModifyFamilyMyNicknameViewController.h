//
//  ModifyFamilyMyNicknameViewController.h
//  inface
//
//  Created by Mac on 15/5/21.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface ModifyFamilyMyNicknameViewController : BaseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property (strong,nonatomic) NSString *familyid;
@property (strong,nonatomic) NSString *content;
@property (assign,nonatomic) BOOL  isAnimating;
@end
