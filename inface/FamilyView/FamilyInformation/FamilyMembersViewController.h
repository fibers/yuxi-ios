//
//  FamilyMembersViewController.h
//  inface
//
//  Created by appleone on 15/7/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "FriendsSelectTableViewCell.h"
@interface FamilyMembersViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    NSMutableArray *selectArr;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableDataFriends;
@property(nonatomic ,strong)NSString *familyid;
@property(nonatomic ,strong)NSString *fromType;
@property(nonatomic,assign)BOOL       isAnimating;
@end
