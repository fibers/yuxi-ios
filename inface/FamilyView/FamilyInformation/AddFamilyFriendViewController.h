//
//  AddFamilyFriendViewController.h
//  inface
//
//  Created by Mac on 15/5/22.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "FriendsSelectTableViewCell.h"
#import "AddGroupMemberModule.h"
@interface AddFamilyFriendViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{

    NSMutableArray *selectArr;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableDataFriends;
@property(nonatomic ,strong)NSString *familyid;
@property(nonatomic ,strong)NSString *fromType;
@property(assign,nonatomic)BOOL       isAnimating;
@end
