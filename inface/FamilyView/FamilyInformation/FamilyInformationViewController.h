//
//  FamilyInformationViewController.h
//  inface
//
//  Created by Mac on 15/4/15.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FamilyNameTableViewCell.h"
#import "FamilyIDTableViewCell.h"
#import "HZSTitleButton.h"
#import "UzysAssetsPickerController.h"
#import "EditTextfieldViewController.h"
#import "EditTextViewViewController.h"
#import "ReportViewController.h"
#import "DeleteFamilyFriendViewController.h"
#import "ModifyFamilyMyNicknameViewController.h"
#import "AddGroupMemberModule.h"
#import "ChatViewController.h"
#import "SessionEntity.h"
#import "ChangeGroupMemberModel.h"
#import "AddFamilyFriendViewController.h"
#import "EVCircularProgressView.h"
@interface FamilyInformationViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UzysAssetsPickerControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIAlertViewDelegate>
{
    UIImagePickerController    *imagepicker;
    NSMutableArray *aimgArray;//家族成员数组
    UIActionSheet * mySheet;
    NSMutableArray * personsArr;
    
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;
@property(nonatomic ,strong)NSDictionary * dic;
@property(nonatomic ,strong)NSString *familyid;
@property(nonatomic,strong) NSString * applyType;
@property(nonatomic,assign)BOOL  isApplyToJoin;//是否被邀请加入
@property(nonatomic,strong)NSString * fromid;//发起人的id；
@property (assign,nonatomic)   BOOL isNeedShowchatting;//返回的时候是否需要根据session来加载会话
@property (strong,nonatomic)  SessionEntity * chatSession;
@property (assign,nonatomic)   BOOL canChat;//是否能跳聊天界面
@property (strong,nonatomic)  EVCircularProgressView * circleView;
@property (strong,nonatomic)  UIImageView * aimageViewPhoto;
@property (strong,nonatomic)  FamilyNameTableViewCell * familyCell;
@property (strong,nonatomic)  UITableViewCell * deleteGroups;
@property(nonatomic,assign)   BOOL  hasDelete;
@property(nonatomic,assign)   BOOL  isAnimating;

@end
