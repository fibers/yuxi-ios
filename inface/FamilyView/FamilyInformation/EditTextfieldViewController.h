//
//  EditTextfieldViewController.h
//  inface
//
//  Created by Mac on 15/4/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

//修改家族名称
#import "BaseViewController.h"

@interface EditTextfieldViewController : BaseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property (strong,nonatomic) NSString *familyid;
@property (strong,nonatomic) NSString *content;
@property (nonatomic,assign) BOOL      isAnimating;
@end
