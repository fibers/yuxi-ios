//
//  ReportTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/5/8.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;//名字
@property (strong, nonatomic) IBOutlet UIButton *SelectButton;//选择按钮
@end
