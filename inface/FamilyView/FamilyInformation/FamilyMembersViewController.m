//
//  FamilyMembersViewController.m
//  inface
//
//  Created by appleone on 15/7/2.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "FamilyMembersViewController.h"
#import "GetGroupRoleName.h"
@interface FamilyMembersViewController ()

@end

@implementation FamilyMembersViewController



//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}



//右上角按钮
-(void)returnBackr{
    
    if (selectArr.count==0) {
        UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:@"请选择要@的成员" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    NSMutableArray *deleteArr=[[NSMutableArray alloc]init];
    NSMutableString * messageString = [NSMutableString string];
    for (int i=0; i<selectArr.count; i++) {
        NSDictionary * dic = [selectArr objectAtIndex:i];
        int indexValue  = [[[dic allValues]objectAtIndex:0]integerValue];
        NSDictionary * personDic = [self.tableDataFriends objectAtIndex:indexValue];
        NSString * selectId = [personDic objectForKey:@"id"];
        NSString * user_id = [NSString stringWithFormat:@"user_%@",selectId];
        NSString * nickname = [personDic objectForKey:@"nickname"];
        NSString * nameStr;
        if (i == 0)
        {
            nameStr = [NSString stringWithFormat:@"%@ ",nickname];
        }
        else
        {
            if(i < [selectArr count]-1 )
            {
                nameStr = [NSString stringWithFormat:@"@%@,",nickname];
                
            }else
            {
                nameStr = [NSString stringWithFormat:@"@%@ ",nickname];
            }
 
        }
        [messageString appendString:nameStr];
        [deleteArr addObject:user_id];
    }
    if (messageString) {
        [ChatViewController shareInstance].hasSelectGroupMembers = YES;
        [[ChatViewController shareInstance]setTextView:messageString];
        
    }
    [ChatViewController shareInstance].selectPersons = deleteArr;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super  viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"添加" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"选择家族成员";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    [self.navigationController setNavigationBarHidden:NO];
    selectArr=[[NSMutableArray alloc]init];
    //获取好友列表
    [self getFamilyMembers];
}



#pragma 获取家族成员列表
-(void)getFamilyMembers
{
    NSArray * members = [[GetGroupRoleName shareInstance]familyMembers:self.familyid];
    NSMutableArray * arr = [NSMutableArray arrayWithArray:members];
    for (int i = 0; i < [arr count]; i++) {
        NSDictionary * dic = [arr objectAtIndex:i];
        NSString * strid = [dic objectForKey:@"id"];
        NSString * userid = [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
        if ([strid isEqualToString:userid ]) {
            [arr removeObjectAtIndex:i];
        }
    }
    self.tableDataFriends = [arr mutableCopy];
    [self.tableView reloadData];

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.tableDataFriends.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"FriendsSelectTableViewCell";
    FriendsSelectTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"FriendsSelectTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    cell.UserNameLabel.text=[[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"nickname"];
    
    NSString *urlString=[[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"portrait"];
    
    int isVip = [urlString isSignedUser];
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
    switch (isVip) {
        case 0:
            cell.vipWriterImage.hidden = YES;
            break;
        case 1:
            cell.vipWriterImage.hidden = NO;
            
            cell.vipWriterImage.image = image;
            
            break;
        case 2:
            cell.vipWriterImage.hidden = NO;
            
            cell.vipWriterImage.image = officalImage;
            
            break;
        case 3:
            cell.vipWriterImage.hidden = NO;
            
            cell.vipWriterImage.image = officalImage;
            
            break;
        default:
            break;
    }
    
    [cell.UserImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"] completed:nil];
    cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
    cell.UserImage.clipsToBounds=YES;
    cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;
    BOOL isexit = NO;
    for (NSDictionary * dic in selectArr) {
        NSString * index_path =[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        if ([[dic allKeys]containsObject:index_path]) {
            isexit = YES;
        }
    }
    if (isexit == YES)
    {
        [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang-1"] forState:UIControlStateNormal];
    }
    
    
    cell.SelectButton.tag=10000+indexPath.row;
    [cell.SelectButton addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 70;
    
}


//选择按钮
-(void)SelectButton:(UIButton *)sender{
    UIButton *btn = (UIButton *)sender;
    NSInteger index=btn.tag-10000;
    NSString * index_key = [NSString stringWithFormat:@"%ld",(long)index];
    BOOL  isExit = NO;
    for (int i = 0; i < [selectArr count]; i++) {
        NSDictionary * dic = [selectArr objectAtIndex:i];
        int indexValue = [[[dic allValues]objectAtIndex:0]integerValue];
        if (index == indexValue) {
            //已经被选中
            isExit = YES;
            [selectArr removeObjectAtIndex:i];
        }
    }
    if (isExit == NO) {
        //没被选过
        NSDictionary * dic = @{index_key:index_key};
        [selectArr addObject:dic];
    }
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
