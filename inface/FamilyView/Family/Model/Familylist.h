//
//  Familylist.h
//  inface
//
//  Created by 邢程 on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Familylist : NSObject

@property (nonatomic, copy) NSString *number;

@property (nonatomic, copy) NSString *portrait;

@property (nonatomic, copy) NSString *ismember;

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *creatorid;

@property (nonatomic, copy) NSString *intro;

@property (nonatomic, copy) NSString *creatorname;

@property (nonatomic, copy) NSString *isvip;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *creattime;

@end
