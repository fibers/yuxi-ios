//
//  FamilyModel.m
//  inface
//
//  Created by 邢程 on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "FamilyModel.h"
#import "Familylist.h"
@implementation FamilyModel


+ (NSDictionary *)objectClassInArray{
    return @{@"familylist" : [Familylist class]};
}
@end

