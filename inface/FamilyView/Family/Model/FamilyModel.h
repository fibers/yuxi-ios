//
//  FamilyModel.h
//  inface
//
//  Created by 邢程 on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FamilyModel : NSObject

@property (nonatomic, copy) NSString *result;

@property (nonatomic, copy) NSString *position;

@property (nonatomic, strong) NSMutableArray *familylist;

@end


