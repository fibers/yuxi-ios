//
//  YXFamilyTableViewCell.m
//  inface
//
//  Created by 邢程 on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXFamilyListTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "NSString+Extention.h"
#import "NSString+YXExtention.h"
@interface YXFamilyListTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *headerImgV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *familyAdminL;
@property (weak, nonatomic) IBOutlet UILabel *infoL;
@property (weak, nonatomic) IBOutlet UILabel *peopleCountL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *peopleCountCW;

@end
@implementation YXFamilyListTableViewCell
-(void)setContentWith:(Familylist *)familyList{
    [self.headerImgV sd_setImageWithURL:[NSURL URLWithString:[familyList.portrait stringOfW100ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    [self.titleL setText:familyList.name];
    [self.familyAdminL setText:[NSString stringWithFormat:@"族长:%@",familyList.creatorname]];
    [self.infoL setText:familyList.intro];
    NSString * peopleCountStr = [NSString stringWithFormat:@"人数:%@",familyList.number];
    [self.peopleCountL setText:peopleCountStr];
   CGSize size =  [peopleCountStr sizeWithfont:[UIFont systemFontOfSize:15] MaxX:160];
    self.peopleCountCW.constant = size.width;
}
- (void)awakeFromNib {
    self.headerImgV.layer.cornerRadius = self.headerImgV.frame.size.width*0.5;
    [self.headerImgV setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
