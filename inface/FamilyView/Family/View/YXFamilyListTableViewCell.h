//
//  YXFamilyTableViewCell.h
//  inface
//
//  Created by 邢程 on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Familylist.h"
@interface YXFamilyListTableViewCell : UITableViewCell
-(void)setContentWith:(Familylist *)familyList;
@end
