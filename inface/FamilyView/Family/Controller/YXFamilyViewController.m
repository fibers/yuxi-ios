//
//  YXFamilyViewController.m
//  inface
//
//  Created by 邢程 on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXFamilyViewController.h"
#import "LjjUISegmentedControl.h"
#import "Menu.h"
#import "Familytype.h"
#import "FamilyModel.h"
#import "YXFamilyListTableViewCell.h"
#import "SearchFamilyViewController.h"
#import "MBProgressHUD+YXStyle.h"
#import <SDWebImage/SDImageCache.h>
#import "UIScrollView+MJRefresh.h"
@interface YXFamilyViewController ()<LjjUISegmentedControlDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)Menu * menu;
@property(nonatomic,strong)UIScrollView * scrollV;
@property(nonatomic,strong)NSMutableArray * tableViewArrM;
@property(nonatomic,strong)NSMutableDictionary * contentDicM;
@end

@implementation YXFamilyViewController
-(instancetype)init{
    if (self = [super init]) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        [self.navigationItem setTitle:@"家族"];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"zs_sousuo"] style:UIBarButtonItemStylePlain target:self action:@selector(searchItemClicked)];
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"zs_fanhui"] style:UIBarButtonItemStylePlain target:self action:@selector(backReturn)];
    };
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setHeaderView];
    [self initTableViews];
}

-(void)backReturn
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
/**
 *  顶部分组view
 */
-(void)setHeaderView{
    LjjUISegmentedControl *headerV = [[LjjUISegmentedControl alloc]initWithFrame:CGRectMake(35, 0, ScreenWidth-70, 35)];
    headerV.delegate = self;
    headerV.buttomViewH = 124;
    [headerV setTitleFont:[UIFont systemFontOfSize:14]];
    headerV.LJBackGroundColor = [UIColor whiteColor];
    NSMutableArray * arr = [NSMutableArray array];
    for( Familytype * type in self.menu.familytype){
        [arr addObject:type.typename];
    }
    [headerV AddSegumentArray:arr];
    [self.view addSubview:headerV];
    
}
-(void)searchItemClicked{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
   SearchFamilyViewController * vc =  [[SearchFamilyViewController alloc]init];
    if (vc.isAnimating) {
        return;
    }
    vc.isAnimating = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)initTableViews{
    for (int i = 0 ;i<[self.menu.familytype count]; i++) {
        UITableView * tableView= [[UITableView alloc]initWithFrame:CGRectMake(ScreenWidth * i, 0, ScreenWidth, self.scrollV.frame.size.height) style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.tag = i;
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.scrollV addSubview:tableView];
        WS(weakSelf);
        [tableView addFooterWithCallback:^{
            [weakSelf loadMoreDataWithTableViewIndex:i];
        }];
        [tableView footerBeginRefreshing];
        [self.tableViewArrM addObject:tableView];
    }
    [self.view addSubview:self.scrollV];

}

-(void)loadMoreDataWithTableViewIndex:(NSInteger)index{
    
    Familytype *type = [self.menu.familytype objectAtIndex:index];
    FamilyModel * model =[self.contentDicM objectForKey:[NSString stringWithFormat:@"%ld",(long)index]];
    NSString * dataIndex = model==nil?@"0":[NSString stringWithFormat:@"%ld",(unsigned long)[model.familylist count]];
    [self initDataWithTableViewIndex:index Type:type.typeid andIndex:dataIndex];
    
}
#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger index = tableView.tag;
    FamilyModel * model =[self.contentDicM objectForKey:[NSString stringWithFormat:@"%ld",(long)index]];
    return model == nil?0:[model.familylist count];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 82;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index                = tableView.tag;
    YXFamilyListTableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"YXFamilyListTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"YXFamilyListTableViewCell" owner:self options:nil]lastObject];
    }
    
     FamilyModel * model =[self.contentDicM objectForKey:[NSString stringWithFormat:@"%ld",(long)index]];
    [cell setContentWith:[model.familylist objectAtIndex:indexPath.row]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger tag = tableView.tag;
    //所有
    FamilyInformationViewController * detailViewController = [[FamilyInformationViewController alloc] init];
    FamilyModel * model = [self.contentDicM objectForKey:[NSString stringWithFormat:@"%ld",(long)tag]];
    Familylist * family = [model.familylist objectAtIndex:indexPath.row];
    detailViewController.familyid = family.id;
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}
/**
 *  网络加载数据
 *
 *  @param index  tableview 的数组索引
 *  @param type   分类type
 *  @param index2 数据索引
 */
-(void)initDataWithTableViewIndex:(NSInteger )index Type:(NSString*)type andIndex:(NSString *)index2{
    UITableView * tableView = [self.tableViewArrM objectAtIndex:index];
    [MBProgressHUD showYXLoadingMiniViewAddToView:tableView animated:YES];
    [HttpTool postWithPath:@"GetFamilyListByType" params:@{@"uid":USERID,@"type":type,@"index":index2,@"count":@"40"} success:^(id JSON)
    {
       FamilyModel *familyModel = [FamilyModel objectWithKeyValues:JSON];
       if ([index2 intValue]==0) {
           [self.contentDicM setObject:familyModel forKey:[NSString stringWithFormat:@"%ld",(long)index]];
       }else{
           FamilyModel * model =   [self.contentDicM objectForKey:[NSString stringWithFormat:@"%ld",(long)index]];
           [model.familylist addObjectsFromArray:familyModel.familylist];
           [self.contentDicM setObject:model forKey:[NSString stringWithFormat:@"%ld",(long)index]];
       }
       
       
       [tableView reloadData];
       [tableView footerEndRefreshing];
       [MBProgressHUD hideHUDForView:tableView animated:YES];
       
   } failure:^(NSError *error) {
       [MBProgressHUD showYXHudShowTextAddToView:self.view Title:@"网络请求失败！" andHideTime:1.5];
       
   }];
}

-(void)uisegumentSelectionChange:(NSInteger)selection{
    [self.scrollV setContentOffset:CGPointMake(ScreenWidth*selection, 0)];
}
-(Menu *)menu{
    if (!_menu) {
       _menu =  [NSKeyedUnarchiver unarchiveObjectWithFile:Menu_Full_File];
    }
    return _menu;
}
-(UIScrollView *)scrollV{
    if (!_scrollV) {
        _scrollV = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 35, ScreenWidth, ScreenHeight - 64-35)];
        [_scrollV setContentSize:CGSizeMake(ScreenWidth * [self.menu.familytype count], ScreenHeight - 64-35)];
        [_scrollV setPagingEnabled:YES];
        [_scrollV setShowsVerticalScrollIndicator:YES];
        [_scrollV setShowsHorizontalScrollIndicator:NO];
        [_scrollV setScrollEnabled:NO];
    }
    return _scrollV;
}
-(NSMutableDictionary *)contentDicM{
    if (!_contentDicM) {
        _contentDicM = [NSMutableDictionary dictionary];
    }
    return _contentDicM;

}
-(NSMutableArray *)tableViewArrM{
    if (!_tableViewArrM) {
        _tableViewArrM = [NSMutableArray array];
    }
    return _tableViewArrM;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
//    [[SDImageCache sharedImageCache]clearMemory];
}

@end
