//
//  FamilyAllTableViewCell.h
//  inface
//
//  Created by Mac on 15/4/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FamilyAllTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *UserImage;//头像

@property (strong, nonatomic) IBOutlet UILabel *TitleLabel;//标题

@end
