//
//  FamilyAllTableViewCell.m
//  inface
//
//  Created by Mac on 15/4/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "FamilyAllTableViewCell.h"

@implementation FamilyAllTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.TitleLabel.textColor=BLACK_CONTENT_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
