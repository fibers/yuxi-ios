//
//  SearchFamilyViewController.h
//  inface
//
//  Created by Mac on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "FamilyAllTableViewCell.h"
@interface SearchFamilyViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableDataFriends;

@property (nonatomic ,retain) UITextField * SearchText;
@property (assign,nonatomic)  BOOL           isAnimating;

@end
