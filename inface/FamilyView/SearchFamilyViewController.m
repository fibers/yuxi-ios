//
//  SearchFamilyViewController.m
//  inface
//
//  Created by Mac on 15/5/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SearchFamilyViewController.h"
#import "NSString+YXExtention.h"
#import "MBProgressHUD+YXStyle.h"

@interface SearchFamilyViewController ()

@end

@implementation SearchFamilyViewController


//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    if ([_SearchText.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入关键字" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    [_SearchText resignFirstResponder];
    [self searchList];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"搜索" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    
    UIImageView *imageView=[[UIImageView alloc]init];
    imageView.userInteractionEnabled=YES;
    imageView.frame=CGRectMake(0, 0, ScreenWidth-90, 36);
    imageView.layer.borderWidth = 1;
    imageView.layer.borderColor = XIAN_COLOR.CGColor;
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = 4;
    imageView.backgroundColor=[UIColor whiteColor];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView=imageView;
    
    UIImageView *imageViewtmp=[[UIImageView alloc]init];
    imageViewtmp.userInteractionEnabled=YES;
    imageViewtmp.image=[UIImage imageNamed:@"sousuoju"];
    imageViewtmp.frame=CGRectMake(10, 9, 22, 18);
    imageViewtmp.clipsToBounds = YES;
    imageViewtmp.contentMode = UIViewContentModeScaleAspectFit;
    [imageView addSubview:imageViewtmp];
    
    _SearchText = [[UITextField alloc] initWithFrame:CGRectMake(37, 0, ScreenWidth-90-40, 36)];
    _SearchText.borderStyle = UITextBorderStyleNone;
    _SearchText.backgroundColor = [UIColor clearColor];
    _SearchText.placeholder = @"请输入家族名称或ID号";
    _SearchText.text=@"";
    _SearchText.tintColor=BLUECOLOR;
    _SearchText.font = [UIFont systemFontOfSize:17];
    _SearchText.returnKeyType = UIReturnKeyDefault;
    _SearchText.delegate = self;
    _SearchText.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [imageView addSubview:_SearchText];
    
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 12, 0, 12)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 12, 0, 12)];
    }
    
    [self.tableView reloadData];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    
}

-(void)searchList
{
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    
    NSDictionary *params = @{@"uid":userid,@"cond":_SearchText.text,@"token":@"110"};
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    
    [HttpTool postWithPath:@"SearchFamily" params:params success:^(id JSON) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            
            self.tableDataFriends=[JSON objectForKey:@"familylist"];
            [self.tableView reloadData];
            
            if (self.tableDataFriends.count==0) {
                UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"该家族还没有，搜索其他家族试试吧~" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertview show];
            }
            
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
        
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];
    
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.tableDataFriends.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"FamilyAllTableViewCell";
    FamilyAllTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"FamilyAllTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSURL* url = [NSURL URLWithString:[[[self.tableDataFriends objectAtIndex:indexPath.row] objectForKey:@"portrait"] stringOfW100ImgUrl]];
    [cell.UserImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"zs_touxianglanfamily"] completed:nil];
    cell.UserImage.contentMode=UIViewContentModeScaleAspectFill;
    cell.UserImage.clipsToBounds=YES;
    
    cell.UserImage.layer.cornerRadius = CGRectGetHeight(cell.UserImage.bounds)/2;
    
    
    NSString *isvipfamliy=[[self.tableDataFriends objectAtIndex:indexPath.row] objectForKey:@"isvip"];
    if ([isvipfamliy isEqualToString:@"1"]) {
        UIImageView *vipimageView=[[UIImageView alloc]initWithFrame:CGRectMake(cell.UserImage.frame.origin.x-1.5,cell.UserImage.frame.origin.y -1.5, 46, 46)];
        vipimageView.image=[UIImage imageNamed:@"isvipfamliy"];
        vipimageView.userInteractionEnabled=YES;
        [cell addSubview:vipimageView];
    }
    
    cell.TitleLabel.text=[[[self.tableDataFriends objectAtIndex:indexPath.row] objectForKey:@"name"] description];
    
    return cell;
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 62;
    
}


//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    //所有
    FamilyInformationViewController * detailViewController = [[FamilyInformationViewController alloc] init];
    NSString * familyId = [[self.tableDataFriends objectAtIndex:indexPath.row]objectForKey:@"id"];
    detailViewController.dic = [self.tableDataFriends objectAtIndex:indexPath.row];
    detailViewController.familyid = familyId;
    detailViewController.hidesBottomBarWhenPushed=YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
