//
//  YXSummayViewController.m
//  inface
//
//  Created by lizhen on 15/10/29.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXSummayViewController.h"
#import "UITextView+Placeholder.h"
#import "YXProgressView.h"
#import "YXSetRulesViewController.h"
#import "YXRolesTypeSettingViewController.h"

@interface YXSummayViewController ()

@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UILabel *lab;
@property (nonatomic,strong)UITextView *textView;

@property (strong, nonatomic)PlayOfCreate *model;

@end

@implementation YXSummayViewController


-(void)YXPlayintroductionPlayOfModel:(PlayOfCreate *)playOfModel{

    _model = [[PlayOfCreate alloc]init];
    _model = playOfModel;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self createLeftButton];
    [self createScrollView];
    [self createTextView];
    self.hidesBottomBarWhenPushed = YES;
    
}

-(void)createLeftButton{
    
    self.navigationItem.title = @"主线剧情";
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    LeftButton.tag = 10;
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _rightButton.frame = CGRectMake(0, 0, 48, 24);
    [_rightButton setTitle:@"下一步" forState:normal];
    _rightButton.titleLabel.font = [UIFont systemFontOfSize:12.0];
    _rightButton.tag = 11;
    _rightButton.layer.cornerRadius = 4.0;
    [_rightButton setTitleColor:[UIColor whiteColor] forState:normal];
    _rightButton.backgroundColor = [UIColor colorWithHexString:@"#7fb0e9"];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_rightButton];
    [_rightButton addTarget:self action:@selector(returnBackl:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    
}

//左上角按钮
-(void)returnBackl:(UIButton *)btn{
    if (btn.tag == 10) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        YXRolesTypeSettingViewController *vc = [[YXRolesTypeSettingViewController alloc]init];
        _model.mainStoryLine = _textView.text;
        [vc YXPlayintroductionPlayOfCreateModel:_model];
        [_textView endEditing:YES];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)createScrollView{

    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [self.view addSubview:_scrollView];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(ScreenWidth, ScreenWidth==320?ScreenHeight+130:ScreenHeight);
    [_scrollView setShowsVerticalScrollIndicator:NO];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [_scrollView addGestureRecognizer:tap];

}

-(void)createTextView{
    ///TextView的高度
    CGFloat textView_H;
    if (ScreenHeight > 700) {
        textView_H = 280.0;
    }else if(ScreenHeight > 650){
        textView_H = 250.0;
    }else{
        textView_H = 200.0;
    }
    if (ScreenHeight == 480) {
        textView_H = 170;
    }
    LOG(@"%f/////",ScreenHeight);
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(15, 6, ScreenWidth-15, textView_H)];
    [_scrollView addSubview:_textView];
    _textView.placeholder = @"选填。详细的剧大纲可以帮助小伙伴更快的融入剧情呦";
    _textView.font = [UIFont systemFontOfSize:14.0];
    _textView.delegate = self;
    
    UILabel *lineLab = [[UILabel alloc]initWithFrame:CGRectMake(0, textView_H+20, ScreenWidth, 1)];
    lineLab.backgroundColor = [UIColor colorWithRed:0.88 green:0.88 blue:0.88 alpha:1];
    [_scrollView addSubview:lineLab];

    YXProgressView *progressView = [[YXProgressView alloc]init];
    UIView *progress =[progressView getCurrentPage:3 andCurrentViewOfY:_textView.frame.origin.y+textView_H+220];
    [_scrollView addSubview:progress];
}

-(void)tapClick{
    
    [_textView endEditing:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//判断内容大小，多出去的部分进行自动截取
- (void)textViewDidChange:(UITextView *)textView {
    NSInteger number = [textView.text length];
    if (number >= 500) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"主线剧情最长不超过500个字" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        textView.text = [textView.text substringToIndex:500];
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    CGFloat panging;
    if (ScreenHeight == 480 && ScreenWidth == 320 ) {
        panging = 10;
    }
    if (ScreenWidth == 320 && ScreenHeight !=480) {
        panging = 10;
    }
    if(ScreenWidth != 320 && ScreenHeight !=480){
        panging = 0;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - panging, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    CGFloat panging;
    if (ScreenHeight == 480 && ScreenWidth == 320 ) {
        panging = 10;
    }
    if (ScreenWidth == 320 && ScreenHeight !=480) {
        panging = 10;
    }
    if(ScreenWidth != 320 && ScreenHeight !=480){
        panging = 0;
    }
    
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + panging, self.view.frame.size.width, self.view.frame.size.height);
    }];
    
}




@end
