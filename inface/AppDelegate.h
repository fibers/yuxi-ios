//
//  AppDelegate.h
//  inface
//
//  Created by Mac on 15/4/1.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//测试

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "BaseTabBarViewController.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WXApi.h"
#import "WeiboSDK.h"
#import <ShareSDK/ShareSDK.h>//sharesdk分享
#import "SessionModule.h"
#import "ChatViewController.h"
#import "WelcomeViewController.h"
#import "MobClick.h"

@protocol UnreadMessageCount <NSObject>

-(NSInteger)setUnreadCount;
-(void)getLocalRecents;
@end
@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>
{

    NSMutableDictionary *adic;
}
@property(unsafe_unretained,nonatomic)id<UnreadMessageCount>delegate;
@property (strong, nonatomic) UIWindow *window;
@property(strong)BaseTabBarViewController* aBaseTabBarViewController;

@end

