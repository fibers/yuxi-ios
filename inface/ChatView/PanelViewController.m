//
//  PanelViewController.m
//  inface
//
//  Created by Mac on 15/4/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "PanelViewController.h"
#define MaxNumberOfDescriptionChars  2000//textview最多输入的字数


@interface PanelViewController ()<UIScrollViewDelegate>
{
    UIButton  * downButton;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatViewBottomConstant;//输入框和下边缘的距离

@end

@implementation PanelViewController


//左上角按钮
-(void)returnBackl{
    //    if (self.chatInputTextView.text.length > 0) {
    //        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"是否保存文本内容" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    //        [alert show];
    //        alert.tag =10000;
    //    }
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    if ([self.chatInputTextView.text isEmpty]) {
        [self.chatInputTextView resignFirstResponder];
        return;
    }
    
    
    //该判断用于联想输入
    if (self.chatInputTextView.text.length > MaxNumberOfDescriptionChars)
    {
        self.chatInputTextView.text = [self.chatInputTextView.text substringToIndex:MaxNumberOfDescriptionChars];
        
    }
    
    //发送消息
    NSString* text = [self.chatInputTextView text];
    DDMessageContentType msgContentType = DDMessageTypeText;
    DDMessageEntity *message = [DDMessageEntity makeMessage:text Module:self.module MsgType:msgContentType isSendImage:NO isCurrentGroup:NO];
    
    [self sendMessage:text messageEntity:message];
    //取得文字
    
    self.chatInputTextView.text    =   @"";
    [self.chatInputTextView resignFirstResponder];
    [[DDDatabaseUtil instance]insertDraftMessage:@"" sessionId:self.module.sessionEntity.sessionID];

    [self.navigationController popViewControllerAnimated:YES];
}

/*由于联想输入的时候，函数textView:shouldChangeTextInRange:replacementText:无法判断字数，
 因此使用textViewDidChange对TextView里面的字数进行判断
 */
- (void)textViewDidChange:(UITextView *)textView
{
    
    //该判断用于联想输入
    if (self.chatInputTextView.text.length > MaxNumberOfDescriptionChars)
    {
        NSData * data = [self.chatInputTextView.text dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString * result = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        self.chatInputTextView.text = [result substringToIndex:MaxNumberOfDescriptionChars];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"文本内容不能超过2000字哦" message:@"程序猿们正在改善中" delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
        [alert show];
    }
    [[DDDatabaseUtil instance]insertDraftMessage:self.chatInputTextView.text sessionId:self.module.sessionEntity.sessionID];

}


-(void)sendMessage:(NSString *)msg messageEntity:(DDMessageEntity *)message
{
    BOOL isGroup = [self.module.sessionEntity isGroup];
    [[DDMessageSendManager instance] sendMessage:message isGroup:isGroup Session:self.module.sessionEntity  completion:^(DDMessageEntity* theMessage,NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            message.state= theMessage.state;
        });
    } Error:^(NSError *error) {
        
        
        
    }];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100000) {
        if (buttonIndex==1) {
            //保存草稿
        }else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[IQKeyboardManager sharedManager]setEnable:NO];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];

    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"发送" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"面板";
    
    UISwipeGestureRecognizer * gesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeDown:)];
    
    [gesture setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.chatInputTextView addGestureRecognizer:gesture];
    
    //设置发文字框为圆角矩形
    self.chatInputTextView.layer.borderWidth = 1;
    self.chatInputTextView.layer.borderColor = COLOR(226, 226, 226, 1).CGColor;
    self.chatInputTextView.layer.cornerRadius = 6;
    self.chatInputTextView.clipsToBounds = YES;
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInputTextView.placeholder = @"请输入想说的话...";
    self.chatInputTextView.textColor=[UIColor blackColor];
    self.chatInputTextView.placeholderFont=[UIFont systemFontOfSize:14];
    self.chatInputTextView.placeholderColor=COLOR(204, 204, 204, 1);
    self.chatInputTextView.backgroundColor=COLOR(245, 245, 245, 1);
    self.chatInputTextViewHeight.constant = ScreenHeight-64-20-20;
    self.chatInputTextView.returnKeyType=UIReturnKeyDefault;
    [[DDDatabaseUtil instance]getEditMessage:self.module.sessionEntity.sessionID success:^(id message) {
        
        self.chatInputTextView.text = message;
    }];
    self.emojiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.emojiBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-40,ScreenHeight-104, 24, 24)];
    self.emojiBtn.backgroundColor = [UIColor clearColor];
    //表情按钮更换状态
    [self.emojiBtn setImage:[UIImage imageNamed:@"emotions"] forState:UIControlStateNormal];
    [self.emojiBtn setImage:[UIImage imageNamed:@"keyboard"] forState:UIControlStateSelected];
    [self.emojiBtn addTarget:self action:@selector(emojiAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.emojiBtn];
    [self.view bringSubviewToFront:self.emojiBtn];
    [self CreateEmojiView];
}


#pragma mark 增加下滑手势收回键盘

-(void)swipeDown:(UISwipeGestureRecognizer*)gesture
{
    if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
        [self.chatInputTextView resignFirstResponder];
    }
    
}


-(void)downKeyboard
{
    [self.chatInputTextView resignFirstResponder];
    
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.chatInputTextView resignFirstResponder];

}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[DDDatabaseUtil instance]getDraftMessage:self.module.sessionEntity.sessionID success:^(id json) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.chatInputTextView.text = json;
            
        });
        
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_chatInputTextView resignFirstResponder];
}


-(void) keyboardWillShow:(NSNotification *) aNotification {
    
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    //跳转到详细的视图控制器
    
    [UIView animateWithDuration:0.3 animations:^{
        
        //改变位置
        downButton.hidden = NO;
        downButton.frame = CGRectMake(ScreenWidth- 80, ScreenHeight -height -90, 30, 30);
        _chatViewBottomConstant.constant = height;
        self.emojiBtn.frame = CGRectMake(ScreenWidth-40,ScreenHeight-90-height, 24, 24);
        
    }];
    
}

-(void) keyboardDidShow:(NSNotification *) aNotification {
    
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    float height = keyboardRect.size.height;
    
    //跳转到详细的视图控制器
    
}



-(void) keyboardWillHide:(NSNotification *) aNotification
{
    downButton.hidden = YES;
    [UIView animateWithDuration:0.3 animations:^{
        
        //改变位置
        downButton.frame = CGRectMake(ScreenWidth- 80, ScreenHeight -100, 30, 30);
        
        _chatViewBottomConstant.constant = 20;
        self.emojiBtn.frame = CGRectMake(ScreenWidth-40,ScreenHeight-104, 24, 24);
        
    }];
    
    
}


- (AZXEmojiManagerView *)CreateEmojiView
{
    if (!_emojiView) {
        _emojiView = [[AZXEmojiManagerView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 200)];
        _emojiView.backgroundColor = [UIColor blackColor];
        _emojiView.delegate = self;
    }
    return _emojiView;
}

- (void)emojiAction:(UIButton *)sender {
    
    //表情
    if (sender.selected==NO) {
        sender.selected=YES;
        [self.chatInputTextView becomeFirstResponder];
        self.chatInputTextView.inputView=self.emojiView;
        [self.chatInputTextView reloadInputViews];
        //        [UIView animateWithDuration:.3f animations:^{
        //            //改变位置
        //            self.InputViewSpace.constant = 200;
        //        }];
    } else {
        sender.selected=NO;
        self.chatInputTextView.inputView=nil;
        [self.chatInputTextView reloadInputViews];
    }
}



#pragma mark emojiManagerView delegate
- (void)emojiManagerView:(UIView *)emojiManagerView didSelectItem:(NSString *)item AtIndex:(NSUInteger)index
{
    NSMutableArray *aimgArray=[[SGXEmotionContent sharedClient]getEmotionArr];
    if (index==aimgArray.count-1) {
        [self.chatInputTextView resignFirstResponder];
        AddDoViewController * detailViewController = [[AddDoViewController alloc] init];
        detailViewController.hidesBottomBarWhenPushed=YES;
        if (detailViewController.isAinmating) {
            return;
        }
        detailViewController.isAinmating = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    } else {
        //取得文字
        NSMutableArray *aimgArray=[[SGXEmotionContent sharedClient]getEmotionArr];
        NSString    *emotiContent   =   [[aimgArray objectAtIndex:index] objectForKey:@"content"];
        
        if (index<6) {
            
            self.chatInputTextView.text    =   [self.chatInputTextView.text stringByAppendingString:[NSString stringWithFormat:@"%@",emotiContent]];
        } else {
            
            self.chatInputTextView.text    =   [self.chatInputTextView.text stringByAppendingString:[NSString stringWithFormat:@"[%@]",emotiContent]];
        }
        
        self.emojiBtn.selected=NO;
        self.chatInputTextView.inputView=nil;
        [self.chatInputTextView reloadInputViews];
    }
    
}

#pragma mark 点击皮表代理方法
-(void)skinSurfaceView:(UIView *)skinSurfaceView didSendItem:(NSString *)item
{
    self.chatInputTextView.text = [self.chatInputTextView.text stringByAppendingString:item];
    self.emojiBtn.selected = NO;
    self.chatInputTextView.inputView = nil;
    [self.chatInputTextView reloadInputViews];
}

- (void)emojiManagerView:(UIView *)emojiManagerView didDeleteItem:(NSString *)item
{
    //删除文字
    self.chatInputTextView.text    =   @"";
    
}


- (void)emojiManagerView:(UIView *)emojiManagerView didSendItem:(NSString *)item
{
    [self textViewEnterSend];
}

- (void)textViewEnterSend
{
    if ([self.chatInputTextView.text isEmpty]) {
        [self.chatInputTextView resignFirstResponder];
        return;
    }
    
    //发送消息
    NSString* text = [self.chatInputTextView text];
    if (text.length > 2000) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"文本内容不能超过2000字哦" message:@"程序猿们正在改善中" delegate:nil cancelButtonTitle:@"收到" otherButtonTitles:nil];
        [alert show];
        return;
    }
    DDMessageContentType msgContentType = DDMessageTypeText;
    DDMessageEntity *message = [DDMessageEntity makeMessage:text Module:self.module MsgType:msgContentType isSendImage:NO isCurrentGroup:NO];
#pragma mark 成功前不用添加到数据库
    [self sendMessage:text messageEntity:message];
    //取得文字
    
    self.chatInputTextView.text    =   @"";
    [[DDDatabaseUtil instance]insertDraftMessage:@"" sessionId:self.module.sessionEntity.sessionID];
    [self.chatInputTextView resignFirstResponder];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
