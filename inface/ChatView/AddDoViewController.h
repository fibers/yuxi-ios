//
//  AddDoViewController.h
//  inface
//
//  Created by Mac on 15/4/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddDoViewController : BaseViewController<UITextViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet PlaceholderTextView *chatInputTextView;
@property (weak, nonatomic) IBOutlet UITextField *chatInputTextField;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (assign,nonatomic)BOOL  isAinmating;

@end
