//
//  RoleTypes.m
//  inface
//
//  Created by appleone on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "RoleTypes.h"
#import "roleTypeDetail.h"
@implementation RoleTypes
+ (NSDictionary *)objectClassInArray{
    return @{@"roleTypes" : [roleTypeDetail class]};
}

@end
