//
//  mixnature.h
//  inface
//
//  Created by appleone on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoleTypes.h"
#import "RoleNature.h"
@interface mixnature : NSObject
@property(strong,nonatomic)RoleTypes * roleType;
@property(strong,nonatomic)RoleNature * roleNature;

@end
