//
//  roleTypeDetail.h
//  inface
//
//  Created by appleone on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface roleTypeDetail : NSObject
@property(strong,nonatomic) NSString * type;
@property(strong,nonatomic) NSString * count;
@property(strong,nonatomic) NSString * background;
@property(strong,nonatomic) NSString * applied;

@end
