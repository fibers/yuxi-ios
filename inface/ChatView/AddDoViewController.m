//
//  AddDoViewController.m
//  inface
//
//  Created by Mac on 15/4/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "AddDoViewController.h"
#define MaxNumberOfDescriptionChars  100//textview最多输入的字数

@interface AddDoViewController ()
@end

@implementation AddDoViewController



//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    if ([self.chatInputTextField.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入文字标签" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if ([self.chatInputTextView.text isEmpty]) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"请输入文字表情" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertview show];
        return;
        
    }
    
    if (self.chatInputTextField.text.length > 4)
    {
        self.chatInputTextField.text = [self.chatInputTextField.text substringToIndex:4];
    }
    //添加表情接口(Post)
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];
    
    NSDictionary *params = @{@"uid":userid,@"title":self.chatInputTextField.text,@"content":self.chatInputTextView.text,@"token":@"110"};
    
    [HttpTool postWithPath:@"AddFavoriteEmotion" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]integerValue]==1) {
            [self.navigationController popViewControllerAnimated:YES];

            [[GetAllFamilysAndStorys shareInstance]GetFavoriteEmotionList];
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
    }];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAinmating = NO;
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAinmating = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"保存" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"添加Do";
    
    self.chatInputTextView.delegate=self;
    self.chatInputTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.chatInputTextView.font = [UIFont systemFontOfSize:14.0f];
    self.chatInputTextView.textColor=[UIColor blackColor];
    self.chatInputTextView.placeholder=@"文字表情";
    self.chatInputTextView.placeholderFont=[UIFont systemFontOfSize:14];
    self.chatInputTextView.placeholderColor=COLOR(204, 204, 204, 1);
    
    self.chatInputTextField.delegate = self;
    self.chatInputTextView.returnKeyType=UIReturnKeyDone;
    self.countLabel.textColor=COLOR(204, 204, 204, 1);
    self.countLabel.text=[NSString stringWithFormat:@"%lu",MaxNumberOfDescriptionChars-self.chatInputTextView.text.length];
}


/*由于联想输入的时候，函数textView:shouldChangeTextInRange:replacementText:无法判断字数，
 因此使用textViewDidChange对TextView里面的字数进行判断
 */
- (void)textViewDidChange:(UITextView *)textView
{

    //该判断用于联想输入
    if (textView.text.length > MaxNumberOfDescriptionChars)
    {
        textView.text = [textView.text substringToIndex:MaxNumberOfDescriptionChars];

    }
    self.countLabel.text=[NSString stringWithFormat:@"%lu",MaxNumberOfDescriptionChars-textView.text.length];
}


- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField==self.chatInputTextField) {
        
        if (textField.text.length > 4)
        {
            textField.text = [textField.text substringToIndex:4];
        }
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
