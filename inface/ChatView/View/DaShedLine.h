//
//  DaShedLine.h
//  inface
//
//  Created by appleone on 15/11/4.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DaShedLine : UIView

@property(nonatomic)CGPoint startPoint;//虚线起点
@property(nonatomic)CGPoint endPoint;//虚线终点
@property(nonatomic,strong)UIColor* lineColor;//虚线颜色
@end
