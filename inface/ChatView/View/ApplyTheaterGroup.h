//
//  ApplyTheaterGroup.h
//  inface
//
//  Created by appleone on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//
@protocol ApplyTheaterGroupDelegate <NSObject>

-(void)ApplyTheaterGroup;

-(void)hideApplyTheaterView;
@end
#import <UIKit/UIKit.h>

@interface ApplyTheaterGroup : UIView
@property(assign,nonatomic)id<ApplyTheaterGroupDelegate>delegate;

@end
