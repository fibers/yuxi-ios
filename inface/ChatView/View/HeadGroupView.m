//
//  HeadGroupView.m
//  inface
//
//  Created by appleone on 15/9/4.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "HeadGroupView.h"

@implementation HeadGroupView
- (void)readGroupNoty:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(readNoty)]) {
        [self.delegate readNoty];
    }
}

- (IBAction)cancelGroupNoty:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(removeView)]) {
        [self.delegate removeView];
    }
    
}

-(void)setTile:(NSString *)title content:(NSString *)content dateTime:(NSString * )dateTime groupId:(NSString *)groupId
{
    __block NSString * creatorName = @"";
    [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:groupId success:^(id JSON) {
        NSDictionary * storyinfo = JSON;
        
        creatorName = [storyinfo objectForKey:@"creatorname"];
        
        
    } failure:^(id Json) {
        
        
    }];
    
    UIButton * bgButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [bgButton setBackgroundColor:[UIColor clearColor]];
    
    [bgButton addTarget:self action:@selector(readGroupNoty:) forControlEvents:UIControlEventTouchUpInside];
    
    bgButton.frame = CGRectMake(0, 0, ScreenWidth-100, 80);
    
    [self addSubview:bgButton];

    self.titleLabel.text = title;
    
    self.creatorLabel.text = creatorName;
    
    self.contentLabel.text = content;
    
    [self.contentLabel setEditable:NO];
    
    self.contentLabel.scrollEnabled = YES;
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:[dateTime integerValue]];
    NSString * dateStrng = [date getGroupNotyTime];
    NSRange range = [dateStrng rangeOfString:@"日"];
    _contentLabel.layer.cornerRadius = 4;
    _contentLabel.clipsToBounds = YES;
    if (range.length > 0) {
        NSString * time1 = [dateStrng substringToIndex:range.location+1];
        NSString * time2 = [dateStrng substringWithRange:NSMakeRange(range.location+1, dateStrng.length - range.location-1)];
        self.dateLabel.text = time1;
        self.timeLabel.text = time2;
    }
    _contentLabel.userInteractionEnabled = NO;
    [self bringSubviewToFront:bgButton];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
