
//  ApplyTheaterViewController.m
//  inface
//
//  Created by appleone on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "ApplyTheaterViewController.h"
#import "StoryRoleSet.h"
#import "mixnature.h"
#import "RoleTypes.h"
#import "RoleNature.h"
#import "roleNatureDetail.h"
#import "roleTypeDetail.h"
#import "DaShedLine.h"
#import "NSAttributedString+CXACoreTextFrameSize.h"
#import "MBProgressHUD.h"
#import "DashesLineView.h"
@interface ApplyTheaterViewController ()<UITextFieldDelegate,UIScrollViewDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(strong,nonatomic)StoryRoleSet * storySet;
@property(strong,nonatomic)UIView * superView;
@property(strong,nonatomic)UIView * backView;
@property(strong,nonatomic)UILabel * backlal;
@property(strong,nonatomic)UIView  * labView;
@property (strong,nonatomic)NSMutableArray * selecBtns;
@property (nonatomic,assign)BOOL    isPullDown;
@property(assign,nonatomic) CGFloat titleHei;
@property (assign,nonatomic)NSInteger  selectIndex;
@property (strong,nonatomic)NSMutableArray * textFields;
@property (strong,nonatomic)UITextView   * detailTextView;
@property (strong,nonatomic)MBProgressHUD   * hud;
@property (strong,nonatomic)NSDictionary    * roleSetDic;
@property (strong,nonatomic)NSMutableArray * roleLabels;
@property (nonatomic,assign)BOOL    isOwner;//是否是管理员自己
@property (nonatomic,assign)BOOL    ifSelectRole;//是否是管理员自己

@end

@implementation ApplyTheaterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    _selecBtns = [NSMutableArray array];
    _textFields = [NSMutableArray array];
    self.view.backgroundColor = [UIColor whiteColor];
    _roleLabels = [NSMutableArray array];
    [self getStoryRoleSettings];
    
    self.navigationItem.title = self.isOwner == YES ?@"角色设定":@"申请角色";
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [_scrollView addGestureRecognizer:tap];
    
    _scrollView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    [IQKeyboardManager sharedManager].enable  =YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [IQKeyboardManager sharedManager].shouldToolbarUsesTextFieldTintColor = YES;
    // Do any additional setup after loading the view from its nib.
}

//左上角按钮
-(void)returnBackl{
    if (_isOwner) {
        UIAlertView * alrt = [[UIAlertView alloc]initWithTitle:@"是否放弃选择角色" message:@"放弃之后就无法选择角色了" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alrt.tag = 10000;
        [alrt show];
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)tapClick
{
    [self.view endEditing:YES];;
}

-(MBProgressHUD *)hud{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc]initWithView:self.view];
    }
    return _hud;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)getStoryRoleSettings
{
    NSDictionary * params = @{@"uid":USERID,@"storyid":self.thearterId};
    
    [HttpTool postWithPath:@"GetStoryRoleSettings" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            _roleSetDic = [JSON objectForKey:@"rolesettings"];
          
            [self createScrollView];
            
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)setTheaterId:(NSString *)theaterId
{
    self.theaterId = theaterId ;
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10000 && buttonIndex == 1) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


#pragma mark 创建视图
-(void)createScrollView
{
    //角色数组
    NSArray * roleTypes = [_roleSetDic objectForKey:@"roletypes"];
    
    CGFloat roleLabOriHei = 53;
    
    CGFloat bottomHei = 10;
    
    CGFloat bottomWid = 26;
    
    CGFloat roleLabOriWid = 17;
    
    CGFloat labelWid = (ScreenWidth - 60)/2;
    
    CGFloat labelHei = 29;
    //创建角色的label
    [roleTypes enumerateObjectsUsingBlock:^(NSDictionary *  obj, NSUInteger idx, BOOL * _Nonnull stop) {
         NSInteger line = idx / 2;
         NSInteger rank = idx %2;
        UIButton * roleBtn = [[UIButton alloc]initWithFrame:CGRectMake(roleLabOriWid + rank * (labelWid + bottomWid), 45 + line * (labelHei + bottomHei), labelWid, labelHei)];
        roleBtn.layer.cornerRadius = 2.0f;
        UILabel * titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, labelWid,labelHei)];
        NSString * title = [obj objectForKey:@"type"];
        [titleLab setText:title];
        [titleLab setFont:[UIFont systemFontOfSize:15.0f]];
        [titleLab setTextColor:[UIColor colorWithHexString:@"#4a4a4a"]];
        titleLab.textAlignment = NSTextAlignmentCenter;
        [roleBtn addSubview:titleLab];
        NSString * applied  = [obj objectForKey:@"applied"];
        [_roleLabels addObject:titleLab];
        titleLab.tag = idx;
        roleBtn.layer.cornerRadius = 4.0f;
        NSString * count =  [obj objectForKey:@"count"];
        BOOL roleIsEnpty = [applied intValue] >= [count intValue] ? YES:NO;//此脚色已经没有了
        if (roleIsEnpty) {
            roleBtn.userInteractionEnabled = NO;
            [roleBtn setBackgroundColor:[UIColor colorWithHexString:@"d1d1d1"]];
        }
        roleBtn.layer.borderWidth = 0.5;
        
        roleBtn.layer.borderColor = [UIColor colorWithHexString:@"#949494"].CGColor;
        [_selecBtns addObject:roleBtn];
        roleBtn.tag = idx;
        [roleBtn addTarget:self action:@selector(showBackDetail:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollView addSubview:roleBtn];
    }];
    
    NSInteger lineViewHei = roleTypes.count /2 + roleTypes.count % 2;
    
    CGFloat   viewOriHei =  lineViewHei * (labelHei + bottomHei) + 45;
    
    self.titleHei = viewOriHei;
    
    UILabel * backLable = [[UILabel alloc]initWithFrame:CGRectMake(17, 0, 80, 20)];
    
    [backLable setText:@"身世背景:"];
    
    [backLable setTintColor:[UIColor colorWithHexString:@"#4a4a4a"]];
    
    [backLable setFont:[UIFont systemFontOfSize:15.0f]];
    
    _labView = [[UIView alloc]initWithFrame:CGRectMake(0, 35, ScreenWidth, 100)];
    
    _labView.backgroundColor = [UIColor colorWithHexString:@"#f4f9ff"];
    
    _backlal = [[UILabel alloc]initWithFrame:CGRectMake(15, 2, ScreenWidth -30, 70)];
    
    _backlal.text = @"身份背景";
    
    [_backlal setTextColor:[UIColor colorWithHexString:@"4a4a4a"]];
    
    [_backlal setFont:[UIFont systemFontOfSize:14]];
    
    [_backlal setNumberOfLines:0];

    //需要创建一个View，将下面的控件放到view中
    _backView = [[UIView alloc]initWithFrame:CGRectMake(0, viewOriHei, ScreenWidth, 152)];
    
    _backView.backgroundColor = [UIColor clearColor];
    
    [_labView addSubview:_backlal];
    
    [_backView addSubview:_labView];
    
    [_backView addSubview:backLable];
    
    [_scrollView addSubview:_backView];
    
    _backView.hidden = YES;

     _superView = [[UIView alloc]initWithFrame:CGRectMake(0, viewOriHei -10, ScreenWidth, 20000)];
    
   UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 1, ScreenWidth, 0.5)];
    lineView.backgroundColor=XIAN_COLOR;;

    [_superView addSubview:lineView];
    
    //角色设定label
    UILabel * roleSetLab = [[UILabel alloc]initWithFrame:CGRectMake(17,  10, 160, 21)];
    
    
    [roleSetLab setFont:[UIFont systemFontOfSize:16.0f]];
    
    [roleSetLab setTextColor:[UIColor colorWithHexString:@"#656464"]];
    [_superView addSubview:roleSetLab];
    //角色设定模块
    NSArray * roleNatures = [_roleSetDic objectForKey:@"rolenatures"];
    
    if (roleNatures.count > 0) {
        [roleSetLab setText:@"角色设定"];

    }else{
        [roleSetLab setText:@"角色设定:暂无"];

    }

    //创建角色设定和textfield
    [roleNatures enumerateObjectsUsingBlock:^(NSDictionary * natureDic, NSUInteger idx, BOOL * _Nonnull stop) {
        UILabel * setLabel = [[UILabel alloc]initWithFrame:CGRectMake(17, 45 + idx * 82, 60, 25)];
        setLabel.layer.cornerRadius = 4.0f;
        NSString * name = [natureDic objectForKey:@"name"];
        [setLabel setText:name];
        [setLabel setBackgroundColor:[UIColor colorWithHexString:@"#eb71a8"]];
        setLabel.layer.cornerRadius = 4.0f;
        setLabel.textAlignment = NSTextAlignmentCenter;
        setLabel.clipsToBounds = YES;
        [setLabel setTextColor:[UIColor whiteColor]];
        [setLabel setFont:[UIFont systemFontOfSize:12]];
        [_superView addSubview:setLabel];
        
        UITextField * field = [[UITextField alloc]initWithFrame:CGRectMake(17, 84+ idx *82, ScreenWidth - 34, 29)];
        
        UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,12,26)];
        leftView.backgroundColor = [UIColor clearColor];
        
        field.leftView = leftView;
        
        field.leftViewMode = UITextFieldViewModeAlways;
        field.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        field.layer.cornerRadius = 4.0f;
        field.layer.borderWidth = 0.5;
        field.layer.borderColor = [UIColor colorWithHexString:@"#949494"].CGColor;
        [field setFont:[UIFont systemFontOfSize:14.0f]];
        [_textFields addObject:field];
        [_superView addSubview:field];
    }];
    

    //创建第二根线
    UIView * lineView2 = [[UIView alloc]initWithFrame:CGRectMake(0,  45 + roleNatures.count * 82, ScreenWidth, 0.5)];
    
    lineView2.backgroundColor=XIAN_COLOR;
    
    [_superView addSubview:lineView2];
    
    CGFloat detailHome =  62 + roleNatures.count * 82;
    
    //写一段戏文给群主审核
    UILabel * detailTitle = [[UILabel alloc]initWithFrame:CGRectMake(17, detailHome, 200, 21)];
    
    [detailTitle setText:@"码一段自戏向群主示好"];
    
    [detailTitle setFont:[UIFont systemFontOfSize:15.0f]];
    
    [detailTitle setTextColor:[UIColor colorWithHexString:@"#656464"]];
    if (!self.isOwner) {
        [_superView addSubview:detailTitle];
 
    }
    
    //戏文说明label
    UILabel * introLabel = [[UILabel alloc]initWithFrame:CGRectMake(17, detailHome + 23, 300, 20)];
    
    [introLabel setTextColor:[UIColor colorWithHexString:@"#7fb0e9"]];
    
    [introLabel setText:@"500字以上戏文、禁H、禁玛丽苏"];
    
    [introLabel setFont:[UIFont systemFontOfSize:12.0f]];
    if (!self.isOwner) {
        [_superView addSubview:introLabel];

    }
    _detailTextView = [[UITextView alloc]initWithFrame:CGRectMake(17, detailHome + 64, ScreenWidth - 34, 160)];

    _detailTextView.layer.cornerRadius = 4.0f;
    if (!self.isOwner) {
        [_superView addSubview:_detailTextView];
    }
    
    _detailTextView.layer.borderWidth = 0.5;
    
    _detailTextView.layer.borderColor = [UIColor colorWithHexString:@"#949494"].CGColor;
    //申请按钮
    CGFloat  applyBtnHei;
    if (!self.isOwner) {
        applyBtnHei = detailHome + 240;
    }else{
        applyBtnHei = detailHome;
    }
    
    UIView * footView = [[UIView alloc]initWithFrame:CGRectMake(0, applyBtnHei, ScreenWidth, 40)];
    
    footView.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
    
    UIButton * applyButton = [[UIButton alloc]initWithFrame:CGRectMake((ScreenWidth - 170)/2, 6, 170, 28)];
    applyButton.backgroundColor = [UIColor colorWithHexString:@"#eb71a8"];
    
    applyButton.layer.cornerRadius = 4.0f;
    
    applyButton.clipsToBounds = YES;
    
    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 170, 28)];
    
    [titleLabel setTextColor:[UIColor whiteColor]];
    
    [titleLabel setText:self.isOwner == YES ?@"确认":@"申请"];
    
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    
    [applyButton addSubview:titleLabel];
    
    [footView addSubview:applyButton];
    
    [_superView addSubview:footView];
    
    _superView.frame = CGRectMake(0, viewOriHei, ScreenWidth, applyBtnHei + 50);
    
    [_scrollView addSubview:_superView];
    if (ScreenHeight < 568) {
        _scrollView.contentSize = CGSizeMake(ScreenWidth, applyBtnHei + 200 + viewOriHei );

    }else{
        _scrollView.contentSize = CGSizeMake(ScreenWidth, applyBtnHei + 100 + viewOriHei );

    }
    if (!self.isOwner) {
            [applyButton addTarget:self action:@selector(applyTheater) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [applyButton addTarget:self action:@selector(chooseRole) forControlEvents:UIControlEventTouchUpInside];
    }

}


-(void)showBackDetail:(UIButton *)sender
{
    [self.selecBtns enumerateObjectsUsingBlock:^(UIButton * button, NSUInteger idx, BOOL * _Nonnull stop) {
          UILabel * titleLabel = _roleLabels[idx];
        if (idx == sender.tag) {
            button.backgroundColor = [UIColor colorWithHexString:@"#eb71a8"];
          
            [titleLabel setTextColor:[UIColor whiteColor]];
            
        }else{
            button.backgroundColor = [UIColor whiteColor];
            [titleLabel setTextColor:[UIColor colorWithHexString:@"#949494"]];

        }
        
    }];
    
    self.ifSelectRole = YES;
    self.selectIndex = sender.tag;
    
    NSArray * roleTypes = [_roleSetDic objectForKey:@"roletypes"];
    
    NSDictionary * typeDetail = roleTypes[sender.tag];
    
    NSString * backDetail = [typeDetail objectForKey:@"background"];
    
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:backDetail];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    [paragraphStyle setLineSpacing:5.0f];
    
    [attribute addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, backDetail.length)];
    [attribute addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(0, backDetail.length)];
    
//    CGFloat  labHei = [attribute  cxa_coreTextFrameSizeWithConstraints:CGSizeMake(ScreenWidth - 30, 9999)].height;
    CGFloat labHei = [HZSInstances boundAutoStingSize:backDetail uifont:14.0 lines:5 size:CGSizeMake(ScreenWidth - 30, 9999)];
    [_backlal setAttributedText:attribute];
    //文本框大小改变
    CGRect rect1 = _backlal.frame;
    
    rect1.size.height = labHei;
    
    _backlal.frame = rect1;
    
    //背景view的frame
    CGRect rect2 = _labView.frame;
    
    rect2.size.height = labHei + 5;
    
    _labView.frame = rect2;
    
    //整个的身世背景view
    CGRect rect3 = _backView.frame;
    
    rect3.size.height = 30+labHei;
    
    _backView.frame = rect3;
    
    //下面视图的变化,如果是管理员跳进来的不需要移动视图

    if (!self.isOwner) {
        _backView.hidden = NO;
        CGRect rect4 = _superView.frame;
       
        rect4.origin.y = self.titleHei + 17 + _backView.frame.size.height ;
        
        _superView.frame =  rect4;
        if (ScreenHeight < 568) {
            _scrollView.contentSize = CGSizeMake(ScreenWidth, rect4.origin.y + rect4.size.height + 150);
            
        }else{
            _scrollView.contentSize = CGSizeMake(ScreenWidth, rect4.origin.y + rect4.size.height + 70);
            
        }


    }
    

}

#pragma mark 管理员认领角色
-(void)chooseRole
{
    NSMutableArray * natureArr = [NSMutableArray arrayWithCapacity:10];
    
    NSArray * roleNature = [_roleSetDic objectForKey:@"rolenatures"];
    
    [self.textFields enumerateObjectsUsingBlock:^(UITextField * obj, NSUInteger idx, BOOL * _Nonnull stop) {

        if (obj.text && ![obj.text isEqualToString:@""]) {
            NSDictionary * natureDetail = roleNature[idx];
            NSDictionary * params = @{@"id":[natureDetail objectForKey:@"id"],@"value":obj.text};
            
            [natureArr addObject:params];
        }
    }];

    if (natureArr.count < self.textFields.count) {
        //有属性未填
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请填写完所有的角色属性" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
        [alert show];
        
        return;
        
    }

    
    NSString * jsonStr = [natureArr JSONString];
    
    NSArray * roleTypes = [_roleSetDic objectForKey:@"roletypes"];
    
    __block  BOOL  ifHasRole = NO;//判断还有没有可选的角色

    [roleTypes enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([[obj objectForKey:@"count"]intValue] > [[obj objectForKey:@"applied"]intValue]) {
            ifHasRole = YES;
        }
        
    }];
    if(!ifHasRole)
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"没有空余的角色了.." message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles: nil];
        [alert show];
        return ;
    }

     NSString * roleString = @"";
    if(roleTypes.count > 0)
    {
        //当角色数大于0时需要选择一个角色
        
        if(roleTypes.count > 0)
        {
            if (!self.ifSelectRole && ifHasRole) {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请选择一个角色喔" message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles: nil];
                [alert show];
                return ;
            }
            NSDictionary * roleDet = roleTypes[self.selectIndex];
            
            roleString = [roleDet objectForKey:@"type"];
        }

    }
    
    NSDictionary * detail = @{@"uid":USERID,@"storyid":self.thearterId,@"roletype":roleString,@"rolenatures":jsonStr};
    
    [self.hud setLabelText:@"数据请求中..."];
    [self.hud setMode:MBProgressHUDModeIndeterminate];
    [self.view addSubview:self.hud];
    [self.hud show:YES];
    [HttpTool postWithPath:@"ApplyMyStoryRole" params:detail success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            [self.hud setLabelText:@"认领角色成功"];
            [self.hud hide:YES afterDelay:2.0];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [self.hud setLabelText:[JSON objectForKey:@"describe"]];
            [self.hud hide:YES afterDelay:2.0];
        }
        
    } failure:^(NSError *error) {
        [self.hud setLabelText:@"认领失败"];
        [self.hud hide:YES afterDelay:2.0];
    }];
}


-(void)ifStoryOwnerApply
{
    _isOwner = YES;
}

-(void)applyTheater
{
    NSMutableArray * natureArr = [NSMutableArray arrayWithCapacity:10];
    
    NSArray * roleNature = [_roleSetDic objectForKey:@"rolenatures"];
    
    [self.textFields enumerateObjectsUsingBlock:^(UITextField * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary * natureDetail = roleNature[idx];
        if (obj.text && ![obj.text isEqualToString:@""]&&![obj.text isEqual:nil]) {
            NSDictionary * params = @{@"id":[natureDetail objectForKey:@"id"],@"value":obj.text};
            
            [natureArr addObject:params];
        }

    }];
    if (natureArr.count < self.textFields.count) {
        //有属性未填
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请填写完所有的角色属性" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
        [alert show];
        
        return;
        
    }
    
    NSString * jsonStr = [natureArr JSONString];
    
    NSArray * roleTypes = [_roleSetDic objectForKey:@"roletypes"];
    
  __block  BOOL  ifHasRole = NO;//判断还有没有可选的角色
    
    
    [roleTypes enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([[obj objectForKey:@"count"]intValue] > [[obj objectForKey:@"applied"]intValue]) {
            ifHasRole = YES;
        }
        
    }];
     if(!ifHasRole)
    {
          UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"没有空余的角色了.." message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles: nil];
          [alert show];
          return ;
    }
     NSString * roleString = @"";

    if(roleTypes.count > 0)
    {
        if (!self.ifSelectRole && ifHasRole) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请选择一个角色喔" message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles: nil];
            [alert show];
            return ;
        }
        NSDictionary * roleDet = roleTypes[self.selectIndex];

        roleString = [roleDet objectForKey:@"type"];
    }
    
    
    NSString * detailText = _detailTextView.text;
    //NSString *text = [NSString stringWithFormat:@"  %@",_detailTextView.text];
    if (!detailText || [detailText isEqualToString:@""]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"请写上一段戏文" message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles: nil];
        [alert show];
        return ;
    }
    NSDictionary * detail = @{@"uid":USERID,@"storyid":self.thearterId,@"roletype":roleString,@"rolenatures":jsonStr,@"applytext":detailText};
    [self.hud setLabelText:@"数据请求中..."];
    [self.hud setMode:MBProgressHUDModeIndeterminate];
    [self.view addSubview:self.hud];
    [self.hud show:YES];
    [self.view bringSubviewToFront:self.hud];
    [HttpTool postWithPath:@"ApplyStoryRole" params:detail success:^(id JSON) {
        [self.hud setHidden:YES];

        if ([[JSON objectForKey:@"result"]isEqualToString:@"1"]) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"申请发送成功" message:@"等待群主大人的审核" delegate:nil cancelButtonTitle:@"收到" otherButtonTitles: nil];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
           UIAlertView * alert = [[UIAlertView alloc]initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"收到" otherButtonTitles: nil];
            [alert show];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } failure:^(NSError *error) {
        [self.hud setLabelText:@"创建失败"];
        [self.hud setMode:MBProgressHUDModeText];
        [self.hud hide:YES afterDelay:2];
        
    }];
    
}


- (NSData *)toJSONData:(id)theData{
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if ([jsonData length] > 0 && error == nil){
        return jsonData;
    }else{
        return nil;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
}

-(void) keyboardWillShow:(NSNotification *) aNotification

{
//    NSDictionary *userInfo = [aNotification userInfo];
//
//    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect keyboardRect = [aValue CGRectValue];
//    float height = keyboardRect.size.height;
//    
//    [UIView animateWithDuration:0.3 animations:^{
//        
//        CGFloat viewHeight = self.view.frame.size.height;
//        self.view.frame = CGRectMake(0, -height, ScreenWidth, viewHeight);
//        
//    }];
}

-(void) keyboardWillHide:(NSNotification *) note
{
//    [UIView animateWithDuration:0.3 animations:^{
//        
//        CGFloat viewHeight = self.view.frame.size.height;
//        self.view.frame = CGRectMake(0, 0, ScreenWidth, viewHeight);
//        
//    }];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
