//
//  ApplyTheaterGroup.m
//  inface
//
//  Created by appleone on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "ApplyTheaterGroup.h"

@interface ApplyTheaterGroup()
@property (weak, nonatomic) IBOutlet UILabel *headtTitleLab;

@property (weak, nonatomic) IBOutlet UILabel *detailLab;

@property (weak, nonatomic) IBOutlet UILabel *lineLab;

@property (weak, nonatomic) IBOutlet UIButton *completeBtn;

@end
@implementation ApplyTheaterGroup

-(void)awakeFromNib
{
   
}

- (IBAction)hideApplyView:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(hideApplyTheaterView)]) {
        [self.delegate hideApplyTheaterView];
    }
}

- (IBAction)applyTherterGroup:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(ApplyTheaterGroup)]) {
        [self.delegate ApplyTheaterGroup];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
