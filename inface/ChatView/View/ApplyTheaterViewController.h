//
//  ApplyTheaterViewController.h
//  inface
//
//  Created by appleone on 15/11/3.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface ApplyTheaterViewController : BaseViewController

@property(nonatomic,strong)NSString * thearterId;

-(void)ifStoryOwnerApply;
@end
