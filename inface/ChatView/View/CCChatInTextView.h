//
//  CCChatInTextView.h
//  inface
//
//  Created by appleone on 15/9/11.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCChatInTextView : UIView
@property (weak, nonatomic) IBOutlet UITextView *chatInTextView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatInTextViewConstent;

@property (weak, nonatomic) IBOutlet UIButton *emotionBtn;


@property (weak, nonatomic) IBOutlet UIButton *addImageBtn;


@end
