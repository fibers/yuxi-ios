//
//  DDChatBaseCell.m
//  IOSDuoduo
//
//  Created by murray on 14-5-28.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDChatBaseCell.h"
#import "UIView+DDAddition.h"
#import "DDUserModule.h"
#import "GetGroupRoleName.h"
#import "YXGroupCardViewController.h"
#import "OtherPersonRolrView.h"
#import "YXMyCenterViewController.h"
CGFloat const dd_avatarEdge = 8.0;                 //头像到边缘的距离
CGFloat const dd_avatarBubbleGap = 10;             //头像和气泡之间的距离
//CGFloat const dd_bubbleGap = 10;                   //气泡到非头像这边的距离
CGFloat const dd_bubbleUpDown = 10;                //气泡到上下边缘的距离
@interface DDChatBaseCell ()
@property(copy)NSString *currentUserID;

@end
@implementation DDChatBaseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        self.userAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [self.userAvatar setUserInteractionEnabled:YES];
        [self.contentView addSubview:self.userAvatar];
        self.vipWriteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 35, 15, 15)];
        [self.vipWriteImageView setImage:[UIImage imageNamed:@"vipWriter"]];
        [self.contentView addSubview:self.vipWriteImageView];
        self.userName =[[UILabel alloc] initWithFrame:CGRectMake(60, 10, ScreenWidth-40, 15)];
        [self.userName setBackgroundColor:[UIColor clearColor]];
        [self.userName setFont:[UIFont systemFontOfSize:10.0]];
        [self.userName setTextColor:[UIColor colorWithHexString:@"949494"]];
        [self.contentView addSubview:self.userName];
        self.bubbleImageView = [[MenuImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [self.contentView addSubview:self.bubbleImageView];
        [self.bubbleImageView setUserInteractionEnabled:YES];
        self.bubbleImageView.delegate = self;
        self.bubbleImageView.tag = 1000;
        
        self.activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.activityView setHidesWhenStopped:YES];
        [self.activityView setHidden:YES];
        [self.contentView addSubview:self.activityView];
        
        self.sendFailuredImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [self.sendFailuredImageView setImage:[UIImage imageNamed:@"dd_send_failed"]];
        [self.sendFailuredImageView setHidden:YES];
        self.sendFailuredImageView.userInteractionEnabled=YES;
        [self.contentView addSubview:self.sendFailuredImageView];
        [self.contentView setAutoresizesSubviews:NO];
        UITapGestureRecognizer *pan = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickTheSendAgain)];
        [self.sendFailuredImageView addGestureRecognizer:pan];
        UITapGestureRecognizer *openProfile = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openProfilePage)];
        [self.userAvatar addGestureRecognizer:openProfile];
        //头像长按手势暂不用
        
        UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGesture:)];
        longPress.minimumPressDuration = 1.0;
        //        [self.userAvatar addGestureRecognizer:longPress];
    }
    return self;
}

#pragma mark 长按个人头像
-(void)longPressGesture:(UITapGestureRecognizer*)tapGesture
{
    if (tapGesture.state == UIGestureRecognizerStateBegan) {
        
        NSString * senderId = @"";
        NSArray * arrids = [self.currentUserID componentsSeparatedByString:@"_"];
        if (arrids.count > 1) {
            senderId = [arrids objectAtIndex:1];
        }else{
            senderId = self.currentUserID;
        }
        if([senderId isEqualToString:USERID]){
            return;
        }
        if ([ChatViewController shareInstance].isChatInStory) {
            //如果是在审核群
            
            [[GetAllFamilysAndStorys shareInstance]getGroupInfoStringgroupid:[ChatViewController shareInstance].checkGroupId success:^(id JSON) {
                self.storyInfo = JSON;
                
            }failure:^(id Json) {
                
                
            }];
            NSString * creatorid = [self.storyInfo objectForKey:@"creatorid"];
            
            //如果是管理员
            if ([creatorid isEqualToString:USERID]) {
                if ([ChatViewController shareInstance].isInCheckGroup) {
                    //在审核群里
                    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"移到水聊群&对戏群" ,@"移出群",@"申请加好友", nil];
                    sheet.tag = 10000;
                    [sheet showInView:self];
                }else{
                    //非审核群
                    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"移出群",@"申请加好友", nil];
                    sheet.tag = 10001;
                    [sheet showInView:self];
                    
                }
            }else{
                UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"申请加好友", nil];
                sheet.tag = 10002;
                [sheet showInView:self];
            }
            
            
        }
        if ([ChatViewController shareInstance].isChatInFamily) {
            //家族聊天
        }
        
    }
}



#pragma mark UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //审核群管理员处理
    if (actionSheet.tag == 10000) {
        if (buttonIndex == 0) {
            //穿越到戏群和水聊群
            [self addMemberToGroup:self.storyInfo];
        }
        if (buttonIndex == 1) {
            //移出群
            [self deleteMembers];
        }
        if (buttonIndex == 2) {
            [self addFriends];
        }
    }
    //非审核群管理员处理
    if (actionSheet.tag == 10001) {
        if (buttonIndex == 0) {
            [self deleteMembers];
        }
        if (buttonIndex == 1) {
            [self addFriends];
        }
    }
    if (actionSheet.tag == 10002) {
        if (buttonIndex == 0) {
            [self addFriends];
        }
    }
    
}


#pragma mark 添加到戏群和水聊群
-(void)addMemberToGroup:(NSDictionary*)storyGroups
{
    NSArray * arr = [storyGroups objectForKey:@"groups"];
    NSMutableArray * groupId = [NSMutableArray array];
    for (int i = 0; i < [arr count]; i++) {
        NSDictionary * dic = [arr objectAtIndex:i];
        NSString * isplay = [dic objectForKey:@"isplay"];
        if([isplay isEqualToString:@"1"]||[isplay isEqualToString:@"2"]){
            //是水群或者戏群
            NSString  * groupid = [dic objectForKey:@"groupid"];
            NSString * group_id =[NSString stringWithFormat:@"group_%@",groupid];
            [groupId addObject:group_id];
        }
    }
    
    if ([groupId count]>0) {
        for (int i = 0; i < [groupId count]; i++) {
            NSString * grouuID = [groupId objectAtIndex:i];
            [[ChangeGroupMemberModel shareInsatance]addMemberNotNeedNotifygroupid:grouuID users:@[self.currentUserID ]];


        }
    }
    UIAlertView * alertView =   [[UIAlertView alloc]initWithTitle:@"穿越成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
    
}


#pragma mark 移出群成员
-(void)deleteMembers
{
    
    NSString * group_id = [NSString stringWithFormat:@"group_%@",[ChatViewController shareInstance].checkGroupId];
    [[ChangeGroupMemberModel shareInsatance]deleteMembersNoNeedNotifygroupid:group_id users:@[self.currentUserID]];
    
    UIAlertView * alertView =  [[UIAlertView alloc]initWithTitle:@"删除成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}

#pragma mark 申请加好友
-(void)addFriends
{
    
    MsgAddFriendReqAPI * addFriendReq = [[MsgAddFriendReqAPI alloc]init];
    
    [addFriendReq requestWithObject:@[self.currentUserID,@(0),@(SessionTypeSessionTypeSingle)] Completion:^(id response, NSError *error) {
        
    }];
    
    UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:@"添加请求已发送" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertview show];
}



#pragma mark 点击头像跳转

-(void)openProfilePage
{
    if (self.currentUserID) {
        NSString * senderId = @"";
        NSArray * arrids = [self.currentUserID componentsSeparatedByString:@"_"];
        if (arrids.count > 1) {
            senderId = [arrids objectAtIndex:1];
        }else{
            senderId = self.currentUserID;
        }
        if ([ChatViewController shareInstance].isChatInStory) {
            //戏群中聊天
//            if([senderId isEqualToString:USERID]){
//                YXGroupCardViewController * detailViewController = [[YXGroupCardViewController alloc] init];
//                detailViewController.theartorId = [ChatViewController shareInstance].theartorId;
//                detailViewController.groupId  =[ChatViewController shareInstance].checkGroupId;
//                detailViewController.hidesBottomBarWhenPushed = YES;
//                if (detailViewController.isAnimating) {
//                    return;
//                }
//                detailViewController.isAnimating = YES;
//                [[ChatViewController shareInstance].navigationController pushViewController:detailViewController animated:YES];
//            }else{
                OtherPersonRolrView * detailViewController = [[OtherPersonRolrView alloc] init];
                detailViewController.friendId = senderId;
                [[GetGroupRoleName shareInstance]ReturnRoleName:[ChatViewController shareInstance].checkGroupId stroryid:[ChatViewController shareInstance] .theartorId userid:senderId success:^(NSDictionary *personDic) {
                    
                    detailViewController.portrait = [personDic objectForKey:@"portrait"];
                }];
                detailViewController.groupId = [ChatViewController shareInstance].checkGroupId;
                detailViewController.thearterId = [ChatViewController shareInstance].theartorId;
                detailViewController.isChatInCheckGroup = [ChatViewController shareInstance].isInCheckGroup;
                detailViewController.hidesBottomBarWhenPushed = YES;
                if (detailViewController.isAnimating) {
                    return;
                }
                detailViewController.isAnimating = YES;
                [[ChatViewController shareInstance].navigationController pushViewController:detailViewController animated:YES];
                
//            }
        }else{
            //家族和个人聊天
            if ([senderId isEqualToString:USERID]) {
                //进入到自己的个人主页
                
                //TODO(Xc.) 这里注释了
                //                            MyCenterViewController * detailViewController = [[MyCenterViewController alloc]init];
                //                            detailViewController.fromClass=@"ViewPersonalData";
                [[DDDatabaseUtil instance]insertDraftMessage:[ChatViewController shareInstance].chatInputTextView.text sessionId:[ChatViewController shareInstance].module.sessionEntity.sessionID];
                
                //                            detailViewController.hidesBottomBarWhenPushed = YES;
                //                            [[ChatViewController shareInstance].navigationController pushViewController:detailViewController animated:YES];
                
            }else{
                //                            FriendCenterViewController * detailViewController = [[FriendCenterViewController alloc]init];
                //                            detailViewController.iscanchat = YES;//从聊天页面进入个人主页时不允许聊天
                //                            detailViewController.hidesBottomBarWhenPushed = YES;
                //                            detailViewController.FriendID = senderId;
                [[DDDatabaseUtil instance]insertDraftMessage:[ChatViewController shareInstance].chatInputTextView.text sessionId:[ChatViewController shareInstance].module.sessionEntity.sessionID];
                
                //                            [[ChatViewController shareInstance].navigationController pushViewController:detailViewController animated:YES];
            }
            YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];
            [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:senderId];
            detailViewController.hidesBottomBarWhenPushed = YES;
            if (detailViewController.isAnimating) {
                return;
            }
            detailViewController.isAnimating = YES;
            [[ChatViewController shareInstance].navigationController pushViewController:detailViewController animated:YES];
            
            
        }
    }
    
}


-(void)clickTheSendAgain
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"重发" message:@"是否重新发送此消息" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[DDTcpClientManager instance]reconnect];
        [self clickTheSendAgain:nil];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setContent:(DDMessageEntity*)content
{
    
    id<DDChatCellProtocol> cell = (id<DDChatCellProtocol>)self;
    //设置头像位置
    
    switch (self.location) {
        case DDBubbleLeft:
            self.userAvatar.left = dd_avatarEdge;
            self.vipWriteImageView.left = dd_avatarEdge + 24;
            break;
        case DDBubbleRight:
            self.userAvatar.right = ScreenWidth - dd_avatarEdge;
            self.vipWriteImageView.right = ScreenWidth - dd_avatarEdge;
            
            break;
        default:
            break;
    }
    [self.userAvatar setContentMode:UIViewContentModeScaleAspectFill];
    [self.userAvatar setClipsToBounds:YES];
    self.userAvatar.layer.cornerRadius = self.userAvatar.frame.size.width /2.0;
    [self.userAvatar setTop:dd_bubbleUpDown];
    self.currentUserID=content.senderId;
    //[self.userAvatar setImage:[UIImage imageNamed:@"user_placeholder"]];
    
    //设置气泡位置
    CGSize size = [cell sizeForContent:content];
    float bubbleY = dd_bubbleUpDown;
    float bubbleheight = [cell contentUpGapWithBubble] + size.height + [cell contentDownGapWithBubble] +10;
    float bubbleWidth = [cell contentLeftGapWithBubble] + size.width + [cell contentRightGapWithBubble];
    float bubbleX = 0;
    UIImage* bubbleImage = nil;
    switch (self.location)
    {
        case DDBubbleLeft:
            [self.userName setHidden:YES];
            bubbleImage = [UIImage imageNamed:@"leftBubble"];
            bubbleX = dd_avatarEdge + self.userAvatar.width + dd_avatarBubbleGap;
            break;
        case DDBubbleRight:
            [self.userName setHidden:YES];
            bubbleImage = [UIImage imageNamed:@"rightBubble"];
            bubbleX =  ScreenWidth - dd_avatarEdge - self.userAvatar.width - dd_avatarBubbleGap - bubbleWidth;
            break;
        default:
            break;
    }
    _bubbleImageView.image = bubbleImage;
    if (self.session.sessionType == SessionTypeSessionTypeSingle) {
        [self.userName setHidden:YES];
        NSString* myUserID = [RuntimeStatus instance].user.objID;
        if ([content.senderId isEqualToString:myUserID]) {
            //自己的头像
            [self.bubbleImageView setFrame:CGRectMake(bubbleX,dd_avatarEdge*2, bubbleWidth, bubbleheight-6)];
            
            NSString * portrait = [[NSUserDefaults standardUserDefaults]objectForKey:@"portrait"];
            
            int isVip = [portrait isSignedUser];
            UIImage * image = [UIImage imageNamed:@"vipWriter"];
            UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
            switch (isVip) {
                case 0:
                    self.vipWriteImageView.hidden = YES;
                    break;
                case 1:
                   self.vipWriteImageView.hidden = NO;
                    
                    self.vipWriteImageView.image = image;
                    
                    break;
                case 2:
                    self.vipWriteImageView.hidden = NO;
                    
                    self.vipWriteImageView.image = officalImage;
                    
                    break;
                case 3:
                    self.vipWriteImageView.hidden = NO;
                    
                    self.vipWriteImageView.image = officalImage;
                    
                    break;
                default:
                    break;
            }

            
            [self.userAvatar sd_setImageWithURL:[NSURL URLWithString:[portrait stringOfW60ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan.png"]];
            
        }else{
            [self.bubbleImageView setFrame:CGRectMake(bubbleX,dd_avatarEdge*2, bubbleWidth, bubbleheight-6)];
            
            [[DDUserModule shareInstance] getUserForUserID:content.senderId Block:^(DDUserEntity *user) {
                NSString * portrait = user.avatar;
                
                int isVip = [portrait isSignedUser];
                UIImage * image = [UIImage imageNamed:@"vipWriter"];
                UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
                switch (isVip) {
                    case 0:
                        self.vipWriteImageView.hidden = YES;
                        break;
                    case 1:
                        self.vipWriteImageView.hidden = NO;
                        
                        self.vipWriteImageView.image = image;
                        
                        break;
                    case 2:
                        self.vipWriteImageView.hidden = NO;
                        
                        self.vipWriteImageView.image = officalImage;
                        
                        break;
                    case 3:
                        self.vipWriteImageView.hidden = NO;
                        
                        self.vipWriteImageView.image = officalImage;
                        
                        break;
                    default:
                        break;
                }
                
                [self.userAvatar sd_setImageWithURL:[NSURL URLWithString:[portrait stringOfW60ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan.png"]];
                if (content.msgContent != NULL) {
                    //                [self setContentAttributedText:content.msgContent];
                    //        [self.countLbl setText:[NSString stringWithFormat:@"%d个字",[HZSInstances countWord:content.msgContent]]];
                    //                [self.countLbl setText:[NSString stringWithFormat:@"%ld个字",content.msgContent.length]];
                }
            }];
        }
        
    }else{
        //群聊界面
        /****////剧群聊天界面
        if ([ChatViewController shareInstance].isChatInFamily ) {
            [self  setFamilyMembersPortrait:content];
        }
        if ([ChatViewController shareInstance].isChatInStory) {
            [self setTheaterMembersPortrait:content];
        }
        
        [self.bubbleImageView setFrame:CGRectMake(bubbleX, bubbleY+20, bubbleWidth, bubbleheight-6)];
        if (self.location != DDBubbleRight) {
            [self.userName setHidden:NO];
            self.userName.frame = CGRectMake(60, 10, 100, 15);
            
        }else{
            [self.bubbleImageView setFrame:CGRectMake(bubbleX,bubbleY, bubbleWidth, bubbleheight-6)];
            
            //            [self.userName setFrame:CGRectMake(ScreenWidth - dd_avatarEdge - self.userAvatar.width - dd_avatarBubbleGap-80,10, 100, 15)];
            [self.userName setHidden:YES];
            
        }
    }
    
    _bubbleImageView.image = [bubbleImage stretchableImageWithLeftCapWidth:bubbleImage.size.width-20 topCapHeight:bubbleImage.size.height-10];
    
    //设置菊花位置
    switch (self.location)
    {
        case DDBubbleLeft:
            self.activityView.left = self.bubbleImageView.right + 10;
            self.sendFailuredImageView.left = self.bubbleImageView.right + 10;
            break;
        case DDBubbleRight:
            self.activityView.right = self.bubbleImageView.left - 10;
            self.sendFailuredImageView.right = self.bubbleImageView.left - 10;
            break;
        default:
            break;
    }
    
    DDImageShowMenu showMenu = 0;
    
    switch (content.state)
    {
        case DDMessageSending:
            [self.activityView startAnimating];
            self.sendFailuredImageView.hidden = YES;
            break;
        case DDMessageSendFailure:
            [self.activityView stopAnimating];
            self.sendFailuredImageView.hidden = NO;
            showMenu = DDShowSendAgain;
            break;
        case DDmessageSendSuccess:
            [self.activityView stopAnimating];
            self.sendFailuredImageView.hidden = YES;
            break;
    }
    
    self.activityView.centerY = (self.bubbleImageView.frame.origin.y +self.bubbleImageView.frame.size.height)/2.0 + 10;
    self.sendFailuredImageView.centerY = (self.bubbleImageView.frame.origin.y +self.bubbleImageView.frame.size.height)/2.0 +10;
    //设置菜单
    switch (content.msgContentType) {
        case DDMessageTypeImage:
            showMenu = showMenu |DDShowPreview;
            break;
        case DDMessageTypeText:
            showMenu = showMenu | DDShowCopy;
            break;
        case DDMessageTypeVoice:
            showMenu = showMenu | DDShowEarphonePlay | DDShowSpeakerPlay;
            break;
            
    }
    [self.bubbleImageView setShowMenu:showMenu];
    
    //设置内容位置
    [cell layoutContentView:content ];
    
}


#pragma mark 获取家族聊天列表的成员头像
-(void)setFamilyMembersPortrait :(DDMessageEntity*)content
{
    
    //家族的聊天页面
    NSString * group_id = [ChatViewController shareInstance].checkGroupId;
    [[DDUserModule shareInstance]getUserForUserID:content.sessionId Block:^(DDUserEntity *user) {
        
        NSString * userid = content.senderId;
        NSString *user_id;
        NSRange range = [userid rangeOfString:@"_"];
        if (range.length > 0) {
            NSArray * arr = [userid componentsSeparatedByString:@"_"];
            user_id = [arr objectAtIndex:1];
            
        }else{
            user_id = userid;
        }
        //获取该人物的角色名
        [[GetGroupRoleName shareInstance]ReturnFamilyInfo:group_id userid:user_id success:^(NSDictionary *dic) {
            
            NSString * nickname = [dic objectForKey:@"title"];
            if (nickname && ![nickname isEqualToString:@""]) {
                [self.userName setText:nickname];
            }else{
                nickname = [dic objectForKey:@"nickname"];
                [self.userName setText:nickname];
            }
            NSString * portrait = [dic objectForKey:@"portrait"];
            
            int isVip = [portrait isSignedUser];
            UIImage * image = [UIImage imageNamed:@"vipWriter"];
            UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
            switch (isVip) {
                case 0:
                    self.vipWriteImageView.hidden = YES;
                    break;
                case 1:
                    self.vipWriteImageView.hidden = NO;
                    
                    self.vipWriteImageView.image = image;
                    
                    break;
                case 2:
                    self.vipWriteImageView.hidden = NO;
                    
                    self.vipWriteImageView.image = officalImage;
                    
                    break;
                case 3:
                    self.vipWriteImageView.hidden = NO;
                    
                    self.vipWriteImageView.image = officalImage;
                    
                    break;
                default:
                    break;
            }

            [self.userAvatar sd_setImageWithURL:[NSURL URLWithString:[portrait stringOfW60ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan.png"]];
        }];
        
        //     [self.contentLbl setText:content.msgContent];
        if (content.msgContent != NULL) {
            //                    [self setContentAttributedText:content.msgContent];
            //                    [self.countLbl setText:[NSString stringWithFormat:@"%d个字",[HZSInstances countWord:content.msgContent]]];
        }
        
    }];
    
}


#pragma mark 获取剧群成员的头像
-(void)setTheaterMembersPortrait:(DDMessageEntity*)content
{
    NSString * groupid = [ChatViewController shareInstance].checkGroupId;
    NSString * theaterId = [ChatViewController shareInstance].theartorId;
//    [[DDUserModule shareInstance]getUserForUserID:content.sessionId Block:^(DDUserEntity *user) {
    
        NSString * userid = content.senderId;
        NSString *user_id=@"";
        NSRange range = [userid rangeOfString:@"_"];
        if (range.length > 0) {
            NSArray * arr = [userid componentsSeparatedByString:@"_"];
            user_id = [arr objectAtIndex:1];
            
        }else{
            user_id = userid;
        }
        //获取该人物的角色名
        [[GetGroupRoleName shareInstance]ReturnRoleName:groupid stroryid:theaterId userid:user_id success:^(NSDictionary *personDic) {
            
            NSString * rolename = [personDic objectForKey:@"rolename"];
            NSString * nickname = [personDic objectForKey:@"nickname"];
            NSString * roleTitle = [personDic objectForKey:@"title"];
            NSString * portrait = [personDic objectForKey:@"portrait"];
            
            int isVip = [portrait isSignedUser];
            UIImage * image = [UIImage imageNamed:@"vipWriter"];
            UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
            switch (isVip) {
                case 0:
                    self.vipWriteImageView.hidden = YES;
                    break;
                case 1:
                    self.vipWriteImageView.hidden = NO;
                    
                    self.vipWriteImageView.image = image;
                    
                    break;
                case 2:
                    self.vipWriteImageView.hidden = NO;
                    
                    self.vipWriteImageView.image = officalImage;
                    
                    break;
                case 3:
                    self.vipWriteImageView.hidden = NO;
                    
                    self.vipWriteImageView.image = officalImage;
                    
                    break;
                default:
                    break;
            }
            [self.userAvatar sd_setImageWithURL:[NSURL URLWithString:[portrait stringOfW60ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan.png"]];
            
            NSString * preName = @"";

            if (rolename.length > 0 && ![rolename isEqualToString:@""] && ![rolename isEqual:[NSNull null]] && rolename !=nil) {
                preName = [NSString stringWithFormat:@"[%@]%@",roleTitle,rolename];
            }
            else {
                preName = nickname;
            }
        [self.userName setText:preName];

            
//            if(preName && extrenName)
//            {
//                rockname = [NSString stringWithFormat:@"%@+%@",preName,extrenName];
//            } else
//            {
//                if (preName) {
//                    rockname = preName;
//                }else{
//                    rockname = extrenName;
//                }
//            }
//            [self.userName setText:rockname];
        }];
        //        [self.contentLbl setText:contnent.msgContent];
        if(content.msgContent !=  NULL) {
            //            [self setContentAttributedText:contnent.msgContent];
            //            [self.countLbl setText:[NSString stringWithFormat:@"%d个字",[HZSInstances countWord:contnent.msgContent]]];
        }
//    }];
    
    
}



- (void)showSendFailure
{
    [self.activityView stopAnimating];
    self.sendFailuredImageView.hidden = NO;
    DDImageShowMenu showMenu = self.bubbleImageView.showMenu | DDShowSendAgain;
    [self.bubbleImageView setShowMenu:showMenu];
    //    [[ChatViewController shareInstance].chatTableView reloadData];
}

- (void)showSendSuccess
{
    [self.activityView stopAnimating];
    self.sendFailuredImageView.hidden = YES;
    //    [[ChatViewController shareInstance].chatTableView reloadData];
    
}

- (void)showSending
{
    [self.activityView startAnimating];
    self.sendFailuredImageView.hidden = YES;
    //    [[ChatViewController shareInstance].chatTableView reloadData];
    
}

#pragma mark -
#pragma mark DDMenuImageView Delegate
- (void)clickTheCopy:(MenuImageView*)imageView
{
    //子类去继承
}

- (void)clickTheEarphonePlay:(MenuImageView*)imageView
{
    //子类去继承
}

- (void)clickTheSpeakerPlay:(MenuImageView*)imageView
{
    //子类去继承
}

- (void)clickTheSendAgain:(MenuImageView*)imageView
{
    //子类去继承
}

- (void)tapTheImageView:(MenuImageView*)imageView
{
    if (self.tapInBubble)
    {
        self.tapInBubble();
    }
}

- (void)clickThePreview:(MenuImageView *)imageView
{
    //子类去继承
    
}

- (void)SaveThePreview:(MenuImageView *)imageView
{
    //子类去继承
    
    
}


@end
