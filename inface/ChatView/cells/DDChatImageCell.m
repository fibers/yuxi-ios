//
//  DDChatImageCell.m
//  IOSDuoduo
//
//  Created by Michael Scofield on 2014-06-09.
//  Copyright (c) 2014 murray. All rights reserved.
//

#import "DDChatImageCell.h"
#import "UIImageView+WebCache.h"
//#import "DDChatImagePreviewViewController.h"
#import "UIView+DDAddition.h"
#import "NSDictionary+JSON.h"
#import "PhotosCache.h"
#import "DDDatabaseUtil.h"
#import "DDMessageSendManager.h"
#import "DDSendPhotoMessageAPI.h"
#import "SessionModule.h"
//#import "MJPhoto.h"
//#import "MJPhotoBrowser.h"0
#import <IDMPhoto.h>
#import <IDMPhotoBrowser.h>
#import "NSString+YXExtention.h"
//#import "UIImage+UIImageAddition.h"
@implementation DDChatImageCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.msgImgView =[[UIImageView alloc] init];
        self.msgImgView.userInteractionEnabled=YES;
        [self.msgImgView setClipsToBounds:YES];
        [self.msgImgView.layer setCornerRadius:3];
        [self.msgImgView setContentMode:UIViewContentModeScaleAspectFill];
        [self.contentView addSubview:self.msgImgView];
        //        [self bringSubviewToFront:self.bubbleImageView];
        [self.bubbleImageView setClipsToBounds:YES];
        self.msgImgView.userInteractionEnabled = NO;
        self.photos = [NSMutableArray new];
        //        [self addLongGesture];
        
    }
    return self;
}


-(void)showPreviewMessage:(DDMessageEntity *)message
{
    if (self.msgImgView.image == nil) {
        return;
    }
    [[ChatViewController shareInstance].chatInputTextView resignFirstResponder];
    [self.photos removeAllObjects];
//    NSArray * messagesCount = [ChatViewController shareInstance].module.showingMessages;
    NSMutableArray * imgsArr = [NSMutableArray array];
    //    for (int i = 0; i < messagesCount.count; i++) {
    //        //将图片的链接提取出来
    //        id object = [messagesCount objectAtIndex:i];
    //        if ([object isKindOfClass:[DDMessageEntity class]]) {
    //            DDMessageEntity * entiry = object;
    //            if (entiry.msgContentType == DDMessageTypeImage) {
    //                NSString * imagePath = entiry.msgContent;
    //                imagePath = [imagePath stringByReplacingOccurrencesOfString:DD_MESSAGE_IMAGE_PREFIX withString:@""];
    //                imagePath = [imagePath stringByReplacingOccurrencesOfString:DD_MESSAGE_IMAGE_SUFFIX withString:@""];
    //                MJPhoto * photo = [[MJPhoto alloc]init];
    //                 photo.url = [NSURL URLWithString:imagePath];
    //                [imgsArr addObject:photo];
    //            }
    //        }
    //    }
    IDMPhoto * photo = [[IDMPhoto alloc]initWithImage:self.msgImgView.image];
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:@[photo] animatedFromView:self.msgImgView];
    [self.viewController presentViewController:browser animated:YES completion:^{
        
    }];
    
    //    [self.photos addObject:[MWPhoto photoWithImage:self.msgImgView.image]];
    //
    //    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    //    browser.displayActionButton = NO;
    //    browser.displayNavArrows = NO;
    //    browser.wantsFullScreenLayout = YES;
    //    browser.zoomPhotosToFill = YES;
    //    [browser setCurrentPhotoIndex:0];
    //    DDChatImagePreviewViewController *preViewControll = [DDChatImagePreviewViewController new];
    //    preViewControll.photos=self.photos;
    
}

- (void)setContent:(DDMessageEntity*)content
{
    [super setContent:content];
    if(content.msgContentType == DDMessageTypeImage)
    {
        NSDictionary* messageContent = [NSDictionary initWithJsonString:content.msgContent];
        if (!messageContent)
        {
            NSString* urlString = content.msgContent;
            urlString = [urlString stringByReplacingOccurrencesOfString:DD_MESSAGE_IMAGE_PREFIX withString:@""];
            urlString = [urlString stringByReplacingOccurrencesOfString:DD_MESSAGE_IMAGE_SUFFIX withString:@""];
            NSURL* url = [NSURL URLWithString:urlString];
            [self showSending];
            [self.msgImgView sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [self showSendSuccess];
            }];
            
            return;
        }
        if (messageContent[DD_IMAGE_LOCAL_KEY])
        {
            //加载本地图片
            NSString* localPath = messageContent[DD_IMAGE_LOCAL_KEY];
            NSData* data = [[PhotosCache sharedPhotoCache] photoFromDiskCacheForKey:localPath];
            UIImage *image = [[UIImage alloc] initWithData:data];
            [self.msgImgView setImage:image];
        }
        else{
            //加载服务器上的图片
            NSString* url = messageContent[DD_IMAGE_URL_KEY];
            __weak DDChatImageCell* weakSelf = self;
            
            [self showSending];
            [self.msgImgView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [weakSelf showSendSuccess];
                if (error) {
                    
                }
            }];
        }
        
    }
    
    
}
#pragma mark -
#pragma mark DDChatCellProtocol Protocol
- (CGSize)sizeForContent:(DDMessageEntity*)content
{
    float height = 135;
    float width = 80;
    return CGSizeMake(width, height);
}

- (float)contentUpGapWithBubble
{
    return 1;
}

- (float)contentDownGapWithBubble
{
    return 1;
}

- (float)contentLeftGapWithBubble
{
    switch (self.location)
    {
        case DDBubbleRight:
            return 1;
        case DDBubbleLeft:
            return 8.5;
    }
    return 0;
}

- (float)contentRightGapWithBubble
{
    switch (self.location)
    {
        case DDBubbleRight:
            return 6.5;
            break;
        case DDBubbleLeft:
            return 1;
            break;
    }
    return 0;
}


#pragma mark 图片气泡的尺寸，设置图片的的高度
- (void)layoutContentView:(DDMessageEntity*)content
{
    float x = self.bubbleImageView.left + [self contentLeftGapWithBubble];
    float y = self.bubbleImageView.top + [self contentUpGapWithBubble];
    CGSize size = [self sizeForContent:content];
    //    [self bringSubviewToFront:self.bubbleImageView];
    [self.msgImgView setFrame:CGRectMake(x-7, y-2, size.width+7, size.height+5)];
    
    NSString * user_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];
    if ([content.senderId isEqualToString:user_id]) {
        [self.msgImgView setFrame:CGRectMake(x, y-2, size.width+6, size.height+5)];
        
    }
}

- (float)cellHeightForMessage:(DDMessageEntity*)message
{
    return 27 + 2 * dd_bubbleUpDown;
}
- (void)dealloc
{
    self.photos = nil;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
#pragma mark -
#pragma mark DDMenuImageView Delegate
- (void)clickTheSendAgain:(MenuImageView*)imageView
{
    //子类去继承
    if (self.sendAgain)
    {
        self.sendAgain();
    }
}
- (void)sendImageAgain:(DDMessageEntity*)message
{
    //子类去继承
    [self showSending];
    NSDictionary* dic = [NSDictionary initWithJsonString:message.msgContent];
    NSString* locaPath = dic[DD_IMAGE_LOCAL_KEY];
    __block UIImage* image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:locaPath];
    if (!image)
    {
        NSData* data = [[PhotosCache sharedPhotoCache] photoFromDiskCacheForKey:locaPath];
        image = [[UIImage alloc] initWithData:data];
        if (!image) {
            [self showSendFailure];
            return ;
        }
    }
    //先上传到优派云
    UpYun * _upYun = [[UpYun alloc]init];
    [_upYun uploadImage:image];
    WEAKSELF
    _upYun.successBlocker = ^(id data){
        if (!weakSelf) {
            return ;
        }
        
        [SVProgressHUD dismiss];
        NSString        *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
        imgUrl  =   [NSString stringWithFormat:@"%@%@%@%@",DD_MESSAGE_IMAGE_PREFIX, IMAGE_SERVER_BASE_URL, imgUrl,DD_MESSAGE_IMAGE_SUFFIX];
        message.state=DDMessageSending;
        NSDictionary* tempMessageContent = [NSDictionary initWithJsonString:message.msgContent];
        NSMutableDictionary* mutalMessageContent = [[NSMutableDictionary alloc] initWithDictionary:tempMessageContent];
        [mutalMessageContent setValue:imgUrl forKey:DD_IMAGE_URL_KEY];
        NSString* messageContent = [mutalMessageContent jsonString];
        message.msgContent = messageContent;
        
        [[DDMessageSendManager instance] sendMessage:message isGroup:[message isGroupMessage] Session:[[SessionModule sharedInstance] getSessionById:message.sessionId] completion:^(DDMessageEntity* theMessage,NSError *error) {
            if (error)
            {
                message.state = DDMessageSendFailure;
                //刷新DB
                [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
                    if (result)
                    {
                        [self showSendFailure];
                    }
                }];
            }
            else
            {
                //刷新DB
                message.state = DDmessageSendSuccess;
                //刷新DB
                [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
                    if (result)
                    {
                        [self showSendSuccess];
                    }
                }];
            }
        } Error:^(NSError *error) {
            [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
                if (result)
                {
                    [self showSendFailure];
                }
            }];
        }];
        
    };
    
    
    _upYun.failBlocker = ^(NSError * error)
    {
        [SVProgressHUD dismiss];
        [SVProgressHUD showErrorWithStatus:@"上传失败" duration:2.0];
        message.state = DDMessageSendFailure;
        //刷新DB
        [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
            if (result)
            {
                [self showSendFailure];
                
            }
        }];
    };
    
}



- (void)clickThePreview:(MenuImageView *)imageView
{
    //子类去继承
    if (self.preview)
    {
        self.preview();
    }
}

///添加长按手势
-(void)addLongGesture{
    UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(cellDidLongPressed:)];
    longPress.minimumPressDuration = 1.0f;
    [self addGestureRecognizer:longPress];
}

///长按手势被响应
-(void)cellDidLongPressed:(UILongPressGestureRecognizer*)sender{
    if(sender.state ==  UIGestureRecognizerStateBegan){
        if ([self.delegate respondsToSelector:@selector(saveImage:)]) {
            [self.delegate saveImage:self.msgImgView.image];
        }
    }
}

- (void)SaveThePreview:(MenuImageView *)imageView
{
    if ([self.delegate respondsToSelector:@selector(saveImage:)]) {
        [self.delegate saveImage:self.msgImgView.image];
    }
}
@end
