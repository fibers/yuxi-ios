//
//  DDChatCellProtocol.h
//  IOSDuoduo
//
//  Created by murray on 14-5-28.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DDMessageEntity;
@protocol DDChatCellProtocol <NSObject>

- (CGSize)sizeForContent:(DDMessageEntity*)content;

- (float)contentUpGapWithBubble;

- (float)contentDownGapWithBubble;

- (float)contentLeftGapWithBubble;

- (float)contentRightGapWithBubble;

- (void)layoutContentView:(DDMessageEntity*)content;

- (float)cellHeightForMessage:(DDMessageEntity*)message;
@end
