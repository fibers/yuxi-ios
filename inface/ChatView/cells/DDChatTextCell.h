//
//  DDChatTextCell.h
//  IOSDuoduo
//
//  Created by murray on 14-5-28.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDChatBaseCell.h"

@class DDMenuLabel;
@interface DDChatTextCell : DDChatBaseCell<DDChatCellProtocol>
@property (nonatomic,retain)CXAHyperlinkLabel* contentLabel;

-(void)sendTextAgain:(DDMessageEntity *)message;

- (void) setNaviController : (UINavigationController *) parentController;
@end
