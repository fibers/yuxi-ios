//
//  DDChatTextCell.m
//  IOSDuoduo
//
//  Created by murray on 14-5-28.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import "DDChatTextCell.h"
#import "UIView+DDAddition.h"
//#import "std.h"
#import "DDDatabaseUtil.h"
//#import "DDMessageSendManager.h"
#import "SessionModule.h"
#import <CoreText/CoreText.h>
#import "NSAttributedString+CXACoreTextFrameSize.h"

static int const fontsize = 16;
static float const maxContentWidth = 200;

@interface DDChatTextCell(PrivateAPI)
- (void)layoutLeftLocationContent:(NSString*)content;
- (void)layoutRightLocationContent:(NSString*)content;


@end

@interface DDChatTextCell() {
    
    UINavigationController * _parentController;
}

@end


@implementation DDChatTextCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.contentLabel = [[CXAHyperlinkLabel alloc] init];
 
        [self.contentView addSubview:self.contentLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setContent:(DDMessageEntity*)content
{
    
    [self.contentLabel setFont:[UIFont systemFontOfSize:fontsize]];
    [self.contentLabel setNumberOfLines:10000];
    [self.contentLabel setBackgroundColor:[UIColor clearColor]];
    [self.contentLabel setText:content.msgContent];
    if (content.msgContent != NULL && ![content.msgContent isEqual:nil]) {
        [self setContentAttributedText:content];

    }
    switch (self.location)
    {
        case DDBubbleLeft:
            [self.contentLabel setTextColor:[UIColor blackColor]];
            break;
        case DDBubbleRight:
            [self.contentLabel setTextColor:[UIColor blackColor]];
            break;
    }
    [super setContent:content];

}


#pragma mark  修改了气泡的高度

-(CGSize)sizeForContent:(DDMessageEntity *)content
{
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:content.msgContent];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:5.0];//调整行间距
    
    UIFont* font = [UIFont systemFontOfSize:fontsize];

    [attribute addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [content.msgContent length])];
    
    [attribute addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [content.msgContent length])];
        
    return  [attribute cxa_coreTextFrameSizeWithConstraints:CGSizeMake(ScreenWidth-120, 99999)];

    

}


//label和气泡上边缘距离

- (float)contentUpGapWithBubble
{
    return 12;
}

- (float)contentDownGapWithBubble
{
    return 2;
}

- (float)contentLeftGapWithBubble
{
    switch (self.location) {
        case DDBubbleLeft:
            return 20;
        case DDBubbleRight:
            return 10;
    }
}

- (float)contentRightGapWithBubble
{
    switch (self.location)
    {
        case DDBubbleLeft:
            return 10;
        case DDBubbleRight:
            return 20;
    }
}


//label的frame
- (void)layoutContentView:(DDMessageEntity*)content 
{
    float x = self.bubbleImageView.left + [self contentLeftGapWithBubble];
    float y = self.bubbleImageView.top + [self contentUpGapWithBubble];
    CGSize size = [self sizeForContent:content];
    NSString * user_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_userid"];
    [self.contentLabel setFrame:CGRectMake(x-2, y-3, size.width, size.height)];

    if([content.senderId isEqualToString:user_id])
   {
     [self.contentLabel setFrame:CGRectMake(x+3, y-3, size.width, size.height)];
   }

}

- (float)cellHeightForMessage:(DDMessageEntity*)message
{
    CGSize size = [self sizeForContent:message];
    float height = [self contentUpGapWithBubble] + [self contentDownGapWithBubble] + size.height + dd_bubbleUpDown * 2 +10;
    
    return height;
}

#pragma mark -
#pragma mark DDMenuImageView Delegate
- (void)clickTheCopy:(MenuImageView*)imageView
{
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.contentLabel.text;
}

- (void)clickTheEarphonePlay:(MenuImageView*)imageView
{
}

- (void)clickTheSpeakerPlay:(MenuImageView*)imageView
{
}

- (void)clickTheSendAgain:(MenuImageView*)imageView
{
    if (self.sendAgain)
    {
        self.sendAgain();
    }
}

- (void)tapTheImageView:(MenuImageView*)imageView
{
    //子类去继承
    [super tapTheImageView:imageView];
}
-(void)sendTextAgain:(DDMessageEntity *)message
{
    message.state = DDMessageSending;
    [self showSending];
    [[DDMessageSendManager instance] sendMessage:message isGroup:[message isGroupMessage]  Session:[[SessionModule sharedInstance] getSessionById:message.sessionId] completion:^(DDMessageEntity* theMessage,NSError *error) {
  
            [self showSendSuccess];
          
        
    } Error:^(NSError *error) {
         [self showSendFailure];
    }];

}

/**
 *  设定URL字段
 *
 */
- (void) setContentAttributedText : (DDMessageEntity *) message
{
    NSError *error;
    NSMutableArray *urls;
    NSMutableArray *urlRanges;
    NSString * text = message.msgContent;
    urlRanges   =   [[NSMutableArray alloc] init];
    urls        =   [[NSMutableArray alloc] init];
    
    NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray *arrayOfAllMatches = [regex matchesInString:text
                                                options:0
                                                  range:NSMakeRange(0, [text length])];
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:text];

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];

    [paragraphStyle setLineSpacing:5.0];//调整行间距

    UIFont* font = [UIFont systemFontOfSize:fontsize];
    
    [attribute addAttribute:NSParagraphStyleAttributeName
                      value:paragraphStyle
                      range:NSMakeRange(0, [text length])];

    [attribute addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [text length])];
    
    for (NSTextCheckingResult *match in arrayOfAllMatches) {
        //没有匹配的场合。
        if (match.range.location == NSNotFound ) {
            continue;
        }
        NSString *substringForMatch =   [text substringWithRange:match.range];
        NSURL    *suburl            =   [NSURL URLWithString:substringForMatch];
        
        [urls addObject:suburl];
         
        [urlRanges addObject:[NSValue valueWithRange:match.range]];

        [attribute addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:match.range];
        
        [attribute addAttribute:(NSString *)kCTForegroundColorAttributeName
                          value:(id)[UIColor blueColor].CGColor
                          range:match.range];
    
    }

    
    [self.contentLabel setAttributedText:attribute];
    [self.contentLabel setURLs:urls forRanges:urlRanges];
    __weak DDChatTextCell * weakself = self;
    self.contentLabel.URLClickHandler = ^(CXAHyperlinkLabel *label, NSURL *URL, NSRange range, NSArray *textRects){
        NSString    *url    =   [URL absoluteString];
        NSRange   rangeUrl = [url rangeOfString:@"www.yuxip.com"];
        if(rangeUrl.length > 0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }
        else if (_parentController != nil){
            HelpInformationViewController * detailViewController = [[HelpInformationViewController alloc] init];
            detailViewController.navTitle   =   url;
            detailViewController.contentUrl =   url;
            detailViewController.hidesBottomBarWhenPushed=YES;
            if (detailViewController.isAnimating) {
                return ;
            }
            detailViewController.isAnimating = NO;
            [[weakself getNavi] pushViewController:detailViewController animated:YES];
        }
        
    };
    
}

- (void) setNaviController : (UINavigationController *) parentController
{
    _parentController   =   parentController;
}


- (UINavigationController *) getNavi
{
    return _parentController;
}
@end
