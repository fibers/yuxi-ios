//
//  DDPromptCell.h
//  IOSDuoduo
//
//  Created by murray on 14-6-9.
//  Copyright (c) 2014年 murray. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDPromptCell : UITableViewCell
{
    UILabel* _promptLabel;
}

- (void)setprompt:(NSString*)prompt;
@end
