//
//  ChatIndexTableViewCell.m
//  inface
//
//  Created by Mac on 15/4/16.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ChatIndexTableViewCell.h"

@implementation ChatIndexTableViewCell

- (void)awakeFromNib {
    // Initialization code

    self.timeLbl.backgroundColor=COLOR(236, 236, 236, 1);
    self.nameLbl.backgroundColor=COLOR(236, 236, 236, 1);
    self.timeLbl.textColor=BLUECOLOR;
    self.nameLbl.textColor=BLUECOLOR;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
