//
//  ChatImgTableViewCell.m
//  inface
//
//  Created by Mac on 15/4/10.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ChatImgTableViewCell.h"

//#import "UpYun.h"
@implementation ChatImgTableViewCell


- (void)awakeFromNib {
    // Initialization code
    
    self.img.contentMode=UIViewContentModeScaleAspectFill;
    self.img.clipsToBounds=YES;
    [self addLongGesture];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)sendImageAgain:(DDMessageEntity*)message
{
    //子类去继承
    NSDictionary* dic = [NSDictionary initWithJsonString:message.msgContent];
    NSString* locaPath = dic[DD_IMAGE_LOCAL_KEY];

    [[DDSendPhotoMessageAPI sharedPhotoCache] uploadImage:locaPath success:^(NSString *imageURL) {
        NSDictionary* tempMessageContent = [NSDictionary initWithJsonString:message.msgContent];
        NSMutableDictionary* mutalMessageContent = [[NSMutableDictionary alloc] initWithDictionary:tempMessageContent];
        [mutalMessageContent setValue:imageURL forKey:DD_IMAGE_URL_KEY];
        NSString* messageContent = [mutalMessageContent jsonString];
        message.msgContent = messageContent;
 
        [[DDMessageSendManager instance] sendMessage:message isGroup:[message isGroupMessage] Session:[[SessionModule sharedInstance] getSessionById:message.sessionId] completion:^(DDMessageEntity* theMessage,NSError *error) {
            if (error)
            {
                message.state = DDMessageSendFailure;
                //刷新DB
                [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
                    if (result)
                    {
                        //                        [self showSendFailure];
                    }
                }];
            }
            else
            {
                //刷新DB
                message.state = DDmessageSendSuccess;
                //刷新DB
                [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
                    if (result)
                    {
                        //                        [self showSendSuccess];
                    }
                }];
            }
        } Error:^(NSError *error) {
            [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
                if (result)
                {
                    //                    [self showSendFailure];
                }
            }];
        }];
        
    } failure:^(id error) {
        message.state = DDMessageSendFailure;
        //刷新DB
        [[DDDatabaseUtil instance] updateMessageForMessage:message completion:^(BOOL result) {
            if (result)
            {
                //                [self showSendFailure];
            }
        }];
    }];
    
}



- (NSString *) displayAndUpload : (UIImage *)img
{
    UpYun * _uYun = [[UpYun alloc] init];

    
    self.img.image  =   img;
    self.evprogressView = [[EVCircularProgressView alloc]init];
    self.evprogressView.center = self.center;
    [self.evprogressView setHidden:NO];
    [self.evprogressView setProgressTintColor:[UIColor whiteColor]] ;
//        self.evprogressView.backgroundColor = [UIColor blackColor];
    
    [self.evprogressView setProgress:0 animated:YES];
    
    [self addSubview:self.evprogressView];
    
    WEAKSELF
    _uYun.successBlocker = ^(id data)
    {
        if (!weakSelf) {
            return ;
        }
        [self.evprogressView setHidden:YES];
        NSString        *imgUrl =   [data valueForKey:@"url"];
        //由返回的字符串拼接成图片特有的key
//        imgUrl  =   [NSString stringWithFormat:@"%@%@", IMAGE_SERVER_BASE_URL, imgUrl];
        if (weakSelf.delegate != nil) {
            [weakSelf.delegate saveAndSendMes:imgUrl :img];
        }
        
        weakSelf.isUpload    =   [NSNumber numberWithBool:NO];
    };
    
    
    
    _uYun.failBlocker = ^(NSError * error)
    {
        weakSelf.img.image=nil;
        [self.evprogressView setHidden:YES];
    };
    
    
    _uYun.progressBlocker = ^(CGFloat percent, long long requestDidSendBytes)
    {
        [self.evprogressView setProgress:percent animated:YES];
    };
    /**
     *	@brief	根据 UIImage 上传
     */
    
    self.isUpload    =   [NSNumber numberWithBool:YES];
    
        NSString * saveKey = [_uYun uploadImage:img];
    _key =   [IMAGE_SERVER_BASE_URL stringByAppendingString:saveKey];
    
    return saveKey;
}




- (void)setContent:(DDMessageEntity*)content
{
    self.img.contentMode=UIViewContentModeScaleAspectFill;
    self.img.clipsToBounds=YES;
    if(content.msgContentType == DDMessageTypeImage)
    {
        NSDictionary* messageContent = [NSDictionary initWithJsonString:content.msgContent];
        if (!messageContent)
        {
            NSString* urlString = content.msgContent;
            urlString = [urlString stringByReplacingOccurrencesOfString:DD_MESSAGE_IMAGE_PREFIX withString:@""];
            urlString = [urlString stringByReplacingOccurrencesOfString:DD_MESSAGE_IMAGE_SUFFIX withString:@""];
            NSURL* url = [NSURL URLWithString:urlString];
            [self.img sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];

            return;
        }
        if (messageContent[DD_IMAGE_LOCAL_KEY])
        {
            //加载本地图片
            NSString* localPath = messageContent[DD_IMAGE_LOCAL_KEY];
            NSData* data = [[PhotosCache sharedPhotoCache] photoFromDiskCacheForKey:localPath];
            UIImage *image = [[UIImage alloc] initWithData:data];
            [self.img setImage:image];
        }
        else{
            //加载服务器上的图片

            NSString* url = messageContent[DD_IMAGE_URL_KEY];
            [self.img sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error) {
                    
                }
            }];

        }
        
    }
    
    
}
#pragma mark -
#pragma mark 长按点击事件
///添加长按手势
-(void)addLongGesture{
    UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(cellDidLongPressed:)];
    longPress.minimumPressDuration = 1.0f;
    [self addGestureRecognizer:longPress];
}
///长按手势被响应
-(void)cellDidLongPressed:(UILongPressGestureRecognizer*)sender{
    if(sender.state ==  UIGestureRecognizerStateBegan){
        if ([self.delegate respondsToSelector:@selector(chatImgCellDidLongPressedWithImg:)]) {
            [self.delegate chatImgCellDidLongPressedWithImg:self.img.image];
        }
    }
}
@end
