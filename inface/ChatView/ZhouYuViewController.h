//
//  ZhouYuViewController.h
//  inface
//
//  Created by appleone on 15/7/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"

@interface ZhouYuViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *zhouyuIntroduct;

@property (assign,nonatomic)  BOOL  commentGroup;//是否是评论群

@property (assign,nonatomic)  BOOL  isAnimating;
@end
