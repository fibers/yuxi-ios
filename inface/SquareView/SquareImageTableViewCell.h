//
//  SquareImageTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/7.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SquareImageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *TitleLable;
@property (weak, nonatomic) IBOutlet UILabel *TimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *IconImageView;
@property (weak, nonatomic) IBOutlet UILabel *ContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *FavorCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *PinglunCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *ImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *ImageView3;
@property (nonatomic,weak)       UIImageView * currentImageView;
@property (weak, nonatomic) IBOutlet UIImageView *shoucangImage;

-(void)setContent:(NSDictionary *)topicDic;
@end
