//
//  UnreadMessagesTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnreadMessagesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *UserDetailLabel;//详情
@end
