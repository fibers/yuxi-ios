//
//  RecruitCellCollectionViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/13.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecruitCellCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *BackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *XianLabel;

@property (strong, nonatomic) IBOutlet UILabel *WhoLabel;//谁的剧
@property (strong, nonatomic) IBOutlet UIImageView *IconImageView;//头像
@property (strong, nonatomic) IBOutlet VerticalAlignmentLabel *BigDetailLabel;//标题
@property (strong, nonatomic) IBOutlet UILabel *TimeLabel;//时间
@property (strong, nonatomic) IBOutlet UILabel *TypeLabel;//类型
@property (strong, nonatomic) IBOutlet VerticalAlignmentLabel *DetailLabel;//详情
-(void)setConten:(NSDictionary * )recommendic;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelConstraint;


@end
