//
//  SquareTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SquareTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *TitleLable;
@property (weak, nonatomic) IBOutlet UILabel *TimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *IconImageView;
@property (weak, nonatomic) IBOutlet UILabel *ContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *FavorCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *PinglunCountLabel;
-(void)setContent:(NSDictionary *)topicDic;
@property (weak, nonatomic) IBOutlet UIImageView *shoucangimageview;

@end
