//
//  TopicCommentDetail.h
//  inface
//
//  Created by lizhen on 15/10/13.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@class Status,TopicComment;
@interface TopicCommentDetail : NSObject
@property(strong,nonatomic)Status * status;
@property(strong,nonatomic)TopicComment *mainComment;
@property(strong,nonatomic)NSArray *childComment;

//@property(nonatomic,strong)TopicComment *mainComment;
//@property(nonatomic,strong)NSArray *childComment;
//@property(nonatomic,strong)Status *status;


@end