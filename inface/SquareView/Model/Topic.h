//
//  Topic.h
//  inface
//
//  Created by 邢程 on 15/10/8.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SUser.h"
#import "MJExtension.h"
@interface Topic : NSObject

@property (nonatomic, copy  ) NSString *topicID;

@property (nonatomic, copy  ) NSString *title;

@property (nonatomic, copy  ) NSString *commentCount;

@property (nonatomic, copy  ) NSString *collectionCount;

@property (nonatomic, copy  ) NSString *isFavor;

@property (nonatomic ,copy  ) NSString * isCollection;

@property (nonatomic, strong) NSArray  *images;

@property (nonatomic, copy  ) NSString *favorCount;

@property (nonatomic, strong) SUser    *creator;

@property (nonatomic, copy  ) NSString *createTime;

@property (nonatomic, copy  ) NSString *content;

@end
