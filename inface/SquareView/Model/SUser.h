//
//  SUser.h
//  inface
//
//  Created by 邢程 on 15/10/8.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SUser : NSObject

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *nickName;

@property (nonatomic, copy) NSString *portrait;

@end
