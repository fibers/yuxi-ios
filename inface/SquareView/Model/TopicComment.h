//
//  TopicComment.h
//  inface
//
//  Created by 邢程 on 15/10/8.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SUser.h"
#import "MJExtension.h"
#import "YXSquareCommentCellTableViewCell.h"
@interface TopicComment : NSObject

@property (nonatomic, copy  ) NSString                               *commentID;

@property (nonatomic, strong) SUser                                  *toUser;

@property (nonatomic, copy  ) NSString                               *floorCount;

@property (nonatomic, copy  ) NSString                               *commentTime;

@property (nonatomic, copy  ) NSString                               *level;

@property (nonatomic, copy  ) NSString                               *totalChildCommentCount;

@property (nonatomic, strong) NSMutableArray                         *childComment;
///显示更多评论标记  666 为显示了十条
@property (nonatomic, assign) YXSquareCommentCellCommentViewShowType hasShowMoreChildCommentFlag;

@property (nonatomic, copy  ) NSString                               *favorCount;

@property (nonatomic, copy  ) NSString                               *commentContent;

@property (nonatomic, copy  ) NSString                               *isFavor;

@property (nonatomic, copy  ) NSString                               *isCollection;

@property (nonatomic, strong) SUser                                  *fromUser;

@end
