//
//  TopicComment.m
//  inface
//
//  Created by 邢程 on 15/10/8.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "TopicComment.h"
#import "TopicComment.h"
@implementation TopicComment
+ (NSDictionary *)objectClassInArray{
    return @{@"childComment" : [TopicComment class]};
}
-(NSString *)commentTime{
    NSDate * date =  [NSDate dateWithTimeIntervalSince1970:[_commentTime integerValue]];
    return [date transformToFuzzyDate];
}
@end
