//
//  TopicCommentDetail.m
//  inface
//
//  Created by lizhen on 15/10/13.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "TopicCommentDetail.h"
#import "TopicComment.h"
@implementation TopicCommentDetail
+ (NSDictionary *)objectClassInArray{
    return @{@"childComment" : [TopicComment class]};
}

@end
