//
//  Status.h
//  inface
//
//  Created by 邢程 on 15/10/8.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Status : NSObject

@property (nonatomic, copy) NSString *result;

@property (nonatomic, copy) NSString *desc;

@end
