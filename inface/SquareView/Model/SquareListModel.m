//
//  SquareListMode.m
//  inface
//
//  Created by 邢程 on 15/10/9.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "SquareListModel.h"
#import "Topic.h"
@implementation SquareListModel
+ (NSDictionary *)objectClassInArray{
    return @{@"squareList" : [Topic class]};
}
@end
