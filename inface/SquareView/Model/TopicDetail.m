//
//  TopicDetail.m
//  inface
//
//  Created by 邢程 on 15/10/8.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "TopicDetail.h"
#import "TopicComment.h"
@implementation TopicDetail
+ (NSDictionary *)objectClassInArray{
    return @{@"comment" : [TopicComment class], @"hotComment" : [TopicComment class]};
}

@end
