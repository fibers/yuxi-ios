//
//  TopicDetail.h
//  inface
//
//  Created by 邢程 on 15/10/8.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@class Status,Topic,TopicComment,SUser;
@interface TopicDetail : NSObject
@property (nonatomic, strong) Status *status;

@property (nonatomic, strong) NSMutableArray *comment;

@property (nonatomic, strong) Topic *topicDetail;

@property (nonatomic, strong) NSMutableArray *hotComment;
@end
