//
//  SquareListMode.h
//  inface
//
//  Created by 邢程 on 15/10/9.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@class Status;
@interface SquareListModel : NSObject
@property (nonatomic, strong) Status *status;

@property (nonatomic, strong) NSArray *squareList;
@end
