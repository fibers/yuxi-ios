//
//  YXNewUserTableViewCell.m
//  inface
//
//  Created by 邢程 on 15/11/27.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXNewUserTableViewCell.h"
#import "NewUserModel.h"
@interface YXNewUserTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImgV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *infoL;
@property (weak, nonatomic) IBOutlet UIButton *addFriendBtn;

@property (copy, nonatomic) NSString * fid;

@property (strong,nonatomic)Userlist * model;
@end
@implementation YXNewUserTableViewCell
-(void)setCOntentWithModel:(Userlist*)model{
    self.model = model;
    [self.titleL setText:model.name];
    [self.avatarImgV sd_setImageWithURL:[NSURL URLWithString:model.portrait] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[model.registtime integerValue]];
    [self.infoL setText:[date transformToFuzzyDate]];
    if ([model.isfriend isEqualToString:@"1"]) {
        self.addFriendBtn.selected = YES;
        [self.addFriendBtn.layer setBorderColor:[UIColor colorWithHexString:@"000000"].CGColor];
    }
}
- (void)awakeFromNib {
    [self.avatarImgV.layer setCornerRadius:21.0f];
    [self.avatarImgV setClipsToBounds:YES];
    [self.addFriendBtn.layer setCornerRadius:5];
    [self.addFriendBtn setClipsToBounds:YES];
    [self.addFriendBtn.layer setBorderWidth:0.5];
    [self.addFriendBtn.layer setBorderColor:[UIColor colorWithHexString:@"eb71a8"].CGColor];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
- (IBAction)addFriend:(UIButton*)sender {
    
    [sender.layer setCornerRadius:5];
    if(sender.isSelected){
        [sender.layer setBorderColor:[UIColor blackColor].CGColor];
    }else{
        [self addFriendWithFid:self.model.id];
        [sender.layer setBorderColor:[UIColor colorWithHexString:@"000000"].CGColor];
    }
}



//选择按钮 //添加好友
-(void)addFriendWithFid:(NSString *)fid{

    self.fid = [NSString stringWithFormat:@"user_%@",fid];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"验证消息" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"发送", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = @"请输入20字以内的验证信息";
    alert.tag = 20000;
    
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag ==20000) {
        
        if (buttonIndex == 1 &&  [alertView textFieldAtIndex:0].text.length <=20) {
            [alertView resignFirstResponder];
            MsgAddFriendReqAPI * addFriendReq = [[MsgAddFriendReqAPI alloc]init];
            NSString * addmessage = @"";
            if ([alertView textFieldAtIndex:0].text != NULL && ![[alertView textFieldAtIndex:0].text isEqual:nil]) {
                addmessage= [alertView textFieldAtIndex:0].text;
            }
            [addFriendReq requestWithObject:@[self.fid,@(0),@(SessionTypeSessionTypeSingle),addmessage] Completion:^(id response, NSError *error) {
                
                
            }];
            NSString * friendName = [NSString stringWithFormat:@"您的请求已发送给,请耐心等待对方的确认"] ;
            UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:friendName message:@"" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            self.addFriendBtn.selected = YES;
            
        }else{
            if (buttonIndex ==0) {
            }
        }
    }
    
}

@end
