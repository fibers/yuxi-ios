//
//  YXNewUserTableViewCell.h
//  inface
//
//  Created by 邢程 on 15/11/27.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Userlist;
@interface YXNewUserTableViewCell : UITableViewCell
-(void)setCOntentWithModel:(Userlist*)model;
@end
