//
//  NewUserModel.h
//  inface
//
//  Created by 邢程 on 15/11/27.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@class Userlist;
@interface NewUserModel : NSObject

@property (nonatomic, copy) NSString *result;

@property (nonatomic, strong) NSArray<Userlist *> *userlist;

@end
@interface Userlist : NSObject

@property (nonatomic, copy) NSString *gender;

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *registtime;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *portrait;

@property (nonatomic, copy) NSString *isfriend;

@end

