//
//  YXNewUserViewController.m
//  inface
//
//  Created by 邢程 on 15/11/27.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXNewUserViewController.h"
#import "NewUserModel.h"
#import "YXNewUserTableViewCell.h"
#import "MJRefresh.h"
#import "MBProgressHUD.h"
#import "YXMyCenterViewController.h"
@interface YXNewUserViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic)UITableView * tableView;
@property(strong,nonatomic)NSMutableArray<Userlist*> * userList;
@end

@implementation YXNewUserViewController
static int pageCount;
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setHidesBottomBarWhenPushed:YES];
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setTitle:@"新人认领"];
    [self loadData];
    
}

-(void)loadData{
    [HttpTool postWithPath:@"GetNewUserList" params:@{@"uid":USERID,@"index":@"0",@"count":@"40",@"token":@"110"} success:^(id JSON) {
        pageCount = 0;
        NewUserModel *model = [NewUserModel objectWithKeyValues:JSON];
        if ([model.result isEqualToString:@"1"]) {
            self.userList = [NSMutableArray arrayWithArray:model.userlist];
            [self.view addSubview:self.tableView];
            [self.tableView reloadData];
        }else{
            [self .tableView footerEndRefreshing];
             [self showMessage:@"服务器错误"];
            
        }
    } failure:^(NSError *error) {
        [self .tableView footerEndRefreshing];
        
    }];
}
-(void)loadMoreData{
    [HttpTool postWithPath:@"GetNewUserList" params:@{@"uid":USERID,@"index":[NSString stringWithFormat:@"%ld",[self.userList count]],@"count":@"40",@"token":@"110"} success:^(id JSON) {
        NewUserModel *model = [NewUserModel objectWithKeyValues:JSON];
        if ([model.result isEqualToString:@"1"]) {
            pageCount++;
            [self.userList addObjectsFromArray:model.userlist];
            [self.view addSubview:self.tableView];
            [self.tableView reloadData];
            [self.tableView footerEndRefreshing];
        }else{
            [self .tableView footerEndRefreshing];
            [self showMessage:@"服务器错误"];
        }
    } failure:^(NSError *error) {
        [self .tableView footerEndRefreshing];
        
    }];

}
-(void)showMessage:(NSString*)message{
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setMode:MBProgressHUDModeText];
    [hud setLabelText:message];
    [hud show:YES];
    [hud hide:YES afterDelay:1.5];
}
#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.userList count];;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 67;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    YXNewUserTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"YXNewUserTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"YXNewUserTableViewCell" owner:nil options:nil]lastObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setCOntentWithModel:[self.userList objectAtIndex:indexPath.row]];
    return cell;
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView addFooterWithTarget:self action:@selector(loadMoreData)];
    }
    return _tableView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * userId = [self.userList objectAtIndex:indexPath.row].id;
    
    YXMyCenterViewController * detailViewController = [[YXMyCenterViewController alloc]init];

    [detailViewController setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:userId];
    
    detailViewController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:detailViewController animated:YES];

}

@end
