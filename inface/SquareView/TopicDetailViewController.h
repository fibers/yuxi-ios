//
//  TopicDetailViewController.h
//  inface
//
//  Created by huangzengsong on 15/7/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
@protocol UpdateTopicInfoDelegate;

@interface TopicDetailViewController : BaseViewController<UIScrollViewDelegate,UIActionSheetDelegate>
{
    
    UIButton * aTopBtn;//滚动到顶端按钮
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,nonatomic) NSString * storyID;
@property(nonatomic ,strong)NSDictionary * dic;
@property(nonatomic,assign) BOOL postNoty;
@property(assign,nonatomic)    BOOL   isAnimating;

@property(unsafe_unretained,nonatomic)id<UpdateTopicInfoDelegate>delegate;
@end
@protocol UpdateTopicInfoDelegate <NSObject>

-(void)updateTopicInfo:(NSDictionary*)storyinfo storyid:(NSString*)storyid;

@end