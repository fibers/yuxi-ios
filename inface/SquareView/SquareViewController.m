//
//  SquareViewController.m
//  inface
//
//  Created by huangzengsong on 15/6/30.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "SquareViewController.h"
#import "YXTopicCommentDetailViewController.h"
@interface SquareViewController ()

@end

@implementation SquareViewController

//右上角按钮

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(100, 100, 100, 100);
    btn.backgroundColor = [UIColor redColor];
    [self.view addSubview:btn];
    
    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)btnClick{

    YXTopicCommentDetailViewController *vc = [[YXTopicCommentDetailViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}






-(void)dealloc
{
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
