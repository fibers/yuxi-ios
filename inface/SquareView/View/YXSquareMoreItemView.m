//
//  YXSquareMoreItemView.m
//  inface
//
//  Created by 邢程 on 15/10/14.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXSquareMoreItemView.h"
#import <Masonry.h>
#import <POP.h>
#define kYXSquareMoreItemViewHideNotification @"kYXSquareMoreItemViewHideNotification"
@interface YXSquareMoreItemView()
@property (weak, nonatomic) IBOutlet UIButton *jubaoBtn;
@property (weak, nonatomic) IBOutlet UIButton *shoucangBtn;
@property (weak, nonatomic) IBOutlet UIButton *pinglunBtn;

@property (strong,nonatomic)UIView * lastV;

@property(strong,nonatomic)MASConstraint * widthConstraint;

@end
@implementation YXSquareMoreItemView
+(instancetype)shareInstance{
    static YXSquareMoreItemView * moreItemView;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        moreItemView = [[[NSBundle mainBundle]loadNibNamed:@"YXSquareMoreItemView" owner:nil options:nil]lastObject];
        
    });
    return moreItemView;
    
}
-(void)awakeFromNib{
    [self.jubaoBtn addTarget:self action:@selector(jubaoBtnClicked) forControlEvents:UIControlEventTouchDown];
    [self.pinglunBtn addTarget:self action:@selector(pinglunBtnClicked) forControlEvents:UIControlEventTouchDown];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hide) name:kYXSquareMoreItemViewHideNotification object:nil];
}
-(void)jubaoBtnClicked{
    if ([self.delegate respondsToSelector:@selector(YXSquareMoreItemViewReportBtnClicked)]) {
        [self.delegate YXSquareMoreItemViewReportBtnClicked];
    }
}
-(void)pinglunBtnClicked{
    if ([self.delegate respondsToSelector:@selector(YXSquareMoreItemViewCommentBtnClicked)]) {
        [self.delegate YXSquareMoreItemViewCommentBtnClicked];
    }
}

-(void)showRightAtView:(UIButton*)btn{
    [[YXSquareMoreItemView shareInstance]removeFromSuperview];
    [[btn superview] addSubview:[YXSquareMoreItemView shareInstance]];
    [self hide];
    [self setHidden:NO];
    WS(weakSelf)
    [self mas_remakeConstraints:^(MASConstraintMaker *make) {
        weakSelf.widthConstraint =  make.width;
        weakSelf.widthConstraint.equalTo(@85);
        make.right.equalTo(btn.mas_left).offset=-6;
        make.height.equalTo(@28);
        make.centerY.equalTo(btn.mas_centerY);
    }];
//    [self layoutSubviews];
//    [UIView animateWithDuration:0.25 animations:^{
//        self.widthConstraint.offset = 135;
//        [self layoutIfNeeded];
//    }];
    
    
//    POPSpringAnimation * springAnimation = [POPSpringAnimation animationWithPropertyNamed:@"offset"];
//    springAnimation.toValue              = @(135.0);
//    springAnimation.springBounciness     = 8;
//    [self.widthConstraint pop_addAnimation:springAnimation forKey:@"aButton4"];
    
    _lastV = btn;
    _isShowing = YES;
    
}


-(void)hide{
    _isShowing = NO;
    [self setHidden:YES];
    if (_lastV == nil || ![_lastV isKindOfClass:[UIView class]]) return;
//    
//    WS(weakSelf)
//    [self mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(@0);
//        make.right.equalTo(weakSelf.lastV.mas_left).offset=-6;
//        make.height.equalTo(@28);
//        make.centerY.equalTo(weakSelf.lastV.mas_centerY);
//    }];
}
-(BOOL)isEqualToLastView:(UIView *)currentV{
    return  currentV == _lastV;
}
@end
