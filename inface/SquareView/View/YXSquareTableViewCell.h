//
//  YXSquareTableViewCell.h
//  inface
//
//  Created by 邢程 on 15/9/18.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Topic;
@interface YXSquareTableViewCell : UITableViewCell
-(void)setContentWithTopicModel:(Topic*)topic;
-(float)cellHight;
@end
