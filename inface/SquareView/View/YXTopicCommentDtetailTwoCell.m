//
//  YXTopicCommentDtetailTwoCell.m
//  inface
//
//  Created by lizhen on 15/10/10.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXTopicCommentDtetailTwoCell.h"
#import "TopicComment.h"
#import "SUser.h"
#import "YXSquareMiniCommentView.h"
@implementation YXTopicCommentDtetailTwoCell{
    UIView *_critiqueView;
    UITextField *_textField;
}

- (void)awakeFromNib {
    // Initialization code
}

-(CGFloat)setCellContentOne:(TopicComment *)topicComment andTopicCreator:(SUser *)creator{

    YXSquareMiniCommentView * miniVC = [[YXSquareMiniCommentView alloc]init];
    [self clearCellOfContent];
    miniVC.delegate = self;
    
    SUser * user;
    user=creator;

    [miniVC setMaxWidth:ScreenWidth-53];
    [miniVC setContentWithTopicCommentModel:topicComment andTopicCreator:user];
    [miniVC setFrame:CGRectMake(49, 3, ScreenWidth-53, miniVC.height)];
    /*此处将所有的控件，加到cell的contentView上，而不是直接加到cell.view中，为了清空cell的缓存*/
    [self.contentView addSubview:miniVC];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return miniVC.height+3;
}

//清空cell缓存()
-(void)clearCellOfContent{
    
    //清楚cell的缓存
    NSArray *subviews = [[NSArray alloc] initWithArray:self.contentView.subviews];
    for (UIView *subview in subviews) {
        [subview removeFromSuperview];
    }
}

//点击名字
-(void)userNickNameClicked:(SUser *)user{

    // 取得ios系统唯一的全局的广播站 通知中心
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    //设置广播内容
    SUser *name = user;
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:name,@"ThemeName",nil];
    
    //将内容封装到广播中
    [nc postNotificationName:@"ClickAfterGoToPersonCenter" object:self userInfo:dict];
}

//点击回复内容
-(void)suqareMiniCommentViewClicked:(TopicComment *)topicComment{

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    //设置广播内容
    TopicComment *Comment = topicComment;
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:Comment,@"ThemeName",nil];
    
    //将内容封装到广播中
    [nc postNotificationName:@"ClickOnName" object:self userInfo:dict];
}

//点击删除按钮
-(void)delBtnClicked:(TopicComment *)topicComment{

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    //设置广播内容
    TopicComment *Comment = topicComment;
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:Comment,@"delTopicComment",nil];
    
    //将内容封装到广播中
    [nc postNotificationName:@"delTopicComment" object:self userInfo:dict];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
