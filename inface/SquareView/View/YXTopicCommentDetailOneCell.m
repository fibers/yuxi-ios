//
//  YXTopicCommentDetailOneCell.m
//  inface
//
//  Created by lizhen on 15/10/9.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXTopicCommentDetailOneCell.h"
#import "NSAttributedString+CXACoreTextFrameSize.h"
#import "TopicComment.h"

@implementation YXTopicCommentDetailOneCell{

    UILabel *openMicPrivilegeTipsLabel;
}

- (void)awakeFromNib {
    [[self.bingView layer]setCornerRadius:4.0];
    [[self.juBaoBtn layer]setCornerRadius:4.0];
    //self.tidaiButton.layer.cornerRadius = 4.0;
    self.tidaipinglunbutton.layer.cornerRadius = 4.0;
    self.iconView.layer.masksToBounds = YES;
    self.iconView.layer.cornerRadius = 13.0;
}

-(void)setCellContentWithModel:(TopicComment *)topicComment{

    self.bingView.hidden = YES;
    [self.topicLabel setHidden:YES];
    if ([topicComment.isFavor isEqualToString:@"1"]) {
        [self.favourBtn setSelected:YES];
    }else{
        [self.favourBtn setSelected:NO];
    }
    self.nameLabel.text = topicComment.fromUser.nickName;
    self.timeLabel.text = [NSString stringWithFormat:@"%@楼   %@",topicComment.floorCount, topicComment.commentTime];
    NSURL *url = [NSURL URLWithString:[topicComment.fromUser.portrait stringOfW60ImgUrl]];
    [self.iconView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    self.topicLabel.text = topicComment.commentContent;
    self.topicLabel.numberOfLines = 0;
    
    if (self.topicLabel.text == nil) {
        
    }else{
    
    CGSize textSize = [self.topicLabel.text sizeWithFont:[UIFont systemFontOfSize:15.0]
                          constrainedToSize:CGSizeMake(ScreenWidth - 48-45, MAXFLOAT)];
    openMicPrivilegeTipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(48, 58, textSize.width, textSize.height)];
    
    openMicPrivilegeTipsLabel.text = self.topicLabel.text;
    openMicPrivilegeTipsLabel.backgroundColor = [UIColor clearColor];
    openMicPrivilegeTipsLabel.textAlignment = NSTextAlignmentLeft;
    openMicPrivilegeTipsLabel.font = [UIFont systemFontOfSize:15.0];
    openMicPrivilegeTipsLabel.numberOfLines = 0;
    
    // 调整行间距
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.topicLabel.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5.0];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.topicLabel.text length])];
    openMicPrivilegeTipsLabel.attributedText = attributedString;
    
    [self addSubview:openMicPrivilegeTipsLabel];
    [openMicPrivilegeTipsLabel sizeToFit];
    
    }
    self.topicContentHight = openMicPrivilegeTipsLabel.frame.size.height;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
