//
//  YXSquareMoreItemView.h
//  inface
//
//  Created by 邢程 on 15/10/14.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol YXSquareMoreItemViewDelegate <NSObject>
@optional
///举报按钮被点击
-(void)YXSquareMoreItemViewReportBtnClicked;
///评论按钮被点击
-(void)YXSquareMoreItemViewCommentBtnClicked;

@end
@interface YXSquareMoreItemView : UIView
-(void)showRightAtView:(UIButton*)btn;
-(void)hide;
-(BOOL)isEqualToLastView:(UIView *)currentV;
+(instancetype)shareInstance;
@property(assign,nonatomic,readonly)BOOL isShowing;
@property(weak,nonatomic)id<YXSquareMoreItemViewDelegate> delegate;
@end
