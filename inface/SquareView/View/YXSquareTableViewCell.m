//
//  YXSquareTableViewCell.m
//  inface
//
//  Created by 邢程 on 15/9/18.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXSquareTableViewCell.h"
#import "Topic.h"
#import "VerticalAlignmentLabel.h"
#import <UIImageView+WebCache.h>
@interface YXSquareTableViewCell()
//********** Veiw ************
@property (weak, nonatomic) IBOutlet UIView                 *headerBGV;
@property (weak, nonatomic) IBOutlet UIView                 *contentBGV;
///头像
@property (weak, nonatomic) IBOutlet UIImageView            *headerIMGV;
///昵称
@property (weak, nonatomic) IBOutlet UILabel                *nikeNameL;
///点赞
@property (weak, nonatomic) IBOutlet UIButton               *favorCountBtn;
///评论
@property (weak, nonatomic) IBOutlet UIButton               *commentCountBtn;
///时间
@property (weak, nonatomic) IBOutlet UILabel                *timeL;
///标题
@property (weak, nonatomic) IBOutlet UILabel                *titleL;
///内容
@property (weak, nonatomic) IBOutlet VerticalAlignmentLabel *contentL;
//组图 第1、2、3张
@property (weak, nonatomic) IBOutlet UIImageView            *imgV_1;

@property (weak, nonatomic) IBOutlet UIImageView            *imgV_2;

@property (weak, nonatomic) IBOutlet UIImageView            *imgV_3;
///图片总张数
@property (weak, nonatomic) IBOutlet UIButton               *imgVCountBtn;
@property (weak, nonatomic) IBOutlet UIImageView *vipImgV;

//********* 约束 ***************
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentBGVCH;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentCountBtnCW;

@property (weak, nonatomic) IBOutlet UIButton           *favorCountBtnCW;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentLCH;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fristImgCH;

@property(assign,nonatomic)CGFloat cellH;
@property(assign,nonatomic)CGFloat firstImgH;
@end
@implementation YXSquareTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
-(void)initView{
    [self.imgV_1 setHidden:NO];
    [self.imgV_2 setHidden:NO];
    [self.imgV_3 setHidden:NO];
    [self.imgVCountBtn setHidden:NO];
    self.contentLCH.constant = 60;
    self.fristImgCH.constant = 75;
    _firstImgH = 75;
}
-(void)setContentWithTopicModel:(Topic*)topic{
    [self initView];
    if ([topic.creator.portrait isSignedUser]) {
        self.vipImgV.hidden = NO;
    }else{
        self.vipImgV.hidden = YES;
        
    }
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
    int isVip = [topic.creator.portrait isSignedUser];
    if (isVip == 0) {
        self.vipImgV.hidden = YES;

    }else{
        if (isVip == 1) {
            self.vipImgV.hidden = NO;
            self.vipImgV.image = image;
        }
        if (isVip == 2 || isVip == 3) {
            self.vipImgV.hidden = NO;
            self.vipImgV.image = officalImage;
        }
    }
    [self.contentL setLineBreakMode:NSLineBreakByTruncatingTail];
    [self.contentL setVerticalAlignment:VerticalAlignmentTop];
    [self.headerIMGV sd_setImageWithURL:[NSURL URLWithString:[topic.creator.portrait stringOfW100ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    [self.nikeNameL setText:topic.creator.nickName];
    [self.favorCountBtn setTitle:topic.favorCount forState:UIControlStateNormal];
    [self.favorCountBtn setSelected:[topic.isFavor isEqualToString:@"1"]];
    [self.commentCountBtn setTitle:topic.commentCount forState:UIControlStateNormal];
    [self.timeL setText:topic.createTime];
    [self.titleL setText:topic.title];
    [self.contentL setText:topic.content];
    if (!topic.content||[topic.content isEqual:@""]) {
        self.contentLCH.constant = 0;
    }
    NSInteger imageCount = [topic.images count];
    if (imageCount == 0) {
        [self.imgV_1 setHidden:YES];
        [self.imgV_2 setHidden:YES];
        [self.imgV_3 setHidden:YES];
        self.fristImgCH.constant = 0;
        _firstImgH = 0;
        [self.imgVCountBtn setHidden:YES];
    }else if(imageCount == 1){
        [self.imgV_1 sd_setImageWithURL:[NSURL URLWithString:[topic.images[0] stringOfW250ImgUrl]] placeholderImage:[UIImage imageNamed:@"huatixiangqing"]];
        [self.imgV_2 setHidden:YES];
        [self.imgV_3 setHidden:YES];
    }else if(imageCount == 2){
        [self.imgV_1 sd_setImageWithURL:[NSURL URLWithString:[topic.images[0] stringOfW250ImgUrl] ]placeholderImage:[UIImage imageNamed:@"huatixiangqing"]];
        [self.imgV_2 sd_setImageWithURL:[NSURL URLWithString:[topic.images[1] stringOfW250ImgUrl] ]placeholderImage:[UIImage imageNamed:@"huatixiangqing"]];
        [self.imgV_3 setHidden:YES];
        
    }else{
        [self.imgV_1 sd_setImageWithURL:[NSURL URLWithString:[topic.images[0] stringOfW250ImgUrl] ] placeholderImage:[UIImage imageNamed:@"huatixiangqing"]];
        [self.imgV_2 sd_setImageWithURL:[NSURL URLWithString:[topic.images[1] stringOfW250ImgUrl] ] placeholderImage:[UIImage imageNamed:@"huatixiangqing"]];
        [self.imgV_3 sd_setImageWithURL:[NSURL URLWithString:[topic.images[2] stringOfW250ImgUrl] ] placeholderImage:[UIImage imageNamed:@"huatixiangqing"]];
        
    }
    [self.imgVCountBtn setTitle:[NSString stringWithFormat:@"共%ld张",imageCount] forState:UIControlStateNormal];
    [self setContentFrameWithTopicModel:topic];
    
}
-(void)setContentFrameWithTopicModel:(Topic *)topic{
//    LOG(@"%@",topic.content);
    CGFloat contentLH = [topic.content boundingRectWithSize:CGSizeMake(ScreenWidth -24, 60) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.height;
        self.contentLCH.constant = contentLH;
        self.contentBGVCH.constant = 65 +contentLH+_firstImgH;
    _cellH = 50+ 65 +contentLH+_firstImgH;
}
-(float)cellHight{
    return _cellH -12;
}
@end
