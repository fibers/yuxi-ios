//
//  YXSquareCommentButtomView.h
//  inface
//
//  Created by 邢程 on 15/10/15.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXSquareCommentButtomView : UIView
@property (weak, nonatomic) IBOutlet UIButton *collectionBtn;
@property (weak, nonatomic) IBOutlet UIButton *favorBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;

@end
