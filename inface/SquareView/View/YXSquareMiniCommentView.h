//
//  YXSquareMiniCommentView.h
//  inface
//
//  Created by 邢程 on 15/10/10.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TopicComment;
@class SUser;


@protocol YXSquareMiniCommentViewDelegate <NSObject>
@optional
-(void)userNickNameClicked:(SUser *)user;
-(void)delBtnClicked:(TopicComment *)topicComment;
/**
 *  miniView 被点击 （用来判断回复哪个用户）
 *
 *  @param user User Model
 */
-(void)suqareMiniCommentViewClicked:(TopicComment *)topicComment;
@end


@interface YXSquareMiniCommentView : UIView

@property(nonatomic,weak)id<YXSquareMiniCommentViewDelegate>delegate;
/**
 *  设置内容
 *
 *  @param topicComment 内容model
 */
-(void)setContentWithTopicCommentModel:(TopicComment *)topicComment andTopicCreator:(SUser *)creator;

/**
 *  获取view 的高度
 *
 *  @return 当前view的高度，注意一定要先设置frame
 */
-(CGFloat)height;

/**
 *  设置label的最大宽度
 *
 *  @param maxW 宽度
 */
-(void)setMaxWidth:(CGFloat)maxW;
@end
