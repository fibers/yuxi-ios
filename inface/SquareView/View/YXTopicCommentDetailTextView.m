//
//  YXTopicCommentDetailTextView.m
//  inface
//
//  Created by lizhen on 15/10/17.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXTopicCommentDetailTextView.h"
#import <Masonry.h>

@interface YXTopicCommentDetailTextView()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (weak, nonatomic) IBOutlet UIButton *button;


@end
@implementation UITextView (yx)
-(CGFloat)height{
    if ([self.text isKindOfClass:[NSString class]] ||[self.text isKindOfClass:[NSMutableString class]]) {
        return  [self.text boundingRectWithSize:CGSizeMake(self.frame.size.width, 9999999) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:self.font} context:nil].size.height;
        
    }else{
        return 0;
    }
}
-(CGFloat)width{
    return [self.text sizeWithAttributes:@{NSFontAttributeName:self.font}].width;
}
@end
@implementation YXTopicCommentDetailTextView
-(instancetype)init{
    if ((self = [[[NSBundle mainBundle]loadNibNamed:@"YXSquareCommentTextView" owner:nil options:nil]lastObject])) {
        self.textView.layer.borderColor = [UIColor colorWithHexString:@"dcdcdd"].CGColor;
        self.textView.delegate = self;
        [self.button addTarget:self action:@selector(sendBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
-(void)sendBtnClicked{
    if ([self.delegate respondsToSelector:@selector(squareCommentTextViewSendButtonClicked)]) {
        [self.delegate squareCommentTextViewSendButtonClicked];
    }
}
-(void)setEditing:(BOOL)isEditing{
    if (isEditing) {
        [self.textView becomeFirstResponder];
    }else{
        [self.textView resignFirstResponder];
    }
}
-(void)textViewDidChange:(UITextView *)textView{
    if([textView width]+8>=textView.frame.size.width){
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@80);
        }];
    }
}

-(NSString *)text{
    return  self.textView.text;
}
-(void)setText:(NSString *)str{
    [self.textView setText:str==nil?@"":str];
}
@end
