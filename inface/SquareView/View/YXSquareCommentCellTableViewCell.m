//
//  YXSquareCommentCellTableViewCell.m
//  inface
//
//  Created by 邢程 on 15/10/10.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXSquareCommentCellTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TopicComment.h"
#import "YXSquareMoreItemView.h"
#import "YXSquareMiniCommentView.h"
#import <Masonry/Masonry.h>
#import <POP.h>
@interface YXSquareCommentCellTableViewCell()<YXSquareMiniCommentViewDelegate,YXSquareMoreItemViewDelegate>
@property (strong,nonatomic)TopicComment * topicComment;
///---
@property (weak, nonatomic ) IBOutlet UIImageView          *headerImgV;
@property (weak, nonatomic ) IBOutlet UILabel              *nickNameL;
@property (weak, nonatomic ) IBOutlet UILabel              *floorNumL;
@property (weak, nonatomic ) IBOutlet UILabel              *timeL;
@property (weak, nonatomic ) IBOutlet UILabel              *comtentL;
@property (weak, nonatomic ) IBOutlet UILabel              *favorL;
@property (weak, nonatomic ) IBOutlet UIButton             *favorBtn;
@property (weak, nonatomic ) IBOutlet UIButton             *moreBtn;
@property (weak, nonatomic ) IBOutlet UIView               *childCommentBGV;
@property (weak, nonatomic ) IBOutlet UIView               *lineV;
@property (weak, nonatomic ) IBOutlet UIButton             *rightMoreBtn;
@property (weak, nonatomic) IBOutlet UIImageView           *vipImageView;

///-----
@property (weak, nonatomic ) IBOutlet NSLayoutConstraint   *floorLCW;
@property (weak, nonatomic ) IBOutlet NSLayoutConstraint   *comtentLCH;
@property (weak, nonatomic ) IBOutlet NSLayoutConstraint   *favorLCW;
@property (weak, nonatomic ) IBOutlet NSLayoutConstraint   *childCommentGBVCH;

@property (strong,nonatomic) YXSquareMoreItemView *squareMoreItemView;
@property (assign,nonatomic) CGFloat              cellH;
@end

@implementation UILabel (yx)
-(CGFloat)height{
    if ([self.text isKindOfClass:[NSString class]] ||[self.text isKindOfClass:[NSMutableString class]]) {
        
        CGFloat labelH = [self.text boundingRectWithSize:CGSizeMake(self.frame.size.width, 9999999) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:self.font} context:nil].size.height;
        return  labelH;
 
    }else{
        return 0;
    }
}

-(CGFloat)width{
    return [self.text sizeWithAttributes:@{NSFontAttributeName:self.font}].width;
}
@end
@implementation YXSquareCommentCellTableViewCell
-(CGSize)sizeThatFits:(CGSize)size{
    
    return CGSizeMake(ScreenWidth, _cellH);
}

-(void)setContentWithTopicCommentModel:(TopicComment *)topicComment andTopicOwnerID:(SUser *)creator{
    //>>init
    [self.childCommentBGV setHidden:NO];
    [[self.childCommentBGV subviews]enumerateObjectsUsingBlock:^( UIView * obj, NSUInteger idx, BOOL *  stop) {
        if ([obj isKindOfClass:[YXSquareMiniCommentView class]]) {
            [obj removeFromSuperview];
        }
    }];
    
    //<<
    _cellH = 80;
    _topicComment = topicComment;
    
    int isSigned = [topicComment.fromUser.portrait isSignedUser];
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];

    switch (isSigned) {
        case 0:
            _vipImageView.hidden = YES;
            break;
            
        case 1:
            _vipImageView.hidden = NO;

            _vipImageView.image = image;
            break;
        case 2:
            _vipImageView.hidden = NO;
            _vipImageView.image = officalImage;

            break;
            
        case 3:
            _vipImageView.hidden = NO;
            _vipImageView.image = officalImage;

            break;
        default:
            break;
    }
    [self.headerImgV sd_setImageWithURL:[NSURL URLWithString:[topicComment.fromUser.portrait stringOfW100ImgUrl]] placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    
    [self.nickNameL setText:topicComment.fromUser.nickName];
    [self.floorNumL setText:[NSString stringWithFormat:@"%@楼",topicComment.floorCount]];
    [self.timeL setText:topicComment.commentTime];
    [self.favorL setText:topicComment.favorCount];
    [self.favorBtn setSelected:[topicComment.isFavor isEqualToString:@"1"]];
    [self.comtentL setText:topicComment.commentContent];
    [self autoResizeSubviews];
    if (0!=[topicComment.childComment count]) {
        [self renderChildCommentViewWithModel:topicComment.childComment andTopicOwnerID:creator];
    }else{
        [self.moreBtn setHidden:YES];
        [self.lineV setHidden:YES];
        [self.childCommentBGV setHidden:YES];
    }
    
    if (topicComment.hasShowMoreChildCommentFlag == YXSquareCommentCellCommentViewShowNormal) {
        NSInteger topicCommentNum = [topicComment.childComment count]-10;
        if(topicCommentNum <=0){
            [self.moreBtn setHidden:YES];
        }else{
            [self.moreBtn setHidden:NO];
            [self.moreBtn setTitle:[NSString stringWithFormat:@"更多%ld条回复..",topicCommentNum] forState:UIControlStateNormal];
        }
    }else if(topicComment.hasShowMoreChildCommentFlag == YXSquareCommentCellCommentViewShowTenItems){
        if([self.delegate respondsToSelector:@selector(commentResouceWithTopicComment:)]){
            [self.delegate commentResouceWithTopicComment:self.topicComment];
        }
    
    }else{
        NSInteger topicCommentNum = [topicComment.childComment count]-2;
        if(topicCommentNum <=0){
            [self.moreBtn setHidden:YES];
        }else{
            [self.moreBtn setHidden:NO];
            [self.moreBtn setTitle:[NSString stringWithFormat:@"更多%ld条回复..",topicCommentNum] forState:UIControlStateNormal];
        }
    }
    
}
#pragma mark -
#pragma mark miniView Delegate
-(void)suqareMiniCommentViewClicked:(TopicComment *)topicComment{
    __block SUser * fromeUser;
    if ([self.delegate respondsToSelector:@selector(miniCommentViewClickedWithUser:andCellModel:)]) {
    [self.topicComment.childComment enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
        if (obj == topicComment) {
            TopicComment * topicCommentItem = [self.topicComment.childComment objectAtIndex:idx];
            fromeUser  = topicCommentItem.fromUser;
            [self.delegate miniCommentViewClickedWithUser:fromeUser andCellModel:self.topicComment];
            *stop = YES;
        }
    }];
    }
    
    
}
- (IBAction)favorBtnClicked:(UIButton *)sender {
    if (!sender.isSelected) {
        [self.favorL setText:[NSString stringWithFormat:@"%ld",[self.favorL.text integerValue] +1]];
    }else{
        [self.favorL setText:[NSString stringWithFormat:@"%ld",[self.favorL.text integerValue] -1]];
    }
    self.topicComment.favorCount = self.favorL.text;
    sender.selected = !sender.selected;
    
    if ([self.delegate respondsToSelector:@selector(squareCommentfavorBtnClicked:andResourceID:)]) {
        [self.delegate squareCommentfavorBtnClicked:self.favorBtn andResourceID:self.topicComment.commentID];
    }
}


-(void)userNickNameClicked:(SUser *)user{
    if ([self.delegate respondsToSelector:@selector(miniCommentViewClickedNickName:)]) {
        [self.delegate miniCommentViewClickedNickName:user];
    }
}

-(void)delBtnClicked:(TopicComment *)topicComment{
    if ([self.delegate respondsToSelector:@selector(miniCommentViewClickedDeleteBtn:)]) {
        [self.topicComment.childComment enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
            if (obj == topicComment) {
                [self.topicComment.childComment removeObjectAtIndex:idx];
                *stop = YES;
            }
        }];
        [self.delegate miniCommentViewClickedDeleteBtn:topicComment];
    }

}

/**
 *  生成子评论（楼层的评论）
 *
 *  @param childComment 子评论数组
 */
-(void)renderChildCommentViewWithModel:(NSArray *)childComment andTopicOwnerID:(SUser *)creator{

    
    __block YXSquareMiniCommentView * miniViewTMP = nil;
    ///背景高度
    __block int bgVH = 0;
    int itemCount = (self.topicComment.hasShowMoreChildCommentFlag != YXSquareCommentCellCommentViewShowNormal)?1:9;
    [childComment enumerateObjectsUsingBlock:^(id   obj, NSUInteger idx, BOOL *  stop) {
        if (idx == itemCount) {
            *stop = YES;
        }
        TopicComment * comment = (TopicComment*)obj;
        YXSquareMiniCommentView * miniV = [[YXSquareMiniCommentView alloc]init];
        miniV.delegate = self;
        [miniV setContentWithTopicCommentModel:comment andTopicCreator:creator];
        [miniV setMaxWidth:ScreenWidth-48];
        [self.childCommentBGV addSubview:miniV];
        WS(weakSelf)
        [miniV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.childCommentBGV.mas_left);
            if(miniViewTMP == nil){
                make.top.equalTo(weakSelf.childCommentBGV.mas_top).offset = 9;
            }else{
                make.top.equalTo(miniViewTMP.mas_bottom).offset = 9;
            }
            make.right.equalTo(weakSelf.childCommentBGV.mas_right).offset = -9;
            bgVH =bgVH+ miniV.height +9;
            make.height.equalTo([NSNumber numberWithFloat:miniV.height]);
        }];
        miniViewTMP = miniV;
        
    }];
    if(bgVH!=0){
        self.childCommentGBVCH.constant = bgVH + 20;
    }else{
        self.childCommentGBVCH.constant = 0;
    }
    _cellH = _cellH+bgVH + 20;
}


-(void)autoResizeSubviews{
    CGFloat favorW = [self.favorL.text sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]}].width+5;
    self.floorLCW.constant   = [self.floorNumL.text sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]}].width+5;
    
    self.favorLCW.constant   = favorW;
    
    CGFloat commentH = [self.comtentL.text boundingRectWithSize:CGSizeMake(ScreenWidth - 91 -favorW , 9999) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.comtentL.font} context:nil].size.height;
    self.comtentLCH.constant = commentH+1;
    _cellH = _cellH +commentH+1;
}
- (void)awakeFromNib {
    _cellH = 0;
    [self.moreBtn addTarget:self action:@selector(moreBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self setClipsToBounds:YES];
    [self.childCommentBGV clipsToBounds];
    self.headerImgV.layer.cornerRadius = 13;
    [self.headerImgV setClipsToBounds:YES];
    

}
- (IBAction)rightBtnClicked:(UIButton *)sender {

        YXSquareMoreItemView * moreItemVeiw = [YXSquareMoreItemView shareInstance];
    moreItemVeiw.delegate = self;
    if (moreItemVeiw.isShowing) {
        if ([moreItemVeiw isEqualToLastView:sender]) {
            [moreItemVeiw hide];
        }else{
            [moreItemVeiw hide];
            [moreItemVeiw showRightAtView:sender];
        }
        
    }else{
        [moreItemVeiw showRightAtView:sender];
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
-(YXSquareMoreItemView *)squareMoreItemView{
    if (!_squareMoreItemView) {
        YXSquareMoreItemView * moreItemView = [[[NSBundle mainBundle]loadNibNamed:@"YXSquareMoreItemView" owner:nil options:nil]lastObject];
        [self.contentView addSubview:moreItemView];
        moreItemView.delegate = self;
        WS(weakSelf)
        [moreItemView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@0);
            make.right.equalTo(weakSelf.rightMoreBtn.mas_left).offset=-6;
            make.height.equalTo(@28);
            make.centerY.equalTo(weakSelf.rightMoreBtn.mas_centerY);
            
        }];
        _squareMoreItemView = moreItemView;
    }
    return _squareMoreItemView;
}

///更多按钮被点击
-(void)moreBtnClicked{
    if ([self.delegate respondsToSelector:@selector(squareCommentCellMoreBtnClickedWithTopicComment:andBtn:)]) {
        [self.delegate squareCommentCellMoreBtnClickedWithTopicComment:self.topicComment andBtn:self.moreBtn];
    }
}
#pragma mark -
#pragma mark moreItemView代理
-(void)YXSquareMoreItemViewCommentBtnClicked{
    if([self.delegate respondsToSelector:@selector(commentResouceWithTopicComment:)]){
        [self.delegate commentResouceWithTopicComment:self.topicComment];
    }

}
-(void)YXSquareMoreItemViewReportBtnClicked{
    if([self.delegate respondsToSelector:@selector(reportSourceWithTopicComment:)]){
        [self.delegate reportSourceWithTopicComment:self.topicComment];
    }

}

@end


