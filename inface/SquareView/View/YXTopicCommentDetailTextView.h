//
//  YXTopicCommentDetailTextView.h
//  inface
//
//  Created by lizhen on 15/10/17.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YXTopicCommentDetailTextViewDelegate <NSObject>
@optional
-(void)squareCommentTextViewSendButtonClicked;

@end

@interface YXTopicCommentDetailTextView : UIView
@property(nonatomic,weak)id<YXTopicCommentDetailTextViewDelegate> delegate;
-(void)setEditing:(BOOL)isEditing;
-(NSString *)text;
-(void)setText:(NSString *)str;

@end
