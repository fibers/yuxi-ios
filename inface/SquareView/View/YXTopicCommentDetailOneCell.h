//
//  YXTopicCommentDetailOneCell.h
//  inface
//
//  Created by lizhen on 15/10/9.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TopicComment;
@interface YXTopicCommentDetailOneCell : UITableViewCell
///头像
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
///名字
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelWidth;
///时间
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLabelWidth;
///内容
@property (weak, nonatomic) IBOutlet UILabel *topicLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topicLabelHeight;
///收藏
//@property (weak, nonatomic) IBOutlet UILabel *shoucangLabel;
///评论
@property (weak, nonatomic) IBOutlet UILabel *pinglunLabel;
@property (weak, nonatomic) IBOutlet UIButton *pingLunButton;
@property (weak, nonatomic) IBOutlet UIButton *favourBtn;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
//@property (weak, nonatomic) IBOutlet UIButton *tidaiButton;
@property (weak, nonatomic) IBOutlet UIButton *tidaipinglunbutton;

@property (weak, nonatomic) IBOutlet UIView *bingView;

@property (weak, nonatomic) IBOutlet UIButton *juBaoBtn;

//@property (weak, nonatomic) IBOutlet UIButton *shoucangBtn;

@property (weak, nonatomic) IBOutlet UIButton *pinglunBtn;

//@property (weak, nonatomic) IBOutlet UIButton *shoucangButton;
///状态标识
@property (nonatomic, assign) BOOL btnStatus;

@property (nonatomic, assign) NSInteger topicContentHight;
-(void)setCellContentWithModel:(TopicComment *)topicComment;


@end
