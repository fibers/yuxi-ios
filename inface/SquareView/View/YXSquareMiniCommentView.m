//
//  YXSquareMiniCommentView.m
//  inface
//
//  Created by 邢程 on 15/10/10.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXSquareMiniCommentView.h"
#import "TopicComment.h"
#import "RegexKitLite.h"
#import "TYTextStorage.h"
#import "TYImageStorage.h"
#import "TYLinkTextStorage.h"
#import "TYViewStorage.h"
#import "TYAttributedLabel.h"
#import "TYTextContainer.h"
#import "SUser.h"
#import "YXMyCenterViewController.h"
@interface YXSquareMiniCommentView()<TYAttributedLabelDelegate>
@property(nonatomic,strong)TYAttributedLabel * label;
@property(nonatomic,assign)CGFloat labelMaxWitdh;
@property (nonatomic,assign)BOOL isFloorOwner;
@property(nonatomic,strong)TopicComment *topicComment;
@end
@implementation YXSquareMiniCommentView{

}
-(instancetype)init{
    if (self = [super init]) {
        _labelMaxWitdh = -666;
        _isFloorOwner = NO;
        [self addSubview:self.label];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewClicked)];
        [self addGestureRecognizer:tap];
        
    }
    return self;
}
-(CGFloat)height{
    return [self.label getHeightWithWidth:_labelMaxWitdh];
}
-(void)setMaxWidth:(CGFloat)maxW{
    _labelMaxWitdh = maxW;
    [self.label setFrameWithOrign:CGPointMake(0, 0) Width:maxW];
}
-(void)setContentWithTopicCommentModel:(TopicComment *)topicComment andTopicCreator:(SUser *)creator{
    self.isFloorOwner = [creator.id isEqualToString:topicComment.fromUser.id];
    [self setContentWithTopicCommentModel:topicComment];
}
-(void)setContentWithTopicCommentModel:(TopicComment *)topicComment{
    self.label.textContainer = [self creatTextContainerWithTopicComment:topicComment];
    [self.label setFrameWithOrign:CGPointMake(0, 0) Width:_labelMaxWitdh == -666 ?CGRectGetWidth(self.frame):self.labelMaxWitdh];
}

-(TYTextContainer *)creatTextContainerWithTopicComment:(TopicComment *)topic andIsFloorOwner:(BOOL)isFloorOwner{
    self.isFloorOwner = isFloorOwner;
    return [self creatTextContainerWithTopicComment:topic];
}

- (TYTextContainer *)creatTextContainerWithTopicComment:(TopicComment *)topic
{
    self.topicComment = topic;
    if (!topic.fromUser.nickName) {
        topic.fromUser.nickName = @"";
        topic.fromUser.id = @"";
        topic.fromUser.portrait = @"";
    }
    ///是否是资源拥有者
    BOOL isOwnner = [topic.fromUser.id isEqualToString:USERID];
    BOOL isHaveToUser = (nil != topic.toUser.nickName);
    NSString * floorOwnnerBtnStr = self.isFloorOwner?@"[``louzhu``] ":@"";
    LOG(@"%@",USERID);
    NSString * toUserStr = isHaveToUser?[NSString stringWithFormat:@"回复 %@: ",topic.toUser.nickName]:@"";
    NSString * delBtnStr = isOwnner?@"``[del]``":@"";

    NSString *text = [NSString stringWithFormat:@"%@ %@: %@%@  %@  %@",topic.fromUser.nickName,floorOwnnerBtnStr,toUserStr,topic.commentContent,topic.commentTime,delBtnStr];
    
    // 属性文本生成器
    TYTextContainer *textContainer = [[TYTextContainer alloc]init];
    textContainer.text = text;
    textContainer.font = [UIFont systemFontOfSize:13];
    [textContainer addLinkWithLinkData:topic.fromUser linkColor:[UIColor colorWithHexString:@"0da6ce"] underLineStyle:kCTUnderlineStyleNone range:[text rangeOfString:topic.fromUser.nickName]];
    //正文
    TYTextStorage *textStorage = [[TYTextStorage alloc]init];
    textStorage.range = [text rangeOfString:topic.commentContent];
    textStorage.textColor = [UIColor blackColor];
    textStorage.font = [UIFont systemFontOfSize:13];
    [textContainer addTextStorage:textStorage];
    textContainer.characterSpacing = 0;

    //发布人
    TYTextStorage *nameStorage = [[TYTextStorage alloc]init];
    nameStorage.range = [text rangeOfString:topic.fromUser.nickName];
    nameStorage.font = [UIFont systemFontOfSize:13];
    [textContainer addTextStorage:nameStorage];
  
    //时间
    TYTextStorage *timeStorage = [[TYTextStorage alloc]init];
    timeStorage.range = [text rangeOfString:topic.commentTime];
    timeStorage.textColor = [UIColor colorWithHexString:@"949494"];
    timeStorage.font = [UIFont systemFontOfSize:9];
    [textContainer addTextStorage:timeStorage];

    //楼主Label
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.layer.cornerRadius = 2;
    [button setBackgroundColor:[UIColor colorWithHexString:@"0da6ce"]];
    button.titleLabel.font = [UIFont systemFontOfSize:9];
    [button setTitle:@"楼主" forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 28, 13);
    [textContainer addView:button range:[text rangeOfString:@"[``louzhu``]"]];
    textContainer.linesSpacing = 0;

    //删除btn
    if (isOwnner) {
        UIButton *delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [delBtn setBackgroundImage:[UIImage imageNamed:@"删除"] forState:UIControlStateNormal];
        [delBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        delBtn.frame = CGRectMake(0, 0, 9, 12);
        [textContainer addView:delBtn range:[text rangeOfString:delBtnStr]];
        
    }
   
    return textContainer;
    
}
///view 被点击了，相应方法
-(void)viewClicked{
    if ([self.delegate respondsToSelector:@selector(suqareMiniCommentViewClicked:)]) {
        [self.delegate suqareMiniCommentViewClicked:self.topicComment];
    }
    
}

#pragma mark - TYAttributedLabelDelegate

- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    NSLog(@"textStorageClickedAtPoint");
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        id linkData = ((TYLinkTextStorage*)TextRun).linkData;
        LOG(@"%@***",linkData);
        if ([linkData isKindOfClass:[SUser class]]) {
            if ([self.delegate respondsToSelector:@selector(userNickNameClicked:)]) {
                [self.delegate userNickNameClicked:linkData];
            }
        }
    }else if ([TextRun isKindOfClass:[TYImageStorage class]]) {
        
    }
}

-(void)buttonClicked:(UIButton *)btn{
    if ([self.delegate respondsToSelector:@selector(delBtnClicked:)]) {
        [self.delegate delBtnClicked:self.topicComment];
    }
    LOG(@"删除按钮");

}

-(TYAttributedLabel *)label{
    if (!_label) {
        _label = [[TYAttributedLabel alloc]init];
        _label.delegate = self;
    }
    return _label;
}

@end
