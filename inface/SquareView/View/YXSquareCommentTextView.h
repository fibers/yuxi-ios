//
//  YXSquareCommentTextView.h
//  inface
//
//  Created by 邢程 on 15/10/15.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YXSquareCommentTextViewDelegate <NSObject>
@optional
-(void)squareCommentTextViewSendButtonClicked;

@end
@interface YXSquareCommentTextView : UIView
@property(nonatomic,weak)id<YXSquareCommentTextViewDelegate> delegate;
-(void)setEditing:(BOOL)isEditing;
-(NSString *)text;
-(void)setText:(NSString *)str;
@end
