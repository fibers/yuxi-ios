//
//  YXTopiccontentCell.m
//  inface
//
//  Created by appleone on 15/10/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "YXTopiccontentCell.h"
#import "Topic.h"
#import "NSAttributedString+CXACoreTextFrameSize.h"
#import "VerticalAlignmentLabel.h"
#import <IDMPhoto.h>
#import <IDMPhotoBrowser.h>
#import "NSAttributedString+CXACoreTextFrameSize.h"
#import "UILabel+Copyable.h"
#import <CoreText/CoreText.h>
#import "HelpInforViewController.h"
#define MarginLeft 8.0f
#define MarginTop 54.0f
@interface YXTopiccontentCell ()<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *portraitImageView;

@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;//创建者

@property (weak, nonatomic) IBOutlet UIButton *ownerBtn;

@property (weak, nonatomic) IBOutlet UIButton *deleteTopicBtn;//删除或者举报按钮
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creatorLalWid;


@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet CXAHyperlinkLabel *contentLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentLabHeight;//label的高度岁文字长度动态改变
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadToConstant;

@property (nonatomic,strong) Topic  * topicDetail;

@property (nonatomic,strong) NSMutableArray * imagesViewArr;

@property (weak, nonatomic) IBOutlet UIImageView *vipImageView;

@property (assign,nonatomic)  BOOL   isTopicView;

@property (nonatomic,strong)  UILongPressGestureRecognizer * pressGesture;

@property (strong,nonatomic)  UINavigationController * parentController;
@end

@implementation YXTopiccontentCell


-(void)setViewClass:(BOOL)isTopic
{
    self.isTopicView = isTopic;
    
}


-(void)setTopicDetailModel:(Topic *)topicDetail
{
    _topicDetail = topicDetail;
    _ownerBtn.layer.cornerRadius = 4;
    _ownerBtn.clipsToBounds = YES;
    _portraitImageView.layer.cornerRadius = _portraitImageView.frame.size.width /2.0;
    _portraitImageView.clipsToBounds = YES;
    self.contentView.userInteractionEnabled = YES;
    NSURL * portaitUrl = [NSURL URLWithString:topicDetail.creator.portrait];
    [_portraitImageView sd_setImageWithURL:portaitUrl  placeholderImage:[UIImage imageNamed:@"zs_touxianglan"]];
    
    int isVip = [topicDetail.creator.portrait isSignedUser];
    UIImage * image = [UIImage imageNamed:@"vipWriter"];
    UIImage * officalImage = [UIImage imageNamed:@"maxOffical"];
    if (isVip == 0) {
        _vipImageView.hidden = YES;

    }else{
        if (isVip == 1) {
            _vipImageView.hidden = NO;
            
            _vipImageView.image = image;
        }else{
            _vipImageView.hidden = NO;
            
            _vipImageView.image = officalImage;
        }
    }

    _creatorLabel.text = topicDetail.creator.nickName;
    
    _creatorLalWid.constant = [_creatorLabel.text sizeWithAttributes:@{NSFontAttributeName:self.creatorLabel.font}].width+3;
    
    if ([topicDetail.creator.id isEqualToString:USERID]) {
        //自己创建的话题，可以删除,如果是自戏或者剧页面进来的，隐藏删除按钮
        if (self.isTopicView) {
            [_deleteTopicBtn setTitle:@"删除" forState:UIControlStateNormal];
            
        }else{
            _deleteTopicBtn.hidden = YES;
        }
    }else{
        [_deleteTopicBtn setTitle:@"举报" forState:UIControlStateNormal];
        
    }
    
    [_timeLabel setText:topicDetail.createTime];
    
    
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:topicDetail.content];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:5.0f];
    
    UIFont* font = [UIFont systemFontOfSize:14.0];
    
    [attribute addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [topicDetail.content length])];
    [attribute addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [topicDetail.content length])];
//    CGFloat   labelHeight = [attribute boundingRectWithSize:CGSizeMake(ScreenWidth - 8, 999999) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin context:NULL].size.height;
//    
//    CGRect  titleF = CGRectMake(MarginLeft, MarginTop, ScreenWidth - MarginLeft * 3, 54);
//    
//    CGRect   titleHei = CGRectMake(titleF.origin.x, CGRectGetMaxY(titleF)+MarginTop, ScreenWidth - 2*MarginLeft, labelHeight);
//    labelHeight  = CGRectGetMaxY(titleHei)+MarginTop;
    CGFloat labelHeight =  [attribute boundingRectWithSize:CGSizeMake(ScreenWidth-16, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:NULL].size.height;
    labelHeight += 20;
    _contentLabHeight.constant = labelHeight;

    //添加超链接
    NSError *error;
    NSMutableArray *urls = [[NSMutableArray alloc]init];
    NSMutableArray *urlRanges = [[NSMutableArray alloc]init];
    
    NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray *arrayOfAllMatches = [regex matchesInString:topicDetail.content
                                                options:0
                                                  range:NSMakeRange(0, [topicDetail.content length])];
    
    for (NSTextCheckingResult * match in arrayOfAllMatches) {
        if (match.range.location == NSNotFound) {
            continue;
        }
        NSString *substringForMatch =   [topicDetail.content substringWithRange:match.range];
        NSURL    *suburl            =   [NSURL URLWithString:substringForMatch];
        [urls addObject:suburl];
        [urlRanges addObject:[NSValue valueWithRange:match.range]];
        [attribute addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:match.range];
        
        [attribute addAttribute:(NSString *)kCTForegroundColorAttributeName
                          value:(id)[UIColor blueColor].CGColor
                          range:match.range];
    }
    [_contentLabel setAttributedText:attribute];
//    [_contentLabel setVerticalAlignment:VerticalAlignmentTop];
    [_contentLabel setNumberOfLines:0];
    
    [_contentLabel setURLs:urls forRanges:urlRanges];

    __weak YXTopiccontentCell * weakself = self;
    
    self.contentLabel.URLClickHandler = ^(CXAHyperlinkLabel *label, NSURL *URL, NSRange range, NSArray *textRects){
        NSString    *url    =   [URL absoluteString];
        NSRange   rangeUrl = [url rangeOfString:@"www.yuxip.com"];
        if(rangeUrl.length > 0) {
            NSRange  range = [url rangeOfString:@"http://"];
            if (range.length == 0) {
                url = [NSString stringWithFormat:@"http://%@",url];
            }
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }else if(_parentController != nil){
        
            HelpInformationViewController * detailViewController = [[HelpInformationViewController alloc] init];
            detailViewController.navTitle   =   url;
            detailViewController.contentUrl =   url;
            detailViewController.hidesBottomBarWhenPushed=YES;
            if (detailViewController.isAnimating) {
                return ;
            }
            detailViewController.isAnimating = NO;
            [ [weakself getNavi ] pushViewController:detailViewController animated:YES];
        }
        
    };
    //    _contentLabel.frame = CGRectMake(12, 54, ScreenWidth - 24, labelHeight);
    //放照片
    CGFloat imgWidth = 75;
    
    CGFloat imgHeight = 75;
    
    CGFloat originHei= labelHeight + 60 ;
    NSInteger imsCount= (ScreenWidth - 24)/75;
    
    if (topicDetail.images.count == 1) {
        //需要判断是否为空
        NSString * imgUrl = topicDetail.images[0];
        if ([imgUrl isEqualToString:@""] || [imgUrl isEqual:nil] || [imgUrl isEqual:NULL]) {
            imsCount  = 0;
        }
    }
    if (imsCount > topicDetail.images.count) {
        //如果返回的图片张数比较少
        imsCount = topicDetail.images.count;
    }
    
    for (int i = 0; i < imsCount; i++) {
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(12 + i * (75+9), originHei, imgWidth, imgHeight)];
        
        NSURL * url = [NSURL URLWithString:topicDetail.images[i]];
        
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"huatixiangqing"]];
        imageView.userInteractionEnabled = YES;
        imageView.tag = i;
        if (i == imsCount-1) {
            UILabel * countLal = [[UILabel alloc]initWithFrame:CGRectMake(30, 55, 30, 20)];
            countLal.text = [NSString stringWithFormat:@"共%ld张",topicDetail.images.count];
            [countLal setTextColor:[UIColor whiteColor]];
            [countLal setFont:[UIFont systemFontOfSize:10]];
            
            [countLal setBackgroundColor:[UIColor blackColor]];
            
            countLal.alpha = 0.5;
            [imageView addSubview:countLal];
        }
        
        UITapGestureRecognizer * singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showImages:)];
        
        [imageView addGestureRecognizer:singleTap];
        
        [self.contentView addSubview:imageView];
        [_imagesViewArr addObject:imageView];
        
    }
    
    CGFloat    imageHeight = 0;
    if (imsCount>0) {
        imageHeight = 85;
    }
    UILabel * lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, labelHeight +70+ imageHeight, ScreenWidth, 0.5)];
    lineLabel.backgroundColor = [UIColor colorWithHexString:@"#c5c5c6"];
    
    [self.contentView addSubview:lineLabel];
    
    UIView * backView = [[UIView alloc]initWithFrame:CGRectMake(0, labelHeight +70+ imageHeight + 0.5, ScreenWidth, 9)];
    
    [backView setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
    
    [self.contentView addSubview:backView];
}

- (void) setNaviController : (UINavigationController *) parentController
{
    _parentController   =   parentController;
}


- (UINavigationController *) getNavi
{
    return _parentController;
}


-(void)showImages:(UITapGestureRecognizer *)recognizer
{
    
    NSMutableArray * urlImgs = [NSMutableArray array];
    [_topicDetail.images enumerateObjectsUsingBlock:^(NSString * obj, NSUInteger idx, BOOL *stop) {
        
        NSURL * url = [NSURL URLWithString:obj];
        
        [urlImgs addObject:url];
    }];
    LOG(@"点击的图片视图:%@ %ld",recognizer.self.view,recognizer.self.view.tag);
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc]initWithPhotoURLs:urlImgs animatedFromView:recognizer.self.view];
    //    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:_topicDetail.images[recognizer.self.view.tag]]];
    [browser setInitialPageIndex:recognizer.self.view.tag];
    [self.viewController presentViewController:browser animated:YES completion:nil];
}

- (UIViewController*)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}

-(CGFloat)sizeForContent:(Topic *)content
{
    
    NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc]initWithString:content.content];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:5.0f];
    
    UIFont* font = [UIFont systemFontOfSize:14];
    
    [attribute addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [content.content length])];
    [attribute addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [content.content length])];
   CGFloat labelHeight =  [attribute boundingRectWithSize:CGSizeMake(ScreenWidth-16, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:NULL].size.height;
    labelHeight += 20;

//    CGFloat   labelHeight = [attribute cxa_coreTextFrameSizeWithConstraints:CGSizeMake(ScreenWidth-8, 99999)].height;
 
    NSInteger imsCount = content.images.count;
    if (content.images.count == 1) {
        //需要判断是否为空
        NSString * imgUrl = content.images[0];
        if ([imgUrl isEqualToString:@""] || [imgUrl isEqual:nil] || [imgUrl isEqual:NULL]) {
            imsCount  = 0;
        }
    }
    
    if (imsCount > 0) {
        return labelHeight + 156+20;
        
    }else{
        return labelHeight + 70+20;
    }
}


#pragma mark 去个人主页按钮
- (void)gotoYxPersonCenter:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(gotoPersonCenter:)]) {
        [self.delegate gotoPersonCenter:_topicDetail];
    }
    
}

- (IBAction)deleteOrReport:(UITapGestureRecognizer *)sender
{
    if ([_topicDetail.creator.id isEqualToString:USERID]) {
        //自己创建的话题，可以删除
        if ([self.delegate respondsToSelector:@selector(deleteTopic:)]) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"是否删除该条资源" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
            alert.tag = 1000;
        }
    }else{
        
        if ([self.delegate respondsToSelector:@selector(reportTopic:)]) {
            
            [self.delegate reportTopic:_topicDetail];
            
        }
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1000 && buttonIndex == 1) {
        [self.delegate deleteTopic:_topicDetail];
    }
    
}

#pragma mark LongPress TapGesture
-(void)copyMenuShow:(UILongPressGestureRecognizer*)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        CGPoint location = [gesture locationInView:self.contentView];
        UIMenuController  * menu = [UIMenuController  sharedMenuController];
        
        UIMenuItem   *menuItem1 = [[UIMenuItem alloc]initWithTitle:@"复制" action:@selector(copyContent)];
        
        menu.menuItems = [NSArray arrayWithObjects:menuItem1, nil];

        [menu setTargetRect:CGRectMake(location.x - 60, location.y , 120, 44) inView:self.superview];
        
        [menu setMenuVisible:YES animated:YES];
    }
    
}


-(void)copyContent
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    NSString * str = _contentLabel.text ;
    
    [pasteboard setString:str];
    
}

- (void)awakeFromNib {
    // Initialization code
    _imagesViewArr = [NSMutableArray array];
    UITapGestureRecognizer * singleGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoYxPersonCenter:)];
    _portraitImageView.userInteractionEnabled = YES;
    [_portraitImageView addGestureRecognizer:singleGesture];
    
    _contentLabel.copyingEnabled = YES;
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
