//
//  YXSquareCommentCellTableViewCell.h
//  inface
//
//  Created by 邢程 on 15/10/10.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, YXSquareCommentCellCommentViewShowType)
{
    YXSquareCommentCellCommentViewShowNormal = 1,
    YXSquareCommentCellCommentViewShowTenItems = 2,
};

@class TopicComment;
@class SUser;
@protocol YXSquareCommentCellDelegate <NSObject>
@optional
-(void)squareCommentCellMoreBtnClickedWithTopicComment:(TopicComment*)topicComment andBtn:(UIButton *)btn;

-(void)miniCommentViewClickedNickName:(SUser*)user;
-(void)miniCommentViewClickedDeleteBtn:(TopicComment *)topicComment;
-(void)squareCommentfavorBtnClicked:(UIButton *)favorBtn andResourceID:(NSString*)resourceID;
///举报
-(void)reportSourceWithTopicComment:(TopicComment *)topicComment;
///评论
-(void)commentResouceWithTopicComment:(TopicComment *)topicComment;
/**
 *  楼层的评论被点击 （回复楼层功能）
 *
 *  @param user         被点击的用户model
 *  @param topicComment 当前楼层的model
 */
-(void)miniCommentViewClickedWithUser:(SUser*)user andCellModel:(TopicComment *)topicComment;
@end
@interface YXSquareCommentCellTableViewCell : UITableViewCell

-(void)setContentWithTopicCommentModel:(TopicComment *)topicComment andTopicOwnerID:(SUser *)creator;
@property(nonatomic,weak)id<YXSquareCommentCellDelegate> delegate;
@end
