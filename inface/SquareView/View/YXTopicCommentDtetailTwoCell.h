//
//  YXTopicCommentDtetailTwoCell.h
//  inface
//
//  Created by lizhen on 15/10/10.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YXSquareMiniCommentView.h"
@class TopicComment;
@interface YXTopicCommentDtetailTwoCell : UITableViewCell<YXSquareMiniCommentViewDelegate >

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

-(CGFloat)setCellContentOne:(TopicComment *)topicComment andTopicCreator:(SUser *)creator;

@end
