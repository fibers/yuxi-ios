//
//  YXTopiccontentCell.h
//  inface
//
//  Created by appleone on 15/10/14.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Topic;

@protocol TopicDetailDelegate <NSObject>

@optional
-(void)gotoPersonCenter:(Topic *)userId;

-(void)reportTopic:(Topic *)topicDetail;

-(void)deleteTopic:(Topic *)topicDetail;

@end
@interface YXTopiccontentCell : UITableViewCell<UIGestureRecognizerDelegate>

@property(unsafe_unretained,nonatomic)id<TopicDetailDelegate>delegate;
-(void)setViewClass:(BOOL)isTopic;
-(void)setTopicDetailModel:(Topic *)topicDetail;

-(CGFloat)sizeForContent:(Topic *)content;

- (void) setNaviController : (UINavigationController *) parentController;

@end
