//
//  SquareScrollTableViewCell.h
//  inface
//
//  Created by huangzengsong on 15/7/6.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
@interface SquareScrollTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end
