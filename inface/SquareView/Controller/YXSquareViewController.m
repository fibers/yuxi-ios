//
//  YXSquareViewController.m
//  inface
//
//  Created by 邢程 on 15/9/18.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXSquareViewController.h"
#import "YXSquareTableViewCell.h"
#import "MJRefresh.h"
#import "SquareListModel.h"
#import "Topic.h"
#import "YXTopicDetailViewController.h"
#import "YXTopicCommentDetailViewController.h"
#import "MBProgressHUD.h"
#import "YXNewUserViewController.h"
#define kYXSquareViewControllerRefreshDataNotification @"kYXSquareViewControllerRefreshDataNotification"
@interface YXSquareViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView * tableView;
@property(nonatomic,strong)NSMutableArray * arrM;
@property(nonatomic,assign)NSInteger currentPage;
@property(nonatomic,strong)SquareListModel *squareListModel;
@end

@implementation YXSquareViewController

-(instancetype)init{
    if (self = [super init]) {
        [self.navigationItem setTitle:@"广场"];
        [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
        _currentPage = 0;
        [self.view addSubview:self.tableView];
        [self.tableView addHeaderWithTarget:self action:@selector(refreshData)];
        [self.tableView addFooterWithTarget:self action:@selector(loadMoreData)];
        [self refreshData];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshData) name:kYXSquareViewControllerRefreshDataNotification object:nil];
        [self.tableView registerNib:[UINib nibWithNibName:@"YXSquareTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"YXSquareTableViewCell"];
    }
    return self;
}
///刷新
-(void)refreshData{
    [HttpTool postWithPath:@"SquareList" params:@{
                                                 @"uid":USERID,
                                                 @"token":@"110",
                                                 @"index":@"0",
                                                 @"count":@"20"} success:^(id JSON) {
                                                     self.currentPage = 1;
                                                     SquareListModel * model = [SquareListModel objectWithKeyValues:JSON];
                                                     _squareListModel = model;
                                                     self.arrM = [NSMutableArray array];
                                                     [self.arrM addObjectsFromArray:model.squareList];
                                                     [self.tableView reloadData];
                                                     [self.tableView headerEndRefreshing];
                                                 } failure:^(NSError *error) {
                                                     [self.tableView headerEndRefreshing];
                                                 }];
    

}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
///加载更多
-(void)loadMoreData{
    NSDictionary * dic = @{
                           @"uid":USERID,
                           @"token":@"110",
                           @"index":[NSString stringWithFormat:@"%ld",self.currentPage],
                           @"count":@"20"
                           };
    LOG(@"%@",dic);
    [HttpTool postWithPath:@"SquareList" params:dic success:^(id JSON) {
                                                      self.currentPage = _currentPage+1;
                                                      SquareListModel * model = [SquareListModel objectWithKeyValues:JSON];
                                                      [self.arrM addObjectsFromArray:model.squareList];
                                                      [self.tableView reloadData];
                                                      [self.tableView footerEndRefreshing];
                                                  } failure:^(NSError *error) {
                                                      [self.tableView footerEndRefreshing];

                                                  }];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    lab.backgroundColor = [UIColor redColor];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f8f8f8"];
    [self.view addSubview:lab];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrM count];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Topic *topic = [self.arrM objectAtIndex:indexPath.row];
    return [self cellHeightWithTopic:topic];
    
}
-(CGFloat)cellHeightWithTopic:(Topic*)topic{
    YXSquareTableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"YXSquareTableViewCell"];
    [cell setContentWithTopicModel:topic];
    return cell.cellHight;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    YXSquareTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"YXSquareTableViewCell"];
    [cell setContentWithTopicModel:[self.arrM objectAtIndex:indexPath.row]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Topic * topic = [self.arrM objectAtIndex:indexPath.row];
    YXTopicDetailViewController *vc = [[YXTopicDetailViewController alloc]init];
    [vc setIsTopicView];
    [vc setTopicID:topic.topicID];
    if (vc.isAnimating) {
        return;
    }
    vc.isAnimating = YES;
    [self.navigationController pushViewController:vc animated:YES];

}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64-49)];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        UIImageView * imgV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 75)];
        [imgV setImage:[UIImage imageNamed:@"banner_xiaobai"]];
        imgV.userInteractionEnabled = YES;
        [imgV setContentMode:UIViewContentModeScaleAspectFill];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bannerClicked)];
        [imgV addGestureRecognizer:tap];
        _tableView.tableHeaderView = imgV;

    }
    return _tableView;
}
-(void)bannerClicked{
    YXNewUserViewController * vc = [[YXNewUserViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark -
#pragma mark 初始化懒加载等等
-(NSMutableArray *)arrM{
    if (!_arrM) {
        _arrM = [NSMutableArray array];
    }
    return _arrM;
}
@end
