//
//  YXTopicDetailViewController.m
//  inface
//
//  Created by 邢程 on 15/10/11.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXTopicDetailViewController.h"
#import "TopicDetail.h"
#import "MJRefresh.h"
#import "TopicComment.h"
#import "Topic.h"
#import "YXSquareTableViewCell.h"
#import "YXSquareCommentCellTableViewCell.h"
#import <UITableView+FDTemplateLayoutCell.h>
#import "YXMyCenterViewController.h"

#import "YXTopiccontentCell.h"
#import "YXMyCenterViewController.h"
#import <Masonry.h>
#import "YXSquareCommentButtomView.h"
#import "YXSquareCommentTextView.h"
#import <Masonry.h>
#import "MBProgressHUD.h"
#import "YXTopicCommentDetailViewController.h"
#import "MBProgressHUD+YXStyle.h"

#import "ReportTopicViewController.h"
#import "YXSquareMoreItemView.h"
#import "UILabel+Copyable.h"
@interface YXTopicDetailViewController ()<UITableViewDelegate,UITableViewDataSource,YXSquareCommentCellDelegate,TopicDetailDelegate,YXSquareCommentTextViewDelegate,UIGestureRecognizerDelegate >
@property (nonatomic,strong) UITableView               * tableView;
@property (nonatomic,strong) TopicDetail               * topicDetail;
@property (nonatomic,strong) YXSquareCommentButtomView * buttomView;
@property (nonatomic,strong) YXSquareCommentTextView   * commentTextView;
@property (nonatomic,copy  ) NSString                  * topicID;
@property (nonatomic,assign) NSInteger                 pageCountTMP;
@property (assign,nonatomic) YXTopicDetailType         type;
@property (assign,nonatomic) BOOL                      isTopicViewController;
@end

@implementation YXTopicDetailViewController
-(instancetype)init{
    if (self = [super init]) {
        [self setHidesBottomBarWhenPushed:YES];
        [[IQKeyboardManager sharedManager]setEnable:NO];
        [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];
        _pageCountTMP = 0;
        [self.view setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
        [self.tableView setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
        [self.view addSubview:self.tableView];
        [self.tableView registerNib:[UINib nibWithNibName:@"YXSquareCommentCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXSquareCommentCellTableViewCell"];
        

//
        //        [self.tableView headerBeginRefreshing];
    }
    return self;
}
-(void)replaceNavBarBackButton{
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"#f5f5f5"];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}
//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)setType:(YXTopicDetailType)type{
    _type = type;
}

-(void)setIsTopicView
{
    self.isTopicViewController = YES;
 
}
-(void)setTopicID:(NSString *)topicID{
    _topicID = topicID;
    [self refreshData];
}
-(YXSquareCommentButtomView *)buttomView{
    if (!_buttomView) {
        _buttomView = [[[NSBundle mainBundle]loadNibNamed:@"YXSquareCommentButtomView" owner:nil options:nil]lastObject];
        [_buttomView.collectionBtn addTarget:self action:@selector(buttomViewCollectionBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_buttomView.favorBtn addTarget:self action:@selector(buttomViewFavorBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_buttomView.commentBtn addTarget:self action:@selector(buttomViewCommentBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_buttomView];
        WS(weakSelf)
        [_buttomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(weakSelf.tableView.mas_width);
            make.height.equalTo(@50);
            make.bottom.equalTo(weakSelf.view.mas_bottom);
            make.right.equalTo(weakSelf.tableView.mas_right);
        }];

       
    }
    return _buttomView;
}
#pragma mark -
#pragma mark 评论TextView
-(YXSquareCommentTextView *)commentTextView{
    if (!_commentTextView) {
        _commentTextView = [[YXSquareCommentTextView alloc]init];
        _commentTextView.delegate = self;
        [self.view addSubview:_commentTextView];
        WS(weakSelf)
        [_commentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.view.mas_left);
            make.width.equalTo(weakSelf.view.mas_width);
            make.height.equalTo(@40);
            make.bottom.equalTo(weakSelf.view.mas_bottom);
        }];
        [self.view layoutIfNeeded];
    }
    return _commentTextView;
}

#pragma mark -
#pragma mark 底部view 的响应
-(void)setButtomViewContent:(TopicDetail *)detail{
    [self.buttomView.favorBtn setSelected:[detail.topicDetail.isFavor isEqualToString:@"1"]];
    [self.buttomView.collectionBtn setSelected:[detail.topicDetail.isCollection isEqualToString:@"1"]];
    [self.buttomView.collectionBtn setTitle:detail.topicDetail.collectionCount forState:UIControlStateNormal];
    [self.buttomView.favorBtn setTitle:detail.topicDetail.favorCount forState:UIControlStateNormal];
}
-(void)buttomViewCollectionBtnClicked:(UIButton *)btn{
    NSString * opt;
    if (!btn.isSelected) {
        [btn setTitle:[NSString stringWithFormat:@"%ld",[btn.titleLabel.text integerValue]+1] forState:UIControlStateNormal];
        opt = @"1";
    }else{
        [btn setTitle:[NSString stringWithFormat:@"%ld",[btn.titleLabel.text integerValue]-1] forState:UIControlStateNormal];
        opt = @"0";
    }
    
    [HttpTool postWithPath:self.type == YXTopicDetailTypeStory ?@"CollectStoryResource":@"CollectSquareResource" params:@{
                                                            @"uid":USERID,
                                                            @"token":@"110",
                                                            @"resourceID":self.topicID,
                                                            @"resourceType":@"0",
                                                            @"operation":opt
                                                            } success:^(id JSON) {
                                                                
                                                            } failure:^(NSError *error) {
                                                                
                                                            }];
    btn.selected = !btn.isSelected;

}
-(void)buttomViewFavorBtnClicked:(UIButton *)btn{
    NSString * opt;
    if (!btn.isSelected) {
        [btn setTitle:[NSString stringWithFormat:@"%ld",[btn.titleLabel.text integerValue]+1] forState:UIControlStateNormal];
        opt = @"1";
    }else{
        [btn setTitle:[NSString stringWithFormat:@"%ld",[btn.titleLabel.text integerValue]-1] forState:UIControlStateNormal];
        opt = @"0";
    }
    
    [HttpTool postWithPath:self.type == YXTopicDetailTypeStory ?@"PraiseStoryResource":@"PraiseSquareResource" params:@{
                                                             @"uid":USERID,
                                                             @"token":@"110",
                                                             @"resourceID":self.topicID,
                                                             @"resourceType":@"0",
                                                             @"operation":opt
                                                             } success:^(id JSON) {
                                                                 
                                                             } failure:^(NSError *error) {
                                                                 
                                                             }];
     btn.selected = !btn.isSelected;
}
-(void)buttomViewCommentBtnClicked:(UIButton *)btn{
    [self.commentTextView setEditing:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self replaceNavBarBackButton];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerForKeyboardNotifications];
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
    [self.tableView setBackgroundColor:[UIColor colorWithHexString:@"f5f5f5"]];
    [self.view addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"YXSquareCommentCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXSquareCommentCellTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"YXTopiccontentCell" bundle:nil] forCellReuseIdentifier:@"YXTopicContentCell"];


}



#pragma mark -
#pragma mark 上下拉刷新请求

-(void)refreshData{
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    LOG(@"topicID : %@",self.topicID);
    NSString *topicKey = self.type == YXTopicDetailTypeStory?@"storyID":@"topicID";
    NSDictionary * dic = @{@"uid":USERID,
                           @"token":@"110",
                           topicKey:self.topicID ==nil?@"":self.topicID,
                           @"index":@"0",
                           @"count":@"20"
                           };
    [HttpTool postWithPath:self.type == YXTopicDetailTypeStory ?@"StoryDetail":@"TopicDetail" params:dic success:^(id JSON) {
                                                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                   TopicDetail * topicDetail = [TopicDetail objectWithKeyValues:JSON];
                                                    [self.navigationItem setTitle:topicDetail.topicDetail.title];
                                                   _topicDetail              = topicDetail;
                                                   [self setButtomViewContent:topicDetail];
                                                   _pageCountTMP = 1;
                                                   [self.tableView reloadData];
                                                   [self.tableView headerEndRefreshing];
   } failure:^(NSError *error) {
       [MBProgressHUD hideHUDForView:self.view animated:YES];
       [self.tableView headerEndRefreshing];
   }];
}
-(void)loadMoreData{
    NSString *topicKey = self.type == YXTopicDetailTypeStory?@"storyID":@"topicID";
    [HttpTool postWithPath:self.type == YXTopicDetailTypeStory ?@"StoryDetail":@"TopicDetail" params:@{@"uid":USERID,
                                                   @"token":@"110",
                                                   topicKey:self.topicID ==nil?@"":self.topicID,
                                                   @"index":[NSString stringWithFormat:@"%ld",self.pageCountTMP],
                                                   @"count":@"20"
                                                   } success:^(id JSON) {
                                                       
                                                       TopicDetail * topicDetail = [TopicDetail objectWithKeyValues:JSON];
                                                       if([topicDetail.comment count] != 0){
                                                           _pageCountTMP++;
                                                           [self.topicDetail.comment addObjectsFromArray:topicDetail.comment];
                                                           [self.tableView reloadData];
                                                       }else{
//                                                           MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                                                           [hud setMode:MBProgressHUDModeText];
//                                                           hud.labelText = @"没有更多数据";
//                                                           [hud show:YES];
//                                                           [hud hide:YES afterDelay:2];
                                                       }
                                                       [self.tableView footerEndRefreshing];
                                                   } failure:^(NSError *error) {
                                                       [self.tableView footerEndRefreshing];
                                                   }];
}



#pragma mark -
#pragma mark tableView dataSource&delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.topicDetail == nil ?0:3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.topicDetail == nil) {
        return 0;
    }
    if (section == 0) {
        return  1;
    }else if(section ==1){
         return [self.topicDetail.hotComment count];
    }else if(section ==2){
        return [self.topicDetail.comment count];
    }else{
        return 0;
    }

}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return section == 0? [[UIView alloc]init]:[[[NSBundle mainBundle]loadNibNamed:@"YXSquareSectionHeaderView" owner:nil options:nil]objectAtIndex:section-1];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return section ==0;
    }else if(section == 1){
        return [self.topicDetail.hotComment count] == 0 ? 0:20;
    }else if(section == 2){
        return [self.topicDetail.comment count] == 0 ? 0:20;
    }
    return 0;
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 ) {
        NSString * identifier = @"YXTopicContentCell";

        YXTopiccontentCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];

        return self.topicDetail.topicDetail == nil ? 0:[cell sizeForContent:self.topicDetail.topicDetail];
    }if(indexPath.section  == 1){
         if([self.topicDetail.hotComment count] == 0 ){
             return  0;
         }else{
             return [tableView fd_heightForCellWithIdentifier:@"YXSquareCommentCellTableViewCell" configuration:^(id cell) {
            [self configureCell:cell atIndexPath:indexPath];
        }];
        }
    }if(indexPath.section  == 2){
        if([self.topicDetail.comment count] == 0 ){
            return  0;
        }else{
            return [tableView fd_heightForCellWithIdentifier:@"YXSquareCommentCellTableViewCell" configuration:^(id cell) {
                [self configureCell:cell atIndexPath:indexPath];
            }];
        }
    }
    return 0;
}

- (void)configureCell:(YXSquareCommentCellTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
        cell.fd_enforceFrameLayout = YES;
            if (indexPath.section ==1) {
                [cell setContentWithTopicCommentModel:[self.topicDetail.hotComment objectAtIndex:indexPath.row] andTopicOwnerID:(SUser *)self.topicDetail.topicDetail.creator];
    
            }if(indexPath.section == 2){
                [cell setContentWithTopicCommentModel:[self.topicDetail.comment objectAtIndex:indexPath.row] andTopicOwnerID:(SUser *)self.topicDetail.topicDetail.creator];
            }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"kYXSquareMoreItemViewHideNotification" object:nil];
    TopicComment * comment;
    if (indexPath.section == 0) {
        
    }else if(indexPath.section == 1){
        comment = [self.topicDetail.hotComment objectAtIndex:indexPath.row];
        YXTopicCommentDetailViewController * vc = [[YXTopicCommentDetailViewController alloc]init];
        [vc setType:self.type];
        [vc getTopicDetailOfTopicID:self.topicDetail.topicDetail.topicID andCommentID:comment.commentID andTopicCreator:self.topicDetail.topicDetail.creator];
        [vc getLastName:comment.fromUser andTopicCreator:self.topicDetail.topicDetail.creator];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    
    }else if(indexPath.section == 2){
        comment = [self.topicDetail.comment objectAtIndex:indexPath.row];
        YXTopicCommentDetailViewController * vc = [[YXTopicCommentDetailViewController alloc]init];
        [vc setType:self.type];
        [vc getTopicDetailOfTopicID:self.topicDetail.topicDetail.topicID andCommentID:comment.commentID andTopicCreator:self.topicDetail.topicDetail.creator];
        [vc topicDetailOfTopicID:self.topicDetail andCommentID:comment andTopicCreator:self.topicDetail.topicDetail.creator];
        [vc getLastName:comment.fromUser andTopicCreator:self.topicDetail.topicDetail.creator];
        [self.navigationController pushViewController:vc animated:YES];
    
    }
    
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {//header  Cell
        NSString * identifier = @"YXTopicContentCell";
        YXTopiccontentCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.delegate = self;
        [cell setViewClass:self.isTopicViewController];
        [cell setTopicDetailModel:self.topicDetail.topicDetail];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setNaviController:self.navigationController];
        return cell;
    }if(indexPath.section == 1){
        YXSquareCommentCellTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"YXSquareCommentCellTableViewCell"];
        cell.delegate = self;
        [cell setContentWithTopicCommentModel:[self.topicDetail.hotComment objectAtIndex:indexPath.row] andTopicOwnerID:(SUser *)self.topicDetail.topicDetail.creator];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }if(indexPath.section == 2){
        YXSquareCommentCellTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"YXSquareCommentCellTableViewCell"];
        cell.delegate = self;
        [cell setContentWithTopicCommentModel:[self.topicDetail.comment objectAtIndex:indexPath.row] andTopicOwnerID:(SUser *)self.topicDetail.topicDetail.creator];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    return nil;
}
#pragma mark -
#pragma mark 删除操作
/**
 *  不清楚状况的话  
 *               不要修改！
 *               不要修改！
 *               不要修改！
 *
 *               重要的事情说三遍！！！
 *
 *
 */
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return NO;
    }
    return NO;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1){
        if ([self.topicDetail.hotComment count]!=0) {
            TopicComment * topicComment = [self.topicDetail.hotComment objectAtIndex:indexPath.row];
            [HttpTool postWithPath:@"DeleteSquareResource" params:@{@"uid":topicComment.fromUser.id== nil?@"1000001":topicComment.fromUser.id,@"token":@"110",@"resourceID":topicComment.commentID,@"resourceType":@"1"} success:^(id JSON) {
                
            } failure:^(NSError *error) {
                
            }];
            [self.topicDetail.hotComment removeObjectAtIndex:indexPath.row];
        }
    }else if(indexPath.section == 2){
        
        
        TopicComment * topicComment = [self.topicDetail.comment objectAtIndex:indexPath.row];
        [HttpTool postWithPath:@"DeleteSquareResource" params:@{@"uid":topicComment.fromUser.id== nil?@"1000001":topicComment.fromUser.id,@"token":@"110",@"resourceID":topicComment.commentID,@"resourceType":@"1"} success:^(id JSON) {
            
        } failure:^(NSError *error) {
            
        }];
        [self.topicDetail.comment removeObjectAtIndex:indexPath.row];
    }
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView            = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64-50)];
        _tableView.dataSource = self;
        _tableView.delegate   = self;
        _tableView.tableFooterView = [[UIView alloc]init];
        [_tableView addHeaderWithTarget:self action:@selector(refreshData)];
        [_tableView addFooterWithTarget:self action:@selector(loadMoreData)];
    }
    return _tableView;
}
 
#pragma mark -
#pragma mark 实现cell代理
-(void)reportSourceWithTopicComment:(TopicComment *)topicComment{
    ReportTopicViewController * rvc = [[ReportTopicViewController alloc]init];
    [rvc setReportType:@"超哥说随便传个值就行" resourceId:topicComment.commentID];
    [self.navigationController pushViewController:rvc animated:YES];
}
-(void)commentResouceWithTopicComment:(TopicComment *)comment{
    YXTopicCommentDetailViewController * vc = [[YXTopicCommentDetailViewController alloc]init];
    [vc setType:self.type];
    [vc getTopicDetailOfTopicID:self.topicDetail.topicDetail.topicID andCommentID:comment.commentID andTopicCreator:self.topicDetail.topicDetail.creator];
    [vc getLastName:comment.fromUser andTopicCreator:self.topicDetail.topicDetail.creator];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)squareCommentfavorBtnClicked:(UIButton *)favorBtn andResourceID:(NSString *)resourceID{
    [HttpTool postWithPath:self.type == YXTopicDetailTypeStory ?@"PraiseStoryResource":@"PraiseSquareResource" params:@{
                                                            @"uid":USERID,
                                                            @"token":@"110",
                                                            @"resourceID":resourceID,
                                                            @"resourceType":@"1",
                                                            @"operation":favorBtn.isSelected ? @"1":@"0"
                                                            } success:^(id JSON) {
        
    } failure:^(NSError *error) {
        
    }];
}
-(void)miniCommentViewClickedWithUser:(SUser *)user andCellModel:(TopicComment *)topicComment{
    LOG(@"%@**",user.nickName);
    
    YXTopicCommentDetailViewController * vc = [[YXTopicCommentDetailViewController alloc]init];
    [vc setType:self.type];
    [vc getTopicDetailOfTopicID:self.topicDetail.topicDetail.topicID User:user andComment:topicComment andTopicCreator:self.topicDetail.topicDetail.creator];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)squareCommentCellMoreBtnClickedWithTopicComment:(TopicComment *)topicComment andBtn:(UIButton *)btn{
    if(topicComment.hasShowMoreChildCommentFlag != YXSquareCommentCellCommentViewShowNormal){
        topicComment.hasShowMoreChildCommentFlag = YXSquareCommentCellCommentViewShowNormal;
    }else{
        YXTopicCommentDetailViewController * vc = [[YXTopicCommentDetailViewController alloc]init];
        [vc setType:self.type];
        [vc getTopicDetailOfTopicID:self.topicDetail.topicDetail.topicID andCommentID:topicComment.commentID andTopicCreator:self.topicDetail.topicDetail.creator];
        [vc getLastName:topicComment.fromUser andTopicCreator:self.topicDetail.topicDetail.creator];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    [self.tableView reloadData];
}
-(void)miniCommentViewClickedDeleteBtn:(TopicComment *)topicComment{
    [self.tableView reloadData];
}
-(void)miniCommentViewClickedNickName:(SUser *)user{
    YXMyCenterViewController * mycenterVC = [[YXMyCenterViewController alloc]init];
    [mycenterVC setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:user.id];
    [self.navigationController pushViewController:mycenterVC animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)gotoPersonCenter:(Topic *)topicDetail
{
    YXMyCenterViewController * detailler = [[YXMyCenterViewController alloc]init];
    
    [detailler setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:topicDetail.creator.id];
    detailler.hidesBottomBarWhenPushed = YES;
    if (detailler.isAnimating) {
        return;
    }
    detailler.isAnimating = YES;
    
    [self.navigationController pushViewController:detailler animated:YES];
}

#pragma 删除话题
-(void)deleteTopic:(Topic *)topicDetail
{
    NSDictionary * params = @{@"uid":USERID,@"token":@"100",@"resourceID":topicDetail.topicID,@"resourceType":@"0"};
    [HttpTool postWithPath:self.type == YXTopicDetailTypeStory ?@"DeleteStoryResource":@"DeleteSquareResource" params:params success:^(id JSON) {
        if ([[JSON objectForKey:@"result"]isEqualToString:@"100"]) {
            //删除成功
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"删除资源成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"kYXSquareViewControllerRefreshDataNotification" object:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
    
}


#pragma mark 举报话题

-(void)reportTopic:(Topic *)topicDetail
{
    
    ReportTopicViewController * detailViewController = [[ReportTopicViewController alloc]init];
    
    [detailViewController setReportDetail:self.topicDetail.topicDetail.topicID];
    
    detailViewController.hidesBottomBarWhenPushed = YES;
    if (detailViewController.isAnimating) {
        return;
    }
    detailViewController.isAnimating = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"kYXSquareMoreItemViewHideNotification" object:nil];
}
#pragma mark 发表评论
///评论发送按钮被点击代理
-(void)squareCommentTextViewSendButtonClicked{
    NSString *content =  self.commentTextView.text;
    if([content length]<1)return;
    [self.commentTextView setEditing:NO];
    [HttpTool postWithPath:self.type == YXTopicDetailTypeStory?@"CommentStoryResource":@"CommentSquareResource" params:@{
                                                             @"uid":USERID,
                                                             @"token":@"110",
                                                             @"resourceID":self.topicDetail.topicDetail.topicID,
                                                             @"resourceType":@"0",
                                                             @"toUserID":self.topicDetail.topicDetail.creator.id,
                                                             @"content":content
                                                             
                                                             } success:^(id JSON) {
                                                                [self.commentTextView setText:@""];
                                                                 [self refreshData];
                                                                 
        
    } failure:^(NSError *error) {
        MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud setMode:MBProgressHUDModeText];
        [hud setLabelText:@"网络错误，回复失败。"];
        [hud show:YES];
        [hud hide:YES afterDelay:2];
    }];
}

#pragma mark -
#pragma mark - 键盘高度
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"kYXSquareMoreItemViewHideNotification" object:nil];
}
- (void) registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardShow:(NSNotification *)notification{
    CGRect keyBoardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat deltaY = keyBoardRect.size.height;
    WS(weakSelf)
    [self.commentTextView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(weakSelf.view.mas_bottom).offset = -deltaY;
    }];
    [UIView animateWithDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue]
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

-(void)keyboardHide:(NSNotification *)notification {
    [UIView animateWithDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue]
                     animations:^{
             WS(weakSelf)
             [self.commentTextView mas_updateConstraints:^(MASConstraintMaker *make) {
                 make.bottom.mas_equalTo(weakSelf.view.mas_bottom).offset =weakSelf.commentTextView.frame.size.height;
             }];
     }];
}

@end
