//
//  YXTopicCommentDetailViewController.m
//  inface
//
//  Created by lizhen on 15/10/9.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "YXTopicCommentDetailViewController.h"
#import "YXTopicCommentDetailOneCell.h"
#import "YXTopicCommentDtetailTwoCell.h"
#import "YXSquareMiniCommentView.h"
#import "TopicComment.h"
#import "Topic.h"
#import "SUser.h"
#import "MJRefresh.h"
#import "YXMyCenterViewController.h"
#import "TopicDetail.h"
#import "TopicCommentDetail.h"
#import "AFNetworking.h"
#import "UIView+LQXkeyboard.h"
#import "MBProgressHUD.h"
#import "YXSquareCommentTextView.h"
#import <Masonry.h>
#import "ReportTopicViewController.h"
#import "MBProgressHUD+YXStyle.h"
#import "YXPlayIntroductionViewController.h"
#import "YXFrontCoverViewController.h"


#define placehode [NSString stringWithFormat:@"回复 %@:",user.nickName]

@interface YXTopicCommentDetailViewController ()<UITableViewDataSource,UITableViewDelegate,YXSquareCommentTextViewDelegate>
@property(nonatomic, strong)UITableView *tableView;
///第一个cell的数据源
@property(nonatomic, strong)NSMutableArray *dataArray;
/// 状态标识 避免冲突
@property (nonatomic, assign) BOOL btnStatus1;
@property (nonatomic, assign) BOOL btnStatus2; 
@property (nonatomic, assign) BOOL btnStatus3;
///判断第几级回复
@property (nonatomic, copy) NSString * isOneOrTwo;
///第二级回复中的resourceID
@property (nonatomic, copy) NSString * childID;
///接收cell返回的tipic的高度
@property(nonatomic,assign)NSInteger topicContentOfHight;
@property(nonatomic,assign)NSInteger currentPage;
//名字的长度
@property(nonatomic,assign)NSInteger nameWidth;
//点赞数
@property(nonatomic,copy)NSString *favouriteCount;
@property(nonatomic,copy)NSString *key;
///其余cell文字的高度
@property(nonatomic,assign)float cellOtherH;
@property(nonatomic,assign)float placehodelLength;
///话题评论模型
@property(nonatomic, strong)TopicComment *topicComment;
///话题评论详情模型
@property(nonatomic, strong)TopicCommentDetail *topicCommentDetail;
///话题详情
@property(nonatomic, strong)TopicDetail *topicDetail;
@property(nonatomic,copy)NSString * w_topicID;
@property(nonatomic,copy)NSString * w_commentID;
///标志位
@property(nonatomic,assign)NSInteger idendify;
@property(nonatomic,strong)YXSquareCommentTextView * commentTextView;

@property(nonatomic,assign)YXTopicDetailType type;
@end
@implementation YXTopicCommentDetailViewController{
    
    ///第一个cell
    YXTopicCommentDetailOneCell *cell;
    ///其余cell
    YXTopicCommentDtetailTwoCell *cellOther;
    TopicComment *n_topicComment;
    SUser *n_user;
    TopicComment *lastComment;
    SUser *lastUser;
    ///点赞/收藏用的
    TopicComment *praiseTopicComment;
    ///楼主的模型
    SUser *topicCreatorUser;
    TopicComment *tmpTopicComment;
    ///是谁评论的
    NSString *whoIsWho;
    ///评论内容字符串
    NSString *content;
    ///点击评论详情页的层主
    SUser *user_cell;

}
#pragma mark -
#pragma mark 设置请求接口
-(void)setType:(YXTopicDetailType)type{
    _type = type;
    
}

-(NSString *)topicCommentDetailUri{

    if(_type == YXTopicDetailTypeStory){
        return @"StoryCommentDetail";
    }else{
        return @"TopicCommentDetail";
    }
}

-(instancetype)init{
    if (self = [super init]) {
        self.view.backgroundColor = [UIColor whiteColor];
        _currentPage = 0;
        [self.view addSubview:self.tableView];
        self.tableView.hidden = YES;
        [self.view setUserInteractionEnabled:YES];
        [self.tableView addFooterWithTarget:self action:@selector(loadMoreData)];
    }
    return self;
}

-(NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

//左上角按钮
-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerForKeyboardNotifications];
    [self sendNotificationFromotherCell];
    self.btnStatus1 = YES;
    self.btnStatus2 = YES;
    [self.view setUserInteractionEnabled:YES];
    [self createLeftButton];
    
    [[IQKeyboardManager sharedManager]setEnable:NO];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];

    //TableView单击手势
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyBoard)];
    [self.tableView addGestureRecognizer:tapGesture];
}

-(void)dismissKeyBoard{
    
    [self.view endEditing:YES];
}

-(void)createLeftButton{

    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
}

//发送来自cell的通知
-(void)sendNotificationFromotherCell{

    //通知
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(recvBcast:) name:@"ClickOnName" object:nil];
    [nc addObserver:self selector:@selector(recvBcastPersonCenter:) name:@"ClickAfterGoToPersonCenter" object:nil];
    [nc addObserver:self selector:@selector(delTopicComment:) name:@"delTopicComment" object:nil];
}

//得到上个界面的数据
-(void)getTopicDetailOfTopicID:(NSString *)topicID andCommentID:(NSString *)commentID andTopicCreator:(SUser *)topicCreator{

    _w_topicID = topicID;
    _w_commentID = commentID;
    topicCreatorUser = topicCreator;
    [self loadData];
    //[self loadMoreData];
}

-(void)topicDetailOfTopicID:(TopicDetail *)topicModel andCommentID:(TopicComment *)commentModel andTopicCreator:(SUser *)topicCreator{

    n_topicComment = commentModel;
    praiseTopicComment = commentModel;
    topicCreatorUser = topicCreator;

}

-(void)getTopicDetailOfTopicID:(NSString *)topicID User:(SUser *)user andComment:(TopicComment *)comment andTopicCreator:(SUser *)topicCreator{

    _w_topicID = topicID;
    _w_commentID = comment.commentID;
    lastComment = comment;
    praiseTopicComment = comment;
    lastUser = user;
    topicCreatorUser = topicCreator;
    [self loadData];
    //[self loadMoreData];
    [self tanchu:user];
    _isOneOrTwo = @"2";
}

//上拉加载更多
-(void)loadMoreData{
    self.currentPage = _currentPage+1;
    [HttpTool postWithPath:[self topicCommentDetailUri] params:@{@"uid":USERID,@"token":@"110",_type == YXTopicDetailTypeStory ?@"storyID":@"topicID":_w_topicID,@"commentId":_w_commentID,@"index":[NSString stringWithFormat:@"%ld",(long)self.currentPage],@"count":@"20"} success:^(id JSON) {
        
        TopicCommentDetail * topicDetail = [TopicCommentDetail objectWithKeyValues:JSON];
        _topicCommentDetail = topicDetail;
        [_dataArray addObjectsFromArray:_topicCommentDetail.childComment];
        //判断是否有新的数据
        if (topicDetail.childComment.count == 0) {
            if (ScreenHeight== 480) {
                
            }else{
            MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [hud setMode:MBProgressHUDModeText];
            [hud setLabelText:@"没有更多了"];
            [hud show:YES];
            [hud hide:YES afterDelay:2];
            }
        }
        if (self.currentPage == 0) {
            _topicComment = _topicCommentDetail.mainComment;
        }
        [_tableView reloadData];
        [self.tableView footerEndRefreshing];
        
    } failure:^(NSError *error) {
        
    }];
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableView setShowsVerticalScrollIndicator:NO];
    }
    return _tableView;
}
//加载数据
-(void)loadData{
    
    [MBProgressHUD showYXLoadingMiniViewAddToView:self.view animated:YES];
    [HttpTool postWithPath:[self topicCommentDetailUri] params:@{ @"uid":USERID,@"token":@"110",_type == YXTopicDetailTypeStory ?@"storyID":@"topicID":_w_topicID ,@"commentID":_w_commentID ,@"index":@"0",@"count":@"30"} success:^(id JSON) {
        [_commentTextView setHidden:NO];
        self.currentPage = 0;
        TopicCommentDetail * topicCommentDetail = [TopicCommentDetail objectWithKeyValues:JSON];
        _topicCommentDetail = topicCommentDetail;
        self.dataArray = [NSMutableArray array];
        [self.dataArray addObjectsFromArray:topicCommentDetail.childComment];
         _topicComment = _topicCommentDetail.mainComment;
        self.navigationItem.title =[NSString stringWithFormat:@"%@楼", _topicComment.floorCount];
        _favouriteCount = _topicComment.favorCount;
        cell.countLabel.text = _favouriteCount;
        [_tableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.tableView.hidden = NO;
        
        if ([[[UIDevice currentDevice]systemVersion]floatValue]<8.0 | ScreenHeight == 480) {
            [self loadMoreData];
        }else{
        }
        //在此判断是第一次加载数据，还是发送消息后的刷新数据
        if (_idendify ==1) {
            
        }else{
        [self.commentTextView setEditing:YES];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}

#pragma mark  delegate of tableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _dataArray.count+1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
    cell = [tableView dequeueReusableCellWithIdentifier:@"YXTopicCommentDetailOneCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"YXTopicCommentDetailOneCell" owner:self options:nil]lastObject];
    }
        [cell setCellContentWithModel:_topicComment];
        [self headCellContent];
       self.topicContentOfHight = cell.topicContentHight;
    return cell;
    
    }else {
        cellOther = [tableView dequeueReusableCellWithIdentifier:@"YXTopicCommentDtetailTwoCell"];
        
        if (!cellOther) {
            cellOther = [[[NSBundle mainBundle]loadNibNamed:@"YXTopicCommentDtetailTwoCell" owner:self options:nil]lastObject];
            }
        
            _cellOtherH = [cellOther setCellContentOne:self.dataArray[indexPath.row-1] andTopicCreator:topicCreatorUser];
        return cellOther;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        return self.topicContentOfHight+68;
    }
    else
    {
       // LOG(@"%f********",_cellOtherH);
        
        //return _cellOtherH;
        return [cellOther setCellContentOne:self.dataArray[indexPath.row-1] andTopicCreator:topicCreatorUser];
    }
}

//头部cell中的包含的控件
-(void)headCellContent{

    [cell.pingLunButton addTarget:self action:@selector(pingLunButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.iconView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(whenClickImage)];
    [cell.iconView addGestureRecognizer:singleTap];
    
    [cell.favourBtn addTarget:self action:@selector(favourBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    ///判断点赞按钮的状态
    if (cell.favourBtn.isSelected == YES) {
        self.btnStatus3 = YES;
      
    }else{
        self.btnStatus3 = NO;
    
    }

}

//个人中心的通知(跳转个人中心)层主
-(void)whenClickImage{
    
    YXMyCenterViewController * personCenter = [[YXMyCenterViewController alloc]init];
    
    [personCenter setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:user_cell.id];
    personCenter.hidesBottomBarWhenPushed = YES;
    if (personCenter.isAnimating) {
        return;
    }
    personCenter.isAnimating = YES;
    [self.navigationController pushViewController:personCenter animated:YES];
}

//点击回复方法(通知方法)
-(void)recvBcast:(NSNotification *)notify{
    
    NSDictionary *dict = [notify userInfo];
    TopicComment *Cpmment = [dict objectForKey:@"ThemeName"];
    tmpTopicComment = Cpmment;
    n_user = Cpmment.fromUser;
    whoIsWho = @"40";
    _isOneOrTwo = @"2";
    [self.commentTextView setEditing:YES];
    
    [self tanchu:Cpmment.fromUser];
    
}

//个人中心的通知(跳转个人中心)（评论用户）
-(void)recvBcastPersonCenter:(NSNotification *)notify{

    SUser *name = [[notify userInfo] objectForKey:@"ThemeName"];
    
    YXMyCenterViewController * personCenter = [[YXMyCenterViewController alloc]init];
    
    [personCenter setFromType:YXMyCenterViewControllerTypeFromOtherPage andPersonID:name.id];
    personCenter.hidesBottomBarWhenPushed = YES;
    if (personCenter.isAnimating) {
        return;
    }
    personCenter.isAnimating = YES;
    [self.navigationController pushViewController:personCenter animated:YES];
    
}

//删除操作
-(void)delTopicComment:(NSNotification *)notify{

    _idendify = 1;
    NSDictionary *dict = [notify userInfo];
    TopicComment *Cpmment = [dict objectForKey:@"delTopicComment"];
    NSString *key = Cpmment.commentID;
    NSString *leavel = Cpmment.level;
    
    [HttpTool postWithPath:_type == YXTopicDetailTypeStory ?@"DeleteStoryResource": @"DeleteSquareResource" params:@{ @"uid":USERID, @"token":@"110",@"resourceType":[leavel isEqualToString:@"2"]? @"3" : @"2",@"resourceID":key,
                                                             
               } success:^(id JSON) {
                   
                   [HttpTool postWithPath:[self topicCommentDetailUri] params:@{@"uid":USERID,@"token":@"110",_type == YXTopicDetailTypeStory ?@"storyID":@"topicID":_w_topicID,@"commentID":_w_commentID ,@"index":@"0",@"count":@"30"} success:^(id JSON) {
                       self.currentPage = 0;
                       TopicCommentDetail * topicCommentDetail = [TopicCommentDetail objectWithKeyValues:JSON];
                       _topicCommentDetail = topicCommentDetail;
                       self.dataArray = [NSMutableArray array];
                       [self.dataArray addObjectsFromArray:topicCommentDetail.childComment];
                       _topicComment = _topicCommentDetail.mainComment;
                       self.navigationItem.title =[NSString stringWithFormat:@"%@楼", _topicComment.floorCount];
                       [self.tableView reloadData];
                       [cell.favourBtn setSelected:YES];
                       
                   } failure:^(NSError *error) {
                       
                   }];

                   
                   
                   
                   
                   MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                   [hud setMode:MBProgressHUDModeText];
                   [hud setLabelText:@"删除成功"];
                   [hud show:YES];
                   [hud hide:YES afterDelay:1];
               } failure:^(NSError *error) {
                   [MBProgressHUD hideHUDForView:self.view animated:YES];
               }];
  
}

//获取上个界面的点击（直接点击cell的时候）
-(void)getLastName:(SUser *)user andTopicCreator:(SUser *)topicCreator{

    n_user = user;
    user_cell = user;
    _isOneOrTwo = @"1";
    topicCreatorUser = topicCreator;
    whoIsWho = @"20";
    [self tanchu:user];
    
    
}

///直接点击评论用户
-(void)getLastClickModel:(TopicComment *)topicComment andTopicCreator:(SUser *)topicCreator{

    topicCreatorUser = topicCreator;
    user_cell = topicComment.fromUser;
    [self tanchu:topicComment.toUser];
    LOG(@"%@+++++",topicComment.fromUser.id);

}
///弹出键盘   判断是不是本人
-(void)tanchu:(SUser *)user{

    _nameWidth = user.nickName.length;
    //[self.commentTextView setEditing:YES];
    [self.commentTextView setText:placehode];
    _placehodelLength = placehode.length;
}


#pragma mark 评论TextView
-(YXSquareCommentTextView *)commentTextView{
    if (!_commentTextView) {
        _commentTextView = [[YXSquareCommentTextView alloc]init];
        _commentTextView.delegate = self;
        [self.view addSubview:_commentTextView];
        [_commentTextView setHidden:YES];
        WS(weakSelf)
        [_commentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.view.mas_left);
            make.width.equalTo(weakSelf.view.mas_width);
            make.height.equalTo(@40);
            make.bottom.equalTo(weakSelf.view.mas_bottom);
        }];
        [self.view layoutIfNeeded];
    }
    return _commentTextView;
}
#pragma mark - 键盘高度
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
//发送键盘的通知
- (void) registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardShow:(NSNotification *)notification{
    CGRect keyBoardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat deltaY = keyBoardRect.size.height;
    WS(weakSelf)
    [self.commentTextView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(weakSelf.view.mas_bottom).offset = -deltaY;
    }];
    [UIView animateWithDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue]
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

-(void)keyboardHide:(NSNotification *)notification {
    [UIView animateWithDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue]
                     animations:^{
                         WS(weakSelf)
                         [self.commentTextView mas_updateConstraints:^(MASConstraintMaker *make) {
                             make.bottom.mas_equalTo(weakSelf.view.mas_bottom).offset =weakSelf.commentTextView.frame.size.height;
                         }];
                     }];
}

//滑动退出
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    /**
     *  退出键盘
     */
    [self.view endEditing:YES];
}

///评论发送按钮被点击代理
-(void)squareCommentTextViewSendButtonClicked{
    
    _idendify = 1;
    
    //清除评论内容的前缀
    NSString *contentTmp =  self.commentTextView.text;
  
    if([contentTmp length]<1)return;
    
    if (contentTmp.length < _placehodelLength) {
        
    }else{
    
        content = [contentTmp substringFromIndex:_nameWidth +4];
    }
    
    //判断来自点击的地方
    if (n_topicComment.childComment == nil) {
        
        if ([_isOneOrTwo isEqualToString:@"2"]) {
            
            if (lastComment == nil) {
                
                self.key = tmpTopicComment.commentID;
                
            }else{
                
                TopicComment *mmm = [lastComment.childComment objectAtIndex:0];
                self.key = mmm.commentID;
                
                if ([whoIsWho isEqualToString:@"10"]) {
                    n_user = praiseTopicComment.fromUser;
                }else{
                    
                    if ([whoIsWho isEqualToString:@"40"]) {
                        n_user = tmpTopicComment.fromUser;
                    }else{
                    
                    n_user = lastUser;
                    }
                }
            }
            
        }else{
            _isOneOrTwo = @"1";
            if (n_topicComment == nil) {
                self.key = _w_commentID;
                if ([whoIsWho isEqualToString:@"10"]) {
                    if (praiseTopicComment == nil) {
                        n_user = _topicComment.fromUser;
                    }else{
                        n_user = praiseTopicComment.fromUser;
                    }
                }else{
                }
            }else{
                self.key = n_topicComment.commentID;
            }
        }
    }else{
    
       if ([_isOneOrTwo isEqualToString:@"2"]) {
           if (n_topicComment.childComment.count == 0) {
            
               self.key = tmpTopicComment.commentID;
           }else{
            TopicComment *mmm = [n_topicComment.childComment objectAtIndex:0];
            self.key = mmm.commentID;
        }
    }else{
            self.key = n_topicComment.commentID;
            n_user = nil;
            n_user = praiseTopicComment.fromUser;
        }
    }
    
    
    if (contentTmp.length < _placehodelLength) {
        if (praiseTopicComment == nil) {
            n_user = self.topicComment.fromUser;
        }else{
           n_user = praiseTopicComment.fromUser;
        }
           content = self.commentTextView.text;
    }

    if (contentTmp.length == _placehodelLength)return;
    [HttpTool postWithPath:_type == YXTopicDetailTypeStory ?@"CommentStoryResource":@"CommentSquareResource" params:@{ @"uid":USERID, @"token":@"110",@"resourceType":_isOneOrTwo,@"resourceID":self.key,@"toUserID":n_user.id,@"content":content
        } success:^(id JSON) {
            
                                self.commentTextView.text = nil;
            if ([[[UIDevice currentDevice]systemVersion]floatValue]<8.0 | ScreenHeight == 480) {
                [self loadData];
            }else{
            
                               [HttpTool postWithPath:[self topicCommentDetailUri] params:@{@"uid":USERID,@"token":@"110",_type == YXTopicDetailTypeStory ?@"storyID":@"topicID":_w_topicID,@"commentID":_w_commentID ,@"index":@"0",@"count":@"30"} success:^(id JSON) {
                                       self.currentPage = 0;
                                       TopicCommentDetail * topicCommentDetail = [TopicCommentDetail objectWithKeyValues:JSON];
                                       _topicCommentDetail = topicCommentDetail;
                                       self.dataArray = [NSMutableArray array];
                                       [self.dataArray addObjectsFromArray:topicCommentDetail.childComment];
                                       _topicComment = _topicCommentDetail.mainComment;
                                       self.navigationItem.title =[NSString stringWithFormat:@"%@楼", _topicComment.floorCount];
                                       [self.tableView reloadData];
                                   [cell.favourBtn setSelected:YES];
                
                                   
                               } failure:^(NSError *error) {
                                   
                               }];
            }
            [self.view endEditing:YES];
            
               } failure:^(NSError *error) {
                   MBProgressHUD * hud = [MBProgressHUD HUDForView:self.view];
                   [hud setMode:MBProgressHUDModeText];
                   [hud setLabelText:@"网络错误，回复失败。"];
                   [hud show:YES];
                   [hud hide:YES afterDelay:2];
               }];
}


//评论点击动画
-(void)pingLunButtonClick:(UIButton *)btn{
    
    if (self.btnStatus1 == YES) {
        cell.bingView.hidden = NO;
        [cell.juBaoBtn addTarget:self action:@selector(juBaoBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        //[cell.shoucangBtn addTarget:self action:@selector(shoucangBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.pinglunBtn addTarget:self action:@selector(juBaoBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        cell.bingView.hidden = YES;
    }
    self.btnStatus1 = !self.btnStatus1;
}

//举报&评论按钮
-(void)juBaoBtnClick:(UIButton *)btn{

    if (btn.tag == 2) {
        //举报
        ReportTopicViewController * detailViewController = [[ReportTopicViewController alloc] init];
        [detailViewController setReportType:@"1" resourceId:praiseTopicComment.commentID];
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    }else{
        //点击评论按钮
        _isOneOrTwo = @"1";
        whoIsWho = @"10";
        [self.commentTextView setEditing:YES];
        self.commentTextView .text = @"";
        if (self.commentTextView.text.length == 0) {
            _nameWidth = -4;
        }else{
   
        }
    }
}



////收藏按钮
//-(void)shoucangBtnClick:(UIButton *)btn{
//   
//    if (self.btnStatus2 == YES) {
//        cell.shoucangButton.selected = YES;
//    }
//    else{
//        cell.shoucangButton.selected = NO;
//    }
//    self.btnStatus2 = !self.btnStatus2;
//}

//点赞按钮
-(void)favourBtnClick:(UIButton *)btn{
    
    int a = [cell.countLabel.text intValue];
    if (self.btnStatus3 == YES) {
        btn.selected = NO;
       
        a = a-1;
        
    }
    else{
        btn.selected = YES;
     
        a = a+1;
        
    }
    
    [self favouriteBtn];
    self.btnStatus3 = !self.btnStatus3;
    cell.countLabel.text = [NSString stringWithFormat:@"%d",a];

}

//点赞
-(void)favouriteBtn{

[HttpTool postWithPath:_type == YXTopicDetailTypeStory?@"PraiseStoryResource":@"PraiseSquareResource" params:@{ @"uid":USERID, @"token":@"110",@"resourceType":@"1",@"resourceID":praiseTopicComment.commentID,@"operation":self.btnStatus3 == YES ? @"0":@"1"
                                                             
        } success:^(id JSON) {
        LOG(@"%@",JSON);
        MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [hud setMode:MBProgressHUDModeText];
            if (self.btnStatus3 == YES) {
                [hud setLabelText:@"点赞成功"];
                }else{
                [hud setLabelText:@"取消点赞成功"];
                }
             [hud show:YES];
             [hud hide:YES afterDelay:2];
              
          } failure:^(NSError *error) {
          }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
}

@end
