//
//  ReportTopicViewController.h
//  inface
//
//  Created by appleone on 15/10/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"


@interface ReportTopicViewController : BaseViewController
@property (assign,nonatomic)     BOOL isAnimating;
-(void)setReportDetail:(NSString *)resouceId;
-(void)setReportType:(NSString *)reportType resourceId:(NSString *)resourceId;
@end
