//
//  YXTopicDetailViewController.h
//  inface
//
//  Created by 邢程 on 15/10/11.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


typedef NS_ENUM(NSInteger, YXTopicDetailType){
    YXTopicDetailTypeDetail = 0,
    YXTopicDetailTypeStory  = 1
};
@interface YXTopicDetailViewController : BaseViewController
@property(assign,nonatomic)    BOOL   isAnimating;
/**
 *  设置数据类型
 *
 *  @param type YXTopicDetailTypeDetail(默认) 广场话题类型 ，YXTopicDetailTypeStory 剧和自戏的评论类型
 */

-(void)setIsTopicView;
-(void)setType:(YXTopicDetailType)type;
/**
 *  设置话题ID
 *
 *  @param topicID 话题ID
 */
-(void)setTopicID:(NSString *)topicID;
@end
