//
//  ReportTopicViewController.m
//  inface
//
//  Created by appleone on 15/10/18.
//  Copyright (c) 2015年 huangzs. All rights reserved.
//

#import "ReportTopicViewController.h"
#import "Topic.h"
#import "ReportTableViewCell.h"
typedef enum : NSUInteger {
    YXReportTopic,
    YXReportComment
    
} YXReportType;
@interface ReportTopicViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) Topic * topic_detail;
@property (strong,nonatomic) NSArray * tableData;
@property (assign,nonatomic) YXReportType     reportType;
@property (strong,nonatomic) NSString * selectIndex;
@property (strong,nonatomic) NSString * resourceId;
@end

@implementation ReportTopicViewController



-(void)returnBackl{
    [self.navigationController popViewControllerAnimated:YES];
}

//右上角按钮
-(void)returnBackr{
    
    //提交举报原因
    
    NSString * userid=[[NSUserDefaults standardUserDefaults]objectForKey:@"userid"];

    NSString * reportType = [NSString stringWithFormat:@"%lu",(unsigned long)self.reportType];
    if (!self.selectIndex || [self.selectIndex isEqualToString:@""]) {
        UIAlertView * alert  = [[UIAlertView alloc]initWithTitle:@"请输入举报理由" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
    }
    NSDictionary *params = @{@"uid":userid,@"resourceID":self.resourceId,@"reportReason":self.selectIndex,@"token":@"110",@"resourceType":reportType};
    [HttpTool postWithPath:@"ReportSquareResource" params:params success:^(id JSON) {
        
        if ([[JSON objectForKey:@"result"]integerValue]==100) {
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"举报成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            [self.navigationController popViewControllerAnimated:YES];
            
            
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:[JSON objectForKey:@"describe"] message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertview show];
            
        }
        
    } failure:^(NSError *error) {
        
        
    }];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //左上角按钮
    UIImage *LeftImage=[UIImage imageNamed:@"zs_fanhui"];
    UIButton *LeftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 21, 21)];
    [LeftButton setImage:LeftImage forState:UIControlStateNormal];
    [LeftButton addTarget:self action:@selector(returnBackl) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback=[[UIBarButtonItem alloc]initWithCustomView:LeftButton];
    self.navigationItem.leftBarButtonItem=barback;
    
    //右上角按钮
    UIButton *RightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [RightButton setTitle:@"提交" forState:UIControlStateNormal];
    RightButton.titleLabel.font=[UIFont systemFontOfSize:14];
    [RightButton setTitleColor:BLUECOLOR forState:UIControlStateNormal];
    [RightButton addTarget:self action:@selector(returnBackr) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barback2=[[UIBarButtonItem alloc]initWithCustomView:RightButton];
    self.navigationItem.rightBarButtonItem=barback2;
    
    self.navigationItem.title=@"举报";
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableFooterView=[[UIView alloc]init];
    
    [self getReportList];
}

-(void)setReportType:(NSString *)reportType resourceId:(NSString *)resourceId;
{
    self.reportType = YXReportComment;
    
    self.resourceId = resourceId;
}

-(void)setReportDetail:(NSString *)resouceId
{
    self.resourceId = resouceId;
    self.reportType = YXReportTopic;
}

#pragma mark获取举报列表
-(void)getReportList
{
    self.tableData = @[@"广告",@"色情",@"反动",@"头像",@"其他"];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"ReportTableViewCell";
    ReportTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell= [[[NSBundle mainBundle]loadNibNamed:@"ReportTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    cell.UserNameLabel.text=[self.tableData objectAtIndex:indexPath.row];
    if (self.selectIndex && ![self.selectIndex isEqualToString:@""]) {
        if ([self.selectIndex integerValue] == indexPath.row) {
            [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang"] forState:UIControlStateNormal];
        }else{
            [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang-1"] forState:UIControlStateNormal];

        }
    }else{
        [cell.SelectButton setImage:[UIImage imageNamed:@"7fuxuankuang-1"] forState:UIControlStateNormal];

    }

    
    cell.SelectButton.tag=10000+indexPath.row;
    [cell.SelectButton addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 70;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *aview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 70)];
    aview.backgroundColor=[UIColor clearColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, ScreenWidth-40, 50.0f)];
    label.font = [UIFont systemFontOfSize:16.0f];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentLeft;
    label.text=@"请选择举报原因";
    [aview addSubview:label];
    
    return aview;
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 44;
}




//选中一行的时候
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //跳转到详细的视图控制器
    
    
    //取消选中的行
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

//选择按钮
-(void)SelectButton:(UIButton *)sender{
    UIButton *btn = (UIButton *)sender;
    
//    [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithInteger:btn.tag-10000] forKey:@"SelectedRowReport"];
    self.selectIndex = [NSString stringWithFormat:@"%ld",btn.tag-10000];
    
    [self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    _isAnimating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    _isAnimating = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
