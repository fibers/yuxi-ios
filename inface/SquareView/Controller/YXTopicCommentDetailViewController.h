//
//  YXTopicCommentDetailViewController.h
//  inface
//
//  Created by lizhen on 15/10/9.
//  Copyright © 2015年 huangzs. All rights reserved.
//

#import "BaseViewController.h"
#import "YXTopicDetailViewController.h"
@class SUser,TopicComment,TopicDetail;
@interface YXTopicCommentDetailViewController : BaseViewController

/**
 *  设置数据类型
 *
 *  @param type YXTopicDetailTypeDetail(默认) 广场话题类型 ，YXTopicDetailTypeStory 剧和自戏的评论类型
 */
-(void)setType:(YXTopicDetailType)type;


-(void)getTopicDetailOfTopicID:(NSString *)topicID andCommentID:(NSString *)commentID andTopicCreator:(SUser *)topicCreator;

-(void)topicDetailOfTopicID:(TopicDetail *)topicModel andCommentID:(TopicComment *)commentModel andTopicCreator:(SUser *)topicCreator;

-(void)getLastName:(SUser *)user andTopicCreator:(SUser *)topicCreator;

-(void)getLastClickModel:(TopicComment *)topicComment andTopicCreator:(SUser *)topicCreator;
///直接点击上一个页面
-(void)getTopicDetailOfTopicID:(NSString *)topicID User:(SUser *)user andComment:(TopicComment *)comment andTopicCreator:(SUser *)topicCreator;


@end
